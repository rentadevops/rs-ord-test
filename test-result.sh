#!/bin/sh
# https://devops.stackexchange.com/questions/6085/how-can-we-get-the-jenkins-console-output-in-a-text-file
# https://unix.stackexchange.com/questions/264962/print-lines-of-a-file-between-two-matching-patterns
# https://issues.jenkins.io/browse/JENKINS-61263

echo "Getting test results log from: $1"
#cd ci
cat ./run-log.txt
curl -q $1 > ./run-log.txt
#wget $1 -O ./run-log.txt
#cat $1 >> run-log.txt'

echo "Get text between Result and maven-surefire report-plugin sysout"
pattern1="Results:"
pattern2="Tests run:"
sed -n "/$pattern1/,/$pattern2/p" ./run-log.txt

echo "Get final Test run counts "
raw_result=$( sed -n "/$pattern1/,/$pattern2/p" ./run-log.txt | grep 'Tests run:' )

echo "Remove text [INFO]/[WARNING]/[ERROR] and keep the part after ']' and save to a file"
trimmed_result=${raw_result##*]}
echo $trimmed_result > ./result-log.txt
cat ./result-log.txt