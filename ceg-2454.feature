Feature: CEG-2454

  As a LC user, I want to be able to create multiple wallbox orders for one vehicle order

  @CEG-2454 @COMPLETED
  Scenario: Success test
    Given I’m a LC user
    When I create wallbox orders
    And the vehicle order for which I create a wallbox order, already has a linkedWallBoxOrderId
    Then this succeeds
