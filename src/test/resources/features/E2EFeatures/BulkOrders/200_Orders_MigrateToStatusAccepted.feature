@asciidoc
Feature: Bulk Migrate accepted 200

  [INFO]
  As a User I upload 200 orders through the MIGRATE-flow

  @migrate-200-to-status-accepted
  Scenario Outline: OL, Migrate to Accepted
    When Dummy <dummy>
    When Add system properties
      | system variable | value |
      | appLanguage     | nl    |
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery        | false    |
    When Add system properties
      | system variable | value |
      | wallbox         | No     |
    When Reset File Number
    #When Delete OrderImports Notification
    When Get Last Id previous test
    Given User is logged in
    When Create Migration OrderImports Request and Upload To FSTP
      | leasingType       | transactionCode | orderStatus | dealerDossierNumber | leasingCompanyRSNumber | DealerRSNumber | userEmail                  | trackingNumber | versionNumber | dateFirstRegistration | clientName        | clientContactPerson | clientLanguageCode | clientVAT       | clientStreetHouseNumber | clientPostalCode | clientCity | clientContactEmail | clientContactPhoneNumber | clientContactMobileNumber | clientContactFaxNumber | driverStreetHouseNumber | driverPostalCode | driverCity | driverContactEmail | driverContactPhoneNumber | driverContactMobileNumber | driverContactFaxNumber | driverNameImport | driverLanguageCode | vehicleBrand | vehicleCylinderContent | vehiclePower | vehicleLanguagePapers | vehicleWeight | vehicleTyreDescription | vehicleStockCar | vehicleCo2Emissions | vehicleIdentification | vehicleModel | vehicleVersion           | vehicleExteriorColor    | vehicleInteriorColor           | pricingTotalPriceInput | pricingListPriceInput | pricingDeliveryCostsIni | pricingDeliveryCosts | pricingOtherCosts | pricingDiscountAmount | pricingDiscountPercentage | pricingFleetDiscountAmount | pricingFleetDiscountPercentage | promotionName | promotionDescription | promotionDiscountAmount | deliveryDesiredDate | deliveryLocationName      | deliveryLocationLanguageCode | deliveryContactPerson | deliveryLocationVAT | deliveryStreetHouseNumber | deliveryPostalCode | deliveryCity | deliveryContactEmail    | deliveryContactPhoneNumber | deliveryContactMobileNumber | deliveryContactFaxNumber | insuranceName    | insuranceEmail   | insuranceValuePrice | buybackName | buybackAmount | buybackContractDuration | buybackStreetHouseNumber | buybackPostalCode | buybackCity | registrationDivToDoBy | vehicleVin        | vehicleVinControlNumber |
      | OPERATIONAL_LEASE | MIGRATE         | ACCEPTED    | DOS-DUMMY           | 799230                 | 730084         | rstest2@rentasolutions.org | 1TRACK         | v1.01         | 2021-02-28            | PWC/Deloitte TEST | Test Klantnaam      | fr                 | BE 0414.325.701 | Robert Schumanplein 2-4 | 1040             | ETTERBEEK  | contact@test.begin | 0031765733060            | 0475772277                | Fax12345               | Bliekstraat 999         | 9800             | ASTENE     | test@mail.begin    | 0032476460851            | 0477227722                | Fax1234567             | Jean             | fr                 | BMW          | 1995                   | 110          | fr                    | 1545          | Goodyear               | true            | 114,00              | VehicleCode           | X1 15        | X1 sDrive 18d 110kW Aut. | black sapphire metallic | sensatec canberrabeige/schwarz | 26795,46               | 28801,66              | 70,00                   | 70,00                | 25,00             | 120,00                | 10,00 %                   | 0,00                       | 0,00 %                         | Salonkorting  | korting op het salon | 5142,56                 | 2020-08-15          | Alternatieve leverlocatie | nl                           | Tim De Leverancier    | BE0720879353        | Mechelbaan 2              | 2500               | Lier         | contactlevering@mail.be | 015317531                  | 0477771177                  | Fax12345678              | Foyer Assurances | contact@foyer.lu | 79693,35            | Buyback Tim | 12000,00      | 10                      | Buybackbaan 5            | 2000              | Antwerpen   | CLIENT                | TDGM1PXEJSJ077744 | 023                     |
    When Switch user to dealer
    When User search order
#    When User marks messages as read
#    When User sends comment "The order has been accepted"
#    When User opens order overview
#    When User accepts order
#    When Switch user to leasing_company
#    When User opens order
#    When User opens order messages
#    When User sends comment "The order has been confirmed"
#    When User marks messages as read
#    When User opens order overview
#    When User confirms order
#   When Get Order Notifications Feed
#    When Switch user to dealer
#    When User opens order
#    When User opens order messages
#    When User sends comment "Weet nie?"
#    When User marks messages as read
#    When User opens order overview
#    When User checks vehicle arrived
#    When User enters delivery info
#    When Get Order By DossierNumber
#    When Switch user to leasing_company
#    When User opens order
#    When User opens order messages
#    When User marks messages as read
#    When User opens order overview
#    When User enters vehicle at supplier
#    When Switch user to dealer
#    When User opens order
#    When Digital delivery is "Yes"
#    When User prepares digital delivery
#    When Add system properties
#      | system variable | value   |
#      | platform | ANDROID |
#    When Add system properties
#      | system variable | value |
#      | language | nl |
#    When User goes to mobile website
#    When Get Order By DossierNumber
#    When Add system properties
#      | system variable | value |
#      | pdfLanguage | nl |
#    When Add system properties
#      | system variable | value             |
#      | leasingType | OPERATIONAL_LEASE |
#    When Switch user to dealer
#    When User search order
#    When User opens order documents
#    When User downloads order documents
#    When User opens pdf


    #When Download OrderImports Notification
    #Then User validates order notifications



    #When Get Order Detail By Id
    #When User opens order details
    #When User gets orderdetails data
    #Then User validates orderdetails are correct
    Then close browser

    Examples:
      | dummy      |
      | "dummy1"   |
      | "dummy2"   |
      | "dummy3"   |
      | "dummy4"   |
      | "dummy5"   |
      | "dummy6"   |
      | "dummy7"   |
      | "dummy8"   |
      | "dummy9"   |
      | "dummy10"  |
      | "dummy11"  |
      | "dummy12"  |
      | "dummy13"  |
      | "dummy14"  |
      | "dummy15"  |
      | "dummy16"  |
      | "dummy17"  |
      | "dummy18"  |
      | "dummy19"  |
      | "dummy20"  |
      | "dummy21"  |
      | "dummy22"  |
      | "dummy23"  |
      | "dummy24"  |
      | "dummy25"  |
      | "dummy26"  |
      | "dummy27"  |
      | "dummy28"  |
      | "dummy29"  |
      | "dummy30"  |
      | "dummy31"  |
      | "dummy32"  |
      | "dummy33"  |
      | "dummy34"  |
      | "dummy35"  |
      | "dummy36"  |
      | "dummy37"  |
      | "dummy38"  |
      | "dummy39"  |
      | "dummy40"  |
      | "dummy41"  |
      | "dummy42"  |
      | "dummy43"  |
      | "dummy44"  |
      | "dummy45"  |
      | "dummy46"  |
      | "dummy47"  |
      | "dummy48"  |
      | "dummy49"  |
      | "dummy50"  |
      | "dummy51"  |
      | "dummy52"  |
      | "dummy53"  |
      | "dummy54"  |
      | "dummy55"  |
      | "dummy56"  |
      | "dummy57"  |
      | "dummy58"  |
      | "dummy59"  |
      | "dummy60"  |
      | "dummy61"  |
      | "dummy62"  |
      | "dummy63"  |
      | "dummy64"  |
      | "dummy65"  |
      | "dummy66"  |
      | "dummy67"  |
      | "dummy68"  |
      | "dummy69"  |
      | "dummy70"  |
      | "dummy71"  |
      | "dummy72"  |
      | "dummy73"  |
      | "dummy74"  |
      | "dummy75"  |
      | "dummy76"  |
      | "dummy77"  |
      | "dummy78"  |
      | "dummy79"  |
      | "dummy80"  |
      | "dummy81"  |
      | "dummy82"  |
      | "dummy83"  |
      | "dummy84"  |
      | "dummy85"  |
      | "dummy86"  |
      | "dummy87"  |
      | "dummy88"  |
      | "dummy89"  |
      | "dummy90"  |
      | "dummy91"  |
      | "dummy92"  |
      | "dummy93"  |
      | "dummy94"  |
      | "dummy95"  |
      | "dummy96"  |
      | "dummy97"  |
      | "dummy98"  |
      | "dummy99"  |
      | "dummy100" |
      | "dummy101" |
      | "dummy102" |
      | "dummy103" |
      | "dummy104" |
      | "dummy105" |
      | "dummy106" |
      | "dummy107" |
      | "dummy108" |
      | "dummy109" |
      | "dummy110" |
      | "dummy111" |
      | "dummy112" |
      | "dummy113" |
      | "dummy114" |
      | "dummy115" |
      | "dummy116" |
      | "dummy117" |
      | "dummy118" |
      | "dummy119" |
      | "dummy120" |
      | "dummy121" |
      | "dummy122" |
      | "dummy123" |
      | "dummy124" |
      | "dummy125" |
      | "dummy126" |
      | "dummy127" |
      | "dummy128" |
      | "dummy129" |
      | "dummy130" |
      | "dummy131" |
      | "dummy132" |
      | "dummy133" |
      | "dummy134" |
      | "dummy135" |
      | "dummy136" |
      | "dummy137" |
      | "dummy138" |
      | "dummy139" |
      | "dummy140" |
      | "dummy141" |
      | "dummy142" |
      | "dummy143" |
      | "dummy144" |
      | "dummy145" |
      | "dummy146" |
      | "dummy147" |
      | "dummy148" |
      | "dummy149" |
      | "dummy150" |
      | "dummy151" |
      | "dummy152" |
      | "dummy153" |
      | "dummy154" |
      | "dummy155" |
      | "dummy156" |
      | "dummy157" |
      | "dummy158" |
      | "dummy159" |
      | "dummy160" |
      | "dummy161" |
      | "dummy162" |
      | "dummy163" |
      | "dummy164" |
      | "dummy165" |
      | "dummy166" |
      | "dummy167" |
      | "dummy168" |
      | "dummy169" |
      | "dummy170" |
      | "dummy171" |
      | "dummy172" |
      | "dummy173" |
      | "dummy174" |
      | "dummy175" |
      | "dummy176" |
      | "dummy177" |
      | "dummy178" |
      | "dummy179" |
      | "dummy180" |
      | "dummy181" |
      | "dummy182" |
      | "dummy183" |
      | "dummy184" |
      | "dummy185" |
      | "dummy186" |
      | "dummy187" |
      | "dummy188" |
      | "dummy189" |
      | "dummy190" |
      | "dummy191" |
      | "dummy192" |
      | "dummy193" |
      | "dummy194" |
      | "dummy195" |
      | "dummy196" |
      | "dummy197" |
      | "dummy198" |
      | "dummy199" |
      | "dummy200" |
      | "dummy201" |

     #Then close browser




