Feature: Create new order 17

[INFO]
As a user i create a new financial lease order

	@FL
	Scenario: financial lease
		When Add system properties
			|leasingType      |
			|FINANCIAL_LEASE  |
		When Add system properties
			| appLanguage     |
			| en |
		When Add system properties
			| language     |
			| en |
		When Add system properties
			| digitalDelivery     |
			| false |
		When Add system properties
			| wallbox     |
			| No |
		When Reset File Number
		When Delete OrderImports Notification
		When Get Last Id previous test
		Given User is logged in
		When Create OrderImports Request and Upload To FSTP
			|leasingType      |
			|FINANCIAL_LEASE  |
		When Switch user to dealer
		When User search order
		When Get Order Notifications Feed
		When User opens order messages
		When User sends comment "The order has been accepted"
		When User marks messages as read
		When Download OrderImports Notification
		Then User validates order notifications
		When Get Order Notifications Feed
		When User opens order overview
		When User accepts order
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates order notifications
		When Switch user to leasing_company
		When User opens order
		When User sends comment "The order has been confirmed"
		When User marks messages as read
		When Get Order Notifications Feed
		When User opens order overview
		When User confirms order
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates order notifications
		When Switch user to dealer
		When User opens order
		When User sends comment "Weet nie?"
		When User marks messages as read
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates order notifications
		When User opens order overview
		When User checks vehicle arrived
		When User enters delivery info
		When User enters vehicle at supplier
    #When Get Order By DossierNumber
    #When Switch user to leasing_company
    #When User opens order
    #When User marks messages as read

    #When Switch user to dealer
    #When User opens order
    #When User opens order overview

    #When User enters vehicle ready for delivery
    #When Get Order Notifications Feed
    #When Download OrderImports Notification
    #Then User validates order notifications
    #When Switch user to dealer
    #When User opens order





		When Digital delivery is "No"
		When User enters vehicle ready for delivery

		When User delivers vehicle
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates order notifications
		When Download OrderImports Notification
		Then User validates order notifications
		When Get Order Detail By Id
		When User opens order details
		When User gets orderdetails data
		Then User validates orderdetails are correct
    #Then close browser


