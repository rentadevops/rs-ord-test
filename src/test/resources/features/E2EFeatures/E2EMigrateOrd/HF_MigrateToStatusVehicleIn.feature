Feature: Migrate order vehicleIn

[INFO]
As a user i create a new operational lease order

  @OL-migrate-to-status-vehiclein
  Scenario Outline: OL, migrate to status VehicleIn
    When Dummy <dummy>
    When Add system properties
      | system variable | value |
      | appLanguage     | nl    |
    When Add system properties
      | system variable | value |
      | language        | nl    |
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Reset File Number
    When Delete OrderImports Notification
    When Get Last Id previous test
    Given User is logged in
    When Create Migration OrderImports Request and Upload To FSTP
      | leasingType       | transactionCode | orderStatus | dealerDossierNumber | leasingCompanyRSNumber | DealerRSNumber | userEmail                  | trackingNumber | versionNumber | dateFirstRegistration | clientName        | clientContactPerson | clientLanguageCode | clientVAT       | clientStreetHouseNumber | clientPostalCode | clientCity | clientContactEmail | clientContactPhoneNumber | clientContactMobileNumber | clientContactFaxNumber | driverStreetHouseNumber | driverPostalCode | driverCity | driverContactEmail | driverContactPhoneNumber | driverContactMobileNumber | driverContactFaxNumber | driverNameImport | driverLanguageCode | vehicleBrand | vehicleCylinderContent | vehiclePower | vehicleLanguagePapers | vehicleWeight | vehicleTyreDescription | vehicleStockCar | vehicleCo2Emissions | vehicleIdentification | vehicleModel | vehicleVersion           | vehicleExteriorColor    | vehicleInteriorColor           | pricingTotalPriceInput | pricingListPriceInput | pricingDeliveryCostsIni | pricingDeliveryCosts | pricingOtherCosts | pricingDiscountAmount | pricingDiscountPercentage | pricingFleetDiscountAmount | pricingFleetDiscountPercentage | promotionName | promotionDescription | promotionDiscountAmount | deliveryDesiredDate | deliveryLocationName      | deliveryLocationLanguageCode | deliveryContactPerson | deliveryLocationVAT | deliveryStreetHouseNumber | deliveryPostalCode | deliveryCity | deliveryContactEmail    | deliveryContactPhoneNumber | deliveryContactMobileNumber | deliveryContactFaxNumber | insuranceName    | insuranceEmail   | insuranceValuePrice | buybackName | buybackAmount | buybackContractDuration | buybackStreetHouseNumber | buybackPostalCode | buybackCity | registrationDivToDoBy | vin               | vinControlNumber |
      | OPERATIONAL_LEASE | MIGRATE         | VEHICLE_IN  | DOS-DUMMY           | 799230                 | 730084         | rstest2@rentasolutions.org | 1TRACK         | v1.01         | 2021-02-28            | PWC/Deloitte TEST | Test Klantnaam      | fr                 | BE 0414.325.701 | Robert Schumanplein 2-4 | 1040             | ETTERBEEK  | contact@test.begin | 0031765733060            | 0475772277                | Fax12345               | Bliekstraat 999         | 9800             | ASTENE     | test@mail.begin    | 0032476460851            | 0477227722                | Fax1234567             | Jean             | fr                 | BMW          | 1995                   | 110          | fr                    | 1545          | Goodyear               | true            | 114,00              | VehicleCode           | X1 15        | X1 sDrive 18d 110kW Aut. | black sapphire metallic | sensatec canberrabeige/schwarz | 26795,46               | 28801,66              | 70,00                   | 70,00                | 25,00             | 120,00                | 10,00 %                   | 0,00                       | 0,00 %                         | Salonkorting  | korting op het salon | 5142,56                 | 2020-08-15          | Alternatieve leverlocatie | nl                           | Tim De Leverancier    | BE0720879353        | Mechelbaan 2              | 2500               | Lier         | contactlevering@mail.be | 015317531                  | 0477771177                  | Fax12345678              | Foyer Assurances | contact@foyer.lu | 79693,35            | Buyback Tim | 12000,00      | 10                      | Buybackbaan 5            | 2000              | Antwerpen   | CLIENT                | 00000000071100083 |                  |
    When Switch user to dealer
    When User search order
#    When User marks messages as read
#    When User sends comment "The order has been accepted"
#    When User opens order overview
#    When User accepts order
#    When Switch user to leasing_company
#    When User opens order
#    When User opens order messages
#    When User sends comment "The order has been confirmed"
#    When User marks messages as read
#    When User opens order overview
#    When User confirms order
#   When Get Order Notifications Feed
#    When Switch user to dealer
#    When User opens order
#    When User opens order messages
#    When User sends comment "Weet nie?"
#    When User marks messages as read
#    When User opens order overview
#    When User checks vehicle arrived
#    When User enters delivery info
#    When Get Order By DossierNumber
#    When Switch user to leasing_company
#    When User opens order
#    When User opens order messages
#    When User marks messages as read
#    When User opens order overview
#    When User enters vehicle at supplier
#    When Switch user to dealer
#    When User opens order
#    When Digital delivery is "Yes"
#    When User prepares digital delivery
#    When Add system properties
#      | system variable | value   |
#      | platform | ANDROID |
#    When Add system properties
#      | system variable | value |
#      | language | nl |
#    When User goes to mobile website
#    When Get Order By DossierNumber
#    When Add system properties
#      | system variable | value |
#      | pdfLanguage | nl |
#    When Add system properties
#      | system variable | value             |
#      | leasingType | OPERATIONAL_LEASE |
#    When Switch user to dealer
#    When User search order
#    When User opens order documents
#    When User downloads order documents
#    When User opens pdf


    #When Download OrderImports Notification
    #Then User validates order notifications



    #When Get Order Detail By Id
    #When User opens order details
    #When User gets orderdetails data
    #Then User validates orderdetails are correct


    Examples:
      | dummy    |
      | "dummy1" |
    #  | "dummy2" |
    #  | "dummy3" |
    #  | "dummy4" |
    #  | "dummy5" |

     #Then close browser




