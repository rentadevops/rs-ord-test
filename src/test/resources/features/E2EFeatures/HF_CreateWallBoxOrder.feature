Feature: Create new order 21

[INFO]
As a user i create a new wallbox order

	@WallBox-test
	Scenario: Wallbox order
		When Add system properties
			| system variable | value             |
			| leasingType     | WALLBOX |
		When Add system properties
			| system variable | value |
			| appLanguage     | en    |
		When Add system properties
			| system variable | value |
			| language        | en    |
		When Add system properties
			| system variable | value |
			| digitalDelivery        | false    |
		When Add system properties
			| system variable | value |
			| wallbox         | Yes     |
		When Reset File Number
		When Delete OrderImports Notification
		When Get Last Id previous test wallbox
		Given User is logged in
		When Create OrderImports Request and Upload To FSTP
			| leasingType       |
			| OPERATIONAL_LEASE |
		When Get Order By DossierNumber
		When Create WallBox OrderImports and Upload To FSTP
			| leasingType |
			| WALLBOX     |
		When Switch user to dealer
		When Switch user to dealer
		When User search order
		When Get Order Notifications Feed
		When User opens order messages
		When User sends comment "The WallBox order has been accepted"
		When User marks messages as read
#When Download OrderImports Notification
#Then User validates order notifications
		When Get Order Notifications Feed
		When User opens order overview
		When User accepts order
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates wallbox order notifications
		When Switch user to leasing_company
		When User search order
		When User opens order messages
		When User sends comment "The order has been confirmed"
		When User marks messages as read
		When Get Order Notifications Feed
		When User opens order overview
		When User confirms order
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates wallbox order notifications
		When Switch user to dealer
		When User opens order
		When User opens order messages
		When User sends comment "Weet nie?"
		When User marks messages as read
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates wallbox order notifications
		When User opens order overview
		When User enters wallbox install info
		When Switch user to leasing_company
		When User search order
		When User opens order messages
		When User marks messages as read
		When User opens order overview
		When User enters ready to activate
		When Switch user to dealer
		When User opens order
		When User enters ready to activate info
		When Get Order Notifications Feed
		When Download OrderImports Notification
		Then User validates wallbox order notifications
		When User opens order details
		When User gets wallbox orderdetails data
		Then User validates wallbox orderdetails are correct
		#Then close browser


