Feature: Create new order 10

[INFO]
As a user i create a new operational lease order

@LUXPLATEORDERED
Scenario: ol Lux plate ordered
	When Add system properties
		| system variable | value                        |
		| leasingType     | OPERATIONAL_LEASE_LUXEMBOURG |
	When Add system properties
		| system variable | value |
		| appLanguage     | nl    |
	When Add system properties
		| system variable | value |
		| language        | en    |
	When Add system properties
		| system variable | value |
		| digitalDelivery        | false    |
	When Add system properties
		| system variable | value |
		| wallbox         | No     |

	When Reset File Number
When Delete OrderImports Notification
When Get Last Id previous test
Given User is logged in
When Create OrderImports Request and Upload To FSTP
	| leasingType                  | logoReferenceCode |
	| OPERATIONAL_LEASE_LUXEMBOURG |                   |
When Switch user to dealer
When User search order
##When Get Order Notifications Feed
When User accepts order
##When Get Order Notifications Feed
##When Download OrderImports Notification Luxembourg
##Then User validates order notifications
When Switch user to leasing_company
When User opens order
When User confirms order
##When Get Order Notifications Feed
##When Download OrderImports Notification Luxembourg
##Then User validates order notifications
When Switch user to dealer
When User opens order
#When User checks vehicle arrived
#When User enters delivery info
#When Switch user to leasing_company
#When User opens order


#When User enters vehicle at supplier


#When Switch user to dealer
#When User opens order
#When User enters vehicle ready for delivery
#When User delivers vehicle
#When Get Order Detail By Id
#When User opens order details
#When User gets orderdetails data
#Then User validates orderdetails are correct
##When Get Order Notifications Feed
##When Download OrderImports Notification
##Then User validates order notifications
##When Download OrderImports Notification
##Then User validates order notifications
#Then close browser



