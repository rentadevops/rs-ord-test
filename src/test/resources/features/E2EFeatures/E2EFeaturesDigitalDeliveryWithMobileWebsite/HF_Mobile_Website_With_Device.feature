Feature: Create new order 2

[INFO]
As a user i view my order through mobile website

  @MOBILEWITHDEVICE
  Scenario Outline: lambda operational lease
    When Add system properties
      | system variable | value |
      | appLanguage     | nl    |
    When Add system properties
      | system variable | value |
      | language        | nl    |
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Add system properties
      | system variable | value   |
      | platform        | ANDROID |
    When User goes to mobile website with device "<device>" and version "<version>"
    Then close browser

    Examples:
      | device                 | version |
      | Pixel 4                | 10      |
      | Pixel 6                | 12      |
      | Pixel 6 Pro            | 12      |
      | Pixel 3 XL             | 9       |
      | Pixel 3                | 9       |
      | Pixel 7 Pro            | 13      |
      | Pixel 3a               | 11      |
      | Pixel 4                | 11      |
      | Pixel 3a               | 10      |
      | Pixel 4 XL             | 11      |
      | Pixel 6 Pro            | 13      |
      | Pixel 3 XL             | 10      |
      | Pixel 4a               | 11      |
      | Pixel 3                | 11      |
      | Pixel 3                | 10      |
      | Galaxy Note20          | 11      |
      | Galaxy Tab S8          | 12      |
      | Galaxy S20+            | 10      |
      | Galaxy S8              | 7       |
      | Galaxy J7 Prime        | 8       |
      | Moto G7 Play           | 9       |
      | OnePlus Nord           | 11      |
      | Vivo T1 5G             | 12      |
      | Xperia Z5 Dual         | 7.1     |
      | Zenfone 6              | 10      |
      | Galaxy S10             | 11      |
      | Galaxy Note20 Ultra 5G | 11      |
      | Galaxy S21 Ultra 5G    | 11      |
      | Galaxy S21 5G          | 11      |
      | Galaxy S21 5G          | 12      |
      | Galaxy S22             | 12      |
      #| Galaxy S22 Ultra        | 12      |
      | Galaxy S20             | 11      |
      | Galaxy S10             | 9       |
      | Galaxy M12             | 11      |
      | Huawei P30             | 10      |
      | Huawei P30 Pro         | 10      |
      | Huawei Mate 20 Pro     | 9       |
      | Huawei P20 Pro         | 10      |
      | Huawei Mate 20 Pro     | 9       |
      | Vivo Y12               | 11      |
      | Vivo Y20g              | 11      |
      | Vivo Y50               | 10      |
      | Galaxy S10e            | 11      |
      | Galaxy A21s            | 10      |
      | Galaxy S7              | 8       |
      | Galaxy S9+             | 8       |
      | Galaxy A12             | 10      |
      | Galaxy S22 5G          | 12      |
      | Galaxy A8+             | 9       |
      | Galaxy S10             | 10      |
      | Galaxy S8+             | 9       |
      | Galaxy S10+            | 11      |
      | Galaxy S8              | 9       |
      | Galaxy S9+             | 9       |
      | Galaxy Tab S4          | 10      |
      | Galaxy A32             | 11      |
      | Galaxy Tab A8          | 11      |
      | Galaxy S9+             | 10      |
      | Galaxy A70             | 10      |
      | Galaxy A12             | 11      |
      | Galaxy S20 Ultra       | 10      |
      | Galaxy S22 Ultra 5G    | 12      |
      | Galaxy Tab A7 Lite     | 11      |
      | Galaxy M31             | 11      |
      | Galaxy Note10+         | 11      |
      | Galaxy S20             | 10      |
      | Galaxy S20+            | 11      |
      | Galaxy S9              | 8       |
      | Galaxy S9              | 10      |
      | Galaxy Tab S3          | 9       |
      | Galaxy A51             | 11      |
      | Galaxy A22             | 11      |
      | Galaxy Note10 5G       | 12      |
      | Galaxy S10+            | 10      |
      | Galaxy S20+            | 10      |
      | Galaxy Tab S4          | 8.1     |
      | Galaxy A31             | 10      |
      | Galaxy S21+ 5G         | 11      |
      | Galaxy S8+             | 7       |
      | Galaxy Tab S3          | 7       |
      | Galaxy S20 FE          | 11      |
      | Galaxy S8+             | 12      |
      | Galaxy Tab S8+         | 12      |
      | Galaxy S21 FE          | 12      |
      | Galaxy Note10 5G       | 11      |
      | Galaxy A8              | 9       |
      | Galaxy S21 FE 5G       | 12      |
      | Galaxy S10             | 12      |
      | Galaxy S10e            | 9       |
      | Galaxy S20 FE          | 10      |
      | Galaxy Note20          | 12      |
      | Galaxy Note10          | 9       |



