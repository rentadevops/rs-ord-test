Feature: Renta Solutions ORD Create new order
  As a user i create a new operational lease order

  @OL-mobile-web-page
  Scenario Outline: Happy Flow ORD order operational lease
    When Dummy <dummy>
    When Add system properties
      | system variable | value |
      | appLanguage     | fr    |
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Add system properties
      | system variable | value   |
      | platform        | ANDROID |
    #Then close browser
    When User goes to mobile website
    #When Get Order By DossierNumber
    #When Add system properties
    #  | system variable | value |
    #  | pdfLanguage     | en      |
    #When Post Digital Delivery Confirmation
    #When Add system properties
    #  | system variable | value             |
    #  | leasingType     | OPERATIONAL_LEASE |
    #When Switch user to dealer
    #When User search order
    #When User opens order documents
    #When User downloads order documents
    #When User opens pdf
    #When Download OrderImports Notification
    #Then User validates order notifications


    #When User opens order details


    #When User gets orderdetails data
    #Then User validates orderdetails are correct
    Examples:
      | dummy    |
      | "dummy1" |
    #  | "dummy2" |
    #  | "dummy3" |
    #  | "dummy4" |
    #  | "dummy5" |

     #Then close browser




