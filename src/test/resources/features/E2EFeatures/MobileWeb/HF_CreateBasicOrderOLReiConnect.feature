Feature: Renta Solutions ORD Create new order
As a user i create a new operational lease order


@REI
Scenario: Happy Flow ORD order operational lease
	When Add system properties
		| system variable | value             |
		| leasingType     | OPERATIONAL_LEASE |
	When Add system properties
		| system variable | value |
		| appLanguage     | en    |
	When Add system properties
		| system variable | value |
		| language        | en    |
	When Add system properties
		| system variable | value |
		| digitalDelivery        | false    |
	When Add system properties
		| system variable | value |
		| wallbox         | No     |
#	When Reset File Number
#When Delete OrderImports Notification
#When Get Last Id previous test
Given User is logged in
When Create OrderImports Request and Upload To FSTP
	|leasingType				|
	|OPERATIONAL_LEASE	|
	When Switch user to dealer
	When User search order
	When User marks messages as read
	When User opens order messages
	When User sends comment "The order has been accepted"
	When User opens order overview
	When User accepts order
	When Switch user to leasing_company
	When User opens order
	When User opens order messages
	When User sends comment "The order has been confirmed"
	When User marks messages as read
	When User opens order overview
	When User confirms order
	When Switch user to dealer
	When User opens order
	When User opens order messages
	When User sends comment "Weet nie?"
	When User marks messages as read
	When User opens order overview
	When User checks vehicle arrived
	When User enters delivery info
	When Get Order By DossierNumber
	When Switch user to leasing_company
	When User opens order
	When User opens order messages
	When User marks messages as read
	When User opens order overview

	When User enters vehicle at supplier
	When Switch user to dealer
	When User opens order
	When Digital delivery is "No"

	When User enters vehicle ready for delivery
	When Get Order Detail By Id
	When User opens order details
	When User gets orderdetails data
	Then User validates orderdetails are correct
	Then close browser



