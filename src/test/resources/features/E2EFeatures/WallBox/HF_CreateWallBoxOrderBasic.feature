Feature: Create new order 21

[INFO]
As a user i create a new wallbox order

	@WallBox-linkedOrder
	Scenario: Wallbox order
		When Add system properties
			| system variable | value             |
			| leasingType     | WALLBOX |
		When Add system properties
			| system variable | value |
			| appLanguage     | en    |
		When Add system properties
			| system variable | value |
			| language        | en    |
		When Add system properties
			| system variable | value |
			| digitalDelivery        | false    |
		When Add system properties
			| system variable | value |
			| wallbox         | Yes     |
		When Create WallBox OrderImports and Upload To FSTP
			| leasingType |
			| WALLBOX     |
		#Then close browser


