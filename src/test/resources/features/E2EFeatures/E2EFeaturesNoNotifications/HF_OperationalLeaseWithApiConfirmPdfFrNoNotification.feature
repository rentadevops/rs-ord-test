Feature: Create new order 14

[INFO]
As a user i create a new operational lease order

  @OL-no-notification-fr
  Scenario Outline: operational lease french
    When Dummy <dummy>
    When Add system properties
      | system variable | value |
      | appLanguage     | fr    |
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Reset File Number
    When Delete OrderImports Notification
    When Get Last Id previous test
    Given User is logged in
    When Create OrderImports Request and Upload To FSTP
      | leasingType       |
      | OPERATIONAL_LEASE |
    When Switch user to dealer
    When User search order
    When User marks messages as read
    When User sends comment "The order has been accepted"
    #When User opens order overview
    When User accepts order
    When Switch user to leasing_company
    When User opens order
    When User opens order messages
    When User sends comment "The order has been confirmed"
    When User marks messages as read
    When User opens order overview
    When User confirms order
    When Get Order Notifications Feed
    When Switch user to dealer
    When User opens order
    When User sends comment "Weet nie?"
    When User marks messages as read
    When User opens order overview
    When User checks vehicle arrived
    When User enters delivery info
    When Get Order By DossierNumber
    When Switch user to leasing_company
    When User opens order
    When User marks messages as read
    When User opens order overview
    When User enters vehicle at supplier
    When Switch user to dealer
    When User opens order
    When Digital delivery is "Yes"
    When User prepares digital delivery
    When Add system properties
      | system variable | value   |
      | platform        | ANDROID |
    #When User goes to mobile website
    When Get Order By DossierNumber
    When Add system properties
      | system variable | value |
      | pdfLanguage     | fr    |
    When Post Digital Delivery Confirmation
    #Given User is logged in
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Switch user to dealer
    When User search order
    When User opens order documents
    When User downloads order documents
    When User opens pdf
    When Download OrderImports Notification
    Then User validates order notifications
    #When User enters vehicle ready for delivery
    #When Get Order Detail By Id

    #When User opens order details


    #When User gets orderdetails data
    #Then User validates orderdetails are correct
    Examples:
      | dummy    |
      | "dummy1" |
    #  | "dummy2" |
    #  | "dummy3" |
    #  | "dummy4" |
    #  | "dummy5" |

     #Then close browser




