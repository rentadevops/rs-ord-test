Feature: Create new order 11

[INFO]
As a user i create a new wallbox order

@Rental
Scenario: Leasingtype Rental
	When Add system properties
		| system variable | value |
		| appLanguage     | en    |
	When Add system properties
		| system variable | value             |
		| leasingType     | OPERATIONAL_LEASE |
	When Add system properties
		| system variable | value |
		| language        | en    |
	When Add system properties
		| system variable | value |
		| digitalDelivery | true  |
	When Add system properties
		| system variable | value |
		| wallbox         | No     |
	When Reset File Number
When Delete OrderImports Notification
When Get Last Id previous test wallbox
Given User is logged in
When Create OrderImports Request and Upload To FSTP
	| leasingType | logoReferenceCode |
	| OPERATIONAL_LEASE      |                   |
	When Get Order By DossierNumber
	When User marks messages as read
	When User sends comment "The order has been accepted"
	When User opens order overview
	When User accepts order
	When Switch user to leasing_company
	When User opens order
	When User opens order messages
	When User sends comment "The order has been confirmed"
	When User marks messages as read
	When User opens order overview
	When User confirms order
	When Get Order Notifications Feed
	When Switch user to dealer
	When User opens order
	#Then close browser


