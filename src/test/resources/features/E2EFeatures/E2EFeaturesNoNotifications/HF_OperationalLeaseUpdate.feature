@asciidoc
Feature: Create new order 13


[INFO]
The Leasing Company has a mandate for digital delivery
As a user i create a new operational lease order with digital delivery


  @OL-update
  Scenario Outline: OL, with Digital Delivery

    When Dummy <dummy>
      #{Set system variables}
    When Add system properties
        | system variable | value |
        | appLanguage     | en    |
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery        | false    |
    When Add system properties
      | system variable | value |
      | wallbox         | No     |

    When Reset File Number
    When Doc
    """
= *Documentation*
== Collapse

[%collapsible]
====
== *Source code*
[source,java]

include::C:/Users/AdAntonisse/git/RsOrdTest/src/main/java/pageobjects/ftp/FTP_PostOrderImportsNotification.java[class]
====
"""
    When Delete OrderImports Notification

    When Get Last Id previous test

    Given User is logged in
    When Create OrderImports Update Order
        | leasingType       | logoReferenceCode |
        | OPERATIONAL_LEASE |                   |
    When Switch user to dealer
    When User search order
#    When User opens order messages
#    When User marks messages as read
#    When User sends comment "The order has been accepted"
#    When User opens order overview
#    When User accepts order
#    When Switch user to leasing_company
#    When User opens order
#    When User opens order messages
#    When User sends comment "The order has been confirmed"
#    When User marks messages as read
#    When User opens order overview
#    When User confirms order
#    When Get Order Notifications Feed
#    When Switch user to dealer
#    When User opens order
#    When User opens order messages
#    When User sends comment "Weet nie?"
#    When User marks messages as read
#    When User opens order overview
#    When User checks vehicle arrived
#    When User enters delivery info
#    When Get Order By DossierNumber
#    When Switch user to leasing_company
#    When User opens order
#    When User opens order messages
#    When User marks messages as read
#    When User opens order overview
#    When User enters vehicle at supplier
#    When Switch user to dealer
#    When User opens order
#    When Digital delivery is "Yes"
#
#
#
#    When User prepares digital delivery
#    When Add system properties
#      | system variable | value   |
#      | platform        | ANDROID |
#    When User goes to mobile website
#    When Get Order By DossierNumber
#    When Add system properties
#      | system variable | value |
#      | pdfLanguage     | en      |
#    When Post Digital Delivery Confirmation
#    When Add system properties
#      | system variable | value             |
#      | leasingType     | OPERATIONAL_LEASE |
#    When Switch user to dealer
#    When User search order
#    When User opens order documents
#    When User downloads order documents
#    When User opens pdf
#    When Download OrderImports Notification
#    Then User validates order notifications
#
#
#      When Get Order Detail By Id
#      When User opens order details
#
#
#    When User gets orderdetails data
#    Then User validates orderdetails are correct
#    Then close browser
    Examples:
        | dummy    |
        | "dummy1" |
    #  | "dummy2" |
    #  | "dummy3" |
    #  | "dummy4" |
    #  | "dummy5" |

     #Then close browser





