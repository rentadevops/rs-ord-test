@asciidoc
Feature: CEG-2284

[INFO]
As Arval, I want to see a variant of the delivery receipt based on my Sub-Lease

IMPORTANT: The leasing company must be 799120, this is also the directory for upload SFTP

  @CEG-2284-1
  Scenario: OL, no digital delivery
    When Add system properties
      | system variable | value             |
      | leasingType     | OPERATIONAL_LEASE |
    When Add system properties
      | system variable | value |
      | appLanguage     | en    |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery        | false    |
    When Add system properties
      | system variable | value |
      | wallbox         | No     |
    When Reset File Number
    When Delete OrderImports Notification
    When Get Last Id previous test
    Given User is logged in
    When Doc
    """
= *Documentation*
== Collapse

[%collapsible]
====
== *Source code*
[source,java]

include::C:/Users/AdAntonisse/git/RsOrdTest/src/test/java/steps/UI/steps/LoginUiSteps.java[class]
====
"""
    When Create OrderImports Request and Upload To FSTP
        | leasingType       | logoReferenceCode |
        | OPERATIONAL_LEASE |                   |
      When Switch user to dealer
    When User search order
    When User marks messages as read
    #When Get Order Notifications Feed
    When User opens order messages
    When User sends comment "The order has been accepted"
    #When Download OrderImports Notification
    #Then User validates order notifications
    #When Get Order Notifications Feed
    When User opens order overview
    When User accepts order
    #When Get Order Notifications Feed
    #When Download OrderImports Notification
    #Then User validates order notifications
    When Switch user to leasing_company
    When User opens order
    When User opens order messages
    When User sends comment "The order has been confirmed"
    When User marks messages as read
    #When Get Order Notifications Feed
    When User opens order overview
    When User confirms order
    #When Get Order Notifications Feed
    #When Download OrderImports Notification
    #Then User validates order notifications
    When Switch user to dealer
    When User opens order
    When User opens order messages
    When User sends comment "Weet nie?"
    When User marks messages as read
    #When Get Order Notifications Feed
    #When Download OrderImports Notification
    #Then User validates order notifications
    When User opens order overview
    When User checks vehicle arrived
    When User enters delivery info
    When Get Order By DossierNumber
    When Switch user to leasing_company
    When User opens order
    When User opens order messages
    When User marks messages as read
    When User opens order overview
    #When Get Order Notifications Feed
    #When Download OrderImports Notification
    #Then User validates order notifications


    When User enters vehicle at supplier
    When Switch user to dealer

      #When Get Order Notifications Feed
    #When Download OrderImports Notification
    #Then User validates order notifications

    When User opens order
    When Digital delivery is "No"

    When User enters vehicle ready for delivery
    When Get Order Detail By Id
    When User opens order details
    When User gets orderdetails data
    Then User validates orderdetails are correct
    #When Get Order Notifications Feed
    #When Download OrderImports Notification
    #Then User validates order notifications
    #When Download OrderImports Notification
    #Then User validates order notifications
    Then close browser



