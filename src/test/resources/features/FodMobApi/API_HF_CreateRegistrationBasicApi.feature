Feature: Create new registration 1

[INFO]
As a user i create a new registration
1. New, Used No, Planned No, Car, Ruese plate No, Plate format R, Order front plate No

@Registration-basic-api
Scenario: basic registration
  When Add system properties
    | system variable | value   |
    | insuranceType   | planned |
  #Given User is logged in
  When Put Registration
    | leasingCompanyOrderDossierNumber | registrarDossierNumber | formNumber | lesseeName | lesseeEnterpriseNumber | licensePlateHolderName | licensePlateHolderEnterpriseNumber | supplierName      | supplierEnterpriseNumber | insuranceBrokerFsmaNumber | insuranceCompanyFsmaNumber | insuranceReferenceNumber | insuranceBeginDate | insurancePlannedRegistrationDate | driverLastName | driverFirstName | driverAddressStreet | driverAddressHouseNumber | driverAddressBoxNumber | driverAddressPostalCode | driverAddressCity | driverLanguageCode | driverMobileNumber | vehicleBrand | vehicleModel | vehicleVIN        | vehicleVINControlNumber | vehicleCatalogueValue | vehicleCatalogueValueWithoutDiscount | vehicleBiv | vehicleCostCentre | vehicleUsedVehicle | vehiclePriorLicensePlate | vehicleLastRegistrationDate | vehicleRegistrationDocumentsLanguage | vehicleEmailGreenCard | vehicleReuseLicensePlate | vehicleLicensePlate | vehicleLicensePlateFormat | vehicleFrontPlateOrdered | vehicleSpeedPedelecFlag | vehiclePedalingAssistance | deliveryBoxNumber | deliveryCity | deliveryFirstName | deliveryHouseNumber | deliveryPostalCode | deliveryLastNameOrCompanyName | deliveryStreet | deliveryAddressType |
    |                                  | TJULDTU3670366955      | 017402942  |            |                        | Renta Solutions        | BE0425071420                       | rS test account 7 | BE0404494849             | 00039                     | 00079                      | TJULDTU3670366955        | 1666088995000      |                                  |                |                 |                     |                          |                        |                         |                   |                    |                    |              |              | TDGM1VTNSTH216443 | 633                     |                       |                                      |            |                   | false              |                          |                             | DUTCH                                |                       | false                    |                     | RECTANGULAR               | false                    |                         |                           | 28                | LintUpdate   | TestUpdate        | 12                  | 2500               | Renta Solutions BVBA          | Heidestraat    | LC                  |
#  When Add system properties
#    | system variable | value  |
#    | insuranceType   | direct |
#  When Put Registration
#  | leasingCompanyOrderDossierNumber | registrarDossierNumber | formNumber | lesseeName | lesseeEnterpriseNumber | licensePlateHolderName | licensePlateHolderEnterpriseNumber | supplierName      | supplierEnterpriseNumber | insuranceBrokerFsmaNumber | insuranceCompanyFsmaNumber | insuranceReferenceNumber | insuranceBeginDate | insurancePlannedRegistrationDate | driverLastName | driverFirstName | driverAddressStreet | driverAddressHouseNumber | driverAddressBoxNumber | driverAddressPostalCode | driverAddressCity | driverLanguageCode | driverMobileNumber | vehicleBrand | vehicleModel | vehicleVIN        | vehicleVINControlNumber | vehicleCatalogueValue | vehicleCatalogueValueWithoutDiscount | vehicleBiv | vehicleCostCentre | vehicleUsedVehicle | vehiclePriorLicensePlate | vehicleLastRegistrationDate | vehicleRegistrationDocumentsLanguage | vehicleEmailGreenCard | vehicleReuseLicensePlate | vehicleLicensePlate | vehicleLicensePlateFormat | vehicleFrontPlateOrdered | vehicleSpeedPedelecFlag | vehiclePedalingAssistance | deliveryBoxNumber | deliveryCity | deliveryFirstName | deliveryHouseNumber | deliveryPostalCode | deliveryLastNameOrCompanyName | deliveryStreet | deliveryAddressType |
#  |                                  | TJULDTU3670366955      | 867293938  |            |                        | Renta Solutions        | BE0425071420                       | rS test account 7 | BE0404494849             |                           | 00079                      | TJULDTU3670366955        | 1666088995000      | 1666088995000                    |                |                 |                     |                          |                        |                         |                   |                    |                    |              |              | TDGM1DURCJE800671 | 813                     |                       |                                      |            |                   | false              |                          |                             | ENGLISH                              |                       | false                    |                     | RECTANGULAR               | false                    |                         |                           | 28                | LintUpdate   | TestUpdate        | 12                  | 2500               | Renta Solutions BVBA          | Heidestraat    | LC                  |
#  When Get Registration
#  Then User validates put registration
#When Post Registration Complete
#When Get Registration
Then close browser




