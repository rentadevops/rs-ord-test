Feature: CEG-2484

  @CEG-2484 @WIP
  Scenario: Registration used vehicle
    When Add system properties
      | system varable| value                               |
      | leasingType   | OPERATIONAL_LEASE_LUXEMBOURG        |
    When Add system properties
      | system variable | value |
      | appLanguage     | nl    |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery        | false    |
    When Add system properties
      | system variable | value |
      | wallbox         | No     |
    When Reset File Number
    When Delete OrderImports Notification
    When Get Last Id previous test
    Given User is logged in
    When Create OrderImports Request and Upload To FSTP
      | leasingType                  | logoReferenceCode |
      | OPERATIONAL_LEASE_LUXEMBOURG |                   |
    When Switch user to dealer
    When User search order
    When User accepts order
    When Switch user to leasing_company
    When User opens order
    When User confirms order
    When Switch user to dealer
    When User opens order
    When User checks vehicle arrived
    When User enters delivery info
    When Switch user to leasing_company
    When User opens order
    When User enters vehicle at supplier
    When Switch user to dealer
    When User opens order
#    When User enters vehicle ready for delivery
#    When User delivers vehicle
#    When Get Order Detail By Id
#    When User opens order details
#    When User gets orderdetails data
#    Then User validates orderdetails are correct
#    Then close browser
