Feature: MU-1430

  As a wallbox dealer, I can not input more than 42 characters in the "EVB Number" and "Charge card number" in the UI,
  when moving from status "Ordered" to "Installed"

  @WallBox-test @MU-1430 @COMPLETED
  Scenario: Both fields length 42
    When Add system properties
      | system variable | value   |
      | leasingType     | WALLBOX |
    When Add system properties
      | system variable | value |
      | appLanguage     | en    |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery | false |
    When Add system properties
      | system variable | value |
      | wallbox         | Yes   |
    When Reset File Number
    When Delete OrderImports Notification
    When Get Last Id previous test wallbox
    Given User is logged in
    When Create OrderImports Request and Upload To FSTP
      | leasingType       | logoReferenceCode |
      | OPERATIONAL_LEASE |                   |
    When Get Order By DossierNumber
    When Create WallBox OrderImports and Upload To FSTP
      | leasingType |
      | WALLBOX     |
    When Switch user to dealer
    When Switch user to dealer
    When User search order
    When User opens order messages
    When User sends comment "The WallBox order has been accepted"
    When User marks messages as read
    When User opens order overview
    When User accepts order
    When Switch user to leasing_company
    When User search order
    When User opens order messages
    When User sends comment "The order has been confirmed"
    When User marks messages as read
    When User opens order overview
    When User confirms order
    When Switch user to dealer
    When User opens order
    When User opens order messages
    When User sends comment "Weet nie?"
    When User marks messages as read
    When User opens order overview
    When User enters wallbox install info
      | evbNumber | cardNumber |
      |           |            |
    When Switch user to leasing_company
    When User search order
    When User opens order messages
    When User marks messages as read
    When User opens order overview
    When User enters ready to activate
    When Switch user to dealer
    When User opens order
    When User enters ready to activate info
    When User opens order details
    When User gets wallbox orderdetails data
    Then User validates wallbox orderdetails are correct
    # Then close browser

  @WallBox-test @MU-1430 @COMPLETED
  Scenario: Evb field length 43
    When Add system properties
      | system variable | value   |
      | leasingType     | WALLBOX |
    When Add system properties
      | system variable | value |
      | appLanguage     | en    |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery | false |
    When Add system properties
      | system variable | value |
      | wallbox         | Yes   |
    When Reset File Number
    When Delete OrderImports Notification
    When Get Last Id previous test wallbox
    Given User is logged in
    When Create OrderImports Request and Upload To FSTP
      | leasingType       | logoReferenceCode |
      | OPERATIONAL_LEASE |                   |
    When Get Order By DossierNumber
    When Create WallBox OrderImports and Upload To FSTP
      | leasingType |
      | WALLBOX     |
    When Switch user to dealer
    When Switch user to dealer
    When User search order
    When User opens order messages
    When User sends comment "The WallBox order has been accepted"
    When User marks messages as read
    When User opens order overview
    When User accepts order
    When Switch user to leasing_company
    When User search order
    When User opens order messages
    When User sends comment "The order has been confirmed"
    When User marks messages as read
    When User opens order overview
    When User confirms order
    When Switch user to dealer
    When User opens order
    When User opens order messages
    When User sends comment "Weet nie?"
    When User marks messages as read
    When User opens order overview
    When User enters wallbox install info
      | evbNumber                                   | cardNumber |
      | This Number is 001 character longer than 42 |            |
    When Switch user to leasing_company
    When User search order
    When User opens order messages
    When User marks messages as read
    When User opens order overview
    When User enters ready to activate
    When Switch user to dealer
    When User opens order
    When User enters ready to activate info
    When User opens order details
    When User gets wallbox orderdetails data
    Then User validates wallbox orderdetails are correct
    # Then close browser

  @WallBox-test @MU-1430 @COMPLETED
  Scenario: Card field length 43
    When Add system properties
      | system variable | value   |
      | leasingType     | WALLBOX |
    When Add system properties
      | system variable | value |
      | appLanguage     | en    |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery | false |
    When Add system properties
      | system variable | value |
      | wallbox         | Yes   |
    When Reset File Number
    When Delete OrderImports Notification
    When Get Last Id previous test wallbox
    Given User is logged in
    When Create OrderImports Request and Upload To FSTP
      | leasingType       | logoReferenceCode |
      | OPERATIONAL_LEASE |                   |
    When Get Order By DossierNumber
    When Create WallBox OrderImports and Upload To FSTP
      | leasingType |
      | WALLBOX     |
    When Switch user to dealer
    When Switch user to dealer
    When User search order
    When User opens order messages
    When User sends comment "The WallBox order has been accepted"
    When User marks messages as read
    When User opens order overview
    When User accepts order
    When Switch user to leasing_company
    When User search order
    When User opens order messages
    When User sends comment "The order has been confirmed"
    When User marks messages as read
    When User opens order overview
    When User confirms order
    When Switch user to dealer
    When User opens order
    When User opens order messages
    When User sends comment "Weet nie?"
    When User marks messages as read
    When User opens order overview
    When User enters wallbox install info
      | evbNumber | cardNumber                                  |
      |           | This Number is 001 character longer than 42 |
    When Switch user to leasing_company
    When User search order
    When User opens order messages
    When User marks messages as read
    When User opens order overview
    When User enters ready to activate
    When Switch user to dealer
    When User opens order
    When User enters ready to activate info
    When User opens order details
    When User gets wallbox orderdetails data
    Then User validates wallbox orderdetails are correct
    # Then close browser
