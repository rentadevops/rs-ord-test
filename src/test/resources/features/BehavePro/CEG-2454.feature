Feature: 1 order on multiple wallox

    [INFO]
    As a User I create multiple wallbox orders linked to 1 order

    @CEG-2454
    Scenario: Multiple Wallbox orders
        When Add system properties
            | system variable | value             |
            | leasingType     | WALLBOX |
        When Add system properties
            | system variable | value |
            | appLanguage     | en    |
        When Add system properties
            | system variable | value |
            | language        | en    |
        When Add system properties
            | system variable | value |
            | digitalDelivery        | false    |
        When Add system properties
            | system variable | value |
            | wallbox         | Yes     |
        When Reset File Number
        When Delete OrderImports Notification
        When Get Last Id previous test wallbox
        Given User is logged in
        When Create OrderImports Request and Upload To FSTP
            | leasingType       | logoReferenceCode |
            | OPERATIONAL_LEASE |                   |
        When Get Order By DossierNumber
        When Add system properties
            | system variable | value             |
            | leasingType     | WALLBOX |
        When Create WallBox OrderImports and Upload To FSTP
            | leasingType |
            | WALLBOX     |
        When Switch user to dealer
        When User search order
        When Create WallBox OrderImports and Upload To FSTP
            | leasingType |
            | WALLBOX     |
#        When Switch user to dealer
#        When Switch user to dealer
#        When User search order
		#Then close browser


