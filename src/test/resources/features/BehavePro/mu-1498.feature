Feature: MU-1498

  Change mapping of Co2 emmission fields on the registration detail screen

  @MU-1498 @MU-1592 @WIP
  Scenario: Nedc and Wltp have different values
        When Add system properties
        | system variable | value |
        | appLanguage     | en    |
      When Add system properties
        | system variable | value           |
        | leasingType     | FINANCIAL_LEASE |
      When Add system properties
        | system variable | value |
        | language        | en    |
      When Add system properties
        | system variable | value |
        | digitalDelivery | true  |
      When Add system properties
        | system variable | value |
        | wallbox         | No     |
    Given User is logged in
      When User clicks open registrations
    When User clicks on new registration
        | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress     | reuseLicensePlate | speedpedelec | boxNumber | city | firstName | houseNumber | postalCode | street | lastNameOrCompanyName | priorLicensePlate |
        | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 883952145           |                    |                                 | rS test account 7 | BE0404494849             | TDGM1ZMBYKJ298404 | 383                     | false       | false          | false      |             | true            | DISTRIBUTION_CENTER | 1HHH666           |              |           |      |           |             |            |        |                       | 1FFF333           |
    #Then close browser
