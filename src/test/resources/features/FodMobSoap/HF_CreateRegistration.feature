Feature: Create new registration 2

[INFO]
As a user i create a new wallbox order
[red bold Silver-background]*ORD Registration New, Used No, Planned No, Car Yes, Bicycle No, Reuse Plate No, Plate Format R, Order Front Plate No, DeliveryAddress Distribution Center*


@ORD-registrationFSTPScenario1
Scenario: Registration scenario 1
When Reset File Number
When Delete OrderImports Notification
When Get Last Id previous test
Given User is logged in
When Create Registration and Upload To FSTP
  | leasingCompanyOrderDossierNumber | registrarRSNumber | userEmail                  | versionNumber | linkedOrderNumber | comments               | registrarDossierNumber | scheduledRegistrationDate | companyIdentificationVATNumber | companyIdentificationName  | clientName                 | clientVAT    | driverLastName | driverFirstName | driverAddressStreet | driverAddressHouseNumber | driverAddressBoxNumber | driverAddressPostalCode | driverAddressCity | driverAddressCountry | driverLanguage | driverPhoneNumber | sellerName | sellerVATNumber | vehicleCarIdentificationBrand | vehicleCarIdentificationModelDutch | vehicleCarIdentificationModelEnglish | vehicleCarIdentificationVIN | vehicleCarIdentificationVINControlNumber | registrationInformationUsedVehicle | registrationInformationLicensePlatePreviousOwner | vehiregistrationInformationLastRegistrationDatecleVIN | registrationInformationLanguageRegistrationPapers | registrationInformationLicensePlateTakenOver | registrationInformationLicensePlateFormat | registrationInformationEmailGreenCard | registrationInformationFormNumber | insuranceBrokerFsmaNumber | insuranceCompanyFsmaNumber | insuranceReferenceNumber | insuranceBeginDate | insuranceEndDate | insuranceBIV | insuranceInsuredValue | insuranceInsuredValueDiscountExcluded | insuranceCostCenterClient | deliveryCompanyName | deliveryLastName | deliveryFirstName | deliveryRushDelivery | deliveryRequestFrontPlate | deliveryAddressStreet | deliveryAddressHouseNumber | deliveryAddressBoxNumber | deliveryAddressPostalCode | deliveryAddressCity | deliveryAddressCountry |
  |                                  | 799189            | rstest2@rentasolutions.org | 1.0           | PZHHBDA6458428015 | Registration Test-0001 |                        | now                       | BE0503995766                   | Belgium Spirits Company HQ | Universal Music Company NV | BE0657864391 | Cook           | Thomas          | Rue Henri Lambert   | 126                      | 24                     | 9850                    | Hansbeke          |                      | nl             | 015317533         | Red Sox    | BE0893459971    | Mercedes Benz                 | Opperdepop                         | Upperthepup                          | TDGM1JFJFCX782548           | 783                                      | false                              |                                                  | 2022-01-01                                            | nl                                                | false                                        |                                           | tst.01@rentasolutions.org             | 911182223                         | 135                       | 39                         |                          | now                | 2027-05-01       | 9922,00      | 25259,00              | 26599,00                              | COST-0001                 | Delivery Company    |                  |                   | true                 | false                     | Kaulillerweg          | 111                        | WW                       | 3990                      | Peer                |                        |
  When Get Registration
Then close browser


