Feature: Create new registration 18

[INFO]
As a user i create a new broker registration

@CEG-1970-insuranceBrokerFsmaNumberFilled
Scenario: approve with a vatNumber
#When Reset File Number
#When Delete OrderImports Notification
#When Get Last Id previous test
Given User is logged in
  When Put Registration
    | leasingCompanyOrderDossierNumber | registrarDossierNumber | formNumber | lesseeName                 | lesseeEnterpriseNumber | licensePlateHolderName | licensePlateHolderEnterpriseNumber | supplierName  | supplierEnterpriseNumber | insuranceBrokerFsmaNumber | insuranceCompanyFsmaNumber | insuranceReferenceNumber | insuranceBeginDate | insurancePlannedRegistrationDate | driverLastName | driverFirstName | driverAddressStreet | driverAddressHouseNumber | driverAddressBoxNumber | driverAddressPostalCode | driverAddressCity | driverLanguageCode | driverMobileNumber | vehicleBrand  | vehicleModel | vehicleVIN        | vehicleVINControlNumber | vehicleCatalogueValue | vehicleCatalogueValueWithoutDiscount | vehicleBiv | vehicleCostCentre | vehicleUsedVehicle | vehiclePriorLicensePlate | vehicleLastRegistrationDate | vehicleRegistrationDocumentsLanguage | vehicleEmailGreenCard     | vehicleReuseLicensePlate | vehicleLicensePlate | vehicleLicensePlateFormat | vehicleFrontPlateOrdered | vehicleSpeedPedelecFlag | vehiclePedalingAssistance | deliveryBoxNumber | deliveryCity | deliveryFirstName | deliveryHouseNumber | deliveryPostalCode | deliveryLastNameOrCompanyName |  |
    |                                  |                        | 313313313  | Universal Music Company NV |                        |                        | BE0842936235                       | Supplier-0001 |                          | 39                        | 51                         |                          | 1585918077790      |                                  | Cook           | Thomas          | Rue Henri Lambert   | 126                      | 24                     | 9850                    | Hansbeke          | DUTCH              | 0411223344         | Mercedes Benz | Opperdepop   | TDGM1JFJFCX782548 | 783                     | 25259                 | 26599                                | 9922       | COST-0001         | true               | 1PRE666                  | 1608595200000               | DUTCH                                | tst.01@rentasolutions.org | false                    | 1XYZ999             | RECTANGULAR               | false                    |                         |                           | WW                | Peer         |                   | 111                 | 3990               | Delivery Company              |  |
When Get Registration
#Then User validates put registration
When Post Registration Complete
When Get Registration
When Put Broker Registration Approved After Company Registration
When Get Broker Registration
#Then User validates put registration
#Then close browser




