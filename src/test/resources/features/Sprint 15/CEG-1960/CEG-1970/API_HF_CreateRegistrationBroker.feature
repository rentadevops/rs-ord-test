@asciidoc
Feature: Create new registration 16

[INFO]
As a user i create a new [lime bold]#broker registration#

@CEG-1970-formNumberNotNull
Scenario: approve registr valid natId
#When Reset File Number
#When Delete OrderImports Notification
#When Get Last Id previous test
Given User is logged in
When Post Broker Registration
  | leasingCompanyOrderDossierNumber | plannedRegistrationDate | formNumber | deliveryFirstName          | deliveryLastNameOrCompanyName | deliveryStreet | deliveryHouseNumber | deliveryBoxNumber | deliveryPostalCode | deliveryCity | deliveryAddressNotSet | licensePlateHolderName | licensePlateHolderVatNumber | registrationInfoUsedVehicle | registrationInfoPriorLicensePlate | registrationInfoLastRegistrationDate | registrationInfoFrontPlateOrdered | registrationInfoReuseLicensePlate | registrationInfoRegistrationDocumentsLanguage | registrationInfoLicensePlate | registrationInfoLicensePlateFormat | registrationInfoEmailGreenCard | sellerName | sellerVat | vehicleVIN        | vehicleVINControlNumber | insuranceCompanyFsmaNumber | insuranceBrokerFsmaNumber | insuranceReferenceNumber | insuranceBeginDate | registrationManagerUserId |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
  |                                  |                         | 313313313  | Universal Music Company NV | Test Company                  | deliveryStreet | 291                 | WW                | 3998               | Genk         | true                  |                        |                             | false                       | 2PRE666                           | 1608595200000                        | false                             | false                             | DUTCH                                         |                              | RECTANGULAR                        | tst.01@rentasolutions.org      |            |           | TDGM1JFJFCX782548 | 783                     | 39                         | 135                       |                          | 1585918077790      |                           |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
  When Get Broker Registration
  #Then User validates put registration
When Put Broker Registration Approved
When Get Broker Registration
#Then User validates put registration
Then close browser




