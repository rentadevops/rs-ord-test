Feature: Create new registration 17

[INFO]
As a user i create a new broker registration
User approves with a [lime bold silver-background]*national Id* (vatNumber)

@CEG-1970-approveWithNationalId
Scenario: User approves 2
#When Reset File Number
#When Delete OrderImports Notification
#When Get Last Id previous test
  When Add system properties
    | system variable | value |
    | appLanguage     | en    |
  When Add system properties
    | system variable | value           |
    | leasingType     | FINANCIAL_LEASE |
  When Add system properties
    | system variable | value |
    | language        | en    |
  When Add system properties
    | system variable | value |
    | digitalDelivery | true  |
  When Add system properties
    | system variable | value |
    | wallbox         | No     |
Given User is logged in
#  When User clicks on new registration
#    | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin                         | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | reuseLicensePlate | speedPedelec | boxNumber | city     | firstName | houseNumber | postalCode | street           | lastNameOrCompanyName | priorLicensePlate |
#  | Renta Solutions | BE0425071420 |  | TESTDOS10005 |  |  | TESTDOS10005 | 926818766 |  | planned | rS test account 7 | BE0404494849 | TDGM1RSKBVT674009 | 473 | false | false | false |  | true | LC | true | false |  | Borsbeek |  | 54 | 2150 | De Robianostraat | Bpost Borsbeek | 2HHH789 |

  When Post Broker Registration
    | leasingCompanyOrderDossierNumber | plannedRegistrationDate | formNumber | deliveryFirstName          | deliveryLastNameOrCompanyName | deliveryStreet | deliveryHouseNumber | deliveryBoxNumber | deliveryPostalCode | deliveryCity | deliveryAddressNotSet | licensePlateHolderName | licensePlateHolderVatNumber | registrationInfoUsedVehicle | registrationInfoPriorLicensePlate | registrationInfoLastRegistrationDate | registrationInfoFrontPlateOrdered | registrationInfoReuseLicensePlate | registrationInfoRegistrationDocumentsLanguage | registrationInfoLicensePlate | registrationInfoLicensePlateFormat | registrationInfoEmailGreenCard | sellerName | sellerVat | vehicleVIN        | vehicleVINControlNumber | insuranceCompanyFsmaNumber | insuranceBrokerFsmaNumber | insuranceReferenceNumber | insuranceBeginDate | registrationManagerUserId |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
    |                                  |                         | 313313313  | Universal Music Company NV | Test Company                  | deliveryStreet | 291                 | WW                | 3998               | Genk         | true                  |                        |                             | false                       | 2PRE666                           | 1608595200000                        | false                             | false                             | DUTCH                                         |                              | RECTANGULAR                        | tst.01@rentasolutions.org      |            |           | TDGM1JFJFCX782548 | 783                     | 39                         | 135                       |                          | 1585918077790      |                           |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |

When Get Broker Registration
  #Then User validates put registration
When Put Broker Registration Approved
When Get Broker Registration
Then User validates put registration
#Then close browser




