Feature: Create new order 16

[INFO]
As a user i create a new operational lease order


@BASIC
Scenario: Basic operational lease
	When Add system properties
		| system variable | value             |
		| leasingType     | OPERATIONAL_LEASE |
	When Add system properties
		| system variable | value |
		| appLanguage     | en    |
	When Add system properties
		| system variable | value |
		| language        | en    |
	When Add system properties
		| system variable | value |
		| digitalDelivery        | false    |
	When Add system properties
		| system variable | value |
		| wallbox         | No     |
	When Reset File Number
When Delete OrderImports Notification
When Get Last Id previous test
Given User is logged in
When Create OrderImports Request and Upload To FSTP
	|leasingType				|
	|OPERATIONAL_LEASE	|
Then close browser



