Feature: API notification feeds 2

@postDigitalDeliveryConfirmation
Scenario: Digital Delivery Confirmation
  When Add system properties
    | system variable | value             |
    | leasingType     | OPERATIONAL_LEASE |
  When Get Order By DossierNumber
#When Post Digital Delivery Confirmation
  Given User is logged in
  When Switch user to dealer
  When User search order
  When User opens order documents
  When User downloads order documents
  When User opens pdf
#Then close browser