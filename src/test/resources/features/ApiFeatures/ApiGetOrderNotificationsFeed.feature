@asciidoc
Feature: API notification feeds 1

@getOrderNotificationsFeed
Scenario: Get Notification feeds
    When Add system properties
        | system variable | value |
        | appLanguage     | nl    |
    # {NOTE: Steps comments are placed *before* each steps so this comment is for the *WHEN* step.}
    When Add system properties
        | system variable | value             |
        | leasingType     | OPERATIONAL_LEASE |
    When Add system properties
        | system variable | value |
        | language        | en    |
    When Add system properties
        | system variable | value |
        | digitalDelivery        | false    |
    When Add system properties
        | system variable | value |
        | wallbox         | No     |
When Get Order Notifications Feed
    """
= *Documentation*
== *Source code*
[source,java]

include::C:/Users/AdAntonisse/git/RsOrdTest/src/test/java/steps/api/steps/GetOrderNotificationsApiSteps.java[class]
"""
Then close browser           