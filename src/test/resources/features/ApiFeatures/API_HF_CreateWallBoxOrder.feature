Feature: API new order 1

[INFO]
As a user i create a new wallbox order

@api-WallBox-test
Scenario: ORD Wallbox order
	When Add system properties
		| leasingType |
		| WALLBOX           |
	When Add system properties
		| appLanguage |
		| en          |
	When Add system properties
		| language |
		| en       |
	When Add system properties
		| system variable | value |
		| digitalDelivery        | false    |
	When Add system properties
		| wallbox |
		| Yes       |
When Reset File Number
When Delete OrderImports Notification
When Get Last Id previous test
Given User is logged in
When Create OrderImports Request and Upload To FSTP
	| leasingType       |
	| OPERATIONAL_LEASE |
When Get Order By DossierNumber
When Create WallBox OrderImports and Upload To FSTP
	| leasingType |
	| WALLBOX     |
When Get WallBox Order By DossierNumber
When Post WallBox Order Accepted
When Get Order Notifications Feed
When Post WallBox Order Ordered
When Get Order Notifications Feed
When Post WallBox Order Installed
When Get Order Notifications Feed
When Post WallBox Order Ready to activate
When Get Order Notifications Feed
When Post WallBox Order Activated
When Get Order Notifications Feed
#When Download OrderImports Notification
#Then User validates order notifications
#When Switch user to dealer
#When User opens order
#When User delivers vehicle
#When Get Order Notifications Feed
#When Download OrderImports Notification
#Then User validates order notifications
#When Download OrderImports Notification
#Then User validates order notifications
#When Get Order Detail By Id
#When User opens order details
#When User gets orderdetails data
#Then User validates orderdetails are correct
Then close browser


