Feature: Create new registration 11

[INFO]
As a user i create a new registration with Used vehicle and Order FrontPlate
Registration used vehicle and Order FrontPlate

@ORD-registration-UI-usedVehicle-order-frontplate
Scenario: Registration used vehicle
Given User is logged in
  When User clicks open registrations
When User clicks on new registration
  | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | vehicleReusedLicensePlate | speedPedelec | boxNumber | city | firstName | houseNumber | postalCode | street | lastNameOrCompanyName | priorLicensePlate | lastRegistrationDate |
  | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 867293938           |                    |                                 | rS test account 7 | BE0404494849             | TDGM1YVKSYD600077 | 721                     | true        | false          | false      | RECTANGULAR | true            | DC              |                           |              |           |      |           |             |            |        |                       | 2BCU253           |                      |
#Then close browser


