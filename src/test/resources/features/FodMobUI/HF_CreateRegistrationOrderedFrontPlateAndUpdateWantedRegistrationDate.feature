Feature: Create new registration 6

[INFO]
As a user i create a new registration with Ordered Front Plate

  @ORD-registration-UI-orderedFrontPlate-update-wanted-registration-date
  Scenario: Registration Plate Ordered
    When Add system properties
      | system variable | value |
      | appLanguage     | en    |
    When Add system properties
      | system variable | value |
      | language        | en    |
    When Add system properties
      | system variable | value |
      | digitalDelivery        | false    |
    When Add system properties
      | system variable | value |
      | wallbox         | No     |
    When Add system properties
      | system variable | value   |
      | insuranceType   | planned |
    Given User is logged in
    When User clicks open registrations
    When User clicks on new registration
      | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin                         | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | reuseLicensePlate | speedPedelec | boxNumber | city     | firstName | houseNumber | postalCode | street           | lastNameOrCompanyName | priorLicensePlate |
      | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 867293938           |                    | planned                         | rS test account 7 | BE0404494849             | TDGM1LGZAER672865TDGM1LGZAER672865 | 263                     | false       | false          | true       |             | true            | LC              | true              | false        |           | Borsbeek |           | 54          | 2150       | De Robianostraat | Bpost Borsbeek        | 2HHH789           |
    When User updates registrations
#Then close browser


