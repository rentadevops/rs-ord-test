Feature: Create new registration 5

[INFO]
As a user i create a [lime bold Silver-background]#new registration# with status [lime bold Silver-background]##Ordered Front Plate##
[red bold Silver-background]*This test only works with a valid formnumber. You can get a formnumber at url[https://rsautomotive.atlassian.net/wiki/spaces/FMS20/pages/2404614200/FodMob+UAT+Testing^]*


  @ORD-registration-UI-orderedFrontPlate
Scenario: Registration FrontPlate 1
  When Add system properties
    | system variable | value |
    | appLanguage     | en    |
  When Add system properties
    | system variable | value           |
    | leasingType     | FINANCIAL_LEASE |
  When Add system properties
    | system variable | value |
    | language        | en    |
  When Add system properties
    | system variable | value |
    | digitalDelivery | true  |
  When Add system properties
    | system variable | value |
    | wallbox         | No     |
  When Add system properties
    | system variable | value  |
    | insuranceType   | direct |
  Given User is logged in
  When User clicks open registrations
  When User clicks on new registration
    | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | reuseLicensePlate | speedPedelec | boxNumber | city     | firstName | houseNumber | postalCode | street           | lastNameOrCompanyName | priorLicensePlate |
    | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 434783494           |                    | generate                        | rS test account 7 | BE0404494849             | TDGM1CCAAZX542910 | 343                      | true        | false          | true       |             | true            | LC              | 2HHH789           | false        |           | Borsbeek |           | 54          | 2150       | De Robianostraat | Bpost Borsbeek        | 2HHH789           |
#Then close browser


