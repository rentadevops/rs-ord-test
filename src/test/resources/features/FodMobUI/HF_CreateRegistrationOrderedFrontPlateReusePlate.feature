Feature: Create new registration 7

[INFO]
As a user i create a new registration with Ordered Front Plate and reuse License Plate
[red bold Silver-background]*Registration with Ordered Front Plate and reuse License Plate*


@ORD-registration-UI-orderedFrontPlateReuseLicensePlate
Scenario: Registration scenario 2
Given User is logged in
  When User clicks open registrations
When User clicks on new registration
  | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | vehicleReusedLicensePlate |
  | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 867293938           |                    |                                 | rS test account 7 | BE0404494849             | TDGM1DRCEUG347234 | 363                     | false       | false          | true       |             | true            | LC              | 1PRE777                   |
#Then close browser


