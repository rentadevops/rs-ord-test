Feature: Create new registration 3

[INFO]
As a user i create a [lime bold Silver-background]#new registration# with status [lime bold Silver-background]##Delivered##
[red bold Silver-background]*This test only works with a valid formnumber, VIN and VINcontrolnumber. You can get these at url[https://rsautomotive.atlassian.net/wiki/spaces/FMS20/pages/2404614200/FodMob+UAT+Testing^]*


@ORD-registration-UI
@asciidoc
Scenario: Create Basic Registration UI
NOTE: This scenario creates a new registration through the ORD-application.
  When Add system properties
    | system variable | value |
    | appLanguage     | en    |
  When Add system properties
    | system variable | value           |
    | leasingType     | FINANCIAL_LEASE |
  When Add system properties
    | system variable | value |
    | language        | en    |
  When Add system properties
    | system variable | value |
    | digitalDelivery | true  |
  When Add system properties
    | system variable | value |
    | wallbox         | No     |
Given User is logged in
  When User clicks open registrations
When User clicks on new registration
  | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress     | reuseLicensePlate | speedpedelec | boxNumber | city | firstName | houseNumber | postalCode | street | lastNameOrCompanyName | priorLicensePlate |
  | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 434783494           |                    |                                 | rS test account 7 | BE0404494849             | TDGM1CCAAZX542910 | 343                     | false       | false          | false      |             | true            | DISTRIBUTION_CENTER | 1HHH666           |              |           |      |           |             |            |        |                       | 1FFF333           |
#Then close browser


