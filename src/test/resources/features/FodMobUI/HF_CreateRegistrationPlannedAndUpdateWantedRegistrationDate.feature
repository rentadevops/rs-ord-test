Feature: Create new registration 9

[INFO]
As a user i create a new registration

@ORD-registration-UI-planned-and-registered-after-update
Scenario: Registr Planned registered
  When Add system properties
    | system variable | value |
    | appLanguage     | en    |
  When Add system properties
    | system variable | value |
    | language        | en    |
  When Add system properties
    | system variable | value |
    | digitalDelivery        | false    |
  When Add system properties
    | system variable | value |
    | wallbox         | No     |
  When Add system properties
    | system variable | value   |
    | insuranceType   | planned |
  Given User is logged in
  When User clicks open registrations
  When User clicks on new registration
    | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | reuseLicensePlate | speedpedelec | boxNumber | city | firstName | houseNumber | postalCode | street | lastNameOrCompanyName | priorLicensePlate |
    | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 867293938           |                    | planned                         | rS test account 7 | BE0404494849             | TDGM1RNCKHY904025 | 963                     | false       | false          | false      |             | false           | DC              |                   |              |           |      |           |             |            |        |                       |                   |
  When User updates registrations
#Then close browser


