Feature: Create new registration 12

[INFO]
As a user i create a new registration with Used vehicle and Order FrontPlate
Basic Flow ORD Registration with used vehicle and Order FrontPlate

@ORD-registration-UI-usedVehicle-order-frontplate-and-update-wanted-registration-date
Scenario: Registration used vehicle 2
  When Add system properties
    | system variable | value |
    | appLanguage     | en    |
  When Add system properties
    | system variable | value |
    | language        | en    |
  When Add system properties
    | system variable | value |
    | digitalDelivery        | false    |
  When Add system properties
    | system variable | value |
    | wallbox         | No     |
  When Add system properties
    | system variable | value   |
    | insuranceType   | planned |
Given User is logged in
  When User clicks open registrations
When User clicks on new registration
  | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | vehicleReusedLicensePlate | speedPedelec | boxNumber | city | firstName | houseNumber | postalCode | street | lastNameOrCompanyName | priorLicensePlate | lastRegistrationDate |
  | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 867293938           |                    | planned                         | rS test account 7 | BE0404494849             | TDGM1GFUWBC103166 | 671                     | true        | false          | false      | RECTANGULAR | true            | DC              |                           |              |           |      |           |             |            |        |                       | 2BCW693           |                      |
  When User updates registrations
#Then close browser


