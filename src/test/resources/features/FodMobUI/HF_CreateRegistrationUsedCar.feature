Feature: Create new registration 10

[INFO]
As a user i create a new registration with Used vehicle

@ORD-registration-UI-usedVehicle
Scenario: Registration used vehicle
Given User is logged in
  When User clicks open registrations
When User clicks on new registration
  | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | vehicleReusedLicensePlate | speedPedelec | boxNumber | city | firstName | houseNumber | postalCode | street | lastNameOrCompanyName | priorLicensePlate | lastRegistrationDate |
  | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 211303300           |                    |                                 | rS test account 7 | BE0404494849             | TDGM1WKWUZF613780 | 511                     | true        | false          | false      | RECTANGULAR | false           | DC              |                           |              |           |      |           |             |            |        |                       | 1XXX999           |                      |
#Then close browser


