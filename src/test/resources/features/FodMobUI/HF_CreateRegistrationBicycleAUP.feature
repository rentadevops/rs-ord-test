Feature: Create new Registration 4

[INFO]
As a user i create a new [lime bold Silver-background]#Bike Registration#
[red bold Silver-background]*This test only works with a valid formnumber. You can get a formnumber at url[https://rsautomotive.atlassian.net/wiki/spaces/FMS20/pages/2404614200/FodMob+UAT+Testing^]*


@ORD-registration-UI-bicycle-AUP
Scenario: User creates a Registration bicycle AUP
  When Add system properties
    | system variable | value |
    | appLanguage     | en    |
  When Add system properties
    | system variable | value           |
    | leasingType     | FINANCIAL_LEASE |
  When Add system properties
    | system variable | value |
    | language        | en    |
  When Add system properties
    | system variable | value |
    | digitalDelivery | true  |
  When Add system properties
    | system variable | value |
    | wallbox         | No     |
Given User is logged in
  When User clicks open registrations
When User clicks on new registration
  | plateHolderName | plateHolderVatNumber | plateHolderLanguage | plateHolderRegistrationDossierNr | insuranceCompany | insuranceBroker | insurancePolicyNr | insuranceFormNumber | insuranceStartDate | insuranceWantedRegistrationDate | supplierName      | supplierEnterpriseNumber | vehicleVin        | vehicleVinControlNumber | usedVehicle | vehicleBicycle | reusePlate | plateFormat | orderFrontPlate | deliveryAddress | reuseLicensePlate | speedPedelec | boxNumber | city     | firstName | houseNumber | postalCode | street           | lastNameOrCompanyName | priorLicensePlate |
  | Renta Solutions | BE0425071420         |                     | TESTDOS10005                     |                  |                 | TESTDOS10005      | 057493421           |                    |                                 | rS test account 7 | BE0404494849             | TDGL1DURDPW417326 | 723                     | false       | true           | false      | PEDELEC     | false           | LC              |                   | true         |           | Borsbeek |           | 54          | 2150       | De Robianostraat | Bpost Borsbeek        |2MMM789            |
#Then close browsers


