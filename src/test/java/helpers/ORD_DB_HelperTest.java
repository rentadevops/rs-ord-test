package helpers;

import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ORD_DB_HelperTest {

    //@Test
    public void testQueryOrd() throws SQLException {
        String user = System.getenv("ORD_DB_TEST_USER_NAME");
        String password = System.getenv("ORD_DB_TEST_USER_PASSWORD");
        System.out.println("Using db user: " + user);

        ORD_DB_Helper ord_db_helper = new ORD_DB_Helper("jdbc:mysql://172.29.121.160:3306", user, password);

        ord_db_helper.open();
        List<List<Object>> result = ord_db_helper.query("select * from s4t_ord.dealer order by dealer_id LIMIT 10");
        System.out.println(result);
        ord_db_helper.close();

        Assert.assertFalse(result.isEmpty());
    }

    //@Test
    public void testQueryMrt() throws SQLException {
        String user = System.getenv("MRT_DB_TEST_USER_NAME");
        String password = System.getenv("MRT_DB_TEST_USER_PASSWORD");
        System.out.println("Using db user: " + user);

        ORD_DB_Helper ord_db_helper = new ORD_DB_Helper("jdbc:mysql://172.29.121.160:3306", user, password);

        ord_db_helper.open();
        List<List<Object>> result = ord_db_helper.query("select * from s4t_mrt.workorderdto order by workorder_id LIMIT 10");
        System.out.println(result);
        ord_db_helper.close();

        Assert.assertFalse(result.isEmpty());
    }

}