package android.tests;

//import org.junit.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import io.appium.java_client.AppiumBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;


public class ExtentReportsDemo extends BaseTest{

    @Test
    public void extentReportTest() throws MalformedURLException, InterruptedException {

        ExtentReports extent = new ExtentReports();
        ExtentSparkReporter spark = new ExtentSparkReporter(System.getProperty("user.dir") + "/index.html");
        spark.config().setTheme(Theme.DARK);
        spark.config().setReportName("Automation report");
        spark.config().setDocumentTitle("Checking");
        extent.attachReporter(spark);


        ExtentTest test = extent.createTest("My first Test case").assignAuthor("Adje").assignDevice("Huawei P30 Pro").assignCategory("Smoke");
        test.pass("Device connected");
        test.info("App loaded");
        test.info("Views is clicked");

        ExtentTest test1 = extent.createTest("My second Test case")
                .assignAuthor("Adje")
                .assignAuthor("Antonisse")
                .assignDevice("Emulator")
                .assignCategory("Smoke")
                .assignCategory("Regression");
        test1.pass("Device connected 2");
        test1.info("App loaded 2");
        test1.info("Views is clicked 2");

        extent.flush();
    }

}
