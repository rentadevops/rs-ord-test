package android.tests;

//import cucumber.api.java.After;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.screenrecording.CanRecordScreen;
//import io.cucumber.java.After;
import io.cucumber.java.After;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pageobjects.android.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
//import java.util.Base64;


public class BrowserBaseTest extends BaseTest{
    //    public AndroidDriver androidDriver;
    public AndroidDriver driver;
        //public AndroidDriver driver;
        public Process p;
        public ProcessBuilder builder;
        public Welcome welcome;
    public UserInfo userInfo;
    public Vehicle vehicle;
    public ReturnedVehicle returnedVehicle;
    public NewVehicleInfo newVehicleInfo;
    public OptionsList optionsList;
    public Confirm confirm;
    public static RemoteWebDriver connection;
    public static String hubURL = "https://hub.lambdatest.com/wd/hub";


    @BeforeClass
    public void classLevelSetup() {
        logger.info("Tests is starting!");
        //driver = new ChromeDriver();
    }
    //@BeforeClass
        public void ConfigureAppium() throws MalformedURLException, InterruptedException {
        //AndroidDriver driver;
        //Process p = null;
        //ProcessBuilder builder;

        //AppiumDriverLocalService service = new AppiumServiceBuilder()
        //        //.usingDriverExecutable(new File("C:/Program Files/nodejs/node.exe"))
        //        .usingDriverExecutable(new File("C:/Users/AdAntonisse/.appium/node_modules/appium-uiautomator2-driver/build/lib/uiautomator2.js"))
        //        .withAppiumJS(new File("C:/Users/AdAntonisse/AppData/Roaming/npm/node_modules/appium/build/lib/main.js"))
        //        .withIPAddress("127.0.0.1")
        //        .usingPort(4723)
        //        .build();
        //service.start();

        System.out.println("Beforeclass Start appium server");
        /*Runtime rt = Runtime.getRuntime();
        try {
            String[] command = new String[]{"C:/Windows/System32/cmd.exe", "/c", "start cmd.exe", "/c", "appium", "-a", "127.0.0.1", "-p", "4723", "--session-override", "-dc"};
            builder = new ProcessBuilder(command);
            p = builder.start();

            Thread.sleep(10000);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }*/

        //String username = System.getenv("LT_USERNAME") == null ? "a.antonisse" : System.getenv("LT_USERNAME");
        //String accesskey = System.getenv("LT_ACCESS_KEY") == null ? "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0" : System.getenv("LT_ACCESS_KEY");

        //DesiredCapabilities capabilities = new DesiredCapabilities();
        //capabilities.setCapability("deviceName", "Galaxy S20");
        //capabilities.setCapability("platformVersion", "11");
        //capabilities.setCapability("platformName", "Android");
        //capabilities.setCapability("isRealMobile", true);
        //capabilities.setCapability("app", "APP_URL"); //Enter your app url
        //capabilities.setCapability("deviceOrientation", "PORTRAIT");
        //capabilities.setCapability("build", "Java Vanilla - Android");
        //capabilities.setCapability("name", "Sample Test Java");
        //capabilities.setCapability("console", true);
        //capabilities.setCapability("network", false);
        //capabilities.setCapability("visual", true);
        //capabilities.setCapability("devicelog", true);


        //driver = new AppiumDriver(new URL("https://" +username + ":" + accesskey + "@mobile-hub.lambdatest.com/wd/hub"), capabilities);
        //System.out.println(driver);





            welcome = new Welcome(driver);
            userInfo = new UserInfo(driver);
            vehicle = new Vehicle(driver);
            returnedVehicle = new ReturnedVehicle(driver);
        newVehicleInfo = new NewVehicleInfo(driver);
        optionsList = new OptionsList(driver);
        confirm = new Confirm(driver);
    }


    public double getFormattedAmount(String amount) {
        double price = Double.parseDouble( amount.substring(1));
        return price;
    }

    @AfterClass
    public void teardown() {
        logger.info("Tests are ending!");
        //driver.quit();
    }

    @After
    public void tearDown() throws IOException {
        //String video = driver.stopRecordingScreen();
        //byte[] decode = Base64.getDecoder().decode(video);
        //FileUtils.writeByteArrayToFile(new File("appiumRecorded.mp4"), decode);

        //String base64output = androidDriver.stopRecordingScreen();
        //SimpleDateFormat format = new SimpleDateFormat("dd_MM_yy_hh:mm:ss_aa");
        //Date date = new Date();
        //String dateStr = format.format(date);

        //try {
        //    byte[] data = Base64.getDecoder().decode(base64output);
        //    String destinationPath = "./resources/runtime/ScreenRecord_" + dateStr + ".mp4";
        //    Path path = Paths.get(destinationPath);
        //    Files.write(path, data);
        //} catch (IOException ex) {
        //    ex.printStackTrace();
        //}

        //String base64String = ((CanRecordScreen)driver).stopRecordingScreen();
        //byte[] data = Base64.getDecoder().decode(base64String);
        //String
        //        destinationPath="./test/java/resources/runtime/browsertest.mp4";
        //Path path = Paths.get(destinationPath);
        //Files.write(path, data);
        
        //FileUtils.writeByteArrayToFile(new File("./test/java/resources/runtime/browsertest.mp4"), decode);
            if(driver == null) {

            } else {
                //((JavascriptExecutor) driver).executeScript("lambda-status=" + status); //Lambda status will be reflected as either passed/ failed
                driver.quit();
                //p.destroyForcibly();
            }

    }


}
