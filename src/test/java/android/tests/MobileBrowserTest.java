package android.tests;

import android.tests.reports.ExtentReport;
import cucumber.TestContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.connection.ConnectionStateBuilder;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.annotations.Test;
import pageobjects.android.*;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;
import utils.AppiumUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.connection.ConnectionStateBuilder;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class MobileBrowserTest extends BrowserBaseTest{
    /*public AndroidDriver driver;
    //public AndroidDriver driver;
    public Process p;
    public ProcessBuilder builder;
    public Welcome welcome;
    public UserInfo userInfo;
    public Vehicle vehicle;
    public ReturnedVehicle returnedVehicle;
    public NewVehicleInfo newVehicleInfo;
    public OptionsList optionsList;
    public Confirm confirm;
    public static RemoteWebDriver connection;
    public static String hubURL = "https://hub.lambdatest.com/wd/hub";
    BaseTest baseTest;*/

    final Logger logger = LogManager.getLogger(MobileBrowserTest.class);

    AppiumUtils au = new AppiumUtils();
    AppiumDriver driver;
    //AndroidDriver driver;
    TestContext testContext;
    ORD_Order_Page orderPage;
    static String statusCode;
    //WebElement btn_continue = driver.findElement(By.xpath("//button[contains(.,'')]"));
    //WebElement list_birthday = driver.findElement(By.xpath("//input[@formcontrolname='birthDate']"));

    @FindBy(how = How.XPATH, using = "//button[@data-testid='license-plate-next-page-btn']")
    private WebElement btn_license_plate_next;

    @FindBy(how = How.XPATH, using = "//button[contains(text(), 'safety')]")
    private WebElement btn_safe;


    //@Test
    public void browserTest() throws InterruptedException, IOException, ParseException {


        String darCookie = au.postGetCookie();
        logger.debug("darCookie: " + darCookie);
        //Cookie language;

        Date now = new Date(),
                exp = new Date(now.getYear(), now.getMonth(), now.getDate()+1);

        //UiAutomator2Options options = new UiAutomator2Options();
        //options.setAutomationName("uiautomator2");
        //options.setDeviceName("Pixel 2 XL API 31");
        //options.setChromedriverExecutable("C:/UdemyAppium/Drivers/chromedriver.exe");
        //options.setCapability("browserName", "Chrome");
        //options.setCapability("autoAcceptAlerts", "true");
        //options.setPlatformName("Android");



        //AppiumDriver appiumDriver = au.startAppiumServer();
        //androidDriver.rotate(ScreenOrientation.LANDSCAPE);
        //Thread.sleep(2000);
        AppiumUtils au = new AppiumUtils();
        driver = au.startAppiumServerLambdaTest(); //connect through LambdaTest
        //driver = au.startAppiumServer(); //Connect with local Appium Server
        //driver.manage().deleteAllCookies();


/*        Runtime rt = Runtime.getRuntime();
        try {
            String[] command = new String[]{"C:/Windows/System32/cmd.exe", "/c", "start cmd.exe", "/c", "appium", "-a", "127.0.0.1", "-p", "4723", "--session-override", "-dc"};
            builder = new ProcessBuilder(command);
            p = builder.start();

            Thread.sleep(10000);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        UiAutomator2Options options = new UiAutomator2Options()
                .setAutomationName("uiautomator2")
                .setDeviceName("AdjesMob")
                .setChromedriverExecutable("C:/UdemyAppium/Drivers/chromedriver.exe")
                //.setApp("C:/Users/AdAntonisse/AppiumProjects/src/test/resources/ApiDemos-debug.apk");
                .setApp("C:/Users/AdAntonisse/AppiumProjects/src/test/resources/General-Store.apk");
        options.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 300);


        driver = new AndroidDriver(new URL("http://127.0.0.1:4723"), options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));*/


        String pass = "lambda-status=passed";
        String fail = "lambda-status=failed";

        System.out.println("driver: " + driver);
        try {
            driver.get("https://delivery.tst.rentasolutions.org");
            driver.navigate().refresh();
            logger.debug("Title: " + driver.getTitle());
            try {
                btn_safe.click();
            } catch(Exception e) {
                logger.debug("Error safety: " + e.getMessage());
            }
        }

                catch(WebDriverException e) {
                logger.debug("stacktrace" + e.getStackTrace().toString());
                    logger.debug("additional info" + e.getAdditionalInformation());
            }
        logger.debug("get cookies: " + driver.manage().getCookies());

        Cookie dar = new Cookie.Builder("dar-authentication", darCookie)//.sameSite("Lax").build()
                        .domain(".rentasolutions.org")
                        .path("/")
                        .expiresOn(exp)
                        .sameSite("Lax")
                .build();
        logger.debug("Cookie toevoegen");
        //Cookie language = new Cookie.Builder("renta-language", System.getProperty("appLanguage"))//.sameSite("Lax").build()
        Cookie language = new Cookie.Builder("renta-language", "en")//.sameSite("Lax").build()
                .domain(".rentasolutions.org")
                .path("/")
                .expiresOn(exp)
                .sameSite("Lax").build();
        logger.debug("Cookie toevoegen");
        try {
            driver.manage().addCookie(dar);
        } catch (Exception e) {
            logger.debug("stacktrace dar:" + e.getStackTrace().toString());
            logger.debug("message dar: " + e.getMessage());
        }

        try {
            driver.manage().addCookie(language);
        } catch (Exception e) {
            logger.debug("stacktrace language: " + e.getStackTrace().toString());
            logger.debug("message language: " + e.getMessage());
        }
        //driver.manage().addCookie(dar);
        //driver.manage().addCookie(language);



/*        language = new Cookie.Builder("renta-language", System.getProperty("appLanguage"))//.sameSite("Lax").build()
                .domain(".rentasolutions.org")
                .path("/")
                //.expiresOn(xd)
                .expiresOn(exp)
                .sameSite("Lax").build();
        System.out.println("Cookie toevoegen");
        driver.manage().addCookie(language);*/

        driver.manage().getCookies();

        logger.debug("Nogmaals naar site met dar cookie");
        driver.get("https://delivery.tst.rentasolutions.org");
        Thread.sleep(2000);
        try {
            btn_safe.click();
        } catch(Exception e) {
            logger.debug("Error safety: " + e.getMessage());
        }
        //driver.h();
        logger.debug("Cookies : " + driver.manage().getCookies());
        logger.debug("sameSite: " + dar.getSameSite());
        logger.debug("Title: " + driver.getTitle());
        //UserInfo userInfo = new UserInfo(driver);
        welcome = new Welcome(driver);
        try {
            btn_safe.click();
        } catch(Exception e) {
            logger.debug("Error safety: " + e.getMessage());
        }
        logger.debug("Cookies after dar-auth: " + driver.manage().getCookies());
        userInfo = new UserInfo(driver);
        vehicle = new Vehicle(driver);
        returnedVehicle = new ReturnedVehicle(driver);
        newVehicleInfo = new NewVehicleInfo(driver);
        optionsList = new OptionsList(driver);
        confirm = new Confirm(driver);
        driver.get("https://delivery.tst.rentasolutions.org/welcome");
        welcome.selectLanguage();
        driver.get("https://delivery.tst.rentasolutions.org");
        userInfo.input_birthday("1965", "June", "19");
        userInfo.input_email();
        userInfo.select_isdriver();
        userInfo.click_btn_continue();
        vehicle.input_license_plate();
        //vehicle.input_license_plate();
        returnedVehicle.setTogglePreviousVehicleTrue();
        returnedVehicle.setOwner();
        returnedVehicle.input_license_plate();
        newVehicleInfo.validate_new_vehicle();
        optionsList.validate_option_list();
        confirm.validate_confirm();


        driver.quit();


    }


    //@Test
    public void browserTestWithDeviceAndVersion(String device, String version) throws InterruptedException, IOException, ParseException {


        /*String darCookie = au.postGetCookie();
        logger.debug("darCookie: " + darCookie);

        Date now = new Date(),
                exp = new Date(now.getYear(), now.getMonth(), now.getDate()+1);

        AppiumUtils au = new AppiumUtils();
        driver = au.startAppiumServerLambdaTestWithDevice(device, version); //connect through LambdaTest
        //driver = au.startAppiumServer(); //Connect with local Appium Server
        //driver.manage().deleteAllCookies();

        String pass = "lambda-status=passed";
        String fail = "lambda-status=failed";

        //System.out.println("driver: " + driver);
        try {
            driver.get("https://delivery.tst.rentasolutions.org");
            //logger.debug("Title: " + driver.getTitle());
        }

        catch(WebDriverException e) {
            logger.debug("stacktrace" + e.getStackTrace().toString());
            logger.debug("additional info" + e.getAdditionalInformation());
        }
        logger.debug("get cookies: " + driver.manage().getCookies());

        Cookie dar = new Cookie.Builder("dar-authentication", darCookie)//.sameSite("Lax").build()
                .domain(".rentasolutions.org")
                .path("/")
                .expiresOn(exp)
                .sameSite("Lax")
                .build();
        logger.debug("Cookie toevoegen");
        //Cookie language = new Cookie.Builder("renta-language", System.getProperty("appLanguage"))//.sameSite("Lax").build()
        Cookie language = new Cookie.Builder("renta-language", "en")//.sameSite("Lax").build()
                .domain(".rentasolutions.org")
                .path("/")
                .expiresOn(exp)
                .sameSite("Lax").build();
        logger.debug("Cookie toevoegen");
        try {
            driver.manage().addCookie(dar);
        } catch (Exception e) {
            logger.debug("stacktrace dar:" + e.getStackTrace().toString());
            logger.debug("message dar: " + e.getMessage());
        }

        try {
            driver.manage().addCookie(language);
        } catch (Exception e) {
            logger.debug("stacktrace language: " + e.getStackTrace().toString());
            logger.debug("message language: " + e.getMessage());
        }
        Cookie language = new Cookie.Builder("renta-language", System.getProperty("appLanguage"))//.sameSite("Lax").build()
                .domain(".rentasolutions.org")
                .path("/")
                //.expiresOn(xd)
                .expiresOn(exp)
                .sameSite("Lax").build();
        System.out.println("Cookie toevoegen");
        driver.manage().addCookie(language);

        driver.manage().getCookies();

        //logger.debug("Nogmaals naar site met dar cookie");
        driver.get("https://delivery.tst.rentasolutions.org");
        //logger.debug("Cookies : " + driver.manage().getCookies());
        //logger.debug("sameSite: " + dar.getSameSite());
        //logger.debug("Title: " + driver.getTitle());
        //UserInfo userInfo = new UserInfo(driver);
        welcome = new Welcome(driver);
        //logger.debug("Cookies after dar-auth: " + driver.manage().getCookies());
        userInfo = new UserInfo(driver);
        vehicle = new Vehicle(driver);
        returnedVehicle = new ReturnedVehicle(driver);
        newVehicleInfo = new NewVehicleInfo(driver);
        optionsList = new OptionsList(driver);
        confirm = new Confirm(driver);
        driver.get("https://delivery.tst.rentasolutions.org/welcome");
        welcome.selectLanguage();
        driver.get("https://delivery.tst.rentasolutions.org");
        userInfo.input_birthday("1965", "June", "19");
        userInfo.input_email();
        userInfo.select_isdriver();
        userInfo.click_btn_continue();
        vehicle.input_license_plate();
        returnedVehicle.setTogglePreviousVehicleTrue();
        returnedVehicle.setOwner();
        returnedVehicle.input_license_plate();
        newVehicleInfo.validate_new_vehicle();
        optionsList.validate_option_list();
        confirm.validate_confirm();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(pass);
        driver.quit();

    }*/


    }
}
