package steps.api.steps;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

//import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import cucumber.TestContext;
import helpers.ORD_ContractNumberGenerator;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import apiEngine.Endpoints;
import apiEngine.model.post.order.imports.Address;
import apiEngine.model.post.order.imports.Buyback;
import apiEngine.model.post.order.imports.Client;
import apiEngine.model.post.order.imports.Contact;
import apiEngine.model.post.order.imports.Delivery;
import apiEngine.model.post.order.imports.Description;
import apiEngine.model.post.order.imports.Discount;
import apiEngine.model.post.order.imports.Driver;
import apiEngine.model.post.order.imports.ExteriorColor;
import apiEngine.model.post.order.imports.FleetDiscount;
import apiEngine.model.post.order.imports.Header;
import apiEngine.model.post.order.imports.InteriorColor;
import apiEngine.model.post.order.imports.Meta;
import apiEngine.model.post.order.imports.Model;
import apiEngine.model.post.order.imports.Name;
import apiEngine.model.post.order.imports.Option;
import apiEngine.model.post.order.imports.OptionCsv;
import apiEngine.model.post.order.imports.Options;
import apiEngine.model.post.order.imports.Order;
import apiEngine.model.post.order.imports.OrderImports;
import apiEngine.model.post.order.imports.Pricing;
import apiEngine.model.post.order.imports.Promotion;
import apiEngine.model.post.order.imports.Promotions;
import apiEngine.model.post.order.imports.Vehicle;
import apiEngine.model.post.order.imports.Version;
import common.GetCookie;
import common.SftpConnect;
import managers.PageObjectManager;
import pageobjects.api.API_GetOrderByDossierNumber;

public class PostOrderVehicleInApiSteps {
	final Logger logger = LogManager.getLogger(PostOrderVehicleInApiSteps.class);
	
	PageObjectManager pageObjectManager;
	TestContext testContext;
	String orderId;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	
	private static Response response;
	
	public PostOrderVehicleInApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();

	}

	
	@When("Post Order Vehicle In")
	public void post_orderVehicleIn() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, URISyntaxException {
		orderId = getOrderByDossierNumber.getOrderId();
		logger.debug("orderId in PostOrderVehicleIn: " + orderId);
		
		response = Endpoints.postOrderVehicleIn(orderId);
		logger.debug("response:" + response.asPrettyString());
		
	}
}



	

