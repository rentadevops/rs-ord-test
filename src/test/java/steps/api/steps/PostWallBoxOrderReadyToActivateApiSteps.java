package steps.api.steps;


import apiEngine.model.post.wallbox.status.Installed;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.response.Response;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_PostWallBoxOrderInstalled;
import pageobjects.api.API_PostWallBoxOrderReadyToActivate;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class PostWallBoxOrderReadyToActivateApiSteps {
	final Logger logger = LogManager.getLogger(PostWallBoxOrderReadyToActivateApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String id;
	String status;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	API_PostWallBoxOrderInstalled postWallBoxOrderInstalled;
	API_PostWallBoxOrderReadyToActivate postWallBoxOrderReadyToActivate;
	Installed installed;

	private static Response response;

	public PostWallBoxOrderReadyToActivateApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		 postWallBoxOrderInstalled = testContext.getPageObjectManager().postAPI_PostWallBoxOrderInstalled();
		postWallBoxOrderReadyToActivate = testContext.getPageObjectManager().postAPI_PostWallBoxOrderReadyToActivate();

	}

	
	@When("Post WallBox Order Ready to activate")
	public void post_wallbox_order_ready_to_activate() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, jakarta.xml.bind.JAXBException {

		id = System.getProperty("wallBoxOrderId");
		logger.debug("orderId in post_wallbox_order_accepted: " + id);

		postWallBoxOrderReadyToActivate.postWallBoxOrderReadyToActivate(id);

	}
}



	

