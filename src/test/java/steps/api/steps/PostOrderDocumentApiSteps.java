package steps.api.steps;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import javax.xml.bind.JAXBException;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;


import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.response.Response;
import apiEngine.Endpoints;
import managers.PageObjectManager;
import pageobjects.api.API_GetOrderByDossierNumber;

public class PostOrderDocumentApiSteps {
	
	PageObjectManager pageObjectManager;
	TestContext testContext;
	String orderId;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	
	private static Response response;
	
	public PostOrderDocumentApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();

	}

	
	@When("Post Order Document")
	public void post_orderVehicleIn() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException {
		orderId = getOrderByDossierNumber.getOrderId();
		System.out.println("orderId in PostOrderVehicleIn: " + orderId);
		
		response = Endpoints.postOrderDocuments(orderId);
		System.out.println("response:" + response.asPrettyString());
		
	}
}



	

