package steps.api.steps;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
//import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_RegistrationImports;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public class PostRegistrationImportsApiSteps {
	final Logger logger = LogManager.getLogger(PostRegistrationImportsApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_RegistrationImports registrationImports;
	String leasingType;

	public PostRegistrationImportsApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		registrationImports = testContext.getPageObjectManager().postFTP_RegistrationImports();

	}

	@When("Create Registration and Upload To FSTP")
	public void create_registrationImportsAndUploadToFstp(DataTable table) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, jakarta.xml.bind.JAXBException {
		
		List<List<String>> data = table.asLists();
		//leasingType = data.get(1).get(0);

		registrationImports.upload_registrationImports(data);
	}


	
	}
