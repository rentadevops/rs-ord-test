package steps.api.steps;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

//import cucumber.api.java.en.When;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.Endpoints;
import apiEngine.model.get.order.Order;
import apiEngine.model.get.order.notifications.feed.Option;
import apiEngine.model.get.order.notifications.feed.OrderNotificationFeed;
import apiEngine.model.get.order.notifications.feed.OrderNotificationsFeed;
import cucumber.TestContext;
import helpers.AssertHelper;
//import io.cucumber.java.en.When;
import io.restassured.response.Response;
//import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_GetOrderNotifications;
import pageobjects.api.API_PostWallBoxOrderAccepted;
import pageobjects.api.API_PostWallBoxOrderInstalled;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_PostWallBoxImports;
import pageobjects.ui.ORD_Order_Page;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.core.AnyOf.anyOf;

public class GetOrderNotificationsApiSteps extends BaseSteps {
	final Logger logger = LogManager.getLogger(GetOrderNotificationsApiSteps.class);

	ORD_Order_Page orderPage;
	API_GetOrderNotifications getApiOrderNotifications;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_PostWallBoxImports postWallBoxImports;
	API_PostWallBoxOrderAccepted postWallBoxOrderAccepted;
	API_PostWallBoxOrderInstalled postWallBoxOrderInstalled;
	private AssertHelper ah = new AssertHelper();

	public String dosNr;
	public String id;

	public String notificationType;
	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());
	String responseBody;
	String leasingType = "";

	public GetOrderNotificationsApiSteps(TestContext testContext) throws InterruptedException, IOException {
		super(testContext);
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		getApiOrderNotifications = testContext.getPageObjectManager().getAPI_GetOrderNotifications();
		postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		postWallBoxImports = testContext.getPageObjectManager().postFTP_PostWallBoxImports();
		postWallBoxOrderAccepted = testContext.getPageObjectManager().postAPI_PostWallBoxOrderAccepted();
		postWallBoxOrderInstalled = testContext.getPageObjectManager().postAPI_PostWallBoxOrderInstalled();
	}

	String lastId;
	private static Response response;
	private static OrderNotificationFeed orderNotificationFeed;
	private static OrderNotificationsFeed orderNotificationsFeed;

	@When("Get Order Notifications Feed$")
	public void get_order_notifications_feed() throws IOException, InterruptedException, ParseException {
		logger.debug("Start get_order_notifications_feed");
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/feedlastid.properties");
		properties.load(inputDriver);
		lastId = properties.getProperty("LastId");

		// response = Endpoints.getOrderNotifications(lastId);
		long endWaitTime = System.currentTimeMillis() + 300000;
		Boolean success = false;
		while (System.currentTimeMillis() < endWaitTime && !success) {
			response = Endpoints.getOrderNotifications(lastId, leasingType);
			logger.debug("response:" + response.asPrettyString());
			if (response != null) {
				responseBody = response.body().asString();
				if (responseBody.length() < 10) {
					responseBody = "";
				}
				success = true;
			}
		}
				logger.debug("response body: " + responseBody + "body size is " + responseBody.length());
				if (!responseBody.isEmpty()) {
					List<OrderNotificationFeed> orderNotifications = Arrays
							.asList(response.getBody().as(OrderNotificationFeed[].class));
					logger.debug("size: " + orderNotifications.size());
					for(int i = 0; i < orderNotifications.size(); i++) {
						//orderNotificationFeed = getApiOrderNotifications.get_order_notifications_feed(orderNotifications.get(i));
						orderNotificationFeed = orderNotifications.get(i);
						logger.debug("orderNotificationFeed: " + orderNotificationFeed.toString());
						//logger.debug("orderNotificationFeed desiredDleveryDate: " + orderNotificationFeed.getDelivery().getDesiredDeliveryDate());
						validate_dealer_feed();

					}
				}

			logger.debug("response:" + response.asPrettyString());
		logger.debug("End get_order_notifications_feed");
			Thread.sleep(2000);
		}




	public void validate_dealer_feed() throws ParseException {
		notificationType = orderNotificationFeed.getNotificationType();
		logger.debug("response:" + response.asPrettyString());
		switch (notificationType) {
		case "NEW":
			validate_new(orderNotificationFeed);
			//break;
		case "WALL_BOX_NEW":
			validate_new(orderNotificationFeed);
			//break;
		case "NEW_COMMENT":
			validate_new_comment(orderNotificationFeed);
			//break;
		case "ACCEPTED":
			validate_accepted(orderNotificationFeed);
			//break;
		case "ORDERED":
			validate_ordered(orderNotificationFeed);
			//break;
		case "VEHICLE_IN":
			validate_vehicle_in(orderNotificationFeed);
			//break;
		case "LICENSE_PLATE_ORDERED":
			validate_license_plate_ordered(orderNotificationFeed);
			//break;
		case "DELIVERED":
			validate_delivered(orderNotificationFeed);
			//break;
		case "WALL_BOX_ACCEPTED":
			validate_wall_box_accepted(orderNotificationFeed);
			//break;
		case "WALL_BOX_ORDERED":
			validate_wall_box_ordered(orderNotificationFeed);
			//break;
		case "WALL_BOX_INSTALL_INFO":
			validate_wall_box_install_info(orderNotificationFeed);
			//break;
		case "WALL_BOX_INSTALLED":
			validate_wall_box_installed(orderNotificationFeed);
			//break;
		case "WALL_BOX_READY_TO_ACTIVATE":
			validate_wall_box_ready_to_activate(orderNotificationFeed);
			//break;
		case "WALL_BOX_ACTIVATED":
			validate_wall_box_activated(orderNotificationFeed);
			//break;
		default:
			logger.debug("OrderNotificationType " + orderNotificationFeed.getNotificationType() + "Undefined");
		}
	}

	public void validate_new(OrderNotificationFeed orderNotificationFeed) throws ParseException {
		logger.debug("Start validate New");

		logger.debug("orderNotificationFeed New: " + orderNotificationFeed.toString());
		ah = new AssertHelper();

		if (orderNotificationFeed.getLeasingCompany().getNumber().equals("799340")) {
			if (orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Axus Luxembourg S.A."); // bron DB leasing_company
				ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Arval Luxembourg"); // bron DB leasing_company
				ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
				ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "LU12977109"); // bron DB leasing_company
				ah.assertEquals("Client Name ", orderNotificationFeed.getClient().getName(), postOrderImportsRequest.getClientName());
				ah.assertNull("Driver", orderNotificationFeed.getDriver());
				logger.debug("Assertion Result:" + ah.processAllAssertions());


				if (orderNotificationFeed.getPricing().getOptions().size() == postOrderImportsRequest.getOptions().getOptions()
						.size()) {
					for (int i = 0; i < orderNotificationFeed.getPricing().getOptions().size(); i++) {
						//logger.debug("feed option: " + orderNotificationFeed.getPricing().getOptions().get(i)
						//		+ "import option: " + postOrderImportsRequest.getOptions().getOptions().get(i));
						ah.assertEquals("Option descriptions ", orderNotificationFeed.getPricing().getOptions().get(i).getDescription().getDutch(), postOrderImportsRequest.getOptions().getOptions().get(i).getDescription().getDutch());
					}
					//logger.debug("Assertion Result:" + ah.processAllAssertions());
				}
				logger.debug("Assertion Result:" + ah.processAllAssertions());

				boolean equalLists = orderNotificationFeed.getPricing().getOptions().size() == postOrderImportsRequest.getOptions().getOptions().size();
				List<Object> differenceBetweenTwoList = new ArrayList<>(postOrderImportsRequest.getOptions().getOptions());
				differenceBetweenTwoList.removeAll(orderNotificationFeed.getPricing().getOptions());
				logger.debug("Differences Option Lists: " + differenceBetweenTwoList.toString());
				// assertThat(Arrays.deepEquals(orderNotificationFeed.getPricing().getOptions(),
				// postOrderImportsRequest.getOptions().getOptions())).isTrue();
				ah.assertTrue("Option List size ", equalLists);

				ah.assertEquals("Promotions", 0, orderNotificationFeed.getPricing().getPromotions().size());
				logger.debug("TotalPrice validate new voor assert: " + postOrderImportsRequest.getPricingTotalPrice());
				ah.assertEquals("Pricing total price ", orderNotificationFeed.getPricing().getTotalPrice(), postOrderImportsRequest.getPricingTotalPrice());
				logger.debug("TotalPrice validate new NA assert: " + postOrderImportsRequest.getPricingTotalPrice());
				ah.assertEquals("Pricing list price ", orderNotificationFeed.getPricing().getListPrice(), postOrderImportsRequest.getPricingListPrice());
				ah.assertEquals("Delivery desired date ", orderNotificationFeed.getDelivery().getDesiredDeliveryDate(), postOrderImportsRequest.getDeliveryDesiredDate());
				ah.assertEquals("Vehicle brand ", orderNotificationFeed.getVehicle().getBrand(), postOrderImportsRequest.getVehicleBrand());
				ah.assertEquals("Vehicle model ", orderNotificationFeed.getVehicle().getModel().getDutch(), postOrderImportsRequest.getVehicleModel());
				//ah.assertEquals("Vehicle version ", orderNotificationFeed.getVehicle().getVersion().getDutch(), postOrderImportsRequest.getVehicleVersion());
				ah.assertEquals("Vehicle stock ", orderNotificationFeed.getVehicle().getStockVehicle().toString(), postOrderImportsRequest.getVehicleStockCar());
				ah.assertEquals("Vehicle exterior colour ", orderNotificationFeed.getVehicle().getExteriorColour().getDutch(), postOrderImportsRequest.getVehicleExteriorColor());
				ah.assertEquals("Vehicle language papers ", orderNotificationFeed.getVehicle().getLanguagePapers().toUpperCase(), "GERMAN");
				ah.assertEquals("Vehicle tyre description ", orderNotificationFeed.getVehicle().getTyreDescription(), postOrderImportsRequest.getVehicleTyreDescription());
				//ah.assertEquals("Vehicle identification ", orderNotificationFeed.getVehicle().getIdentification(), postOrderImportsRequest.getVehicleIdentification());
				ah.assertEquals("Vehicle interior colour ", orderNotificationFeed.getVehicle().getInteriorColour().getDutch(), postOrderImportsRequest.getVehicleInteriorColor());
				ah.assertEquals("Vehicle engine displacement ", orderNotificationFeed.getVehicle().getEngineDisplacement().toString(), postOrderImportsRequest.getVehicleCylinderContent()); // Voorgelegd aan Raphael
				ah.assertEquals("Vehicle Power ", orderNotificationFeed.getVehicle().getPower().toString(), postOrderImportsRequest.getVehiclePower());
				ah.assertEquals("Vehicle max weight ", orderNotificationFeed.getVehicle().getMaxWeight().toString(), postOrderImportsRequest.getVehicleWeight());
				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
		} else if (notificationType.equals("WALL_BOX_NEW")) {
			if (orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postWallBoxImports.getDosNrWB())) {
				ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
			logger.debug("LC DossierNumber WALL_BOX_NEW in tak WALL_BOX_NEW:" + postWallBoxImports.getDosNrWB());
			ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postWallBoxImports.getDosNrWB());
			ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company
			logger.debug("Assertion Result:" + ah.processAllAssertions());
		} else {
			if (orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
				logger.debug("LC DossierNumber WALL_BOX_NEW in tak ELSE:" + postOrderImportsRequest.getDosNr());
				ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
				ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company
				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}

		}
		ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
		ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
		ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer

		ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
		logger.debug("Assertion Result:" + ah.processAllAssertions());
		// TODO assert orderDate, in the feed its date/time, must convert to date and
		// compare with today
	}

		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE") && !orderNotificationFeed.getLeasingCompany().getNumber().equals("799340")) {
		ah.assertEquals("Client Name ", orderNotificationFeed.getClient().getName(), postOrderImportsRequest.getClientName());
		ah.assertEquals("Client StreetHouseNumber ", orderNotificationFeed.getClient().getAddress().getStreetHouseNumber(), postOrderImportsRequest.getClientStreetHouseNumber());
		ah.assertEquals("Client Postal Code ", orderNotificationFeed.getClient().getAddress().getPostalCode(), postOrderImportsRequest.getClientPostalCode());
		ah.assertEquals("Client City ", orderNotificationFeed.getClient().getAddress().getCity(), postOrderImportsRequest.getClientCity());
		ah.assertEquals("Client Contact Person ", orderNotificationFeed.getClient().getContactPerson(), postOrderImportsRequest.getClientContactPerson());
		ah.assertEquals("Client Contact Email ", orderNotificationFeed.getClient().getContact().getEmail(), postOrderImportsRequest.getClientContactEmail());
			if(postOrderImportsRequest.getClientContactPhoneNumber() != null) {
				ah.assertEquals("Client Contact Phonenumber ", orderNotificationFeed.getClient().getContact().getPhoneNumber().replace("-", ""), postOrderImportsRequest.getClientContactPhoneNumber().replace(" ", "").replace("-", ""));
			} else {
				ah.assertNull("Client Contact Phonenumber ", orderNotificationFeed.getClient().getContact().getPhoneNumber());
			}
		//ah.assertEquals("Client Contact Phonenumber ", orderNotificationFeed.getClient().getContact().getPhoneNumber(), postOrderImportsRequest.getClientContactPhoneNumber().replace(" ", ""));
			if(postOrderImportsRequest.getClientContactMobileNumber() != null) {
				ah.assertEquals("Client Contact Mobilenumber ", orderNotificationFeed.getClient().getContact().getMobileNumber(), postOrderImportsRequest.getClientContactMobileNumber().replace(" ", "").replace("-", "").replace(".", ""));
			} else {
				ah.assertNull("Client Contact Phonenumber ", orderNotificationFeed.getClient().getContact().getMobileNumber());
			}
			//ah.assertEquals("Client Contact Mobilenumber ", orderNotificationFeed.getClient().getContact().getMobileNumber(), postOrderImportsRequest.getClientContactMobileNumber().replace(" ", "").replace("-", "").replace(".", ""));


		if(postOrderImportsRequest.getClientLanguageCode() != null) {
			Langs cl = Langs.valueOf(postOrderImportsRequest.getClientLanguageCode());
			String clc = cl.getLanguagecode();
			ah.assertEquals("Client Language ", orderNotificationFeed.getClient().getLanguageCode(), clc);
		} else {
			ah.assertNull("Client Language ", orderNotificationFeed.getClient().getLanguageCode());
		}
		ah.assertEquals("Client VatNumber ", orderNotificationFeed.getClient().getVatNumber(), postOrderImportsRequest.getClientVAT());

		ah.assertEquals("Driver Name ", orderNotificationFeed.getDriver().getFullName(), postOrderImportsRequest.getDriverNameImport());
		ah.assertEquals("Driver StreetHouseNumber ", orderNotificationFeed.getDriver().getAddress().getStreetHouseNumber(), postOrderImportsRequest.getDriverStreetHouseNumber());
		ah.assertEquals("Driver Postal Code ", orderNotificationFeed.getDriver().getAddress().getPostalCode(), postOrderImportsRequest.getDriverPostalCode());
		ah.assertEquals("Driver City ", orderNotificationFeed.getDriver().getAddress().getCity(), postOrderImportsRequest.getDriverCity());
		ah.assertEquals("Driver Contact Email ", orderNotificationFeed.getDriver().getContact().getEmail(), postOrderImportsRequest.getDriverContactEmail());

			if(postOrderImportsRequest.getDeliveryContactPhoneNumber() != null) {
				ah.assertEquals("Driver Contact Phonenumber ", orderNotificationFeed.getDriver().getContact().getPhoneNumber().replace("-", ""), postOrderImportsRequest.getDriverContactPhoneNumber().replace(" ", "").replace("-", ""));
			} else {
				ah.assertNull("Driver Contact Phonenumber ", orderNotificationFeed.getDriver().getContact().getPhoneNumber());
			}
		//ah.assertEquals("Driver Contact Phonenumber ", orderNotificationFeed.getDriver().getContact().getPhoneNumber(), postOrderImportsRequest.getDriverContactPhoneNumber().replace(" ", ""));
			if(postOrderImportsRequest.getDeliveryContactMobileNumber() != null) {
				ah.assertEquals("Driver Contact Mobileenumber ", orderNotificationFeed.getDriver().getContact().getMobileNumber().replace("-", ""), postOrderImportsRequest.getDriverContactMobileNumber().replace(" ", "").replace("-", "").replace(".", ""));
			} else {
				ah.assertNull("Driver Contact Phonenumber ", orderNotificationFeed.getDriver().getContact().getMobileNumber());
			}
			//ah.assertEquals("Driver Contact Mobileenumber ", orderNotificationFeed.getDriver().getContact().getMobileNumber(), postOrderImportsRequest.getDriverContactMobileNumber().replace(" ", "").replace("-", "").replace(".", ""));
			/*if(postOrderImportsRequest.getDriverLanguageCode() != null) {
				Langs cl = Langs.valueOf(postOrderImportsRequest.getDriverLanguageCode());
				String clc = cl.getLanguagecode();
				ah.assertEquals("Driver Language ", orderNotificationFeed.getDriver().getLanguageCode(), clc);
			} else {
				ah.assertNull("Driver Language ", orderNotificationFeed.getClient().getLanguageCode());
			}*/
		//TODO waarom levert dit een nullpointer op?
		//if(postOrderImportsRequest.getPricingDeliveryCosts() != null) {
		//	ah.assertEquals("Pricing deliverycosts ", orderNotificationFeed.getPricing().getDeliveryCosts().getValue(), postOrderImportsRequest.getPricingDeliveryCosts());
		//}

			logger.debug("Assertion Result:" + ah.processAllAssertions());
		// ah.assertEquals("Pricing Options",
		// orderNotificationFeed.getPricing().getOptions().toString(),
		// postOrderImportsRequest.getOptions().getOptions());
		if (orderNotificationFeed.getPricing().getOptions().size() == postOrderImportsRequest.getOptions().getOptions()
				.size()) {
			for (int i = 0; i < orderNotificationFeed.getPricing().getOptions().size(); i++) {
				logger.debug("feed option: " + orderNotificationFeed.getPricing().getOptions().get(i)
						+ "import option: " + postOrderImportsRequest.getOptions().getOptions().get(i));
				ah.assertEquals("Option descriptions ", orderNotificationFeed.getPricing().getOptions().get(i).getDescription().getDutch(), postOrderImportsRequest.getOptions().getOptions().get(i).getDescription().getDutch());
				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
		}

		boolean equalLists = orderNotificationFeed.getPricing().getOptions().size() == postOrderImportsRequest.getOptions().getOptions().size();
		List<Object> differenceBetweenTwoList = new ArrayList<>(postOrderImportsRequest.getOptions().getOptions());
		differenceBetweenTwoList.removeAll(orderNotificationFeed.getPricing().getOptions());
		logger.debug("Differences Option Lists: " + differenceBetweenTwoList.toString());
		// assertThat(Arrays.deepEquals(orderNotificationFeed.getPricing().getOptions(),
		// postOrderImportsRequest.getOptions().getOptions())).isTrue();
		ah.assertTrue("Option List size ", equalLists);

		ah.assertEquals("Promotions name ", orderNotificationFeed.getPricing().getPromotions().get(0).getName().getDutch(), postOrderImportsRequest.getPromotions().getPromotions().get(0).getName().getDutch());
		ah.assertEquals("Promotions description ", orderNotificationFeed.getPricing().getPromotions().get(0).getDescription().getDutch(), postOrderImportsRequest.getPromotions().getPromotions().get(0).getDescription().getDutch());
		ah.assertEquals("Promotions discount percentage ", orderNotificationFeed.getPricing().getPromotions().get(0).getDiscount().getPercentage(), postOrderImportsRequest.getPromotions().getPromotions().get(0).getDiscount().getPercentage());
		ah.assertEquals("Promotions discount amount ", orderNotificationFeed.getPricing().getPromotions().get(0).getDiscount().getAmount(), postOrderImportsRequest.getPromotions().getPromotions().get(0).getDiscount().getAmount());
		ah.assertEquals("Pricing total price ", orderNotificationFeed.getPricing().getTotalPrice(), postOrderImportsRequest.getPricingTotalPrice());
		ah.assertEquals("Pricing list price ", orderNotificationFeed.getPricing().getListPrice(), postOrderImportsRequest.getPricingListPrice());

		ah.assertEquals("Delivery desired date ", orderNotificationFeed.getDelivery().getDesiredDeliveryDate(), postOrderImportsRequest.getDeliveryDesiredDate());
		ah.assertEquals("Delivery location name ", orderNotificationFeed.getDelivery().getLocationName(), postOrderImportsRequest.getDeliveryLocationName());
		ah.assertEquals("Delivery address line ", orderNotificationFeed.getDelivery().getAddress().getAddressLine(), postOrderImportsRequest.getDeliveryStreetHouseNumber());
		ah.assertEquals("Delivery postal code ", orderNotificationFeed.getDelivery().getAddress().getPostalCode(), postOrderImportsRequest.getDeliveryPostalCode());
		ah.assertEquals("Delivery city ", orderNotificationFeed.getDelivery().getAddress().getCity(), postOrderImportsRequest.getDeliveryCity());
		ah.assertEquals("Delivery contact email ", orderNotificationFeed.getDelivery().getContact().getEmail(), postOrderImportsRequest.getDeliveryContactEmail());
		ah.assertEquals("Delivery contact phonenumber ", orderNotificationFeed.getDelivery().getContact().getPhoneNumber().replace("-", ""), postOrderImportsRequest.getDeliveryContactPhoneNumber().replace("-", "").replace(" ", ""));
		ah.assertEquals("Delivery contact mobile number ", orderNotificationFeed.getDelivery().getContact().getMobileNumber(), postOrderImportsRequest.getDeliveryContactMobileNumber().replace("-", "").replace(" ", "").replace(".", ""));
			if(orderNotificationFeed.getDelivery().getLanguageCode().toUpperCase() != null) {
				//if(!orderNotificationFeed.getDelivery().getLanguageCode().toUpperCase().equals("―")) {
				//	Langs cl = Langs.valueOf(postOrderImportsRequest.getDeliveryLocationLanguageCode().toUpperCase());
				//	String clc = cl.getLanguagecode();
					ah.assertEquals("getDelivery().getLanguageCode().toUpperCase() ", orderNotificationFeed.getDelivery().getLanguageCode().toUpperCase(), "DUTCH");

				//} else {

				//}
							} else {
				ah.assertNull("getDelivery().getLanguageCode().toUpperCase() ", orderNotificationFeed.getClient().getLanguageCode());
			}
		//ah.assertEquals("c ", orderNotificationFeed.getDelivery().getLanguageCode().toUpperCase(), postOrderImportsRequest.getDeliveryLocationLanguageCode().toUpperCase());
		ah.assertEquals("Delivery contact person ", orderNotificationFeed.getDelivery().getContactPerson(), postOrderImportsRequest.getDeliveryContactPerson());
		ah.assertEquals("Delivery vat number ", orderNotificationFeed.getDelivery().getVatNumber(), postOrderImportsRequest.getDeliveryLocationVAT());

		ah.assertEquals("Vehicle brand ", orderNotificationFeed.getVehicle().getBrand(), postOrderImportsRequest.getVehicleBrand());
		ah.assertEquals("Vehicle model ", orderNotificationFeed.getVehicle().getModel().getDutch(), postOrderImportsRequest.getVehicleModel());
		ah.assertEquals("Vehicle version ", orderNotificationFeed.getVehicle().getVersion().getDutch(), postOrderImportsRequest.getVehicleVersion());
		ah.assertEquals("Vehicle stock ", orderNotificationFeed.getVehicle().getStockVehicle().toString(), postOrderImportsRequest.getVehicleStockCar());
		ah.assertEquals("Vehicle exterior colour ", orderNotificationFeed.getVehicle().getExteriorColour().getDutch(), postOrderImportsRequest.getVehicleExteriorColor());
			if(postOrderImportsRequest.getVehicleLanguagePapers().toUpperCase() != null) {
				//Langs cl = Langs.valueOf(postOrderImportsRequest.getVehicleLanguagePapers().toUpperCase());
				//String clc = cl.getLanguagecode();
				ah.assertEquals("Vehicle language papers ", orderNotificationFeed.getVehicle().getLanguagePapers().toUpperCase(), "FRENCH");
			} else {
				ah.assertNull("Vehicle language papers ", orderNotificationFeed.getClient().getLanguageCode());
			}
		ah.assertEquals("Vehicle language papers ", orderNotificationFeed.getVehicle().getLanguagePapers().toUpperCase(), "FRENCH");
		ah.assertEquals("Vehicle tyre description ", orderNotificationFeed.getVehicle().getTyreDescription(), postOrderImportsRequest.getVehicleTyreDescription());
		ah.assertEquals("Vehicle identification ", orderNotificationFeed.getVehicle().getIdentification(), postOrderImportsRequest.getVehicleIdentification());
		ah.assertEquals("Vehicle interior colour ", orderNotificationFeed.getVehicle().getInteriorColour().getDutch(), postOrderImportsRequest.getVehicleInteriorColor());
		ah.assertEquals("Vehicle engine displacement ", orderNotificationFeed.getVehicle().getEngineDisplacement().toString(), postOrderImportsRequest.getVehicleCylinderContent()); // Voorgelegd aan Raphael
		ah.assertEquals("Vehicle Power ", orderNotificationFeed.getVehicle().getPower().toString(), postOrderImportsRequest.getVehiclePower());
		ah.assertEquals("Vehicle max weight ", orderNotificationFeed.getVehicle().getMaxWeight().toString(), postOrderImportsRequest.getVehicleWeight());

		ah.assertEquals("Buyback name ", orderNotificationFeed.getBuyback().getName(), postOrderImportsRequest.getBuybackName());
		ah.assertEquals("Buyback contractduration ", orderNotificationFeed.getBuyback().getContractDuration().toString(), postOrderImportsRequest.getBuybackContractDuration());
		ah.assertEquals("Buyback address streethousenumber ", orderNotificationFeed.getBuyback().getAddress().getStreetHouseNumber(), postOrderImportsRequest.getBuybackStreetHouseNumber());
		ah.assertEquals("Buyback address postalcode ", orderNotificationFeed.getBuyback().getAddress().getPostalCode(), postOrderImportsRequest.getBuybackPostalCode());
		ah.assertEquals("Buyback address city ", orderNotificationFeed.getBuyback().getAddress().getCity(), postOrderImportsRequest.getBuybackCity());

		ah.assertEquals("Leasingtype ", orderNotificationFeed.getLeasingType(), "OPERATIONAL_LEASE"); // Leasingtype wordt afgeleid ?????
		ah.assertEquals("RegistrationDivToDoBy ", orderNotificationFeed.getRegistrationDivToDoBy(), "LC"); // RegistrationDivToDoBy wordt afgeleid ?????
		
		logger.debug("Assertion Result:" + ah.processAllAssertions());
		}
		
		else if(System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			if (orderNotificationFeed.getLeasingType().equals("FINANCIAL_LEASE")) {
				ah.assertNull("Comment ", orderNotificationFeed.getComment());
				ah.assertEquals("Client Name ", orderNotificationFeed.getClient().getName(), postOrderImportsRequest.getClientName());
				//ah.assertNull("Client StreetHouseNumber ", orderNotificationFeed.getClient().getAddress().getStreetHouseNumber());
				//ah.assertNull("Client Postal Code ", orderNotificationFeed.getClient().getAddress().getPostalCode());
				//ah.assertNull("Client City ", orderNotificationFeed.getClient().getAddress().getCity());
				//ah.assertNull("Client Contact Person ", orderNotificationFeed.getClient().getContactPerson());
				//ah.assertNull("Client Contact Email ", orderNotificationFeed.getClient().getContact().getEmail());
				//ah.assertNull("Client Contact Phonenumber ", orderNotificationFeed.getClient().getContact().getPhoneNumber());
				//ah.assertNull("Client Contact Mobilenumber ", orderNotificationFeed.getClient().getContact().getMobileNumber());
				//Langs cl = Langs.valueOf(postOrderImportsRequest.getClientLanguageCode());
				//String clc = cl.getLanguagecode();
				//ah.assertNull("Client Language ", orderNotificationFeed.getClient().getLanguageCode());
				ah.assertEquals("Client VatNumber ", orderNotificationFeed.getClient().getVatNumber(), postOrderImportsRequest.getClientVAT());

				//ah.assertNull("Driver Name ", orderNotificationFeed.getDriver().getFullName());
				//ah.assertNull("Driver StreetHouseNumber ", orderNotificationFeed.getDriver().getAddress().getStreetHouseNumber());
				//ah.assertNull("Driver Postal Code ", orderNotificationFeed.getDriver().getAddress().getPostalCode());
				//ah.assertNull("Driver City ", orderNotificationFeed.getDriver().getAddress().getCity());
				//ah.assertNull("Driver Contact Email ", orderNotificationFeed.getDriver().getContact().getEmail());
				//ah.assertNull("Driver Contact Phonenumber ", orderNotificationFeed.getDriver().getContact().getPhoneNumber());
				//ah.assertNull("Driver Contact Mobileenumber ", orderNotificationFeed.getDriver().getContact().getMobileNumber());
				GetWallBoxOrderNotificationsApiSteps.Langs dl = GetWallBoxOrderNotificationsApiSteps.Langs.valueOf(postOrderImportsRequest.getDriverLanguageCode());
				String dlc = dl.getLanguagecode();
				//ah.assertNull("Driver Language ", orderNotificationFeed.getDriver().getLanguageCode());

				//ah.assertNull("Pricing deliverycosts ", orderNotificationFeed.getPricing().getDeliveryCosts().getValue());

				// ah.assertEquals("Pricing Options",
				// orderNotificationFeed.getPricing().getOptions().toString(),
				// postOrderImportsRequest.getOptions().getOptions());


				//if (orderNotificationFeed.getPricing().getOptions().size() == postOrderImportsRequest.getOptions().getOptions()
				//.size()) {
				//for (int i = 0; i < orderNotificationFeed.getPricing().getOptions().size(); i++) {
				//logger.debug("feed option: " + orderNotificationFeed.getPricing().getOptions().get(i)
				//	+ "import option: " + postOrderImportsRequest.getOptions().getOptions().get(i));
				//ah.assertNull("Option descriptions ", orderNotificationFeed.getPricing().getOptions().get(i).getDescription().getDutch());
				//}
				//}

				//boolean equalLists = orderNotificationFeed.getPricing().getOptions().size() == postOrderImportsRequest.getOptions().getOptions().size();
				//List<Object> differenceBetweenTwoList = new ArrayList<>(postOrderImportsRequest.getOptions().getOptions());
				//differenceBetweenTwoList.removeAll(orderNotificationFeed.getPricing().getOptions());
				//logger.debug("Differences Option Lists: " + differenceBetweenTwoList.toString());
				//ah.assertTrue("Option List size ", equalLists);

				//ah.assertNull("Promotions name ", orderNotificationFeed.getPricing().getPromotions().get(0).getName().getDutch());
				//ah.assertNull("Promotions description ", orderNotificationFeed.getPricing().getPromotions().get(0).getDescription().getDutch());
				//ah.assertNull("Promotions discount percentage ", orderNotificationFeed.getPricing().getPromotions().get(0).getDiscount().getPercentage());
				//ah.assertNull("Promotions discount amount ", orderNotificationFeed.getPricing().getPromotions());

				//ah.assertEquals("Pricing total price ", orderNotificationFeed.getPricing().getTotalPrice(), postOrderImportsRequest.getPricingTotalPrice());
				//ah.assertEquals("Pricing list price ", orderNotificationFeed.getPricing().getListPrice(), postOrderImportsRequest.getPricingListPrice());

				//ah.assertNull("Delivery desired date ", orderNotificationFeed.getDelivery().getDesiredDeliveryDate());
				//ah.assertNull("Delivery location name ", orderNotificationFeed.getDelivery().getLocationName());
				//ah.assertNull("Delivery address line ", orderNotificationFeed.getDelivery().getAddress().getAddressLine());
				//ah.assertNull("Delivery postal code ", orderNotificationFeed.getDelivery().getAddress().getPostalCode());
				//ah.assertNull("Delivery city ", orderNotificationFeed.getDelivery().getAddress().getCity());
				//ah.assertNull("Delivery contact email ", orderNotificationFeed.getDelivery().getContact().getEmail());
				//ah.assertNull("Delivery contact phonenumber ", orderNotificationFeed.getDelivery().getContact().getPhoneNumber());
				//ah.assertNull("Delivery contact mobile number ", orderNotificationFeed.getDelivery().getContact().getMobileNumber());
				//ah.assertNull("Delivery language ", orderNotificationFeed.getDelivery().getLanguageCode().toUpperCase());
				//ah.assertNull("Delivery contact person ", orderNotificationFeed.getDelivery().getContactPerson());
				//ah.assertNull("Delivery vat number ", orderNotificationFeed.getDelivery().getVatNumber());

				//ah.assertNull("Vehicle brand ", orderNotificationFeed.getVehicle().getBrand());
				//ah.assertNull("Vehicle model ", orderNotificationFeed.getVehicle().getModel().getDutch());
				//ah.assertNull("Vehicle version ", orderNotificationFeed.getVehicle().getVersion().getDutch());
				//ah.assertNull("Vehicle stock ", orderNotificationFeed.getVehicle().getStockVehicle().toString());
				//ah.assertNull("Vehicle exterior colour ", orderNotificationFeed.getVehicle().getExteriorColour().getDutch());
				//ah.assertNull("Vehicle language papers ", orderNotificationFeed.getVehicle().getLanguagePapers().toUpperCase());
				//ah.assertNull("Vehicle tyre description ", orderNotificationFeed.getVehicle().getTyreDescription());
				//ah.assertNull("Vehicle identification ", orderNotificationFeed.getVehicle().getIdentification());
				//ah.assertNull("Vehicle interior colour ", orderNotificationFeed.getVehicle().getInteriorColour().getDutch());
				//ah.assertNull("Vehicle engine displacement ", orderNotificationFeed.getVehicle().getEngineDisplacement().toString()); // Voorgelegd aan Raphael
				//ah.assertNull("Vehicle Power ", orderNotificationFeed.getVehicle().getPower().toString());
				//ah.assertNull("Vehicle max weight ", orderNotificationFeed.getVehicle().getMaxWeight().toString());

				//ah.assertNull("Buyback name ", orderNotificationFeed.getBuyback().getName());
				//ah.assertNull("Buyback contractduration ", orderNotificationFeed.getBuyback().getContractDuration().toString());
				//ah.assertNull("Buyback address streethousenumber ", orderNotificationFeed.getBuyback().getAddress().getStreetHouseNumber());
				//ah.assertNull("Buyback address postalcode ", orderNotificationFeed.getBuyback().getAddress().getPostalCode());
				//ah.assertNull("Buyback address city ", orderNotificationFeed.getBuyback().getAddress().getCity());

				ah.assertEquals("Leasingtype ", orderNotificationFeed.getLeasingType(), "FINANCIAL_LEASE"); // Leasingtype wordt afgeleid ?????
				ah.assertEquals("RegistrationDivToDoBy ", orderNotificationFeed.getRegistrationDivToDoBy(), "CLIENT"); // RegistrationDivToDoBy wordt afgeleid ?????

				logger.debug("Assertion Result:" + ah.processAllAssertions());
			} else {
				logger.debug("LeasingType is: " + orderNotificationFeed.getLeasingType());
			}

		}
	}

	public void validate_new_comment(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate New Comment");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("NEW_COMMENT")) {
			if(orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
				//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Renta Solutions"); // bron DB leasing_company
				assertThat(orderNotificationFeed.getLeasingCompany().getName(), isOneOf("Arval Belgium NV/SA", "Renta Solutions", "ALD Automotive"));
				//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Arval Belgium NV/SA"); // bron DB leasing_company
				ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
				ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
				//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0503995766"); // bron DB leasing_company
				assertThat(orderNotificationFeed.getLeasingCompany().getVatNumber(), isOneOf("BE0503995766", "BE0436781102", "BE0842936235"));
				//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0436781102"); // bron DB leasing_company

				ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account Renta 7"); // bron DB dealer
				ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postOrderImportsRequest.getDealerRSNumber());
				ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
				ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE0503995766"); // bron DB dealer

				if (orderNotificationFeed.getOrderStatus().equals("ACCEPTED")) {
					if (orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {


						assertThat(orderNotificationFeed.getSender().getName(), isOneOf("Arval Belgium NV/SA", "Renta Solutions"));
						//ah.assertEquals("Sender Name accepted ", orderNotificationFeed.getSender().getName(), "Arval Belgium NV/SA");
						ah.assertEquals("Sender Number accepted ", orderNotificationFeed.getSender().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
						ah.assertEquals("Sender UserName accepted ", orderNotificationFeed.getSender().getUserName().replace(" ", "").trim().toUpperCase(), postOrderImportsRequest.getUserEmail().substring(0, 7).toUpperCase());
					} else {
						ah.assertEquals("Sender Name ", orderNotificationFeed.getSender().getName(), "Renta Solutions");
						ah.assertEquals("Sender Number ", orderNotificationFeed.getSender().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
						ah.assertEquals("Sender UserName ", orderNotificationFeed.getSender().getUserName().replace(" ", "").trim().toUpperCase(), postOrderImportsRequest.getUserEmail().substring(0, 7).toUpperCase());
					}
					logger.debug("Assertion Result:" + ah.processAllAssertions());
				}
			}
		}
	}

	public void validate_accepted(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Accepted");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("ACCEPTED")) {
			if (orderNotificationFeed.getLeasingCompany().getNumber().equals("799340")) {
				if(orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
					logger.debug("Voor orderstatus: " + containsOrderStatus(orderNotificationFeed.getOrderStatus()));
					ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
					logger.debug("Na orderstatus: " + containsOrderStatus(orderNotificationFeed.getOrderStatus()));
					ah.assertEquals("Expected Deliverydate ", orderNotificationFeed.getExpectedDeliveryDate().substring(0, 10), dateToday);
					logger.debug("Na Expected Deliverydate: " + orderNotificationFeed.getExpectedDeliveryDate().substring(0, 10));
					assertThat(orderNotificationFeed.getLeasingCompany().getName(), anyOf(
							containsString("Arval Belgium NV/SA"),
							containsString("Renta Solutions")));
					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Axus Luxembourg S.A."); // bron DB leasing_company
					ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
					assertThat(orderNotificationFeed.getLeasingCompany().getVatNumber(), anyOf(
							containsString("BE0503995766"),
							containsString("BE0436781102")));
					//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "LU12977109"); // bron DB leasing_company
					logger.debug("Assertion Result:" + ah.processAllAssertions());
				}
			} else {
				ah = new AssertHelper();
				if(orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
					logger.debug("Accepted. else tak:" + orderNotificationFeed.toString());
					ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
					ah.assertEquals("Expected Deliverydate ", orderNotificationFeed.getExpectedDeliveryDate().substring(0, 10), dateToday);
					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Renta Solutions");
					assertThat(orderNotificationFeed.getLeasingCompany().getName(), isOneOf("Arval Belgium NV/SA", "Renta Solutions"));
					//assertThat(orderNotificationFeed.getLeasingCompany().getName(), anyOf(
					//		containsString("Arval Belgium NV/SA"),
					//		containsString("Renta Solutions")));

					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Arval Belgium NV/SA");

					ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
					ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
					//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company
					//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0503995766");

					assertThat(orderNotificationFeed.getLeasingCompany().getVatNumber(), isOneOf("BE0503995766", "BE0436781102"));
					//assertThat(orderNotificationFeed.getLeasingCompany().getVatNumber(), anyOf(
					//		containsString("BE0503995766"),
					//		containsString("BE0436781102")));
					//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0436781102");
					logger.debug("Assertion Result:" + ah.processAllAssertions());
				}
			}
			if(orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				//ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
				ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account Renta 7"); // bulk
				ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postOrderImportsRequest.getDealerRSNumber());
				ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
				//ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer
				ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE0503995766"); // bulk
				logger.debug("Na Supplier VatNumber: " + orderNotificationFeed.getSupplier().getVatNumber());
			}
			logger.debug("Assertion Result:" + ah.processAllAssertions());
		}
	}
	
	public void validate_ordered(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Ordered");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("ORDERED")) {
			ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
			if (orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				if (orderNotificationFeed.getLeasingCompany().getNumber().equals("799340")) {
					assertThat(orderNotificationFeed.getLeasingCompany().getName(), isOneOf("Axus Luxembourg S.A.", "Renta Solutions"));
					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Axus Luxembourg S.A."); // bron DB leasing_company
					ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
					ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "LU12977109"); // bron DB leasing_company

				} else {

					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
					assertThat(orderNotificationFeed.getLeasingCompany().getName(), isOneOf("Arval Belgium NV/SA", "Renta Solutions"));
					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Arval Belgium NV/SA"); // bulk
					ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
					ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
					//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company
					assertThat(orderNotificationFeed.getLeasingCompany().getVatNumber(), isOneOf("BE0436781102", "BE0503995766"));
					//ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0436781102"); //bulk
				}

				//ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
				ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account Renta 7"); // bulk
				ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postOrderImportsRequest.getDealerRSNumber());
				ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
				//ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer
				ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE0503995766"); // bulk

				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
		}
	}
	
	public void validate_vehicle_in(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Vehicle In");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("VEHICLE_IN")) {
			if(orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));

				assertThat(orderNotificationFeed.getLeasingCompany().getName(), isOneOf("Renta Solutions", "Arval Belgium NV/SA"));
				//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Arval Belgium NV/SA"); // bron DB leasing_company
				ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
				ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());
				assertThat(orderNotificationFeed.getLeasingCompany().getVatNumber(), isOneOf("BE0503995766", "BE0436781102"));
				ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0436781102"); // bron DB leasing_company

				ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account Renta 7"); // bron DB dealer
				ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postOrderImportsRequest.getDealerRSNumber());
				ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
				assertThat(orderNotificationFeed.getSupplier().getVatNumber(), isOneOf("BE0503995766", "BE0436781102"));
				ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE0503995766"); // bron DB dealer

				ah.assertEquals("Vehicle Vin ", orderNotificationFeed.getVehicle().getVin().getValue(), orderPage.getVIN());
				ah.assertEquals("Vehicle Vin Controlnumber", orderNotificationFeed.getVehicle().getVinControlNumber(), orderPage.getVINControlNumber());

				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
		}
	}
	
	public void validate_license_plate_ordered(OrderNotificationFeed orderNotificationFeed) throws ParseException {
		logger.debug("Start validate Licenseplate Ordered");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("LICENSE_PLATE_ORDERED")) {
			if(orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
				if (orderNotificationFeed.getLeasingCompany().getNumber().equals("799340")) {
					//ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Axus Luxembourg S.A.");
					ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Arval Luxembourg");
					ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "LU12977109");
				} else {
					ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Arval Belgium NV/SA"); // bron DB leasing_company
					ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0436781102"); // bron DB leasing_company
				}

				ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
				ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());


				ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account Renta 7"); // bron DB dealer
				ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postOrderImportsRequest.getDealerRSNumber());
				ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
				ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE0503995766"); // bron DB dealer

				if (orderNotificationFeed.getLeasingCompany().getNumber().equals("799340")) {
					ah.assertEquals("Vehicle Vin Controlnumber", orderNotificationFeed.getVehicle().getVinControlNumber(), null);
				} else {
					ah.assertEquals("Vehicle Vin Controlnumber", orderNotificationFeed.getVehicle().getVinControlNumber(), orderPage.getVINControlNumber());
				}

				ah.assertEquals("Vehicle Vin ", orderNotificationFeed.getVehicle().getVin().getValue(), orderPage.getVIN());
				ah.assertEquals("Vehicle Licenseplate", orderNotificationFeed.getVehicle().getLicensePlate(), orderPage.getLicensePlate());
				ah.assertEquals("Date First Registration ", orderNotificationFeed.getVehicle().getDateFirstRegistration().substring(0, 10), orderPage.getDateFirstRegistration());
				//ah.assertEquals("Construction Year ", orderNotificationFeed.getVehicle().getConstructionYear(), orderPage.getModelYear());
				ah.assertEquals("CO2Emissions Nedc ", orderNotificationFeed.getVehicle().getCo2EmissionsNedc(), orderPage.getNEDC());
				if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
					ah.assertEquals("CO2Emissions Wltp ", orderNotificationFeed.getVehicle().getCo2EmissionsWltp(), orderPage.getWLTPLux());
				} else {
					ah.assertEquals("CO2Emissions Wltp ", orderNotificationFeed.getVehicle().getCo2EmissionsWltp(), orderPage.getWLTP());
				}


				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
		}
	}
	
	public void validate_delivered(OrderNotificationFeed orderNotificationFeed) throws ParseException {
		logger.debug("Start validate Delivered");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("DELIVERED")) {
			//ah.assertEquals("Comment ", orderNotificationFeed.getComment(), postOrderImportsRequest.getComment());
			if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
				logger.debug("Start validate Delivered Luxembourg");
				if (orderNotificationFeed.getLeasingCompany().getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
					ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "Axus Luxembourg S.A.");
					ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "LU12977109");
					ah.assertNull("Vehicle tyre description ", orderNotificationFeed.getVehicle().getTyreDescription());
					ah.assertEquals("CO2Emissions Wltp ", orderNotificationFeed.getVehicle().getCo2EmissionsWltp(), orderPage.getWLTPLux());
					ah.assertEquals("Vehicle Previous vehicle dropoff ", orderNotificationFeed.getVehicle().getPreviousVehicleDropoff(), Boolean.FALSE);
					ah.assertNull("Vehicle Previous vehicle owner ", orderNotificationFeed.getVehicle().getPreviousVehicleOwner());
					ah.assertNull("Vehicle Previous vehicle licenseplate ", orderNotificationFeed.getVehicle().getPreviousVehicleLicensePlate());
					ah.assertNull("Vehicle Tyre brand ", orderNotificationFeed.getVehicle().getTyreBrand());
					ah.assertNull("Vehicle Tyre type ", orderNotificationFeed.getVehicle().getTyreType());
				} else if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
					logger.debug("Start validate Delivered Financial Lease");
					ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
					ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company
					ah.assertEquals("Vehicle tyre description ", orderNotificationFeed.getVehicle().getTyreDescription(), orderPage.getTyreSpecs());
					ah.assertEquals("CO2Emissions Wltp ", orderNotificationFeed.getVehicle().getCo2EmissionsWltp(), orderPage.getWLTP());

					ah.assertEquals("Vehicle Previous vehicle dropoff ", orderNotificationFeed.getVehicle().getPreviousVehicleDropoff(), false);
					ah.assertNull("Vehicle Previous vehicle owner ", orderNotificationFeed.getVehicle().getPreviousVehicleOwner());
					ah.assertNull("Vehicle Previous vehicle licenseplate ", orderNotificationFeed.getVehicle().getPreviousVehicleLicensePlate());
					ah.assertEquals("Vehicle Tyre brand ", orderNotificationFeed.getVehicle().getTyreBrand(), orderPage.getTyreBrand());
					ah.assertEquals("Vehicle Tyre type ", orderNotificationFeed.getVehicle().getTyreType(), orderPage.getTyreType());

				} else {
					logger.debug("Start validate Delivered else-tak");
					ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
					ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company
					ah.assertEquals("Vehicle tyre description ", orderNotificationFeed.getVehicle().getTyreDescription(), orderPage.getTyreSpecs());
					ah.assertEquals("CO2Emissions Wltp ", orderNotificationFeed.getVehicle().getCo2EmissionsWltp(), orderPage.getWLTP());
					ah.assertEquals("Vehicle Previous vehicle dropoff ", orderNotificationFeed.getVehicle().getPreviousVehicleDropoff(), Boolean.parseBoolean(orderPage.getReturnedVehicle()));
					ah.assertEquals("Vehicle Previous vehicle owner ", orderNotificationFeed.getVehicle().getPreviousVehicleOwner(), orderPage.getOwnerReturnedVehicle());
					ah.assertEquals("Vehicle Previous vehicle licenseplate ", orderNotificationFeed.getVehicle().getPreviousVehicleLicensePlate(), orderPage.getLicensePlateReturnedVehicle());
					ah.assertEquals("Vehicle Tyre brand ", orderNotificationFeed.getVehicle().getTyreBrand(), orderPage.getTyreBrand());
					ah.assertEquals("Vehicle Tyre type ", orderNotificationFeed.getVehicle().getTyreType(), orderPage.getTyreType());
				}

				ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postOrderImportsRequest.getLeasingCompanyRSNumber());
				ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postOrderImportsRequest.getDosNr());


				ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account Renta 7"); // bron DB dealer
				ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postOrderImportsRequest.getDealerRSNumber());
				ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), orderPage.getDealerDossierNr());
				ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE0503995766"); // bron DB dealer

				ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));

				ah.assertEquals("Vehicle Date First Registration ", orderNotificationFeed.getVehicle().getDateFirstRegistration().substring(0, 10), orderPage.getDateFirstRegistration());
				ah.assertEquals("Vehicle Construction Year ", orderNotificationFeed.getVehicle().getConstructionYear(), orderPage.getModelYear());

				ah.assertEquals("CO2Emissions Nedc ", orderNotificationFeed.getVehicle().getCo2EmissionsNedc(), orderPage.getNEDC());

				ah.assertEquals("Vehicle certificate Of Conformity ", orderNotificationFeed.getVehicle().getCertificateOfConformity(), Boolean.parseBoolean(orderPage.getSelectCertificateOfConformity()));
				ah.assertEquals("Vehicle Fuel Card ", orderNotificationFeed.getVehicle().getFuelCard(), Boolean.parseBoolean(orderPage.getSelectFuelCard()));
				ah.assertEquals("Vehicle Legal Kit ", orderNotificationFeed.getVehicle().getLegalKit(), Boolean.parseBoolean(orderPage.getSelectLegalKit()));
				ah.assertEquals("Vehicle Manual ", orderNotificationFeed.getVehicle().getManual(), Boolean.parseBoolean(orderPage.getSelectServiceManual()));
				ah.assertEquals("Vehicle proof Of Insurance ", orderNotificationFeed.getVehicle().getProofOfInsurance(), Boolean.parseBoolean(orderPage.getSelectInsuranceCertificate()));
				ah.assertEquals("Vehicle proof Of Registration ", orderNotificationFeed.getVehicle().getProofOfRegistration(), Boolean.parseBoolean(orderPage.getSelectRegistrationCertificate()));
				ah.assertEquals("Vehicle proof Of Roadworthiness ", orderNotificationFeed.getVehicle().getProofOfRoadworthiness(), Boolean.parseBoolean(orderPage.getSelectTechnicalInspectionCertificate()));
				ah.assertEquals("Vehicle Mileage ", orderNotificationFeed.getVehicle().getMileage().toString(), orderPage.getMileage());
				ah.assertEquals("Vehicle Startcode ", orderNotificationFeed.getVehicle().getStartCode(), orderPage.getStartcode());
				ah.assertEquals("Vehicle Keycode ", orderNotificationFeed.getVehicle().getStartCode(), orderPage.getStartcode());
				ah.assertEquals("Vehicle Number of keys ", orderNotificationFeed.getVehicle().getNumberOfKeys().toString(), orderPage.getNumberOfKeys());
				ah.assertEquals("Vehicle Driver first name ", orderNotificationFeed.getVehicle().getDriverFirstName(), orderPage.getDriverFirstName());
				ah.assertEquals("Vehicle Driver last name ", orderNotificationFeed.getVehicle().getDriverLastName(), orderPage.getDriverLastName());
				ah.assertEquals("Vehicle Driver Birthdate ", orderNotificationFeed.getVehicle().getDriverBirthDate().substring(0, 10), orderPage.getDriverDateOfBirth());
				ah.assertEquals("Vehicle Driver Birthplace ", orderNotificationFeed.getVehicle().getDriverBirthPlace(), orderPage.getDriverBirthPlace());
				if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
					ah.assertNull("Vehicle previousVehicleOwnerLeasingCompanyRsNumber ", orderNotificationFeed.getVehicle().getPreviousVehicleOwnerLeasingCompanyRsNumber());
				} else if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
					ah.assertNull("Vehicle previousVehicleOwnerLeasingCompanyRsNumber ", orderNotificationFeed.getVehicle().getPreviousVehicleOwnerLeasingCompanyRsNumber());
				} else if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
					ah.assertEquals("Vehicle previousVehicleOwnerLeasingCompanyRsNumber ", orderNotificationFeed.getVehicle().getPreviousVehicleOwnerLeasingCompanyRsNumber().toString(), "799188"); //Waar komt dit vandaan?

				}
				ah.assertEquals("Actual Delivery date ", orderNotificationFeed.getActualDeliveryDate().substring(0, 10), dateToday);

				// ah.processAllAssertions();
				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
		}
	}

	public void validate_wall_box_accepted(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Wallbox Accepted");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("WALL_BOX_ACCEPTED")) {
			logger.debug("Voor orderstatus: " + containsOrderStatus(orderNotificationFeed.getOrderStatus()));
			ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
			ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
			ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postWallBoxImports.getLeasingCompanyRSNumber());
			ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postWallBoxImports.getDosNrWB());
			ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company

			ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
			ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postWallBoxImports.getDealerRSNumber());
			ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
			ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer
			ah.assertEquals("expectedInstallationDate ", orderNotificationFeed.getExpectedInstallationDate(), postWallBoxOrderAccepted.getExpectedInstallationDate());

			logger.debug("Assertion Result:" + ah.processAllAssertions());
		}
	}

	public void validate_wall_box_ordered(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Wallbox Ordered");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("WALL_BOX_ORDERED")) {
			ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
			ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
			ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postWallBoxImports.getLeasingCompanyRSNumber());
			ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postWallBoxImports.getDosNrWB());
			ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company

			ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
			ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postWallBoxImports.getDealerRSNumber());
			ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
			ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer

			logger.debug("Assertion Result:" + ah.processAllAssertions());
		}
	}

	public void validate_wall_box_install_info(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Wallbox Install Info");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("WALL_BOX_INSTALL_INFO")) {
			logger.debug("orderNotificationFeed Install_info: " + orderNotificationFeed.toString());

			ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
			ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
			ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postWallBoxImports.getLeasingCompanyRSNumber());
			ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postWallBoxImports.getDosNrWB());
			ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company

			ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
			ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postWallBoxImports.getDealerRSNumber());
			ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
			ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer

			if (orderNotificationFeed.getNotificationType().equals("WALL_BOX_INSTALL_INFO")) {
				logger.debug("orderNotificationFeed.getEvbNumber:" + orderNotificationFeed.getEvbNumber());
				logger.debug("postWallBoxOrderInstalled.getEvbNumber:" + postWallBoxOrderInstalled.getEvbNumber());
				ah.assertEquals("EvbNumber ", orderNotificationFeed.getEvbNumber(), postWallBoxOrderInstalled.getEvbNumber());
				ah.assertEquals("ChargeCardNumber ", orderNotificationFeed.getChargeCardNumber(), postWallBoxOrderInstalled.getChargeCardNumber());
				ah.assertTrue("SentCard ", orderNotificationFeed.getSentCard());
				ah.assertEquals("InstallationDate ", orderNotificationFeed.getInstallationDate(), postWallBoxOrderInstalled.getInstallationDate());
				ah.assertEquals("CertificationDate ", orderNotificationFeed.getCertificationDate(), postWallBoxOrderInstalled.getCertificationDate());
				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
			logger.debug("Assertion Result:" + ah.processAllAssertions());
		}
	}

	public void validate_wall_box_installed(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Wallbox Installed");
		ah = new AssertHelper();
		if(orderNotificationFeed.getNotificationType().equals("WALL_BOX_INSTALLED")) {
			ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
			ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
			ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postWallBoxImports.getLeasingCompanyRSNumber());
			ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postWallBoxImports.getDosNrWB());
			ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company

			ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
			ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postWallBoxImports.getDealerRSNumber());
			ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
			ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer

			if (orderNotificationFeed.getNotificationType().equals("WALL_BOX_INSTALLLED")) {
				ah.assertEquals("EvbNumber ", orderNotificationFeed.getEvbNumber(), postWallBoxOrderInstalled.getEvbNumber());
				ah.assertEquals("ChargeCardNumber ", orderNotificationFeed.getChargeCardNumber(), postWallBoxOrderInstalled.getChargeCardNumber());
				ah.assertEquals("SentCard ", orderNotificationFeed.getSentCard(), postWallBoxOrderInstalled.getCardSent());
				ah.assertEquals("InstallationDate ", orderNotificationFeed.getInstallationDate(), postWallBoxOrderInstalled.getInstallationDate());
				ah.assertEquals("CertificationDate ", orderNotificationFeed.getCertificationDate(), postWallBoxOrderInstalled.getCertificationDate());
				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}
			logger.debug("Assertion Result:" + ah.processAllAssertions());
		}
	}

	public void validate_wall_box_ready_to_activate(OrderNotificationFeed orderNotificationFeed) {
		logger.debug("Start validate Wallbox Ready to Activate");
		ah = new AssertHelper();
		if (orderNotificationFeed.getNotificationType().equals("WALL_BOX_READY_TO_ACTIVATE")) {
			ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
			ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
			ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postWallBoxImports.getLeasingCompanyRSNumber());
			ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postWallBoxImports.getDosNrWB());
			ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company

			ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
			ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postWallBoxImports.getDealerRSNumber());
			ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
			ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer

			logger.debug("Assertion Result:" + ah.processAllAssertions());
		}
	}
		public void validate_wall_box_activated(OrderNotificationFeed orderNotificationFeed){
			logger.debug("Start validate Wallbox Activated");
			ah = new AssertHelper();
			if (orderNotificationFeed.getNotificationType().equals("WALL_BOX_ACTIVATED")) {
				logger.debug("Ik zit in methode wall box activated");
				ah.assertTrue("OrderStatus is valid ", containsOrderStatus(orderNotificationFeed.getOrderStatus()));
				ah.assertEquals("LC Name ", orderNotificationFeed.getLeasingCompany().getName(), "ALD Automotive"); // bron DB leasing_company
				ah.assertEquals("LC Number ", orderNotificationFeed.getLeasingCompany().getNumber(), postWallBoxImports.getLeasingCompanyRSNumber());
				ah.assertEquals("LC DossierNumber ", orderNotificationFeed.getLeasingCompany().getDossierNumber(), postWallBoxImports.getDosNrWB());
				ah.assertEquals("LC VatNumber ", orderNotificationFeed.getLeasingCompany().getVatNumber(), "BE0842936235"); // bron DB leasing_company

				ah.assertEquals("Supplier Name ", orderNotificationFeed.getSupplier().getName(), "TEST account RENTA 9"); // bron DB dealer
				ah.assertEquals("Supplier Number ", orderNotificationFeed.getSupplier().getNumber(), postWallBoxImports.getDealerRSNumber());
				ah.assertEquals("Supplier DossierNumber ", orderNotificationFeed.getSupplier().getDossierNumber(), null);
				ah.assertEquals("Supplier VatNumber ", orderNotificationFeed.getSupplier().getVatNumber(), "BE67876646"); // bron DB dealer

				logger.debug("Assertion Result:" + ah.processAllAssertions());
			}

	}

	public enum Langs {
		DUTCH("NL"), ENGLISH("EN"), FRENCH("FR"), DEUTSCH("DE");

		private String languagecode;

		Langs(String languagecode) {
			this.languagecode = languagecode;
		}

		public String getLanguagecode() {
			return languagecode;
		}
	}

	public enum OrderStatus {
		NEW, ACCEPTED, ORDERED, VEHICLE_IN, LICENSE_PLATE_ORDERED, DELIVERED, INSTALLED, READY_TO_ACTIVATE, ACTIVATED;
	}

	public boolean containsOrderStatus(String orderStatus) {
		for (OrderStatus os : OrderStatus.values()) {
			if (os.name().equals(orderStatus)) {
				return true;
			}
		}

		return false;
	}

	@When("Get Last Id previous test$")
	public void get_last_id_previous_test() throws IOException, InterruptedException {
		
		
        
        //File fDir = new File("./src/test/resources/runtime/feedlastid.properties");
        File fDir = new File("./src/test/resources/runtime");
        Boolean ex = fDir.exists();
        if(!ex) {
        	lastId = "0000";
        	Properties p = new Properties();
            p.put("LastId", lastId);
        	
            Boolean b = fDir.mkdirs();
    		//logger.debug("fDir.mkdirs(): " + b);
    		File f = new File("./src/test/resources/runtime/feedlastid.properties");
            f.createNewFile();
            FileOutputStream fos = new FileOutputStream(f, false);
            p.store(fos, "");
            //logger.debug("lastId " + lastId +  " naar proprty file geschreven");
        } else {
        	Boolean b = fDir.mkdirs();
    		//logger.debug("fDir.mkdirs(): " + b);
    		File f = new File("./src/test/resources/runtime/feedlastid.properties");
            f.createNewFile();
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/feedlastid.properties");
		properties.load(inputDriver);
		if(lastId !=null && !lastId.isEmpty()) {
			lastId = properties.getProperty("LastId");
		} else {
			lastId = "17800";
		}
		//lastId = properties.getProperty("LastId");

			//logger.debug("leasingType van System property lezen");
			//logger.debug("lastId is: " + lastId);
			String leasingType = System.getProperty("leasingType");
			logger.debug("leasingType:" + leasingType);
		response = Endpoints.getOrderNotifications(lastId, leasingType);
		//logger.debug("response:" + response.asPrettyString());

		List<OrderNotificationFeed> orderNotifications = Arrays
				.asList(response.getBody().as(OrderNotificationFeed[].class));

		// order = getOrderByDossierNumber.get_order_by_dossiernumber(orderList, dosNr);
		// id = order.orderId;
		//logger.debug("size: " + orderNotifications.size());
		if (orderNotifications.size() > 0 && orderNotifications != null) {
			String lastIdPrevious = getApiOrderNotifications.get_order_notifications_last_id(orderNotifications,
					lastId);
			//logger.debug("lastIdPrevious: " + lastIdPrevious);
		}
        }

	}

	public String getDosNr() {
		return dosNr;
	}

	public void setDosNr(String dosNr) {
		this.dosNr = dosNr;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
