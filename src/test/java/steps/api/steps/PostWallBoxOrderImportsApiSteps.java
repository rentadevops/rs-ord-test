package steps.api.steps;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.xml.bind.JAXBException;

//import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import managers.PageObjectManager;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_PostWallBoxImports;


public class PostWallBoxOrderImportsApiSteps {
	final Logger logger = LogManager.getLogger(PostWallBoxOrderImportsApiSteps.class);
	
	PageObjectManager pageObjectManager;
	TestContext testContext;
	FTP_PostWallBoxImports postWallBoxImports;
	String leasingType;
	
	public PostWallBoxOrderImportsApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 postWallBoxImports = testContext.getPageObjectManager().postFTP_PostWallBoxImports();

	}

	@When("Create WallBox OrderImports and Upload To FSTP")
	public void create_orderImportsRequestAndUploadToFstp(DataTable table) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, jakarta.xml.bind.JAXBException {
		
		List<List<String>> data = table.asLists();
		leasingType = data.get(1).get(0);
		
		//postWallBoxImports.upload_wallboxImports(leasingType);
		postWallBoxImports.upload_wallboxImports(leasingType);
	}


	
	}
