package steps.api.steps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

//import cucumber.api.java.en.When;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.Endpoints;
import apiEngine.model.get.order.Order;
import cucumber.TestContext;
//import io.cucumber.java.en.When;
import io.restassured.response.Response;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.ui.ORD_Order_Page;

//import static org.junit.Assert.assertThat;

public class GetOrderByDossierNumberApiSteps extends BaseSteps {
	final Logger logger = LogManager.getLogger(GetOrderByDossierNumberApiSteps.class);
	

	ORD_Order_Page orderPage;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	
	public String dosNr;


	public GetOrderByDossierNumberApiSteps(TestContext testContext) throws InterruptedException, IOException {
		super(testContext);
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();

	}

	String id;
	String orderId;
	private static Response response;
	private static Order order;

	@When("Get Order By DossierNumber$")
	public void get_order_by_dossiernumber() throws IOException, InterruptedException {
		logger.debug("Start get_order_by_dossiernumber");
		Properties properties = new Properties();
		//InputStream inputDriver = getClass().getClassLoader().getResourceAsStream("dossiernumber.properties");
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		dosNr = properties.getProperty("DossierNumber");
		setDosNr(dosNr);
		//dosNr = orderPage.getDosNr();
		Integer seconden = 600;
		boolean succes = false;
		long endWaitTime = System.currentTimeMillis() + seconden*1000;

		while (System.currentTimeMillis() < endWaitTime && !succes) {
			response = Endpoints.getOrderByDosNr(dosNr);
			logger.debug("response:" + response.asPrettyString());
			String body = response.asPrettyString();
			logger.debug("responseBody length: " + body.length());
			if(body.length() > 80) {
				logger.debug("response:" + response.asPrettyString());
				succes = true;
				break;
			} else {
				logger.debug("Order nog niet grvonden");
				Thread.sleep(5000);
			}
		}
		//response = Endpoints.getOrderByDosNr(dosNr);
		logger.debug("response:" + response.asPrettyString());
		
		List<Order> orderList = Arrays.asList(response.getBody().as(Order[].class));
		
		//orderList = Arrays.asList(response.getBody().as(Order[].class));	
		
		//order = response.getBody().as(Order.class);
		order = getOrderByDossierNumber.get_order_by_dossiernumber(orderList, dosNr);
		logger.debug("Dit is de laatste response in get order by dossiernumber:" + order.toString());
		logger.debug("Dit is de orderid uit de laatste response in get order by dossiernumber:" + order.getOrderId());
		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
			dosNr = order.getLeasingCompanyDossierNumber();
		} else {
			dosNr = order.getLeasingCompanyDossierNumber();
		}
		logger.debug("id: " + id);

	}

	public Order getOrder() {
		return order;
	}

	public String getDosNr() {
		logger.debug("dosNr getter getOrderByDossierNumber" + dosNr);
		return dosNr;
	}

	public void setDosNr(String dosNr) {
		this.dosNr = dosNr;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
