package steps.api.steps;

import apiEngine.Endpoints;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetBrokerRegistration;
import pageobjects.api.API_GetRegistration;
import pageobjects.api.API_PostBrokerRegistration;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

public class PutBrokerRegistrationApprovedApiSteps {
	final Logger logger = LogManager.getLogger(PutBrokerRegistrationApprovedApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String orderId;
	API_GetRegistration getRegistration;
	API_GetBrokerRegistration getBrokerRegistration;
	GetBrokerRegistrationApiSteps getBrokerRegistrationApiSteps;
	API_PostBrokerRegistration postBrokerRegistration;

	private static Response response;

	public PutBrokerRegistrationApprovedApiSteps(TestContext context) throws InterruptedException, IOException {
		 testContext = context;
		 getRegistration = testContext.getPageObjectManager().getAPI_GetRegistration();
		 getBrokerRegistration = testContext.getPageObjectManager().getAPI_GetBrokerRegistration();
		postBrokerRegistration = testContext.getPageObjectManager().postAPI_PostBrokerRegistration();



	}

	
	@When("Put Broker Registration Approved")
	public void put_broker_registration_approve() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, URISyntaxException {
		Response res = getBrokerRegistration.get_broker_registration();
		logger.debug("GetBrokerRegistrationApiSteps respomse:" + res.asPrettyString());
		//Response res = getBrokerRegistration.getResponse();
		JsonPath jsonPathEvaluator = res.jsonPath();
		String registrationId = jsonPathEvaluator.getString("registrationId");
		logger.debug("registrationId in PostRegistrationComplete: " + registrationId);
		String vatNumber = jsonPathEvaluator.getString("licensePlateHolder.vatNumber.value");
		logger.debug("vatNumber invoer:" + vatNumber);
		String jsonBodyNationalId = "{\n" +
				"  \"nationalId\": {\n" +
				"    \"value\": \"25020139913\"\n" +
				"  }\n" +
				"}";
		String jsonBodyEmpty = "{\n" +
				"  \"nationalId\": null\n" +
				"}";
		if(vatNumber == null) {
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyNationalId);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		} else {
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyEmpty);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		}
		

		
	}

	@When("Put Broker Registration Approved After Company Registration")
	public void put_broker_registration_approve_after_company_registration() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, URISyntaxException {
		Response res = getRegistration.getResponse();
		JsonPath jsonPathEvaluator = res.jsonPath();
		String registrationId = jsonPathEvaluator.getString("registrationId");
		logger.debug("registrationId in PostRegistrationApproved: " + registrationId);
		String vatNumber = jsonPathEvaluator.getString("licensePlateHolder.enterpriseNumber");
		logger.debug("vatNumber invoer:" + vatNumber);
		String jsonBodyNationalId = "{\n" +
				"  \"nationalId\": {\n" +
				"    \"value\": \"25020139913\"\n" +
				"  }\n" +
				"}";
		String jsonBodyEmpty = "{\n" +
				"  \"nationalId\": null\n" +
				"}";
		if(vatNumber == null) {
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyNationalId);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		} else {
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyEmpty);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		}



	}
}



	

