package steps.api.steps;

import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.concurrent.Callable;

import javax.xml.bind.JAXBException;

//import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
import managers.PageObjectManager;
import pageobjects.ftp.FTP_GetOrderNotifications;
import pageobjects.ftp.FTP_PostOrderImportsNotification;
//import org.awaitility.*;

public class PostOrderImportsNotificationApiSteps {
	final Logger logger = LogManager.getLogger(PostOrderImportsNotificationApiSteps.class);
	
	PageObjectManager pageObjectManager;
	TestContext testContext;
	FTP_PostOrderImportsNotification postOrderImportsNotification;
	FTP_GetOrderNotifications getOrderNotifications;
	
	public PostOrderImportsNotificationApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 postOrderImportsNotification = testContext.getPageObjectManager().postFTP_PostOrderImportsNotification();
		 getOrderNotifications = testContext.getPageObjectManager().getFTP_GetOrderNotifications();

	}

	@When("Download OrderImports Notification")
	public void download_orderImportsNotification() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, NullPointerException, ParseException, jakarta.xml.bind.JAXBException {
		
		postOrderImportsNotification.download_orderImportsNotification();
		//assertTrue(check);
		//Awaitility.await().atMost(Duration.ONE_SECOND).until(check);
		//postOrderImportsNotification.download_orderImportsNotification();
		
		
	}
	
	@When("Download OrderImports Notification Luxembourg")
	public void download_orderImportsNotification_Luxembourg() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, NullPointerException, ParseException, jakarta.xml.bind.JAXBException {
		
		postOrderImportsNotification.download_orderImportsNotificationLux();
		//assertTrue(check);
		//Awaitility.await().atMost(Duration.ONE_SECOND).until(check);
		//postOrderImportsNotification.download_orderImportsNotification();
		
		
	}
	
	@When("Delete OrderImports Notification")
	public void delete_orderImportsNotification() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException {
		
		//List<List<String>> data = table.asLists();
		
		postOrderImportsNotification.delete_orderImportsNotification();
	}
	
	@When("Reset File Number")
	public void reset_file_number() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, NullPointerException, ParseException {
		

		postOrderImportsNotification.reset_fileNr();
		
		
		
	}

	@When("^Dummy \"([^\"]*)\"$")
	public void dummy(String dummy) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, NullPointerException, ParseException {


		logger.debug(dummy);



	}
	
	


	
	}
