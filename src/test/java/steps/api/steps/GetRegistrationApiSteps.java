package steps.api.steps;

import apiEngine.Endpoints;
import apiEngine.model.get.order.Order;
import apiEngine.model.get.registration.registration.Registration;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_GetRegistration;
import pageobjects.api.API_PutRegistration;
import pageobjects.ftp.FTP_RegistrationImports;
import pageobjects.ui.ORD_Order_Page;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Properties;

//import static org.junit.Assert.assertThat;

public class GetRegistrationApiSteps extends BaseSteps {
	final Logger logger = LogManager.getLogger(GetRegistrationApiSteps.class);


	//ORD_Order_Page orderPage;
	//API_GetOrderByDossierNumber getOrderByDossierNumber;
	//FTP_RegistrationImports registrationImports;
	API_PutRegistration putRegistration;
	API_GetRegistration getRegistration;
	//GetRegistrationApiSteps getRegistration;

	public String dosNr;


	public GetRegistrationApiSteps(TestContext testContext) throws InterruptedException {
		super(testContext);
		//orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		//getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		//registrationImports = testContext.getPageObjectManager().postFTP_RegistrationImports();
		putRegistration = testContext.getPageObjectManager().putAPI_PutRegistration();
		getRegistration = testContext.getPageObjectManager().getAPI_GetRegistration();


	}

	String id;
	String vin;
	String registrationId;
	String insuranceReferenceNumber;
	static Response response;
	//private static Order order;



	@When("Get Registration$")
	public Response get_registration() throws IOException, InterruptedException, ParseException {
		logger.debug("Start get_registration");
		//Properties properties = new Properties();
		//InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		//properties.load(inputDriver);
		//dosNr = properties.getProperty("DossierNumber");
		//dosNr = orderPage.getDosNr();
		vin = putRegistration.getVehicleVIN();
		insuranceReferenceNumber = putRegistration.getInsuranceReferenceNumber();
		logger.debug("VIN to Search: " + vin);
		Integer seconden = 600;
		boolean succes = false;
		long endWaitTime = System.currentTimeMillis() + seconden * 1000;

		while (System.currentTimeMillis() < endWaitTime && !succes) {
			response = Endpoints.getRegistration(insuranceReferenceNumber);
			logger.debug("response getRegistration:" + response.asPrettyString());
			String body = response.asPrettyString();
			logger.debug("responseBody length: " + body.length());
			if (body.length() > 80) {
				logger.debug("response:" + response.asPrettyString());
				succes = true;
				break;
			} else {
				logger.debug("Registration nog niet gevonden");
				Thread.sleep(5000);
			}
		}
		JsonPath jsonPathEvaluator = response.jsonPath();
		//String jsonId = jsonPathEvaluator.getList().get("id").toString();
		List<Object> registrations = jsonPathEvaluator.getList("registrationId");
		logger.debug("response getRegistration registrations size: " + registrations.size());
		logger.debug("response getRegistration registrationids: " + registrations.toString());
		for (int i = 0; i < registrations.size(); i++) {
			registrationId = null;
			String registration = registrations.get(i).toString();
			logger.debug("response getRegistration get jsonpath id:" + registration);
			ResponseBody body = response.body();
			registrationId = registration.replace("[", "");
			registrationId = registration.replace("]", "");

			response = Endpoints.getRegistrationById(registrationId);

			setResponse(response);
			logger.debug("response getRegistration:" + response.asPrettyString());
			ResponseBody body1 = response.body();
			//JsonPath jp = $;
			//Registration reg = body1.jsonPath("$");
			//logger.debug("String body1 length: " + body1.length());
			//id = jsonId;
			//System.setProperty("wallBoxOrderId", id);
			logger.debug("registration body1: " + body1.asPrettyString());
			logger.debug("registrationId: " + registrationId);
			getRegistration.get_registration_data(response);
		}



		return response;
	}
	public static Response getResponse() {
		return response;
	}

	private void setResponse(Response response) {
	}


	


}


//public String getId() {
	//	return id;
	//}

	//public void setId(String id) {
	//	this.id = id;
	//}

	

