package steps.api.steps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

//import cucumber.api.java.en.When;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.Endpoints;
import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.order.Order;
import apiEngine.model.get.order.detail.OrderDetail;
import apiEngine.model.get.order.timeline.OrderTimeline;
import apiEngine.model.get.supplier.actor.SupplierActor;
import cucumber.TestContext;
//import io.cucumber.java.en.When;
import io.restassured.response.Response;
import pageobjects.api.API_GetLeasingCompanyActorById;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_GetOrderDetailById;
import pageobjects.api.API_GetOrderTimelineById;
import pageobjects.api.API_GetSupplierActorById;


public class GetOrderDetailByIdApiSteps extends BaseSteps {
	final Logger logger = LogManager.getLogger(GetOrderDetailByIdApiSteps.class);

	API_GetOrderDetailById getOrderDetailById;
	API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	API_GetSupplierActorById getSupplierActorById;
	GetOrderByDossierNumberApiSteps getOrderByDossierNumberApiSteps;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	API_GetOrderTimelineById getOrderTimelineById;
	
	public GetOrderDetailByIdApiSteps(TestContext testContext) throws InterruptedException {
		super(testContext);
		getOrderDetailById = testContext.getPageObjectManager().getAPI_GetOrderDetailById();
		getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		getLeasingCompanyActorById = testContext.getPageObjectManager().getAPI_GetLeasingCompanyActorById();
		getSupplierActorById = testContext.getPageObjectManager().getAPI_GetSupplierActorById();
		getOrderTimelineById = testContext.getPageObjectManager().getAPI_GetOrderTimelineById();
	}

	String id;
	String dosNr;
	Order order;
	private static Response response;
	private static Response responseLCActor;
	private static Response responseSupplierActor;
	private static OrderDetail orderDetail;
	private static LeasingCompanyActor leasingCompanyActor;
	private static SupplierActor supplierActor;
	private static OrderTimeline orderTimeline;

	@When("Get Order Detail By Id$")
	public void get_order_detail_by_id() throws IOException, InterruptedException, ParseException {
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		dosNr = properties.getProperty("DossierNumber");
		//dosNr = orderPage.getDosNr();
		response = Endpoints.getOrderByDosNr(dosNr);
		logger.debug("response getOrderByDossierNumber:" + response.asPrettyString());
		JsonPath jsonPathEvaluator = response.jsonPath();
		String jsonOrderId = jsonPathEvaluator.get("orderId").toString();
		String orderId = jsonOrderId;
		logger.debug("response getOrderByDossierNumber get jsonpath orderId:" + jsonOrderId);
		ResponseBody body = response.body();

		
		//List<Order> orderList = Arrays.asList(response.getBody().as(Order[].class));
		//List<Order> tmpOrderList = Arrays.asList(body.as(Order[].class));
		//Order tmpOrder = tmpOrderList.get(0);
		//logger.debug("tmpOrder getOrderId:" + tmpOrder.getOrderId());

		//logger.debug("orderList:" + orderList.toString());
		//logger.debug("orderList orderId:" + orderList.get(0).orderId);

		//order = getOrderByDossierNumber.get_order_by_dossiernumber(orderList, dosNr);
		logger.debug("orderId van jsonPath:" + orderId);
		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG") || System.getProperty("leasingType").equals("FINANCIAL_LEASE") || System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
			logger.debug("Ik ben Lux");
			orderId = orderId.replace("[", "");
			orderId = orderId.replace("]", "");
			id = orderId;
		} else {
			String jsonId = jsonPathEvaluator.get("id").toString();
			logger.debug("Ik ben gewoon");
			jsonId = jsonId.replace("[", "");
			jsonId = jsonId.replace("]", "");
			id = jsonId;
		}

		logger.debug("id: " + id);


		response = Endpoints.getOrderDetailById(id);
		logger.debug("response getOrderDetailById :" + response.asPrettyString());
		orderDetail = response.getBody().as(OrderDetail.class);
		logger.debug("response orderdetail by id: " + response.asPrettyString());
		logger.debug("orderdetail: " + orderDetail.toString());
		getOrderDetailById.get_order_detail_by_id_data(orderDetail);
		
		response = Endpoints.getOrderTimelineById(id);
		orderTimeline = response.getBody().as(OrderTimeline.class);
		logger.debug("response ordertimeline:" + response.asPrettyString());
		getOrderTimelineById.get_order_timeline_by_id_data(orderTimeline);
		
		String rsNumber = orderDetail.getLeasingCompany().getRsNumber().toString();
		responseLCActor = Endpoints.getLeasingCompanyActorById(rsNumber);
		logger.debug("responseLCActor:" + responseLCActor.asPrettyString());
		leasingCompanyActor = responseLCActor.getBody().as(LeasingCompanyActor.class);
		logger.debug("leasingCompanyActor:" + leasingCompanyActor.toString());
		getLeasingCompanyActorById.get_leasing_company_actor_by_id_data(leasingCompanyActor);

		String rsNumberDealer = orderDetail.getDealer().getNumber().toString();
		responseSupplierActor = Endpoints.getSupplierActorById(rsNumberDealer);
		supplierActor = responseSupplierActor.getBody().as(SupplierActor.class);
		logger.debug("responseSupplierActor:" + responseSupplierActor.asPrettyString());
		getSupplierActorById.get_supplier_actor_by_id_data(supplierActor);
		
	}

	
}