package steps.api.steps;


import apiEngine.model.post.wallbox.status.Installed;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
//import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_PostBrokerRegistration;
import pageobjects.api.API_PostWallBoxOrderInstalled;
import pageobjects.api.API_PutRegistration;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public class PutRegistrationApiSteps {
	final Logger logger = LogManager.getLogger(PutRegistrationApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String id;
	String status;
	//API_GetOrderByDossierNumber getOrderByDossierNumber;
	//API_PostWallBoxOrderInstalled postWallBoxOrderInstalled;
	API_PutRegistration putRegistration;
	API_PostBrokerRegistration postBrokerRegistration;
	Installed installed;

	private static Response response;

	public PutRegistrationApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 //getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		putRegistration = testContext.getPageObjectManager().putAPI_PutRegistration();
		postBrokerRegistration = testContext.getPageObjectManager().postAPI_PostBrokerRegistration();

	}

	
	@When("Put Registration")
	public void put_registration(DataTable table) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, ParseException, jakarta.xml.bind.JAXBException {

		List<List<String>> data = table.asLists();
		//leasingType = data.get(1).get(0);

		putRegistration.putRegistration(data);

	}

	@When("Post Broker Registration")
	public void post_broker_registration(DataTable table) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, ParseException, jakarta.xml.bind.JAXBException {

		List<List<String>> data = table.asLists();
		//leasingType = data.get(1).get(0);

		postBrokerRegistration.postBrokerRegistration(data);

	}
}



	

