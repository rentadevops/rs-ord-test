package steps.api.steps;


import apiEngine.Endpoints;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.response.Response;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import pageobjects.api.API_GetOrderByDossierNumber;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class PostWallBoxOrderOrderedApiSteps {
	final Logger logger = LogManager.getLogger(PostWallBoxOrderOrderedApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String id;
	String status;
	API_GetOrderByDossierNumber getOrderByDossierNumber;

	private static Response response;

	public PostWallBoxOrderOrderedApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();

	}

	
	@When("Post WallBox Order Ordered")
	public void post_wallbox_order_ordered() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException {
		id = System.getProperty("wallBoxOrderId");
		logger.debug("orderId in post_wallbox_order_accepted: " + id);
		
		status = Endpoints.postWallBoxOrderOrdered(id);
		logger.debug("response postWallBoxOrderOrdered:" + status);
		Assert.assertEquals(status.trim(), "HTTP/1.1 200");
		
	}
}



	

