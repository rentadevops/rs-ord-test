package steps.api.steps;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
//import org.apache.commons.io.input.XmlStreamReader;

import javax.imageio.ImageIO;
import javax.xml.stream.XMLStreamReader;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class TestTranscode {

    public static void main(String... args) throws Exception {
        //FileInputStream fis = new FileInputStream("file:///C:/Users/AdAntonisse/git/RsOrdTest/src/test/resources/runtime/QRCode.svg");

        //String qrCodeURL = qr_code_digital_delivery.getAttribute("src");

        //InputStream in = new URL(qrCodeURL).openStream();
        //Files.copy(in, Paths.get("./src/test/resources/runtime/QRCode.svg"), StandardCopyOption.REPLACE_EXISTING);

        FileInputStream fis = new FileInputStream(String.valueOf("./src/test/resources/runtime/QRCode.svg"));
        //BufferedImage image = ImageIO.read(in);

        JPEGTranscoder transcoder = new JPEGTranscoder();
        transcoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, 1.0F);
        TranscoderInput input = new TranscoderInput("file:///C:/Users/AdAntonisse/git/RsOrdTest/src/test/resources/runtime/QRCode.svg");
        OutputStream ostream = new FileOutputStream("./src/test/resources/runtime/QRCode.jpg");
        TranscoderOutput output = new TranscoderOutput(ostream);
        transcoder.transcode(input, output);
        ostream.close();

        fis = new FileInputStream(new File("./src/test/resources/runtime/QRCode.jpg"));
        //image = ImageIO.read(fis);
        //BufferedImage cropedImage = image.getSubimage(0, 0, 400, 400);

        //LuminanceSource luminanceSource= new BufferedImageLuminanceSource(image);
        //BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(luminanceSource));
        //Result result = new MultiFormatReader().decode(binaryBitmap);
        //String textInQrCode = result.getText();
        //System.out.println("textInQrCode:" + textInQrCode);


    }
}
