package steps.api.steps;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import apiEngine.Endpoints;
import cucumber.ScenarioContext;
import cucumber.TestContext;
 
public class BaseSteps {
    private Endpoints endPoints;
    private ScenarioContext scenarioContext;
 
    public BaseSteps(TestContext testContext) {
     scenarioContext = testContext.getScenarioContext();
    }
 
    public Endpoints getEndPoints() {
        return endPoints;
    }
    
    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }
    
    //@BeforeClass
    public void classLevelSetup() {

    }
    
    @AfterClass
    public void tearDown() {
    }
}
