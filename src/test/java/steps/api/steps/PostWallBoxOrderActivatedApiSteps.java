package steps.api.steps;


import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.response.Response;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_PostWallBoxOrderAccepted;
import pageobjects.api.API_PostWallBoxOrderActivated;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class PostWallBoxOrderActivatedApiSteps {
	final Logger logger = LogManager.getLogger(PostWallBoxOrderActivatedApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String id;
	String status;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	API_PostWallBoxOrderAccepted postWallBoxOrderAccepted;
	API_PostWallBoxOrderActivated postWallBoxOrderActivated;

	private static Response response;

	public PostWallBoxOrderActivatedApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		 postWallBoxOrderAccepted = testContext.getPageObjectManager().postAPI_PostWallBoxOrderAccepted();
		postWallBoxOrderActivated = testContext.getPageObjectManager().postAPI_PostWallBoxOrderActivated();

	}

	
	@When("Post WallBox Order Activated")
	public void post_wallbox_order_activated() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, jakarta.xml.bind.JAXBException {
		id = System.getProperty("wallBoxOrderId");
		logger.debug("orderId in post_wallbox_order_activated: " + id);

		postWallBoxOrderActivated.postWallBoxOrderActivated(id);

	}
}



	

