package steps.api.steps;


import apiEngine.Endpoints;
import apiEngine.model.post.wallbox.status.Installed;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.response.Response;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_PostWallBoxOrderInstalled;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class PostWallBoxOrderInstalledApiSteps {
	final Logger logger = LogManager.getLogger(PostWallBoxOrderInstalledApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String id;
	String status;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	API_PostWallBoxOrderInstalled postWallBoxOrderInstalled;
	Installed installed;

	private static Response response;

	public PostWallBoxOrderInstalledApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		 postWallBoxOrderInstalled = testContext.getPageObjectManager().postAPI_PostWallBoxOrderInstalled();

	}

	
	@When("Post WallBox Order Installed")
	public void post_wallbox_order_installed() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, jakarta.xml.bind.JAXBException {

		id = System.getProperty("wallBoxOrderId");
		logger.debug("orderId in post_wallbox_order_accepted: " + id);

		postWallBoxOrderInstalled.postWallBoxOrderInstalled(id);

	}
}



	

