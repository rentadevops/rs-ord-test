package steps.api.steps;

import apiEngine.Endpoints;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_GetRegistration;
import pageobjects.ftp.FTP_PostOrderImportsRequest;

import javax.xml.bind.JAXBException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Properties;

public class PostDigitalDeliveryConfirmationApiSteps {
	final Logger logger = LogManager.getLogger(PostDigitalDeliveryConfirmationApiSteps.class);

	PageObjectManager pageObjectManager;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	TestContext testContext;
	String orderId;
	//API_GetRegistration getRegistration;

	private static Response response;

	public PostDigitalDeliveryConfirmationApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 //getRegistration = testContext.getPageObjectManager().getAPI_GetRegistration();
		getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();

	}

	
	@When("Post Digital Delivery Confirmation")
	public void post_digital_delivery_confirmation() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, URISyntaxException {
		String dosNr;
		//Response res = getRegistration.getResponse();
		//JsonPath jsonPathEvaluator = res.jsonPath();
		//String registrationId = jsonPathEvaluator.getString("registrationId");
		//logger.debug("registrationId in PostRegistrationComplete: " + registrationId);
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		dosNr = properties.getProperty("DossierNumber");
		//dosNr = orderPage.getDosNr();
		response = Endpoints.getOrderByDosNr(dosNr);
		logger.debug("response getOrderByDossierNumber:" + response.asPrettyString());
		JsonPath jsonPathEvaluator = response.jsonPath();
		String jsonOrderId = jsonPathEvaluator.get("orderId").toString();
		String orderId = jsonOrderId;
		//orderId = getOrderByDossierNumber.getOrderId();
		//logger.debug("orderId:" + orderId);
		
		//response = Endpoints.postDigitalDeliveryConfirmation(orderId);
		response = Endpoints.postDigitalDeliveryConfirmation(orderId);
		logger.debug("statusLine:" + response.statusLine());


	}
}



	

