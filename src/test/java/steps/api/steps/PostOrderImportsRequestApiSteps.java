package steps.api.steps;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.xml.bind.JAXBException;

//import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import managers.PageObjectManager;
import pageobjects.ftp.FTP_PostOrderImportsMigrate;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_PostUpdateOrderImportsRequest;


public class PostOrderImportsRequestApiSteps {
	final Logger logger = LogManager.getLogger(PostOrderImportsRequestApiSteps.class);
	
	PageObjectManager pageObjectManager;
	TestContext testContext;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_PostUpdateOrderImportsRequest postUpdateOrderImportsRequest;
	FTP_PostOrderImportsMigrate postOrderImportsMigrate;
	String leasingType;
	
	public PostOrderImportsRequestApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		postOrderImportsMigrate = testContext.getPageObjectManager().postFTP_PostOrderImportsMigrate();
		postUpdateOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostUpdateOrderImportsRequest();

	}

	@When("Create OrderImports Request and Upload To FSTP")
	public void create_orderImportsRequestAndUploadToFstp(DataTable table) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, jakarta.xml.bind.JAXBException {
		
		List<List<String>> data = table.asLists();
		leasingType = data.get(1).get(0);
		
		postOrderImportsRequest.upload_orderImportsRequest(table);
	}

	@When("Create OrderImports Update Order")
	public void create_orderImportsUpdateOrder(DataTable table) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, jakarta.xml.bind.JAXBException {

		List<List<String>> data = table.asLists();
		leasingType = data.get(1).get(0);

		postUpdateOrderImportsRequest.upload_orderUpdateImportsRequest(table);
	}

	@When("Create Migration OrderImports Request and Upload To FSTP")
	public void create_migrationOrderImportsRequestAndUploadToFstp(DataTable table) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException {

		List<List<String>> data = table.asLists();
		leasingType = data.get(1).get(0);
		logger.debug("leasingType: " + leasingType);
		try {
			postOrderImportsMigrate.upload_migrateOrderImportsRequest(leasingType, data);
		} catch(Exception e) {
			logger.debug("Stacktrace: " + e.getMessage());
			e.printStackTrace();
		}
	}


	
	}
