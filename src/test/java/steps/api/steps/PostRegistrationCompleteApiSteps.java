package steps.api.steps;

import apiEngine.Endpoints;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_GetRegistration;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

public class PostRegistrationCompleteApiSteps {
	final Logger logger = LogManager.getLogger(PostRegistrationCompleteApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String orderId;
	API_GetRegistration getRegistration;

	private static Response response;

	public PostRegistrationCompleteApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getRegistration = testContext.getPageObjectManager().getAPI_GetRegistration();

	}

	
	@When("Post Registration Complete")
	public void post_registrationComplete() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, URISyntaxException {
		Response res = getRegistration.getResponse();
		JsonPath jsonPathEvaluator = res.jsonPath();
		String registrationId = jsonPathEvaluator.getString("registrationId");
		logger.debug("registrationId in PostRegistrationComplete: " + registrationId);
		
		response = Endpoints.postRegistrationComplete(registrationId);
		logger.debug("post registration complete response:" + response.asPrettyString());
		ResponseBody rb = response.getBody().prettyPeek();
		logger.debug("ERROR post registration complete response " + rb.asPrettyString());
		
	}
}



	

