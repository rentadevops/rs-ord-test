package steps.api.steps;

import apiEngine.Endpoints;
import apiEngine.model.get.order.Order;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.ui.ORD_Order_Page;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

//import static org.junit.Assert.assertThat;

public class GetWallBoxOrderByDossierNumberApiSteps extends BaseSteps {
	final Logger logger = LogManager.getLogger(GetWallBoxOrderByDossierNumberApiSteps.class);


	ORD_Order_Page orderPage;
	API_GetOrderByDossierNumber getOrderByDossierNumber;

	public String dosNr;


	public GetWallBoxOrderByDossierNumberApiSteps(TestContext testContext) throws InterruptedException, IOException {
		super(testContext);
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();

	}

	String id;
	private static Response response;
	private static Order order;

	@When("Get WallBox Order By DossierNumber$")
	public void get_wallbox_order_by_dossiernumber() throws IOException, InterruptedException {
		logger.debug("Start get_order_by_dossiernumber");
		Properties properties = new Properties();
		//InputStream inputDriver = getClass().getClassLoader().getResourceAsStream("dossiernumber.properties");
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		dosNr = properties.getProperty("DossierNumber");
		//dosNr = orderPage.getDosNr();
		Integer seconden = 600;
		boolean succes = false;
		long endWaitTime = System.currentTimeMillis() + seconden*1000;

		while (System.currentTimeMillis() < endWaitTime && !succes) {
			response = Endpoints.getWallBoxOrderByDosNr(dosNr);
			logger.debug("response getWallBoxOrderByDosNr:" + response.asPrettyString());
			String body = response.asPrettyString();
			logger.debug("responseBody length: " + body.length());
			if(body.length() > 80) {
				logger.debug("response:" + response.asPrettyString());
				succes = true;
				break;
			} else {
				logger.debug("WallBox Order nog niet grvonden");
				Thread.sleep(5000);
			}
		}
		JsonPath jsonPathEvaluator = response.jsonPath();
		String jsonId = jsonPathEvaluator.get("id").toString();
		logger.debug("response getOrderByDossierNumber get jsonpath id:" + jsonId);
		ResponseBody body = response.body();
		jsonId = jsonId.replace("[", "");
		jsonId = jsonId.replace("]", "");
		id = jsonId;
		System.setProperty("wallBoxOrderId", id);
		logger.debug("id: " + id);




		//List<Order> orderList = Arrays.asList(response.getBody().as(Order[].class));
		

		//order = getOrderByDossierNumber.get_order_by_dossiernumber(orderList, dosNr);
		//id = order.id;


	}

	public Order getOrder() {
		return order;
	}

	public String getDosNr() {
		return dosNr;
	}

	public void setDosNr(String dosNr) {
		this.dosNr = dosNr;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
