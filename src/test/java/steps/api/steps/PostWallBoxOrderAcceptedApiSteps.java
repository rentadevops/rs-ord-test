package steps.api.steps;


import apiEngine.Endpoints;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cucumber.TestContext;
import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.restassured.response.Response;
import managers.PageObjectManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.api.API_PostWallBoxOrderAccepted;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class PostWallBoxOrderAcceptedApiSteps {
	final Logger logger = LogManager.getLogger(PostWallBoxOrderAcceptedApiSteps.class);

	PageObjectManager pageObjectManager;
	TestContext testContext;
	String id;
	String status;
	API_GetOrderByDossierNumber getOrderByDossierNumber;
	API_PostWallBoxOrderAccepted postWallBoxOrderAccepted;

	private static Response response;

	public PostWallBoxOrderAcceptedApiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		 postWallBoxOrderAccepted = testContext.getPageObjectManager().postAPI_PostWallBoxOrderAccepted();

	}

	
	@When("Post WallBox Order Accepted")
	public void post_wallbox_order_accepted() throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, jakarta.xml.bind.JAXBException {
		id = System.getProperty("wallBoxOrderId");
		logger.debug("orderId in post_wallbox_order_accepted: " + id);

		postWallBoxOrderAccepted.postWallBoxOrderAccepted(id);

	}
}



	

