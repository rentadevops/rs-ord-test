package steps.validation.steps;

import apiEngine.Endpoints;
import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.order.timeline.OrderTimeline;
import apiEngine.model.get.order.timeline.wallbox.WallBoxOrderTimeLine;
import apiEngine.model.get.supplier.actor.SupplierActor;
import cucumber.TestContext;
import enums.*;
import helpers.AssertHelper;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetLeasingCompanyActorById;
import pageobjects.api.API_GetOrderTimelineById;
import pageobjects.api.API_GetSupplierActorById;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_PostWallBoxImports;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;
import pageobjects.ui.ORD_Wallbox_Order_Details_Page;
import steps.api.steps.BaseSteps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Properties;

public class ValidateWallBoxOrderDetailsUiApi {
	final Logger logger = LogManager.getLogger(ValidateWallBoxOrderDetailsUiApi.class);

	TestContext testContext;
	ORD_Order_Details_Page orderDetailsPage;
	ORD_Wallbox_Order_Details_Page wallboxDetailsPage;
	ORD_Order_Page orderPage;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_PostWallBoxImports postWallBoxImports;
	API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	API_GetSupplierActorById getSupplierActorById;
	API_GetOrderTimelineById getOrderTimelineById;

	private AssertHelper ah = new AssertHelper();


	public ValidateWallBoxOrderDetailsUiApi(TestContext context) throws InterruptedException, IOException {
		testContext = context;
		orderDetailsPage = testContext.getPageObjectManager().getORD_Order_Details_Page();
		wallboxDetailsPage = testContext.getPageObjectManager().getORD_Wallbox_Order_Details_Page();
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		postWallBoxImports = testContext.getPageObjectManager().postFTP_PostWallBoxImports();
		getLeasingCompanyActorById = testContext.getPageObjectManager().getAPI_GetLeasingCompanyActorById();
		getSupplierActorById = testContext.getPageObjectManager().getAPI_GetSupplierActorById();
		getOrderTimelineById = testContext.getPageObjectManager().getAPI_GetOrderTimelineById();

	}

	String dosNr;
	String id;
	String cardNumber;
	String evbNumber;
	private static Response responseLCActor;
	private static LeasingCompanyActor leasingCompanyActor;
	private static Response responseSupplierActor;
	private static SupplierActor supplierActor;
	private static Response response;
	private static OrderTimeline orderTimeline;
	private static WallBoxOrderTimeLine wallBoxOrderTimeline;

	@Then("^User validates wallbox orderdetails are correct$")
	public void user_validates_wallbox_orderdetails_are_correct() throws InterruptedException, ParseException, IOException {



//********************************************** assertions of data from enter pages compared with data from overview page******************************************************************	
		RestAssured.defaultParser = Parser.JSON;
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		dosNr = properties.getProperty("DossierNumber");
		//dosNr = orderPage.getDosNr();
		response = Endpoints.getWallBoxOrderByDosNr(dosNr);
		logger.debug("response getOrderByDossierNumber:" + response.asPrettyString());
		JsonPath jsonPathEvaluator = response.jsonPath();
		String jsonOrderId = jsonPathEvaluator.get("id").toString();
		String orderId = jsonOrderId;
		logger.debug("response getOrderByDossierNumber get jsonpath orderId:" + jsonOrderId);
		ResponseBody body = response.body();
		logger.debug("orderId van jsonPath:" + orderId);

			String jsonId = jsonPathEvaluator.get("id").toString();
			logger.debug("Ik ben gewoon");
			jsonId = jsonId.replace("[", "");
			jsonId = jsonId.replace("]", "");
			id = jsonId;
		logger.debug("id: " + id);

		response = Endpoints.getWallBoxOrderTimelineById(id);
		logger.debug("response ordertimeline VOOR parsen:" + response.asPrettyString());
		wallBoxOrderTimeline = response.getBody().as(WallBoxOrderTimeLine.class);
		logger.debug("response ordertimeline:" + response.asPrettyString());
		getOrderTimelineById.get_wallbox_order_timeline_by_id_data(wallBoxOrderTimeline);

		String rsNumber = wallboxDetailsPage.getLeasingCompanyRsNumber();
		responseLCActor = Endpoints.getLeasingCompanyActorById(rsNumber);
		leasingCompanyActor = responseLCActor.getBody().as(LeasingCompanyActor.class);
		logger.debug("responseLCActor:" + responseLCActor.asPrettyString());
		getLeasingCompanyActorById.get_leasing_company_actor_by_id_data(leasingCompanyActor);

		String rsNumberDealer = wallboxDetailsPage.getSupplierRsNumber();
		responseSupplierActor = Endpoints.getSupplierActorById(rsNumberDealer);
		supplierActor = responseSupplierActor.getBody().as(SupplierActor.class);
		logger.debug("responseSupplierActor:" + responseSupplierActor.asPrettyString());
		getSupplierActorById.get_supplier_actor_by_id_data(supplierActor);

			ah.assertEquals("ORDERSTATUS ",	wallboxDetailsPage.getOrderStatus(),"Activated");
			ah.assertEquals("LEASINGCOMPANYDOSSIERNUMBER ", wallboxDetailsPage.getLeasingCompanyDossierNumber(), postWallBoxImports.getDosNrWB());

			//Leasing company
			ah.assertEquals("LEASINGCOMPANYNAME ", wallboxDetailsPage.getLeasingCompanyName(), getLeasingCompanyActorById.getLcInvoiceName());
			ah.assertEquals("LEASINGCOMPANYENTERPRISENUMBER ", wallboxDetailsPage.getLeasingCompanyEnterpriseNumber(), getLeasingCompanyActorById.getLcEnterpriseNumber());
			ah.assertEquals("LEASINGCOMPANYRSNUMBER ", wallboxDetailsPage.getLeasingCompanyRsNumber(), getLeasingCompanyActorById.getLcRsNumber());
			ah.assertEquals("LEASINGCOMPANYBILLINGADDRESS ", wallboxDetailsPage.getLeasingCompanybillingAddress(), getLeasingCompanyActorById.getLcBillingAddress());
			ah.assertEquals("LEASINGCOMPANYCOUNTRY ", wallboxDetailsPage.getLeasingCompanyCountry(), getLeasingCompanyActorById.getLcCountry());

			ah.assertNull("LEASINGCOMPANYCONTACTMOBILENUMBER ", wallboxDetailsPage.getLeasingCompanyContactMobileNumber());
			ah.assertNull("LEASINGCOMPANYCONTACTPHONENUMBER ", wallboxDetailsPage.getLeasingCompanyContactPhoneNumber());
			ah.assertEquals("LEASINGCOMPANYCONTACTLANGUAGE ", wallboxDetailsPage.getLeasingCompanyContactLanguage(), getLeasingCompanyActorById.getLcLanguage());
			ah.assertNull("LEASINGCOMPANYCONTACTEMAIL ", wallboxDetailsPage.getLeasingCompanyContactEmail());

			//Supplier
			ah.assertEquals("SUPPLIERNAMETITLE ", wallboxDetailsPage.getSupplierNameTitle(), getSupplierActorById.getCommercialName());
			ah.assertEquals("SUPPLIERNAME ", wallboxDetailsPage.getSupplierName(), getSupplierActorById.getInvoiceName());
			ah.assertEquals("SUPPLIERENTERPRISENUMBER ", wallboxDetailsPage.getSupplierEnterpriseNumber(), getSupplierActorById.getEnterpriseNumber());
			ah.assertEquals("SUPPLIERRSNUMBER ", wallboxDetailsPage.getSupplierRsNumber(), getSupplierActorById.getRsNumber());
			ah.assertEquals("SUPPLIERBILLINGADDRESS ", wallboxDetailsPage.getSupplierbillingAddress(), getSupplierActorById.getBillingAddress());
			ah.assertEquals("SUPPLIERCOUNTRY", wallboxDetailsPage.getSupplierCountry(), getSupplierActorById.getCountry());
			ah.assertEquals("SUPPLIERCONTACTPERSON ", wallboxDetailsPage.getSupplierContactPerson(), getSupplierActorById.getContactFullName());
			ah.assertEquals("SUPPLIERCONTACTEMAIL ", wallboxDetailsPage.getSupplierContactEmail(), getSupplierActorById.getContactEmail());
			if (wallboxDetailsPage.getSupplierContactMobileNumber() == null) {
			ah.assertNull("SUPPLIERCONTACTMOBILENUMBER is null ", getSupplierActorById.getContactPhoneNumber());
			} else {
			ah.assertEquals("SUPPLIERCONTACTMOBILENUMBER ", wallboxDetailsPage.getSupplierContactMobileNumber(), getSupplierActorById.getContactMobileNumber());
			}
			if (wallboxDetailsPage.getSupplierContactPhoneNumber() == null) {
				ah.assertNull("SUPPLIERCONTACTPHONENUMBER is null", getSupplierActorById.getContactPhoneNumber());
			} else {
				ah.assertEquals("SUPPLIERCONTACTPHONENUMBER ", wallboxDetailsPage.getSupplierContactPhoneNumber(), getSupplierActorById.getContactPhoneNumber());
			}
			ah.assertEquals("SUPPLIERCONTACTLANGUAGE ", wallboxDetailsPage.getSupplierContactLanguage(), getSupplierActorById.getLanguage());

			//Lessee
			//ah.assertEquals("LESSEEENAMETITLE ", wallboxDetailsPage.getLesseeNameTitle(), postWallBoxImports.getClientName());
			ah.assertEquals("LESSEEENTERPRISENUMBER ", wallboxDetailsPage.getLesseeEnterpriseNumber(), postWallBoxImports.getClientVAT());
			String zipCode = postWallBoxImports.getClientPostalCode();
			String city = postWallBoxImports.getClientCity();
			String streetHouseNumber = postWallBoxImports.getClientStreetHouseNumber();
			String cityZipCode = streetHouseNumber + ", " + zipCode.trim() + " " + city.trim();
			ah.assertEquals("LESSEEBILLINGADDRESS ", wallboxDetailsPage.getLesseeBillingAddress(), cityZipCode);
			ah.assertEquals("LESSEECONTACTPERSON ", wallboxDetailsPage.getLesseeContactPerson(), postWallBoxImports.getClientContactPerson());
			ah.assertEquals("LESSEECONTACTEMAIL ", wallboxDetailsPage.getLesseeContactEmail(), postWallBoxImports.getClientContactEmail());
			ah.assertEquals("LESSEECONTACTMOBILENUMBER ", wallboxDetailsPage.getLesseeContactMobileNumber(), postWallBoxImports.getClientContactMobileNumber());
			ah.assertEquals("LESSEECONTACTPHONENUMBER ", wallboxDetailsPage.getLesseeContactPhoneNumber(), postWallBoxImports.getClientContactPhoneNumber());
			ah.assertEquals("LESSEECONTACTLANGUAGE ", wallboxDetailsPage.getLesseeContactLanguage(), postWallBoxImports.getClientLanguageCode());

			//Product
			ah.assertEquals("BRAND ", wallboxDetailsPage.getProductBrand(), postWallBoxImports.getWallboxBrand());
			ah.assertEquals("MODEL ", wallboxDetailsPage.getProductModel(), postWallBoxImports.getWallboxModel());
			ah.assertEquals("PRODUCTDESCRIPTION ", wallboxDetailsPage.getProductDescription(), postWallBoxImports.getWallboxDescription());
			try {
				if(orderPage.getEvbNumber().length() > 42) {
					evbNumber = StringUtils.truncate(orderPage.getEvbNumber(), 42);
					logger.debug("evbNumber input was longer than 42, string cut off after 42 characters and now is: " + evbNumber);
				} else {
					evbNumber = orderPage.getEvbNumber();
				}
				ah.assertEquals("EVBNUMBER ", wallboxDetailsPage.getProductEvNumber(), evbNumber);
			} catch(AssertionError ae) {
				logger.debug(ae.getMessage());
			}
			//ah.assertEquals("EVBNUMBER ", wallboxDetailsPage.getProductEvNumber(), orderPage.getEvbNumber());
		try {
			if(orderPage.getCardNumber().length() > 42) {
				cardNumber = StringUtils.truncate(orderPage.getCardNumber(), 42);
				logger.debug("cardNumber input was longer than 42, string cut off after 42 characters and now is: " + cardNumber);
			} else {
				cardNumber = orderPage.getCardNumber();
			}
			ah.assertEquals("CARDNUMBER ", wallboxDetailsPage.getProductCardNumber(), cardNumber);
		} catch(AssertionError ae) {
			logger.debug(ae.getMessage());
		}
			//ah.assertEquals("CARDNUMBER ", wallboxDetailsPage.getProductCardNumber(), orderPage.getCardNumber());
			//ah.assertEquals("CARDSENT ", wallboxDetailsPage.getProductCardSent(), wallboxDetailsPage.getProductCardSent());
			ah.assertEquals("VEHICLEBRAND ", wallboxDetailsPage.getVehicleBrand(), postWallBoxImports.getVehicleBrand());
			ah.assertEquals("VEHICLEMODEL ", wallboxDetailsPage.getVehicleModel(), postWallBoxImports.getVehicleModel());

			//Driver and Delivery
			ah.assertEquals("DESIREDINSTALLATIONDATE ", wallboxDetailsPage.getDeliveryDesiredInstallationDate(), postWallBoxImports.getDeliveryDesiredDate());
			ah.assertEquals("EXPECTEDINSTALLATIONDATE ", wallboxDetailsPage.getDeliveryExpectedInstallationDate(), wallBoxOrderTimeline.getDeliveryDates().getExpectedInstallationDate());
			ah.assertEquals("INSTALLATIONDATE ", wallboxDetailsPage.getDeliveryInstallationDate(), wallBoxOrderTimeline.getDeliveryDates().getInstallationDate());
			ah.assertEquals("CERTIFICATIONDATE ", wallboxDetailsPage.getDeliveryCertificationDate(), wallBoxOrderTimeline.getDeliveryDates().getCertificationDate());
			ah.assertEquals("ACTIVATIONDATE ", wallboxDetailsPage.getDeliveryActivationDate(), wallBoxOrderTimeline.getDeliveryDates().getActivationDate());
			ah.assertEquals("DRIVERCONTACTPERSON ", wallboxDetailsPage.getDriverContactPerson(), postWallBoxImports.getDeliveryContactPerson());
			ah.assertEquals("DRIVERCONTACTEMAIL ", wallboxDetailsPage.getDriverContactEmail().replace("email ", ""), postWallBoxImports.getDeliveryContactEmail());
			ah.assertEquals("DRIVERCONTACTMOBILENUMBER ", wallboxDetailsPage.getDriverContactMobileNumber(), postWallBoxImports.getDeliveryContactMobileNumber());
			ah.assertEquals("DRIVERCONTACTPHONENUMBER ", wallboxDetailsPage.getDriverContactPhoneNumber(), postWallBoxImports.getDeliveryContactPhoneNumber());
			ah.assertEquals("DRIVERCONTACTLANGUAGE ", wallboxDetailsPage.getDriverContactLanguage(), null);

			//Pricing
			ah.assertEquals("PRICINGDESCRIPTION ", wallboxDetailsPage.getPricingDescription(), postWallBoxImports.getWallboxDescription());
			ah.assertEquals("PRICINGCROSSAMOUNT ", wallboxDetailsPage.getPricingCrossAmount(), postWallBoxImports.getPricingListPrice());
			String pricingDiscountAmount = postWallBoxImports.getPricingDiscountAmount().replace(",", ".");
			String pricingDiscountPercentage = postWallBoxImports.getPricingDiscountPercentage().replace(",00", "");
			String pricingDisc = "€" + pricingDiscountAmount + pricingDiscountPercentage;
			String scrDisc = wallboxDetailsPage.getPricingDiscount().replaceAll("\\s+","");
		scrDisc = scrDisc.replace("%", " %");
			ah.assertEquals("PRICINGDISCOUNT ", scrDisc, pricingDisc);
			ah.assertEquals("PRICINGTOTALAMOUNT ", wallboxDetailsPage.getPricingTotalNetAmount(), postWallBoxImports.getPricingTotalPrice());

		logger.debug(ah.processAllAssertions());


		}
	}

