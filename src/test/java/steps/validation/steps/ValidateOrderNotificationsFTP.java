package steps.validation.steps;

import apiEngine.model.get.sftp.wallbox.order.notification.WallBoxOrderNotification;
import apiEngine.model.get.sftp.wallbox.order.notification.WallBoxOrderNotifications;
import io.cucumber.java.en.Then;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import pageobjects.api.API_GetLeasingCompanyActorById;
import pageobjects.api.API_GetSupplierActorById;
import pageobjects.ftp.FTP_GetOrderNotifications;
import pageobjects.ftp.FTP_GetWallBoxOrderNotifications;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_PostWallBoxImports;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;
import cucumber.TestContext;
import enums.ContextGetLeasingCompanyActorByIdApi;
import enums.ContextGetSupplierActorByIdApi;
import enums.ContextOrderDetailUI;
import enums.ContextOrderUI;
import enums.ContextPostOrderImportsRequest;
import helpers.AssertHelper;

import java.io.File;

//import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.model.get.sftp.order.notification.CommentsNotification;
import apiEngine.model.get.sftp.order.notification.DealerAcceptedNotification;
import apiEngine.model.get.sftp.order.notification.DeliveredNotification;
import apiEngine.model.get.sftp.order.notification.LicensePlateNotification;
import apiEngine.model.get.sftp.order.notification.OrderNotifications;
import apiEngine.model.get.sftp.order.notification.OrderedNotification;
import apiEngine.model.get.sftp.order.notification.VehicleInNotification;

public class ValidateOrderNotificationsFTP {
	final Logger logger = LogManager.getLogger(ValidateOrderNotificationsFTP.class);

	TestContext testContext;
	ORD_Order_Details_Page orderDetailsPage;
	ORD_Order_Page orderPage;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_PostWallBoxImports postWallBoxImports;
	FTP_GetOrderNotifications getOrderNotifications;
	FTP_GetWallBoxOrderNotifications getWallBoxOrderNotifications;
	API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	API_GetSupplierActorById getSupplierActorById;
	private AssertHelper ah = new AssertHelper();

	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());

	String localFile = "./src/test/resources/runtime/sftpdownloads/order_update.xml";
	String localFileWB = "./src/test/resources/runtime/sftpdownloads/wallbox_order_update.xml";


	public ValidateOrderNotificationsFTP(TestContext context) throws InterruptedException, IOException {
		testContext = context;
		orderDetailsPage = testContext.getPageObjectManager().getORD_Order_Details_Page();
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		postWallBoxImports = testContext.getPageObjectManager().postFTP_PostWallBoxImports();
		getOrderNotifications = testContext.getPageObjectManager().getFTP_GetOrderNotifications();
		getWallBoxOrderNotifications = testContext.getPageObjectManager().getFTP_GetWallBoxOrderNotifications();
		getLeasingCompanyActorById = testContext.getPageObjectManager().getAPI_GetLeasingCompanyActorById();
		getSupplierActorById = testContext.getPageObjectManager().getAPI_GetSupplierActorById();

	}

	@Then("^User validates order notifications$")
	public void user_validates_order_notifications() throws InterruptedException, ParseException, JAXBException, jakarta.xml.bind.JAXBException {
		logger.debug("Start user_validates_order_notifications");
		JAXBContext jaxbContext;

		jaxbContext = JAXBContext.newInstance(OrderNotifications.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		// OrderNotifications orderNotifications = (OrderNotifications)
		// jaxbUnmarshaller.unmarshal(new
		// File("./src/test/resources/runtime/sftpdownloads/order_update_nn.xml"));

		String path = localFile;
		File file = new File(path);
		String absolutePath = file.getAbsolutePath();

		logger.debug("absolutePath: " + absolutePath);

		OrderNotifications orderNotifications = (OrderNotifications) jaxbUnmarshaller
				.unmarshal(new File(absolutePath)); //was localFile
		logger.debug("orderNotifications: " + orderNotifications);
		getOrderNotifications.get_order_notifications_data(orderNotifications);
		for (int i = 0; i < orderNotifications.getOrderNotification().size(); i++) {
			CommentsNotification comnot = orderNotifications.getOrderNotification().get(i).getCommentsNotification();
			DealerAcceptedNotification deanot = orderNotifications.getOrderNotification().get(i).getDealerAcceptedNotification();
			OrderedNotification ordnot = orderNotifications.getOrderNotification().get(i).getOrderedNotification();
			VehicleInNotification vinnot = orderNotifications.getOrderNotification().get(i).getVehicleInNotification();
			if(vinnot != null) {
				logger.debug("vinnot for loop: " + vinnot.toString());
			}

			LicensePlateNotification licnot = orderNotifications.getOrderNotification().get(i).getLicensePlateNotification();
			DeliveredNotification delnot = orderNotifications.getOrderNotification().get(i).getDeliveredNotification();
			if (comnot != null) {
				//String tc = orderNotifications.getOrderNotification().get(0).getCommentsNotification().getTransactionCode();
				String comnotTransactionCode = getOrderNotifications.getComnotTransactionCode();
				ah.assertEquals("TransactionCode ", comnotTransactionCode, "COMMENTS");
				logger.debug("TransactionCode: " + comnotTransactionCode);
				String comnotNotificationDate = getOrderNotifications.getComnotNotificationDate();
				ah.assertEquals("NotificationDate ", comnotNotificationDate, dateToday);
				logger.debug("NotificationDate: " + comnotNotificationDate);
				String comnotComments = getOrderNotifications.getComnotComments();
				logger.debug("Comments: " + comnotComments);
				String comnotDossierNumber = getOrderNotifications.getComnotDossierNumber();
				ah.assertEquals("DossierNumber ", comnotDossierNumber, postOrderImportsRequest.getDosNr());
				logger.debug("DossierNumber: " + comnotDossierNumber);
				//ah.processAllAssertions();
			} else if (deanot != null) {
				String deanotTransactionCode = deanot.getTransactionCode();
				ah.assertEquals("TransactionCode ", deanotTransactionCode, "DEALER_ACCEPTED");
				logger.debug("TransactionCode: " + deanotTransactionCode);
				String deanotNotificationDate = deanot.getNotificationDate();
				ah.assertEquals("NotificationDate ", deanotNotificationDate, dateToday);
				logger.debug("NotificationDate: " + deanotNotificationDate);
				String deanotComments = deanot.getComments();
				logger.debug("Comments: " + deanotComments);
				String deanotDossierNumber = deanot.getDossierNumber();
				ah.assertEquals("DossierNumber ", deanotDossierNumber, postOrderImportsRequest.getDosNr());
				logger.debug("DossierNumber: " + deanotDossierNumber);
				//ah.processAllAssertions();
			} else if (ordnot != null && ordnot.getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
				String ordnotTransactionCode = ordnot.getTransactionCode();
				ah.assertEquals("ordnotTransactionCode ", ordnotTransactionCode, "ORDERED");
				logger.debug("TransactionCode: " + ordnotTransactionCode);
				String ordnotNotificationDate = ordnot.getNotificationDate();
				ah.assertEquals("ordnotNotificationDate ", ordnotNotificationDate, dateToday);
				logger.debug("NotificationDate: " + ordnotNotificationDate);
				String ordnotDossierNumber = ordnot.getDossierNumber();
				ah.assertEquals("ordnotDossierNumber ", ordnotDossierNumber, postOrderImportsRequest.getDosNr());
				logger.debug("DossierNumber: " + ordnotDossierNumber);
				String ordnotComments = ordnot.getComments();
				logger.debug("Comments: " + ordnotComments);
				//ah.processAllAssertions();
			} else if (vinnot != null) {
				if(vinnot.getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
					logger.debug("vinnot: " + vinnot.toString());
					String vinnotTransactionCode = vinnot.getTransactionCode();
					ah.assertEquals("vinnotTransactionCode ", vinnotTransactionCode, "VEHICLE_IN");
					logger.debug("TransactionCode: " + vinnotTransactionCode);
					String vinnotNotificationDate = vinnot.getNotificationDate();
					logger.debug("NotificationDate Test: " + vinnotNotificationDate);
					//ah.assertEquals("vinnotNotificationDate ", vinnotNotificationDate, dateToday);
					logger.debug("NotificationDate: " + vinnotNotificationDate);
					String vinnotDossierNumber = vinnot.getDossierNumber();
					//ah.assertEquals("vinnotDossierNumber ", vinnotDossierNumber, postOrderImportsRequest.getDosNr());
					logger.debug("DossierNumber: " + vinnotDossierNumber);
					String vinnotVinNumber = vinnot.getVinNumber();
					ah.assertEquals("vinnotVinNumber ", vinnotVinNumber, orderPage.getVIN());
					logger.debug("VinNumber: " + vinnotVinNumber);
					if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
						String vinnotVinControlNumber = vinnot.getVinControlNumber();
						ah.assertEquals("vinnotVinControlNumber ", vinnotVinControlNumber, null);
						logger.debug("VinControlNumber: " + vinnotVinControlNumber);
						String vinnotWeight = vinnot.getWeight();
						ah.assertEquals("vinnotWeight ", vinnotWeight, null);
						logger.debug("vinnotWeight: " + vinnotWeight);
						String vinnotCylinderContent = vinnot.getCylinderContent();
						ah.assertEquals("vinnotCylinderContent ", vinnotCylinderContent, null);
						logger.debug("vinnotCylinderContent: " + vinnotCylinderContent);
						String vinnotPower = vinnot.getPower();
						ah.assertEquals("vinnotPower ", vinnotPower, null);
						logger.debug("vinnotPower: " + vinnotPower);
					} else if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
						String vinnotVinControlNumber = vinnot.getVinControlNumber();
						ah.assertEquals("vinnotVinControlNumber ", vinnotVinControlNumber, orderPage.getVINControlNumber());
						logger.debug("VinControlNumber: " + vinnotVinControlNumber);
						String vinnotWeight = vinnot.getWeight();
						ah.assertEquals("vinnotWeight ", vinnotWeight, orderPage.getMaxWeight());
						logger.debug("vinnotWeight: " + vinnotWeight);
						String vinnotCylinderContent = vinnot.getCylinderContent();
						ah.assertEquals("vinnotCylinderContent ", vinnotCylinderContent, orderPage.getEngineDisplacement());
						logger.debug("vinnotCylinderContent: " + vinnotCylinderContent);
						String vinnotPower = vinnot.getPower();
						ah.assertEquals("vinnotPower ", vinnotPower, orderPage.getPower());
						logger.debug("vinnotPower: " + vinnotPower);
					} else {
						String vinnotVinControlNumber = vinnot.getVinControlNumber();
						ah.assertEquals("vinnotVinControlNumber ", vinnotVinControlNumber, orderPage.getVINControlNumber());
						logger.debug("VinControlNumber: " + vinnotVinControlNumber);
						String vinnotWeight = vinnot.getWeight();
						ah.assertEquals("vinnotWeight ", vinnotWeight, postOrderImportsRequest.getVehicleWeight());
						logger.debug("vinnotWeight: " + vinnotWeight);
						String vinnotCylinderContent = vinnot.getCylinderContent();
						ah.assertEquals("vinnotCylinderContent ", vinnotCylinderContent, postOrderImportsRequest.getVehicleCylinderContent());
						logger.debug("vinnotCylinderContent: " + vinnotCylinderContent);
						String vinnotPower = vinnot.getPower();
						ah.assertEquals("vinnotPower ", vinnotPower, postOrderImportsRequest.getVehiclePower());
						logger.debug("vinnotPower: " + vinnotPower);
					}
				}else {

				}

				String vinnotCo2Emissions = vinnot.getCo2Emissions();
				//ah.assertEquals("vinnotCo2Emissions ", vinnotCo2Emissions, postOrderImportsRequest.getVehicleCo2Emissions());
				logger.debug("vinnotCo2Emissions: " + vinnotCo2Emissions);
				String vinnotComments = vinnot.getComments();
				logger.debug("vinnotComments: " + vinnotComments);
				String vinnotDateFirstRegistration = vinnot.getDateFirstRegistration();
				//ah.assertEquals("vinnotDateFirstRegistration ", vinnotDateFirstRegistration, orderPage.getVehicleCo2Emissions());
				logger.debug("vinnotDateFirstRegistration: " + vinnotDateFirstRegistration);
				String vinnotDateLastRegistration = vinnot.getDateLastRegistration();
				//ah.assertEquals("vinnotDateLastRegistration ", vinnotDateLastRegistration, orderPage.getVehicleCo2Emissions());
				logger.debug("vinnotDateLastRegistration: " + vinnotDateLastRegistration);
				String vinnotFuel = vinnot.getFuel();
				//ah.assertEquals("vinnotFuel ", vinnotFuel, postOrderImportsRequest.);
				logger.debug("vinnotFuel: " + vinnotFuel);
				String vinnotLicensePlateNumber = vinnot.getLicensePlateNumber();
				//ah.assertEquals("vinnotLicensePlateNumber ", vinnotLicensePlateNumber, orderPage.getLicensePlate());
				logger.debug("vinnotLicensePlateNumber: " + vinnotLicensePlateNumber);
				ah.processAllAssertions();
			} else if (licnot != null) {
				if(licnot.getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
					String licnotTransactionCode = licnot.getTransactionCode();
					ah.assertEquals("licnotTransactionCode ", licnotTransactionCode, "LICENSE_PLATE");
					logger.debug("licnotTransactionCode: " + licnotTransactionCode);
					String licnotNotificationDate = licnot.getNotificationDate();
					ah.assertEquals("licnotNotificationDate ", licnotNotificationDate, dateToday);
					logger.debug("licnotNotificationDate: " + licnotNotificationDate);
					String licnotDossierNumber = licnot.getDossierNumber();
					ah.assertEquals("licnotDossierNumber ", licnotDossierNumber, postOrderImportsRequest.getDosNr());
					logger.debug("licnotDossierNumber: " + licnotDossierNumber);
					String licnotLicensePlateNumber = licnot.getLicensePlateNumber();
					ah.assertEquals("licnotLicensePlateNumber ", licnotLicensePlateNumber, orderPage.getLicensePlate());
					logger.debug("licnotLicensePlateNumber: " + licnotLicensePlateNumber);
					String licnotCo2Emissions = licnot.getCo2Emissions();
					//Assert gaat fout omdat Co2Emissions wordt overschreven met WLTP waarde
					//ah.assertEquals("licnotCo2Emissions ", licnotCo2Emissions, postOrderImportsRequest.getVehicleCo2Emissions());
					logger.debug("licnotCo2Emissions: " + licnotCo2Emissions);
					String licnotDateFirstRegistration = licnot.getDateFirstRegistration();
					ah.assertEquals("licnotDateFirstRegistration ", licnotDateFirstRegistration, orderPage.getDateFirstRegistration());
					logger.debug("licnotDateFirstRegistration: " + licnotDateFirstRegistration);
					//ah.processAllAssertions();
				} else {

				}
			} else if (delnot != null) {
				logger.debug("Delivered notification" + delnot.toString());
				if (delnot.getDossierNumber().equals(postOrderImportsRequest.getDosNr())) {
					if (delnot.isDigitalDelivery()) {
						String delnotTransactionCode = delnot.getTransactionCode();
						ah.assertEquals("delnotTransactionCode ", delnotTransactionCode, "DELIVERED");
						//logger.debug("delnotTransactionCode: " + delnotTransactionCode);
						String delnotNotificationDate = delnot.getNotificationDate();
						ah.assertEquals("delnotNotificationDate ", delnotNotificationDate, dateToday);
						//logger.debug("delnotNotificationDate: " + delnotNotificationDate);
						String delnotDossierNumber = delnot.getDossierNumber();
						ah.assertEquals("delnotDossierNumber ", delnotDossierNumber, postOrderImportsRequest.getDosNr());
						//logger.debug("delnotDossierNumber: " + delnotDossierNumber);
						String delnotCo2Emissions = delnot.getCo2Emissions();
						//Assert gaat fout omdat Co2Emissions wordt overschreven met WLTP waarde
						//ah.assertEquals("delnotCo2Emissions ", delnotCo2Emissions, postOrderImportsRequest.getVehicleCo2Emissions());
						//logger.debug("delnotCo2Emissions: " + delnotCo2Emissions);
						String delnotDateFirstRegistration = delnot.getDateFirstRegistration();
						ah.assertEquals("delnotDateFirstRegistration ", delnotDateFirstRegistration, orderPage.getDateFirstRegistration());
						//logger.debug("delnotDateFirstRegistration: " + delnotDateFirstRegistration);
						Integer delnotMileage = delnot.getMileage();
						ah.assertEquals("delnotMileage ", delnotMileage, Integer.parseInt(orderPage.getMileage()));
						//logger.debug("delnotMileage: " + delnotMileage);
						Integer delnotConstructionYear = delnot.getConstructionYear();
						ah.assertEquals("delnotConstructionYear ", delnotConstructionYear, Integer.parseInt(orderPage.getModelYear()));
						//logger.debug("delnotConstructionYear: " + delnotConstructionYear);
						String delnotKeyCode = delnot.getKeyCode();
						ah.assertEquals("delnotKeyCode ", delnotKeyCode, orderPage.getKeycode());
						//logger.debug("delnotKeyCode: " + delnotKeyCode);
						String delnotStartCode = delnot.getStartCode();
						ah.assertEquals("delnotStartCode ", delnotStartCode, orderPage.getStartcode());
						//logger.debug("delnotStartCode: " + delnotStartCode);
						Boolean delnotPreviousVehicleDropoff = delnot.isPreviousVehicleDropoff();
						ah.assertEquals("delnotPreviousVehicleDropoff ", delnotPreviousVehicleDropoff, Boolean.parseBoolean(orderPage.getReturnedVehicle()));
						//logger.debug("delnotPreviousVehicleDropoff: " + delnotPreviousVehicleDropoff);
						String delnotPreviousVehicleLicensePlate = delnot.getPreviousVehicleLicensePlate();
						ah.assertEquals("delnotPreviousVehicleLicensePlate ", delnotPreviousVehicleLicensePlate, "1FFF333");
						//logger.debug("delnotPreviousVehicleLicensePlate: " + delnotPreviousVehicleLicensePlate);
						String delnotPreviousVehicleOwner = delnot.getPreviousVehicleOwner();
						ah.assertEquals("delnotPreviousVehicleOwner ", delnotPreviousVehicleOwner, "ALD Automotive");
						//logger.debug("delnotPreviousVehicleOwner: " + delnotPreviousVehicleOwner);
						String delnotActualDeliveryDate = delnot.getActualDeliveryDate();
						ah.assertEquals("delnotActualDeliveryDate ", delnotActualDeliveryDate, dateToday);
						//logger.debug("delnotActualDeliveryDate: " + delnotActualDeliveryDate);
						Boolean delnotDigitalDelivery = delnot.isDigitalDelivery();
						ah.assertEquals("delnotDigitalDelivery ", delnotDigitalDelivery, true);
						//logger.debug("delnotDigitalDelivery: " + delnotDigitalDelivery);
						String delnotDriverFirstName = delnot.getDriverFirstName();
						ah.assertEquals("delnotDriverFirstName ", delnotDriverFirstName, "Adje");
						//logger.debug("delnotDriverFirstName: " + delnotDriverFirstName);
						String delnotDriverLastName = delnot.getDriverLastName();
						ah.assertEquals("delnotDriverLastName ", delnotDriverLastName, "Antonisse");
						//logger.debug("delnotDriverLastName: " + delnotDriverLastName);
						String delnotDriverBirthDate = delnot.getDriverBirthDate();
						ah.assertEquals("delnotDriverBirthDate ", delnotDriverBirthDate, "1965-06-19");
						//logger.debug("delnotDriverBirthDate: " + delnotDriverBirthDate);
						String delnotDriverBirthPlace = delnot.getDriverBirthPlace();
						ah.assertNull("delnotDriverBirthPlace ", delnotDriverBirthPlace);
						//logger.debug("delnotDriverBirthPlace: " + delnotDriverBirthPlace);
						Boolean delnotRegistrationCertificate = delnot.isRegistrationCertificate();
						ah.assertEquals("delnotRegistrationCertificate ", delnotRegistrationCertificate, Boolean.parseBoolean(orderPage.getSelectRegistrationCertificate()));
						//logger.debug("delnotRegistrationCertificate: " + delnotRegistrationCertificate);
						Boolean delnotInsuranceCertificate = delnot.isInsuranceCertificate();
						ah.assertEquals("delnotInsuranceCertificate ", delnotInsuranceCertificate, Boolean.parseBoolean(orderPage.getSelectInsuranceCertificate()));
						//logger.debug("delnotInsuranceCertificate: " + delnotInsuranceCertificate);
						Boolean delnotCertificateOfConformity = delnot.isCertificateOfConformity();
						ah.assertEquals("delnotCertificateOfConformity ", delnotCertificateOfConformity, Boolean.parseBoolean(orderPage.getSelectCertificateOfConformity()));
						//logger.debug("delnotCertificateOfConformity: " + delnotCertificateOfConformity);
						Boolean delnotTechnicalInspectionCertificate = delnot.isTechnicalInspectionCertificate();
						ah.assertEquals("delnotTechnicalInspectionCertificate ", delnotTechnicalInspectionCertificate, Boolean.parseBoolean(orderPage.getSelectTechnicalInspectionCertificate()));
						//logger.debug("delnotTechnicalInspectionCertificate: " + delnotTechnicalInspectionCertificate);
						Boolean delnotLegalKit = delnot.isLegalKit();
						ah.assertEquals("delnotLegalKit ", delnotLegalKit, Boolean.parseBoolean(orderPage.getSelectLegalKit()));
						//logger.debug("delnotLegalKit: " + delnotLegalKit);
						Boolean delnotServiceManual = delnot.isServiceManual();
						ah.assertEquals("delnotServiceManual ", delnotServiceManual, Boolean.parseBoolean(orderPage.getSelectServiceManual()));
						//logger.debug("delnotServiceManual: " + delnotServiceManual);
						Boolean delnotFuelCard = delnot.isFuelCard();
						ah.assertEquals("delnotFuelCard ", delnotFuelCard, Boolean.parseBoolean(orderPage.getSelectFuelCard()));
						//logger.debug("delnotFuelCard: " + delnotFuelCard);
						String delnotNumberOfKeys = delnot.getNumberOfKeys();
						ah.assertEquals("delnotNumberOfKeys ", delnotNumberOfKeys.replace(",00", ""), orderPage.getNumberOfKeys().replace(",00", ""));
						//logger.debug("delnotNumberOfKeys: " + delnotNumberOfKeys);
						String delnotTyreSpecification = delnot.getTyreSpecification();
						ah.assertEquals("delnotTyreSpecification ", delnotTyreSpecification, orderPage.getTyreSpecs());
						//logger.debug("delnotTyreSpecification: " + delnotTyreSpecification);
						String delnotTyreBrand = delnot.getTyreBrand();
						ah.assertEquals("delnotTyreBrand ", delnotTyreBrand, orderPage.getTyreBrand());
						//logger.debug("delnotTyreBrand: " + delnotTyreBrand);
						String delnotTyreType = delnot.getTyreType();
						ah.assertEquals("delnotTyreType ", delnotTyreType, orderPage.getTyreType());
						//logger.debug("delnotTyreType: " + delnotTyreType);
						//String delnotExteriorColor = delnot.getExteriorColor().getDutch();
						ah.assertEquals("delnotExteriorColor ", delnot.getExteriorColor().getDutch(), postOrderImportsRequest.getVehicleExteriorColor());
						//logger.debug("delnotExteriorColor: " + delnotExteriorColor);
						//String delnotInteriorColor = delnot.getInteriorColor().getDutch();
						ah.assertEquals("delnotInteriorColor ", delnot.getInteriorColor().getDutch(), postOrderImportsRequest.getVehicleInteriorColor());
						//logger.debug("delnotInteriorColor: " + delnotInteriorColor);

					} else {
						String delnotTransactionCode = delnot.getTransactionCode();
						ah.assertEquals("delnotTransactionCode ", delnotTransactionCode, "DELIVERED");
						//logger.debug("delnotTransactionCode: " + delnotTransactionCode);
						String delnotNotificationDate = delnot.getNotificationDate();
						ah.assertEquals("delnotNotificationDate ", delnotNotificationDate, dateToday);
						//logger.debug("delnotNotificationDate: " + delnotNotificationDate);
						String delnotDossierNumber = delnot.getDossierNumber();
						ah.assertEquals("delnotDossierNumber ", delnotDossierNumber, postOrderImportsRequest.getDosNr());
						//logger.debug("delnotDossierNumber: " + delnotDossierNumber);
						String delnotCo2Emissions = delnot.getCo2Emissions();
						//Assert gaat fout omdat Co2Emissions wordt overschreven met WLTP waarde
						//ah.assertEquals("delnotCo2Emissions ", delnotCo2Emissions, postOrderImportsRequest.getVehicleCo2Emissions());
						//logger.debug("delnotCo2Emissions: " + delnotCo2Emissions);
						String delnotDateFirstRegistration = delnot.getDateFirstRegistration();
						ah.assertEquals("delnotDateFirstRegistration ", delnotDateFirstRegistration, orderPage.getDateFirstRegistration());
						//logger.debug("delnotDateFirstRegistration: " + delnotDateFirstRegistration);
						Integer delnotMileage = delnot.getMileage();
						ah.assertEquals("delnotMileage ", delnotMileage, Integer.parseInt(orderPage.getMileage()));
						//logger.debug("delnotMileage: " + delnotMileage);
						Integer delnotConstructionYear = delnot.getConstructionYear();
						ah.assertEquals("delnotConstructionYear ", delnotConstructionYear, Integer.parseInt(orderPage.getModelYear()));
						//logger.debug("delnotConstructionYear: " + delnotConstructionYear);
						String delnotKeyCode = delnot.getKeyCode();
						ah.assertEquals("delnotKeyCode ", delnotKeyCode, orderPage.getKeycode());
						//logger.debug("delnotKeyCode: " + delnotKeyCode);
						String delnotStartCode = delnot.getStartCode();
						ah.assertEquals("delnotStartCode ", delnotStartCode, orderPage.getStartcode());
						//logger.debug("delnotStartCode: " + delnotStartCode);

						if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
							Boolean delnotPreviousVehicleDropoff = delnot.isPreviousVehicleDropoff();
							ah.assertEquals("delnotPreviousVehicleDropoff ", delnotPreviousVehicleDropoff, Boolean.FALSE);
						//	logger.debug("delnotPreviousVehicleDropoff: " + delnotPreviousVehicleDropoff);
							String delnotPreviousVehicleLicensePlate = delnot.getPreviousVehicleLicensePlate();
							ah.assertEquals("delnotPreviousVehicleLicensePlate ", delnotPreviousVehicleLicensePlate, null);
						//	logger.debug("delnotPreviousVehicleLicensePlate: " + delnotPreviousVehicleLicensePlate);
							String delnotPreviousVehicleOwner = delnot.getPreviousVehicleOwner();
							ah.assertEquals("delnotPreviousVehicleOwner ", delnotPreviousVehicleOwner, null);
						//	logger.debug("delnotPreviousVehicleOwner: " + delnotPreviousVehicleOwner);
							String delnotTyreSpecification = delnot.getTyreSpecification();
							ah.assertNull("delnotTyreSpecification ", delnotTyreSpecification);
						//	logger.debug("delnotTyreSpecification: " + delnotTyreSpecification);
							String delnotTyreBrand = delnot.getTyreBrand();
							ah.assertNull("delnotTyreBrand ", delnotTyreBrand);
						//	logger.debug("delnotTyreBrand: " + delnotTyreBrand);
							String delnotTyreType = delnot.getTyreType();
							ah.assertNull("delnotTyreType ", delnotTyreType);
						//	logger.debug("delnotTyreType: " + delnotTyreType);
							//String delnotExteriorColor = delnot.getExteriorColor().getDutch();
							//ah.assertEquals("delnotExteriorColor ", delnotExteriorColor, postOrderImportsRequest.getVehicleExteriorColor());
							//ah.assertNull("delnotExteriorColor", delnot.getExteriorColor().getDutch());
						//	logger.debug("delnotExteriorColor: " + delnotExteriorColor);
							//String delnotInteriorColor = delnot.getInteriorColor().getDutch();
							//ah.assertEquals("delnotInteriorColor ", delnotInteriorColor, postOrderImportsRequest.getVehicleInteriorColor());
							//ah.assertNull("delnotInteriorColor", delnot.getInteriorColor().getDutch());
						//	logger.debug("delnotInteriorColor: " + delnotInteriorColor);
							ah.processAllAssertions();
						} else if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
							//String delnotExteriorColor = delnot.getExteriorColor().getDutch();
							ah.assertNull("delnotExteriorColor ", delnot.getExteriorColor());
							//logger.debug("delnotExteriorColor: " + delnotExteriorColor);
							//String delnotInteriorColor = delnot.getInteriorColor().getDutch();
							ah.assertNull("delnotInteriorColor ", delnot.getInteriorColor());
							//logger.debug("delnotInteriorColor: " + delnotInteriorColor);
							ah.processAllAssertions();
						} else {
							Boolean delnotPreviousVehicleDropoff = delnot.isPreviousVehicleDropoff();
							ah.assertEquals("delnotPreviousVehicleDropoff ", delnotPreviousVehicleDropoff, Boolean.parseBoolean(orderPage.getReturnedVehicle()));
							//logger.debug("delnotPreviousVehicleDropoff: " + delnotPreviousVehicleDropoff);
							String delnotPreviousVehicleLicensePlate = delnot.getPreviousVehicleLicensePlate();
							ah.assertEquals("delnotPreviousVehicleLicensePlate ", delnotPreviousVehicleLicensePlate, orderPage.getLicensePlateReturnedVehicle());
							//logger.debug("delnotPreviousVehicleLicensePlate: " + delnotPreviousVehicleLicensePlate);
							String delnotPreviousVehicleOwner = delnot.getPreviousVehicleOwner();
							ah.assertEquals("delnotPreviousVehicleOwner ", delnotPreviousVehicleOwner, orderPage.getOwnerReturnedVehicle());
							//logger.debug("delnotPreviousVehicleOwner: " + delnotPreviousVehicleOwner);
							String delnotTyreSpecification = delnot.getTyreSpecification();
							ah.assertEquals("delnotTyreSpecification ", delnotTyreSpecification, orderPage.getTyreSpecs());
							//logger.debug("delnotTyreSpecification: " + delnotTyreSpecification);
							String delnotTyreBrand = delnot.getTyreBrand();
							ah.assertEquals("delnotTyreBrand ", delnotTyreBrand, orderPage.getTyreBrand());
							//logger.debug("delnotTyreBrand: " + delnotTyreBrand);
							String delnotTyreType = delnot.getTyreType();
							ah.assertEquals("delnotTyreType ", delnotTyreType, orderPage.getTyreType());
							//logger.debug("delnotTyreType: " + delnotTyreType);
							if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
								String delnotExteriorColor = delnot.getExteriorColor().getDutch();
								ah.assertEquals("delnotExteriorColor ", delnotExteriorColor, postOrderImportsRequest.getVehicleExteriorColor());
								//logger.debug("delnotExteriorColor: " + delnotExteriorColor);
								String delnotInteriorColor = delnot.getInteriorColor().getDutch();
								ah.assertEquals("delnotInteriorColor ", delnotInteriorColor, postOrderImportsRequest.getVehicleInteriorColor());
								//logger.debug("delnotInteriorColor: " + delnotInteriorColor);
								ah.processAllAssertions();
							}

							String delnotActualDeliveryDate = delnot.getActualDeliveryDate();
							ah.assertEquals("delnotActualDeliveryDate ", delnotActualDeliveryDate, dateToday);
							//logger.debug("delnotActualDeliveryDate: " + delnotActualDeliveryDate);
							Boolean delnotDigitalDelivery = delnot.isDigitalDelivery();
							ah.assertEquals("delnotDigitalDelivery ", delnotDigitalDelivery, Boolean.parseBoolean(System.getProperty("digitalDelivery")));
							//logger.debug("delnotDigitalDelivery: " + delnotDigitalDelivery);
							String delnotDriverFirstName = delnot.getDriverFirstName();
							ah.assertEquals("delnotDriverFirstName ", delnotDriverFirstName, orderPage.getDriverFirstName());
							//logger.debug("delnotDriverFirstName: " + delnotDriverFirstName);
							String delnotDriverLastName = delnot.getDriverLastName();
							ah.assertEquals("delnotDriverLastName ", delnotDriverLastName, orderPage.getDriverLastName());
							//logger.debug("delnotDriverLastName: " + delnotDriverLastName);
							String delnotDriverBirthDate = delnot.getDriverBirthDate();
							//ah.assertEquals("delnotDriverBirthDate ", delnotDriverBirthDate, orderPage.getDriverDateOfBirth());
							//logger.debug("delnotDriverBirthDate: " + delnotDriverBirthDate);
							String delnotDriverBirthPlace = delnot.getDriverBirthPlace();
							ah.assertEquals("delnotDriverBirthPlace ", delnotDriverBirthPlace, orderPage.getDriverBirthPlace());
							//logger.debug("delnotDriverBirthPlace: " + delnotDriverBirthPlace);
							Boolean delnotRegistrationCertificate = delnot.isRegistrationCertificate();
							ah.assertEquals("delnotRegistrationCertificate ", delnotRegistrationCertificate, Boolean.parseBoolean(orderPage.getSelectRegistrationCertificate()));
							//logger.debug("delnotRegistrationCertificate: " + delnotRegistrationCertificate);
							Boolean delnotInsuranceCertificate = delnot.isInsuranceCertificate();
							ah.assertEquals("delnotInsuranceCertificate ", delnotInsuranceCertificate, Boolean.parseBoolean(orderPage.getSelectInsuranceCertificate()));
							//logger.debug("delnotInsuranceCertificate: " + delnotInsuranceCertificate);
							Boolean delnotCertificateOfConformity = delnot.isCertificateOfConformity();
							ah.assertEquals("delnotCertificateOfConformity ", delnotCertificateOfConformity, Boolean.parseBoolean(orderPage.getSelectCertificateOfConformity()));
							//logger.debug("delnotCertificateOfConformity: " + delnotCertificateOfConformity);
							Boolean delnotTechnicalInspectionCertificate = delnot.isTechnicalInspectionCertificate();
							ah.assertEquals("delnotTechnicalInspectionCertificate ", delnotTechnicalInspectionCertificate, Boolean.parseBoolean(orderPage.getSelectTechnicalInspectionCertificate()));
							//logger.debug("delnotTechnicalInspectionCertificate: " + delnotTechnicalInspectionCertificate);
							Boolean delnotLegalKit = delnot.isLegalKit();
							ah.assertEquals("delnotLegalKit ", delnotLegalKit, Boolean.parseBoolean(orderPage.getSelectLegalKit()));
							//logger.debug("delnotLegalKit: " + delnotLegalKit);
							Boolean delnotServiceManual = delnot.isServiceManual();
							ah.assertEquals("delnotServiceManual ", delnotServiceManual, Boolean.parseBoolean(orderPage.getSelectServiceManual()));
							//logger.debug("delnotServiceManual: " + delnotServiceManual);
							Boolean delnotFuelCard = delnot.isFuelCard();
							ah.assertEquals("delnotFuelCard ", delnotFuelCard, Boolean.parseBoolean(orderPage.getSelectFuelCard()));
							//logger.debug("delnotFuelCard: " + delnotFuelCard);
							String delnotNumberOfKeys = delnot.getNumberOfKeys();
							ah.assertEquals("delnotNumberOfKeys ", delnotNumberOfKeys.replace(",00", ""), orderPage.getNumberOfKeys().replace(",00", ""));
							//logger.debug("delnotNumberOfKeys: " + delnotNumberOfKeys);

							ah.processAllAssertions();


							if (System.getProperty("leasingType").equals("FINANCIAL_LEASE") || System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {

							} else if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
								String delnotExteriorColor = delnot.getExteriorColor().getDutch();
								ah.assertEquals("delnotExteriorColor ", delnotExteriorColor, postOrderImportsRequest.getVehicleExteriorColor());
								//logger.debug("delnotExteriorColor: " + delnotExteriorColor);
								String delnotInteriorColor = delnot.getInteriorColor().getDutch();
								ah.assertEquals("delnotInteriorColor ", delnotInteriorColor, postOrderImportsRequest.getVehicleInteriorColor());
								//logger.debug("delnotInteriorColor: " + delnotInteriorColor);
								ah.processAllAssertions();
							}

							ah.processAllAssertions();

						}
					}


					ah.processAllAssertions();
				}

			}
		}
		logger.debug("End user_validates_order_notifications");
	}

	@Then("^User validates wallbox order notifications$")
	public void user_validates_wallbox_order_notifications() throws InterruptedException, ParseException, JAXBException, jakarta.xml.bind.JAXBException {
		logger.debug("Start validatie");
		JAXBContext jaxbContext;

		jaxbContext = JAXBContext.newInstance(WallBoxOrderNotifications.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		// OrderNotifications orderNotifications = (OrderNotifications)
		// jaxbUnmarshaller.unmarshal(new
		// File("./src/test/resources/runtime/sftpdownloads/order_update_nn.xml"));
		logger.debug("localFileWb: " + localFileWB);
		WallBoxOrderNotifications wallBoxOrderNotifications = (WallBoxOrderNotifications) jaxbUnmarshaller
				.unmarshal(new File(localFileWB));
		logger.debug("wallBoxOrderNotifications: " + wallBoxOrderNotifications);
		getWallBoxOrderNotifications.get_wallbox_order_notifications_data(wallBoxOrderNotifications);
		for (int i = 0; i < wallBoxOrderNotifications.getOrderNotification().size(); i++) {
			apiEngine.model.get.sftp.wallbox.order.notification.CommentsNotification comnot = wallBoxOrderNotifications.getOrderNotification().get(i).getCommentsNotification();
			apiEngine.model.get.sftp.wallbox.order.notification.DealerAcceptedNotification deanot = wallBoxOrderNotifications.getOrderNotification().get(i).getDealerAcceptedNotification();
			apiEngine.model.get.sftp.wallbox.order.notification.OrderedNotification ordnot = wallBoxOrderNotifications.getOrderNotification().get(i).getOrderedNotification();
			apiEngine.model.get.sftp.wallbox.order.notification.NotAcceptedNotification nannot = wallBoxOrderNotifications.getOrderNotification().get(i).getNotAcceptedNotification();
			apiEngine.model.get.sftp.wallbox.order.notification.ReadyToActivateNotification rtanot = wallBoxOrderNotifications.getOrderNotification().get(i).getReadyToActivateNotification();
			apiEngine.model.get.sftp.wallbox.order.notification.ActivatedNotification actnot = wallBoxOrderNotifications.getOrderNotification().get(i).getActivatedNotification();
			apiEngine.model.get.sftp.wallbox.order.notification.InstalledNotification insnot = wallBoxOrderNotifications.getOrderNotification().get(i).getInstalledNotification();
			apiEngine.model.get.sftp.wallbox.order.notification.CancelledNotification cannot = wallBoxOrderNotifications.getOrderNotification().get(i).getCancelledNotification();
			if (comnot != null) {
				logger.debug("CommentsNotification asserts in wallbox comnot asserts: " + comnot.toString());
				logger.debug("wallBoxOrderNotifications.getOrderNotification().get(i): " + wallBoxOrderNotifications.getOrderNotification().get(i).toString());
				wallBoxOrderNotifications.getOrderNotification().get(i).toString();
				//String tc = orderNotifications.getOrderNotification().get(0).getCommentsNotification().getTransactionCode();
				String comnotTransactionCode = comnot.getTransactionCode().value();
				ah.assertEquals("TransactionCode ", comnotTransactionCode, "COMMENTS");
				logger.debug("TransactionCode: " + comnotTransactionCode);
				String comnotNotificationDate = comnot.getNotificationDate();
				ah.assertEquals("NotificationDate ", comnotNotificationDate, dateToday);
				logger.debug("NotificationDate: " + comnotNotificationDate);
				String comnotComments = comnot.getComments();
				logger.debug("Comments: " + comnotComments);
				String comnotDossierNumber = comnot.getDossierNumber();
				ah.assertEquals("DossierNumber ", comnotDossierNumber, postWallBoxImports.getDosNrWB());
				logger.debug("DossierNumber: " + comnotDossierNumber);
				ah.processAllAssertions();
			} else if (deanot != null) {
				logger.debug("deanot: " + deanot.toString());
				if(deanot.getDossierNumber().equals(postWallBoxImports.getDosNrWB())) {
					String deanotTransactionCode = deanot.getTransactionCode().value();
					ah.assertEquals("TransactionCode ", deanotTransactionCode, "DEALER_ACCEPTED");
					logger.debug("TransactionCode: " + deanotTransactionCode);
					String deanotNotificationDate = deanot.getNotificationDate();
					ah.assertEquals("NotificationDate ", deanotNotificationDate, dateToday);
					logger.debug("NotificationDate: " + deanotNotificationDate);
					String deanotDossierNumber = deanot.getDossierNumber();
					ah.assertEquals("DossierNumber ", deanotDossierNumber, postWallBoxImports.getDosNrWB());
					logger.debug("DossierNumber: " + deanotDossierNumber);
					String deanotExpectedInstallationDate = deanot.getExpectedInstallationDate();
					ah.assertEquals("ExpectedInstallationDate ", deanotExpectedInstallationDate, dateToday);
					logger.debug("ExpectedInstallationDate: " + deanotExpectedInstallationDate);
					ah.processAllAssertions();
				}
			} else if (ordnot != null) {
				String ordnotTransactionCode = ordnot.getTransactionCode().value();
				ah.assertEquals("ordnotTransactionCode ", ordnotTransactionCode, "ORDERED");
				logger.debug("TransactionCode: " + ordnotTransactionCode);
				String ordnotNotificationDate = ordnot.getNotificationDate();
				ah.assertEquals("ordnotNotificationDate ", ordnotNotificationDate, dateToday);
				logger.debug("NotificationDate: " + ordnotNotificationDate);
				String ordnotDossierNumber = ordnot.getDossierNumber();
				ah.assertEquals("ordnotDossierNumber ", ordnotDossierNumber, postWallBoxImports.getDosNrWB());
				logger.debug("DossierNumber: " + ordnotDossierNumber);
				//String ordnotComments = ordnot.getComments();
				//logger.debug("Comments: " + ordnotComments);
				//ah.processAllAssertions();
			} else if (nannot != null) {
				logger.debug("TODO: asserts NotAcceptedNotification ");
			} else if (rtanot != null) {
				logger.debug("TODO: asserts ReadyToActivateNotification ");
			} else if (actnot != null) {
				logger.debug("TODO: asserts ActivatedNotification ");
			} else if (insnot != null) {
				logger.debug("TODO: asserts InstalledNotification ");
			} else if (cannot != null) {
				logger.debug("TODO: asserts CancelledNotification ");
			}

			ah.processAllAssertions();

		}
	}
}


			//ah.processAllAssertions();





