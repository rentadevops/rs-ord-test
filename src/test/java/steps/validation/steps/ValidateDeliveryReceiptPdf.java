package steps.validation.steps;

import apiEngine.Endpoints;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import cucumber.TestContext;
import enums.*;
import helpers.AssertHelper;
import io.cucumber.java.en.Then;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.commons.codec.Charsets;
//import org.apache.commons.io.Charsets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.fontbox.cmap.CMap;
//import org.apache.fontbox.ttf.CmapLookup;
import org.apache.fontbox.ttf.TTFParser;
import org.apache.fontbox.ttf.TrueTypeFont;
//import org.apache.fontbox.ttf.model.GsubData;
import org.apache.fontbox.util.BoundingBox;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
//import org.apache.pdfbox.util.Charsets;
//import org.apache.pdfbox.util.Charsets;
//import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import pageobjects.android.ReturnedVehicle;
import pageobjects.api.API_GetLeasingCompanyActorById;
import pageobjects.api.API_GetSupplierActorById;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.github.romankh3.image.comparison.model.ImageComparisonState.MATCH;
import static com.github.romankh3.image.comparison.model.ImageComparisonState.MISMATCH;
//import static org.apache.tika.utils.DateUtils.formatDate;

public class ValidateDeliveryReceiptPdf {
	final Logger logger = LogManager.getLogger(ValidateDeliveryReceiptPdf.class);

	TestContext testContext;
	ORD_Order_Details_Page orderDetailsPage;
	ORD_Order_Page orderPage;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	API_GetSupplierActorById getSupplierActorById;
	AssertHelper ah = new AssertHelper();
	private PDDocument doc;


	String title;
	String leasingCompanyDossierNumber;
	String dataClient;
	String dataDriver;
	String clientNameLabel;
	String clientStreetLabel;
	String clientPostalCodeLabel;
	String clientName;
	String clientStreet;
	String driverNameLabel;
	String driverStreetLabel;
	String driverPostalCodeLabel;
	String driverMobileLabel;
	String driverEmailLabel;
	String driverNamePdf;
	String driverStreet;
	String driverPostalCode;
	String driverMobile;
	String driverEmail;
	String dataVehicle;
	String vehicleBrandLabel;
	String vehicleModelLabel;
	String vehicleVersionLabel;
	String vehicleExtColorLabel;
	String vehicleIntColorLabel;
	String vehicleTyresLabel;
	String vehicleVinLabel;
	String vehicleLicensePlateLabel;
	String vehicleModel;
	String vehicleVersion;
	String vehicleExtColor;
	String vehicleVin;
	String vehicleLicensePlate;
	String extraProofOfRegistration;
	String extraProofOfInsurance;
	String extraCertificateOfConformity;
	String extraProofOfRoadworthiness;
	String extraLegalKit;
	String extraManual;
	String extraFuelCard;
	String extraNumberOfKeysLabel;
	String extraNumberOfKeys;
	String dataPreviousVehicle;
	String previousVehicleLicensePlateLabel;
	String previousVehicleOwnerLabel;
	String previousVehicleLicensePlate;
	String previousVehicleOwner;
	String dataSignature;
	String vehicleBrand;
	String extraProofOfRegistrationLabel;
	String extraProofOfInsuranceLabel;
	String extraCertificateOfConformityLabel;
	String extraProofOfRoadworthinessLabel;
	String extraLegalKitLabel;
	String extraManualLabel;
	String extraFuelCardLabel;
	String vehicleIntColor;
	String vehicleTyres;
	String previousVehicle;
	String dataDeliveryLocation;
	String deliveryLocationNameLabel;
	String deliveryLocationStreetLabel;
	String deliveryLocationPostalCodeLabel;
	String deliveryLocationName;
	String deliveryLocationStreet;
	String deliveryLocationPostalCode;
	String dataExtra;
	String clientCityLabel;
	String clientPostalCode;
	String clientCity;
	String driverCityLabel;
	String driverCity;
	String deliveryLocationCityLabel;
	String deliveryLocationCity;
	String signatureDescriptionLine1;
	String signatureDescriptionLine2;
	String financialLeaseSignatureDisclaimer;
	String dummyFieldName1;

	Boolean boolPdfPrevVeh;
	String locAddress;
	Boolean boolOrderReg;
	Boolean boolPdfReg;
	Boolean boolPdfInsurance;
	Boolean boolOrderInsurance;
	Boolean boolPdfCoc;
	Boolean boolOrderCoc;
	Boolean boolPdfTec;
	Boolean boolOrderTec;
	Boolean boolPdfKit;
	Boolean boolOrderKit;
	Boolean boolPdfManual;
	Boolean boolOrderManual;
	Boolean boolPdfFuelCard;
	Boolean boolOrderFuelCard;
	Boolean boolOrderPrevVeh;
	String[] locStreetTmp;
	String locStreet;
	String[] locPostalCodeTmp;
	String locPostalCodeTmp1;
	String[] locPostalCodeTmp2;
	String locPostalCode;

	List<String> textLines;





	public ValidateDeliveryReceiptPdf(TestContext context) throws InterruptedException, IOException {
		 testContext = context;
		 orderDetailsPage = testContext.getPageObjectManager().getORD_Order_Details_Page();
		 orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		 postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		 getLeasingCompanyActorById = testContext.getPageObjectManager().getAPI_GetLeasingCompanyActorById();
		 getSupplierActorById = testContext.getPageObjectManager().getAPI_GetSupplierActorById();
		 ReturnedVehicle returnedVehicle;

	}

	public void validate_delivery_receipt_pdf_options(PDDocument doc) {
		logger.debug("Methode under construction");
		PDDocumentCatalog cat = doc.getDocumentCatalog();
		ah.assertEquals("Number of pages", doc.getNumberOfPages(), 2);
		doc.getPage(1);

	}

	//@Then("^User validates orderdetails are correct$")
	public void validate_delivery_receipt_pdf(PDDocument doc) throws InterruptedException, ParseException, IOException {

		PDFTextStripper stripper = new PDFTextStripper();
		stripper.setStartPage(1); // Start extracting from page 3
		stripper.setEndPage(2); // Extract till page 5
		PDDocumentCatalog cat = doc.getDocumentCatalog();
		ah.assertEquals("Number of pages", doc.getNumberOfPages(), 2);
		doc.getPage(0);
		String text = stripper.getText(doc);
		System.out.println(text);
		String pdfFileInText = stripper.getText(doc);
		String lines[] = pdfFileInText.split("\\r?\\n");
		List<String> actText = new ArrayList<String>();
		for(String line:lines){
			System.out.println(line);
			actText.add(line);
		}

			//FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/actText.txt");


		FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/actText.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(actText);
		oos.close();
		//Closing the document
		//doc.close();
		if(System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			textLines = Files.readAllLines(
					new File("./src/test/resources/pdftextFinancialLeaseEngels.txt").toPath(), Charsets.UTF_8);
			logger.debug("eerste regel textlines en: " + textLines.get(0));
		} else {
			if (System.getProperty("language").equals("fr")) {
				textLines = Files.readAllLines(
						new File("./src/test/resources/pdftextFrans.txt").toPath(), Charsets.UTF_8);
				logger.debug("eerste regel textlines fr: " + textLines.get(0));
			} else {
				textLines = Files.readAllLines(
						new File("./src/test/resources/pdftextEngels.txt").toPath(), Charsets.UTF_8);
				logger.debug("eerste regel textlines en: " + textLines.get(0));
			}
		}

		List<String> textLabels = Files.readAllLines(
				new File("./src/test/resources/pdftextLabelsEngels.txt").toPath(), Charsets.UTF_8 );

		logger.debug("pdftext 1e regel" + textLines.get(0));
			logger.debug("size: " + textLines.size());

		Date date = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String today = formatter.format(date);
		//StringBuffer content = new StringBuffer();
		//PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
		//PDAcroForm acroForm = docCatalog.getAcroForm();
		//content.append("\n");
		//List<PDField> fields = acroForm.getFields();
		//logger.debug("{} top-level fields were found on the form", fields.size());
		String lang = System.getProperty("pdfLanguage");

		if(lang.equals("en")) {
			if(System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
				ah.assertEquals("Title", actText.get(0), textLines.get(0));
				ah.assertEquals("Dossiernumber", actText.get(1), postOrderImportsRequest.getDosNr());
				ah.assertEquals("Lessee", actText.get(2), textLines.get(2));
				ah.assertEquals("Name", actText.get(3), "Name " + postOrderImportsRequest.getClientName());
				ah.assertEquals("Street", actText.get(4), textLines.get(4));
				ah.assertEquals("Postal code", actText.get(5), textLines.get(5));
				ah.assertEquals("City", actText.get(6), textLines.get(6));
				ah.assertEquals("Lessee enterpriseNumber", actText.get(7), "Enterprise No. " + postOrderImportsRequest.getClientVAT());
				ah.assertEquals("Kop vehicle", actText.get(8), textLines.get(8));
				ah.assertEquals("Merk", actText.get(9), "Make " + postOrderImportsRequest.getVehicleBrand());
				ah.assertEquals("Model", actText.get(10), "Model " + postOrderImportsRequest.getVehicleModel());
				ah.assertEquals("Version", actText.get(11), textLines.get(11));
				ah.assertEquals("VIN", actText.get(12), "VIN " + orderPage.getVIN());
				ah.assertEquals("Colour", actText.get(13), textLines.get(13));
				ah.assertEquals("Interior", actText.get(14), textLines.get(14));
				ah.assertEquals("Tyres", actText.get(15), "Tyres " + orderPage.getTyreSpecs() + "License plate " + orderPage.getLicensePlate());
				ah.assertEquals("Kop delivery address", actText.get(16), textLines.get(16));
				ah.assertEquals("Name", actText.get(17), "Name " + postOrderImportsRequest.getDeliveryLocationName());
				ah.assertEquals("Street", actText.get(18), "Street " + postOrderImportsRequest.getDeliveryStreetHouseNumber());
				ah.assertEquals("Postal code", actText.get(19), "Postal code " + postOrderImportsRequest.getDeliveryPostalCode());
				ah.assertEquals("City", actText.get(20), "City " + postOrderImportsRequest.getDeliveryCity());
				ah.assertEquals("Lessee enterpriseNumber", actText.get(21), textLines.get(21));
				ah.assertEquals("Kop returned vehicle", actText.get(22), textLines.get(22));
				ah.assertEquals("License plate returned vehicle", actText.get(23), "License plate returned vehicle 1FFF333");
				ah.assertEquals("Vehicle registered on the name of", actText.get(24), "Vehicle registered on the name of ALD Automotive");
				ah.assertEquals("Kop Additional equipment and enclosed documents", actText.get(25), textLines.get(25));
				//ah.assertEquals("Registration certificateand Legal kit", actText.get(26), textLines.get(26));
				//ah.assertEquals("Insurance certificate and Service manual", actText.get(27), textLines.get(27));
				//ah.assertEquals("Fuel card and Certificate of conformity", actText.get(28), textLines.get(28));
				//ah.assertEquals("Technical inspection certificate and Number of keys", actText.get(29), textLines.get(29));
				ah.assertEquals("Kop Signature", actText.get(30), textLines.get(30));
				ah.assertEquals("Signature regel1", actText.get(31), "The undersigned Adje Antonisse, born on " + orderPage.getDriverDateOfBirth() + ",");
				ah.assertEquals("Signature regel2", actText.get(32), "declares to have received the vehicle described here on " + today + ".");
				ah.assertEquals("Signature regel3", actText.get(33), textLines.get(33));
				ah.assertEquals("Signature regel4", actText.get(34), textLines.get(34));
				ah.assertEquals("Kop Options", actText.get(35), textLines.get(35));
				//for (int i = 34; i < actText.size() - 1; i++) {
				//	ah.assertEquals("Options", actText.get(i), textLines.get(i));
				//}
				logger.debug("Assertions Financial Lease Engels: " + ah.processAllAssertions());

			} else {
				ah.assertEquals("Title", actText.get(0), textLines.get(0));
				ah.assertEquals("Dossiernumber", actText.get(1), postOrderImportsRequest.getDosNr());
				ah.assertEquals("Lessee", actText.get(2), textLines.get(2));
				ah.assertEquals("Name", actText.get(3), "Name " + postOrderImportsRequest.getClientName());
				ah.assertEquals("Street", actText.get(4), "Street " + postOrderImportsRequest.getClientStreetHouseNumber());
				ah.assertEquals("Postal code", actText.get(5), "Postal code " + postOrderImportsRequest.getClientPostalCode());
				ah.assertEquals("City", actText.get(6), "City " + postOrderImportsRequest.getClientCity());
				ah.assertEquals("Kop vehicle", actText.get(7), textLines.get(7));
				ah.assertEquals("Merk", actText.get(8), "Make " + postOrderImportsRequest.getVehicleBrand());
				ah.assertEquals("Model", actText.get(9), "Model " + postOrderImportsRequest.getVehicleModel());
				ah.assertEquals("Version", actText.get(10), "Version " + postOrderImportsRequest.getVehicleVersion());
				ah.assertEquals("VIN", actText.get(11), "VIN " + orderPage.getVIN());
				ah.assertEquals("Colour", actText.get(12), "Colour " + postOrderImportsRequest.getVehicleExteriorColor());
				ah.assertEquals("Interior", actText.get(13), "Interior " + postOrderImportsRequest.getVehicleInteriorColor());
				ah.assertEquals("Tyres", actText.get(14), "Tyres " + orderPage.getTyreSpecs() + "License plate " + orderPage.getLicensePlate());
				ah.assertEquals("Kop delivery address", actText.get(15), textLines.get(15));
				ah.assertEquals("Name", actText.get(16), "Name " + postOrderImportsRequest.getDeliveryLocationName());
				ah.assertEquals("Street", actText.get(17), "Street " + postOrderImportsRequest.getDeliveryStreetHouseNumber());
				ah.assertEquals("Postal code", actText.get(18), "Postal code " + postOrderImportsRequest.getDeliveryPostalCode());
				ah.assertEquals("City", actText.get(19), "City " + postOrderImportsRequest.getDeliveryCity());
				ah.assertEquals("Kop returned vehicle", actText.get(20), textLines.get(20));
				ah.assertEquals("License plate returned vehicle", actText.get(21), "License plate returned vehicle 1FFF333");
				ah.assertEquals("Vehicle registered on the name of", actText.get(22), "Vehicle registered on the name of ALD Automotive");
				ah.assertEquals("Kop Additional equipment and enclosed documents", actText.get(23), textLines.get(23));
				ah.assertEquals("Registration certificateand Legal kit", actText.get(24), textLines.get(24));
				ah.assertEquals("Insurance certificate and Service manual", actText.get(25), textLines.get(25));
				ah.assertEquals("Fuel card and Certificate of conformity", actText.get(26), textLines.get(26));
				ah.assertEquals("Technical inspection certificate and Number of keys", actText.get(27), textLines.get(27));
				ah.assertEquals("Kop Signature", actText.get(28), textLines.get(28));
				ah.assertEquals("Signature regel1", actText.get(29), "The undersigned Adje Antonisse, born on " + orderPage.getDriverDateOfBirth() + ",");
				ah.assertEquals("Signature regel2", actText.get(30), "declares to have received the vehicle described here on " + today + ".");
				ah.assertEquals("Signature regel3", actText.get(31), textLines.get(31));
				ah.assertEquals("Signature regel4", actText.get(32), textLines.get(32));
				ah.assertEquals("Kop Options", actText.get(33), textLines.get(33));
				for (int i = 34; i < actText.size() - 1; i++) {
					ah.assertEquals("Options", actText.get(i), textLines.get(i));
				}
				logger.debug("Assertions: " + ah.processAllAssertions());
			}
		}else if(lang.equals("fr")) {
			ah.assertEquals("Title", actText.get(0), textLines.get(0));
			ah.assertEquals("Dossiernumber", actText.get(1), postOrderImportsRequest.getDosNr());
			ah.assertEquals("Lessee", actText.get(2), textLines.get(2));
			ah.assertEquals("Name", actText.get(3), "Nom " + postOrderImportsRequest.getClientName());
			ah.assertEquals("Street", actText.get(4), "Rue " + postOrderImportsRequest.getClientStreetHouseNumber());
			ah.assertEquals("Postal code", actText.get(5), "Code postal " + postOrderImportsRequest.getClientPostalCode());
			ah.assertEquals("City", actText.get(6), "Ville " + postOrderImportsRequest.getClientCity());
			ah.assertEquals("Kop vehicle", actText.get(7), textLines.get(7));
			ah.assertEquals("Merk", actText.get(8), "Marque " + postOrderImportsRequest.getVehicleBrand());
			ah.assertEquals("Model", actText.get(9), "Modèle " + postOrderImportsRequest.getVehicleModel());
			ah.assertEquals("Version", actText.get(10), "Version " + postOrderImportsRequest.getVehicleVersion());
			ah.assertEquals("VIN", actText.get(11), "Numéro châssis " + orderPage.getVIN());
			ah.assertEquals("Colour", actText.get(12), "Couleur ext. " + postOrderImportsRequest.getVehicleExteriorColor());
			ah.assertEquals("Interior", actText.get(13), "Couleur int. " + postOrderImportsRequest.getVehicleInteriorColor());
			ah.assertEquals("Tyres", actText.get(14), "Pneus " + orderPage.getTyreSpecs() + "Plaque");
			ah.assertEquals("Kop license plate", actText.get(15), textLines.get(15));
			ah.assertEquals("License plate", actText.get(16), orderPage.getLicensePlate());
			ah.assertEquals("Kop delivery address", actText.get(17), textLines.get(17));
			ah.assertEquals("Name", actText.get(18), "Nom " + postOrderImportsRequest.getDeliveryLocationName());
			ah.assertEquals("Street", actText.get(19), "Rue " + postOrderImportsRequest.getDeliveryStreetHouseNumber());
			ah.assertEquals("Postal code", actText.get(20), "Code postal " + postOrderImportsRequest.getDeliveryPostalCode());
			ah.assertEquals("City", actText.get(21), "Ville " + postOrderImportsRequest.getDeliveryCity());
			ah.assertEquals("Kop returned vehicle", actText.get(22), textLines.get(22));
			ah.assertEquals("License plate returned vehicle", actText.get(23), "Plaque immatriculation véhicule rentrant 1FFF333");
			ah.assertEquals("Vehicle registered on the name of", actText.get(24), "Véhicule enregistré au nom de ALD Automotive");
			ah.assertEquals("Kop Additional equipment and enclosed documents", actText.get(25), textLines.get(25));
			ah.assertEquals("Registration certificateand Legal kit", actText.get(26), textLines.get(26));
			ah.assertEquals("Insurance certificate and Service manual", actText.get(27), textLines.get(27));
			ah.assertEquals("Fuel card and Certificate of conformity", actText.get(28), textLines.get(28));
			ah.assertEquals("Technical inspection certificate and Number of keys", actText.get(29), textLines.get(29));
			ah.assertEquals("Kop Signature", actText.get(30), textLines.get(30));
			ah.assertEquals("Signature regel1", actText.get(31), "Le soussigné Adje Antonisse, né le " + orderPage.getDriverDateOfBirth() + ",");
			ah.assertEquals("Signature regel2", actText.get(32), "déclare d'avoir réceptionné le véhicule décrit le " + today + ".");
			ah.assertEquals("Signature regel3", actText.get(33), textLines.get(33));
			ah.assertEquals("Signature regel4", actText.get(34), textLines.get(34));
			ah.assertEquals("Signature regel5", actText.get(35), textLines.get(35));
			ah.assertEquals("Kop Options", actText.get(36), textLines.get(36));
			for (int i = 37; i < actText.size(); i++) {
				ah.assertEquals("Options", actText.get(i), textLines.get(i));
			}
			logger.debug("Assertions: " + ah.processAllAssertions());
			}






		//logger.debug("Assertions: " + ah.processAllAssertions());

		/*for (PDField field : fields) {
			content.append(field.getPartialName());
			content.append(" = ");
			content.append(field.getValueAsString());
			content.append(" \n ");
			String attr = field.getPartialName();
			String value = field.getValueAsString();
			attr = value;
			logger.debug("attributes with value: " + attr);
		}
		logger.debug("content: " + content.toString());
		//String title = acroForm.getField("Title").toString();
		//logger.debug("Title: " + title);
		//String value = acroForm.getField("Title").getValueAsString();

		leasingCompanyDossierNumber = acroForm.getField("LeasingCompanyDossierNumber").getValueAsString();
		ah.assertEquals("leasingCompanyDossierNumber: ",postOrderImportsRequest.getDosNr(), leasingCompanyDossierNumber);

		dataDriver = acroForm.getField("DataDriver").getValueAsString();
		ah.assertEquals("dataDriver: ","", dataDriver);


		clientName = acroForm.getField("ClientName").getValueAsString();
		ah.assertEquals("clientName: ", postOrderImportsRequest.getClientName(), clientName);
		clientStreet = acroForm.getField("ClientStreet").getValueAsString();
		ah.assertEquals("clientStreet: ", postOrderImportsRequest.getClientStreetHouseNumber(), clientStreet);


		driverNamePdf = acroForm.getField("DriverName").getValueAsString();
		ah.assertEquals("driverNamePdf: ", "", driverNamePdf);
		driverStreet = acroForm.getField("DriverStreet").getValueAsString();
		ah.assertEquals("driverStreet: ", "", driverStreet);
		driverPostalCode = acroForm.getField("DriverPostalCode").getValueAsString();
		ah.assertEquals("driverPostalCode: ", "", driverPostalCode);
		driverMobile = acroForm.getField("DriverMobile").getValueAsString();
		ah.assertEquals("driverMobile: ", "", driverMobile);
		driverEmail = acroForm.getField("DriverEmail").getValueAsString();
		ah.assertEquals("driverEmail: ", "", driverEmail);

		vehicleModel = acroForm.getField("VehicleModel").getValueAsString();
		ah.assertEquals("vehicleModel: ", postOrderImportsRequest.getVehicleModel(), vehicleModel);
		vehicleVersion = acroForm.getField("VehicleVersion").getValueAsString();
		ah.assertEquals("vehicleVersion: ", postOrderImportsRequest.getVehicleVersion(), vehicleVersion);
		vehicleExtColor = acroForm.getField("VehicleExtColor").getValueAsString();
		ah.assertEquals("vehicleExtColor: ", postOrderImportsRequest.getVehicleExteriorColor(), vehicleExtColor);
		vehicleVin = acroForm.getField("VehicleVin").getValueAsString();
		ah.assertEquals("vehicleVin: ", orderPage.getVIN(), vehicleVin);
		vehicleLicensePlate = acroForm.getField("VehicleLicensePlate").getValueAsString();
		ah.assertEquals("vehicleLicensePlate: ", orderPage.getLicensePlate(), vehicleLicensePlate);
		extraProofOfRegistration = acroForm.getField("ExtraProofOfRegistration").getValueAsString();
		Boolean boolPdfReg = null;
		if(extraProofOfRegistration.equals("Yes")) {
			boolPdfReg = true;
		}
		Boolean boolOrderReg = Boolean.parseBoolean(orderPage.getSelectRegistrationCertificate());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("extraProofOfRegistration: ", boolOrderReg, boolPdfReg);

		extraProofOfInsurance = acroForm.getField("ExtraProofOfInsurance").getValueAsString();
		Boolean boolPdfInsurance = false;
		if(extraProofOfInsurance.equals("Yes")) {
			boolPdfInsurance = true;
		}
		Boolean boolOrderInsurance = Boolean.parseBoolean(orderPage.getSelectInsuranceCertificate());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("extraProofOfInsurance: ", boolOrderInsurance, boolPdfInsurance);

		extraCertificateOfConformity = acroForm.getField("ExtraCertificateOfConformity").getValueAsString();
		Boolean boolPdfCoc = false;
		if(extraCertificateOfConformity.equals("Yes")) {
			boolPdfCoc = true;
		}
		Boolean boolOrderCoc = Boolean.parseBoolean(orderPage.getSelectCertificateOfConformity());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("extraCertificateOfConformity: ", boolOrderCoc, boolPdfCoc);

		extraProofOfRoadworthiness = acroForm.getField("ExtraProofOfRoadworthiness").getValueAsString();
		Boolean boolPdfTec = false;
		if(extraProofOfRoadworthiness.equals("Yes")) {
			boolPdfTec = true;
		}
		Boolean boolOrderTec = Boolean.parseBoolean(orderPage.getSelectTechnicalInspectionCertificate());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("extraProofOfRoadworthiness: ", boolOrderTec, boolPdfTec);

		extraLegalKit = acroForm.getField("ExtraLegalKit").getValueAsString();
		Boolean boolPdfKit = false;
		if(extraLegalKit.equals("Yes")) {
			boolPdfKit = true;
		}
		Boolean boolOrderKit = Boolean.parseBoolean(orderPage.getSelectLegalKit());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("extraLegalKit: ", boolOrderKit, boolPdfKit);

		extraManual = acroForm.getField("ExtraManual").getValueAsString();
		Boolean boolPdfManual = false;
		if(extraManual.equals("Yes")) {
			boolPdfManual = true;
		}
		Boolean boolOrderManual = Boolean.parseBoolean(orderPage.getSelectServiceManual());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("extraManual: ", boolOrderManual, boolPdfManual);

		extraFuelCard = acroForm.getField("ExtraFuelCard").getValueAsString();
		Boolean boolPdfFuelCard = false;
		if(extraFuelCard.equals("Yes")) {
			boolPdfFuelCard = true;
		}
		Boolean boolOrderFuelCard = Boolean.parseBoolean(orderPage.getSelectFuelCard());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("extraFuelCard: ", boolOrderFuelCard, boolPdfFuelCard);

		extraNumberOfKeys = acroForm.getField("ExtraNumberOfKeys").getValueAsString();
		ah.assertEquals("extraNumberOfKeys: ", orderPage.getNumberOfKeys(), extraNumberOfKeys);

		previousVehicleLicensePlate = acroForm.getField("PreviousVehicleLicensePlate").getValueAsString();
		ah.assertEquals("previousVehicleLicensePlate: ", "1FFF333", previousVehicleLicensePlate);

		previousVehicleOwner = acroForm.getField("PreviousVehicleOwner").getValueAsString();
		ah.assertEquals("previousVehicleOwner: ", "ALD Automotive", previousVehicleOwner);

		vehicleBrand = acroForm.getField("VehicleBrand").getValueAsString();
		ah.assertEquals("vehicleBrand: ", postOrderImportsRequest.getVehicleBrand(), vehicleBrand);




		vehicleIntColor = acroForm.getField("VehicleIntColor").getValueAsString();
		ah.assertEquals("vehicleIntColor: ", postOrderImportsRequest.getVehicleInteriorColor(), vehicleIntColor);

		vehicleTyres = acroForm.getField("VehicleTyres").getValueAsString();
		ah.assertEquals("vehicleTyres: ", orderPage.getTyreSpecs(), vehicleTyres);

		previousVehicle = acroForm.getField("PreviousVehicle").getValueAsString();
		Boolean boolPdfPrevVeh = false;
		if(previousVehicle.equals("Yes")) {
			boolPdfPrevVeh = true;
		}
		Boolean boolOrderPrevVeh = Boolean.parseBoolean(orderPage.getReturnedVehicle());
		//Boolean boolPdfReg = Boolean.parseBoolean(extraProofOfRegistration);
		ah.assertEquals("previousVehicle: ", boolOrderPrevVeh, boolPdfPrevVeh);


		deliveryLocationName = acroForm.getField("DeliveryLocationName").getValueAsString();
		ah.assertEquals("deliveryLocationName: ", postOrderImportsRequest.getDeliveryLocationName(), deliveryLocationName);

		deliveryLocationStreet = acroForm.getField("DeliveryLocationStreet").getValueAsString();
		String locAddress = postOrderImportsRequest.getDeliveryLocationAddress();
		String[] locStreetTmp = locAddress.split(",");
		String locStreet = locStreetTmp[0].trim();
		ah.assertEquals("deliveryLocationStreet: ", locStreet, deliveryLocationStreet);

		deliveryLocationPostalCode = acroForm.getField("DeliveryLocationPostalCode").getValueAsString();
		locAddress = postOrderImportsRequest.getDeliveryLocationAddress();
		String[] locPostalCodeTmp = locAddress.split(",");
		String locPostalCodeTmp1 = locStreetTmp[1].trim();
		String[] locPostalCodeTmp2 = locPostalCodeTmp1.split(" ");
		String locPostalCode = locPostalCodeTmp2[0].trim();
		ah.assertEquals("deliveryLocationPostalCode: ", locPostalCode, deliveryLocationPostalCode);


		clientPostalCode = acroForm.getField("ClientPostalCode").getValueAsString();
		ah.assertEquals("clientPostalCode: ", postOrderImportsRequest.getClientPostalCode(), clientPostalCode);

		clientCity = acroForm.getField("ClientCity").getValueAsString();
		ah.assertEquals("clientCity: ", postOrderImportsRequest.getClientCity(), clientCity);


		driverCity = acroForm.getField("DriverCity").getValueAsString();
		ah.assertEquals("driverCity: ", "", driverCity);

		deliveryLocationCity = acroForm.getField("DeliveryLocationCity").getValueAsString();
		ah.assertEquals("deliveryLocationCity: ", postOrderImportsRequest.getDeliveryCity(), deliveryLocationCity);

		logger.debug("Assertions: " + ah.processAllAssertions());
		if(lang.equals("nl")) {
			title = acroForm.getField("Title").getValueAsString();
			logger.debug("Title value: " + title);
			Assert.assertEquals("Ontvangstbewijs voertuig", title);
			dataClient = acroForm.getField("DataClient").getValueAsString();
			ah.assertEquals("dataClient: ","Gegevens huurder", dataClient);
			dataDriver = acroForm.getField("DataDriver").getValueAsString();
			ah.assertEquals("dataDriver: ","", dataDriver);

			driverNamePdf = acroForm.getField("DriverName").getValueAsString();
			ah.assertEquals("driverNamePdf: ", "", driverNamePdf);
			driverStreet = acroForm.getField("DriverStreet").getValueAsString();
			ah.assertEquals("driverStreet: ", "", driverStreet);
			driverPostalCode = acroForm.getField("DriverPostalCode").getValueAsString();
			ah.assertEquals("driverPostalCode: ", "", driverPostalCode);
			driverMobile = acroForm.getField("DriverMobile").getValueAsString();
			ah.assertEquals("driverMobile: ", "", driverMobile);
			driverEmail = acroForm.getField("DriverEmail").getValueAsString();
			ah.assertEquals("driverEmail: ", "", driverEmail);
			dataVehicle = acroForm.getField("DataVehicle").getValueAsString();
			ah.assertEquals("dataVehicle: ", "Gegevens Leasingvoertuig (zie opties op volgende pagina)", dataVehicle);
			vehicleBrandLabel = acroForm.getField("VehicleBrandLabel").getValueAsString();
			ah.assertEquals("vehicleBrandLabel: ", "Merk:", vehicleBrandLabel);
			vehicleModelLabel = acroForm.getField("VehicleModelLabel").getValueAsString();
			ah.assertEquals("vehicleModelLabel: ", "Model:", vehicleModelLabel);
			vehicleVersionLabel = acroForm.getField("VehicleVersionLabel").getValueAsString();
			ah.assertEquals("vehicleVersionLabel: ", "Versie:", vehicleVersionLabel);
			vehicleExtColorLabel = acroForm.getField("VehicleExtColorLabel").getValueAsString();
			ah.assertEquals("vehicleExtColorLabel: ", "Buitenkleur:", vehicleExtColorLabel);
			vehicleIntColorLabel = acroForm.getField("VehicleIntColorLabel").getValueAsString();
			ah.assertEquals("vehicleIntColorLabel: ", "Kleur intérieur:", vehicleIntColorLabel);
			vehicleTyresLabel = acroForm.getField("VehicleTyresLabel").getValueAsString();
			ah.assertEquals("vehicleTyresLabel: ", "Specificaties band:", vehicleTyresLabel);
			vehicleVinLabel = acroForm.getField("VehicleVinLabel").getValueAsString();
			ah.assertEquals("vehicleVinLabel: ", "Chassisnummer:", vehicleVinLabel);
			vehicleLicensePlateLabel = acroForm.getField("VehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("vehicleLicensePlateLabel: ", "Nummerplaat:", vehicleLicensePlateLabel);
			extraNumberOfKeysLabel = acroForm.getField("ExtraNumberOfKeysLabel").getValueAsString();
			ah.assertEquals("extraNumberOfKeysLabel: ", "Aantal sleutels", extraNumberOfKeysLabel);
			dataPreviousVehicle = acroForm.getField("DataPreviousVehicle").getValueAsString();
			ah.assertEquals("dataPreviousVehicle: ", "Ingenomen voertuig", dataPreviousVehicle);
			previousVehicleLicensePlateLabel = acroForm.getField("PreviousVehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("previousVehicleLicensePlateLabel: ", "Nummerplaat ingenomen voertuig:", previousVehicleLicensePlateLabel);
			previousVehicleOwnerLabel = acroForm.getField("PreviousVehicleOwnerLabel").getValueAsString();
			ah.assertEquals("previousVehicleOwnerLabel: ", "Voertuig op naam van:", previousVehicleOwnerLabel);
			previousVehicleLicensePlate = acroForm.getField("PreviousVehicleLicensePlate").getValueAsString();
			ah.assertEquals("previousVehicleLicensePlate: ", orderPage.getLicensePlateReturnedVehicle(), previousVehicleLicensePlate);
			previousVehicleOwner = acroForm.getField("PreviousVehicleOwner").getValueAsString();
			ah.assertEquals("previousVehicleOwner: ", "ALD Automotive", previousVehicleOwner);
			dataSignature = acroForm.getField("DataSignature").getValueAsString();
			ah.assertEquals("dataSignature: ", "Handtekening", dataSignature);
			vehicleBrand = acroForm.getField("VehicleBrand").getValueAsString();
			ah.assertEquals("vehicleBrand: ", postOrderImportsRequest.getVehicleBrand(), vehicleBrand);
			extraProofOfRegistrationLabel = acroForm.getField("ExtraProofOfRegistrationLabel").getValueAsString();
			ah.assertEquals("extraProofOfRegistrationLabel: ", "Inschrijvingsbewijs", extraProofOfRegistrationLabel);
			extraProofOfInsuranceLabel = acroForm.getField("ExtraProofOfInsuranceLabel").getValueAsString();
			ah.assertEquals("extraProofOfInsuranceLabel: ", "Verzekeringsbewijs", extraProofOfInsuranceLabel);
			extraCertificateOfConformityLabel = acroForm.getField("ExtraCertificateOfConformityLabel").getValueAsString();
			ah.assertEquals("extraCertificateOfConformityLabel: ", "Gelijkvormigheidsattest", extraCertificateOfConformityLabel);
			extraProofOfRoadworthinessLabel = acroForm.getField("ExtraProofOfRoadworthinessLabel").getValueAsString();
			ah.assertEquals("extraProofOfRoadworthinessLabel: ", "Bewijs technische controle", extraProofOfRoadworthinessLabel);
			extraLegalKitLabel = acroForm.getField("ExtraLegalKitLabel").getValueAsString();
			ah.assertEquals("extraLegalKitLabel: ", "Wettelijke kit", extraLegalKitLabel);
			extraManualLabel = acroForm.getField("ExtraManualLabel").getValueAsString();
			ah.assertEquals("extraManualLabel: ", "Onderhoudsboekje", extraManualLabel);
			extraFuelCardLabel = acroForm.getField("ExtraFuelCardLabel").getValueAsString();
			ah.assertEquals("extraFuelCardLabel: ", "Brandstofkaart", extraFuelCardLabel);
			dataExtra = acroForm.getField("DataExtra").getValueAsString();
			ah.assertEquals("dataExtra: ", "Bijkomende uitrusting en bijgevoegde documenten", dataExtra);
			clientCityLabel = acroForm.getField("ClientCityLabel").getValueAsString();
			ah.assertEquals("clientCityLabel: ", "Gemeente:", clientCityLabel);
			driverCityLabel = acroForm.getField("DriverCityLabel").getValueAsString();
			ah.assertEquals("driverCityLabel: ", "", driverCityLabel);
			deliveryLocationCityLabel = acroForm.getField("DeliveryLocationCityLabel").getValueAsString();
			ah.assertEquals("deliveryLocationCityLabel: ", "Gemeente:", deliveryLocationCityLabel);
			signatureDescriptionLine1 = acroForm.getField("SignatureDescriptionLine1").getValueAsString();
			ah.assertEquals("signatureDescriptionLine1: ", "Ondergetekende Adje Antonisse, geboren op 19/06/1965,", signatureDescriptionLine1);
			signatureDescriptionLine2 = acroForm.getField("SignatureDescriptionLine2").getValueAsString();
			ah.assertEquals("signatureDescriptionLine2: ", "verklaart het omschreven voertuig in ontvangst te hebben genomen op " + today + ".", signatureDescriptionLine2);
			financialLeaseSignatureDisclaimer = acroForm.getField("FinancialLeaseSignatureDisclaimer").getValueAsString();
			ah.assertEquals("financialLeaseSignatureDisclaimer: ", "De bestuurder, zoals gemachtigd door de huurder, verklaart aan de hand van dit ontvangstbewijs dat het leasingvoertuig overeenstemt met hetgeen besteld werd, dat deze vrij van zichtbare gebreken is, en dat deze inontvangstname de inwerkingtreding van het leasingcontract met zich meebrengt.", financialLeaseSignatureDisclaimer);
			dummyFieldName1 = acroForm.getField("dummyFieldName1").getValueAsString();
			ah.assertEquals("dummyFieldName1: ", "Opties - Accessoires", dummyFieldName1);

			logger.debug("Assertions: " + ah.processAllAssertions());
		}
		else if(lang.equals("en")) {
			title = acroForm.getField("Title").getValueAsString();
			logger.debug("Title value: " + title);
			Assert.assertEquals("Delivery receipt of leasing vehicle", title);
			dataClient = acroForm.getField("DataClient").getValueAsString();
			ah.assertEquals("dataClient: ","Data lessee", dataClient);
			dataDriver = acroForm.getField("DataDriver").getValueAsString();
			ah.assertEquals("dataDriver: ","", dataDriver);
			clientNameLabel = acroForm.getField("ClientNameLabel").getValueAsString();
			ah.assertEquals("clientName: ","Name:", clientNameLabel);
			clientStreetLabel = acroForm.getField("ClientStreetLabel").getValueAsString();
			ah.assertEquals("clientStreetLabel: ","Street:", clientStreetLabel);
			clientPostalCodeLabel = acroForm.getField("ClientPostalCodeLabel").getValueAsString();
			ah.assertEquals("clientPostalCodeLabel","Postal code:", clientPostalCodeLabel);
			driverNameLabel = acroForm.getField("DriverNameLabel").getValueAsString();
			ah.assertEquals("driverNameLabel: ", "", driverNameLabel);
			driverNameLabel = acroForm.getField("DriverNameLabel").getValueAsString();
			ah.assertEquals("driverNameLabel: ", "", driverNameLabel);
			driverPostalCodeLabel = acroForm.getField("DriverPostalCodeLabel").getValueAsString();
			ah.assertEquals("driverPostalCodeLabel: ", "", driverPostalCodeLabel);
			driverMobileLabel = acroForm.getField("DriverMobileLabel").getValueAsString();
			ah.assertEquals("driverMobileLabel: ", "", driverMobileLabel);
			driverEmailLabel = acroForm.getField("DriverEmailLabel").getValueAsString();
			ah.assertEquals("driverEmailLabel: ", "", driverEmailLabel);
			driverNamePdf = acroForm.getField("DriverName").getValueAsString();
			ah.assertEquals("driverNamePdf: ", "", driverNamePdf);
			driverStreet = acroForm.getField("DriverStreet").getValueAsString();
			ah.assertEquals("driverStreet: ", "", driverStreet);
			driverPostalCode = acroForm.getField("DriverPostalCode").getValueAsString();
			ah.assertEquals("driverPostalCode: ", "", driverPostalCode);
			driverMobile = acroForm.getField("DriverMobile").getValueAsString();
			ah.assertEquals("driverMobile: ", "", driverMobile);
			driverEmail = acroForm.getField("DriverEmail").getValueAsString();
			ah.assertEquals("driverEmail: ", "", driverEmail);
			dataVehicle = acroForm.getField("DataVehicle").getValueAsString();
			ah.assertEquals("dataVehicle: ", "Data leasing vehicle (see options on next page)", dataVehicle);
			vehicleBrandLabel = acroForm.getField("VehicleBrandLabel").getValueAsString();
			ah.assertEquals("vehicleBrandLabel: ", "Make:", vehicleBrandLabel);
			vehicleModelLabel = acroForm.getField("VehicleModelLabel").getValueAsString();
			ah.assertEquals("vehicleModelLabel: ", "Model:", vehicleModelLabel);
			vehicleVersionLabel = acroForm.getField("VehicleVersionLabel").getValueAsString();
			ah.assertEquals("vehicleVersionLabel: ", "Version:", vehicleVersionLabel);
			vehicleExtColorLabel = acroForm.getField("VehicleExtColorLabel").getValueAsString();
			ah.assertEquals("vehicleExtColorLabel: ", "Exterior:", vehicleExtColorLabel);
			vehicleIntColorLabel = acroForm.getField("VehicleIntColorLabel").getValueAsString();
			ah.assertEquals("vehicleIntColorLabel: ", "Interior:", vehicleIntColorLabel);
			vehicleTyresLabel = acroForm.getField("VehicleTyresLabel").getValueAsString();
			ah.assertEquals("vehicleTyresLabel: ", "Tyre specifications:", vehicleTyresLabel);
			vehicleVinLabel = acroForm.getField("VehicleVinLabel").getValueAsString();
			ah.assertEquals("vehicleVinLabel: ", "VIN:", vehicleVinLabel);
			vehicleLicensePlateLabel = acroForm.getField("VehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("vehicleLicensePlateLabel: ", "License plate:", vehicleLicensePlateLabel);
			extraNumberOfKeysLabel = acroForm.getField("ExtraNumberOfKeysLabel").getValueAsString();
			ah.assertEquals("extraNumberOfKeysLabel: ", "Number of keys", extraNumberOfKeysLabel);
			dataPreviousVehicle = acroForm.getField("DataPreviousVehicle").getValueAsString();
			ah.assertEquals("dataPreviousVehicle: ", "Returned vehicle", dataPreviousVehicle);
			previousVehicleLicensePlateLabel = acroForm.getField("PreviousVehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("previousVehicleLicensePlateLabel: ", "License plate returned vehicle:", previousVehicleLicensePlateLabel);
			previousVehicleOwnerLabel = acroForm.getField("PreviousVehicleOwnerLabel").getValueAsString();
			ah.assertEquals("previousVehicleOwnerLabel: ", "Vehicle registered on the name of:", previousVehicleOwnerLabel);
			dataSignature = acroForm.getField("DataSignature").getValueAsString();
			ah.assertEquals("dataSignature: ", "Signature", dataSignature);
			extraProofOfRegistrationLabel = acroForm.getField("ExtraProofOfRegistrationLabel").getValueAsString();
			ah.assertEquals("extraProofOfRegistrationLabel: ", "Registration certificate", extraProofOfRegistrationLabel);
			extraProofOfInsuranceLabel = acroForm.getField("ExtraProofOfInsuranceLabel").getValueAsString();
			ah.assertEquals("extraProofOfInsuranceLabel: ", "Insurance certificate", extraProofOfInsuranceLabel);
			extraCertificateOfConformityLabel = acroForm.getField("ExtraCertificateOfConformityLabel").getValueAsString();
			ah.assertEquals("extraCertificateOfConformityLabel: ", "Certificate of conformity", extraCertificateOfConformityLabel);
			extraProofOfRoadworthinessLabel = acroForm.getField("ExtraProofOfRoadworthinessLabel").getValueAsString();
			ah.assertEquals("extraProofOfRoadworthinessLabel: ", "Technical inspection certificate", extraProofOfRoadworthinessLabel);
			extraLegalKitLabel = acroForm.getField("ExtraLegalKitLabel").getValueAsString();
			ah.assertEquals("extraLegalKitLabel: ", "Legal kit", extraLegalKitLabel);
			extraManualLabel = acroForm.getField("ExtraManualLabel").getValueAsString();
			ah.assertEquals("extraManualLabel: ", "Service manual", extraManualLabel);
			extraFuelCardLabel = acroForm.getField("ExtraFuelCardLabel").getValueAsString();
			ah.assertEquals("extraFuelCardLabel: ", "Fuel card", extraFuelCardLabel);
			dataExtra = acroForm.getField("DataExtra").getValueAsString();
			ah.assertEquals("dataExtra: ", "Additional equipment and enclosed documents", dataExtra);
			clientCityLabel = acroForm.getField("ClientCityLabel").getValueAsString();
			ah.assertEquals("clientCityLabel: ", "City:", clientCityLabel);
			driverCityLabel = acroForm.getField("DriverCityLabel").getValueAsString();
			ah.assertEquals("driverCityLabel: ", "", driverCityLabel);
			deliveryLocationCityLabel = acroForm.getField("DeliveryLocationCityLabel").getValueAsString();
			ah.assertEquals("deliveryLocationCityLabel: ", "City:", deliveryLocationCityLabel);
			signatureDescriptionLine1 = acroForm.getField("SignatureDescriptionLine1").getValueAsString();
			ah.assertEquals("signatureDescriptionLine1: ", "The undersigned Adje Antonisse, born on 19/06/1965,", signatureDescriptionLine1);
			signatureDescriptionLine2 = acroForm.getField("SignatureDescriptionLine2").getValueAsString();
			ah.assertEquals("signatureDescriptionLine2: ", "declares to have received the vehicle described here on " + today + ".", signatureDescriptionLine2);
			financialLeaseSignatureDisclaimer = acroForm.getField("FinancialLeaseSignatureDisclaimer").getValueAsString();
			ah.assertEquals("financialLeaseSignatureDisclaimer: ", "By means of this delivery receipt, the driver, as authorized by the lessee, declares that the leasing vehicle corresponds to what was ordered, that it is free of visible defects, and the acceptance of the vehicle leads to the start of the leasing contract.", financialLeaseSignatureDisclaimer);
			dummyFieldName1 = acroForm.getField("dummyFieldName1").getValueAsString();
			ah.assertEquals("dummyFieldName1: ", "Options - Accessories", dummyFieldName1);

			logger.debug("Assertions: " + ah.processAllAssertions());
		}
		else if(lang.equals("de")) {
			title = acroForm.getField("Title").getValueAsString();
			logger.debug("Title value: " + title);
			Assert.assertEquals("Bereitstellungsbestätigung", title);
			dataClient = acroForm.getField("DataClient").getValueAsString();
			ah.assertEquals("dataClient: ","Kundendaten", dataClient);
			dataDriver = acroForm.getField("DataDriver").getValueAsString();
			ah.assertEquals("dataDriver: ","", dataDriver);
			clientNameLabel = acroForm.getField("ClientNameLabel").getValueAsString();
			ah.assertEquals("clientName: ","Name:", clientNameLabel);
			clientStreetLabel = acroForm.getField("ClientStreetLabel").getValueAsString();
			ah.assertEquals("clientStreetLabel: ","Strasse:", clientStreetLabel);
			clientPostalCodeLabel = acroForm.getField("ClientPostalCodeLabel").getValueAsString();
			ah.assertEquals("clientPostalCodeLabel","Postleitzahl:", clientPostalCodeLabel);
			driverNameLabel = acroForm.getField("DriverNameLabel").getValueAsString();
			ah.assertEquals("driverNameLabel: ", "", driverNameLabel);
			driverPostalCodeLabel = acroForm.getField("DriverPostalCodeLabel").getValueAsString();
			ah.assertEquals("driverPostalCodeLabel: ", "", driverPostalCodeLabel);
			driverMobileLabel = acroForm.getField("DriverMobileLabel").getValueAsString();
			ah.assertEquals("driverMobileLabel: ", "", driverMobileLabel);
			driverEmailLabel = acroForm.getField("DriverEmailLabel").getValueAsString();
			ah.assertEquals("driverEmailLabel: ", "", driverEmailLabel);
			driverNamePdf = acroForm.getField("DriverName").getValueAsString();
			ah.assertEquals("driverNamePdf: ", "", driverNamePdf);
			driverStreet = acroForm.getField("DriverStreet").getValueAsString();
			ah.assertEquals("driverStreet: ", "", driverStreet);
			driverPostalCode = acroForm.getField("DriverPostalCode").getValueAsString();
			ah.assertEquals("driverPostalCode: ", "", driverPostalCode);
			driverMobile = acroForm.getField("DriverMobile").getValueAsString();
			ah.assertEquals("driverMobile: ", "", driverMobile);
			driverEmail = acroForm.getField("DriverEmail").getValueAsString();
			ah.assertEquals("driverEmail: ", "", driverEmail);
			dataVehicle = acroForm.getField("DataVehicle").getValueAsString();
			ah.assertEquals("dataVehicle: ", "Fahrzeugdaten (Optionen auf der nächsten Seite)", dataVehicle);
			vehicleBrandLabel = acroForm.getField("VehicleBrandLabel").getValueAsString();
			ah.assertEquals("vehicleBrandLabel: ", "Marke:", vehicleBrandLabel);
			vehicleModelLabel = acroForm.getField("VehicleModelLabel").getValueAsString();
			ah.assertEquals("vehicleModelLabel: ", "Modell:", vehicleModelLabel);
			vehicleVersionLabel = acroForm.getField("VehicleVersionLabel").getValueAsString();
			ah.assertEquals("vehicleVersionLabel: ", "Motortyp:", vehicleVersionLabel);
			vehicleExtColorLabel = acroForm.getField("VehicleExtColorLabel").getValueAsString();
			ah.assertEquals("vehicleExtColorLabel: ", "Farbe:", vehicleExtColorLabel);
			vehicleIntColorLabel = acroForm.getField("VehicleIntColorLabel").getValueAsString();
			ah.assertEquals("vehicleIntColorLabel: ", "Innenraum:", vehicleIntColorLabel);
			vehicleTyresLabel = acroForm.getField("VehicleTyresLabel").getValueAsString();
			ah.assertEquals("vehicleTyresLabel: ", "Reifenkennzeichnung:", vehicleTyresLabel);
			vehicleVinLabel = acroForm.getField("VehicleVinLabel").getValueAsString();
			ah.assertEquals("vehicleVinLabel: ", "Fahrgestellnummer:", vehicleVinLabel);
			vehicleLicensePlateLabel = acroForm.getField("VehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("vehicleLicensePlateLabel: ", "Kennzeichen:", vehicleLicensePlateLabel);
			extraNumberOfKeysLabel = acroForm.getField("ExtraNumberOfKeysLabel").getValueAsString();
			ah.assertEquals("extraNumberOfKeysLabel: ", "Anzahl Schlüssel", extraNumberOfKeysLabel);
			dataPreviousVehicle = acroForm.getField("DataPreviousVehicle").getValueAsString();
			ah.assertEquals("dataPreviousVehicle: ", "Zurückgegeben Fahrzeug", dataPreviousVehicle);
			previousVehicleLicensePlateLabel = acroForm.getField("PreviousVehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("previousVehicleLicensePlateLabel: ", "Kennzeichen zurückgegeben Fahrzeug:", previousVehicleLicensePlateLabel);
			previousVehicleOwnerLabel = acroForm.getField("PreviousVehicleOwnerLabel").getValueAsString();
			ah.assertEquals("previousVehicleOwnerLabel: ", "Registriert auf den Namen von:", previousVehicleOwnerLabel);
			dataSignature = acroForm.getField("DataSignature").getValueAsString();
			ah.assertEquals("dataSignature: ", "Unterzeichnung", dataSignature);
			extraProofOfRegistrationLabel = acroForm.getField("ExtraProofOfRegistrationLabel").getValueAsString();
			ah.assertEquals("extraProofOfRegistrationLabel: ", "Registrierungsbescheinigung", extraProofOfRegistrationLabel);
			extraProofOfInsuranceLabel = acroForm.getField("ExtraProofOfInsuranceLabel").getValueAsString();
			ah.assertEquals("extraProofOfInsuranceLabel: ", "Versicherungsbescheinigung", extraProofOfInsuranceLabel);
			extraCertificateOfConformityLabel = acroForm.getField("ExtraCertificateOfConformityLabel").getValueAsString();
			ah.assertEquals("extraCertificateOfConformityLabel: ", "Konformitätszertifikat", extraCertificateOfConformityLabel);
			extraProofOfRoadworthinessLabel = acroForm.getField("ExtraProofOfRoadworthinessLabel").getValueAsString();
			ah.assertEquals("extraProofOfRoadworthinessLabel: ", "Bescheinigung der technischen Überprüfung", extraProofOfRoadworthinessLabel);
			extraLegalKitLabel = acroForm.getField("ExtraLegalKitLabel").getValueAsString();
			ah.assertEquals("extraLegalKitLabel: ", "Legale Ausrüstung", extraLegalKitLabel);
			extraManualLabel = acroForm.getField("ExtraManualLabel").getValueAsString();
			ah.assertEquals("extraManualLabel: ", "Service-Handbuch", extraManualLabel);
			extraFuelCardLabel = acroForm.getField("ExtraFuelCardLabel").getValueAsString();
			ah.assertEquals("extraFuelCardLabel: ", "Tankkarte", extraFuelCardLabel);
			dataExtra = acroForm.getField("DataExtra").getValueAsString();
			ah.assertEquals("dataExtra: ", "Zusatzausstattung und beigefügte Dokumente", dataExtra);
			clientCityLabel = acroForm.getField("ClientCityLabel").getValueAsString();
			ah.assertEquals("clientCityLabel: ", "Stadt:", clientCityLabel);
			driverCityLabel = acroForm.getField("DriverCityLabel").getValueAsString();
			ah.assertEquals("driverCityLabel: ", "", driverCityLabel);
			deliveryLocationCityLabel = acroForm.getField("DeliveryLocationCityLabel").getValueAsString();
			ah.assertEquals("deliveryLocationCityLabel: ", "Stadt:", deliveryLocationCityLabel);
			signatureDescriptionLine1 = acroForm.getField("SignatureDescriptionLine1").getValueAsString();
			ah.assertEquals("signatureDescriptionLine1: ", "Der Unterzeichnende Adje Antonisse, geboren am 19/06/1965,", signatureDescriptionLine1);
			signatureDescriptionLine2 = acroForm.getField("SignatureDescriptionLine2").getValueAsString();
			ah.assertEquals("signatureDescriptionLine2: ", "erklärt hiermit das beschriebene Fahrzeug entgegengenommen zu haben am  " + today + "", signatureDescriptionLine2);
			financialLeaseSignatureDisclaimer = acroForm.getField("FinancialLeaseSignatureDisclaimer").getValueAsString();
			ah.assertEquals("financialLeaseSignatureDisclaimer: ", "Der vom Vermieter bevollmächtigte Fahrer erklärt mit dieser Quittung, dass das gemietete Fahrzeug dem bestellten entspricht, dass es frei von sichtbaren Mängeln ist und dass mit dieser Annahme der Leasingvertrag in Kraft tritt.", financialLeaseSignatureDisclaimer);
			dummyFieldName1 = acroForm.getField("dummyFieldName1").getValueAsString();
			ah.assertEquals("dummyFieldName1: ", "Optionen - Zubehör", dummyFieldName1);

			logger.debug("Assertions: " + ah.processAllAssertions());
		}
		else if(lang.equals("fr")) {
			title = acroForm.getField("Title").getValueAsString();
			logger.debug("Title value: " + title);
			Assert.assertEquals("Attestation de mise à disposition", title);
			dataClient = acroForm.getField("DataClient").getValueAsString();
			ah.assertEquals("dataClient: ","Données du locataire", dataClient);
			dataDriver = acroForm.getField("DataDriver").getValueAsString();
			ah.assertEquals("dataDriver: ","", dataDriver);
			clientNameLabel = acroForm.getField("ClientNameLabel").getValueAsString();
			ah.assertEquals("clientName: ","Nom:", clientNameLabel);
			clientStreetLabel = acroForm.getField("ClientStreetLabel").getValueAsString();
			ah.assertEquals("clientStreetLabel: ","Rue:", clientStreetLabel);
			clientPostalCodeLabel = acroForm.getField("ClientPostalCodeLabel").getValueAsString();
			ah.assertEquals("clientPostalCodeLabel","Code postal:", clientPostalCodeLabel);
			driverNameLabel = acroForm.getField("DriverNameLabel").getValueAsString();
			ah.assertEquals("driverNameLabel: ", "", driverNameLabel);
			driverPostalCodeLabel = acroForm.getField("DriverPostalCodeLabel").getValueAsString();
			ah.assertEquals("driverPostalCodeLabel: ", "", driverPostalCodeLabel);
			driverMobileLabel = acroForm.getField("DriverMobileLabel").getValueAsString();
			ah.assertEquals("driverMobileLabel: ", "", driverMobileLabel);
			driverEmailLabel = acroForm.getField("DriverEmailLabel").getValueAsString();
			ah.assertEquals("driverEmailLabel: ", "", driverEmailLabel);
			driverNamePdf = acroForm.getField("DriverName").getValueAsString();
			ah.assertEquals("driverNamePdf: ", "", driverNamePdf);
			driverStreet = acroForm.getField("DriverStreet").getValueAsString();
			ah.assertEquals("driverStreet: ", "", driverStreet);
			driverPostalCode = acroForm.getField("DriverPostalCode").getValueAsString();
			ah.assertEquals("driverPostalCode: ", "", driverPostalCode);
			driverMobile = acroForm.getField("DriverMobile").getValueAsString();
			ah.assertEquals("driverMobile: ", "", driverMobile);
			driverEmail = acroForm.getField("DriverEmail").getValueAsString();
			ah.assertEquals("driverEmail: ", "", driverEmail);
			dataVehicle = acroForm.getField("DataVehicle").getValueAsString();
			ah.assertEquals("dataVehicle: ", "Données véhicule de leasing (voir options à la page suivante)", dataVehicle);
			vehicleBrandLabel = acroForm.getField("VehicleBrandLabel").getValueAsString();
			ah.assertEquals("vehicleBrandLabel: ", "Marque:", vehicleBrandLabel);
			vehicleModelLabel = acroForm.getField("VehicleModelLabel").getValueAsString();
			ah.assertEquals("vehicleModelLabel: ", "Modèle:", vehicleModelLabel);
			vehicleVersionLabel = acroForm.getField("VehicleVersionLabel").getValueAsString();
			ah.assertEquals("vehicleVersionLabel: ", "Version:", vehicleVersionLabel);
			vehicleExtColorLabel = acroForm.getField("VehicleExtColorLabel").getValueAsString();
			ah.assertEquals("vehicleExtColorLabel: ", "Couleur du véhicule:", vehicleExtColorLabel);
			vehicleIntColorLabel = acroForm.getField("VehicleIntColorLabel").getValueAsString();
			ah.assertEquals("vehicleIntColorLabel: ", "Couleur intérieur:", vehicleIntColorLabel);
			vehicleTyresLabel = acroForm.getField("VehicleTyresLabel").getValueAsString();
			ah.assertEquals("vehicleTyresLabel: ", "Spécifications pneus:", vehicleTyresLabel);
			vehicleVinLabel = acroForm.getField("VehicleVinLabel").getValueAsString();
			ah.assertEquals("vehicleVinLabel: ", "Numéro châssis:", vehicleVinLabel);
			vehicleLicensePlateLabel = acroForm.getField("VehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("vehicleLicensePlateLabel: ", "Plaque d'immatriculation:", vehicleLicensePlateLabel);
			extraNumberOfKeysLabel = acroForm.getField("ExtraNumberOfKeysLabel").getValueAsString();
			ah.assertEquals("extraNumberOfKeysLabel: ", "Nombre de clés", extraNumberOfKeysLabel);
			dataPreviousVehicle = acroForm.getField("DataPreviousVehicle").getValueAsString();
			ah.assertEquals("dataPreviousVehicle: ", "Véhicule rentrant", dataPreviousVehicle);
			previousVehicleLicensePlateLabel = acroForm.getField("PreviousVehicleLicensePlateLabel").getValueAsString();
			ah.assertEquals("previousVehicleLicensePlateLabel: ", "Plaque immatriculation véhicule rentrant:", previousVehicleLicensePlateLabel);
			previousVehicleOwnerLabel = acroForm.getField("PreviousVehicleOwnerLabel").getValueAsString();
			ah.assertEquals("previousVehicleOwnerLabel: ", "Véhicule enregistré au nom de:", previousVehicleOwnerLabel);
			dataSignature = acroForm.getField("DataSignature").getValueAsString();
			ah.assertEquals("dataSignature: ", "Signature", dataSignature);
			extraProofOfRegistrationLabel = acroForm.getField("ExtraProofOfRegistrationLabel").getValueAsString();
			ah.assertEquals("extraProofOfRegistrationLabel: ", "Certificat immatriculation", extraProofOfRegistrationLabel);
			extraProofOfInsuranceLabel = acroForm.getField("ExtraProofOfInsuranceLabel").getValueAsString();
			ah.assertEquals("extraProofOfInsuranceLabel: ", "Certificat assurance", extraProofOfInsuranceLabel);
			extraCertificateOfConformityLabel = acroForm.getField("ExtraCertificateOfConformityLabel").getValueAsString();
			ah.assertEquals("extraCertificateOfConformityLabel: ", "Certificat conformité", extraCertificateOfConformityLabel);
			extraProofOfRoadworthinessLabel = acroForm.getField("ExtraProofOfRoadworthinessLabel").getValueAsString();
			ah.assertEquals("extraProofOfRoadworthinessLabel: ", "Certificat contrôle techn.", extraProofOfRoadworthinessLabel);
			extraLegalKitLabel = acroForm.getField("ExtraLegalKitLabel").getValueAsString();
			ah.assertEquals("extraLegalKitLabel: ", "Kit légal", extraLegalKitLabel);
			extraManualLabel = acroForm.getField("ExtraManualLabel").getValueAsString();
			ah.assertEquals("extraManualLabel: ", "Carnet d'entretien", extraManualLabel);
			extraFuelCardLabel = acroForm.getField("ExtraFuelCardLabel").getValueAsString();
			ah.assertEquals("extraFuelCardLabel: ", "Carte carburant", extraFuelCardLabel);
			dataExtra = acroForm.getField("DataExtra").getValueAsString();
			ah.assertEquals("dataExtra: ", "Equipement supplémentaire et documents inclus", dataExtra);
			clientCityLabel = acroForm.getField("ClientCityLabel").getValueAsString();
			ah.assertEquals("clientCityLabel: ", "Ville:", clientCityLabel);
			driverCityLabel = acroForm.getField("DriverCityLabel").getValueAsString();
			ah.assertEquals("driverCityLabel: ", "", driverCityLabel);
			deliveryLocationCityLabel = acroForm.getField("DeliveryLocationCityLabel").getValueAsString();
			ah.assertEquals("deliveryLocationCityLabel: ", "Ville:", deliveryLocationCityLabel);
			signatureDescriptionLine1 = acroForm.getField("SignatureDescriptionLine1").getValueAsString();
			ah.assertEquals("signatureDescriptionLine1: ", "Le soussigné Adje Antonisse, né le 19/06/1965,", signatureDescriptionLine1);
			signatureDescriptionLine2 = acroForm.getField("SignatureDescriptionLine2").getValueAsString();
			ah.assertEquals("signatureDescriptionLine2: ", "déclare d'avoir réceptionné le véhicule décrit le " + today + ".", signatureDescriptionLine2);
			financialLeaseSignatureDisclaimer = acroForm.getField("FinancialLeaseSignatureDisclaimer").getValueAsString();
			ah.assertEquals("financialLeaseSignatureDisclaimer: ", "Le conducteur, tel que mandaté par le locataire, déclare au moyen de ce procès-verbal de réception, que le véhicule de leasing correspond à ce qui a été commandé, qu’il est libre de vices apparents, que le contrat de leasing peut entrer en force à la date de livraison.", financialLeaseSignatureDisclaimer);
			dummyFieldName1 = acroForm.getField("dummyFieldName1").getValueAsString();
			ah.assertEquals("dummyFieldName1: ", "Options - Accessoires", dummyFieldName1);

			logger.debug("Assertions: " + ah.processAllAssertions());
		}
		else {
			logger.debug("Pdf language is not specified");
		}*/
}

	public void validatePdfLogo(PDDocument doc) throws IOException {
		File imgLogo = null;
		PDPageTree list = doc.getPages();
		for (PDPage page : list) {
			PDResources pdResources = page.getResources();
			for (COSName c : pdResources.getXObjectNames()) {
				PDXObject o = pdResources.getXObject(c);
				if (o instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
					imgLogo = new File("./src/test/resources/runtime/Pdf/" + System.nanoTime() + ".jpg");
					logger.debug("File imgLogo: " + imgLogo.toString());
					ImageIO.write(((org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject)o).getImage(), "jpeg", imgLogo);
					logger.debug("image weggeschreven naar timestamp.jpg");
				}
			}
		}

		BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources("./src/test/resources/runtime/RSlogo.jpg");
		BufferedImage actualImage = ImageComparisonUtil.readImageFromResources(imgLogo.toString());

		File resultDestination = new File( "./src/test/resources/runtime/result.png" );

		ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();
		logger.debug("imageComparisonResult: " + imageComparisonResult.getResult().toString());

		//Images mogen niet meer dan 1% afwijken
		ImageComparison imageComparison = new ImageComparison(expectedImage, actualImage)
				.setAllowingPercentOfDifferentPixels(1);
		ImageComparisonResult compareResult = imageComparison.compareImages();
		Assert.assertEquals(MATCH, compareResult.getImageComparisonState());
		//compareResult = imageComparison.setAllowingPercentOfDifferentPixels(0).compareImages();
		//Assert.assertEquals(MISMATCH, compareResult.getImageComparisonState());

		for (int i = 0; i < doc.getNumberOfPages(); ++i)
		{
			PDPage page = doc.getPage(i);
			PDResources res = page.getResources();
			for (COSName fontName : res.getFontNames())
			{
				PDFont font = res.getFont(fontName);
				logger.debug("FONT :: "+ font);
			}
		}


		PDFRenderer pdfRenderer	= new PDFRenderer(doc);
		BufferedImage img = pdfRenderer.renderImage(0);
		ImageIO.write(
				img, "JPEG",
				new File("./src/test/resources/runtime/Pdf/delivery-receipt-00000000-000001.jpg"));
		System.out.println(
				"Image has been extracted successfully");
		doc.close();
	}

	public void validate_metadata(PDDocument doc) throws IOException {
		PDDocumentInformation info = doc.getDocumentInformation();
		PDDocumentCatalog cat = doc.getDocumentCatalog();
		PDMetadata metadata = cat.getMetadata();
		logger.debug( "Page Count=" + doc.getNumberOfPages() );
		logger.debug( "Title=" + info.getTitle() );
		logger.debug( "Author=" + info.getAuthor() );
		logger.debug( "Subject=" + info.getSubject() );
		logger.debug( "Keywords=" + info.getKeywords() );
		logger.debug( "Creator=" + info.getCreator() );
		logger.debug( "Producer=" + info.getProducer() );
		//logger.debug( "Creation Date=" + formatDate( info.getCreationDate() ) );
		//logger.debug( "Modification Date=" + formatDate( info.getModificationDate() ) );
		logger.debug( "Trapped=" + info.getTrapped() );
		if( metadata != null )
		{
			String string =  new String( metadata.toByteArray(), "ISO-8859-1" );
			logger.debug( "Metadata=" + string );
		}
	}
}
