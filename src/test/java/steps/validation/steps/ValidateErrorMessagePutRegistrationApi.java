package steps.validation.steps;

import apiEngine.Endpoints;
import apiEngine.Route;
import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.order.timeline.OrderTimeline;
import apiEngine.model.get.order.timeline.wallbox.WallBoxOrderTimeLine;
import apiEngine.model.get.supplier.actor.SupplierActor;
import cucumber.TestContext;
import helpers.AssertHelper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetLeasingCompanyActorById;
import pageobjects.api.API_GetOrderTimelineById;
import pageobjects.api.API_GetSupplierActorById;
import pageobjects.api.API_PutRegistration;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_PostWallBoxImports;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;
import pageobjects.ui.ORD_Wallbox_Order_Details_Page;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Properties;

public class ValidateErrorMessagePutRegistrationApi {
	final Logger logger = LogManager.getLogger(ValidateErrorMessagePutRegistrationApi.class);

	TestContext testContext;
	ORD_Order_Details_Page orderDetailsPage;
	ORD_Wallbox_Order_Details_Page wallboxDetailsPage;
	ORD_Order_Page orderPage;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_PostWallBoxImports postWallBoxImports;
	API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	API_GetSupplierActorById getSupplierActorById;
	API_GetOrderTimelineById getOrderTimelineById;
	API_PutRegistration putRegistration;

	private AssertHelper ah = new AssertHelper();


	public ValidateErrorMessagePutRegistrationApi(TestContext context) throws InterruptedException, IOException {
		testContext = context;
		orderDetailsPage = testContext.getPageObjectManager().getORD_Order_Details_Page();
		wallboxDetailsPage = testContext.getPageObjectManager().getORD_Wallbox_Order_Details_Page();
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		postWallBoxImports = testContext.getPageObjectManager().postFTP_PostWallBoxImports();
		getLeasingCompanyActorById = testContext.getPageObjectManager().getAPI_GetLeasingCompanyActorById();
		getSupplierActorById = testContext.getPageObjectManager().getAPI_GetSupplierActorById();
		getOrderTimelineById = testContext.getPageObjectManager().getAPI_GetOrderTimelineById();
		putRegistration = testContext.getPageObjectManager().putAPI_PutRegistration();

	}

	String dosNr;
	String id;
	String errorMessage;
	private static Response responseLCActor;
	private static LeasingCompanyActor leasingCompanyActor;
	private static Response responseSupplierActor;
	private static SupplierActor supplierActor;
	private static Response response;
	private static OrderTimeline orderTimeline;
	private static WallBoxOrderTimeLine wallBoxOrderTimeline;

	@Then("^User validates error messages put registration$")
	public void user_validates_error_messages_put_registration_are_correct(DataTable table) throws InterruptedException, ParseException, IOException {

		List<List<String>> data = table.asLists();
		errorMessage = data.get(1).get(0);


		Response resp = putRegistration.getRes();
		logger.debug("response in ValidateErrorMessage: : " + resp.asPrettyString());
		//Response response = request.put(Route.PutRegistration());
		ResponseBody rb = resp.getBody().prettyPeek();
		logger.debug("ERROR_MESSAGE: " + rb.prettyPeek().jsonPath().get("error"));

		String strRes = rb.prettyPeek().jsonPath().get("error").toString();
		strRes = strRes.replace("[", "");
		strRes = strRes.replace("]", "");





			ah.assertEquals("ERROR MESSAGE ",	errorMessage,strRes);


		logger.debug(ah.processAllAssertions());


		}
	}

