package steps.validation.steps;

import io.cucumber.java.en.Then;
import pageobjects.api.API_GetLeasingCompanyActorById;
import pageobjects.api.API_GetSupplierActorById;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;
import cucumber.TestContext;
import enums.ContextGetLeasingCompanyActorByIdApi;
import enums.ContextGetSupplierActorByIdApi;
import enums.ContextOrderDetailUI;
import enums.ContextOrderUI;
import enums.ContextPostOrderImportsRequest;
import helpers.AssertHelper;

//import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ValidateOrderDetailsUiApi {
	final Logger logger = LogManager.getLogger(ValidateOrderDetailsUiApi.class);
	
	TestContext testContext;
	ORD_Order_Details_Page orderDetailsPage;
	ORD_Order_Page orderPage;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	API_GetSupplierActorById getSupplierActorById;
	AssertHelper ah = new AssertHelper();


	
	public ValidateOrderDetailsUiApi(TestContext context) throws InterruptedException, IOException {
		 testContext = context;
		 orderDetailsPage = testContext.getPageObjectManager().getORD_Order_Details_Page();
		 orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		 postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		 getLeasingCompanyActorById = testContext.getPageObjectManager().getAPI_GetLeasingCompanyActorById();
		 getSupplierActorById = testContext.getPageObjectManager().getAPI_GetSupplierActorById();

	}

	@Then("^User validates orderdetails are correct$")
	public void user_validates_orderdetails_are_correct() throws InterruptedException, ParseException {
		

		
//****************************************************** set context with data from order details page ****************************************************************************************
		
		testContext.scenarioContext.setContext(ContextOrderDetailUI.ORDERSTATUS, orderDetailsPage.getOrderStatus());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYDOSSIERNUMBER, orderDetailsPage.getLeasingCompanyDossierNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.ORDERNUMBERSUPPLIER, orderDetailsPage.getOrderNumberSupplier());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.CUSTOMERNUMBERSUPPLIER, orderDetailsPage.getCustomerNumberSupplier());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.AGENTRSNUMBER, orderDetailsPage.getAgentRsNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.AGENTNAME, orderDetailsPage.getAgentName());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VERSIONNUMBER, orderDetailsPage.getVersionNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYNAME, orderDetailsPage.getLeasingCompanyName());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYENTERPRISENUMBER, orderDetailsPage.getLeasingCompanyEnterpriseNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYRSNUMBER, orderDetailsPage.getLeasingCompanyRsNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYBILLINGADDRESS, orderDetailsPage.getLeasingCompanybillingAddress());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYCOUNTRY, orderDetailsPage.getLeasingCompanyCountry());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPERSON, orderDetailsPage.getLeasingCompanyContactPerson());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTEMAIL, orderDetailsPage.getLeasingCompanyContactEmail());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTMOBILENUMBER, orderDetailsPage.getLeasingCompanyContactMobileNumber());
		logger.debug("orderDetailsPage.getLeasingCompanyContactMobileNumber(): " + orderDetailsPage.getLeasingCompanyContactMobileNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPHONENUMBER, orderDetailsPage.getLeasingCompanyContactPhoneNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTLANGUAGE, orderDetailsPage.getLeasingCompanyContactLanguage());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERNAMETITLE, orderDetailsPage.getSupplierNameTitle());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERNAME, orderDetailsPage.getSupplierName());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERENTERPRISENUMBER, orderDetailsPage.getSupplierEnterpriseNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERRSNUMBER, orderDetailsPage.getSupplierRsNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERBILLINGADDRESS, orderDetailsPage.getSupplierbillingAddress());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERCOUNTRY, orderDetailsPage.getSupplierCountry());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERCONTACTPERSON, orderDetailsPage.getSupplierContactPerson());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERCONTACTEMAIL, orderDetailsPage.getSupplierContactEmail());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERCONTACTMOBILENUMBER, orderDetailsPage.getSupplierContactMobileNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERCONTACTPHONENUMBER, orderDetailsPage.getSupplierContactPhoneNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SUPPLIERCONTACTLANGUAGE, orderDetailsPage.getSupplierContactLanguage());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEEENAMETITLE, orderDetailsPage.getLesseeNameTitle());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEEENTERPRISENUMBER, orderDetailsPage.getLesseeEnterpriseNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEEBILLINGADDRESS, orderDetailsPage.getLesseeBillingAddress());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEECONTACTPERSON, orderDetailsPage.getLesseeContactPerson());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEECONTACTEMAIL, orderDetailsPage.getLesseeContactEmail());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEECONTACTMOBILENUMBER, orderDetailsPage.getLesseeContactMobileNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEECONTACTPHONENUMBER, orderDetailsPage.getLesseeContactPhoneNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LESSEECONTACTLANGUAGE, orderDetailsPage.getLesseeContactLanguage());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DRIVERADDRESS, orderDetailsPage.getDriverAddress());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DRIVERCONTACTPERSON, orderDetailsPage.getDriverContactPerson());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DRIVERCONTACTEMAIL, orderDetailsPage.getDriverContactEmail());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DRIVERCONTACTMOBILENUMBER, orderDetailsPage.getDriverContactMobileNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DRIVERCONTACTPHONENUMBER, orderDetailsPage.getDriverContactPhoneNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DRIVERCONTACTLANGUAGE, orderDetailsPage.getDriverContactLanguage());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEDESCRIPTIONTITLE, orderDetailsPage.getVehicleDescriptionTitle());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEVIN, orderDetailsPage.getVehicleVIN());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEVINCONTROLNUMBER, orderDetailsPage.getVehicleVINControlNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEMODELYEAR, orderDetailsPage.getVehicleModelYear());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEIDENTIFICATIONCODE, orderDetailsPage.getVehicleIdentificationCode());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLELICENSEPLATE, orderDetailsPage.getVehicleLicensePlate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLESTOCK, orderDetailsPage.getVehicleStock());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEMILEAGE, orderDetailsPage.getVehicleMileage());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLELANGUAGEDOCUMENTS, orderDetailsPage.getVehicleLanguageDocuments());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEKEYCODE, orderDetailsPage.getVehicleKeycode());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLESTARTCODE, orderDetailsPage.getVehicleStartcode());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.MOTORTYPE, orderDetailsPage.getMotorType());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.POWER, orderDetailsPage.getPower());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.ENGINECAPACITY, orderDetailsPage.getEngineCapacity());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.NEDCVALUE, orderDetailsPage.getNedcValue());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.WLTPVALUE, orderDetailsPage.getWltpValue());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.INTERIORCOLOR, orderDetailsPage.getInteriorColor());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.TYRESPECIFICATIONS, orderDetailsPage.getTyreSpecifications());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.TYREBRAND, orderDetailsPage.getTyreBrand());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.TYRETYPE, orderDetailsPage.getTyreType());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.WEIGHT, orderDetailsPage.getWeight());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.EXTERIORCOLOR, orderDetailsPage.getExteriorColor());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.VEHICLEPRICE, orderDetailsPage.getVehiclePrice());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DISCOUNT, orderDetailsPage.getDiscount());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.PROMOTIONDISCOUNT, orderDetailsPage.getPromotionDiscount());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYCOSTS, orderDetailsPage.getDeliveryCosts());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYCOSTSDISCOUNT, orderDetailsPage.getDeliveryCostsDiscount());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.OTHERCOSTS, orderDetailsPage.getOtherCosts());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.OTHERCOSTSDISCOUNT, orderDetailsPage.getOtherCostsDiscount());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.TOTALPRICE, orderDetailsPage.getTotalPrice());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.TOTALPRICEDISCOUNT, orderDetailsPage.getTotalPriceDiscount());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DESIREDDELIVERYDATE, orderDetailsPage.getDesiredDeliveryDate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.EXPECTEDDELIVERYDATE, orderDetailsPage.getExpectedDeliveryDate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.ACTUALDELIVERYDATE, orderDetailsPage.getActualDeliveryDate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.FIRSTREGISTRATIONDATE, orderDetailsPage.getFirstRegistrationDate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYLOCATIONNAMEUI, orderDetailsPage.getDeliveryLocationNameUI());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYENTERPRISENUMBER, orderDetailsPage.getDeliveryEnterpriseNumber());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYADDRESS, orderDetailsPage.getDeliveryAddress());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYCONTACTPERSONUI, orderDetailsPage.getDeliveryContactPersonUI());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYCONTACTEMAILUI, orderDetailsPage.getDeliveryContactEmailUI());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYCONTACTMOBILENUMBERUI, orderDetailsPage.getDeliveryContactMobileNumberUI());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYCONTACTPHONENUMBERUI, orderDetailsPage.getDeliveryContactPhoneNumberUI());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.DELIVERYCONTACTLANGUAGEUI, orderDetailsPage.getDeliveryContactLanguageUI());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.RETURNEDVEHICLE, orderDetailsPage.getReturnedVehicle());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.REGISTEREDONTHENAME, orderDetailsPage.getRegisteredOnTheName());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.RETURNEDVEHICLELICENSEPLATE, orderDetailsPage.getReturnedVehicleLicenseplate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.REGISTRATIONCERTIFICATE, orderDetailsPage.getRegistrationCertificate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.INSURANCECERTIFICATE, orderDetailsPage.getInsuranceCertificate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.CERTIFICATEOFCONFORMITY, orderDetailsPage.getCertificateOfConformity());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.TECHNICALINSPECTIONCERTIFICATE, orderDetailsPage.getTechnicalInspectionCertificate());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.LEGALKIT, orderDetailsPage.getLegalKit());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.SERVICEMANUAL, orderDetailsPage.getServiceManual());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.FUELCARD, orderDetailsPage.getFuelCard());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.NUMBEROFKEYS, orderDetailsPage.getNumberOfKeys());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERNAME, orderDetailsPage.getReturnedVehicleDriverName());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERDATEOFBIRTH, orderDetailsPage.getReturnedVehicleDriverDateOfBirth());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERBIRTHPLACE, orderDetailsPage.getReturnedVehicleDriverBirthplace());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.BUYBACKAPPLICABLE, orderDetailsPage.getBuybackApplicable());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.BUYBACKNAME, orderDetailsPage.getBuybackName());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.BUYBACKADDRESS, orderDetailsPage.getBuybackAddress());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.CONTRACTDURATION, orderDetailsPage.getContractDuration());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.TOTALMILEAGE, orderDetailsPage.getTotalMileage());
		testContext.scenarioContext.setContext(ContextOrderDetailUI.BUYBACKAMOUNT, orderDetailsPage.getBuybackAmount());
		
		
		
//************************************************************ end of set context with data from overview page ****************************************************************************		 

//************************************************************ set context with data from sftp post order **************************************************************************************
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.XMLNS, postOrderImportsRequest.getXmlns());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DOSNR, postOrderImportsRequest.getDosNr());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.TRANSACTIONCODE, postOrderImportsRequest.getTransactionCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.LEASINGCOMPANYRSNUMBER, postOrderImportsRequest.getLeasingCompanyRSNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DEALERRSNUMBER, postOrderImportsRequest.getDealerRSNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.USEREMAIL, postOrderImportsRequest.getUserEmail());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.TRACKINGNUMBER, postOrderImportsRequest.getTrackingNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.COMMENT, postOrderImportsRequest.getComment());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTNAME, postOrderImportsRequest.getClientName());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTCONTACTPERSON, postOrderImportsRequest.getClientContactPerson());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTLANGUAGECODE, postOrderImportsRequest.getClientLanguageCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTVAT, postOrderImportsRequest.getClientVAT());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTSTREETHOUSENUMBER, postOrderImportsRequest.getClientStreetHouseNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTPOSTALCODE, postOrderImportsRequest.getClientPostalCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTCITY, postOrderImportsRequest.getClientCity());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTBILLINGADRES, postOrderImportsRequest.getClientBillingAddress());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTCONTACTEMAIL, postOrderImportsRequest.getClientContactEmail());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTCONTACTPHONENUMBER, postOrderImportsRequest.getClientContactPhoneNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTCONTACTMOBILENUMBER, postOrderImportsRequest.getClientContactMobileNumber().replace(" ", "").trim());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.CLIENTCONTACTFAXNUMBER, postOrderImportsRequest.getClientContactFaxNumber().trim());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERSTREETHOUSENUMBER, postOrderImportsRequest.getDriverStreetHouseNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERPOSTALCODE, postOrderImportsRequest.getDriverPostalCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERCITY, postOrderImportsRequest.getDriverCity());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERCONTACTEMAIL, postOrderImportsRequest.getDriverContactEmail());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERCONTACTPHONENUMBER, postOrderImportsRequest.getDriverContactPhoneNumber().trim());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERCONTACTMOBILENUMBER, postOrderImportsRequest.getDriverContactMobileNumber().trim());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERCONTACTFAXNUMBER, postOrderImportsRequest.getDriverContactFaxNumber().trim());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERNAMEIMPORT, postOrderImportsRequest.getDriverNameImport());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DRIVERLANGUAGECODE, postOrderImportsRequest.getDriverLanguageCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEDESCRIPTION, postOrderImportsRequest.getVehicDescription());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEBRAND, postOrderImportsRequest.getVehicleBrand());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLECYLINDERCONTENT, postOrderImportsRequest.getVehicleCylinderContent());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEPOWER, postOrderImportsRequest.getVehiclePower());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLELANGUAGEPAPERS, postOrderImportsRequest.getVehicleLanguagePapers());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEWEIGHT, postOrderImportsRequest.getVehicleWeight());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLETYREDESCRIPTION, postOrderImportsRequest.getVehicleTyreDescription());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLESTOCKCAR, postOrderImportsRequest.getVehicleStockCar());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLECO2EMISSIONS, postOrderImportsRequest.getVehicleCo2Emissions());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEIDENTIFICATION, postOrderImportsRequest.getVehicleIdentification());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEMODEL, postOrderImportsRequest.getVehicleModel());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEVERSION, postOrderImportsRequest.getVehicleVersion());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEEXTERIORCOLOR, postOrderImportsRequest.getVehicleExteriorColor());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.VEHICLEINTERIORCOLOR, postOrderImportsRequest.getVehicleInteriorColor());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PRICINGTOTALPRICE, postOrderImportsRequest.getPricingTotalPrice());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PRICINGLISTPRICE, postOrderImportsRequest.getPricingListPrice());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PRICINGDELIVERYCOSTS, postOrderImportsRequest.getPricingDeliveryCosts());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PRICINGOTHERCOSTS, postOrderImportsRequest.getPricingOtherCosts());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PRICINGDISCOUNTAMOUNT, postOrderImportsRequest.getPricingDiscountAmount());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PRICINGDISCOUNTPERCENTAGE, postOrderImportsRequest.getPricingDiscountPercentage());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PROMOTIONNAME, postOrderImportsRequest.getPromotionName());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PROMOTIONDESCRIPTION, postOrderImportsRequest.getPromotionDescription());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.PROMOTIONDISCOUNTAMOUNT, postOrderImportsRequest.getPromotionDiscountAmount());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYDESIREDDATE, postOrderImportsRequest.getDeliveryDesiredDate());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONNAME, postOrderImportsRequest.getDeliveryLocationName());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONLANGUAGECODE, postOrderImportsRequest.getDeliveryLocationLanguageCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYCONTACTPERSON, postOrderImportsRequest.getDeliveryContactPerson());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONVAT, postOrderImportsRequest.getDeliveryLocationVAT());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYSTREETHOUSENUMBER, postOrderImportsRequest.getDeliveryStreetHouseNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYPOSTALCODE, postOrderImportsRequest.getDeliveryPostalCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYCITY, postOrderImportsRequest.getDeliveryCity());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONADDRESS, postOrderImportsRequest.getDeliveryLocationAddress());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYCONTACTEMAIL, postOrderImportsRequest.getDeliveryContactEmail());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYCONTACTPHONENUMBER, postOrderImportsRequest.getDeliveryContactPhoneNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYCONTACTMOBILENUMBER, postOrderImportsRequest.getDeliveryContactMobileNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.DELIVERYCONTACTFAXNUMBER, postOrderImportsRequest.getDeliveryContactFaxNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.BUYBACKNAMEINPUT, postOrderImportsRequest.getBuybackName());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.BUYBACKAMOUNTINPUT, postOrderImportsRequest.getBuybackAmount());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.BUYBACKCONTRACTDURATIONINPUT, postOrderImportsRequest.getBuybackContractDuration());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.BUYBACKSTREETHOUSENUMBER, postOrderImportsRequest.getBuybackStreetHouseNumber());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.BUYBACKPOSTALCODE, postOrderImportsRequest.getBuybackPostalCode());
		testContext.scenarioContext.setContext(ContextPostOrderImportsRequest.BUYBACKCITY, postOrderImportsRequest.getBuybackCity());
	
		
				
//************************************************************ end of set context with data from sftp post order ********************************************************************************		
	
//************************************************************ set context with data entered in order page **************************************************************************************
		testContext.scenarioContext.setContext(ContextOrderUI.DEALERDOSSIERNR, orderPage.getDealerDossierNr());
		testContext.scenarioContext.setContext(ContextOrderUI.VIN, orderPage.getVIN());
		testContext.scenarioContext.setContext(ContextOrderUI.VINCONTROLNUMBER, orderPage.getVINControlNumber());
		testContext.scenarioContext.setContext(ContextOrderUI.POWER, orderPage.getPower());
		testContext.scenarioContext.setContext(ContextOrderUI.MAXWEIGHT, orderPage.getMaxWeight());
		testContext.scenarioContext.setContext(ContextOrderUI.ENGINEDISPLACEMENT, orderPage.getEngineDisplacement());
		testContext.scenarioContext.setContext(ContextOrderUI.FUELTYPE, orderPage.getFuelType());
		testContext.scenarioContext.setContext(ContextOrderUI.WIDTH, orderPage.getWidth());
		testContext.scenarioContext.setContext(ContextOrderUI.RELATIONWIDTHHEIGHT, orderPage.getRelationWidthHeight());
		testContext.scenarioContext.setContext(ContextOrderUI.RIMDIAMETER, orderPage.getRimDiameter());
		testContext.scenarioContext.setContext(ContextOrderUI.SPEEDINDEX, orderPage.getSpeedIndex());
		testContext.scenarioContext.setContext(ContextOrderUI.LOADINDEX, orderPage.getLoadIndex());
		testContext.scenarioContext.setContext(ContextOrderUI.MODELYEAR, orderPage.getModelYear());
		testContext.scenarioContext.setContext(ContextOrderUI.LICENSEPLATE, orderPage.getLicensePlate());
		testContext.scenarioContext.setContext(ContextOrderUI.MILEAGE, orderPage.getMileage());
		testContext.scenarioContext.setContext(ContextOrderUI.KEYCODE, orderPage.getKeycode());
		testContext.scenarioContext.setContext(ContextOrderUI.STARTCODE, orderPage.getStartcode());
		testContext.scenarioContext.setContext(ContextOrderUI.NEDC, orderPage.getNEDC());
		testContext.scenarioContext.setContext(ContextOrderUI.WLTP, orderPage.getWLTP());
		testContext.scenarioContext.setContext(ContextOrderUI.TYRESPECS, orderPage.getTyreSpecs());
		testContext.scenarioContext.setContext(ContextOrderUI.TYRETYPE, orderPage.getTyreType());
		testContext.scenarioContext.setContext(ContextOrderUI.TYREBRAND, orderPage.getTyreBrand());
		testContext.scenarioContext.setContext(ContextOrderUI.RETURNEDVEHICLEINPUT, orderPage.getReturnedVehicle());
		testContext.scenarioContext.setContext(ContextOrderUI.OWNERRETURNEDVEHICLEINPUT, orderPage.getOwnerReturnedVehicle());
		testContext.scenarioContext.setContext(ContextOrderUI.LICENSEPLATERETURNEDVEHICLE, orderPage.getLicensePlateReturnedVehicle());
		testContext.scenarioContext.setContext(ContextOrderUI.SELECTREGISTRATIONCERTIFICATE, orderPage.getSelectRegistrationCertificate());
		testContext.scenarioContext.setContext(ContextOrderUI.SELECTINSURANCECERTIFICATE, orderPage.getSelectInsuranceCertificate());
		testContext.scenarioContext.setContext(ContextOrderUI.SELECTCERTIFICATEOFCONFORMITY, orderPage.getSelectCertificateOfConformity());
		testContext.scenarioContext.setContext(ContextOrderUI.SELECTTECHNICALINSPECTIONCERTIFICATE, orderPage.getSelectTechnicalInspectionCertificate());
		testContext.scenarioContext.setContext(ContextOrderUI.SELECTLEGALKIT, orderPage.getSelectLegalKit());
		testContext.scenarioContext.setContext(ContextOrderUI.SELECTSERVICEMANUAL, orderPage.getSelectServiceManual());
		testContext.scenarioContext.setContext(ContextOrderUI.SELECTFUELCARD, orderPage.getSelectFuelCard());
		testContext.scenarioContext.setContext(ContextOrderUI.NUMBEROFKEYSINPUT, orderPage.getNumberOfKeys());
		testContext.scenarioContext.setContext(ContextOrderUI.RETURNEDVEHICLEDRIVERNAMEINPUT, orderPage.getDriverName());
		testContext.scenarioContext.setContext(ContextOrderUI.RETURNEDVEHICLEDRIVERDATEOFBIRTHINPUT, orderPage.getDriverDateOfBirth());
		testContext.scenarioContext.setContext(ContextOrderUI.RETURNEDVEHICLEDRIVERBIRTHPLACEINPUT, orderPage.getDriverBirthPlace());
		testContext.scenarioContext.setContext(ContextOrderUI.DATEFIRSTREGISTRATION, orderPage.getDateFirstRegistration());
		testContext.scenarioContext.setContext(ContextOrderUI.DRIVERFIRSTNAME, orderPage.getDriverFirstName());
		testContext.scenarioContext.setContext(ContextOrderUI.DRIVERLASTNAME, orderPage.getDriverLastName());
		testContext.scenarioContext.setContext(ContextOrderUI.DRIVERNAME, orderPage.getDriverName());
		testContext.scenarioContext.setContext(ContextOrderUI.DRIVERBIRTHPLACE, orderPage.getDriverBirthPlace());
		testContext.scenarioContext.setContext(ContextOrderUI.DRIVERDATEOFBIRTH, orderPage.getDriverDateOfBirth());

		
//************************************************************ end of set context with data entered in order page ********************************************************************************		
		
		
//************************************************************ set context leasing company actor api **************************************************************************************
				logger.debug("Start get leasing company Actor");
		logger.debug("LCBILLINGADDRESS: " + getLeasingCompanyActorById.getLcBillingAddress());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCBILLINGADDRESS, getLeasingCompanyActorById.getLcBillingAddress());
		logger.debug("LCCOMMERCIALNAME: " + getLeasingCompanyActorById.getLcCommercialName());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCCOMMERCIALNAME, getLeasingCompanyActorById.getLcCommercialName());
		logger.debug("LCCONTACTEMAIL: " + getLeasingCompanyActorById.getLcContactEmail());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTEMAIL, getLeasingCompanyActorById.getLcContactEmail());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTFULLNAME, getLeasingCompanyActorById.getLcContactFullName());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTMOBILENUMBER, getLeasingCompanyActorById.getLcContactMobileNumber());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTPHONENUMBER, getLeasingCompanyActorById.getLcContactPhoneNumber());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCENTERPRISENUMBER, getLeasingCompanyActorById.getLcEnterpriseNumber());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCINVOICENAME, getLeasingCompanyActorById.getLcInvoiceName());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCLANGUAGE, getLeasingCompanyActorById.getLcLanguage());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCRSNUMBER, getLeasingCompanyActorById.getLcRsNumber());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCSTREET, getLeasingCompanyActorById.getLcStreet());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCHOUSENR, getLeasingCompanyActorById.getLcHouseNr());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCZIPCODE, getLeasingCompanyActorById.getLcZipcode());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCCITY, getLeasingCompanyActorById.getLcCity());
				testContext.scenarioContext.setContext(ContextGetLeasingCompanyActorByIdApi.LCCOUNTRY, getLeasingCompanyActorById.getLcCountry());
				logger.debug("End get leasing company Actor");
				
//************************************************************ end of set context leasing company actor api ********************************************************************************		

				//************************************************************ set context supplier actor api **************************************************************************************
				logger.debug("Start get supplier Actor");
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORBILLINGADDRESS, getSupplierActorById.getBillingAddress());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORINVOICENAME, getSupplierActorById.getInvoiceName());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORENTERPRISENUMBER, getSupplierActorById.getEnterpriseNumber());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORRSNUMBER, getSupplierActorById.getRsNumber());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOUNTRY, getSupplierActorById.getCountry());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOMMERCIALNAME, getSupplierActorById.getCommercialName());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTEMAIL, getSupplierActorById.getContactEmail());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER, getSupplierActorById.getContactMobileNumber());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTPHONENUMBER, getSupplierActorById.getContactPhoneNumber());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORLANGUAGE, getSupplierActorById.getLanguage());
				testContext.scenarioContext.setContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTFULLNAME, getSupplierActorById.getContactFullName());
				logger.debug("End get supplier Actor");


//************************************************************ end of set context supplier actor api ********************************************************************************		

				
		
//********************************************** assertions of data from enter pages compared with data from overview page******************************************************************	
		
		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
		logger.debug("LEASINGCOMPANYDOSSIERNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYDOSSIERNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DOSNR));
		ah.assertEquals("LEASINGCOMPANYDOSSIERNUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYDOSSIERNUMBER),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DOSNR));
		logger.debug("ORDERNUMBERSUPPLIER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.ORDERNUMBERSUPPLIER)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.DEALERDOSSIERNR));
		if((String) testContext.scenarioContext.getContext(ContextOrderUI.DEALERDOSSIERNR) == null) {
			String ddosnr = "―";
			ah.assertEquals("ORDERNUMBERSUPPLIER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.ORDERNUMBERSUPPLIER), ddosnr);
		} else {
			ah.assertEquals("ORDERNUMBERSUPPLIER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.ORDERNUMBERSUPPLIER),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.DEALERDOSSIERNR));
		}

		//Leasing company
		logger.debug("LEASINGCOMPANYNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYNAME)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCINVOICENAME));
		ah.assertEquals("LEASINGCOMPANYNAME ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYNAME),
				(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCINVOICENAME));
		logger.debug("LEASINGCOMPANYENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCENTERPRISENUMBER));
		ah.assertEquals("LEASINGCOMPANYENTERPRISENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYENTERPRISENUMBER),
				(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCENTERPRISENUMBER));
		logger.debug("LEASINGCOMPANYRSNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYRSNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCRSNUMBER));
		ah.assertEquals("LEASINGCOMPANYRSNUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYRSNUMBER),
				(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCRSNUMBER));
		logger.debug("LEASINGCOMPANYBILLINGADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYBILLINGADDRESS)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCBILLINGADDRESS));
		ah.assertEquals("LEASINGCOMPANYBILLINGADDRESS ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYBILLINGADDRESS),
				(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCBILLINGADDRESS));
		logger.debug("LEASINGCOMPANYCOUNTRY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCOUNTRY)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOUNTRY));
		ah.assertEquals("LEASINGCOMPANYCOUNTRY ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCOUNTRY),
				(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOUNTRY));
		//TODO Deze naam komt uit dossiermanager, kan via get /rs-number/{rsNumber}/dossier-managers request opgehaald worden, nog implementeren
		//logger.debug("LEASINGCOMPANYCONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPERSON)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOMMERCIALNAME));
		//assertEquals(
				//(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPERSON),
				//(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOMMERCIALNAME));
		//TODO uitzoeken waarom emailaddress leeg is, is wel gevuld in lc actor , Tim vragen om uitleg
		//logger.debug("LEASINGCOMPANYCONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTEMAIL)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTEMAIL));
		//assertEquals(
				//(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTEMAIL),
				//(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTEMAIL));
		logger.debug("LEASINGCOMPANYCONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTMOBILENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTMOBILENUMBER));
		ah.assertEquals("LEASINGCOMPANYCONTACTMOBILENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTMOBILENUMBER),
				(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTMOBILENUMBER));
		logger.debug("LEASINGCOMPANYCONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPHONENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTPHONENUMBER));
		ah.assertEquals("LEASINGCOMPANYCONTACTPHONENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPHONENUMBER),
				(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTPHONENUMBER));
		if(System.getProperty("language").equals("fr")) {
			logger.debug("LEASINGCOMPANYCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCLANGUAGE));
			ah.assertEquals("LEASINGCOMPANYCONTACTLANGUAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTLANGUAGE),
					"NÉERLANDAIS");

		} else {
			logger.debug("LEASINGCOMPANYCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCLANGUAGE));
			ah.assertEquals("LEASINGCOMPANYCONTACTLANGUAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTLANGUAGE),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCLANGUAGE));
		}

		//Supplier
		logger.debug("SUPPLIERNAMETITLE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAMETITLE)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOMMERCIALNAME));
		ah.assertEquals("SUPPLIERNAMETITLE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAMETITLE),
				(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOMMERCIALNAME));
		logger.debug("SUPPLIERNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAME)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORINVOICENAME));
		ah.assertEquals("SUPPLIERNAME ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAME),
				(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORINVOICENAME));
		logger.debug("SUPPLIERENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORENTERPRISENUMBER));
		ah.assertEquals("SUPPLIERENTERPRISENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERENTERPRISENUMBER),
				(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORENTERPRISENUMBER));
		logger.debug("SUPPLIERRSNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERRSNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORRSNUMBER));
		ah.assertEquals("SUPPLIERRSNUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERRSNUMBER),
				(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORRSNUMBER));
		logger.debug("SUPPLIERBILLINGADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERBILLINGADDRESS)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORBILLINGADDRESS));
		//TODO activate assertion when UI is fixed
		//ah.assertEquals("SUPPLIERBILLINGADDRESS ",
		//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERBILLINGADDRESS),
		//		(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORBILLINGADDRESS));
		logger.debug("SUPPLIERCOUNTRY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCOUNTRY)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOUNTRY));
		ah.assertEquals("SUPPLIERCOUNTRY",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCOUNTRY),
				(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOUNTRY));
		logger.debug("SUPPLIERCONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPERSON)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTFULLNAME));
		ah.assertEquals("SUPPLIERCONTACTPERSON ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPERSON),
				(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTFULLNAME));
		logger.debug("SUPPLIERCONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTEMAIL)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTEMAIL));
		ah.assertEquals("SUPPLIERCONTACTEMAIL ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTEMAIL),
				(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTEMAIL));
		if(testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTMOBILENUMBER).equals("―")) {
			logger.debug("SUPPLIERCONTACTMOBILENUMBER: null");
			ah.assertNull("SUPPLIERCONTACTMOBILENUMBER is null ", testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
		} else {
			logger.debug("SUPPLIERCONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTMOBILENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
			ah.assertEquals("SUPPLIERCONTACTMOBILENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTMOBILENUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
		}
		if(testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPHONENUMBER).equals("―")) {
			logger.debug("SUPPLIERCONTACTPHONENUMBER: null");
			ah.assertNull("SUPPLIERCONTACTPHONENUMBER is null", testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
		} else {
			logger.debug("SUPPLIERCONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPHONENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTPHONENUMBER));
			ah.assertEquals("SUPPLIERCONTACTPHONENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPHONENUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTPHONENUMBER));
		}

		if(System.getProperty("language").equals("fr")) {
			logger.debug("SUPPLIERCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORLANGUAGE));
			//ah.assertEquals("SUPPLIERCONTACTLANGUAGE ",
			//		"NÉERLANDAIS",
			//		(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORLANGUAGE));
		} else {
			logger.debug("SUPPLIERCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORLANGUAGE));
			ah.assertEquals("SUPPLIERCONTACTLANGUAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTLANGUAGE),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORLANGUAGE));
		}

		
	
		//Lessee
		logger.debug("LESSEEENAMETITLE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENAMETITLE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTNAME));
		ah.assertEquals("LESSEEENAMETITLE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENAMETITLE),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTNAME));
		if(System.getProperty("language").equals("fr")) {
			logger.debug("LESSEEENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTVAT));
			ah.assertEquals("LESSEEENTERPRISENUMBER ",
					"BE 0414.325.701",
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTVAT));
		} else {
			logger.debug("LESSEEENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTVAT));
			ah.assertEquals("LESSEEENTERPRISENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENTERPRISENUMBER),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTVAT));
		}

		logger.debug("LESSEEBILLINGADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEBILLINGADDRESS)+" should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTBILLINGADRES));
		ah.assertEquals("LESSEEBILLINGADDRESS ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEBILLINGADDRESS),	
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTBILLINGADRES));
		logger.debug("LESSEECONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPERSON)+" should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTPERSON));
		ah.assertEquals("LESSEECONTACTPERSON ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPERSON), 
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTPERSON));
		logger.debug("LESSEECONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTEMAIL)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTEMAIL));
		ah.assertEquals("LESSEECONTACTEMAIL ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTEMAIL),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTEMAIL));
		logger.debug("LESSEECONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTMOBILENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTMOBILENUMBER));
		String lesseeMobNr = (String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTMOBILENUMBER);
			lesseeMobNr = lesseeMobNr.replace(".", "").replace("-", "");
		ah.assertEquals("LESSEECONTACTMOBILENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTMOBILENUMBER), lesseeMobNr.trim());
		logger.debug("LESSEECONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPHONENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTPHONENUMBER));
			String lesseePhoneNr = (String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTCONTACTPHONENUMBER);
			lesseePhoneNr = lesseePhoneNr.replace(" ", "");
		ah.assertEquals("LESSEECONTACTPHONENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPHONENUMBER).toString().replace("-", ""),	lesseePhoneNr.replace("-", ""));
		if(System.getProperty("language").equals("fr")) {

		} else {
			logger.debug("LESSEECONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTLANGUAGECODE));
			ah.assertEquals("LESSEECONTACTLANGUAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTLANGUAGE),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTLANGUAGECODE));
		}

		logger.debug("DRIVERADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERADDRESS)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERADDRESS));
		ah.assertEquals("DRIVERADDRESS ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERADDRESS),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERADDRESS));
		logger.debug("DRIVERCONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPERSON)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERNAMEIMPORT));
		ah.assertEquals("DRIVERCONTACTPERSON ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPERSON),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERNAMEIMPORT));
		logger.debug("DRIVERCONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTEMAIL)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTEMAIL));
		ah.assertEquals("DRIVERCONTACTEMAIL ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTEMAIL),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTEMAIL));
		logger.debug("DRIVERCONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTMOBILENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTMOBILENUMBER));
		ah.assertEquals("DRIVERCONTACTMOBILENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTMOBILENUMBER),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTMOBILENUMBER));
		logger.debug("DRIVERCONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPHONENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTPHONENUMBER));
		ah.assertEquals("DRIVERCONTACTPHONENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPHONENUMBER),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTPHONENUMBER));
		if(System.getProperty("language").equals("fr")) {

		} else {
			logger.debug("DRIVERCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERLANGUAGECODE));
			ah.assertEquals("DRIVERCONTACTLANGUAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTLANGUAGE),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERLANGUAGECODE));
		}

		//Vehicle information
		logger.debug("VEHICLEDESCRIPTIONTITLE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEDESCRIPTIONTITLE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEDESCRIPTION));
		ah.assertEquals("VEHICLEDESCRIPTIONTITLE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEDESCRIPTIONTITLE),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEDESCRIPTION));
		logger.debug("VEHICLEVIN: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVIN)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.VIN));
		ah.assertEquals("VEHICLEVIN ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVIN),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.VIN));
		logger.debug("VEHICLEVINCONTROLNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVINCONTROLNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.VINCONTROLNUMBER));
		ah.assertEquals("VEHICLEVINCONTROLNUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVINCONTROLNUMBER),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.VINCONTROLNUMBER));
		logger.debug("VEHICLEMODELYEAR: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMODELYEAR)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.MODELYEAR));
		ah.assertEquals("VEHICLEMODELYEAR ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMODELYEAR),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.MODELYEAR));
		logger.debug("VEHICLEIDENTIFICATIONCODE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEIDENTIFICATIONCODE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEIDENTIFICATION));
		ah.assertEquals("VEHICLEIDENTIFICATIONCODE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEIDENTIFICATIONCODE),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEIDENTIFICATION));
		logger.debug("VEHICLELICENSEPLATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELICENSEPLATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.LICENSEPLATE));
		ah.assertEquals("VEHICLELICENSEPLATE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELICENSEPLATE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.LICENSEPLATE));
		if(System.getProperty("language").equals("fr")) {

		} else {
			logger.debug("VEHICLESTOCK: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTOCK)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLESTOCKCAR));
			ah.assertEquals("VEHICLESTOCK ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTOCK),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLESTOCKCAR));
		}

		logger.debug("VEHICLEMILEAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMILEAGE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.MILEAGE));
		ah.assertEquals("VEHICLEMILEAGE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMILEAGE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.MILEAGE));
		if(System.getProperty("language").equals("fr")) {

		} else {
			if ((String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELANGUAGEDOCUMENTS) != null) {
				logger.debug("VEHICLELANGUAGEDOCUMENTS: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELANGUAGEDOCUMENTS) + " should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLELANGUAGEPAPERS));
				ah.assertEquals("VEHICLELANGUAGEDOCUMENTS ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELANGUAGEDOCUMENTS),
						//(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLELANGUAGEPAPERS));
						"French");
			} else {
				ah.assertNull("VEHICLELANGUAGEDOCUMENTS ", (String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELANGUAGEDOCUMENTS));
			}
		}

		logger.debug("VEHICLEKEYCODE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEKEYCODE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.KEYCODE));
		ah.assertEquals("VEHICLEKEYCODE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEKEYCODE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.KEYCODE));
		logger.debug("VEHICLESTARTCODE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTARTCODE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.STARTCODE));
		ah.assertEquals("VEHICLESTARTCODE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTARTCODE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.STARTCODE));
		//Technical specifications
		logger.debug("MOTORTYPE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.MOTORTYPE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEVERSION));
		ah.assertEquals("MOTORTYPE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.MOTORTYPE),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEVERSION));
		logger.debug("POWER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.POWER)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.POWER));
		ah.assertEquals("POWER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.POWER),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.POWER));
		logger.debug("ENGINECAPACITY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.ENGINECAPACITY)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.ENGINEDISPLACEMENT));
		ah.assertEquals("ENGINECAPACITY ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.ENGINECAPACITY),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLECYLINDERCONTENT));
		if(System.getProperty("language").equals("fr")) {

		} else {
			logger.debug("NEDCVALUE: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.NEDCVALUE) + " should equal " + testContext.scenarioContext.getContext(ContextOrderUI.NEDC));
			ah.assertEquals("NEDCVALUE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.NEDCVALUE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.NEDC) + ".00");
		}
			if(System.getProperty("language").equals("fr")) {

			} else {
				logger.debug("WLTPVALUE: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.WLTPVALUE) + " should equal " + testContext.scenarioContext.getContext(ContextOrderUI.WLTP));
				ah.assertEquals("WLTPVALUE ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.WLTPVALUE),
						(String) testContext.scenarioContext.getContext(ContextOrderUI.WLTP) + ".00");
			}
		logger.debug("INTERIORCOLOR: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.INTERIORCOLOR)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEINTERIORCOLOR));
		ah.assertEquals("INTERIORCOLOR ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.INTERIORCOLOR),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEINTERIORCOLOR));
		logger.debug("TYRESPECIFICATIONS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRESPECIFICATIONS)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.TYRESPECS));
		ah.assertEquals("TYRESPECIFICATIONS ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRESPECIFICATIONS),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.TYRESPECS));
		logger.debug("TYREBRAND: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TYREBRAND)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.TYREBRAND));
		ah.assertEquals("TYREBRAND ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TYREBRAND),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.TYREBRAND));
		logger.debug("TYRETYPE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRETYPE)+" should equal Summer");
		ah.assertEquals("TYRETYPE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRETYPE),
				"SUMMER");
		logger.debug("WEIGHT: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.WEIGHT)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEWEIGHT));
		ah.assertEquals("WEIGHT ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.WEIGHT),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEWEIGHT));
		logger.debug("EXTERIORCOLOR: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.EXTERIORCOLOR)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEEXTERIORCOLOR));
		ah.assertEquals("EXTERIORCOLOR ",
				//"Blablabla",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.EXTERIORCOLOR),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEEXTERIORCOLOR));
		
		//Pricing and optionslogger.debug("VEHICLEPRICE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEPRICE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGLISTPRICE));
			if(System.getProperty("language").equals("fr")) {

			} else {
				ah.assertEquals("VEHICLEPRICE ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEPRICE),
						(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGLISTPRICE));
			}
			if(System.getProperty("language").equals("fr")) {

			} else {
				logger.debug("PROMOTIONDISCOUNT: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.PROMOTIONDISCOUNT) + " should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PROMOTIONDISCOUNTAMOUNT));
				ah.assertEquals("PROMOTIONDISCOUNT ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.PROMOTIONDISCOUNT),
						(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PROMOTIONDISCOUNTAMOUNT));
			}
			if(System.getProperty("language").equals("fr")) {

			} else {
				logger.debug("DELIVERYCOSTS: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCOSTS) + " should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGDELIVERYCOSTS));
				ah.assertEquals("DELIVERYCOSTS ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCOSTS),
						(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGDELIVERYCOSTS));
			}

			if(System.getProperty("language").equals("fr")) {

			} else {
				logger.debug("OTHERCOSTS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.OTHERCOSTS)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGOTHERCOSTS));
				ah.assertEquals("OTHERCOSTS ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.OTHERCOSTS),
						(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGOTHERCOSTS));
			}

			if(System.getProperty("language").equals("fr")) {

			} else {
				logger.debug("TOTALPRICE: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.TOTALPRICE) + " should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGTOTALPRICE));
				ah.assertEquals("TOTALPRICE ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TOTALPRICE),
						(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGTOTALPRICE));
			}
		//TODO Delivery information dates
		
		//Different delivery address
		logger.debug("DELIVERYLOCATIONNAMEUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYLOCATIONNAMEUI)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONNAME));
		ah.assertEquals("DELIVERYLOCATIONNAMEUI ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYLOCATIONNAMEUI),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONNAME));
		logger.debug("DELIVERYENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONVAT));
		ah.assertEquals("DELIVERYENTERPRISENUMBER ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYENTERPRISENUMBER),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONVAT));
		logger.debug("DELIVERYADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYADDRESS)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONADDRESS));
		ah.assertEquals("DELIVERYADDRESS ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYADDRESS),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONADDRESS));
		logger.debug("DELIVERYCONTACTPERSONUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPERSONUI)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTPERSON));
		ah.assertEquals("DELIVERYCONTACTPERSONUI ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPERSONUI),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTPERSON));

		logger.debug("DELIVERYCONTACTEMAILUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTEMAILUI)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTEMAIL));
		ah.assertEquals("DELIVERYCONTACTEMAILUI ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTEMAILUI),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTEMAIL));
		logger.debug("DELIVERYCONTACTMOBILENUMBERUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTMOBILENUMBERUI)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTMOBILENUMBER));
		String deliveryContMobNr = (String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTMOBILENUMBER);
			deliveryContMobNr = deliveryContMobNr.replace("-", "").replace(".", "").replace(" ", "");
		ah.assertEquals("DELIVERYCONTACTMOBILENUMBERUI ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTMOBILENUMBERUI), deliveryContMobNr);
		logger.debug("DELIVERYCONTACTPHONENUMBERUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPHONENUMBERUI)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTPHONENUMBER));
		String deliveryContPhoneNr = (String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYCONTACTPHONENUMBER);
			deliveryContPhoneNr = deliveryContPhoneNr.replace(" ", "");
		ah.assertEquals("DELIVERYCONTACTPHONENUMBERUI ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPHONENUMBERUI),	deliveryContPhoneNr.replace("-", ""));
			if(System.getProperty("language").equals("fr")) {

			} else {
		if(testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTLANGUAGEUI) != null) {
			logger.debug("DELIVERYCONTACTLANGUAGEUI: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTLANGUAGEUI) + " should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONLANGUAGECODE));
			ah.assertEquals("DELIVERYCONTACTLANGUAGEUI ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTLANGUAGEUI),
					//(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DELIVERYLOCATIONLANGUAGECODE));
					"Dutch");
		}
		}
		//Equipment and enclosed documents
		logger.debug("RETURNEDVEHICLE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEINPUT));
		ah.assertEquals("RETURNEDVEHICLE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEINPUT));
		logger.debug("REGISTEREDONTHENAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTEREDONTHENAME)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.OWNERRETURNEDVEHICLEINPUT));
		ah.assertEquals("REGISTEREDONTHENAME ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTEREDONTHENAME),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.OWNERRETURNEDVEHICLEINPUT));
		logger.debug("RETURNEDVEHICLELICENSEPLATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLELICENSEPLATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.LICENSEPLATERETURNEDVEHICLE));
		ah.assertEquals("RETURNEDVEHICLELICENSEPLATE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLELICENSEPLATE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.LICENSEPLATERETURNEDVEHICLE));
		logger.debug("REGISTRATIONCERTIFICATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTRATIONCERTIFICATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTREGISTRATIONCERTIFICATE));
		ah.assertEquals("REGISTRATIONCERTIFICATE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTRATIONCERTIFICATE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTREGISTRATIONCERTIFICATE));
		logger.debug("INSURANCECERTIFICATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.INSURANCECERTIFICATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTINSURANCECERTIFICATE));
		ah.assertEquals("INSURANCECERTIFICATE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.INSURANCECERTIFICATE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTINSURANCECERTIFICATE));
		logger.debug("CERTIFICATEOFCONFORMITY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.CERTIFICATEOFCONFORMITY)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTCERTIFICATEOFCONFORMITY));
		ah.assertEquals("CERTIFICATEOFCONFORMITY ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.CERTIFICATEOFCONFORMITY),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTCERTIFICATEOFCONFORMITY));
		logger.debug("TECHNICALINSPECTIONCERTIFICATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TECHNICALINSPECTIONCERTIFICATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTTECHNICALINSPECTIONCERTIFICATE));
		ah.assertEquals("TECHNICALINSPECTIONCERTIFICATE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TECHNICALINSPECTIONCERTIFICATE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTTECHNICALINSPECTIONCERTIFICATE));
		logger.debug("LEGALKIT: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEGALKIT)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTLEGALKIT));
		ah.assertEquals("LEGALKIT ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEGALKIT),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTLEGALKIT));
		logger.debug("SERVICEMANUAL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SERVICEMANUAL)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTSERVICEMANUAL));
		ah.assertEquals("SERVICEMANUAL ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SERVICEMANUAL),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTSERVICEMANUAL));
		logger.debug("FUELCARD: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.FUELCARD)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTFUELCARD));
		ah.assertEquals("FUELCARD ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.FUELCARD),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTFUELCARD));
		logger.debug("NUMBEROFKEYS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.NUMBEROFKEYS)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.NUMBEROFKEYSINPUT));
		ah.assertEquals("NUMBEROFKEYS ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.NUMBEROFKEYS),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.NUMBEROFKEYSINPUT));
		logger.debug("RETURNEDVEHICLEDRIVERNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERNAME)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERNAMEINPUT));
		//TODO activate assertion when UI is fixed
		//ah.assertEquals("RETURNEDVEHICLEDRIVERNAME ",
		//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERNAME),
		//		(String) testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERNAMEINPUT));
		//logger.debug("RETURNEDVEHICLEDRIVERDATEOFBIRTH: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERDATEOFBIRTH)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERDATEOFBIRTHINPUT));
		//ah.assertEquals("RETURNEDVEHICLEDRIVERDATEOFBIRTH ",
		//		"1965-06-19",
		//		(String) testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERDATEOFBIRTHINPUT));
		logger.debug("RETURNEDVEHICLEDRIVERBIRTHPLACE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERBIRTHPLACE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERBIRTHPLACEINPUT));
		ah.assertEquals("RETURNEDVEHICLEDRIVERBIRTHPLACE ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERBIRTHPLACE),
				(String) testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERBIRTHPLACEINPUT));
		logger.debug("BUYBACKNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKNAME)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKNAMEINPUT));
		ah.assertEquals("BUYBACKNAME ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKNAME),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKNAMEINPUT));
		String BuybackAddressInput = testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKSTREETHOUSENUMBER) + " , " + 
				testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKPOSTALCODE) + " " + 
				testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKCITY);
		logger.debug("BUYBACKADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKADDRESS)+" should equal " + BuybackAddressInput);
		ah.assertEquals("BUYBACKADDRESS ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKADDRESS), BuybackAddressInput);
		logger.debug("CONTRACTDURATION: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.CONTRACTDURATION)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKCONTRACTDURATIONINPUT));
		ah.assertEquals("CONTRACTDURATION ",
				(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.CONTRACTDURATION),
				(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKCONTRACTDURATIONINPUT));
			if(System.getProperty("language").equals("fr")) {

			} else {
				logger.debug("BUYBACKAMOUNT: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKAMOUNT) + " should equal " + testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKAMOUNTINPUT));
				ah.assertEquals("BUYBACKAMOUNT ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKAMOUNT),
						(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKAMOUNTINPUT));
			}

		ah.processAllAssertions();
		}
		else if(System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			
			logger.debug("LEASINGCOMPANYDOSSIERNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYDOSSIERNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DOSNR));
			ah.assertEquals("LEASINGCOMPANYDOSSIERNUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYDOSSIERNUMBER),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DOSNR));
			logger.debug("ORDERNUMBERSUPPLIER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.ORDERNUMBERSUPPLIER)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.DEALERDOSSIERNR));
			ah.assertEquals("ORDERNUMBERSUPPLIER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.ORDERNUMBERSUPPLIER),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.DEALERDOSSIERNR));
			
			//Leasing company
			logger.debug("LEASINGCOMPANYNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYNAME)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCINVOICENAME));
			ah.assertEquals("LEASINGCOMPANYNAME ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYNAME),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCINVOICENAME));
			logger.debug("LEASINGCOMPANYENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCENTERPRISENUMBER));
			ah.assertEquals("LEASINGCOMPANYENTERPRISENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYENTERPRISENUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCENTERPRISENUMBER));
			logger.debug("LEASINGCOMPANYRSNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYRSNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCRSNUMBER));
			ah.assertEquals("LEASINGCOMPANYRSNUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYRSNUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCRSNUMBER));
			logger.debug("LEASINGCOMPANYBILLINGADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYBILLINGADDRESS)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCBILLINGADDRESS));
			ah.assertEquals("LEASINGCOMPANYBILLINGADDRESS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYBILLINGADDRESS),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCBILLINGADDRESS));
			logger.debug("LEASINGCOMPANYCOUNTRY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCOUNTRY)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOUNTRY));
			ah.assertEquals("LEASINGCOMPANYCOUNTRY ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCOUNTRY),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOUNTRY));
			//TODO Deze naam komt uit dossiermanager, kan via get /rs-number/{rsNumber}/dossier-managers request opgehaald worden, nog implementeren
			//logger.debug("LEASINGCOMPANYCONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPERSON)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOMMERCIALNAME));
			//assertEquals(
					//(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPERSON),
					//(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCOMMERCIALNAME));
			//TODO uitzoeken waarom emailaddress leeg is, is wel gevuld in lc actor , Tim vragen om uitleg
			//logger.debug("LEASINGCOMPANYCONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTEMAIL)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTEMAIL));
			//assertEquals(
					//(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTEMAIL),
					//(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTEMAIL));
			logger.debug("LEASINGCOMPANYCONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTMOBILENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTMOBILENUMBER));
			ah.assertEquals("LEASINGCOMPANYCONTACTMOBILENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTMOBILENUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTMOBILENUMBER));
			logger.debug("LEASINGCOMPANYCONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPHONENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTPHONENUMBER));
			ah.assertEquals("LEASINGCOMPANYCONTACTPHONENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTPHONENUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCCONTACTPHONENUMBER));
			logger.debug("LEASINGCOMPANYCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCLANGUAGE));
			ah.assertEquals("LEASINGCOMPANYCONTACTLANGUAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEASINGCOMPANYCONTACTLANGUAGE),
					(String) testContext.scenarioContext.getContext(ContextGetLeasingCompanyActorByIdApi.LCLANGUAGE));
			
			//Supplier
			logger.debug("SUPPLIERNAMETITLE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAMETITLE)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOMMERCIALNAME));
			ah.assertEquals("SUPPLIERNAMETITLE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAMETITLE),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOMMERCIALNAME));
			logger.debug("SUPPLIERNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAME)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORINVOICENAME));
			ah.assertEquals("SUPPLIERNAME ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERNAME),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORINVOICENAME));
			logger.debug("SUPPLIERENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORENTERPRISENUMBER));
			ah.assertEquals("SUPPLIERENTERPRISENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERENTERPRISENUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORENTERPRISENUMBER));
			logger.debug("SUPPLIERRSNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERRSNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORRSNUMBER));
			ah.assertEquals("SUPPLIERRSNUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERRSNUMBER),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORRSNUMBER));
			logger.debug("SUPPLIERBILLINGADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERBILLINGADDRESS)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORBILLINGADDRESS));
			//TODO activate
			//ah.assertEquals("SUPPLIERBILLINGADDRESS ",
			//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERBILLINGADDRESS),
			//		(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORBILLINGADDRESS));
			logger.debug("SUPPLIERCOUNTRY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCOUNTRY)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOUNTRY));
			ah.assertEquals("SUPPLIERCOUNTRY",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCOUNTRY),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCOUNTRY));
			logger.debug("SUPPLIERCONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPERSON)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTFULLNAME));
			ah.assertEquals("SUPPLIERCONTACTPERSON ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPERSON),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTFULLNAME));
			logger.debug("SUPPLIERCONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTEMAIL)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTEMAIL));
			ah.assertEquals("SUPPLIERCONTACTEMAIL ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTEMAIL),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTEMAIL));
			if(testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTMOBILENUMBER).equals("―")) {
				logger.debug("SUPPLIERCONTACTMOBILENUMBER: null");
				ah.assertNull("SUPPLIERCONTACTMOBILENUMBER is null ", testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
			} else {
				logger.debug("SUPPLIERCONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTMOBILENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
				ah.assertEquals("SUPPLIERCONTACTMOBILENUMBER ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTMOBILENUMBER),
						(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
			}
			if(testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPHONENUMBER).equals("―")) {
				logger.debug("SUPPLIERCONTACTPHONENUMBER: null");
				ah.assertNull("SUPPLIERCONTACTPHONENUMBER is null", testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTMOBILENUMBER));
			} else {
				logger.debug("SUPPLIERCONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPHONENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTPHONENUMBER));
				ah.assertEquals("SUPPLIERCONTACTPHONENUMBER ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTPHONENUMBER),
						(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORCONTACTPHONENUMBER));
			}
			logger.debug("SUPPLIERCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTLANGUAGE)+" should equal "+testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORLANGUAGE));
			ah.assertEquals("SUPPLIERCONTACTLANGUAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SUPPLIERCONTACTLANGUAGE),
					(String) testContext.scenarioContext.getContext(ContextGetSupplierActorByIdApi.SUPPLIERACTORLANGUAGE));
			
		
			//Lessee
			logger.debug("LESSEEENAMETITLE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENAMETITLE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTNAME));
			ah.assertEquals("LESSEEENAMETITLE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENAMETITLE),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTNAME));
			logger.debug("LESSEEENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENTERPRISENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTVAT));
			ah.assertEquals("LESSEEENTERPRISENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEENTERPRISENUMBER),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.CLIENTVAT));
			logger.debug("LESSEEBILLINGADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEBILLINGADDRESS)+" should equal ―");
			ah.assertEquals("LESSEEBILLINGADDRESS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEEBILLINGADDRESS),	"―");
			logger.debug("LESSEECONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPERSON)+" should equal ―");
			ah.assertEquals("LESSEECONTACTPERSON ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPERSON), "―");
			logger.debug("LESSEECONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTEMAIL)+" should equal ―");
			ah.assertEquals("LESSEECONTACTEMAIL ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTEMAIL), "―");
			logger.debug("LESSEECONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTMOBILENUMBER)+" should equal ―");
			ah.assertEquals("LESSEECONTACTMOBILENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTMOBILENUMBER), "―");
			logger.debug("LESSEECONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPHONENUMBER)+" should equal ―");
			ah.assertEquals("LESSEECONTACTPHONENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTPHONENUMBER),	"―");
			//logger.debug("LESSEECONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTLANGUAGE)+" should equal ―");
			//ah.assertEquals("LESSEECONTACTLANGUAGE ",
			//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LESSEECONTACTLANGUAGE), "LANGUAGES.NULL");
			logger.debug("DRIVERADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERADDRESS)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERADDRESS));
			ah.assertEquals("DRIVERADDRESS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERADDRESS),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERADDRESS));
			logger.debug("DRIVERCONTACTPERSON: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPERSON)+" should equal ―");
			ah.assertEquals("DRIVERCONTACTPERSON ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPERSON),	"―");
			logger.debug("DRIVERCONTACTEMAIL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTEMAIL)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTEMAIL));
			ah.assertEquals("DRIVERCONTACTEMAIL ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTEMAIL),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTEMAIL));
			logger.debug("DRIVERCONTACTMOBILENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTMOBILENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTMOBILENUMBER));
			ah.assertEquals("DRIVERCONTACTMOBILENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTMOBILENUMBER),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTMOBILENUMBER));
			logger.debug("DRIVERCONTACTPHONENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPHONENUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTPHONENUMBER));
			ah.assertEquals("DRIVERCONTACTPHONENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTPHONENUMBER),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.DRIVERCONTACTPHONENUMBER));
			//logger.debug("DRIVERCONTACTLANGUAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTLANGUAGE)+" should equal ―");
			//ah.assertEquals("DRIVERCONTACTLANGUAGE ",
			//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DRIVERCONTACTLANGUAGE), "LANGUAGES.NULL");
			//Vehicle information
			logger.debug("VEHICLEDESCRIPTIONTITLE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEDESCRIPTIONTITLE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEDESCRIPTION));
			ah.assertEquals("VEHICLEDESCRIPTIONTITLE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEDESCRIPTIONTITLE),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEDESCRIPTION));
			logger.debug("VEHICLEVIN: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVIN)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.VIN));
			ah.assertEquals("VEHICLEVIN ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVIN),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.VIN));
			logger.debug("VEHICLEVINCONTROLNUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVINCONTROLNUMBER)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.VINCONTROLNUMBER));
			ah.assertEquals("VEHICLEVINCONTROLNUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEVINCONTROLNUMBER),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.VINCONTROLNUMBER));
			logger.debug("VEHICLEMODELYEAR: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMODELYEAR)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.MODELYEAR));
			ah.assertEquals("VEHICLEMODELYEAR ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMODELYEAR),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.MODELYEAR));
			logger.debug("VEHICLEIDENTIFICATIONCODE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEIDENTIFICATIONCODE)+" should equal ―");
			ah.assertEquals("VEHICLEIDENTIFICATIONCODE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEIDENTIFICATIONCODE), "―");
			logger.debug("VEHICLELICENSEPLATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELICENSEPLATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.LICENSEPLATE));
			ah.assertEquals("VEHICLELICENSEPLATE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELICENSEPLATE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.LICENSEPLATE));

			logger.debug("VEHICLESTOCK: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTOCK)+" should equal ―");
			//TODO activate assertion when UI is fixed (shows wrongly "unknown", should be --)
			ah.assertEquals("VEHICLESTOCK",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTOCK),	"Unknown");
			logger.debug("VEHICLEMILEAGE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMILEAGE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.MILEAGE));
			ah.assertEquals("VEHICLEMILEAGE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEMILEAGE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.MILEAGE));
			logger.debug("VEHICLELANGUAGEDOCUMENTS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELANGUAGEDOCUMENTS)+" should equal ―");
			//ah.assertEquals("VEHICLELANGUAGEDOCUMENTS ",
			//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLELANGUAGEDOCUMENTS),	"Languages.null");
			logger.debug("VEHICLEKEYCODE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEKEYCODE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.KEYCODE));
			ah.assertEquals("VEHICLEKEYCODE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEKEYCODE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.KEYCODE));
			logger.debug("VEHICLESTARTCODE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTARTCODE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.STARTCODE));
			ah.assertEquals("VEHICLESTARTCODE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLESTARTCODE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.STARTCODE));
			//Technical specifications
			logger.debug("MOTORTYPE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.MOTORTYPE)+" should equal ―");
			ah.assertEquals("MOTORTYPE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.MOTORTYPE), "―");
			logger.debug("POWER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.POWER)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.VEHICLEPOWER));
			ah.assertEquals("POWER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.POWER),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.POWER));
			logger.debug("ENGINECAPACITY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.ENGINECAPACITY)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.ENGINEDISPLACEMENT));
			ah.assertEquals("ENGINECAPACITY ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.ENGINECAPACITY),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.ENGINEDISPLACEMENT));
			logger.debug("NEDCVALUE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.NEDCVALUE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.NEDC));
			ah.assertEquals("NEDCVALUE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.NEDCVALUE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.NEDC) + ".00");
			logger.debug("WLTPVALUE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.WLTPVALUE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.WLTP));
			ah.assertEquals("WLTPVALUE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.WLTPVALUE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.WLTP) + ".00");
			logger.debug("INTERIORCOLOR: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.INTERIORCOLOR)+" should equal ―");
			ah.assertEquals("INTERIORCOLOR ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.INTERIORCOLOR), "―");
			logger.debug("TYRESPECIFICATIONS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRESPECIFICATIONS)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.TYRESPECS));
			ah.assertEquals("TYRESPECIFICATIONS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRESPECIFICATIONS),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.TYRESPECS));
			logger.debug("TYREBRAND: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TYREBRAND)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.TYREBRAND));
			ah.assertEquals("TYREBRAND ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TYREBRAND),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.TYREBRAND));
			logger.debug("TYRETYPE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRETYPE)+" should equal Summer");
			ah.assertEquals("TYRETYPE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TYRETYPE),
					"SUMMER");
			logger.debug("WEIGHT: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.WEIGHT)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.MAXWEIGHT));
			ah.assertEquals("WEIGHT ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.WEIGHT),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.MAXWEIGHT));
			logger.debug("EXTERIORCOLOR: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.EXTERIORCOLOR)+" should equal ―");
			ah.assertEquals("EXTERIORCOLOR ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.EXTERIORCOLOR), "―");
			
			//Pricing and options
			logger.debug("VEHICLEPRICE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEPRICE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGLISTPRICE));
			ah.assertEquals("VEHICLEPRICE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.VEHICLEPRICE),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGLISTPRICE));
			logger.debug("PROMOTIONDISCOUNT: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.PROMOTIONDISCOUNT)+" should equal null");
			ah.assertNull("PROMOTIONDISCOUNT ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.PROMOTIONDISCOUNT));

			logger.debug("DELIVERYCOSTS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCOSTS)+" should equal  ");
			ah.assertEquals("DELIVERYCOSTS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCOSTS), "");
			
			logger.debug("OTHERCOSTS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.OTHERCOSTS)+" should equal  ");
			ah.assertEquals("OTHERCOSTS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.OTHERCOSTS), "");
			logger.debug("TOTALPRICE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TOTALPRICE)+" should equal "+testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGTOTALPRICE));
			ah.assertEquals("TOTALPRICE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TOTALPRICE),
					(String) testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.PRICINGTOTALPRICE));
			//TODO Delivery information dates
			
			//Different delivery address
			logger.debug("DELIVERYLOCATIONNAMEUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYLOCATIONNAMEUI)+" should equal ―");
			ah.assertEquals("DELIVERYLOCATIONNAMEUI ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYLOCATIONNAMEUI), "―");
			logger.debug("DELIVERYENTERPRISENUMBER: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYENTERPRISENUMBER)+" should equal ―");
			ah.assertEquals("DELIVERYENTERPRISENUMBER ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYENTERPRISENUMBER),
					"―");
			logger.debug("DELIVERYADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYADDRESS)+" should equal ―");
			ah.assertEquals("DELIVERYADDRESS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYADDRESS), "―");
			logger.debug("DELIVERYCONTACTPERSONUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPERSONUI)+" should equal ―");
			ah.assertEquals("DELIVERYCONTACTPERSONUI ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPERSONUI), "―");
			logger.debug("DELIVERYCONTACTEMAILUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTEMAILUI)+" should equal ―");
			ah.assertEquals("DELIVERYCONTACTEMAILUI ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTEMAILUI), "―");
			logger.debug("DELIVERYCONTACTMOBILENUMBERUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTMOBILENUMBERUI)+" should equal ―");
			ah.assertEquals("DELIVERYCONTACTMOBILENUMBERUI ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTMOBILENUMBERUI), "―");
			logger.debug("DELIVERYCONTACTPHONENUMBERUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPHONENUMBERUI)+" should equal ―");
			ah.assertEquals("DELIVERYCONTACTPHONENUMBERUI ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTPHONENUMBERUI), "―");
			//logger.debug("DELIVERYCONTACTLANGUAGEUI: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTLANGUAGEUI)+" should equal ―");
			//ah.assertEquals("DELIVERYCONTACTLANGUAGEUI ",
			//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.DELIVERYCONTACTLANGUAGEUI), "Languages.null");
			//Equipment and enclosed documents
			if(System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
				logger.debug("RETURNEDVEHICLE: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLE) + " should equal false");
				ah.assertEquals("RETURNEDVEHICLE ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLE), "true");
				logger.debug("REGISTEREDONTHENAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTEREDONTHENAME)+" should equal ―");
				ah.assertEquals("REGISTEREDONTHENAME ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTEREDONTHENAME), "ALD Automotive");
				logger.debug("RETURNEDVEHICLELICENSEPLATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLELICENSEPLATE)+" should equal ―");
				ah.assertEquals("RETURNEDVEHICLELICENSEPLATE ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLELICENSEPLATE), "1FFF333");
			} else {
				logger.debug("RETURNEDVEHICLE: " + testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLE) + " should equal false");
				ah.assertEquals("RETURNEDVEHICLE ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLE), "false");
				logger.debug("REGISTEREDONTHENAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTEREDONTHENAME)+" should equal ―");
				ah.assertEquals("REGISTEREDONTHENAME ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTEREDONTHENAME), "―");
				logger.debug("RETURNEDVEHICLELICENSEPLATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLELICENSEPLATE)+" should equal ―");
				ah.assertEquals("RETURNEDVEHICLELICENSEPLATE ",
						(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLELICENSEPLATE), "―");
			}

			logger.debug("REGISTRATIONCERTIFICATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTRATIONCERTIFICATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTREGISTRATIONCERTIFICATE));
			ah.assertEquals("REGISTRATIONCERTIFICATE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.REGISTRATIONCERTIFICATE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTREGISTRATIONCERTIFICATE));
			logger.debug("INSURANCECERTIFICATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.INSURANCECERTIFICATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTINSURANCECERTIFICATE));
			ah.assertEquals("INSURANCECERTIFICATE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.INSURANCECERTIFICATE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTINSURANCECERTIFICATE));
			logger.debug("CERTIFICATEOFCONFORMITY: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.CERTIFICATEOFCONFORMITY)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTCERTIFICATEOFCONFORMITY));
			ah.assertEquals("CERTIFICATEOFCONFORMITY ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.CERTIFICATEOFCONFORMITY),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTCERTIFICATEOFCONFORMITY));
			logger.debug("TECHNICALINSPECTIONCERTIFICATE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.TECHNICALINSPECTIONCERTIFICATE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTTECHNICALINSPECTIONCERTIFICATE));
			ah.assertEquals("TECHNICALINSPECTIONCERTIFICATE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.TECHNICALINSPECTIONCERTIFICATE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTTECHNICALINSPECTIONCERTIFICATE));
			logger.debug("LEGALKIT: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.LEGALKIT)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTLEGALKIT));
			ah.assertEquals("LEGALKIT ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.LEGALKIT),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTLEGALKIT));
			logger.debug("SERVICEMANUAL: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.SERVICEMANUAL)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTSERVICEMANUAL));
			ah.assertEquals("SERVICEMANUAL ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.SERVICEMANUAL),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTSERVICEMANUAL));
			logger.debug("FUELCARD: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.FUELCARD)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.SELECTFUELCARD));
			ah.assertEquals("FUELCARD ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.FUELCARD),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.SELECTFUELCARD));
			logger.debug("NUMBEROFKEYS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.NUMBEROFKEYS)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.NUMBEROFKEYSINPUT));
			ah.assertEquals("NUMBEROFKEYS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.NUMBEROFKEYS),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.NUMBEROFKEYSINPUT));
			logger.debug("RETURNEDVEHICLEDRIVERNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERNAME)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERNAMEINPUT));
			//TODO activate assertion when UI is fixed
			//ah.assertEquals("RETURNEDVEHICLEDRIVERNAME ",
			//		(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERNAME),
			//		(String) testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERNAMEINPUT));

			String OLD_FORMAT = "dd/MM/yyyy";
			String NEW_FORMAT = "yyyy-MM-dd";

// August 12, 2010
			String oldDateString = (String) testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERDATEOFBIRTHINPUT);
			String newDateString;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			logger.debug("RETURNEDVEHICLEDRIVERDATEOFBIRTH: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERDATEOFBIRTH)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERDATEOFBIRTHINPUT));
			ah.assertEquals("RETURNEDVEHICLEDRIVERDATEOFBIRTH ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERDATEOFBIRTH),
					newDateString);
			logger.debug("RETURNEDVEHICLEDRIVERBIRTHPLACE: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERBIRTHPLACE)+" should equal "+testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERBIRTHPLACEINPUT));
			ah.assertEquals("RETURNEDVEHICLEDRIVERBIRTHPLACE ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.RETURNEDVEHICLEDRIVERBIRTHPLACE),
					(String) testContext.scenarioContext.getContext(ContextOrderUI.RETURNEDVEHICLEDRIVERBIRTHPLACEINPUT));
			logger.debug("BUYBACKNAME: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKNAME)+" should equal ―");
			ah.assertEquals("BUYBACKNAME ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKNAME), "―");
			String BuybackAddressInput = testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKSTREETHOUSENUMBER) + " , " + 
					testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKPOSTALCODE) + " " + 
					testContext.scenarioContext.getContext(ContextPostOrderImportsRequest.BUYBACKCITY);
			logger.debug("BUYBACKADDRESS: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKADDRESS)+" should equal ―");
			ah.assertEquals("BUYBACKADDRESS ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKADDRESS), "―");
			logger.debug("CONTRACTDURATION: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.CONTRACTDURATION)+" should equal ―");
			ah.assertEquals("CONTRACTDURATION ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.CONTRACTDURATION), "―");
			logger.debug("BUYBACKAMOUNT: "+testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKAMOUNT)+" should equal ―");
			ah.assertEquals("BUYBACKAMOUNT ",
					(String) testContext.scenarioContext.getContext(ContextOrderDetailUI.BUYBACKAMOUNT),
					"―");

			ah.processAllAssertions();
		}
				
				
}
}
