package steps.validation.steps;

import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.order.timeline.OrderTimeline;
import apiEngine.model.get.order.timeline.wallbox.WallBoxOrderTimeLine;
import apiEngine.model.get.supplier.actor.SupplierActor;
import cucumber.TestContext;
import helpers.AssertHelper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.*;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ftp.FTP_PostWallBoxImports;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;
import pageobjects.ui.ORD_Wallbox_Order_Details_Page;
import steps.api.steps.GetRegistrationApiSteps;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class ValidatePutRegistrationApi {
	final Logger logger = LogManager.getLogger(ValidatePutRegistrationApi.class);

	TestContext testContext;
	ORD_Order_Details_Page orderDetailsPage;
	ORD_Wallbox_Order_Details_Page wallboxDetailsPage;
	ORD_Order_Page orderPage;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_PostWallBoxImports postWallBoxImports;
	API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	API_GetSupplierActorById getSupplierActorById;
	API_GetOrderTimelineById getOrderTimelineById;
	API_PutRegistration putRegistration;
	//GetRegistrationApiSteps getRegistration;
	API_GetRegistration getRegistration;

	private AssertHelper ah = new AssertHelper();


	public ValidatePutRegistrationApi(TestContext context) throws InterruptedException, IOException {
		testContext = context;
		orderDetailsPage = testContext.getPageObjectManager().getORD_Order_Details_Page();
		wallboxDetailsPage = testContext.getPageObjectManager().getORD_Wallbox_Order_Details_Page();
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		postWallBoxImports = testContext.getPageObjectManager().postFTP_PostWallBoxImports();
		getLeasingCompanyActorById = testContext.getPageObjectManager().getAPI_GetLeasingCompanyActorById();
		getSupplierActorById = testContext.getPageObjectManager().getAPI_GetSupplierActorById();
		getOrderTimelineById = testContext.getPageObjectManager().getAPI_GetOrderTimelineById();
		putRegistration = testContext.getPageObjectManager().putAPI_PutRegistration();
		getRegistration = testContext.getPageObjectManager().getAPI_GetRegistration();

	}

	String dosNr;
	String id;
	String errorMessage;
	private static Response responseLCActor;
	private static LeasingCompanyActor leasingCompanyActor;
	private static Response responseSupplierActor;
	private static SupplierActor supplierActor;
	private static Response response;
	private static OrderTimeline orderTimeline;
	private static WallBoxOrderTimeLine wallBoxOrderTimeline;

	@Then("^User validates put registration$")
	public void user_validates_put_registration() throws InterruptedException, ParseException, IOException {
		Response res = getRegistration.getResponse();
		JsonPath jsonPathEvaluator = res.jsonPath();
		String formNumber = jsonPathEvaluator.getString("formNumber");
		//logger.debug("formNumber validate: " + formNumber);
		if(putRegistration.getFormNumber() == null) {
			ah.assertNull("formNumber is null ",	formNumber);
		} else {
			ah.assertEquals("formNumber", formNumber, putRegistration.getFormNumber());
		}


		//Response resp = putRegistration.getRes();
		//logger.debug("response in ValidateErrorMessage: : " + resp.asPrettyString());



		logger.debug(ah.processAllAssertions());


		}
	}

