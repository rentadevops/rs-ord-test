package steps.database.steps;

import apiEngine.model.database.*;
import cucumber.TestContext;
import io.cucumber.java.en.When;
import objects.database.DB_Query;
//import steps.database.steps.DB_Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import steps.api.steps.BaseSteps;

public class DatabaseSteps extends BaseSteps {
    final Logger logger = LogManager.getLogger(DatabaseSteps.class);
    DB_Query dbQuery;

    public DatabaseSteps(TestContext testContext) throws InterruptedException {
        super(testContext);
        //orderPage = testContext.getPageObjectManager().getORD_Order_Page();
        //getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
        //registrationImports = testContext.getPageObjectManager().postFTP_RegistrationImports();
        //putRegistration = testContext.getPageObjectManager().putAPI_PutRegistration();
        dbQuery = testContext.getPageObjectManager().getDB_Query();


    }

    @When("Query ord$")
    public void queryOrderDto() throws Exception {
        logger.debug("Start get_registration");
        Order_dto od = dbQuery.readOrder_dto();
        logger.debug("order_dto: " + od.toString());
        Leasing_company lc = dbQuery.readLeasing_company();
        logger.debug("Leasing_company: " + lc.toString());
        Dealer d = dbQuery.readDealer();
        logger.debug("Dealer: " + d.toString());
        Vehicle_dto ve = dbQuery.readVehicle();
        logger.debug("Vehicle: " + ve.toString());
        Order_export_dto oe = dbQuery.readOrder_export_dto();
        logger.debug("order_export_dto: " + oe.toString());
        Order_option oo = dbQuery.readOrder_option();
        logger.debug("order_option: " + oo.toString());
        Order_promotion pr = dbQuery.readOrder_promotion();
        logger.debug("order_promotion: " + pr.toString());
        Product op = dbQuery.readProduct();
        logger.debug("product: " + op.toString());




    }

}


