package steps.ui.steps;

import com.aventstack.extentreports.reporter.ExtentSparkReporter;
//import cucumber.api.Scenario;
//import cucumber.api.Scenario;
//import cucumber.api.java.After;
//import cucumber.api.java.Before;
//import org.jetbrains.annotations.NotNull;
import org.mockserver.integration.ClientAndServer;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.DevTools.*;
//import org.openqa.selenium.devtools.v107.fetch.model.RequestPattern;
//import org.openqa.selenium.devtools.v107.fetch.model.RequestStage;
//import org.openqa.selenium.devtools.v107.network.Network;
//import org.openqa.selenium.devtools.v107.network.model.ConnectionType;
//import org.openqa.selenium.devtools.v107.network.model.RequestPattern;
//import org.openqa.selenium.devtools.v107.network.model.ResourceType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
//import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;

//import atu.testrecorder.ATUTestRecorder;
//import common.ATUTstRecorder;
//import atu.testrecorder.ATUTestRecorder;
import common.VideoRecorder_utlity;
import cucumber.TestContext;

import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.time.Duration;
//import java.util.Date;

import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.support.ui.WebDriverWait;

//import io.cucumber.java.After;
//import io.cucumber.java.AfterStep;
//import io.cucumber.java.Before;
//import io.cucumber.java.BeforeStep;
import io.cucumber.java.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;

public class Hooks {
	final Logger logger = LogManager.getLogger(Hooks.class.getName());

	WebDriver driver;
	TestContext testContext;
	//ATUTestRecorder recorder;
	DevTools devTools;
	private ClientAndServer mockServer;
	
	public ExtentReports report;
	
	public Hooks(TestContext context) {
		testContext = context;
		
	}


	//@BeforeSuite
	//public void setUpSuite() {
		//ExtentHtmlReporter extent = new ExtentHtmlReporter(new File(System.getProperty("user.dir")));
		
	//}
	
	//@BeforeStep
	//public void stepInfo(Scenario scenario) {
	//}
	
	//public static void deleteVideos(String path) {
		//File directory = new File(path);
		//File[] files = directory.listFiles();
		//for (File file : files) {
			//file.delete();
		//}
	//}
	
	/*
	 * @Before public synchronized void beforeScenario(Scenario scenario) throws
	 * MalformedURLException { logger.debug("Headless: " +
	 * System.getProperty("java.awt.headless"));
	 * logger.debug("Start video in beforeTest method in hooks");
	 * logger.debug("userdir = " + System.getProperty("user.dir"));
	 * logger.debug("projectbuilddir = " +
	 * System.getProperty("project.build.directory"));
	 * 
	 * try { recorder = new
	 * ATUTestRecorder("./src/test/resources/runtime/ScriptVideos",
	 * scenario.getName(), false); } catch (Exception e) { logger.error("Error", e);
	 * } try { recorder.start(); } catch (Exception e) {
	 * logger.error("Error in starting the video", e); } }
	 */
	
	@Before
	public synchronized void beforeTest() throws Exception {
		logger.debug("Headless: " + System.getProperty("java.awt.headless"));
		/*DevTools chromeDevTools = ((HasDevTools) driver).getDevTools();

		List<RequestPattern> reqPattern = new ArrayList<>();
		reqPattern.add(new RequestPattern(Optional.of("*"), Optional.of(ResourceType.XHR), Optional.of(RequestStage.RESPONSE)));
		chromeDevTools.send(Fetch.enable(Optional.of(reqPattern), Optional.of(false)));*/
			//driver.get("https://fast.com");
			//Thread.sleep(15000);
			//devTools.close();
			//System.out.println("Test Video: " + VIDEO_URL + sessionId.toString());
			//driver.quit();

	}
	//}
	
	@After
	public void tearDown(@NotNull Scenario scenario) throws Exception {
		if (scenario.isFailed()) {
			logger.debug("scenario.isFailed @After tearDown");
			logger.debug("scenario.get status @After tearDown" + scenario.getStatus());
		}
	}

		//VideoRecorder_utlity.stopRecord();
		
		//Dit hoort bij ATUTestRecorder
		//try {
			//logger.debug("Stop video");
			//recorder.stop();
		//} catch (Exception e) {
			//logger.error("Unable to stop the screen recording");
		//}
	//}
	
	/*@AfterStep public void addScreenshot(Scenario scenario) throws IOException {
		
		try {
			File screenshot = ((TakesScreenshot)testContext.getWebDriverManager().getDriver()).getScreenshotAs(OutputType.FILE);
			byte[] fileContent = FileUtils.readFileToByteArray(screenshot);
			scenario.attach(fileContent, "image/png", "screenshot");
		} catch (Exception e) {
		  
		}

	} */
	  
	@AfterTest public void closeBrowser(Scenario scenario) {
		
		if(System.getProperty("BrowserName").toLowerCase().equalsIgnoreCase("firefox")) {
			logger.debug("driver quit in @AfterTest closeBrowser");
			//driver.close();
			if(driver != null) {
				logger.debug("driver" + driver.toString());
				driver.close();
				driver.quit();
				driver = null;

			}

		} else {
			logger.debug("driver quit in @AfterTest closeBrowser else-tak");
			//driver.close();
			driver.quit();
			driver = null;
		}
		  
	}
	
	@BeforeClass
    public void classLevelSetup() {
    }


	@AfterClass
	public void addLog(Scenario scenario) {
		scenario.log(System.getProperty("BrowserName"));
		scenario.log(System.getProperty("AppLanguage"));
	}

	@After public void endScenario(Scenario scenario){
		//logger.debug("devTools" + devTools.toString());
		//devTools.toString();
		//devTools.close();
		if(driver != null) {
			logger.debug("driver" + driver.toString());
			driver.quit();
			driver=null;

		}

	}

	
	
}
