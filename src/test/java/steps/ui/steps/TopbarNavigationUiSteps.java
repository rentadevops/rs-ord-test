package steps.ui.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import pageobjects.ui.ORD_Topbar;
import cucumber.TestContext;

import java.io.FileNotFoundException;
import java.io.IOException;

public class TopbarNavigationUiSteps {

	ORD_Topbar topBar;
	TestContext testContext;
	
	public TopbarNavigationUiSteps(TestContext context) throws InterruptedException {
		 testContext = context;
		 topBar = testContext.getPageObjectManager().getORD_topbar();
		 
	}
	
	@When("^User opens administration screen$")
	public void user_opens_administration_screen() throws InterruptedException, FileNotFoundException, IOException {
		topBar.click_administration();
		
	}

	@Given("^User opens registration screen$")
	public void user_opens_leasing_company_contract_screen() throws InterruptedException, FileNotFoundException, IOException {
		topBar.click_registration();
		
	}
	
	@When("^Switch user to dealer$")
	public void switch_user() throws InterruptedException, FileNotFoundException, IOException {
		topBar.click_switch_user_to_dealer();
	}
	
	@When("^Switch user to leasing_company$")
	public void switch_user_to_leasing_company() throws InterruptedException, FileNotFoundException, IOException {
		topBar.click_switch_user_to_leasing_company();
	}
	
	
}
