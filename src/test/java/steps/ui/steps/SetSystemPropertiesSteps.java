package steps.ui.steps;

import cucumber.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.ui.ORD_SetSystemProperties_Page;
import pageobjects.ui.Portal_Page;
import steps.api.steps.BaseSteps;

import java.io.IOException;
import java.util.List;

public class SetSystemPropertiesSteps extends BaseSteps {
	final Logger logger = LogManager.getLogger(SetSystemPropertiesSteps.class);

	ORD_SetSystemProperties_Page setSystemProperties;


	public SetSystemPropertiesSteps(TestContext testContext) throws InterruptedException {
		super(testContext);
		setSystemProperties = testContext.getPageObjectManager().setORD_SetSystemProperties_Page();
	}
	 
	@Given("^Add system properties$")
	public void add_system_properties(DataTable table) throws InterruptedException, IOException {
		List<List<String>> data = table.asLists();
		String systemVariable = data.get(1).get(0);
		String value = data.get(1).get(1);
		logger.debug("systemVariable " + systemVariable + " heeft waarde " + value);


		System.setProperty(systemVariable, value);
	}
 
}
