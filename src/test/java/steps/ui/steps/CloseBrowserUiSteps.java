package steps.ui.steps;

import cucumber.TestContext;
//import cucumber.api.java.en.Then;
import io.cucumber.java.en.Then;

public class CloseBrowserUiSteps {

	TestContext testContext;

	public CloseBrowserUiSteps(TestContext context) {
		testContext = context;
	}

	@Then("^close browser$")
	public void close_browser() {
		if (System.getProperty("BrowserName").equals("firefox")) {
			testContext.getWebDriverManager().closeDriverFF();
		} else {
			testContext.getWebDriverManager().closeDriver();
		}
	}

}
