package steps.ui.steps;

import java.io.IOException;

import cucumber.TestContext;
import io.cucumber.java.en.Given;
import pageobjects.ui.Portal_Page;

public class LoginUiSteps {
	
	TestContext testContext;
	Portal_Page portalRSPage;
	 
	 
	public LoginUiSteps(TestContext context) {
	testContext = context;
	portalRSPage = testContext.getPageObjectManager().getPortalRS_Page();
	}
	 
	@Given("^User is logged in$")
	public void user_is_logged_in() throws InterruptedException, IOException {
		portalRSPage.navigateToPortalPage();
		portalRSPage.login();
		portalRSPage.navigateToORDPage();
	}
 
}
