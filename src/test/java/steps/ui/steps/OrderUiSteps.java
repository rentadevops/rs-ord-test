package steps.ui.steps;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;

import javax.script.ScriptException;

import apiEngine.Endpoints;
import apiEngine.model.get.order.timeline.OrderTimeline;
import com.google.zxing.NotFoundException;
//import org.apache.batik.transcoder.TranscoderException;
import io.cucumber.datatable.DataTable;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cucumber.TestContext;
import io.cucumber.java.en.When;
import org.apache.pdfbox.pdmodel.PDDocument;
import pageobjects.ui.*;
import steps.validation.steps.ValidateDeliveryReceiptPdf;


public class OrderUiSteps {
	final Logger logger = LogManager.getLogger(OrderUiSteps.class.getName());
	
	ORD_Registration_Page registrationPage;
	ORD_Orders_Page ordersPage;
	ORD_Order_Page orderPage;
	ORD_Order_Details_Page detailsPage;
	ORD_Wallbox_Order_Details_Page wallboxDetailsPage;
	ORD_Topbar topBar;
	Validate_Pdf validatePdf;
	TestContext testContext;
	
	public OrderUiSteps(TestContext context) throws InterruptedException, IOException {
		testContext = context;
		registrationPage = testContext.getPageObjectManager().getORD_Registration_Page();
		ordersPage = testContext.getPageObjectManager().getORD_Orders_Page();
		orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		detailsPage = testContext.getPageObjectManager().getORD_Order_Details_Page();
		wallboxDetailsPage = testContext.getPageObjectManager().getORD_Wallbox_Order_Details_Page();
		topBar = testContext.getPageObjectManager().getORD_topbar();
		//validatePdf = testContext.getPageObjectManager().getValidate_Pdf();
		

	}

	@When("^User search order$")
	public void user_search_order() throws InterruptedException, IOException, URISyntaxException {
		ordersPage.search_order();
	}
	
	@When("^User opens order$")
	public void user_opens_order() throws InterruptedException, IOException {
		ordersPage.open_order();
	}
	
	@When("^User marks messages as read$")
	public void user_marks_messages_as_read() throws Exception {
		orderPage.mark_messages_as_read();
	}
	
	@When("^User opens order overview$")
	public void user_opens_order_overview() throws Exception {
		orderPage.open_overview();
	}
	
	@When("^User opens order details$")
	public void user_opens_order_details() throws InterruptedException, IOException {
		orderPage.open_details();
	}

	@When("^User opens order documents$")
	public void user_opens_order_documents() throws InterruptedException, IOException {
		orderPage.open_documents();
	}

	@When("^User downloads order documents$")
	public void user_downloads_order_documents() throws InterruptedException, IOException {
		orderPage.download_documents();
	}

	@When("^User opens pdf$")
	public void user_opens_pdf() throws InterruptedException, IOException, ParseException {
		PDDocument doc = orderPage.open_pdf();
		ValidateDeliveryReceiptPdf valpdf = new ValidateDeliveryReceiptPdf(testContext);
		valpdf.validate_delivery_receipt_pdf(doc);
		valpdf.validate_delivery_receipt_pdf_options(doc);
		valpdf.validatePdfLogo(doc);
		valpdf.validate_metadata(doc);
	}

	//@When("^User validates pdf$")
	//public void user_validates_pdf(PDDocument doc) throws InterruptedException, IOException, ParseException {
	//	validatePdf.validatePdf(doc);
	//}

	@When("^User opens registrations$")
	public void user_opens_registrations() throws InterruptedException, IOException {
		orderPage.open_details();
	}
	
	@When("^User opens order messages$")
	public void user_opens_order_messages() throws InterruptedException, IOException {
		orderPage.open_messages();
	}
	
	@When("^User accepts order$")
	public void user_accept_order() throws InterruptedException, IOException {
		orderPage.accept_order();
	}
	
	@When("^User sends comment \"([^\"]*)\"$")
	public void user_send_comment(String comment) throws InterruptedException, IOException {
		orderPage.send_comment(comment);
	}
	
	@When("^User confirms order$")
	public void user_confirms_order() throws InterruptedException, IOException {
		orderPage.confirm_order();
	}
	
	@When("^User checks vehicle arrived$")
	public void user_checks_vehicle_arrived() throws InterruptedException, IOException {
		orderPage.check_vehicle_arrived();
	}
	
	@When("^User enters delivery info$")
	public void user_enters_delivery_info() throws InterruptedException, IOException {
		orderPage.enter_delivery_info();
	}

	@When("^User enters wallbox install info$")
	public void user_enters_wallbox_install_info(DataTable data) throws InterruptedException, IOException {
		orderPage.enter_wallbox_install_info(data);
	}

	@When("^User enters ready to activate$")
	public void user_enters_ready_to_activate() throws InterruptedException, IOException {
		orderPage.enter_ready_to_activate();
	}

	@When("^User enters ready to activate info$")
	public void user_enters_ready_to_activate_info() throws InterruptedException, IOException {
		orderPage.enter_ready_to_activate_info();
	}
	
	@When("^User enters vehicle at supplier$")
	public void user_enters_vehicle_at_supplier() throws InterruptedException, IOException, ParseException {
		orderPage.enter_vehicle_at_supplier();
	}
	
	@When("^User enters vehicle ready for delivery$")
	public void user_enters_vehicle_ready_for_delivery() throws InterruptedException, IOException, ScriptException, NoSuchMethodException {
		orderPage.enter_vehicle_ready_for_delivery();
	}

	@When("^User prepares digital delivery$")
	public void user_prepares_digital_delivery() throws InterruptedException, IOException, ScriptException, NoSuchMethodException, NotFoundException, URISyntaxException, TranscoderException {
		orderPage.prepare_digital_delivery();
	}

	@When("^Digital delivery is \"([^\"]*)\"$")
	public void set_digital_delivery(String choice) throws InterruptedException, IOException {
		orderPage.set_toggle_digital_delivery(choice);
	}
	
	@When("^User delivers vehicle$")
	public void user_delivers_vehicle() throws InterruptedException, IOException, ScriptException, NoSuchMethodException {
		orderPage.enter_deliver_vehicle();
	}
	
	@When("^User gets orderdetails data$")
	public void user_gets_orderdetails_data() throws InterruptedException, IOException, ScriptException, NoSuchMethodException, ParseException {
		detailsPage.get_order_details_data();
	}

	@When("^User gets wallbox orderdetails data$")
	public void user_gets_wallbox_orderdetails_data() throws InterruptedException, IOException, ScriptException, NoSuchMethodException, ParseException {
		wallboxDetailsPage.get_wallbox_order_details_data();
	}

	@When("^Doc$")
	public void doc(String gherkin)  {

	}
	
	

}
