package steps.ui.steps;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import cucumber.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import pageobjects.ui.ORD_Registration_Page;
import pageobjects.ui.ORD_Topbar;

public class RegistrationUiSteps {
	
	ORD_Registration_Page registrationPage;
	ORD_Topbar topBar;
	TestContext testContext;
	
	public RegistrationUiSteps(TestContext context) throws InterruptedException {
		testContext = context;
		registrationPage = testContext.getPageObjectManager().getORD_Registration_Page();

	}

	@When("^User clicks on new registration$")
	public void user_clicks_on_new_registration(DataTable table) throws InterruptedException, IOException {
		List<List<String>> data = table.asLists();
		registrationPage.click_add(data);
		
	}

	@When("^User clicks open registrations$")
	public void user_clicks_open_registrations() throws InterruptedException {
		registrationPage.open_registrations();

	}

	@When("^User updates registrations$")
	public void user_updatesregistrations() throws InterruptedException {
		registrationPage.update_registrations();

	}
	
	//@When("^User clicks on switch user$")
	//public void user_clicks_on_add_contract() throws InterruptedException, FileNotFoundException, IOException {
	//	topBar.click_switch_user();
		
	//}
	
	//@When("^User filters to find the registration$")
	//public void user_filters_to_find_the_leasing_company_contract(DataTable table) throws InterruptedException, IOException {
		//registrationPage.filter(table);
		
	//}

}
