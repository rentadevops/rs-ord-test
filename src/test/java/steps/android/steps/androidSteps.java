package steps.android.steps;

import android.tests.MobileBrowserTest;
import cucumber.TestContext;
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.When;
//import cucumber.api.java.en.When;
import io.cucumber.java.en.When;
import pageobjects.ui.Portal_Page;

import java.io.IOException;
import java.text.ParseException;

public class androidSteps {

	TestContext testContext;
	Portal_Page portalRSPage;


	public androidSteps(TestContext context) {
	testContext = context;
	portalRSPage = testContext.getPageObjectManager().getPortalRS_Page();
	}
	 
	@When("^User goes to mobile website$")
	public void user_goes_to_mobile_website() throws InterruptedException, IOException, ParseException {
		MobileBrowserTest mb = new MobileBrowserTest();
		mb.browserTest();
	}

	@When("^User goes to mobile website with device \"([^\"]*)\" and version \"([^\"]*)\"$")
	public void user_goes_to_mobile_website_with_device_and_version(String device, String version) throws InterruptedException, IOException, ParseException {
		MobileBrowserTest mb = new MobileBrowserTest();
		mb.browserTestWithDeviceAndVersion(device, version);
	}
 
}
