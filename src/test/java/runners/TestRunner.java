
package runners;
//import org.junit.runner.RunWith;
//import io.cucumber.junit.Cucumber;
//import io.cucumber.junit.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


//@RunWith(Cucumber.class)
@CucumberOptions(
		features={"src/test/resources/features"}, 
		glue= {"steps"}, 
		monochrome=true, 
		publish=true,
				
		//tags = "(@ORD-registration-UI)",
		//tags = "(@Registration-basic-api)",
		//tags = "(@CEG-2284-1 or @CEG-2284-2 or @CEG-2284-3)",
		//tags = "(@CEG-2859)",
		//tags = "(@MU-1498)",
		//tags = "(@Rental)",
		//tags = "(@migrate-200-to-status-accepted)",
		//tags = "(@ORD-registration-UI-orderedFrontPlate)",
		//tags = "(@WallBox-test or @LUX or @OperLease or @FL or @api-WallBox-test)",
		//tags = "(@WallBox-test or @LUX or @OperLease or @FL)",
		//tags = "(@OperLease)",
		//tags = "(@OLPLATEORDERED)",
		//tags = "(@OperLeaseVehicleAtSupplier)",
		//tags = "(@FL)",
		//tags = "(@FLNONOTIFICATION)",
		//tags = "(@REI)",
		//tags = "(@OL-no-notification-mobile-en)",

		tags = "(@OL-no-notification-en)",
		//tags = "(@OL-update)",
		//tags = "(@OL-no-notification-en or @OperLease or @FLNONOTIFICATION)",
		//tags = "(@getOrderNotificationsFeed)",
		//tags = "(@postDigitalDeliveryConfirmation)",
		//tags = "(@OL-mobile-web-page)",
		//tags = "(@ORD-registration-UI-usedVehicle)",
		//tags = "(@OL-migrate-to-status-vehiclein)",

		//tags = "(@OL-bulk-500)",
		//tags = "(@OL-bulk)",
		//tags = "(@LUX)",
		//tags = "(@MOBILEWITHDEVICE)",
		//tags = "(@MOBILE)",
		//tags = "(@OLDIGITALDELIVERY)",
		//tags = "(@LUXNONOTIFICATION)",
		//tags = "(@api-Registration)",
		//tags = "(@CEG-1960-formNumberNull)",
		//tags = "(@CEG-1960-splitEmissionValues)",
		//tags = "(@CEG-1970-formNumberNotNull)",
		//tags = "(@CEG-1970-approveWithNationalId)",

		//tags = "(@CEG-1970-insuranceBrokerFsmaNumberFilled)",
		//tags = "(@ORD-registrationFSTPScenario1)",
		//tags = "(@Registration-basic-api)",
		//tags = "(@ORD-registration-UI-orderedFrontPlateReuseLicensePlate)",
		//tags = "(@ORD-registration-UI-orderedFrontPlate)",
		//tags = "(@ORD-registration-UI-planned-and-registered-after-update)",
		//tags = "(@ORD-registration-UI-usedVehicle-order-frontplate-and-update-wanted-registration-date)",


		//tags = "(@WallBox-linkedOrder)",
		//tags = "(@WallBox-test-no-notification)",
		//tags = "(@getOrderNotificationsFeed)",
		
		
		//tags = "(@LeasingCompanyContractFlow)",
		//tags = "(@LeasingCompanyPartsReductionFlow)",
		//tags = "(@LeasingCompanyThresholdFlow)",	
		//tags = "(@MainIntervalFlow)",
		//tags = "(@SupplierFlow)",
		//tags = "(@SupplierGroupFlow)",
		//tags = "(@LeasingCompanyContractFlow or @LeasingCompanyPartsReductionFlow or @LeasingCompanyThresholdFlow or @MainIntervalFlow or @SupplierFlow or SupplierGroupFlow)", 
		
		//tags = "(@Regression)",
		//tags = "(@Regression or @LeasingContractEdit or @LeasingCompanyThresholdFlow or @LeasingCompanyPartsReductionFlow or @MainIntervalFlow)", 
		//tags = "(@LeasingCompanyThresholdFlow or @LeasingCompanyPartsReductionFlow or @MainIntervalFlow)",
		//tags = "@DB-Query",
	
		plugin={"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:target/json-report/cucumber.json"} //, "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm"
		//plugin={"pretty","html:target/cucumber-html-report","json:target/cucumber-reports/cucumber.json","junit:target/cucumber-reports/cucumber.xml","com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}
		)
                                                                   
public class TestRunner extends AbstractTestNGCucumberTests {

	 
}
