package common;

import io.restassured.path.json.JsonPath;
//import org.apache.commons-io.FileUtils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.custommonkey.xmlunit.DetailedDiff;
//import org.custommonkey.xmlunit.XMLUnit;
//import org.testng.Assert;
//import org.custommonkey.xmlunit.DetailedDiff;
//import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ValidateXml {
	final Logger logger = LogManager.getLogger(ValidateXml.class);

	public ValidateXml() {

	}

	public void validateXml() throws IOException, SAXException {

		String xmlExpectedToString = FileUtils.readFileToString(new File("/file/path/file.xml"));

		String expectedXml = "<abc             attr=\\\"value1\\\"                title=\\\"something\\\">            </abc>"; //String expectedXml = xmlExpectedToString; gebruiken als de xml in een file staat
		String actualXml = "<abc attr=\\\"value1\\\" title=\\\"something\\\"></abc>";
		// will be ok
		//assertXMLEquals("<abc attr=\"value1\" title=\"something\"></abc>", result);

		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);

		DetailedDiff diff = new DetailedDiff(XMLUnit.compareXML(expectedXml, actualXml));

		List<?> allDifferences = diff.getAllDifferences();
		Assert.assertEquals("Differences found: "+ diff.toString(), 0, String.valueOf(allDifferences.size()));


	}

}
