package common;

import java.util.Base64;

//import org.junit.Test;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.HostKey;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SftpConnect {
	
	String username = "clordftpdev";
	//String remoteHost = "ord-tst.rentasolutions.org";
	String remoteHost = "212.113.73.114";
	String password = "eiMip7AeCh";
	String keyString = "AAAAB3NzaC1yc2EAAAABIwAAAQEAqb1HABQoAQPT4Ic76nAcQP1HpqIEv9VuR188mc8LK+z8Ckqu1tpiDfpIHgd18N8ml3if6mx97LzG5CIcZUDDsIIaONzsyZk2ARxxEn2c7r/wD/RIUALKN3YUySTy1AAcocrGjp0YeJpaENWV0pe1Jmqi5lFAX3J1ziyD/yuWNhXqCaSkBD51o/JGdm6hTVsK/fc8ZLm7CofJqaOGWj5hF26BO0X8vQ6pYyVgLCGvzAGgKpxqKRUcNCkdKSv+blChgitAVmmOmVPWTCxoWzVA8HVb3I4w5blPHYZ36zw15xR47+hpQtZJqo9mDMcbA1kII17ajah4gk1KkKbZr4XOEQ==";
	byte [] key = Base64.getDecoder().decode ( keyString );
	
	public ChannelSftp setupJsch() throws JSchException {
	    JSch jsch = new JSch();
	    HostKey hostKey = new HostKey ( remoteHost, key );
	    jsch.getHostKeyRepository ().add ( hostKey, null );
	    jsch.setKnownHosts("212.113.73.114");
	    Session jschSession = jsch.getSession(username, remoteHost);
	    jschSession.setPort(22);
	    jschSession.setPassword(password);
	    jschSession.connect();
	    return (ChannelSftp) jschSession.openChannel("sftp");
	}

}
