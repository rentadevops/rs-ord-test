package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class GetFileInputStream {
	
	public InputStream getInputStream(String filename) throws FileNotFoundException {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = null;
		    File file = new File(classLoader.getResource(filename).getFile());
		    inputStream = new FileInputStream(file);

		return inputStream;
	}
	
	public void closeInputStream(InputStream inputStream) throws IOException {
		inputStream.close();
	}

}
