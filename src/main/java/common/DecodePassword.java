package common;

import java.util.Base64;

public class DecodePassword {

public String getDecodedPassword(String psw) {
	
	return new String(Base64.getDecoder().decode(psw.getBytes()));
}
}
