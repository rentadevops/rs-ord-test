package common;

import apiEngine.Route;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;


public class CmdGenerator {
	final Logger logger = LogManager.getLogger(CmdGenerator.class);

	public static void main(String[] args) throws Exception {
		ProcessBuilder builder = new ProcessBuilder(
		"cmd.exe", "/c", "newman run \"src/test/resources/newmanCollections/TST.postman_environment.json");
		builder = new ProcessBuilder(
				"cmd.exe", "/c", "newman run \"src/test/resources/newmanCollections/Activity_Defect_Task_Warranty_Resource.postman_collection.json");

		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) { break; }
			System.out.println(line);
		}
	}
}
