package common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

//import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;


  public class StepData {
  
  public ValidatableResponse response; public RequestSpecification request;
  Properties properties = new Properties();
  
  public StepData() throws FileNotFoundException, IOException {
  loadProperties(); }
  
  public void loadProperties() throws FileNotFoundException, IOException {
  
  FileInputStream fis = new
  FileInputStream("src/test/resources/properties.properties");
  properties.load(fis); }
  
  public String getProperty(String key) { return properties.getProperty(key); }
  
  }
 
