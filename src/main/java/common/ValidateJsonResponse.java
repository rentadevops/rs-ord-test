package common;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.restassured.path.json.JsonPath;

public class ValidateJsonResponse {
	final Logger logger = LogManager.getLogger(ValidateJsonResponse.class);

	JsonPath jpath;
	Boolean found = false;
	
private StepData stepData;
	
	public ValidateJsonResponse(StepData stepData) {
		this.stepData = stepData;
	}

	public boolean validateJsonResponse(String field, String type, String value) {

		jpath = stepData.response.extract().jsonPath();
		switch (type) {
		case "int":
			int intValue = Integer.parseInt(value);
			List<Object> allValues = jpath.getList(field);
			for (int i = 0; i < allValues.size(); i++) {

				if ((int) allValues.get(i) == intValue) {
					found = true;
				}
			}

			break;
		}
		switch (type) {
		case "string":
			//int intValue = Integer.parseInt(value);
			logger.debug("value: " + value);
			List<Object> allValues = jpath.getList(field);
			logger.debug("Fields: " + allValues);
			for (int i = 0; i < allValues.size(); i++) {
				logger.debug("Values: " +  allValues.get(i));
				if ( allValues.get(i).equals(value)) {
					found = true;
				}
			}

			break;
		}
		return found;
	}

}
