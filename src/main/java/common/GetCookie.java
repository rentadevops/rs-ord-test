package common;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.Route;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class GetCookie {
	final Logger logger = LogManager.getLogger(GetCookie.class);

	public Response getCookie(String rsNumber) throws FileNotFoundException, IOException {
		String authCode = "";
		//ADMIN
		if(rsNumber.equals("000006021")) {
			authCode = "Basic YS5hbnRvbmlzc2VAcmVudGFzb2x1dGlvbnMub3JnOkFkcmlhMTMwNCQk";
		}
		//ADMIN
		else if(rsNumber.equals("000001003")) {
			authCode = "Basic cnN0ZXN0QHJlbnRhc29sdXRpb25zLm9yZzpSU3Rlc3Q5OTk=";
		}
		//LEASING COMPANY MANAGER
		else if(rsNumber.equals("100005028")) {
			authCode = "Basic cnN0ZXN0QHJlbnRhc29sdXRpb25zLm9yZzpSU3Rlc3Q5OTk=";
		}
		else if(rsNumber.equals("000007004")) {
			authCode = "Basic cnN0ZXN0QHJlbnRhc29sdXRpb25zLm9yZzpSU3Rlc3Q5OTk=";
		}
		//SUPPLIER COMPANY
		else if(rsNumber.equals("666666668")) {
			authCode = "Basic cnN0ZXN0QHJlbnRhc29sdXRpb25zLm9yZzpSU3Rlc3Q5OTk=";
		}
		//DEALER
				else if(rsNumber.equals("730086007")) {
					authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
				}
		//WallBox Dealer
		else if(rsNumber.equals("800404005")) {
			authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
		}

				//else if(rsNumber.equals("730084107")) {
				//	authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
				//}
		//LEASING COMPANY
				else if(rsNumber.equals("799230111")) {
					authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
				}
		else if(rsNumber.equals("799120005")) {
			authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
		}
		else if(rsNumber.equals("799189017")) {
			authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
		}
		else if(rsNumber.equals("799330011")) {
			authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
		}
		else if(rsNumber.equals("799340001")) {
			authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
		}
		else if(rsNumber.equals("800075010")) {
			authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
		}
		else if(rsNumber.equals("730084107")) {
			authCode = "Basic cnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmc6UlN0ZXN0OTk5";
		}
		
		
		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
		
		//.header("authorization", "Basic YS5hbnRvbmlzc2VAcmVudGFzb2x1dGlvbnMub3JnOkFkcmlhMTMwNCQk") //000007004
		//.header("authorization", "Basic YS5hbnRvbmlzc2VAcmVudGFzb2x1dGlvbnMub3JnOkFkcmlhMTMwNCQk") //000006021
		//.header("authorization", "Basic cnN0ZXN0QHJlbnRhc29sdXRpb25zLm9yZzpSU3Rlc3Q5OTk=") //000005028 LC manager
		//.header("authorization", "Basic cnN0ZXN0QHJlbnRhc29sdXRpb25zLm9yZzpSU3Rlc3Q5OTk=") //000001003 Admin
		.header("authorization", authCode)
		.log().all()
		;

        Response response = request.get(Route.sso());
		logger.debug("Cookie: " + response.asPrettyString());
        return response;
	}
}
