package tests.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReport {

    public static ExtentReports extent;
    public static ExtentTest test;

    public static void initReports() {
        extent = new ExtentReports();
        ExtentSparkReporter spark = new ExtentSparkReporter(System.getProperty("user.dir") + "/index.html");
        spark.config().setTheme(Theme.DARK);
        spark.config().setReportName("Automation report");
        spark.config().setDocumentTitle("Checking");
        extent.attachReporter(spark);
    }

    public static void tearDownReports() {
        extent.flush();
    }

    public static void createTest(String testcasename) {
        test = extent.createTest(testcasename);
    }
}
