package tests;


import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.remote.MobileCapabilityType;
import listeners.TestListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import steps.ui.steps.Hooks;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

public class BaseTest {
    final Logger logger = LogManager.getLogger(BaseTest.class);
        public AndroidDriver driver;
        public Process p;
        public ProcessBuilder builder;

        //@BeforeClass
        public void ConfigureAppium() throws MalformedURLException, InterruptedException {
        //AndroidDriver driver = null;
        //Process p = null;
        //ProcessBuilder builder;

        /*AppiumDriverLocalService service = new AppiumServiceBuilder()
                .usingDriverExecutable(new File("C:/Program Files/nodejs/node.exe"))
                .usingDriverExecutable(new File("C:/Users/AdAntonisse/.appium/node_modules/appium-uiautomator2-driver/build/lib/uiautomator2.js"))
                .withAppiumJS(new File("C:/Users/AdAntonisse/AppData/Roaming/npm/node_modules/appium/build/lib/main.js"))
                .withIPAddress("127.0.0.1")
                .usingPort(4723)
                .build();
        service.start();*/


        Runtime rt = Runtime.getRuntime();
        try {
            String[] command = new String[]{"C:/Windows/System32/cmd.exe", "/c", "start cmd.exe", "/c", "appium", "-a", "127.0.0.1", "-p", "4723", "--session-override", "-dc"};
            builder = new ProcessBuilder(command);
            p = builder.start();

            Thread.sleep(10000);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        UiAutomator2Options options = new UiAutomator2Options()
                .setAutomationName("uiautomator2")
                .setDeviceName("AdjesMob")
                .setChromedriverExecutable("C:/UdemyAppium/Drivers/chromedriver.exe")
                //.setApp("C:/Users/AdAntonisse/AppiumProjects/src/test/resources/ApiDemos-debug.apk");
                .setApp("C:/Users/AdAntonisse/AppiumProjects/src/test/resources/General-Store.apk");
        options.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 300);


        driver = new AndroidDriver(new URL("http://127.0.0.1:4723"), options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @BeforeClass
    public void classLevelSetup() {
        logger.info("Tests is starting!");
        //driver = new ChromeDriver();
    }

    public void longPressAction(WebElement ele) {
        ((JavascriptExecutor)driver).executeScript("mobile: longClickGesture",
                ImmutableMap.of("elementId",((RemoteWebElement)ele).getId(),
                        "duration", 2000));
    }

    public void swipeAction(WebElement ele, String direction) {
        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of("elementId", ((RemoteWebElement)ele).getId(),
                "direction", direction,
                "percent", 0.75));
    }

    public double getFormattedAmount(String amount) {
        double price = Double.parseDouble( amount.substring(1));
        return price;
    }

    @AfterClass
    public void tearDown() throws IOException {
        driver.quit();
        p.destroyForcibly();
    }
}
