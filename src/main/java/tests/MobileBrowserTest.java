package tests;

import cucumber.TestContext;
import io.appium.java_client.AppiumDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
//import org.testng.annotations.Test;
import pageobjects.android.*;
import pageobjects.ui.ORD_Order_Page;
import utils.AppiumUtils;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
//import io.appium.java_client.MobileElement;


public class MobileBrowserTest extends BrowserBaseTest {
    final Logger logger = LogManager.getLogger(MobileBrowserTest.class);

    AppiumUtils au = new AppiumUtils();
    AppiumDriver driver;
    //AndroidDriver driver;
    TestContext testContext;
    ORD_Order_Page orderPage;
    static String statusCode;
    //WebElement btn_continue = driver.findElement(By.xpath("//button[contains(.,'')]"));
    //WebElement list_birthday = driver.findElement(By.xpath("//input[@formcontrolname='birthDate']"));

    @FindBy(how = How.XPATH, using = "//button[@data-testid='license-plate-next-page-btn']")
    private WebElement btn_license_plate_next;



    //@Test
    public void browserTest() throws InterruptedException, IOException, ParseException {


        String darCookie = au.postGetCookie();
        logger.debug("darCookie: " + darCookie);

        Date now = new Date(),
                exp = new Date(now.getYear(), now.getMonth(), now.getDate()+1);

        //UiAutomator2Options options = new UiAutomator2Options();
        //options.setAutomationName("uiautomator2");
        //options.setDeviceName("Pixel 2 XL API 31");
        //options.setChromedriverExecutable("C:/UdemyAppium/Drivers/chromedriver.exe");
        //options.setCapability("browserName", "Chrome");
        //options.setCapability("autoAcceptAlerts", "true");
        //options.setPlatformName("Android");



        //AppiumDriver appiumDriver = au.startAppiumServer();
        //androidDriver.rotate(ScreenOrientation.LANDSCAPE);
        //Thread.sleep(2000);
        AppiumUtils au = new AppiumUtils();
        driver = au.startAppiumServerLambdaTest(); //connect through LambdaTest
        //driver = au.startAppiumServer(); //Connect with local Appium Server
        //driver.manage().deleteAllCookies();

        /*String userName = System.getenv("LT_USERNAME") == null ?
                "a.antonisse" : System.getenv("LT_USERNAME"); //Add username here
        String accessKey = System.getenv("LT_ACCESS_KEY") == null ?
                "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0" : System.getenv("LT_ACCESS_KEY"); //Add accessKey here

        String gridURL = "@mobile-hub.lambdatest.com/wd/hub";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        HashMap<String, Object> ltOptions = new HashMap<String, Object>();
        ltOptions.put("w3c", true);
        ltOptions.put("platformName", "android");
        ltOptions.put("deviceName", "Pixel 4");
        ltOptions.put("platformVersion", "10");
        capabilities.setCapability("visual", true);
        ltOptions.put("isRealMobile", true);
        capabilities.setCapability("console", true);
        capabilities.setCapability("lt:options", ltOptions);

        String hub = "http://" + userName + ":" + accessKey + gridURL;


        AndroidDriver driver = new AndroidDriver(new URL(hub), capabilities);*/

        System.out.println("driver: " + driver);
        try {
            driver.get("https://delivery.tst.rentasolutions.org");
            logger.debug("Title: " + driver.getTitle());
        }

                catch(WebDriverException e) {
                logger.debug("stacktrace" + e.getStackTrace().toString());
                    logger.debug("additional info" + e.getAdditionalInformation());
            }
        logger.debug("get cookies: " + driver.manage().getCookies());

        Cookie dar = new Cookie.Builder("dar-authentication", darCookie)//.sameSite("Lax").build()
                        .domain(".rentasolutions.org")
                        .path("/")
                        .expiresOn(exp)
                        .sameSite("Lax")
                .build();
        logger.debug("Cookie toevoegen");
        //Cookie language = new Cookie.Builder("renta-language", System.getProperty("appLanguage"))//.sameSite("Lax").build()
        Cookie language = new Cookie.Builder("renta-language", "en")//.sameSite("Lax").build()
                .domain(".rentasolutions.org")
                .path("/")
                .expiresOn(exp)
                .sameSite("Lax").build();
        logger.debug("Cookie toevoegen");
        try {
            driver.manage().addCookie(dar);
        } catch (Exception e) {
            logger.debug("stacktrace dar:" + e.getStackTrace().toString());
            logger.debug("message dar: " + e.getMessage());
        }

        try {
            driver.manage().addCookie(language);
        } catch (Exception e) {
            logger.debug("stacktrace language: " + e.getStackTrace().toString());
            logger.debug("message language: " + e.getMessage());
        }
        //driver.manage().addCookie(dar);
        //driver.manage().addCookie(language);



        /*Cookie language = new Cookie.Builder("renta-language", System.getProperty("appLanguage"))//.sameSite("Lax").build()
                .domain(".rentasolutions.org")
                .path("/")
                //.expiresOn(xd)
                .expiresOn(exp)
                .sameSite("Lax").build();
        System.out.println("Cookie toevoegen");
        driver.manage().addCookie(language);*/

        driver.manage().getCookies();

        logger.debug("Nogmaals naar site met dar cookie");
        driver.get("https://delivery.tst.rentasolutions.org");
        //driver.h();
        logger.debug("Cookies : " + driver.manage().getCookies());
        logger.debug("sameSite: " + dar.getSameSite());
        logger.debug("Title: " + driver.getTitle());
        //UserInfo userInfo = new UserInfo(driver);
        welcome = new Welcome(driver);
        logger.debug("Cookies after dar-auth: " + driver.manage().getCookies());
        userInfo = new UserInfo(driver);
        vehicle = new Vehicle(driver);
        returnedVehicle = new ReturnedVehicle(driver);
        newVehicleInfo = new NewVehicleInfo(driver);
        optionsList = new OptionsList(driver);
        confirm = new Confirm(driver);
        driver.get("https://delivery.tst.rentasolutions.org/welcome");
        welcome.selectLanguage();
        driver.get("https://delivery.tst.rentasolutions.org");
        userInfo.input_birthday("1965", "June", "19");
        userInfo.input_email();
        userInfo.select_isdriver();
        userInfo.click_btn_continue();
        vehicle.input_license_plate();
        //vehicle.input_license_plate();
        returnedVehicle.setTogglePreviousVehicleTrue();
        returnedVehicle.setOwner();
        returnedVehicle.input_license_plate();
        newVehicleInfo.validate_new_vehicle();
        optionsList.validate_option_list();
        confirm.validate_confirm();
        //ExtentReport.test.pass("Test passed");



        //vehicle.input_license_plate();


        //driver.pressKey(new KeyEvent(AndroidKey.TAB));
        //driver.pressKey(new KeyEvent(AndroidKey.TAB));
        //driver.pressKey(new KeyEvent(AndroidKey.TAB));
        //driver.pressKey(new KeyEvent(AndroidKey.TAB));
        //driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        //driver.findElement(By.name("q")).sendKeys("rahul shetty academy");
        //driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
        //Thread.sleep(30000);
        //driver.quit();


    }

    /*public static String postGetCookie() throws FileNotFoundException, IOException {
        RestAssured.baseURI = "https://api.dar.tst.rentasolutions.org";
        //GetCookie getCookie = new GetCookie();
        //COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
        String jsonBody = "{\"email\": \"\",\"firstName\": \"Adje\",\"lastName\": \"Antonisse\"}";

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json")
                //.header("cookie", "sso=" + COOKIE)
                //.header("x-wss-rs-user-number", getLeasingCompany())
                //.pathParams("id", orderId)
                .body(jsonBody)
                .log().all();
                //.filter(new AllureRestAssured());

        Response response = request.post("/test/cookie");
        System.out.println("response: " + response.asPrettyString());
        statusCode = String.valueOf(response.getStatusLine());
        String res = response.asPrettyString();

        //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
        //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
        //getFileInputStream.closeInputStream(inputStream);

        return res;

    }*/
}
