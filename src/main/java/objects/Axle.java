package objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Axle {

//	@FindBy(how = How.XPATH, using = "//h3[text()='Axles']")
//	public static WebElement label_axles_info;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/following-sibling::div//input[@formcontrolname='width']")
//	public static WebElement input_tyre_winter_front_width;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/following-sibling::div//input[@formcontrolname='widthHeightRatio']")
//	public static WebElement input_tyre_winter_front_relation_w_h;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/following-sibling::div//input[@formcontrolname='rimDiameter']")
//	public static WebElement input_tyre_winter_front_rim_diameter;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/parent::div//mat-select[@formcontrolname='speedIndicator']//span")
//	public static WebElement open_winter_front_speed_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/parent::div//mat-select[@formcontrolname='loadIndex']//span")
//	public static WebElement open_winter_front_load_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/following-sibling::div//input[@formcontrolname='width']")
//	public static WebElement input_tyre_winter_rear_width;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/following-sibling::div//input[@formcontrolname='widthHeightRatio']")
//	public static WebElement input_tyre_winter_rear_relation_w_h;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/following-sibling::div//input[@formcontrolname='rimDiameter']")
//	public static WebElement input_tyre_winter_rear_rim_diameter;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/parent::div//mat-select[@formcontrolname='speedIndicator']//span")
//	public static WebElement open_winter_rear_speed_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Winter axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/parent::div//mat-select[@formcontrolname='loadIndex']//span")
//	public static WebElement open_winter_rear_load_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/following-sibling::div//input[@formcontrolname='width']")
//	public static WebElement input_tyre_summer_front_width;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/following-sibling::div//input[@formcontrolname='widthHeightRatio']")
//	public static WebElement input_tyre_summer_front_relation_w_h;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/following-sibling::div//input[@formcontrolname='rimDiameter']")
//	public static WebElement input_tyre_summer_front_rim_diameter;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/parent::div//mat-select[@formcontrolname='speedIndicator']//span")
//	public static WebElement open_summer_front_speed_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Front']/parent::div/parent::div//mat-select[@formcontrolname='loadIndex']//span")
//	public static WebElement open_summer_front_load_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/following-sibling::div//input[@formcontrolname='width']")
//	public static WebElement input_tyre_summer_rear_width;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/following-sibling::div//input[@formcontrolname='widthHeightRatio']")
//	public static WebElement input_tyre_summer_rear_relation_w_h;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/following-sibling::div//input[@formcontrolname='rimDiameter']")
//	public static WebElement input_tyre_summer_rear_rim_diameter;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/parent::div//mat-select[@formcontrolname='speedIndicator']//span")
//	public static WebElement open_summer_rear_speed_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Rear']/parent::div/parent::div//mat-select[@formcontrolname='loadIndex']//span")
//	public static WebElement open_summer_rear_load_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Spare']/parent::div/following-sibling::div//input[@formcontrolname='width']")
//	public static WebElement input_tyre_summer_spare_width;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Spare']/parent::div/following-sibling::div//input[@formcontrolname='widthHeightRatio']")
//	public static WebElement input_tyre_summer_spare_relation_w_h;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Spare']/parent::div/following-sibling::div//input[@formcontrolname='rimDiameter']")
//	public static WebElement input_tyre_summer_spare_rim_diameter;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Spare']/parent::div/parent::div//mat-select[@formcontrolname='speedIndicator']//span")
//	public static WebElement open_summer_spare_speed_index_list;
//	
//	@FindBy(how = How.XPATH, using = "//h3[text()='Summer axle']/parent::div/parent::form//span[text() = 'Spare']/parent::div/parent::div//mat-select[@formcontrolname='loadIndex']//span")
//	public static WebElement open_summer_spare_load_index_list;
//
//	@FindBy(how = How.XPATH, using = "(//span[contains(text(), 'Next')])[4]")
//	public static WebElement btn_next_after_axles_input;
//
//	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Save')]")
//	public static WebElement btn_save;
	
}
