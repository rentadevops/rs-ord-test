package objects.database;

import apiEngine.model.database.*;
import com.google.common.primitives.Bytes;
import helpers.ORD_DB_Helper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.openqa.selenium.support.PageFactory;

public class DB_Query {
    static final Logger logger = LogManager.getLogger(DB_Query.class);

    WebDriver driver;
    Order_dto od;
    Leasing_company lc;
    Dealer d;
    Vehicle_dto ve;
    Order_export_dto oe;
    Order_option oo;
    Order_promotion pr;
    Product op;
    private static ORD_DB_Helper odh;
    JSONArray result = new JSONArray();
    UUID uuid;

    public DB_Query(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    static String JDBC_DRIVER = "com.mysql.jdbc.driver";
    static String DB_URL = "jdbc:mysql://172.29.121.160:3306/s4t_ord";

    // Database credentials
    static String USER = "antonissea";
    static String PASS = "!FN88je63lg";

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs;
    JSONObject row = new JSONObject();

    public Order_dto readOrder_dto() throws Exception {
        byte[] order_id;
        Object order_status;
        int version;
        java.sql.Date expected_delivery_date;
        BigDecimal pricing_fleet_discount_amount;
        java.math.BigDecimal pricing_fleet_discount_percentage;
        BigDecimal pricing_list_price;
        String driver_address;
        String driver_city;
        String driver_email;
        String driver_name;
        String driver_phone_number;
        String driver_postal_code;
        String client_address;
        String client_city;
        String client_email;
        String client_name;
        String client_phone_number;
        String client_postal_code;
        String client_contact_person;
        String client_fax_number;
        String client_mobile_number;
        Object client_language_code;
        String client_vat_number;
        Object driver_language_code;
        String driver_mobile_number;
        java.sql.Date desired_delivery_date;
        String leasing_company_dossier_number;
        long dealer_id;
        long leasing_company_id;
        String version_number;
        String delivery_address_line;
        String delivery_address_postal_code;
        String delivery_address_city;
        String delivery_contact_email;
        String delivery_contact_person;
        String delivery_phone_number;
        String delivery_mobile_number;
        String delivery_fax_number;
        Object delivery_location_language;
        String delivery_location_name;
        String delivery_location_vat_number;
        String buyback_address_line;
        String buyback_address_city;
        String buyback_address_postal_code;
        BigDecimal buyback_amount;
        byte buyback;
        int buyback_contract_duration;
        int buyback_kilometers;
        String buyback_name;
        java.sql.Date actual_delivery_date;
        int pickup_construction_year;
        java.sql.Date pickup_date_first_registration;
        String pickup_key_code;
        int pickup_mileage;
        String pickup_start_code;
        String dealer_dossier_number;
        String client_dealer_number;
        java.sql.Date invoice_date;
        byte invoiced;
        int rei_batch_tracking_number;
        int order_version;
        String last_comment_comment;
        java.sql.Date last_comment_creation_date;
        String last_comment_order_change;
        java.sql.Date last_comment_updated_expected_delivery_date;
        String last_comment_updated_order_status;
        long last_comment_user_id;
        long product_id;
        BigDecimal pricing_total_price;
        BigDecimal pricing_delivery_costs;
        BigDecimal pricing_other_costs;
        byte pickup_previous_vehicle_dropoff;
        String pickup_previous_vehicle_license_plate;
        String pickup_previous_vehicle_owner;
        byte already_ordered;
        byte modified;
        long dossier_manager;
        long agent_id;
        long last_comment_agent_id;
        String pickup_driver_first_name;
        String pickup_driver_last_name;
        java.sql.Date pickup_driver_birth_date;
        String pickup_driver_birth_place;
        byte pickup_proof_of_registration;
        byte pickup_proof_of_insurance;
        byte pickup_certificate_of_conformity;
        byte pickup_proof_of_roadworthiness;
        byte pickup_legal_kit;
        byte pickup_manual;
        byte pickup_fuel_card;
        int pickup_number_of_keys;
        long client_registration_number_lessee;
        String insurance_company_name;
        String insurance_company_email;
        BigDecimal insured_value_price;
        String tyrefitter_name;
        String tyrefitter_email;
        java.sql.Date proposed_delivery_date;
        BigDecimal tax_stamp_costs;
        BigDecimal inscription_costs;
        String delivery_contact;
        byte[] linked_registration_id;
        java.sql.Date financial_lease_registration_info_date_last_registration;
        Object registration_div_to_do_by;
        Object leasing_type;
        java.sql.Date busy_until;
        java.sql.Date dealer_due_date;
        java.sql.Date leasing_company_due_date;
        byte dealer_has_unseen_messages;
        byte leasing_company_has_unseen_messages;
        byte redactedGDPR;
        boolean dealer_has_unseen_documents;
        boolean leasing_company_has_unseen_documents;
        long vehicle_dto_id;
        String quote_reference;
        String pickup_address_line;
        String pickup_address_city;
        String pickup_address_postal_code;
        java.sql.Date creation_date;
        String digital_delivery_status;
        byte pickup_main_driver;
        java.sql.Date to_archive_date;
        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_dto WHERE leasing_company_dossier_number LIKE \"%DOSSIERNUMBER-11532%\"", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        ResultSet rs = pstmt.executeQuery();
        logger.debug("result size: " + rs.getMetaData().getColumnName(1));
        od = new Order_dto();
        while (rs.next()) {
            byte[] orderIdByte = rs.getBytes("order_id");
            uuid = java.util.UUID.nameUUIDFromBytes(orderIdByte);
            logger.debug("" + uuid.toString());
            setUuid(uuid);

            orderIdByte = rs.getBytes("order_id");
            od.setorder_id(orderIdByte);
            od.setorder_status(rs.getObject("order_status"));
            od.setversion(rs.getInt("version"));
            od.setexpected_delivery_date(rs.getDate("expected_delivery_date"));
            od.setpricing_fleet_discount_amount(rs.getBigDecimal("pricing_fleet_discount_amount"));
            od.setpricing_fleet_discount_percentage(rs.getBigDecimal("pricing_fleet_discount_percentage"));
            od.setpricing_list_price(rs.getBigDecimal("pricing_list_price"));
            od.setdriver_address(rs.getString("driver_address"));
            od.setdriver_city(rs.getString("driver_city"));
            od.setdriver_email(rs.getString("driver_email"));
            od.setdriver_name(rs.getString("driver_name"));
            od.setdriver_phone_number(rs.getString("driver_phone_number"));
            od.setdriver_postal_code(rs.getString("driver_postal_code"));
            od.setclient_address(rs.getString("client_address"));
            od.setclient_city(rs.getString("client_city"));
            od.setclient_email(rs.getString("client_email"));
            od.setclient_name(rs.getString("client_name"));
            od.setclient_phone_number(rs.getString("client_phone_number"));
            od.setclient_postal_code(rs.getString("client_postal_code"));
            od.setclient_contact_person(rs.getString("client_contact_person"));
            od.setclient_fax_number(rs.getString("client_fax_number"));
            od.setclient_mobile_number(rs.getString("client_mobile_number"));
            od.setclient_language_code(rs.getString("client_language_code"));
            od.setclient_vat_number(rs.getString("client_vat_number"));
            od.setdriver_language_code(rs.getString("driver_language_code"));
            od.setdriver_mobile_number(rs.getString("driver_mobile_number"));
            od.setdesired_delivery_date(rs.getDate("desired_delivery_date"));
            od.setleasing_company_dossier_number(rs.getString("leasing_company_dossier_number"));
            od.setdealer_id(rs.getLong("dealer_id"));
            od.setleasing_company_id(rs.getLong("leasing_company_id"));
            od.setversion_number(rs.getString("version_number"));
            od.setdelivery_address_line(rs.getString("delivery_address_line"));
            od.setdelivery_address_postal_code(rs.getString("delivery_address_postal_code"));
            od.setdelivery_address_city(rs.getString("delivery_address_city"));
            od.setdelivery_contact_email(rs.getString("delivery_contact_email"));
            od.setdelivery_contact_person(rs.getString("delivery_contact_person"));
            od.setdelivery_phone_number(rs.getString("delivery_phone_number"));
            od.setdelivery_mobile_number(rs.getString("delivery_mobile_number"));
            od.setdelivery_fax_number(rs.getString("delivery_fax_number"));
            od.setdelivery_location_language(rs.getObject("delivery_location_language"));
            od.setdelivery_location_name(rs.getString("delivery_location_name"));
            od.setdelivery_location_vat_number(rs.getString("delivery_location_vat_number"));
            od.setbuyback_address_line(rs.getString("buyback_address_line"));
            od.setbuyback_address_city(rs.getString("buyback_address_city"));
            od.setbuyback_address_postal_code(rs.getString("buyback_address_postal_code"));
            od.setbuyback_amount(rs.getBigDecimal("buyback_amount"));
            od.setbuyback(rs.getByte("buyback"));
            od.setbuyback_contract_duration(rs.getInt("buyback_contract_duration"));
            od.setbuyback_kilometers(rs.getInt("buyback_kilometers"));
            od.setbuyback_name(rs.getString("buyback_name"));
            od.setactual_delivery_date(rs.getDate("actual_delivery_date"));
            od.setpickup_construction_year(rs.getInt("pickup_construction_year"));
            od.setpickup_date_first_registration(rs.getDate("pickup_date_first_registration"));
            od.setpickup_key_code(rs.getString("pickup_key_code"));
            od.setpickup_mileage(rs.getInt("pickup_mileage"));
            od.setpickup_start_code(rs.getString("pickup_start_code"));
            od.setdealer_dossier_number(rs.getString("dealer_dossier_number"));
            od.setclient_dealer_number(rs.getString("client_dealer_number"));
            od.setinvoice_date(rs.getDate("invoice_date"));
            od.setinvoiced(rs.getByte("invoiced"));
            od.setrei_batch_tracking_number(rs.getInt("rei_batch_tracking_number"));
            od.setorder_version(rs.getInt("order_version"));
            od.setlast_comment_comment(rs.getString("last_comment_comment"));
            od.setlast_comment_creation_date(rs.getDate("last_comment_creation_date"));
            od.setlast_comment_order_change(rs.getString("last_comment_order_change"));
            od.setlast_comment_updated_expected_delivery_date(rs.getDate("last_comment_updated_expected_delivery_date"));
            od.setlast_comment_updated_order_status(rs.getString("last_comment_updated_order_status"));
            od.setlast_comment_user_id(rs.getLong("last_comment_user_id"));
            od.setproduct_id(rs.getLong("product_id"));
            od.setpricing_total_price(rs.getBigDecimal("pricing_total_price"));
            od.setpricing_delivery_costs(rs.getBigDecimal("pricing_delivery_costs"));
            od.setpricing_other_costs(rs.getBigDecimal("pricing_other_costs"));
            od.setpickup_previous_vehicle_dropoff(rs.getByte("pickup_previous_vehicle_dropoff"));
            od.setpickup_previous_vehicle_license_plate(rs.getString("pickup_previous_vehicle_license_plate"));
            od.setpickup_previous_vehicle_owner(rs.getString("pickup_previous_vehicle_owner"));
            od.setalready_ordered(rs.getByte("already_ordered"));
            od.setmodified(rs.getByte("modified"));
            od.setdossier_manager(rs.getLong("dossier_manager"));
            od.setagent_id(rs.getLong("agent_id"));
            od.setlast_comment_agent_id(rs.getLong("last_comment_agent_id"));
            od.setpickup_driver_first_name(rs.getString("pickup_driver_first_name"));
            od.setpickup_driver_last_name(rs.getString("pickup_driver_last_name"));
            od.setpickup_driver_birth_date(rs.getDate("pickup_driver_birth_date"));
            od.setpickup_driver_birth_place(rs.getString("pickup_driver_birth_place"));
            od.setpickup_proof_of_registration(rs.getByte("pickup_proof_of_registration"));
            od.setpickup_proof_of_insurance(rs.getByte("pickup_proof_of_insurance"));
            od.setpickup_certificate_of_conformity(rs.getByte("pickup_certificate_of_conformity"));
            od.setpickup_proof_of_roadworthiness(rs.getByte("pickup_proof_of_roadworthiness"));
            od.setpickup_legal_kit(rs.getByte("pickup_legal_kit"));
            od.setpickup_manual(rs.getByte("pickup_manual"));
            od.setpickup_fuel_card(rs.getByte("pickup_fuel_card"));
            od.setpickup_number_of_keys(rs.getInt("pickup_number_of_keys"));
            od.setclient_registration_number_lessee(rs.getLong("client_registration_number_lessee"));
            od.setinsurance_company_name(rs.getString("insurance_company_name"));
            od.setinsurance_company_email(rs.getString("insurance_company_email"));
            od.setinsured_value_price(rs.getBigDecimal("insured_value_price"));
            od.settyrefitter_name(rs.getString("tyrefitter_name"));
            od.settyrefitter_email(rs.getString("tyrefitter_email"));
            od.setproposed_delivery_date(rs.getDate("proposed_delivery_date"));
            od.settax_stamp_costs(rs.getBigDecimal("tax_stamp_costs"));
            od.setinscription_costs(rs.getBigDecimal("inscription_costs"));
            od.setdelivery_contact(rs.getString("delivery_contact"));
            od.setlinked_registration_id(rs.getBytes("linked_registration_id"));
            od.setfinancial_lease_registration_info_date_last_registration(rs.getDate("financial_lease_registration_info_date_last_registration"));
            od.setregistration_div_to_do_by(rs.getObject("registration_div_to_do_by"));
            od.setleasing_type(rs.getObject("leasing_type"));
            od.setbusy_until(rs.getDate("busy_until"));
            od.setdealer_due_date(rs.getDate("dealer_due_date"));
            od.setleasing_company_due_date(rs.getDate("leasing_company_due_date"));
            od.setdealer_has_unseen_messages(rs.getByte("dealer_has_unseen_messages"));
            od.setleasing_company_has_unseen_messages(rs.getByte("leasing_company_has_unseen_messages"));
            od.setredactedGDPR(rs.getByte("redactedGDPR"));
            od.setdealer_has_unseen_documents(rs.getBoolean("dealer_has_unseen_documents"));
            od.setleasing_company_has_unseen_documents(rs.getBoolean("leasing_company_has_unseen_documents"));
            od.setvehicle_dto_id(rs.getLong("vehicle_dto_id"));
            od.setquote_reference(rs.getString("quote_reference"));
            od.setpickup_address_line(rs.getString("pickup_address_line"));
            od.setpickup_address_city(rs.getString("pickup_address_city"));
            od.setpickup_address_postal_code(rs.getString("pickup_address_postal_code"));
            od.setcreation_date(rs.getDate("creation_date"));
            od.setdigital_delivery_status(rs.getString("digital_delivery_status"));
            od.setpickup_main_driver(rs.getByte("pickup_main_driver"));
            od.setto_archive_date(rs.getDate("to_archive_date"));
        }

        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());
        logger.debug("order object leasing type: " + od.getleasing_type());

        rs.close();
        pstmt.close();
        //System.out.println("od: " + od.toString());
        return od;
    }

    public Leasing_company readLeasing_company() throws Exception {
        int version;
        String commercial_name;
        int number;
        String phone_number;
        String fax_number;
        String vat_number;
        String invoice_name;
        String address_city;
        String address_country;
        String address_house_number;
        String address_street;
        String address_zip_code;
        String email;
        String contact_person;
        int parent_leasing_company_id;
        String registration_number;
        String ceo_name;
        java.sql.Blob ceo_signature;
        String leasing_company_delivery_first_Name;
        String leasing_company_delivery_last_name_or_company_name;
        String leasing_company_delivery_street;
        String leasing_company_delivery_house_number;
        String leasing_company_delivery_box_number;
        String leasing_company_delivery_postal_code;
        String leasing_company_delivery_city;
        String b_post_delivery_first_name;
        String b_post_delivery_last_name_or_company_name;
        String b_post_delivery_street;
        String b_post_delivery_house_number;
        String b_post_delivery_box_number;
        String b_post_delivery_postal_code;
        String b_post_delivery_city;
        String language_official_communication;
        String green_card_email_default;
        int gdpr_threshold_in_days;
        String application_version;
        String mobile_number;
        int delivery_follow_up_days;
        int delivery_follow_up_days_fallback;

        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.leasing_company WHERE leasing_company_id = " + od.getleasing_company_id(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        logger.debug("pstmt: " + pstmt);
        ResultSet rs = pstmt.executeQuery();
        logger.debug("result size: " + rs.getMetaData().getColumnName(1));
        Leasing_company lc = new Leasing_company();
        while (rs.next()) {
            lc.setVersion(rs.getInt("version"));
            lc.setCommercial_name(rs.getString("commercial_name"));
            lc.setNumber(rs.getInt("number"));
            lc.setPhone_number(rs.getString("phone_number"));
            lc.setFax_number(rs.getString("fax_number"));
            lc.setVat_number(rs.getString("vat_number"));
            lc.setInvoice_name(rs.getString("invoice_name"));
            lc.setAddress_city(rs.getString("address_city"));
            lc.setAddress_country(rs.getString("address_country"));
            lc.setAddress_house_number(rs.getString("address_house_number"));
            lc.setAddress_street(rs.getString("address_street"));
            lc.setAddress_zip_code(rs.getString("address_zip_code"));
            lc.setEmail(rs.getString("email"));
            lc.setContact_person(rs.getString("contact_person"));
            lc.setParent_leasing_company_id(rs.getInt("parent_leasing_company_id"));
            lc.setRegistration_number(rs.getString("registration_number"));
            lc.setCeo_name(rs.getString("ceo_name"));
            lc.setCeo_signature(rs.getBlob("ceo_signature"));
            lc.setLeasing_company_delivery_first_name(rs.getString("leasing_company_delivery_first_name"));
            lc.setLeasing_company_delivery_last_name_or_company_name(rs.getString("leasing_company_delivery_last_name_or_company_name"));
            lc.setLeasing_company_delivery_street(rs.getString("leasing_company_delivery_street"));
            lc.setLeasing_company_delivery_house_number(rs.getString("leasing_company_delivery_house_number"));
            lc.setLeasing_company_delivery_box_number(rs.getString("leasing_company_delivery_box_number"));
            lc.setLeasing_company_delivery_postal_code(rs.getString("leasing_company_delivery_postal_code"));
            lc.setLeasing_company_delivery_city(rs.getString("leasing_company_delivery_city"));
            lc.setB_post_delivery_first_name(rs.getString("b_post_delivery_first_name"));
            lc.setB_post_delivery_last_name_or_company_name(rs.getString("b_post_delivery_last_name_or_company_name"));
            lc.setB_post_delivery_street(rs.getString("b_post_delivery_street"));
            lc.setB_post_delivery_house_number(rs.getString("b_post_delivery_house_number"));
            lc.setB_post_delivery_box_number(rs.getString("b_post_delivery_box_number"));
            lc.setB_post_delivery_postal_code(rs.getString("b_post_delivery_postal_code"));
            lc.setB_post_delivery_city(rs.getString("b_post_delivery_city"));
            lc.setLanguage_official_communication(rs.getString("language_official_communication"));
            lc.setGreen_card_email_default(rs.getString("green_card_email_default"));
            lc.setGdpr_threshold_in_days(rs.getInt("gdpr_threshold_in_days"));
            lc.setApplication_version(rs.getString("application_version"));
            lc.setMobile_number(rs.getString("mobile_number"));
            lc.setDelivery_follow_up_days(rs.getInt("delivery_follow_up_days"));
            lc.setDelivery_follow_up_days_fallback(rs.getInt("delivery_follow_up_days_fallback"));

        }

        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());


        rs.close();
        pstmt.close();
        //System.out.println("od: " + d.toString());
        return lc;
    }

    public Dealer readDealer() throws Exception {
        int version;
        String commercial_name;
        int number;
        String phone_number;
        String fax_number;
        String vat_number;
        String invoice_name;
        String address_city;
        String address_country;
        String address_house_number;
        String address_street;
        String address_zip_code;
        String email;
        String contact_person;
        int parent_leasing_company_id;
        String registration_number;
        String ceo_name;
        java.sql.Blob ceo_signature;
        String leasing_company_delivery_first_Name;
        String leasing_company_delivery_last_name_or_company_name;
        String leasing_company_delivery_street;
        String leasing_company_delivery_house_number;
        String leasing_company_delivery_box_number;
        String leasing_company_delivery_postal_code;
        String leasing_company_delivery_city;
        String b_post_delivery_first_name;
        String b_post_delivery_last_name_or_company_name;
        String b_post_delivery_street;
        String b_post_delivery_house_number;
        String b_post_delivery_box_number;
        String b_post_delivery_postal_code;
        String b_post_delivery_city;
        String language_official_communication;
        String green_card_email_default;
        int gdpr_threshold_in_days;
        String application_version;
        String mobile_number;
        int delivery_follow_up_days;
        int delivery_follow_up_days_fallback;

        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.dealer WHERE dealer_id = " + od.getdealer_id(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        logger.debug("pstmt: " + pstmt);
        ResultSet rs = pstmt.executeQuery();
        logger.debug("result size: " + rs.getMetaData().getColumnName(1));
        Dealer d = new Dealer();
        while (rs.next()) {


            d.setVersion(rs.getInt("version"));
            d.setName(rs.getString("name"));
            d.setNumber(rs.getInt("number"));
            d.setContact_person(rs.getString("contact_person"));
            d.setEmail(rs.getString("email"));
            d.setPhone_number(rs.getString("phone_number"));
            d.setFax_number(rs.getString("fax_number"));
            d.setMobile_number(rs.getString("mobile_number"));
            d.setVat_number(rs.getString("vat_number"));
            d.setAddress_city(rs.getString("address_city"));
            d.setAddress_country(rs.getString("address_country"));
            d.setAddress_house_number(rs.getString("address_house_number"));
            d.setAddress_street(rs.getString("address_street"));
            d.setAddress_zip_code(rs.getString("address_zip_code"));
            d.setBank_account_bic(rs.getString("bank_account_bic"));
            d.setCompany_registration_number(rs.getString("company_registration_number"));
            d.setBank_account_iban(rs.getString("bank_account_iban"));
            d.setInvoicing_city(rs.getString("invoicing_city"));
            d.setInvoicing_city(rs.getString("invoicing_country"));
            d.setInvoicing_city(rs.getString("invoicing_house_number"));
            d.setInvoicing_city(rs.getString("invoicing_street"));
            d.setInvoicing_city(rs.getString("invoicing_zip_code"));
            d.setInvoicing_name(rs.getString("invoicing_name"));
            d.setRpr(rs.getString("rpr"));
            d.setParent_dealer_number(rs.getInt("parent_dealer_number"));
            d.setSupplier_brand(rs.getInt("supplier_brand"));
            d.setApplication_version(rs.getString("application_version"));
            d.setDealer_type(rs.getString("dealer_type"));
        }

        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());


        rs.close();
        pstmt.close();
        //System.out.println("od: " + d.toString());
        return d;
    }

    public Vehicle_dto readVehicle() throws Exception {
        int authorized_mass_max;
        String brand;
        double co2_emissions_nedc;
        int cylinder_content;
        String exterior_colour_dutch;
        String exterior_colour_english;
        String exterior_colour_french;
        String fuel_label;
        String identification;
        String interior_colour_dutch;
        String interior_colour_english;
        String interior_colour_french;
        String kind_label;
        String license_plate;
        int mass_on_coupling;
        String model_dutch;
        String model_english;
        String model_french;
        int number_of_seats;
        int power;
        String stock_vehicle;
        String tyre_brand;
        String tyre_description;
        String version_dutch;
        String version_english;
        String version_french;
        String vin_control_number;
        String vin;
        int weight;
        String frame_size;
        String frame_size_unit;
        String non_vin_identification_number;
        String frame_number;
        String frame_type;
        String segment_dutch;
        String segment_english;
        String segment_french;
        int construction_year;
        String gear;
        String battery_power;
        String battery_power_unit;
        double co2_emissions_wltp;
        String form_number;

        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.vehicle_dto WHERE vehicle_dto_id = " + od.getvehicle_dto_id(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        logger.debug("pstmt: " + pstmt);
        ResultSet rs = pstmt.executeQuery();


        /*int size =0;
        if (rs != null)
        {
            rs.beforeFirst();
            rs.last();
            size = rs.getRow();
            logger.debug("rs: " + rs.getString("vin"));
        }*/
        //logger.debug("result size: " + size);
        Vehicle_dto ve = new Vehicle_dto();
        while (rs.next()) {


            ve.setAuthorized_mass_max(rs.getInt("authorized_mass_max"));
            ve.setBrand(rs.getString("brand"));
            ve.setCo2_emissions_nedc(rs.getDouble("co2_emissions_nedc"));
            ve.setCylinder_content(rs.getInt("cylinder_content"));
            ve.setExterior_colour_dutch(rs.getString("exterior_colour_dutch"));
            ve.setExterior_colour_english(rs.getString("exterior_colour_english"));
            ve.setExterior_colour_french(rs.getString("exterior_colour_french"));
            ve.setFuel_label(rs.getString("fuel_label"));
            ve.setIdentification(rs.getString("identification"));
            ve.setInterior_colour_dutch(rs.getString("interior_colour_dutch"));
            ve.setInterior_colour_english(rs.getString("interior_colour_english"));
            ve.setInterior_colour_french(rs.getString("interior_colour_french"));
            ve.setKind_label(rs.getString("kind_label"));
            ve.setLicense_plate(rs.getString("license_plate"));
            ve.setMass_on_coupling(rs.getInt("mass_on_coupling"));
            ve.setModel_dutch(rs.getString("model_dutch"));
            ve.setModel_english(rs.getString("model_english"));
            ve.setModel_french(rs.getString("model_french"));
            ve.setNumber_of_seats(rs.getInt("number_of_seats"));
            ve.setPower(rs.getInt("power"));
            ve.setStock_vehicle(rs.getString("stock_vehicle"));
            ve.setTyre_brand(rs.getString("tyre_brand"));
            ve.setTyre_description(rs.getString("tyre_description"));
            ve.setVersion_dutch(rs.getString("version_dutch"));
            ve.setVersion_english(rs.getString("version_english"));
            ve.setVersion_french(rs.getString("version_french"));
            ve.setVin_control_number(rs.getString("vin_control_number"));
            ve.setVin(rs.getString("vin"));
            ve.setWeight(rs.getInt("weight"));
            ve.setFrame_size(rs.getString("frame_size"));
            ve.setFrame_size_unit(rs.getString("frame_size_unit"));
            ve.setNon_vin_identification_number(rs.getString("non_vin_identification_number"));
            ve.setFrame_number(rs.getString("frame_number"));
            ve.setFrame_type(rs.getString("frame_type"));
            ve.setSegment_dutch(rs.getString("segment_dutch"));
            ve.setSegment_english(rs.getString("segment_english"));
            ve.setSegment_french(rs.getString("segment_french"));
            ve.setConstruction_year(rs.getInt("construction_year"));
            ve.setGear(rs.getString("gear"));
            ve.setBattery_power(rs.getString("battery_power"));
            ve.setBattery_power_unit(rs.getString("battery_power_unit"));
            ve.setCo2_emissions_wltp(rs.getDouble("co2_emissions_wltp"));
            ve.setForm_number(rs.getString("form_number"));
        }

        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());


        rs.close();
        pstmt.close();
        //System.out.println("od: " + d.toString());
        return ve;
    }

    public Order_export_dto readOrder_export_dto() throws Exception {

        byte[] order_id = new byte[0];
        int version;
        String brand;
        String client_name;
        String dealer_dossier_number;
        String dealer_name;
        String driver_name;
        String leasing_company_dossier_number;
        String leasing_company_name;
        int leasing_company_number;
        String license_plate;
        String vehicle_model_dutch;
        String vehicle_model_french;
        int dealer_number;
        String product_name;
        int user_id;
        String vehicle_vin;
        String dealer_invoice_name;
        String dealer_vat_number;
        int agent_id;
        String vehicle_model_english;
        String version_number;
        String vehicle_version_dutch;
        String vehicle_version_french;
        String vehicle_version_english;
        String vehicle_exterior_colour_dutch;
        String vehicle_exterior_colour_french;
        String vehicle_exterior_colour_english;
        String vehicle_interior_colour_dutch;
        String vehicle_interior_colour_french;
        String vehicle_interior_colour_english;
        double pricing_total_price;
        String vehicle_vin_control_number;
        double vehicle_co2_emissions_nedc;
        int pickup_mileage;
        int pickup_construction_year;
        String pickup_key_code;
        String pickup_start_code;
        String pickup_previous_vehicle_license_plate;
        String pickup_previous_vehicle_owner;
        int pickup_number_of_keys;
        String pickup_driver_first_name;
        String pickup_driver_last_name;
        String pickup_driver_birth_place;
        String client_dealer_number;
        String insurance_broker_name;
        String insurance_company_email;
        String tyre_fitter_name;
        String tyre_fitter_email;
        String vehicle_tyre_brand;
        String vehicle_tyre_description;
        String leasing_type;
        String last_comment;
        double vehicle_co2_emissions_wltp;
        String driver_email;
        String pickup_address_line;
        String pickup_address_city;
        String pickup_address_postal_code;

        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_export_dto WHERE leasing_company_dossier_number = " + "'" + od.getleasing_company_dossier_number() + "'", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        //PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_export_dto WHERE order_id = " +  order_id = String.valueOf(od.getorder_id()), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );

         ResultSet rs = pstmt.executeQuery();
        logger.debug("result size: " + rs.getMetaData().getColumnName(1));
        oe = new Order_export_dto();
        while (rs.next()) {

            byte[] orderIdByte = rs.getBytes("order_id");
            uuid = java.util.UUID.nameUUIDFromBytes(orderIdByte);
            logger.debug("" + uuid.toString());
            setUuid(uuid);
            oe.setorder_id(orderIdByte);
            oe.setBrand(rs.getString("brand"));
            oe.setClient_name(rs.getString("client_name"));
            oe.setDealer_dossier_number(rs.getString("dealer_dossier_number"));
            oe.setDealer_name(rs.getString("dealer_name"));
            oe.setDriver_name(rs.getString("driver_name"));
            oe.setLeasing_company_dossier_number(rs.getString("leasing_company_dossier_number"));
            oe.setLeasing_company_name(rs.getString("leasing_company_name"));
            oe.setLeasing_company_number(rs.getInt("leasing_company_number"));
            oe.setLicense_plate(rs.getString("license_plate"));
            oe.setVehicle_model_dutch(rs.getString("vehicle_model_dutch"));
            oe.setVehicle_model_french(rs.getString("vehicle_model_french"));
            oe.setDealer_number(rs.getInt("dealer_number"));
            oe.setProduct_name(rs.getString("product_name"));
            oe.setUser_id(rs.getInt("user_id"));
            oe.setVehicle_vin(rs.getString("vehicle_vin"));
            oe.setDealer_invoice_name(rs.getString("dealer_invoice_name"));
            oe.setDealer_vat_number(rs.getString("dealer_vat_number"));
            oe.setAgent_id(rs.getInt("agent_id"));
            oe.setVehicle_model_english(rs.getString("vehicle_model_english"));
            oe.setVersion_number(rs.getString("version_number"));
            oe.setVehicle_version_dutch(rs.getString("vehicle_version_dutch"));
            oe.setVehicle_version_french(rs.getString("vehicle_version_french"));
            oe.setVehicle_version_english(rs.getString("vehicle_version_english"));
            oe.setVehicle_exterior_colour_dutch(rs.getString("vehicle_exterior_colour_dutch"));
            oe.setVehicle_exterior_colour_french(rs.getString("vehicle_exterior_colour_french"));
            oe.setVehicle_exterior_colour_english(rs.getString("vehicle_exterior_colour_english"));
            oe.setVehicle_interior_colour_dutch(rs.getString("vehicle_interior_colour_dutch"));
            oe.setVehicle_interior_colour_french(rs.getString("vehicle_interior_colour_french"));
            oe.setVehicle_interior_colour_english(rs.getString("vehicle_interior_colour_english"));
            oe.setPricing_total_price(rs.getDouble("pricing_total_price"));
            oe.setVehicle_vin_control_number(rs.getString("vehicle_vin_control_number"));
            oe.setVehicle_co2_emissions_nedc(rs.getDouble("vehicle_co2_emissions_nedc"));
            oe.setPickup_mileage(rs.getInt("pickup_mileage"));
            oe.setPickup_construction_year(rs.getInt("pickup_construction_year"));
            oe.setPickup_key_code(rs.getString("pickup_key_code"));
            oe.setPickup_start_code(rs.getString("pickup_start_code"));
            oe.setPickup_previous_vehicle_license_plate(rs.getString("pickup_previous_vehicle_license_plate"));
            oe.setPickup_previous_vehicle_owner(rs.getString("pickup_previous_vehicle_owner"));
            oe.setPickup_number_of_keys(rs.getInt("pickup_number_of_keys"));
            oe.setPickup_driver_first_name(rs.getString("pickup_driver_first_name"));
            oe.setPickup_driver_last_name(rs.getString("pickup_driver_last_name"));
            oe.setPickup_driver_birth_place(rs.getString("pickup_driver_birth_place"));
            oe.setClient_dealer_number(rs.getString("client_dealer_number"));
            oe.setInsurance_broker_name(rs.getString("insurance_broker_name"));
            oe.setInsurance_company_email(rs.getString("insurance_company_email"));
            oe.setTyre_fitter_name(rs.getString("tyre_fitter_name"));
            oe.setTyre_fitter_email(rs.getString("tyre_fitter_email"));
            oe.setVehicle_tyre_brand(rs.getString("vehicle_tyre_brand"));
            oe.setVehicle_tyre_description(rs.getString("vehicle_tyre_description"));
            oe.setLeasing_type(rs.getString("leasing_type"));
            oe.setLast_comment(rs.getString("last_comment"));
            oe.setVehicle_co2_emissions_wltp(rs.getDouble("vehicle_co2_emissions_wltp"));
            oe.setDriver_email(rs.getString("driver_email"));
            oe.setPickup_address_line(rs.getString("pickup_address_line"));
            oe.setPickup_address_city(rs.getString("pickup_address_city"));
            oe.setPickup_address_postal_code(rs.getString("pickup_address_postal_code"));

        }

        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());
        logger.debug("order object leasing type: " + od.getleasing_type());

        rs.close();
        pstmt.close();
        //System.out.println("od: " + od.toString());
        return oe;
    }

    public Order_option readOrder_option() throws Exception {

        byte[] order_id;
        int version;
        double option_amount_fleet_discount;
        String option_codes;
        String option_description_dutch;
        double option_list_price;
        double option_percentage_fleet_discount;
        String option_supplier_name;
        String option_supplier_number;
        String option_type;
        String option_description_french;
        String option_name_dutch;
        String option_name_french;
        String option_name_english;
        String option_description_english;
        double applicable_vat_percentage;

        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

                                                            //SELECT * FROM order_option WHERE order_id IN(SELECT order_id FROM order_dto WHERE s4t_ord.order_dto.leasing_company_dossier_number = "DOSSIERNUMBER-11532")
        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_option WHERE order_id IN(SELECT order_id FROM order_dto WHERE s4t_ord.order_dto.leasing_company_dossier_number = " + '"' + od.getleasing_company_dossier_number() + '"' + ")", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        logger.debug("pstmt: " + pstmt.toString());
        //PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_export_dto WHERE order_id = " +  order_id = String.valueOf(od.getorder_id()), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );

        ResultSet rs = pstmt.executeQuery();
        logger.debug("result size: " + rs.getMetaData().getColumnName(1));
        oo = new Order_option();

        //ArrayList<Order_option> options = new ArrayList<>();
        List<Order_option> options = new ArrayList<Order_option>();
        Order_option oro = new Order_option();

        while (rs.next()) {

            oo = new Order_option();

            oo.setorder_id(rs.getBytes("order_id"));
            uuid = java.util.UUID.nameUUIDFromBytes(oo.getorder_id());
            logger.debug("" + uuid.toString());
            setUuid(uuid);

            oo.setVersion(rs.getInt("version"));
            oo.setOption_amount_fleet_discount(rs.getDouble("option_amount_fleet_discount"));
            oo.setOption_codes(rs.getString("option_codes"));
            oo.setOption_description_dutch(rs.getString("option_description_dutch"));
            oo.setOption_list_price(rs.getDouble("option_list_price"));
            oo.setOption_percentage_fleet_discount(rs.getDouble("option_percentage_fleet_discount"));
            oo.setOption_supplier_name(rs.getString("option_supplier_name"));
            oo.setOption_supplier_number(rs.getString("option_supplier_number"));
            oo.setOption_type(rs.getString("option_type"));
            oo.setOption_description_french(rs.getString("option_description_french"));
            oo.setOption_name_dutch(rs.getString("option_name_dutch"));
            oo.setOption_name_french(rs.getString("option_name_french"));
            oo.setOption_name_english(rs.getString("option_name_english"));
            oo.setOption_description_english(rs.getString("option_description_english"));
            oo.setApplicable_vat_percentage(rs.getDouble("applicable_vat_percentage"));
            options.add(oo);

        }

        logger.debug("options: " + options.toString());
        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());
        //logger.debug("order object leasing type: " + od.getleasing_type());

        rs.close();
        pstmt.close();
        //System.out.println("od: " + od.toString());
        return oo;
    }

    public Order_promotion readOrder_promotion() throws Exception {

        byte[] order_id;
        double promotion_amount;
        double promotion_percentage;
        String promotion_text_dutch;
        int version;
        String promotion_text_french;
        String promotion_name_dutch;
        String promotion_name_french;
        String promotion_description_dutch;
        String promotion_description_french;
        String promotion_name_english;
        String promotion_description_english;

        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

        //SELECT * FROM order_option WHERE order_id IN(SELECT order_id FROM order_dto WHERE s4t_ord.order_dto.leasing_company_dossier_number = "DOSSIERNUMBER-11532")
        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_promotion WHERE order_id IN(SELECT order_id FROM order_dto WHERE s4t_ord.order_dto.leasing_company_dossier_number = " + '"' + od.getleasing_company_dossier_number() + '"' + ")", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        logger.debug("pstmt: " + pstmt.toString());
        //PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_export_dto WHERE order_id = " +  order_id = String.valueOf(od.getorder_id()), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );

        ResultSet rs = pstmt.executeQuery();
        logger.debug("result size: " + rs.getMetaData().getColumnName(1));
        pr = new Order_promotion();

        //ArrayList<Order_option> options = new ArrayList<>();
        List<Order_promotion> promo = new ArrayList<Order_promotion>();
        Order_promotion opr = new Order_promotion();

        while (rs.next()) {

            pr = new Order_promotion();

            pr.setOrder_promotion(rs.getBytes("order_id"));
            uuid = java.util.UUID.nameUUIDFromBytes(oo.getorder_id());
            logger.debug("" + uuid);
            setUuid(uuid);

            pr.setPromotion_amount(rs.getDouble("promotion_amount"));
            pr.setPromotion_percentage(rs.getDouble("promotion_percentage"));
            pr.setPromotion_text_dutch(rs.getString("promotion_text_dutch"));
            pr.setVersion(rs.getInt("version"));
            pr.setPromotion_text_french(rs.getString("promotion_text_french"));
            pr.setPromotion_name_dutch(rs.getString("promotion_name_dutch"));
            pr.setPromotion_name_french(rs.getString("promotion_name_french"));
            pr.setPromotion_description_dutch(rs.getString("promotion_description_dutch"));
            pr.setPromotion_description_french(rs.getString("promotion_description_french"));
            pr.setPromotion_name_english(rs.getString("promotion_name_english"));
            pr.setPromotion_description_english(rs.getString("promotion_description_english"));

            promo.add(opr);

        }

        logger.debug("promotions: " + promo.toString());
        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());
        //logger.debug("order object leasing type: " + od.getleasing_type());

        rs.close();
        pstmt.close();
        //System.out.println("od: " + od.toString());
        return pr;
    }

    public Product readProduct() throws Exception {

        byte[] order_id;
        int version;
        String name;
        String reference_code;
        int leasing_company_id;
        java.sql.Blob logo_file;
        java.sql.Blob terms_and_conditions_file;
        String logo_name;
        java.sql.Blob mobile_logo_file;
        String mobile_logo_name;

        logger.debug("Start");
        conn = DriverManager.getConnection(DB_URL, USER,PASS);
        logger.debug("Connectie gestart");

        //SELECT * FROM order_option WHERE order_id IN(SELECT order_id FROM order_dto WHERE s4t_ord.order_dto.leasing_company_dossier_number = "DOSSIERNUMBER-11532")
        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.product WHERE product_id IN(SELECT order_id FROM order_dto WHERE s4t_ord.order_dto.leasing_company_dossier_number = " + '"' + od.getleasing_company_dossier_number() + '"' + ")", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
        logger.debug("pstmt: " + pstmt.toString());
        //PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM s4t_ord.order_export_dto WHERE order_id = " +  order_id = String.valueOf(od.getorder_id()), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );

        ResultSet rs = pstmt.executeQuery();
        logger.debug("result size: " + rs.getMetaData().getColumnName(1));
        op = new Product();

        //List<Order_option> options = new ArrayList<Order_option>();
        //Order_option oro = new Order_option();

        while (rs.next()) {

            //oo = new Order_option();

            //oo.setorder_id(rs.getBytes("order_id"));
            //uuid = java.util.UUID.nameUUIDFromBytes(oo.getorder_id());
            //logger.debug("" + uuid.toString());
            //setUuid(uuid);

            op.setVersion(rs.getInt("version"));
            op.setName(rs.getString("name"));
            op.setReference_code(rs.getString("reference_code"));
            op.setLeasing_company_id(rs.getInt("leasing_company_id"));
            op.setLogo_file(rs.getBlob("logo_file"));
            op.setTerms_and_conditions_file(rs.getBlob("terms_and_conditions_file"));
            op.setLogo_name(rs.getString("logo_name"));
            op.setMobile_logo_file(rs.getBlob("mobile_logo_file"));
            op.setLogo_name(rs.getString("logo_name"));
            //options.add(oo);

        }

        //logger.debug("options: " + options.toString());
        logger.debug("resultset: " + rs.toString());
        logger.debug("jsonobject: " + row.toString());
        //logger.debug("order object leasing type: " + od.getleasing_type());

        rs.close();
        pstmt.close();
        //System.out.println("od: " + od.toString());
        return op;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
