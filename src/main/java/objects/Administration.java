package objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Administration {

	@FindBy(how = How.XPATH, using = "(//h2)[1]/span") // Active/Inactive
	public static WebElement active;

	@FindBy(how = How.XPATH, using = "(//h2)[1]") // element contains lc name and active
	public static WebElement administration_name;

	@FindBy(how = How.XPATH, using = "(//h2)[1]/ancestor::div[2]/following-sibling::div//span")
	public static WebElement administration_number;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Parent company')]/ancestor::div[2]/div[1]")
	public static WebElement parent_company;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Vehicles')]/ancestor::div[2]/div[1]")
	public static WebElement vehicles;

	@FindBy(how = How.XPATH, using = "//mat-icon[contains(.,'email')]/following-sibling::span")
	public static WebElement email;

	@FindBy(how = How.XPATH, using = "//mat-icon[contains(.,'smartphone')]/following-sibling::span")
	public static WebElement mobile_number;

	@FindBy(how = How.XPATH, using = "//mat-icon[contains(.,'print')]/following-sibling::span")
	public static WebElement fax_number;

	@FindBy(how = How.XPATH, using = "//mat-icon[contains(.,'chat_bubble_outline')]/following-sibling::span")
	public static WebElement language;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Billing name')]/following-sibling::div")
	public static WebElement billing_name;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Address')]/following-sibling::div")
	public static WebElement billing_address;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Country')]/following-sibling::div")
	public static WebElement billing_country;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'VAT number')]/following-sibling::div")
	public static WebElement billing_vat_number;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'VAT unit')]/following-sibling::div")
	public static WebElement billing_vat_unit;
	
}
