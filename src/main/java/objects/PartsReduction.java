package objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PartsReduction {
	
	@FindBy(how = How.XPATH, using = "//mat-select[@ng-reflect-name = 'activityGroup']")
	public  WebElement open_activity_group_list;
	
	@FindBy(how = How.XPATH, using = "//div[@id='Activity group']//span[contains(text(), 'Next')]")
	public static WebElement btn_next_after_activity_group_input;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@ng-reflect-name = 'supplierGroup']/div/div[2]")
	public static WebElement open_supplier_group_list;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='partsReductionPercentage']")
	public static WebElement input_reduction;

	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Save')]")
	public static WebElement btn_save;

}
