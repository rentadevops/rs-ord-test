package objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SupplierGroup {
	
	//Edit Supplier Group
//	@FindBy(how = How.XPATH, using = "//h2[@id='group']/parent::div/parent::div/child::div/child::button")
//	public static WebElement btn_edit_supplier_group_info;
//	
//	@FindBy(how = How.XPATH, using = "//input[@placeholder='Name']")
//	public static WebElement input_supplier_group_name;
//	
//	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@formcontrolname='active']//input[@type='checkbox']")
//	public static WebElement slider_active;
//	
//	@FindBy(how = How.XPATH, using = "//input[@placeholder='Description']")
//	public static WebElement input_supplier_group_description;
//	
//	@FindBy(how = How.XPATH, using = "//mat-checkbox[@formcontrolname='allowParallelWorkOrder']//input[@type='checkbox']")
//	public static WebElement checkbox_allow_parallel_work_orders;
//	
//	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
//	public static WebElement btn_save_supplier_group_info;
//	
//	@FindBy(how = How.XPATH, using = "//h2[@id='competences']/parent::div/parent::div/child::div/child::button")
//	public static WebElement btn_edit_competences;
//	
//	@FindBy(how = How.XPATH, using = "//div[@formarrayname='competences'][1]//mat-checkbox//input")
//	public static WebElement checkbox_maintenance;
//	
//	@FindBy(how = How.XPATH, using = "//div[@formarrayname='competences'][2]//mat-checkbox//input")
//	public static WebElement checkbox_repair;
//	
//	@FindBy(how = How.XPATH, using = "//div[@formarrayname='competences'][3]//mat-checkbox//input")
//	public static WebElement checkbox_replacement_vehicle;
//	
//	@FindBy(how = How.XPATH, using = "//div[@formarrayname='competences'][4]//mat-checkbox//input")
//	public static WebElement checkbox_summer_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//div[@formarrayname='competences'][5]//mat-checkbox//input")
//	public static WebElement checkbox_winter_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//div[@formarrayname='competences'][6]//mat-checkbox//input")
//	public static WebElement checkbox_all_weather_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
//	public static WebElement btn_save_competences;
//	
//	@FindBy(how = How.XPATH, using = "//h2[@id='priceSettings']/parent::div/parent::div/child::div/child::button")
//	public static WebElement btn_edit_price_settings;
//	
//	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='priorityThreshold']")
//	public static WebElement open_priority_threshold_list;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='hourlyRateThreshold']")
//	public static WebElement input_hourlyRateThreshold;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='oilRateThreshold']")
//	public static WebElement input_oilRateThreshold;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='tyreTolerance']")
//	public static WebElement input_tyreTolerance;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='activityThresholdTolerance']")
//	public static WebElement input_activityThresholdTolerance;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='discountHourlyRate']")
//	public static WebElement input_discountHourlyRate;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='partsDiscount']")
//	public static WebElement input_partsDiscount;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='allInDiscount']")
//	public static WebElement input_allInDiscount;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='discountReplacementVehicle']")
//	public static WebElement input_discountReplacementVehicle;
//	
//	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
//	public static WebElement btn_save_price_settings;
//	
//	@FindBy(how = How.XPATH, using = "//h2[@id='serviceCostsTyres']/parent::div/parent::div/child::div/child::button")
//	public static WebElement btn_edit_service_costs_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='serviceCostTyreSwitch']")
//	public static WebElement input_serviceCostTyreSwitch;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='serviceCostTyreSwitchWithRim']")
//	public static WebElement input_serviceCostTyreSwitchWithRim;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='serviceCostTyreReplaceByNew']")
//	public static WebElement input_serviceCostTyreReplaceByNew;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='serviceCostTyreReplaceBySpare']")
//	public static WebElement input_serviceCostTyreReplaceBySpare;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='serviceCostTyreSwitchRunFlat']")
//	public static WebElement input_serviceCostTyreSwitchRunFlat;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='serviceCostTyreReplaceRunFlat']")
//	public static WebElement input_serviceCostTyreReplaceRunFlat;
//	
//	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
//	public static WebElement btn_save_service_costs_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//h2[@id='subleases']/parent::div/parent::div/child::div/child::button")
//	public static WebElement btn_edit_subleases;
//	
//	@FindBy(how = How.XPATH, using = "//div[@class='col-2 justify-content-between ng-star-inserted'][1]//input")
//	public static WebElement checkbox_sublease_maintenance;
//	
//	@FindBy(how = How.XPATH, using = "//div[@class='col-2 justify-content-between ng-star-inserted'][2]//input")
//	public static WebElement checkbox_sublease_repair;
//	
//	@FindBy(how = How.XPATH, using = "//div[@class='col-2 justify-content-between ng-star-inserted'][3]//input")
//	public static WebElement checkbox_sublease_replacement_vehicle;
//	
//	@FindBy(how = How.XPATH, using = "//div[@class='col-2 justify-content-between ng-star-inserted'][4]//input")
//	public static WebElement checkbox_sublease_summer_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//div[@class='col-2 justify-content-between ng-star-inserted'][5]//input")
//	public static WebElement checkbox_sublease_winter_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//div[@class='col-2 justify-content-between ng-star-inserted'][6]//input")
//	public static WebElement checkbox_sublease_all_weather_tyres;
//	
//	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='priorityThreshold']")
//	public static WebElement open_sublease_priority_threshold_list;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='hourlyRateThreshold']")
//	public static WebElement input_sublease_hourlyRateThreshold;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='oilRateThreshold']")
//	public static WebElement input_sublease_oilRateThreshold;
//	
//	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='partsPercentage']")
//	public static WebElement input_sublease_partsPercentage;
//	
//	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
//	public static WebElement btn_save_subleases;
//	
//	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Delete')]")
//	public static WebElement btn_delete_sublease;
//	
//	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Cancel')]")
//	public static WebElement btn_cancel;

}
