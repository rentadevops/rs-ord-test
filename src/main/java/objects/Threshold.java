package objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Threshold {

//	@FindBy(how = How.XPATH, using = "//input[contains(@data-placeholder, 'Search an activity')]")
//	public static WebElement input_activity;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Activity']//mat-select[@id='task']")
//	public static WebElement open_task_list;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Activity']//span[contains(text(), 'Next')]")
//	public static WebElement btn_next_after_activity_task_input;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//mat-select[@formcontrolname='sublease']")
//	public static WebElement open_sublease_list;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//mat-select[@formcontrolname='brand']")
//	public static WebElement open_brand_list;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//mat-select[@formcontrolname='modelDescription']")
//	public static WebElement open_model_list;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//mat-datepicker-toggle")
//	public static WebElement select_date_picker_date_first_registration;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//mat-select[@formcontrolname='fuelCode']")
//	public static WebElement open_fuel_list;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//input[@formcontrolname='power']")
//	public static WebElement input_power;
//
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//input[@formcontrolname='cylinderContent']")
//	public static WebElement input_cylinder_content;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//input[@formcontrolname='transmission']")
//	public static WebElement input_transmission;
//	
//	@FindBy(how = How.XPATH, using = "//div[@id='Vehicle']//span[contains(text(), 'Next')]")
//	public static WebElement btn_next_after_vehicle_input;
//	
//	@FindBy(how = How.XPATH, using = "//input[@data-placeholder='0,00']")
//	public static  WebElement input_amount;
//	
//	@FindBy(how = How.XPATH, using = "//input[@value='TIME_ONLY']")
//	public static WebElement radio_threshold_type_time_only;
//	
//	@FindBy(how = How.XPATH, using = "//input[@value='GLOBAL']")
//	public static WebElement radio_threshold_type_global;
//
//	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
//	public static WebElement btn_save;
	
	//Delete Supplier Group Threshold
	@FindBy(how = How.XPATH, using = "//h2[@id='threshold-amount']/parent::div/parent::div/child::div/child::button")
	public static WebElement btn_edit_threshold;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), ' Delete ')]")
	public static WebElement btn_delete_leasing_company_theshold;

	@FindBy(how = How.XPATH, using = "//h1/parent::ui-renta-save-dialog-edit/child::div/child::button[2]")
	public static WebElement btn_comfirm_delete_leasing_company_theshold;
	
	//Edit buttons
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public static WebElement btn_save_threshold;
	
	@FindBy(how = How.XPATH, using = "//h2[@id='vehicle']/parent::div/parent::div/child::div/child::button")
	public static WebElement btn_edit_vehicle_info;
	
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public static WebElement btn_save_vehicle_info;
	
	//Threshold Overview
	@FindBy(how = How.XPATH, using = "(//ui-renta-leasing-company-threshold-activity//span)[1]")
	public static WebElement output_activity;
	
	@FindBy(how = How.XPATH, using = "(//ui-renta-leasing-company-threshold-activity//span)[2]")
	public static WebElement output_activity_description;
	
	@FindBy(how = How.XPATH, using = "//ui-renta-leasing-company-threshold-activity/div[2]//span")
	public static WebElement output_task;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Sublease')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_sublease;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Brand')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_brand;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Model')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_model;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'First registration date')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_first_registation_date;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Fuel')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_fuel;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Power')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_power;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Cylinder content')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_cylinder_content;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Transmission')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_transmission;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Type')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_thresholdType;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Amount')]//parent::div//parent::div//child::div[2]//child::span")
	public static WebElement output_amount;
	
	//Leasing Company Threshold page
	@FindBy(how = How.XPATH, using = "//span[contains(text(), ' New ')]")
	public static WebElement btn_add;
	
	@FindBy(how = How.XPATH, using = "//mat-icon[contains(text(), 'filter_list')]")
	public static WebElement btn_filter;
	
	@FindBy(how = How.XPATH, using = "//section//mat-icon[contains(text(), 'close')]")
	public static WebElement btn_close_filter;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-reflect-name = 'activityCodeFrom']")
	public static WebElement input_activity_code_from;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-reflect-name = 'activityCodeTo']")
	public static WebElement input_activity_code_to;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-reflect-name = 'activityDescription']")
	public static WebElement input_activity_description;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td/span")
	public static WebElement first_result;
	
}
