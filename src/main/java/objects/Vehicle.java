package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Vehicle {

	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='dateFirstRegistration']")
	public static WebElement select_date_picker_date_first_registration;
	
	
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Choose month and year']")
	public static WebElement select_choose_month_and_year;
	
	@FindBy(how = How.XPATH, using = "//h3[text()='Vehicle']")
	public static WebElement label_vehicle;
	
	@FindBy(how = How.XPATH, using = "//form[@id='vehicleForm']//input[@formcontrolname='licensePlate']")
	public static WebElement input_licensePlate;
	
	
	@FindBy(how = How.XPATH, using = "//form[@id='vehicleForm']//input[@formcontrolname='vin']")
	public static WebElement input_vin;
	
	@FindBy(how = How.XPATH, using = "//form[@id='vehicleForm']//input[@formcontrolname='version']")
	public static WebElement input_version;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='vehicleType']/div/div/span")
	public static WebElement open_vehicle_type_list;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='fuelCode']//span")
	public static WebElement open_fuel_list;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='brand']//span")
	public static WebElement open_brand_list;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='description']//span")
	public static WebElement open_model_list;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='fuelcode']//span")
	public static WebElement input_fuelcode;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='transmission']//span")
	public static WebElement input_transmission;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='power']//span")
	public static WebElement input_power;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='cylinderContent']//span")
	public static WebElement input_cylinderContent;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='gears']//span")
	public static WebElement input_gears;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='traction']//span")
	public static WebElement input_traction;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@formcontrolname='catalyst']//input")
	public static WebElement checkbox_catalyst;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='hpDin']//span")
	public static WebElement input_hpDin;
	
	@FindBy(how = How.XPATH, using = "//mat-checkbox[@formcontrolname='particleFilter']//input")
	public static WebElement checkbox_particleFilter;
	
	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='euroNorm']//span")
	public static WebElement input_euroNorm;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(), 'Next')])[1]")
	public static WebElement btn_next_after_vehicle_input;
	
}
