package apiEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import apiEngine.model.post.brokerregistration.BrokerRegistration;
import apiEngine.model.post.wallbox.status.Installed;
import apiEngine.model.put.registration.Registration;
import io.restassured.response.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

//import org.testng.annotations.Test;

import common.GetCookie;
import common.GetFileInputStream;
import cucumber.TestContext;
import dataproviders.PropertyFileReader;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pageobjects.api.API_PutRegistration;

public class Endpoints {
	static final Logger logger = LogManager.getLogger(Endpoints.class);
	
	
	
    static TestContext testContext;
	PropertyFileReader propertyFileReader;
	private static GetFileInputStream getFileInputStream = new GetFileInputStream();
	static Response result;
	static String statusCode;
	RequestSpecification request;
	private static String COOKIE;
	static URL url;
	static String pattern = "yyyy-MM-dd";
	static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	static String dateToday = simpleDateFormat.format(new Date());

	public static String getBaseUrl() throws IOException {
		Properties properties = new Properties();
		//InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String BaseUrl = properties.getProperty("base_url");	
		
		return BaseUrl;
	}

	public static String getDealer() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		//InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String dealer = properties.getProperty("dealer");	
		
		return dealer;
	}

	public static String getDealerWB() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		//InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String dealer = properties.getProperty("dealer_wb");

		return dealer;
	}
	
	public static String getLeasingCompany() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		//InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String LeasingCompany = properties.getProperty("leasing_company");	
		
		return LeasingCompany;
	}

	public static String getBroker() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		//InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String Broker = properties.getProperty("broker");

		return Broker;
	}

	public static String getLeasingCompanyLux() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		//InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String LeasingCompany = properties.getProperty("leasing_company_lux");

		return LeasingCompany;
	}

	public static String getLeasingCompanyWb() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		//InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String LeasingCompany = properties.getProperty("leasing_company_wb");

		return LeasingCompany;
	}

	public static String getUserLanguage() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = Endpoints.class.getClassLoader().getResourceAsStream("properties.properties");
		//InputStream inputDriver = new FileInputStream("src/test/resources/properties.properties");
		properties.load(inputDriver);
		String UserLanguage = properties.getProperty("language");	
		
		return UserLanguage;
	}

	public static Response postDigitalDeliveryConfirmation(String orderId) throws FileNotFoundException, IOException {
		String orderIdTmp = orderId.replace("[", "");
		orderId = orderIdTmp.replace("]", "");
		RestAssured.baseURI = getBaseUrl();
		//GetCookie getCookie = new GetCookie();
		//COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		String lang = System.getProperty("pdfLanguage");
		String jsonBody = "{\n" +
				"  \"pickupDriver\": {\n" +
				"    \"firstName\": \"Adje\",\n" +
				"    \"lastName\": \"Antonisse\",\n" +
				"    \"birthDate\": \"1965-06-19T08:15:34.678Z\",\n" +
				"    \"email\": \"a.antonisse@rentasolutions.org\",\n" +
				"    \"language\": " + "\"" + lang + "\" ,\n" +
				"    \"mainDriver\": true,\n" +
				"    \"sub\": \"sub1\"\n" +
				"  },\n" +
				"  \"previousVehicle\": {\n" +
				"    \"leasingCompanyRsNumber\": 799189,\n" +
				"    \"licensePlate\": \"1FFF333\"\n" +
				"  }\n" +
				"}";

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				//.header("cookie", "sso=" + COOKIE)
				//.header("x-wss-rs-user-number", getLeasingCompany())
				.header("Authorization", "Basic ZGFyOllUSEhLWnh0cyUkMFd1NTR3NE90WHIydw==")
				//.auth().basic("dar", "YTHHKZxts%$0Wu54w4OtXr2w")
				//.auth().basic("ZGFyOllUSEhLWnh0cyUkMFd1NTR3NE90WHIydw==")
				//.pathParams("id", "4ba3e80d-1fbe-406d-b309-ceb5af872c13")
				.pathParams("id", orderId)
				.body(jsonBody)
				.log().all();
		//.filter(new AllureRestAssured());

		Response response = request.post(Route.PostDigitalDeliveryConfirmation());
		result = response;

		//Response response = request.post("/test/cookie");
		logger.debug("response: " + response.asPrettyString());
		statusCode = String.valueOf(response.getStatusLine());
		String res = response.asPrettyString();

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return result;

	}


	public static Response getOrderByDosNr(String DosNr) throws FileNotFoundException, IOException {
    	RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			COOKIE = getCookie.getCookie(getLeasingCompanyLux()).asString();
		} else if(System.getProperty("wallbox").equals("Yes")) {
			COOKIE = getCookie.getCookie(getLeasingCompanyWb()).asString();
		}
		else {
			COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		}

		String cookieTemp = "eyJhbGciOiJIUzI1NiJ9.eyJSdW1Vc2VyIjoie1wicnVtR3VpZFwiOlwiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzXCIsXCJlbWFpbFwiOlwicnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmdcIixcImZ1bGxOYW1lXCI6XCJSUyBUZXN0IDJcIixcImluaXRpYWxzXCI6XCJSVFwiLFwiZmFsbEJhY2tIYXBwZW5lZFwiOmZhbHNlLFwiYXBwc1wiOltcIkZNU1wiLFwiTVJUXCIsXCJPUkQyXCIsXCJSRUlcIl19Iiwic3ViIjoiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzIiwiaWF0IjoxNjQ5MjM2MjEyLCJleHAiOjE2NDkyNjUwMTJ9.z52nfS60ae0S9pLrrgv55L3OFqWB2mgiqeymWUyJQr8";

		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			RequestSpecification request = RestAssured.given();
			request.header("Content-Type", "application/json")
					.header("cookie", "sso=" + COOKIE)
					.header("x-wss-rs-user-number", getLeasingCompanyLux())
					//.pathParams("searchTerm",DosNr)
					//.queryParam("dealerActionNeeded", false)
					//.queryParam("dealerHasUnseenDocuments", false)
					//.queryParam("dealerHasUnseenMessages", false)
					//.queryParam("beingProcessed", false)
					//.queryParam("orderStatus", "NEW")
					//.queryParam("orderStatus", "MODIFIED")
					//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
					//.queryParam("orderStatus", "ACCEPTED")
					//.queryParam("orderStatus", "ORDERED")
					//.queryParam("orderStatus", "VEHICLE_IN")
					//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
					//.queryParam("orderStatus", "DELIVERED")
					.queryParam("searchTerm",DosNr)
					.log().all()
					.filter(new AllureRestAssured());

			Response response = request.get(Route.OrderByDosNr());
			result = response;
		} else if(System.getProperty("wallbox").equals("Yes")) {
			RequestSpecification request = RestAssured.given();
			request.header("Content-Type", "application/json")
					.header("cookie", "sso=" + COOKIE)
					.header("x-wss-rs-user-number", getLeasingCompanyWb())
					//.pathParams("searchTerm",DosNr)
					//.queryParam("dealerActionNeeded", false)
					//.queryParam("dealerHasUnseenDocuments", false)
					//.queryParam("dealerHasUnseenMessages", false)
					//.queryParam("beingProcessed", false)
					//.queryParam("orderStatus", "NEW")
					//.queryParam("orderStatus", "MODIFIED")
					//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
					//.queryParam("orderStatus", "ACCEPTED")
					//.queryParam("orderStatus", "ORDERED")
					//.queryParam("orderStatus", "VEHICLE_IN")
					//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
					//.queryParam("orderStatus", "DELIVERED")
					.queryParam("searchTerm",DosNr)
					.log().all()
					.filter(new AllureRestAssured());

			Response response = request.get(Route.OrderByDosNr());
			result = response;
		}
		else {
			RequestSpecification request = RestAssured.given();
			request.header("Content-Type", "application/json")
					.header("cookie", "sso=" + COOKIE)
					.header("x-wss-rs-user-number", getLeasingCompany())
					//.pathParams("searchTerm",DosNr)
					//.queryParam("dealerActionNeeded", false)
					//.queryParam("dealerHasUnseenDocuments", false)
					//.queryParam("dealerHasUnseenMessages", false)
					//.queryParam("beingProcessed", false)
					//.queryParam("orderStatus", "NEW")
					//.queryParam("orderStatus", "MODIFIED")
					//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
					//.queryParam("orderStatus", "ACCEPTED")
					//.queryParam("orderStatus", "ORDERED")
					//.queryParam("orderStatus", "VEHICLE_IN")
					//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
					//.queryParam("orderStatus", "DELIVERED")
					.queryParam("searchTerm",DosNr)
					.log().all()
					.filter(new AllureRestAssured());

			Response response = request.get(Route.OrderByDosNr());
			result = response;
		}


        //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
        //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
        //getFileInputStream.closeInputStream(inputStream);

        return result;
        
	}

	public static Response getWallBoxOrderByDosNr(String DosNr) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealerWB()).asString();
		String cookieTemp = "eyJhbGciOiJIUzI1NiJ9.eyJSdW1Vc2VyIjoie1wicnVtR3VpZFwiOlwiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzXCIsXCJlbWFpbFwiOlwicnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmdcIixcImZ1bGxOYW1lXCI6XCJSUyBUZXN0IDJcIixcImluaXRpYWxzXCI6XCJSVFwiLFwiZmFsbEJhY2tIYXBwZW5lZFwiOmZhbHNlLFwiYXBwc1wiOltcIkZNU1wiLFwiTVJUXCIsXCJPUkQyXCIsXCJSRUlcIl19Iiwic3ViIjoiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzIiwiaWF0IjoxNjQ5MjQxMzAwLCJleHAiOjE2NDkyNzAxMDB9.7FKqLMjRATuryLSCYjjMD03TtUV49Mu1KDDjjH9J1Nk";


		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getDealerWB())
				//.pathParams("searchTerm",DosNr)
				//.queryParam("dealerActionNeeded", false)
				//.queryParam("dealerHasUnseenDocuments", false)
				//.queryParam("dealerHasUnseenMessages", false)
				//.queryParam("beingProcessed", false)
				//.queryParam("orderStatus", "NEW")
				//.queryParam("orderStatus", "MODIFIED")
				//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
				//.queryParam("orderStatus", "ACCEPTED")
				//.queryParam("orderStatus", "ORDERED")
				//.queryParam("orderStatus", "VEHICLE_IN")
				//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
				//.queryParam("orderStatus", "DELIVERED")
				.queryParam("searchTerm",DosNr)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.get(Route.WallBoxOrderByDosNr());
		result = response;

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return result;

	}

	public static Response getOrderDetailById(String Id) throws FileNotFoundException, IOException {
    	RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealer()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
		.header("cookie", " _ga=GA1.2.14778490.1642679085; renta-language=en; _gid=GA1.2.125415147.1649067690;sso=" + COOKIE)
		.header("x-wss-rs-user-number", getDealer())

		.pathParam("id", Id)
		.log().all()
		.filter(new AllureRestAssured());

        Response response = request.get(Route.OrderDetailById());
        request.log().all();
        result = response;

        //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
        //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
        //getFileInputStream.closeInputStream(inputStream);

        return result;
        
	}	
	
	public static Response getOrderNotifications(String LastId, String leasingType) throws FileNotFoundException, IOException {
    	RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		String rs_user;
		leasingType = "OPERATIONAL_LEASE";
		if(leasingType != null && !leasingType.isEmpty()) {
			if (leasingType.equals("WALLBOX")) {
				COOKIE = getCookie.getCookie(getDealerWB()).asString();
				rs_user = getDealerWB();
			} else {
				COOKIE = getCookie.getCookie(getDealer()).asString();
				rs_user = getDealer();
			}

			String cookieTemp = "eyJhbGciOiJIUzI1NiJ9.eyJSdW1Vc2VyIjoie1wicnVtR3VpZFwiOlwiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzXCIsXCJlbWFpbFwiOlwicnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmdcIixcImZ1bGxOYW1lXCI6XCJSUyBUZXN0IDJcIixcImluaXRpYWxzXCI6XCJSVFwiLFwiZmFsbEJhY2tIYXBwZW5lZFwiOmZhbHNlLFwiYXBwc1wiOltcIkZNU1wiLFwiTVJUXCIsXCJPUkQyXCIsXCJSRUlcIl19Iiwic3ViIjoiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzIiwiaWF0IjoxNjQ5MjM2MjEyLCJleHAiOjE2NDkyNjUwMTJ9.z52nfS60ae0S9pLrrgv55L3OFqWB2mgiqeymWUyJQr8";


			RequestSpecification request = RestAssured.given();
			request.header("Content-Type", "application/json")
					.header("cookie", "sso=" + COOKIE)
					//.header("cookie", " _ga=GA1.2.14778490.1642679085; renta-language=en; _gid=GA1.2.125415147.1649067690; sso=" + COOKIE)
					.header("x-wss-rs-user-number", rs_user)
					.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)")
					.queryParam("lastId", LastId)
					//.queryParam("limit", 100)
					.log().all()
					.filter(new AllureRestAssured());

			Response response = request.get(Route.OrderNotifications());
			request.log().all();
			result = response;

			//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
			//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
			//getFileInputStream.closeInputStream(inputStream);
		}
        return result;
        
	}
	
	public static Response getOrderTimelineById(String Id) throws FileNotFoundException, IOException {
    	RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealer()).asString();
		
		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
		.header("cookie", "sso=" + COOKIE)
		.header("x-wss-rs-user-number", getDealer())
		.pathParam("id", Id)
		.log().all()
		.filter(new AllureRestAssured());

        Response response = request.get(Route.OrderTimelineById());
        request.log().all();
        result = response;

        //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
        //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
        //getFileInputStream.closeInputStream(inputStream);

        return result;
        
	}

	public static Response getWallBoxOrderTimelineById(String Id) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealerWB()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getDealerWB())
				.pathParam("id", Id)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.get(Route.WallBoxOrderTimelineById());
		request.log().all();
		result = response;

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return result;

	}

	public static Response getLeasingCompanyActorById(String rsNumber) throws FileNotFoundException, IOException {
    	RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealer()).asString();
		
		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
		.header("cookie", "sso=" + COOKIE)
		.header("x-wss-rs-user-number", getLeasingCompany())
		.pathParam("rsNumber", rsNumber)
		.log().all()
		.filter(new AllureRestAssured());

        Response response = request.get(Route.LeasingCompanyActorById());
        request.log().all();
        result = response;

        //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
        //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
        //getFileInputStream.closeInputStream(inputStream);

        return result;
        
	}	
	
	public static Response getSupplierActorById(String rsNumberDealer) throws FileNotFoundException, IOException {
    	RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealer()).asString();
		
		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
		.header("cookie", "sso=" + COOKIE)
		.header("x-wss-rs-user-number", getDealer())
		.pathParam("rsNumber", rsNumberDealer)
		.log().all()
		.filter(new AllureRestAssured());

        Response response = request.get(Route.SupplierActorById());
		logger.debug("Response van getSupplierActorById: " + response.asPrettyString());
        request.log().all();
        result = response;

        //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
        //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
        //getFileInputStream.closeInputStream(inputStream);

        return result;
        
	}	
	
	public static Response postOrderVehicleIn(String orderId) throws FileNotFoundException, IOException, URISyntaxException {
	    RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealer()).asString();

		RequestSpecification request = RestAssured.given();
		
		String boundary = Long.toHexString(System.currentTimeMillis());
		
		request
		.config(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false).encodeContentTypeAs("multipart/form-data", ContentType.JSON)))
		.contentType("multipart/form-data; boundary="+boundary)
		.header("cookie", "sso=" + COOKIE)
		.header("x-wss-rs-user-number", getDealer())
		.pathParams("id", orderId)
		.multiPart("data", "{\r\n"
				+ "  \"vinNumber\":\"00000000071100083\", \"vinControlNumber\" : \"441\"\r\n"
				+ "}", "application/json")
		//.multiPart("registration", new File("./src/test/resources/Registration.jpeg"))
		.multiPart("registration", new File("./src/test/resources/runtime/Registration.jpeg"))
		.multiPart("coc", new File("./src/test/resources/runtime/COC.png"))
		//new File("./src/test/resources/" + document + ""),"text/csv
		//.body()
		.log().all()
		.filter(new AllureRestAssured());
		
	    Response response = request.post(Route.OrderVehicleIn());
	    result = response;
	    
	    //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-definition-allowed-location-resource/PostActivityLocationSchema.json");
	    //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
	    //getFileInputStream.closeInputStream(inputStream);

	    return result;
	        
	}
	
	public static Response postOrderDeliveredLux(String orderId) throws FileNotFoundException, IOException {
	    RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealer()).asString();

		RequestSpecification request = RestAssured.given();
		
		String boundary = Long.toHexString(System.currentTimeMillis());
		
		request
		.config(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false).encodeContentTypeAs("multipart/form-data", ContentType.JSON)))
		.contentType("multipart/form-data; boundary="+boundary)
		.header("cookie", "sso=" + COOKIE)
		.header("x-wss-rs-user-number", getDealer())
		.pathParams("id", orderId)
		.multiPart("data", "{\r\n"
				+ "  \"vinNumber\":\"00000000071100083\", \"vinControlNumber\" : \"441\"\r\n"
				+ "}", "application/json")
		.multiPart("registration", new File("./src/test/resources/runtime/Registration.jpeg"))
		.multiPart("coc", new File("./src/test/resources/runtime/COC.png"))
		//new File("./src/test/resources/" + document + ""),"text/csv
		//.body()
		.log().all()
		.filter(new AllureRestAssured());
		
	    Response response = request.post(Route.OrderVehicleIn());
	    result = response;
	    
	    //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-definition-allowed-location-resource/PostActivityLocationSchema.json");
	    //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
	    //getFileInputStream.closeInputStream(inputStream);

	    return result;
	        
	}
	
	public static Response postOrderDocuments(String orderId) throws FileNotFoundException, IOException {
	    RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealer()).asString();

		RequestSpecification request = RestAssured.given();
		
		String boundary = Long.toHexString(System.currentTimeMillis());
		
		request
		.config(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false).encodeContentTypeAs("multipart/form-data", ContentType.JSON)))
		.contentType("multipart/form-data; boundary="+boundary)
		.header("cookie", "sso=" + COOKIE)
		.header("x-wss-rs-user-number", getDealer())
		.pathParams("id", orderId)
		.multiPart("data", "{\"documentType\":\"REGDOC\",\"comment\":\"-> vandaag2222\"}", "application/json")
		.multiPart("document", new File("./src/test/resources/runtime/Registration.jpeg"))
		.multiPart("data", "{\"documentType\":\"EID\",\"comment\":\"-> vandaag3333\"}", "application/json")
		.multiPart("document", new File("./src/test/resources/runtime/rijbewijs.jpeg"))
		//new File("./src/test/resources/" + document + ""),"text/csv
		//.body()
		.log().all()
		.filter(new AllureRestAssured());
		
	    Response response = request.post(Route.OrderDocument());
	    result = response;
	    
	    //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-definition-allowed-location-resource/PostActivityLocationSchema.json");
	    //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
	    //getFileInputStream.closeInputStream(inputStream);

	    return result;
	        
	}

	public static String postWallBoxOrderAccepted(String orderId, String body) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealerWB()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getDealerWB())
				.pathParams("id", orderId)
				.body(body)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.post(Route.WallBoxOrderAccepted());
		statusCode = String.valueOf(response.getStatusLine());

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return statusCode;

	}

	public static String postWallBoxOrderOrdered(String orderId) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompanyWb())
				.pathParams("id", orderId)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.post(Route.WallBoxOrderOrdered());
		statusCode = String.valueOf(response.getStatusLine());

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return statusCode;

	}

	public static String postWallBoxOrderInstalled(String orderId, Installed installed) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealerWB()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getDealerWB())
				.pathParams("id", orderId)
				.body(installed)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.post(Route.WallBoxOrderInstalled());
		statusCode = String.valueOf(response.getStatusLine());

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return statusCode;

	}

	public static String postWallBoxOrderReadyToActivate(String orderId) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				.pathParams("id", orderId)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.post(Route.WallBoxOrderReadyToActivate());
		statusCode = String.valueOf(response.getStatusLine());

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return statusCode;

	}

	public static String postWallBoxOrderActivated(String orderId, String body) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getDealerWB()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getDealerWB())
				.pathParams("id", orderId)
				.body(body)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.post(Route.WallBoxOrderActivated());
		statusCode = String.valueOf(response.getStatusLine());

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return statusCode;

	}

	public static Response getRegistration(String InsuranceReferenceNumber) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		//String cookieTemp = "eyJhbGciOiJIUzI1NiJ9.eyJSdW1Vc2VyIjoie1wicnVtR3VpZFwiOlwiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzXCIsXCJlbWFpbFwiOlwicnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmdcIixcImZ1bGxOYW1lXCI6XCJSUyBUZXN0IDJcIixcImluaXRpYWxzXCI6XCJSVFwiLFwiZmFsbEJhY2tIYXBwZW5lZFwiOmZhbHNlLFwiYXBwc1wiOltcIkZNU1wiLFwiTVJUXCIsXCJPUkQyXCIsXCJSRUlcIl19Iiwic3ViIjoiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzIiwiaWF0IjoxNjQ5MjQxMzAwLCJleHAiOjE2NDkyNzAxMDB9.7FKqLMjRATuryLSCYjjMD03TtUV49Mu1KDDjjH9J1Nk";


		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				//.pathParams("searchTerm",DosNr)
				//.queryParam("dealerActionNeeded", false)
				//.queryParam("dealerHasUnseenDocuments", false)
				//.queryParam("dealerHasUnseenMessages", false)
				//.queryParam("beingProcessed", false)
				//.queryParam("orderStatus", "NEW")
				//.queryParam("orderStatus", "MODIFIED")
				//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
				//.queryParam("orderStatus", "ACCEPTED")
				//.queryParam("orderStatus", "ORDERED")
				//.queryParam("orderStatus", "VEHICLE_IN")
				//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
				//.queryParam("orderStatus", "DELIVERED")
				.queryParam("searchTerm",InsuranceReferenceNumber)
				.queryParam("registrationStatus", "DRAFT", "PLANNED", "TO_VALIDATE_BY_INSURANCE_BROKER", "REGISTERED",
						"LC_ARCHIVED", "LC_ARCHIVED", "BROKER_ARCHIVED", "ARCHIVED", "CANCELED", "FAILED", "REFUSED")
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.get(Route.Registration());
		result = response;

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return result;

	}

	public static Response getRegistrationById(String Id) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		//String cookieTemp = "eyJhbGciOiJIUzI1NiJ9.eyJSdW1Vc2VyIjoie1wicnVtR3VpZFwiOlwiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzXCIsXCJlbWFpbFwiOlwicnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmdcIixcImZ1bGxOYW1lXCI6XCJSUyBUZXN0IDJcIixcImluaXRpYWxzXCI6XCJSVFwiLFwiZmFsbEJhY2tIYXBwZW5lZFwiOmZhbHNlLFwiYXBwc1wiOltcIkZNU1wiLFwiTVJUXCIsXCJPUkQyXCIsXCJSRUlcIl19Iiwic3ViIjoiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzIiwiaWF0IjoxNjQ5MjQxMzAwLCJleHAiOjE2NDkyNzAxMDB9.7FKqLMjRATuryLSCYjjMD03TtUV49Mu1KDDjjH9J1Nk";


		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				//.pathParams("searchTerm",DosNr)
				//.queryParam("dealerActionNeeded", false)
				//.queryParam("dealerHasUnseenDocuments", false)
				//.queryParam("dealerHasUnseenMessages", false)
				//.queryParam("beingProcessed", false)
				//.queryParam("orderStatus", "NEW")
				//.queryParam("orderStatus", "MODIFIED")
				//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
				//.queryParam("orderStatus", "ACCEPTED")
				//.queryParam("orderStatus", "ORDERED")
				//.queryParam("orderStatus", "VEHICLE_IN")
				//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
				//.queryParam("orderStatus", "DELIVERED")
				.pathParam("id", Id)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.get(Route.RegistrationById());

		result = response;

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return result;

	}

	public static Response putRegistration(Registration registration) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				//.pathParams("id", orderId)
				.body(registration)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.put(Route.PutRegistration());
		//ResponseBody rb = response.getBody().prettyPeek();
		//logger.debug("HET ANTWOORD??? " + rb.prettyPeek().jsonPath().get("error"));

		//statusCode = String.valueOf(response.getStatusLine());
		//Boolean b = statusCode.contains("200");
		//logger.debug("statusCode Line: " + b);

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return response;

	}

	public static Response postRegistrationComplete(String registrationId) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				.pathParams("id", registrationId)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.post(Route.RegistrationComplete());
		statusCode = String.valueOf(response.getStatusLine());

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return response;

	}

	public static Response postBrokerRegistration(BrokerRegistration brokerRegistration) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getBroker()).asString();

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getBroker())
				//.pathParams("id", orderId)
				.body(brokerRegistration)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.post(Route.PostBrokerRegistration());
		ResponseBody rb = response.getBody().prettyPeek();
		logger.debug("HET ANTWOORD??? " + rb.prettyPeek().jsonPath().get("error"));

		//statusCode = String.valueOf(response.getStatusLine());
		//Boolean b = statusCode.contains("200");
		//logger.debug("statusCode Line: " + b);

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return response;

	}

	public static Response getBrokerRegistration(String InsuranceReferenceNumber) throws FileNotFoundException, IOException {
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		String dosNr = properties.getProperty("DossierNumber");
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		//COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		//String cookieTemp = "eyJhbGciOiJIUzI1NiJ9.eyJSdW1Vc2VyIjoie1wicnVtR3VpZFwiOlwiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzXCIsXCJlbWFpbFwiOlwicnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmdcIixcImZ1bGxOYW1lXCI6XCJSUyBUZXN0IDJcIixcImluaXRpYWxzXCI6XCJSVFwiLFwiZmFsbEJhY2tIYXBwZW5lZFwiOmZhbHNlLFwiYXBwc1wiOltcIkZNU1wiLFwiTVJUXCIsXCJPUkQyXCIsXCJSRUlcIl19Iiwic3ViIjoiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzIiwiaWF0IjoxNjQ5MjQxMzAwLCJleHAiOjE2NDkyNzAxMDB9.7FKqLMjRATuryLSCYjjMD03TtUV49Mu1KDDjjH9J1Nk";


		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				//.pathParams("searchTerm",DosNr)
				//.queryParam("dealerActionNeeded", false)
				//.queryParam("dealerHasUnseenDocuments", false)
				//.queryParam("dealerHasUnseenMessages", false)
				//.queryParam("beingProcessed", false)
				//.queryParam("orderStatus", "NEW")
				//.queryParam("orderStatus", "MODIFIED")
				//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
				//.queryParam("orderStatus", "ACCEPTED")
				//.queryParam("orderStatus", "ORDERED")
				//.queryParam("orderStatus", "VEHICLE_IN")
				//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
				//.queryParam("orderStatus", "DELIVERED")
				.queryParam("searchTerm",InsuranceReferenceNumber)
				//.queryParam("searchTerm",dosNr)
				//.queryParam("registrationStatus", "DRAFT", "PLANNED", "TO_VALIDATE_BY_INSURANCE_BROKER", "REGISTERED",
				//		"LC_ARCHIVED", "LC_ARCHIVED", "BROKER_ARCHIVED", "ARCHIVED", "CANCELED", "FAILED", "REFUSED")
				.queryParam("registrationStatus", "REGISTERED")
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.get(Route.BrokerRegistration());
		result = response;

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return result;

	}

	public static Response getBrokerRegistrationById(String Id) throws FileNotFoundException, IOException {
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		//String cookieTemp = "eyJhbGciOiJIUzI1NiJ9.eyJSdW1Vc2VyIjoie1wicnVtR3VpZFwiOlwiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzXCIsXCJlbWFpbFwiOlwicnN0ZXN0MkByZW50YXNvbHV0aW9ucy5vcmdcIixcImZ1bGxOYW1lXCI6XCJSUyBUZXN0IDJcIixcImluaXRpYWxzXCI6XCJSVFwiLFwiZmFsbEJhY2tIYXBwZW5lZFwiOmZhbHNlLFwiYXBwc1wiOltcIkZNU1wiLFwiTVJUXCIsXCJPUkQyXCIsXCJSRUlcIl19Iiwic3ViIjoiQTVFQkQ4NDgtOTYyNi1FQzExLUI2RTUtNjA0NUJEOEMxOTgzIiwiaWF0IjoxNjQ5MjQxMzAwLCJleHAiOjE2NDkyNzAxMDB9.7FKqLMjRATuryLSCYjjMD03TtUV49Mu1KDDjjH9J1Nk";


		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				//.pathParams("searchTerm",DosNr)
				//.queryParam("dealerActionNeeded", false)
				//.queryParam("dealerHasUnseenDocuments", false)
				//.queryParam("dealerHasUnseenMessages", false)
				//.queryParam("beingProcessed", false)
				//.queryParam("orderStatus", "NEW")
				//.queryParam("orderStatus", "MODIFIED")
				//.queryParam("orderStatus", "REQUEST_TO_CANCEL")
				//.queryParam("orderStatus", "ACCEPTED")
				//.queryParam("orderStatus", "ORDERED")
				//.queryParam("orderStatus", "VEHICLE_IN")
				//.queryParam("orderStatus", "LICENSE_PLATE_ORDERED")
				//.queryParam("orderStatus", "DELIVERED")
				.pathParam("id", Id)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.get(Route.BrokerRegistrationById());

		result = response;

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/work-order-resource/GetWorkOrderByIdSchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return result;

	}

	public static Response putBrokerRegistrationApproved(String registrationId, String jsonBody) throws FileNotFoundException, IOException {
		logger.debug("response put registration registrationId: " + registrationId);
		RestAssured.baseURI = getBaseUrl();
		GetCookie getCookie = new GetCookie();
		COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
		//String jsonBody = "{\n" +
		//		"  \"vatNumber\": {\n" +
		//		"    \"value\": \"BE0842936235\"\n" +
		//		"  }\n" +
		//		"}";

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json")
				.header("cookie", "sso=" + COOKIE)
				.header("x-wss-rs-user-number", getLeasingCompany())
				.pathParams("id", registrationId)
				.body(jsonBody)
				.log().all()
				.filter(new AllureRestAssured());

		Response response = request.put(Route.PutBrokerRegistrationApproved());
		logger.debug("response put registration: " + response.asPrettyString());
		ResponseBody rb = response.getBody().prettyPeek();
		logger.debug("HET ANTWOORD??? " + rb.prettyPeek().jsonPath().get("error"));

		//statusCode = String.valueOf(response.getStatusLine());
		//Boolean b = statusCode.contains("200");
		//logger.debug("statusCode Line: " + b);

		//InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
		//response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
		//getFileInputStream.closeInputStream(inputStream);

		return response;

	}

}
