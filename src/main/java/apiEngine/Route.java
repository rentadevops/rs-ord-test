package apiEngine;

public class Route {
	
	public static String sso() {
		//return "https://api.acc.rentasolutions.org/sso-cookie";
		return "https://api.tst.rentasolutions.org/sso-cookie";

	}
	
	public static String OrderByDosNr() {return "/ord/webapi/order";}

	public static String WallBoxOrderByDosNr() {return "/ord/webapi/wall-box-order";}
	
	public static String OrderDetailById() {
		return "/ord/webapi/order/{id}";
	}
	
	public static String OrderTimelineById() {
		return "/ord/webapi/order/{id}/timeline";
	}

	public static String WallBoxOrderTimelineById() {
		return "/ord/webapi/wall-box-order/{id}/timeline";
	}
	
	public static String LeasingCompanyActorById() {
		return "/ord/webapi/actor/leasingcompany/{rsNumber}";
	}
	
	public static String SupplierActorById() {
		return "/ord/webapi/actor/supplier/{rsNumber}";
	}
	
	public static String OrderVehicleIn() {
		return "/ord/webapi/order/{id}/vehicle-in";
	}

	public static String WallBoxOrderAccepted() {
		return "/ord/webapi/wall-box-order/{id}/accepted";
	}

	public static String WallBoxOrderOrdered() {
		return "/ord/webapi/wall-box-order/{id}/ordered";
	}

	public static String WallBoxOrderInstalled() {
		return "/ord/webapi/wall-box-order/{id}/installed";
	}

	public static String WallBoxOrderActivated() {
		return "/ord/webapi/wall-box-order/{id}/activated";
	}

	public static String WallBoxOrderReadyToActivate() {
		return "/ord/webapi/wall-box-order/{id}/ready-to-activate";
	}
	
	public static String OrderDocument() {
		return "/ord/webapi/order/{id}/document";
	}
	
	public static String OrderNotifications() {
		return "/ord/feed/order/notifications";
	}

	public static String Registration() {
		return "/ord/webapi/registration";
	}

	public static String BrokerRegistration() {
		return "/ord/webapi/broker/registration";
	}

	public static String RegistrationById() {
		return "/ord/webapi/registration/{id}";
	}

	public static String BrokerRegistrationById() {
		return "/ord/webapi/broker/registration/{id}";
	}

	public static String PutRegistration() {
		return "/ord/webapi/registration";
	}

	public static String PutBrokerRegistrationApproved() {
		return "/ord/webapi/broker/registration/{id}/approve";
		//return "/ord/webapi/broker/registration/{id}";
	}

	public static String PostBrokerRegistration() {
		return "/ord/webapi/broker/registration";
	}

	public static String RegistrationComplete() {
		return "/ord/webapi/registration/{id}/complete";
	}

	public static String PostDigitalDeliveryConfirmation() {
		return "/ord/webapi/order/{id}/digital-delivery-confirmation";
	}
	
}
