package apiEngine.model.database;

public class Product {

    private int version;
    private String name;
    private String reference_code;
    private int leasing_company_id;
    private java.sql.Blob logo_file;
    private java.sql.Blob terms_and_conditions_file;
    private String logo_name;
    private java.sql.Blob mobile_logo_file;
    private String mobile_logo_name;

    public int getVersion(){
        return version;
    }

    public void setVersion(int version){
        this.version=version;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getReference_code(){
        return reference_code;
    }

    public void setReference_code(String reference_code){
        this.reference_code=reference_code;
    }

    public int getLeasing_company_id(){
        return leasing_company_id;
    }

    public void setLeasing_company_id(int leasing_company_id){
        this.leasing_company_id=leasing_company_id;
    }

    public java.sql.Blob getLogo_file(){
        return logo_file;
    }

    public void setLogo_file(java.sql.Blob logo_file){
        this.logo_file=logo_file;
    }

    public java.sql.Blob getTerms_and_conditions_file(){
        return terms_and_conditions_file;
    }

    public void setTerms_and_conditions_file(java.sql.Blob terms_and_conditions_file){
        this.terms_and_conditions_file=terms_and_conditions_file;
    }

    public String getLogo_name(){
        return logo_name;
    }

    public void setLogo_name(String logo_name){
        this.logo_name=logo_name;
    }

    public java.sql.Blob getMobile_logo_file(){
        return mobile_logo_file;
    }

    public void setMobile_logo_file(java.sql.Blob mobile_logo_file){
        this.mobile_logo_file=mobile_logo_file;
    }

    public String getMobile_logo_name(){
        return mobile_logo_name;
    }

    public void setMobile_logo_name(String mobile_logo_name){
        this.mobile_logo_name=mobile_logo_name;
    }

    @Override
    public String toString() {
        return "Product{" + "\n" +
                "version=" + version + "\n" +
                "name=" + name + "\n" +
                "reference_code=" + reference_code + "\n" +
                "leasing_company_id=" + leasing_company_id + "\n" +
                "logo_file=" + logo_file + "\n" +
                "terms_and_conditions_file=" + terms_and_conditions_file + "\n" +
                "logo_name=" + logo_name + "\n" +
                "mobile_logo_file=" + mobile_logo_file + "\n" +
                "mobile_logo_name=" + mobile_logo_name + "\n" +
                '}';
    }
}
