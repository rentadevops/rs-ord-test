package apiEngine.model.database;

public class Leasing_company{
private int version;
private String commercial_name;
private int number;
private String phone_number;
private String fax_number;
private String vat_number;
private String invoice_name;
private String address_city;
private String address_country;
private String address_house_number;
private String address_street;
private String address_zip_code;
private String email;
private String contact_person;
private int parent_leasing_company_id;
private String registration_number;
private String ceo_name;
private java.sql.Blob ceo_signature;
private String leasing_company_delivery_first_Name;
private String leasing_company_delivery_last_name_or_company_name;
private String leasing_company_delivery_street;
private String leasing_company_delivery_house_number;
private String leasing_company_delivery_box_number;
private String leasing_company_delivery_postal_code;
private String leasing_company_delivery_city;
private String b_post_delivery_first_name;
private String b_post_delivery_last_name_or_company_name;
private String b_post_delivery_street;
private String b_post_delivery_house_number;
private String b_post_delivery_box_number;
private String b_post_delivery_postal_code;
private String b_post_delivery_city;
private String language_official_communication;
private String green_card_email_default;
private int gdpr_threshold_in_days;
private String application_version;
private String mobile_number;
private int delivery_follow_up_days;
private int delivery_follow_up_days_fallback;

    @Override
    public String toString() {
        return "Leasing_company{" +
                "version=" + version +
                "commercial_name=" + commercial_name + "\n" +
                "number=" + number +
                "phone_number=" + phone_number + "\n" +
                "fax_number=" + fax_number + "\n" +
                "vat_number=" + vat_number + "\n" +
                "invoice_name=" + invoice_name + "\n" +
                "address_city=" + address_city + "\n" +
                "address_country=" + address_country + "\n" +
                "address_house_number=" + address_house_number + "\n" +
                "address_street=" + address_street + "\n" +
                "address_zip_code=" + address_zip_code + "\n" +
                "email=" + email + "\n" +
                "contact_person=" + contact_person + "\n" +
                "parent_leasing_company_id=" + parent_leasing_company_id +
                "registration_number=" + registration_number + "\n" +
                "ceo_name=" + ceo_name + "\n" +
                "ceo_signature=" + ceo_signature +
                "leasing_company_delivery_first_Name=" + leasing_company_delivery_first_Name + "\n" +
                "leasing_company_delivery_last_name_or_company_name=" + leasing_company_delivery_last_name_or_company_name + "\n" +
                "leasing_company_delivery_street=" + leasing_company_delivery_street + "\n" +
                "leasing_company_delivery_house_number=" + leasing_company_delivery_house_number + "\n" +
                "leasing_company_delivery_box_number=" + leasing_company_delivery_box_number + "\n" +
                "leasing_company_delivery_postal_code=" + leasing_company_delivery_postal_code + "\n" +
                "leasing_company_delivery_city=" + leasing_company_delivery_city + "\n" +
                "b_post_delivery_first_name=" + b_post_delivery_first_name + "\n" +
                "b_post_delivery_last_name_or_company_name=" + b_post_delivery_last_name_or_company_name + "\n" +
                "b_post_delivery_street=" + b_post_delivery_street + "\n" +
                "b_post_delivery_house_number=" + b_post_delivery_house_number + "\n" +
                "b_post_delivery_box_number=" + b_post_delivery_box_number + "\n" +
                "b_post_delivery_postal_code=" + b_post_delivery_postal_code + "\n" +
                "b_post_delivery_city=" + b_post_delivery_city + "\n" +
                "language_official_communication=" + language_official_communication + "\n" +
                "green_card_email_default=" + green_card_email_default + "\n" +
                "gdpr_threshold_in_days=" + gdpr_threshold_in_days +
                "application_version=" + application_version + "\n" +
                "mobile_number=" + mobile_number + "\n" +
                "delivery_follow_up_days=" + delivery_follow_up_days +
                "delivery_follow_up_days_fallback=" + delivery_follow_up_days_fallback +
                '}';
    }

    public Leasing_company() {

        }

    public int getVersion(){
        return version;
    }

    public void setVersion(int version){
        this.version=version;
    }

    public String getCommercial_name(){
        return commercial_name;
    }

    public void setCommercial_name(String commercial_name){
        this.commercial_name=commercial_name;
    }

    public int getNumber(){
        return number;
    }

    public void setNumber(int number){
        this.number=number;
    }

    public String getPhone_number(){
        return phone_number;
    }

    public void setPhone_number(String phone_number){
        this.phone_number=phone_number;
    }

    public String getFax_number(){
        return fax_number;
    }

    public void setFax_number(String fax_number){
        this.fax_number=fax_number;
    }

    public String getVat_number(){
        return vat_number;
    }

    public void setVat_number(String vat_number){
        this.vat_number=vat_number;
    }

    public String getInvoice_name(){
        return invoice_name;
    }

    public void setInvoice_name(String invoice_name){
        this.invoice_name=invoice_name;
    }

    public String getAddress_city(){
        return address_city;
    }

    public void setAddress_city(String address_city){
        this.address_city=address_city;
    }

    public String getAddress_country(){
        return address_country;
    }

    public void setAddress_country(String address_country){
        this.address_country=address_country;
    }

    public String getAddress_house_number(){
        return address_house_number;
    }

    public void setAddress_house_number(String address_house_number){
        this.address_house_number=address_house_number;
    }

    public String getAddress_street(){
        return address_street;
    }

    public void setAddress_street(String address_street){
        this.address_street=address_street;
    }

    public String getAddress_zip_code(){
        return address_zip_code;
    }

    public void setAddress_zip_code(String address_zip_code){
        this.address_zip_code=address_zip_code;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email=email;
    }

    public String getContact_person(){
        return contact_person;
    }

    public void setContact_person(String contact_person){
        this.contact_person=contact_person;
    }

    public int getParent_leasing_company_id(){
        return parent_leasing_company_id;
    }

    public void setParent_leasing_company_id(int parent_leasing_company_id){
        this.parent_leasing_company_id=parent_leasing_company_id;
    }

    public String getRegistration_number(){
        return registration_number;
    }

    public void setRegistration_number(String registration_number){
        this.registration_number=registration_number;
    }

    public String getCeo_name(){
        return ceo_name;
    }

    public void setCeo_name(String ceo_name){
        this.ceo_name=ceo_name;
    }

    public java.sql.Blob getCeo_signature(){
        return ceo_signature;
    }

    public void setCeo_signature(java.sql.Blob ceo_signature){
        this.ceo_signature=ceo_signature;
    }

    public String getLeasing_company_delivery_first_name(){
        return leasing_company_delivery_first_Name;
    }

    public void setLeasing_company_delivery_first_name(String leasing_company_delivery_first_Name){
        this.leasing_company_delivery_first_Name=leasing_company_delivery_first_Name;
    }

    public String getLeasing_company_delivery_last_name_or_company_name(){
        return leasing_company_delivery_last_name_or_company_name;
    }

    public void setLeasing_company_delivery_last_name_or_company_name(String leasing_company_delivery_last_name_or_company_name){
        this.leasing_company_delivery_last_name_or_company_name=leasing_company_delivery_last_name_or_company_name;
    }

    public String getLeasing_company_delivery_street(){
        return leasing_company_delivery_street;
    }

    public void setLeasing_company_delivery_street(String leasing_company_delivery_street){
        this.leasing_company_delivery_street=leasing_company_delivery_street;
    }

    public String getLeasing_company_delivery_house_number(){
        return leasing_company_delivery_house_number;
    }

    public void setLeasing_company_delivery_house_number(String leasing_company_delivery_house_number){
        this.leasing_company_delivery_house_number=leasing_company_delivery_house_number;
    }

    public String getLeasing_company_delivery_box_number(){
        return leasing_company_delivery_box_number;
    }

    public void setLeasing_company_delivery_box_number(String leasing_company_delivery_box_number){
        this.leasing_company_delivery_box_number=leasing_company_delivery_box_number;
    }

    public String getLeasing_company_delivery_postal_code(){
        return leasing_company_delivery_postal_code;
    }

    public void setLeasing_company_delivery_postal_code(String leasing_company_delivery_postal_code){
        this.leasing_company_delivery_postal_code=leasing_company_delivery_postal_code;
    }

    public String getLeasing_company_delivery_city(){
        return leasing_company_delivery_city;
    }

    public void setLeasing_company_delivery_city(String leasing_company_delivery_city){
        this.leasing_company_delivery_city=leasing_company_delivery_city;
    }

    public String getB_post_delivery_first_name(){
        return b_post_delivery_first_name;
    }

    public void setB_post_delivery_first_name(String b_post_delivery_first_name){
        this.b_post_delivery_first_name=b_post_delivery_first_name;
    }

    public String getB_post_delivery_last_name_or_company_name(){
        return b_post_delivery_last_name_or_company_name;
    }

    public void setB_post_delivery_last_name_or_company_name(String b_post_delivery_last_name_or_company_name){
        this.b_post_delivery_last_name_or_company_name=b_post_delivery_last_name_or_company_name;
    }

    public String getB_post_delivery_street(){
        return b_post_delivery_street;
    }

    public void setB_post_delivery_street(String b_post_delivery_street){
        this.b_post_delivery_street=b_post_delivery_street;
    }

    public String getB_post_delivery_house_number(){
        return b_post_delivery_house_number;
    }

    public void setB_post_delivery_house_number(String b_post_delivery_house_number){
        this.b_post_delivery_house_number=b_post_delivery_house_number;
    }

    public String getB_post_delivery_box_number(){
        return b_post_delivery_box_number;
    }

    public void setB_post_delivery_box_number(String b_post_delivery_box_number){
        this.b_post_delivery_box_number=b_post_delivery_box_number;
    }

    public String getB_post_delivery_postal_code(){
        return b_post_delivery_postal_code;
    }

    public void setB_post_delivery_postal_code(String b_post_delivery_postal_code){
        this.b_post_delivery_postal_code=b_post_delivery_postal_code;
    }

    public String getB_post_delivery_city(){
        return b_post_delivery_city;
    }

    public void setB_post_delivery_city(String b_post_delivery_city){
        this.b_post_delivery_city=b_post_delivery_city;
    }

    public String getLanguage_official_communication(){
        return language_official_communication;
    }

    public void setLanguage_official_communication(String language_official_communication){
        this.language_official_communication=language_official_communication;
    }

    public String getGreen_card_email_default(){
        return green_card_email_default;
    }

    public void setGreen_card_email_default(String green_card_email_default){
        this.green_card_email_default=green_card_email_default;
    }

    public int getGdpr_threshold_in_days(){
        return gdpr_threshold_in_days;
    }

    public void setGdpr_threshold_in_days(int gdpr_threshold_in_days){
        this.gdpr_threshold_in_days=gdpr_threshold_in_days;
    }

    public String getApplication_version(){
        return application_version;
    }

    public void setApplication_version(String application_version){
        this.application_version=application_version;
    }

    public String getMobile_number(){
        return mobile_number;
    }

    public void setMobile_number(String mobile_number){
        this.mobile_number=mobile_number;
    }

    public int getDelivery_follow_up_days(){
        return delivery_follow_up_days;
    }

    public void setDelivery_follow_up_days(int delivery_follow_up_days){
        this.delivery_follow_up_days=delivery_follow_up_days;
    }

    public int getDelivery_follow_up_days_fallback(){
        return delivery_follow_up_days_fallback;
    }

    public void setDelivery_follow_up_days_fallback(int delivery_follow_up_days_fallback){
        this.delivery_follow_up_days_fallback=delivery_follow_up_days_fallback;
    }
        }
