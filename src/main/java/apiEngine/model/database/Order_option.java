package apiEngine.model.database;

import java.util.Arrays;

public class Order_option {
    private byte[] order_id;
    private int version;
    private double option_amount_fleet_discount;
    private String option_codes;
    private String option_description_dutch;
    private double option_list_price;
    private double option_percentage_fleet_discount;
    private String option_supplier_name;
    private String option_supplier_number;
    private String option_type;
    private String option_description_french;
    private String option_name_dutch;
    private String option_name_french;
    private String option_name_english;
    private String option_description_english;
    private double applicable_vat_percentage;

    public byte[] getorder_id()
    {
        return this.order_id;
    }
    public void setorder_id(byte[] value)
    {
        this.order_id = value;
    }

    public int getVersion(){
        return version;
    }

    public void setVersion(int version){
        this.version=version;
    }

    public double getOption_amount_fleet_discount(){
        return option_amount_fleet_discount;
    }

    public void setOption_amount_fleet_discount(double option_amount_fleet_discount){
        this.option_amount_fleet_discount=option_amount_fleet_discount;
    }

    public String getOption_codes(){
        return option_codes;
    }

    public void setOption_codes(String option_codes){
        this.option_codes=option_codes;
    }

    public String getOption_description_dutch(){
        return option_description_dutch;
    }

    public void setOption_description_dutch(String option_description_dutch){
        this.option_description_dutch=option_description_dutch;
    }

    public double getOption_list_price(){
        return option_list_price;
    }

    public void setOption_list_price(double option_list_price){
        this.option_list_price=option_list_price;
    }

    public double getOption_percentage_fleet_discount(){
        return option_percentage_fleet_discount;
    }

    public void setOption_percentage_fleet_discount(double option_percentage_fleet_discount){
        this.option_percentage_fleet_discount=option_percentage_fleet_discount;
    }

    public String getOption_supplier_name(){
        return option_supplier_name;
    }

    public void setOption_supplier_name(String option_supplier_name){
        this.option_supplier_name=option_supplier_name;
    }

    public String getOption_supplier_number(){
        return option_supplier_number;
    }

    public void setOption_supplier_number(String option_supplier_number){
        this.option_supplier_number=option_supplier_number;
    }

    public String getOption_type(){
        return option_type;
    }

    public void setOption_type(String option_type){
        this.option_type=option_type;
    }

    public String getOption_description_french(){
        return option_description_french;
    }

    public void setOption_description_french(String option_description_french){
        this.option_description_french=option_description_french;
    }

    public String getOption_name_dutch(){
        return option_name_dutch;
    }

    public void setOption_name_dutch(String option_name_dutch){
        this.option_name_dutch=option_name_dutch;
    }

    public String getOption_name_french(){
        return option_name_french;
    }

    public void setOption_name_french(String option_name_french){
        this.option_name_french=option_name_french;
    }

    public String getOption_name_english(){
        return option_name_english;
    }

    public void setOption_name_english(String option_name_english){
        this.option_name_english=option_name_english;
    }

    public String getOption_description_english(){
        return option_description_english;
    }

    public void setOption_description_english(String option_description_english){
        this.option_description_english=option_description_english;
    }

    public double getApplicable_vat_percentage(){
        return applicable_vat_percentage;
    }

    public void setApplicable_vat_percentage(double applicable_vat_percentage){
        this.applicable_vat_percentage=applicable_vat_percentage;
    }

    @Override
    public String toString() {
        return "Order_option{" + "\n" +
                "order_id=" + Arrays.toString(order_id) + "\n" +
                "version=" + version + "\n" +
                "option_amount_fleet_discount=" + option_amount_fleet_discount + "\n" +
                "option_codes=" + option_codes + "\n" +
                "option_description_dutch=" + option_description_dutch + "\n" +
                "option_list_price=" + option_list_price + "\n" +
                "option_percentage_fleet_discount=" + option_percentage_fleet_discount + "\n" +
                "option_supplier_name=" + option_supplier_name + "\n" +
                "option_supplier_number=" + option_supplier_number + "\n" +
                "option_type=" + option_type + "\n" +
                "option_description_french=" + option_description_french + "\n" +
                "option_name_dutch=" + option_name_dutch + "\n" +
                "option_name_french=" + option_name_french + "\n" +
                "option_name_english=" + option_name_english + "\n" +
                "option_description_english=" + option_description_english + "\n" +
                "applicable_vat_percentage=" + applicable_vat_percentage +
                '}';
    }
}
