package apiEngine.model.database;

public class Order_promotion {

    private byte[] order_id;
    private double promotion_amount;
    private double promotion_percentage;
    private String promotion_text_dutch;
    private int version;
    private String promotion_text_french;
    private String promotion_name_dutch;
    private String promotion_name_french;
    private String promotion_description_dutch;
    private String promotion_description_french;
    private String promotion_name_english;
    private String promotion_description_english;

    public byte[] order_id(){
        return order_id;
    }

    public void setOrder_promotion(byte[] order_id){
        this.order_id=order_id;
    }

    public double getPromotion_amount(){
        return promotion_amount;
    }

    public void setPromotion_amount(double promotion_amount){
        this.promotion_amount=promotion_amount;
    }

    public double getPromotion_percentage(){
        return promotion_percentage;
    }

    public void setPromotion_percentage(double promotion_percentage){
        this.promotion_percentage=promotion_percentage;
    }

    public String getPromotion_text_dutch(){
        return promotion_text_dutch;
    }

    public void setPromotion_text_dutch(String promotion_text_dutch){
        this.promotion_text_dutch=promotion_text_dutch;
    }

    public int getVersion(){
        return version;
    }

    public void setVersion(int version){
        this.version=version;
    }

    public String getPromotion_text_french(){
        return promotion_text_french;
    }

    public void setPromotion_text_french(String promotion_text_french){
        this.promotion_text_french=promotion_text_french;
    }

    public String getPromotion_name_dutch(){
        return promotion_name_dutch;
    }

    public void setPromotion_name_dutch(String promotion_name_dutch){
        this.promotion_name_dutch=promotion_name_dutch;
    }

    public String getPromotion_name_french(){
        return promotion_name_french;
    }

    public void setPromotion_name_french(String promotion_name_french){
        this.promotion_name_french=promotion_name_french;
    }

    public String getPromotion_description_dutch(){
        return promotion_description_dutch;
    }

    public void setPromotion_description_dutch(String promotion_description_dutch){
        this.promotion_description_dutch=promotion_description_dutch;
    }

    public String getPromotion_description_french(){
        return promotion_description_french;
    }

    public void setPromotion_description_french(String promotion_description_french){
        this.promotion_description_french=promotion_description_french;
    }

    public String getPromotion_name_english(){
        return promotion_name_english;
    }

    public void setPromotion_name_english(String promotion_name_english){
        this.promotion_name_english=promotion_name_english;
    }

    public String getPromotion_description_english(){
        return promotion_description_english;
    }

    public void setPromotion_description_english(String promotion_description_english){
        this.promotion_description_english=promotion_description_english;
    }

    @Override
    public String toString() {
        return "Order_promotion{" + "\n" +
                "promotion_amount=" + promotion_amount + "\n" +
                "promotion_percentage=" + promotion_percentage + "\n" +
                "promotion_text_dutch=" + promotion_text_dutch + "\n" +
                "version=" + version + "\n" +
                "promotion_text_french=" + promotion_text_french + "\n" +
                "promotion_name_dutch=" + promotion_name_dutch + "\n" +
                "promotion_name_french=" + promotion_name_french + "\n" +
                "promotion_description_dutch=" + promotion_description_dutch + "\n" +
                "promotion_description_french=" + promotion_description_french + "\n" +
                "promotion_name_english=" + promotion_name_english + "\n" +
                "promotion_description_english=" + promotion_description_english + "\n" +
                '}';
    }
}
