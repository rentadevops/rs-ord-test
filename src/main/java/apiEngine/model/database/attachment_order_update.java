package apiEngine.model.database;

public class attachment_order_update {
    private long attachment_order_id;
    public long getattachment_order_id()
    {
        return this.attachment_order_id;
    }
    public void setattachment_order_id(long value)
    {
        this.attachment_order_id = value;
    }

    private int version;
    public int getversion()
    {
        return this.version;
    }
    public void setversion(int value)
    {
        this.version = value;
    }

    private String file_name;
    public String getfile_name()
    {
        return this.file_name;
    }
    public void setfile_name(String value)
    {
        this.file_name = value;
    }

    private byte[] order_id;
    public byte[] getorder_id()
    {
        return this.order_id;
    }
    public void setorder_id(byte[] value)
    {
        this.order_id = value;
    }

    private byte[] file_id;
    public byte[] getfile_id()
    {
        return this.file_id;
    }
    public void setfile_id(byte[] value)
    {
        this.file_id = value;
    }

    private java.sql.Date upload_date;
    public java.sql.Date getupload_date()
    {
        return this.upload_date;
    }
    public void setupload_date(java.sql.Date value)
    {
        this.upload_date = value;
    }

    private String mime_type;
    public String getmime_type()
    {
        return this.mime_type;
    }
    public void setmime_type(String value)
    {
        this.mime_type = value;
    }

    private String document_type;
    public String getdocument_type()
    {
        return this.document_type;
    }
    public void setdocument_type(String value)
    {
        this.document_type = value;
    }


    public attachment_order_update(long attachment_order_id_,int version_,String file_name_,byte[] order_id_,byte[] file_id_,java.sql.Date upload_date_,String mime_type_,String document_type_)
    {
        this.attachment_order_id = attachment_order_id_;
        this.version = version_;
        this.file_name = file_name_;
        this.order_id = order_id_;
        this.file_id = file_id_;
        this.upload_date = upload_date_;
        this.mime_type = mime_type_;
        this.document_type = document_type_;
    }
}
