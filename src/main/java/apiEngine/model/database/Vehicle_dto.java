package apiEngine.model.database;

public class Vehicle_dto {

    private int authorized_mass_max;
    private String brand;
    private double co2_emissions_nedc;
    private int cylinder_content;
    private String exterior_colour_dutch;
    private String exterior_colour_english;
    private String exterior_colour_french;
    private String fuel_label;
    private String identification;
    private String interior_colour_dutch;
    private String interior_colour_english;
    private String interior_colour_french;
    private String kind_label;
    private String license_plate;
    private int mass_on_coupling;
    private String model_dutch;
    private String model_english;
    private String model_french;
    private int number_of_seats;
    private int power;
    private String stock_vehicle;
    private String tyre_brand;
    private String tyre_description;
    private String version_dutch;
    private String version_english;
    private String version_french;
    private String vin_control_number;
    private String vin;
    private int weight;
    private String frame_size;
    private String frame_size_unit;
    private String non_vin_identification_number;
    private String frame_number;
    private String frame_type;
    private String segment_dutch;
    private String segment_english;
    private String segment_french;
    private int construction_year;
    private String gear;
    private String battery_power;
    private String battery_power_unit;
    private double co2_emissions_wltp;
    private String form_number;

    public int getAuthorized_mass_max(){
        return authorized_mass_max;
    }

    public void setAuthorized_mass_max(int authorized_mass_max){
        this.authorized_mass_max=authorized_mass_max;
    }

    public String getBrand(){
        return brand;
    }

    public void setBrand(String brand){
        this.brand=brand;
    }

    public double getCo2_emissions_nedc(){
        return co2_emissions_nedc;
    }

    public void setCo2_emissions_nedc(double co2_emissions_nedc){
        this.co2_emissions_nedc=co2_emissions_nedc;
    }

    public int getCylinder_content(){
        return cylinder_content;
    }

    public void setCylinder_content(int cylinder_content){
        this.cylinder_content=cylinder_content;
    }

    public String getExterior_colour_dutch(){
        return exterior_colour_dutch;
    }

    public void setExterior_colour_dutch(String exterior_colour_dutch){
        this.exterior_colour_dutch=exterior_colour_dutch;
    }

    public String getExterior_colour_english(){
        return exterior_colour_english;
    }

    public void setExterior_colour_english(String exterior_colour_english){
        this.exterior_colour_english=exterior_colour_english;
    }

    public String getExterior_colour_french(){
        return exterior_colour_french;
    }

    public void setExterior_colour_french(String exterior_colour_french){
        this.exterior_colour_french=exterior_colour_french;
    }

    public String getFuel_label(){
        return fuel_label;
    }

    public void setFuel_label(String fuel_label){
        this.fuel_label=fuel_label;
    }

    public String getIdentification(){
        return identification;
    }

    public void setIdentification(String identification){
        this.identification=identification;
    }

    public String getInterior_colour_dutch(){
        return interior_colour_dutch;
    }

    public void setInterior_colour_dutch(String interior_colour_dutch){
        this.interior_colour_dutch=interior_colour_dutch;
    }

    public String getInterior_colour_english(){
        return interior_colour_english;
    }

    public void setInterior_colour_english(String interior_colour_english){
        this.interior_colour_english=interior_colour_english;
    }

    public String getInterior_colour_french(){
        return interior_colour_french;
    }

    public void setInterior_colour_french(String interior_colour_french){
        this.interior_colour_french=interior_colour_french;
    }

    public String getKind_label(){
        return kind_label;
    }

    public void setKind_label(String kind_label){
        this.kind_label=kind_label;
    }

    public String getLicense_plate(){
        return license_plate;
    }

    public void setLicense_plate(String license_plate){
        this.license_plate=license_plate;
    }

    public int getMass_on_coupling(){
        return mass_on_coupling;
    }

    public void setMass_on_coupling(int mass_on_coupling){
        this.mass_on_coupling=mass_on_coupling;
    }

    public String getModel_dutch(){
        return model_dutch;
    }

    public void setModel_dutch(String model_dutch){
        this.model_dutch=model_dutch;
    }

    public String getModel_english(){
        return model_english;
    }

    public void setModel_english(String model_english){
        this.model_english=model_english;
    }

    public String getModel_french(){
        return model_french;
    }

    public void setModel_french(String model_french){
        this.model_french=model_french;
    }

    public int getNumber_of_seats(){
        return number_of_seats;
    }

    public void setNumber_of_seats(int number_of_seats){
        this.number_of_seats=number_of_seats;
    }

    public int getPower(){
        return power;
    }

    public void setPower(int power){
        this.power=power;
    }

    public String getStock_vehicle(){
        return stock_vehicle;
    }

    public void setStock_vehicle(String stock_vehicle){
        this.stock_vehicle=stock_vehicle;
    }

    public String getTyre_brand(){
        return tyre_brand;
    }

    public void setTyre_brand(String tyre_brand){
        this.tyre_brand=tyre_brand;
    }

    public String getTyre_description(){
        return tyre_description;
    }

    public void setTyre_description(String tyre_description){
        this.tyre_description=tyre_description;
    }

    public String getVersion_dutch(){
        return version_dutch;
    }

    public void setVersion_dutch(String version_dutch){
        this.version_dutch=version_dutch;
    }

    public String getVersion_english(){
        return version_english;
    }

    public void setVersion_english(String version_english){
        this.version_english=version_english;
    }

    public String getVersion_french(){
        return version_french;
    }

    public void setVersion_french(String version_french){
        this.version_french=version_french;
    }

    public String getVin_control_number(){
        return vin_control_number;
    }

    public void setVin_control_number(String vin_control_number){
        this.vin_control_number=vin_control_number;
    }

    public String getVin(){
        return vin;
    }

    public void setVin(String vin){
        this.vin=vin;
    }

    public int getWeight(){
        return weight;
    }

    public void setWeight(int weight){
        this.weight=weight;
    }

    public String getFrame_size(){
        return frame_size;
    }

    public void setFrame_size(String frame_size){
        this.frame_size=frame_size;
    }

    public String getFrame_size_unit(){
        return frame_size_unit;
    }

    public void setFrame_size_unit(String frame_size_unit){
        this.frame_size_unit=frame_size_unit;
    }

    public String getNon_vin_identification_number(){
        return non_vin_identification_number;
    }

    public void setNon_vin_identification_number(String non_vin_identification_number){
        this.non_vin_identification_number=non_vin_identification_number;
    }

    public String getFrame_number(){
        return frame_number;
    }

    public void setFrame_number(String frame_number){
        this.frame_number=frame_number;
    }

    public String getFrame_type(){
        return frame_type;
    }

    public void setFrame_type(String frame_type){
        this.frame_type=frame_type;
    }

    public String getSegment_dutch(){
        return segment_dutch;
    }

    public void setSegment_dutch(String segment_dutch){
        this.segment_dutch=segment_dutch;
    }

    public String getSegment_english(){
        return segment_english;
    }

    public void setSegment_english(String segment_english){
        this.segment_english=segment_english;
    }

    public String getSegment_french(){
        return segment_french;
    }

    public void setSegment_french(String segment_french){
        this.segment_french=segment_french;
    }

    public int getConstruction_year(){
        return construction_year;
    }

    public void setConstruction_year(int construction_year){
        this.construction_year=construction_year;
    }

    public String getGear(){
        return gear;
    }

    public void setGear(String gear){
        this.gear=gear;
    }

    public String getBattery_power(){
        return battery_power;
    }

    public void setBattery_power(String battery_power){
        this.battery_power=battery_power;
    }

    public String getBattery_power_unit(){
        return battery_power_unit;
    }

    public void setBattery_power_unit(String battery_power_unit){
        this.battery_power_unit=battery_power_unit;
    }

    public double getCo2_emissions_wltp(){
        return co2_emissions_wltp;
    }

    public void setCo2_emissions_wltp(double co2_emissions_wltp){
        this.co2_emissions_wltp=co2_emissions_wltp;
    }

    public String getForm_number(){
        return form_number;
    }

    public void setForm_number(String form_number){
        this.form_number=form_number;
    }

    @Override
    public String toString() {
        return "Vehicle_dto{" +
                "authorized_mass_max=" + authorized_mass_max + "\n" +
                "brand=" + brand + "\n" +
                "co2_emissions_nedc=" + co2_emissions_nedc + "\n" +
                "cylinder_content=" + cylinder_content + "\n" +
                "exterior_colour_dutch=" + exterior_colour_dutch + "\n" +
                "exterior_colour_english=" + exterior_colour_english + "\n" +
                "exterior_colour_french=" + exterior_colour_french + "\n" +
                "fuel_label=" + fuel_label + "\n" +
                "identification=" + identification + "\n" +
                "interior_colour_dutch=" + interior_colour_dutch + "\n" +
                "interior_colour_english=" + interior_colour_english + "\n" +
                "interior_colour_french=" + interior_colour_french + "\n" +
                "kind_label=" + kind_label + "\n" +
                "license_plate=" + license_plate + "\n" +
                "mass_on_coupling=" + mass_on_coupling + "\n" +
                "model_dutch=" + model_dutch + "\n" +
                "model_english=" + model_english + "\n" +
                "model_french=" + model_french + "\n" +
                "number_of_seats=" + number_of_seats + "\n" +
                "power=" + power + "\n" +
                "stock_vehicle=" + stock_vehicle + "\n" +
                "tyre_brand=" + tyre_brand + "\n" +
                "tyre_description=" + tyre_description + "\n" +
                "version_dutch=" + version_dutch + "\n" +
                "version_english=" + version_english + "\n" +
                "version_french=" + version_french + "\n" +
                "vin_control_number=" + vin_control_number + "\n" +
                "vin=" + vin + "\n" +
                "weight=" + weight + "\n" +
                "frame_size=" + frame_size + "\n" +
                "frame_size_unit=" + frame_size_unit + "\n" +
                "non_vin_identification_number=" + non_vin_identification_number + "\n" +
                "frame_number=" + frame_number + "\n" +
                "frame_type=" + frame_type + "\n" +
                "segment_dutch=" + segment_dutch + "\n" +
                "segment_english=" + segment_english + "\n" +
                "segment_french=" + segment_french + "\n" +
                "construction_year=" + construction_year +
                "gear=" + gear + "\n" +
                "battery_power=" + battery_power + "\n" +
                "battery_power_unit=" + battery_power_unit + "\n" +
                "co2_emissions_wltp=" + co2_emissions_wltp +
                "form_number=" + form_number + "\n" +
                '}';
    }
}
