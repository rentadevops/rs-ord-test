package apiEngine.model.database;

public class adminordersummarydto {
    private byte[] order_id;
    public byte[] getorder_id()
    {
        return this.order_id;
    }
    public void setorder_id(byte[] value)
    {
        this.order_id = value;
    }

    private int version;
    public int getversion()
    {
        return this.version;
    }
    public void setversion(int value)
    {
        this.version = value;
    }

    private String leasing_company_dossier_number;
    public String getleasing_company_dossier_number()
    {
        return this.leasing_company_dossier_number;
    }
    public void setleasing_company_dossier_number(String value)
    {
        this.leasing_company_dossier_number = value;
    }

    private String dealer_dossier_number;
    public String getdealer_dossier_number()
    {
        return this.dealer_dossier_number;
    }
    public void setdealer_dossier_number(String value)
    {
        this.dealer_dossier_number = value;
    }

    private long leasing_company_number;
    public long getleasing_company_number()
    {
        return this.leasing_company_number;
    }
    public void setleasing_company_number(long value)
    {
        this.leasing_company_number = value;
    }

    private long dealer_number;
    public long getdealer_number()
    {
        return this.dealer_number;
    }
    public void setdealer_number(long value)
    {
        this.dealer_number = value;
    }

    private long agent_number;
    public long getagent_number()
    {
        return this.agent_number;
    }
    public void setagent_number(long value)
    {
        this.agent_number = value;
    }

    private Object order_status;
    public Object getorder_status()
    {
        return this.order_status;
    }
    public void setorder_status(Object value)
    {
        this.order_status = value;
    }

    private java.sql.Date creation_date;
    public java.sql.Date getcreation_date()
    {
        return this.creation_date;
    }
    public void setcreation_date(java.sql.Date value)
    {
        this.creation_date = value;
    }

    private java.sql.Date invoice_date;
    public java.sql.Date getinvoice_date()
    {
        return this.invoice_date;
    }
    public void setinvoice_date(java.sql.Date value)
    {
        this.invoice_date = value;
    }


    public adminordersummarydto(byte[] order_id_,int version_,String leasing_company_dossier_number_,String dealer_dossier_number_,long leasing_company_number_,long dealer_number_,long agent_number_,Object order_status_,java.sql.Date creation_date_,java.sql.Date invoice_date_)
    {
        this.order_id = order_id_;
        this.version = version_;
        this.leasing_company_dossier_number = leasing_company_dossier_number_;
        this.dealer_dossier_number = dealer_dossier_number_;
        this.leasing_company_number = leasing_company_number_;
        this.dealer_number = dealer_number_;
        this.agent_number = agent_number_;
        this.order_status = order_status_;
        this.creation_date = creation_date_;
        this.invoice_date = invoice_date_;
    }
}
