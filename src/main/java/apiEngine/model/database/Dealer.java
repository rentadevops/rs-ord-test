package apiEngine.model.database;

public class Dealer {
    private int version;
    private String name;
    private int number;
    private String contact_person;
    private String email;
    private String phone_number;
    private String fax_number;
    private String mobile_number;
    private String vat_number;
    private String address_city;
    private String address_country;
    private String address_house_number;
    private String address_street;
    private String address_zip_code;
    private String bank_account_bic;
    private String company_registration_number;
    private String bank_account_iban;
    private String invoicing_city;
    private String invoicing_country;
    private String invoicing_house_number;
    private String invoicing_street;
    private String invoicing_zip_code;
    private String invoicing_name;
    private String rpr;
    private int parent_dealer_number;
    private int supplier_brand;
    private String application_version;
    private String dealer_type;

    public Dealer() {

    }

    public int getVersion(){
        return version;
    }

    public void setVersion(int version){
        this.version=version;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public int getNumber(){
        return number;
    }

    public void setNumber(int number){
        this.number=number;
    }

    public String getContact_person(){
        return contact_person;
    }

    public void setContact_person(String contact_person){
        this.contact_person=contact_person;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email=email;
    }

    public String getPhone_number(){
        return phone_number;
    }

    public void setPhone_number(String phone_number){
        this.phone_number=phone_number;
    }

    public String getFax_number(){
        return fax_number;
    }

    public void setFax_number(String fax_number){
        this.fax_number=fax_number;
    }

    public String getMobile_number(){
        return mobile_number;
    }

    public void setMobile_number(String mobile_number){
        this.mobile_number=mobile_number;
    }

    public String getVat_number(){
        return vat_number;
    }

    public void setVat_number(String vat_number){
        this.vat_number=vat_number;
    }

    public String getAddress_city(){
        return address_city;
    }

    public void setAddress_city(String address_city){
        this.address_city=address_city;
    }

    public String getAddress_country(){
        return address_country;
    }

    public void setAddress_country(String address_country){
        this.address_country=address_country;
    }

    public String getAddress_house_number(){
        return address_house_number;
    }

    public void setAddress_house_number(String address_house_number){
        this.address_house_number=address_house_number;
    }

    public String getAddress_street(){
        return address_street;
    }

    public void setAddress_street(String address_street){
        this.address_street=address_street;
    }

    public String getAddress_zip_code(){
        return address_zip_code;
    }

    public void setAddress_zip_code(String address_zip_code){
        this.address_zip_code=address_zip_code;
    }

    public String getBank_account_bic(){
        return bank_account_bic;
    }

    public void setBank_account_bic(String bank_account_bic){
        this.bank_account_bic=bank_account_bic;
    }

    public String getCompany_registration_number(){
        return company_registration_number;
    }

    public void setCompany_registration_number(String company_registration_number){
        this.company_registration_number=company_registration_number;
    }

    public String getBank_account_iban(){
        return bank_account_iban;
    }

    public void setBank_account_iban(String bank_account_iban){
        this.bank_account_iban=bank_account_iban;
    }

    public String getInvoicing_city(){
        return invoicing_city;
    }

    public void setInvoicing_city(String invoicing_city){
        this.invoicing_city=invoicing_city;
    }

    public String getInvoicing_country(){
        return invoicing_country;
    }

    public void setInvoicing_country(String invoicing_country){
        this.invoicing_country=invoicing_country;
    }

    public String getInvoicing_house_number(){
        return invoicing_house_number;
    }

    public void setInvoicing_house_number(String invoicing_house_number){
        this.invoicing_house_number=invoicing_house_number;
    }

    public String getInvoicing_street(){
        return invoicing_street;
    }

    public void setInvoicing_street(String invoicing_street){
        this.invoicing_street=invoicing_street;
    }

    public String getInvoicing_zip_code(){
        return invoicing_zip_code;
    }

    public void setInvoicing_zip_code(String invoicing_zip_code){
        this.invoicing_zip_code=invoicing_zip_code;
    }

    public String getInvoicing_name(){
        return invoicing_name;
    }

    public void setInvoicing_name(String invoicing_name){
        this.invoicing_name=invoicing_name;
    }

    public String getRpr(){
        return rpr;
    }

    public void setRpr(String rpr){
        this.rpr=rpr;
    }

    public int getParent_dealer_number(){
        return parent_dealer_number;
    }

    public void setParent_dealer_number(int parent_dealer_number){
        this.parent_dealer_number=parent_dealer_number;
    }

    public int getSupplier_brand(){
        return supplier_brand;
    }

    public void setSupplier_brand(int supplier_brand){
        this.supplier_brand=supplier_brand;
    }

    public String getApplication_version(){
        return application_version;
    }

    public void setApplication_version(String application_version){
        this.application_version=application_version;
    }

    public String getDealer_type(){
        return dealer_type;
    }

    public void setDealer_type(String dealer_type){
        this.dealer_type=dealer_type;
    }

    @Override
    public String toString() {
        return "Dealer{" +
                "version=" + version +
                "name=" + name + "\n" +
                "number=" + number +
                "contact_person=" + contact_person + "\n" +
                "email=" + email + "\n" +
                "phone_number=" + phone_number + "\n" +
                "fax_number=" + fax_number + "\n" +
                "mobile_number=" + mobile_number + "\n" +
                "vat_number=" + vat_number + "\n" +
                "address_city=" + address_city + "\n" +
                "address_country=" + address_country + "\n" +
                "address_house_number=" + address_house_number + "\n" +
                "address_street=" + address_street + "\n" +
                "address_zip_code=" + address_zip_code + "\n" +
                "bank_account_bic=" + bank_account_bic + "\n" +
                "company_registration_number=" + company_registration_number + "\n" +
                "bank_account_iban=" + bank_account_iban + "\n" +
                "invoicing_city=" + invoicing_city + "\n" +
                "invoicing_country=" + invoicing_country + "\n" +
                "invoicing_house_number=" + invoicing_house_number + "\n" +
                "invoicing_street=" + invoicing_street + "\n" +
                "invoicing_zip_code=" + invoicing_zip_code + "\n" +
                "invoicing_name=" + invoicing_name + "\n" +
                "rpr=" + rpr + "\n" +
                "parent_dealer_number=" + parent_dealer_number +
                "supplier_brand=" + supplier_brand +
                "application_version=" + application_version + "\n" +
                "dealer_type=" + dealer_type + "\n" +
                '}';
    }
}
