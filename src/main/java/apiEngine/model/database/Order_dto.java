package apiEngine.model.database;

import java.math.BigDecimal;
import java.util.Arrays;

public class Order_dto {

    private byte[] order_id;

    public Order_dto() {

    }

    public byte[] getorder_id()
    {
        return this.order_id;
    }
    public void setorder_id(byte[] value)
    {
        this.order_id = value;
    }

    private Object order_status;
    public Object getorder_status()
    {
        return this.order_status;
    }
    public void setorder_status(Object value)
    {
        this.order_status = value;
    }

    private int version;
    public int getversion()
    {
        return this.version;
    }
    public void setversion(int value)
    {
        this.version = value;
    }

    private java.sql.Date expected_delivery_date;
    public java.sql.Date getexpected_delivery_date()
    {
        return this.expected_delivery_date;
    }
    public void setexpected_delivery_date(java.sql.Date value)
    {
        this.expected_delivery_date = value;
    }

    private BigDecimal pricing_fleet_discount_amount;
    public BigDecimal getpricing_fleet_discount_amount()
    {
        return this.pricing_fleet_discount_amount;
    }
    public void setpricing_fleet_discount_amount(BigDecimal value)
    {
        this.pricing_fleet_discount_amount = value;
    }

    private BigDecimal pricing_fleet_discount_percentage;
    public BigDecimal getpricing_fleet_discount_percentage()
    {
        return this.pricing_fleet_discount_percentage;
    }
    public void setpricing_fleet_discount_percentage(BigDecimal value)
    {
        this.pricing_fleet_discount_percentage = value;
    }

    private BigDecimal pricing_list_price;
    public BigDecimal getpricing_list_price()
    {
        return this.pricing_list_price;
    }
    public void setpricing_list_price(BigDecimal value)
    {
        this.pricing_list_price = value;
    }

    private String driver_address;
    public String getdriver_address()
    {
        return this.driver_address;
    }
    public void setdriver_address(String value)
    {
        this.driver_address = value;
    }

    private String driver_city;
    public String getdriver_city()
    {
        return this.driver_city;
    }
    public void setdriver_city(String value)
    {
        this.driver_city = value;
    }

    private String driver_email;
    public String getdriver_email()
    {
        return this.driver_email;
    }
    public void setdriver_email(String value)
    {
        this.driver_email = value;
    }

    private String driver_name;
    public String getdriver_name()
    {
        return this.driver_name;
    }
    public void setdriver_name(String value)
    {
        this.driver_name = value;
    }

    private String driver_phone_number;
    public String getdriver_phone_number()
    {
        return this.driver_phone_number;
    }
    public void setdriver_phone_number(String value)
    {
        this.driver_phone_number = value;
    }

    private String driver_postal_code;
    public String getdriver_postal_code()
    {
        return this.driver_postal_code;
    }
    public void setdriver_postal_code(String value)
    {
        this.driver_postal_code = value;
    }

    private String client_address;
    public String getclient_address()
    {
        return this.client_address;
    }
    public void setclient_address(String value)
    {
        this.client_address = value;
    }

    private String client_city;
    public String getclient_city()
    {
        return this.client_city;
    }
    public void setclient_city(String value)
    {
        this.client_city = value;
    }

    private String client_email;
    public String getclient_email()
    {
        return this.client_email;
    }
    public void setclient_email(String value)
    {
        this.client_email = value;
    }

    private String client_name;
    public String getclient_name()
    {
        return this.client_name;
    }
    public void setclient_name(String value)
    {
        this.client_name = value;
    }

    private String client_phone_number;
    public String getclient_phone_number()
    {
        return this.client_phone_number;
    }
    public void setclient_phone_number(String value)
    {
        this.client_phone_number = value;
    }

    private String client_postal_code;
    public String getclient_postal_code()
    {
        return this.client_postal_code;
    }
    public void setclient_postal_code(String value)
    {
        this.client_postal_code = value;
    }

    private String client_contact_person;
    public String getclient_contact_person()
    {
        return this.client_contact_person;
    }
    public void setclient_contact_person(String value)
    {
        this.client_contact_person = value;
    }

    private String client_fax_number;
    public String getclient_fax_number()
    {
        return this.client_fax_number;
    }
    public void setclient_fax_number(String value)
    {
        this.client_fax_number = value;
    }

    private String client_mobile_number;
    public String getclient_mobile_number()
    {
        return this.client_mobile_number;
    }
    public void setclient_mobile_number(String value)
    {
        this.client_mobile_number = value;
    }

    private Object client_language_code;
    public Object getclient_language_code()
    {
        return this.client_language_code;
    }
    public void setclient_language_code(Object value)
    {
        this.client_language_code = value;
    }

    private String client_vat_number;
    public String getclient_vat_number()
    {
        return this.client_vat_number;
    }
    public void setclient_vat_number(String value)
    {
        this.client_vat_number = value;
    }

    private Object driver_language_code;
    public Object getdriver_language_code()
    {
        return this.driver_language_code;
    }
    public void setdriver_language_code(Object value)
    {
        this.driver_language_code = value;
    }

    private String driver_mobile_number;
    public String getdriver_mobile_number()
    {
        return this.driver_mobile_number;
    }
    public void setdriver_mobile_number(String value)
    {
        this.driver_mobile_number = value;
    }

    private java.sql.Date desired_delivery_date;
    public java.sql.Date getdesired_delivery_date()
    {
        return this.desired_delivery_date;
    }
    public void setdesired_delivery_date(java.sql.Date value)
    {
        this.desired_delivery_date = value;
    }

    private String leasing_company_dossier_number;
    public String getleasing_company_dossier_number()
    {
        return this.leasing_company_dossier_number;
    }
    public void setleasing_company_dossier_number(String value)
    {
        this.leasing_company_dossier_number = value;
    }

    private long dealer_id;
    public long getdealer_id()
    {
        return this.dealer_id;
    }
    public void setdealer_id(long value)
    {
        this.dealer_id = value;
    }

    private long leasing_company_id;
    public long getleasing_company_id()
    {
        return this.leasing_company_id;
    }
    public void setleasing_company_id(long value)
    {
        this.leasing_company_id = value;
    }

    private String version_number;
    public String getversion_number()
    {
        return this.version_number;
    }
    public void setversion_number(String value)
    {
        this.version_number = value;
    }

    private String delivery_address_line;
    public String getdelivery_address_line()
    {
        return this.delivery_address_line;
    }
    public void setdelivery_address_line(String value)
    {
        this.delivery_address_line = value;
    }

    private String delivery_address_postal_code;
    public String getdelivery_address_postal_code()
    {
        return this.delivery_address_postal_code;
    }
    public void setdelivery_address_postal_code(String value)
    {
        this.delivery_address_postal_code = value;
    }

    private String delivery_address_city;
    public String getdelivery_address_city()
    {
        return this.delivery_address_city;
    }
    public void setdelivery_address_city(String value)
    {
        this.delivery_address_city = value;
    }

    private String delivery_contact_email;
    public String getdelivery_contact_email()
    {
        return this.delivery_contact_email;
    }
    public void setdelivery_contact_email(String value)
    {
        this.delivery_contact_email = value;
    }

    private String delivery_contact_person;
    public String getdelivery_contact_person()
    {
        return this.delivery_contact_person;
    }
    public void setdelivery_contact_person(String value)
    {
        this.delivery_contact_person = value;
    }

    private String delivery_phone_number;
    public String getdelivery_phone_number()
    {
        return this.delivery_phone_number;
    }
    public void setdelivery_phone_number(String value)
    {
        this.delivery_phone_number = value;
    }

    private String delivery_mobile_number;
    public String getdelivery_mobile_number()
    {
        return this.delivery_mobile_number;
    }
    public void setdelivery_mobile_number(String value)
    {
        this.delivery_mobile_number = value;
    }

    private String delivery_fax_number;
    public String getdelivery_fax_number()
    {
        return this.delivery_fax_number;
    }
    public void setdelivery_fax_number(String value)
    {
        this.delivery_fax_number = value;
    }

    private Object delivery_location_language;
    public Object getdelivery_location_language()
    {
        return this.delivery_location_language;
    }
    public void setdelivery_location_language(Object value)
    {
        this.delivery_location_language = value;
    }

    private String delivery_location_name;
    public String getdelivery_location_name()
    {
        return this.delivery_location_name;
    }
    public void setdelivery_location_name(String value)
    {
        this.delivery_location_name = value;
    }

    private String delivery_location_vat_number;
    public String getdelivery_location_vat_number()
    {
        return this.delivery_location_vat_number;
    }
    public void setdelivery_location_vat_number(String value)
    {
        this.delivery_location_vat_number = value;
    }

    private String buyback_address_line;
    public String getbuyback_address_line()
    {
        return this.buyback_address_line;
    }
    public void setbuyback_address_line(String value)
    {
        this.buyback_address_line = value;
    }

    private String buyback_address_city;
    public String getbuyback_address_city()
    {
        return this.buyback_address_city;
    }
    public void setbuyback_address_city(String value)
    {
        this.buyback_address_city = value;
    }

    private String buyback_address_postal_code;
    public String getbuyback_address_postal_code()
    {
        return this.buyback_address_postal_code;
    }
    public void setbuyback_address_postal_code(String value)
    {
        this.buyback_address_postal_code = value;
    }

    private BigDecimal buyback_amount;
    public BigDecimal getbuyback_amount()
    {
        return this.buyback_amount;
    }
    public void setbuyback_amount(BigDecimal value)
    {
        this.buyback_amount = value;
    }

    private byte buyback;
    public byte getbuyback()
    {
        return this.buyback;
    }
    public void setbuyback(byte value)
    {
        this.buyback = value;
    }

    private int buyback_contract_duration;
    public int getbuyback_contract_duration()
    {
        return this.buyback_contract_duration;
    }
    public void setbuyback_contract_duration(int value)
    {
        this.buyback_contract_duration = value;
    }

    private int buyback_kilometers;
    public int getbuyback_kilometers()
    {
        return this.buyback_kilometers;
    }
    public void setbuyback_kilometers(int value)
    {
        this.buyback_kilometers = value;
    }

    private String buyback_name;
    public String getbuyback_name()
    {
        return this.buyback_name;
    }
    public void setbuyback_name(String value)
    {
        this.buyback_name = value;
    }

    private java.sql.Date actual_delivery_date;
    public java.sql.Date getactual_delivery_date()
    {
        return this.actual_delivery_date;
    }
    public void setactual_delivery_date(java.sql.Date value)
    {
        this.actual_delivery_date = value;
    }

    private int pickup_construction_year;
    public int getpickup_construction_year()
    {
        return this.pickup_construction_year;
    }
    public void setpickup_construction_year(int value)
    {
        this.pickup_construction_year = value;
    }

    private java.sql.Date pickup_date_first_registration;
    public java.sql.Date getpickup_date_first_registration()
    {
        return this.pickup_date_first_registration;
    }
    public void setpickup_date_first_registration(java.sql.Date value)
    {
        this.pickup_date_first_registration = value;
    }

    private String pickup_key_code;
    public String getpickup_key_code()
    {
        return this.pickup_key_code;
    }
    public void setpickup_key_code(String value)
    {
        this.pickup_key_code = value;
    }

    private int pickup_mileage;
    public int getpickup_mileage()
    {
        return this.pickup_mileage;
    }
    public void setpickup_mileage(int value)
    {
        this.pickup_mileage = value;
    }

    private String pickup_start_code;
    public String getpickup_start_code()
    {
        return this.pickup_start_code;
    }
    public void setpickup_start_code(String value)
    {
        this.pickup_start_code = value;
    }

    private String dealer_dossier_number;
    public String getdealer_dossier_number()
    {
        return this.dealer_dossier_number;
    }
    public void setdealer_dossier_number(String value)
    {
        this.dealer_dossier_number = value;
    }

    private String client_dealer_number;
    public String getclient_dealer_number()
    {
        return this.client_dealer_number;
    }
    public void setclient_dealer_number(String value)
    {
        this.client_dealer_number = value;
    }

    private java.sql.Date invoice_date;
    public java.sql.Date getinvoice_date()
    {
        return this.invoice_date;
    }
    public void setinvoice_date(java.sql.Date value)
    {
        this.invoice_date = value;
    }

    private byte invoiced;
    public byte getinvoiced()
    {
        return this.invoiced;
    }
    public void setinvoiced(byte value)
    {
        this.invoiced = value;
    }

    private int rei_batch_tracking_number;
    public int getrei_batch_tracking_number()
    {
        return this.rei_batch_tracking_number;
    }
    public void setrei_batch_tracking_number(int value)
    {
        this.rei_batch_tracking_number = value;
    }

    private int order_version;
    public int getorder_version()
    {
        return this.order_version;
    }
    public void setorder_version(int value)
    {
        this.order_version = value;
    }

    private String last_comment_comment;
    public String getlast_comment_comment()
    {
        return this.last_comment_comment;
    }
    public void setlast_comment_comment(String value)
    {
        this.last_comment_comment = value;
    }

    private java.sql.Date last_comment_creation_date;
    public java.sql.Date getlast_comment_creation_date()
    {
        return this.last_comment_creation_date;
    }
    public void setlast_comment_creation_date(java.sql.Date value)
    {
        this.last_comment_creation_date = value;
    }

    private String last_comment_order_change;
    public String getlast_comment_order_change()
    {
        return this.last_comment_order_change;
    }
    public void setlast_comment_order_change(String value)
    {
        this.last_comment_order_change = value;
    }

    private java.sql.Date last_comment_updated_expected_delivery_date;
    public java.sql.Date getlast_comment_updated_expected_delivery_date()
    {
        return this.last_comment_updated_expected_delivery_date;
    }
    public void setlast_comment_updated_expected_delivery_date(java.sql.Date value)
    {
        this.last_comment_updated_expected_delivery_date = value;
    }

    private String last_comment_updated_order_status;
    public String getlast_comment_updated_order_status()
    {
        return this.last_comment_updated_order_status;
    }
    public void setlast_comment_updated_order_status(String value)
    {
        this.last_comment_updated_order_status = value;
    }

    private long last_comment_user_id;
    public long getlast_comment_user_id()
    {
        return this.last_comment_user_id;
    }
    public void setlast_comment_user_id(long value)
    {
        this.last_comment_user_id = value;
    }

    private long product_id;
    public long getproduct_id()
    {
        return this.product_id;
    }
    public void setproduct_id(long value)
    {
        this.product_id = value;
    }

    private BigDecimal pricing_total_price;
    public BigDecimal getpricing_total_price()
    {
        return this.pricing_total_price;
    }
    public void setpricing_total_price(BigDecimal value)
    {
        this.pricing_total_price = value;
    }

    private BigDecimal pricing_delivery_costs;
    public BigDecimal getpricing_delivery_costs()
    {
        return this.pricing_delivery_costs;
    }
    public void setpricing_delivery_costs(BigDecimal value)
    {
        this.pricing_delivery_costs = value;
    }

    private BigDecimal pricing_other_costs;
    public BigDecimal getpricing_other_costs()
    {
        return this.pricing_other_costs;
    }
    public void setpricing_other_costs(BigDecimal value)
    {
        this.pricing_other_costs = value;
    }

    private byte pickup_previous_vehicle_dropoff;
    public byte getpickup_previous_vehicle_dropoff()
    {
        return this.pickup_previous_vehicle_dropoff;
    }
    public void setpickup_previous_vehicle_dropoff(byte value)
    {
        this.pickup_previous_vehicle_dropoff = value;
    }

    private String pickup_previous_vehicle_license_plate;
    public String getpickup_previous_vehicle_license_plate()
    {
        return this.pickup_previous_vehicle_license_plate;
    }
    public void setpickup_previous_vehicle_license_plate(String value)
    {
        this.pickup_previous_vehicle_license_plate = value;
    }

    private String pickup_previous_vehicle_owner;
    public String getpickup_previous_vehicle_owner()
    {
        return this.pickup_previous_vehicle_owner;
    }
    public void setpickup_previous_vehicle_owner(String value)
    {
        this.pickup_previous_vehicle_owner = value;
    }

    private byte already_ordered;
    public byte getalready_ordered()
    {
        return this.already_ordered;
    }
    public void setalready_ordered(byte value)
    {
        this.already_ordered = value;
    }

    private byte modified;
    public byte getmodified()
    {
        return this.modified;
    }
    public void setmodified(byte value)
    {
        this.modified = value;
    }

    private long dossier_manager;
    public long getdossier_manager()
    {
        return this.dossier_manager;
    }
    public void setdossier_manager(long value)
    {
        this.dossier_manager = value;
    }

    private long agent_id;
    public long getagent_id()
    {
        return this.agent_id;
    }
    public void setagent_id(long value)
    {
        this.agent_id = value;
    }

    private long last_comment_agent_id;
    public long getlast_comment_agent_id()
    {
        return this.last_comment_agent_id;
    }
    public void setlast_comment_agent_id(long value)
    {
        this.last_comment_agent_id = value;
    }

    private String pickup_driver_first_name;
    public String getpickup_driver_first_name()
    {
        return this.pickup_driver_first_name;
    }
    public void setpickup_driver_first_name(String value)
    {
        this.pickup_driver_first_name = value;
    }

    private String pickup_driver_last_name;
    public String getpickup_driver_last_name()
    {
        return this.pickup_driver_last_name;
    }
    public void setpickup_driver_last_name(String value)
    {
        this.pickup_driver_last_name = value;
    }

    private java.sql.Date pickup_driver_birth_date;
    public java.sql.Date getpickup_driver_birth_date()
    {
        return this.pickup_driver_birth_date;
    }
    public void setpickup_driver_birth_date(java.sql.Date value)
    {
        this.pickup_driver_birth_date = value;
    }

    private String pickup_driver_birth_place;
    public String getpickup_driver_birth_place()
    {
        return this.pickup_driver_birth_place;
    }
    public void setpickup_driver_birth_place(String value)
    {
        this.pickup_driver_birth_place = value;
    }

    private byte pickup_proof_of_registration;
    public byte getpickup_proof_of_registration()
    {
        return this.pickup_proof_of_registration;
    }
    public void setpickup_proof_of_registration(byte value)
    {
        this.pickup_proof_of_registration = value;
    }

    private byte pickup_proof_of_insurance;
    public byte getpickup_proof_of_insurance()
    {
        return this.pickup_proof_of_insurance;
    }
    public void setpickup_proof_of_insurance(byte value)
    {
        this.pickup_proof_of_insurance = value;
    }

    private byte pickup_certificate_of_conformity;
    public byte getpickup_certificate_of_conformity()
    {
        return this.pickup_certificate_of_conformity;
    }
    public void setpickup_certificate_of_conformity(byte value)
    {
        this.pickup_certificate_of_conformity = value;
    }

    private byte pickup_proof_of_roadworthiness;
    public byte getpickup_proof_of_roadworthiness()
    {
        return this.pickup_proof_of_roadworthiness;
    }
    public void setpickup_proof_of_roadworthiness(byte value)
    {
        this.pickup_proof_of_roadworthiness = value;
    }

    private byte pickup_legal_kit;
    public byte getpickup_legal_kit()
    {
        return this.pickup_legal_kit;
    }
    public void setpickup_legal_kit(byte value)
    {
        this.pickup_legal_kit = value;
    }

    private byte pickup_manual;
    public byte getpickup_manual()
    {
        return this.pickup_manual;
    }
    public void setpickup_manual(byte value)
    {
        this.pickup_manual = value;
    }

    private byte pickup_fuel_card;
    public byte getpickup_fuel_card()
    {
        return this.pickup_fuel_card;
    }
    public void setpickup_fuel_card(byte value)
    {
        this.pickup_fuel_card = value;
    }

    private int pickup_number_of_keys;
    public int getpickup_number_of_keys()
    {
        return this.pickup_number_of_keys;
    }
    public void setpickup_number_of_keys(int value)
    {
        this.pickup_number_of_keys = value;
    }

    private long client_registration_number_lessee;
    public long getclient_registration_number_lessee()
    {
        return this.client_registration_number_lessee;
    }
    public void setclient_registration_number_lessee(long value)
    {
        this.client_registration_number_lessee = value;
    }

    private String insurance_company_name;
    public String getinsurance_company_name()
    {
        return this.insurance_company_name;
    }
    public void setinsurance_company_name(String value)
    {
        this.insurance_company_name = value;
    }

    private String insurance_company_email;
    public String getinsurance_company_email()
    {
        return this.insurance_company_email;
    }
    public void setinsurance_company_email(String value)
    {
        this.insurance_company_email = value;
    }

    private BigDecimal insured_value_price;
    public BigDecimal getinsured_value_price()
    {
        return this.insured_value_price;
    }
    public void setinsured_value_price(BigDecimal value)
    {
        this.insured_value_price = value;
    }

    private String tyrefitter_name;
    public String gettyrefitter_name()
    {
        return this.tyrefitter_name;
    }
    public void settyrefitter_name(String value)
    {
        this.tyrefitter_name = value;
    }

    private String tyrefitter_email;
    public String gettyrefitter_email()
    {
        return this.tyrefitter_email;
    }
    public void settyrefitter_email(String value)
    {
        this.tyrefitter_email = value;
    }

    private java.sql.Date proposed_delivery_date;
    public java.sql.Date getproposed_delivery_date()
    {
        return this.proposed_delivery_date;
    }
    public void setproposed_delivery_date(java.sql.Date value)
    {
        this.proposed_delivery_date = value;
    }

    private BigDecimal tax_stamp_costs;
    public BigDecimal gettax_stamp_costs()
    {
        return this.tax_stamp_costs;
    }
    public void settax_stamp_costs(BigDecimal value)
    {
        this.tax_stamp_costs = value;
    }

    private BigDecimal inscription_costs;
    public BigDecimal getinscription_costs()
    {
        return this.inscription_costs;
    }
    public void setinscription_costs(BigDecimal value)
    {
        this.inscription_costs = value;
    }

    private String delivery_contact;
    public String getdelivery_contact()
    {
        return this.delivery_contact;
    }
    public void setdelivery_contact(String value)
    {
        this.delivery_contact = value;
    }

    private byte[] linked_registration_id;
    public byte[] getlinked_registration_id()
    {
        return this.linked_registration_id;
    }
    public void setlinked_registration_id(byte[] value)
    {
        this.linked_registration_id = value;
    }

    private java.sql.Date financial_lease_registration_info_date_last_registration;
    public java.sql.Date getfinancial_lease_registration_info_date_last_registration()
    {
        return this.financial_lease_registration_info_date_last_registration;
    }
    public void setfinancial_lease_registration_info_date_last_registration(java.sql.Date value)
    {
        this.financial_lease_registration_info_date_last_registration = value;
    }

    private Object registration_div_to_do_by;
    public Object getregistration_div_to_do_by()
    {
        return this.registration_div_to_do_by;
    }
    public void setregistration_div_to_do_by(Object value)
    {
        this.registration_div_to_do_by = value;
    }

    private Object leasing_type;
    public Object getleasing_type()
    {
        return this.leasing_type;
    }
    public void setleasing_type(Object value)
    {
        this.leasing_type = value;
    }

    private java.sql.Date busy_until;
    public java.sql.Date getbusy_until()
    {
        return this.busy_until;
    }
    public void setbusy_until(java.sql.Date value)
    {
        this.busy_until = value;
    }

    private java.sql.Date dealer_due_date;
    public java.sql.Date getdealer_due_date()
    {
        return this.dealer_due_date;
    }
    public void setdealer_due_date(java.sql.Date value)
    {
        this.dealer_due_date = value;
    }

    private java.sql.Date leasing_company_due_date;
    public java.sql.Date getleasing_company_due_date()
    {
        return this.leasing_company_due_date;
    }
    public void setleasing_company_due_date(java.sql.Date value)
    {
        this.leasing_company_due_date = value;
    }

    private byte dealer_has_unseen_messages;
    public byte getdealer_has_unseen_messages()
    {
        return this.dealer_has_unseen_messages;
    }
    public void setdealer_has_unseen_messages(byte value)
    {
        this.dealer_has_unseen_messages = value;
    }

    private byte leasing_company_has_unseen_messages;
    public byte getleasing_company_has_unseen_messages()
    {
        return this.leasing_company_has_unseen_messages;
    }
    public void setleasing_company_has_unseen_messages(byte value)
    {
        this.leasing_company_has_unseen_messages = value;
    }

    private byte redactedGDPR;
    public byte getredactedGDPR()
    {
        return this.redactedGDPR;
    }
    public void setredactedGDPR(byte value)
    {
        this.redactedGDPR = value;
    }

    private boolean dealer_has_unseen_documents;
    public boolean getdealer_has_unseen_documents()
    {
        return this.dealer_has_unseen_documents;
    }
    public void setdealer_has_unseen_documents(boolean value)
    {
        this.dealer_has_unseen_documents = value;
    }

    private boolean leasing_company_has_unseen_documents;
    public boolean getleasing_company_has_unseen_documents()
    {
        return this.leasing_company_has_unseen_documents;
    }
    public void setleasing_company_has_unseen_documents(boolean value)
    {
        this.leasing_company_has_unseen_documents = value;
    }

    private long vehicle_dto_id;
    public long getvehicle_dto_id()
    {
        return this.vehicle_dto_id;
    }
    public void setvehicle_dto_id(long value)
    {
        this.vehicle_dto_id = value;
    }

    private String quote_reference;
    public String getquote_reference()
    {
        return this.quote_reference;
    }
    public void setquote_reference(String value)
    {
        this.quote_reference = value;
    }

    private String pickup_address_line;
    public String getpickup_address_line()
    {
        return this.pickup_address_line;
    }
    public void setpickup_address_line(String value)
    {
        this.pickup_address_line = value;
    }

    private String pickup_address_city;
    public String getpickup_address_city()
    {
        return this.pickup_address_city;
    }
    public void setpickup_address_city(String value)
    {
        this.pickup_address_city = value;
    }

    private String pickup_address_postal_code;
    public String getpickup_address_postal_code()
    {
        return this.pickup_address_postal_code;
    }
    public void setpickup_address_postal_code(String value)
    {
        this.pickup_address_postal_code = value;
    }

    private java.sql.Date creation_date;
    public java.sql.Date getcreation_date()
    {
        return this.creation_date;
    }
    public void setcreation_date(java.sql.Date value)
    {
        this.creation_date = value;
    }

    private String digital_delivery_status;
    public String getdigital_delivery_status()
    {
        return this.digital_delivery_status;
    }
    public void setdigital_delivery_status(String value)
    {
        this.digital_delivery_status = value;
    }

    private byte pickup_main_driver;
    public byte getpickup_main_driver()
    {
        return this.pickup_main_driver;
    }
    public void setpickup_main_driver(byte value)
    {
        this.pickup_main_driver = value;
    }

    private java.sql.Date to_archive_date;
    public java.sql.Date getto_archive_date()
    {
        return this.to_archive_date;
    }
    public void setto_archive_date(java.sql.Date value)
    {
        this.to_archive_date = value;
    }


    @Override
    public String toString() {
        return "order_dto{" +
                "order_id='" + order_id + "\n" +
                "order_status=" + order_status + "\n" +
                "version=" + version + "\n" +
                "expected_delivery_date=" + expected_delivery_date + "\n" +
                "pricing_fleet_discount_amount=" + pricing_fleet_discount_amount + "\n" +
                "pricing_fleet_discount_percentage=" + pricing_fleet_discount_percentage + "\n" +
                "pricing_list_price=" + pricing_list_price + "\n" +
                "driver_address='" + driver_address + "\n" +
                "driver_city='" + driver_city + "\n" +
                "driver_email='" + driver_email + "\n" +
                "driver_name='" + driver_name + "\n" +
                "driver_phone_number='" + driver_phone_number + "\n" +
                "driver_postal_code='" + driver_postal_code + "\n" +
                "client_address='" + client_address + "\n" +
                "client_city='" + client_city + "\n" +
                "client_email='" + client_email + "\n" +
                "client_name='" + client_name + "\n" +
                "client_phone_number='" + client_phone_number + "\n" +
                "client_postal_code='" + client_postal_code + "\n" +
                "client_contact_person='" + client_contact_person + "\n" +
                "client_fax_number='" + client_fax_number + "\n" +
                "client_mobile_number='" + client_mobile_number + "\n" +
                "client_language_code=" + client_language_code +
                "client_vat_number='" + client_vat_number + "\n" +
                "driver_language_code=" + driver_language_code +
                "driver_mobile_number='" + driver_mobile_number + "\n" +
                "desired_delivery_date=" + desired_delivery_date + "\n" +
                "leasing_company_dossier_number='" + leasing_company_dossier_number + "\n" +
                "dealer_id=" + dealer_id + "\n" +
                "leasing_company_id=" + leasing_company_id + "\n" +
                "version_number=" + version_number + "\n" +
                "delivery_address_line=" + delivery_address_line + "\n" +
                "delivery_address_postal_code=" + delivery_address_postal_code + "\n" +
                "delivery_address_city=" + delivery_address_city + "\n" +
                "delivery_contact_email=" + delivery_contact_email + "\n" +
                "delivery_contact_person=" + delivery_contact_person + "\n" +
                "delivery_phone_number=" + delivery_phone_number + "\n" +
                "delivery_mobile_number=" + delivery_mobile_number + "\n" +
                "delivery_fax_number=" + delivery_fax_number + "\n" +
                "delivery_location_language=" + delivery_location_language + "\n" +
                "delivery_location_name=" + delivery_location_name + "\n" +
                "delivery_location_vat_number=" + delivery_location_vat_number + "\n" +
                "buyback_address_line=" + buyback_address_line + "\n" +
                "buyback_address_city=" + buyback_address_city + "\n" +
                "buyback_address_postal_code=" + buyback_address_postal_code + "\n" +
                "buyback_amount=" + buyback_amount + "\n" +
                "buyback=" + buyback + "\n" +
                "buyback_contract_duration=" + buyback_contract_duration + "\n" +
                "buyback_kilometers=" + buyback_kilometers + "\n" +
                "buyback_name=" + buyback_name + "\n" +
                "actual_delivery_date=" + actual_delivery_date + "\n" +
                "pickup_construction_year=" + pickup_construction_year + "\n" +
                "pickup_date_first_registration=" + pickup_date_first_registration + "\n" +
                "pickup_key_code=" + pickup_key_code + "\n" +
                "pickup_mileage=" + pickup_mileage + "\n" +
                "pickup_start_code=" + pickup_start_code + "\n" +
                "dealer_dossier_number='" + dealer_dossier_number + "\n" +
                "client_dealer_number=" + client_dealer_number + "\n" +
                "invoice_date=" + invoice_date + "\n" +
                "invoiced=" + invoiced + "\n" +
                "rei_batch_tracking_number=" + rei_batch_tracking_number + "\n" +
                "order_version=" + order_version + "\n" +
                "last_comment_comment=" + last_comment_comment + "\n" +
                "last_comment_creation_date=" + last_comment_creation_date + "\n" +
                "last_comment_order_change=" + last_comment_order_change + "\n" +
                "last_comment_updated_expected_delivery_date=" + last_comment_updated_expected_delivery_date + "\n" +
                "last_comment_updated_order_status=" + last_comment_updated_order_status + "\n" +
                "last_comment_user_id=" + last_comment_user_id + "\n" +
                "product_id=" + product_id + "\n" +
                "pricing_total_price=" + pricing_total_price + "\n" +
                "pricing_delivery_costs=" + pricing_delivery_costs + "\n" +
                "pricing_other_costs=" + pricing_other_costs + "\n" +
                "pickup_previous_vehicle_dropoff=" + pickup_previous_vehicle_dropoff + "\n" +
                "pickup_previous_vehicle_license_plate='" + pickup_previous_vehicle_license_plate + "\n" +
                "pickup_previous_vehicle_owner='" + pickup_previous_vehicle_owner + "\n" +
                "already_ordered=" + already_ordered + "\n" +
                "modified=" + modified + "\n" +
                "dossier_manager=" + dossier_manager + "\n" +
                "agent_id=" + agent_id + "\n" +
                "last_comment_agent_id=" + last_comment_agent_id + "\n" +
                "pickup_driver_first_name=" + pickup_driver_first_name + "\n" +
                "pickup_driver_last_name=" + pickup_driver_last_name + "\n" +
                "pickup_driver_birth_date=" + pickup_driver_birth_date +
                "pickup_driver_birth_place=" + pickup_driver_birth_place + "\n" +
                "pickup_proof_of_registration=" + pickup_proof_of_registration + "\n" +
                "pickup_proof_of_insurance=" + pickup_proof_of_insurance + "\n" +
                "pickup_certificate_of_conformity=" + pickup_certificate_of_conformity + "\n" +
                "pickup_proof_of_roadworthiness=" + pickup_proof_of_roadworthiness + "\n" +
                "pickup_legal_kit=" + pickup_legal_kit + "\n" +
                "pickup_manual=" + pickup_manual + "\n" +
                "pickup_fuel_card=" + pickup_fuel_card + "\n" +
                "pickup_number_of_keys=" + pickup_number_of_keys + "\n" +
                "client_registration_number_lessee=" + client_registration_number_lessee + "\n" +
                "insurance_company_name=" + insurance_company_name + "\n" +
                "insurance_company_email=" + insurance_company_email + "\n" +
                "insured_value_price=" + insured_value_price + "\n" +
                "tyrefitter_name=" + tyrefitter_name + "\n" +
                "tyrefitter_email=" + tyrefitter_email + "\n" +
                "proposed_delivery_date=" + proposed_delivery_date + "\n" +
                "tax_stamp_costs=" + tax_stamp_costs + "\n" +
                "inscription_costs=" + inscription_costs + "\n" +
                "delivery_contact=" + delivery_contact + "\n" +
                "linked_registration_id=" + Arrays.toString(linked_registration_id) + "\n" +
                "financial_lease_registration_info_date_last_registration=" + financial_lease_registration_info_date_last_registration + "\n" +
                "registration_div_to_do_by=" + registration_div_to_do_by + "\n" +
                "leasing_type=" + leasing_type + "\n" +
                "busy_until=" + busy_until + "\n" +
                "dealer_due_date=" + dealer_due_date + "\n" +
                "leasing_company_due_date=" + leasing_company_due_date + "\n" +
                "dealer_has_unseen_messages=" + dealer_has_unseen_messages + "\n" +
                "leasing_company_has_unseen_messages=" + leasing_company_has_unseen_messages + "\n" +
                "redactedGDPR=" + redactedGDPR + "\n" +
                "dealer_has_unseen_documents=" + dealer_has_unseen_documents + "\n" +
                "leasing_company_has_unseen_documents=" + leasing_company_has_unseen_documents + "\n" +
                "vehicle_dto_id=" + vehicle_dto_id + "\n" +
                "quote_reference=" + quote_reference + "\n" +
                "pickup_address_line=" + pickup_address_line + "\n" +
                "pickup_address_city=" + pickup_address_city + "\n" +
                "pickup_address_postal_code=" + pickup_address_postal_code + "\n" +
                "creation_date=" + creation_date + "\n" +
                "digital_delivery_status=" + digital_delivery_status + "\n" +
                "pickup_main_driver=" + pickup_main_driver + "\n" +
                "to_archive_date=" + to_archive_date +
                '}';
    }

    public Order_dto(byte[] order_id_, Object order_status_, int version_, java.sql.Date expected_delivery_date_, BigDecimal pricing_fleet_discount_amount_, BigDecimal pricing_fleet_discount_percentage_, BigDecimal pricing_list_price_, String driver_address_, String driver_city_, String driver_email_, String driver_name_, String driver_phone_number_, String driver_postal_code_, String client_address_, String client_city_, String client_email_, String client_name_, String client_phone_number_, String client_postal_code_, String client_contact_person_, String client_fax_number_, String client_mobile_number_, Object client_language_code_, String client_vat_number_, Object driver_language_code_, String driver_mobile_number_, java.sql.Date desired_delivery_date_, String leasing_company_dossier_number_, long dealer_id_, long leasing_company_id_, String version_number_, String delivery_address_line_, String delivery_address_postal_code_, String delivery_address_city_, String delivery_contact_email_, String delivery_contact_person_, String delivery_phone_number_, String delivery_mobile_number_, String delivery_fax_number_, Object delivery_location_language_, String delivery_location_name_, String delivery_location_vat_number_, String buyback_address_line_, String buyback_address_city_, String buyback_address_postal_code_, BigDecimal buyback_amount_, byte buyback_, int buyback_contract_duration_, int buyback_kilometers_, String buyback_name_, java.sql.Date actual_delivery_date_, int pickup_construction_year_, java.sql.Date pickup_date_first_registration_, String pickup_key_code_, int pickup_mileage_, String pickup_start_code_, String dealer_dossier_number_, String client_dealer_number_, java.sql.Date invoice_date_, byte invoiced_, int rei_batch_tracking_number_, int order_version_, String last_comment_comment_, java.sql.Date last_comment_creation_date_, String last_comment_order_change_, java.sql.Date last_comment_updated_expected_delivery_date_, String last_comment_updated_order_status_, long last_comment_user_id_, long product_id_, BigDecimal pricing_total_price_, BigDecimal pricing_delivery_costs_, BigDecimal pricing_other_costs_, byte pickup_previous_vehicle_dropoff_, String pickup_previous_vehicle_license_plate_, String pickup_previous_vehicle_owner_, byte already_ordered_, byte modified_, long dossier_manager_, long agent_id_, long last_comment_agent_id_, String pickup_driver_first_name_, String pickup_driver_last_name_, java.sql.Date pickup_driver_birth_date_, String pickup_driver_birth_place_, byte pickup_proof_of_registration_, byte pickup_proof_of_insurance_, byte pickup_certificate_of_conformity_, byte pickup_proof_of_roadworthiness_, byte pickup_legal_kit_, byte pickup_manual_, byte pickup_fuel_card_, int pickup_number_of_keys_, long client_registration_number_lessee_, String insurance_company_name_, String insurance_company_email_, BigDecimal insured_value_price_, String tyrefitter_name_, String tyrefitter_email_, java.sql.Date proposed_delivery_date_, BigDecimal tax_stamp_costs_, BigDecimal inscription_costs_, String delivery_contact_, byte[] linked_registration_id_, java.sql.Date financial_lease_registration_info_date_last_registration_, Object registration_div_to_do_by_, Object leasing_type_, java.sql.Date busy_until_, java.sql.Date dealer_due_date_, java.sql.Date leasing_company_due_date_, byte dealer_has_unseen_messages_, byte leasing_company_has_unseen_messages_, byte redactedGDPR_, boolean dealer_has_unseen_documents_, boolean leasing_company_has_unseen_documents_, long vehicle_dto_id_, String quote_reference_, String pickup_address_line_, String pickup_address_city_, String pickup_address_postal_code_, java.sql.Date creation_date_, String digital_delivery_status_, byte pickup_main_driver_, java.sql.Date to_archive_date_)
    {
        this.order_id = order_id_;
        this.order_status = order_status_;
        this.version = version_;
        this.expected_delivery_date = expected_delivery_date_;
        this.pricing_fleet_discount_amount = pricing_fleet_discount_amount_;
        this.pricing_fleet_discount_percentage = pricing_fleet_discount_percentage_;
        this.pricing_list_price = pricing_list_price_;
        this.driver_address = driver_address_;
        this.driver_city = driver_city_;
        this.driver_email = driver_email_;
        this.driver_name = driver_name_;
        this.driver_phone_number = driver_phone_number_;
        this.driver_postal_code = driver_postal_code_;
        this.client_address = client_address_;
        this.client_city = client_city_;
        this.client_email = client_email_;
        this.client_name = client_name_;
        this.client_phone_number = client_phone_number_;
        this.client_postal_code = client_postal_code_;
        this.client_contact_person = client_contact_person_;
        this.client_fax_number = client_fax_number_;
        this.client_mobile_number = client_mobile_number_;
        this.client_language_code = client_language_code_;
        this.client_vat_number = client_vat_number_;
        this.driver_language_code = driver_language_code_;
        this.driver_mobile_number = driver_mobile_number_;
        this.desired_delivery_date = desired_delivery_date_;
        this.leasing_company_dossier_number = leasing_company_dossier_number_;
        this.dealer_id = dealer_id_;
        this.leasing_company_id = leasing_company_id_;
        this.version_number = version_number_;
        this.delivery_address_line = delivery_address_line_;
        this.delivery_address_postal_code = delivery_address_postal_code_;
        this.delivery_address_city = delivery_address_city_;
        this.delivery_contact_email = delivery_contact_email_;
        this.delivery_contact_person = delivery_contact_person_;
        this.delivery_phone_number = delivery_phone_number_;
        this.delivery_mobile_number = delivery_mobile_number_;
        this.delivery_fax_number = delivery_fax_number_;
        this.delivery_location_language = delivery_location_language_;
        this.delivery_location_name = delivery_location_name_;
        this.delivery_location_vat_number = delivery_location_vat_number_;
        this.buyback_address_line = buyback_address_line_;
        this.buyback_address_city = buyback_address_city_;
        this.buyback_address_postal_code = buyback_address_postal_code_;
        this.buyback_amount = buyback_amount_;
        this.buyback = buyback_;
        this.buyback_contract_duration = buyback_contract_duration_;
        this.buyback_kilometers = buyback_kilometers_;
        this.buyback_name = buyback_name_;
        this.actual_delivery_date = actual_delivery_date_;
        this.pickup_construction_year = pickup_construction_year_;
        this.pickup_date_first_registration = pickup_date_first_registration_;
        this.pickup_key_code = pickup_key_code_;
        this.pickup_mileage = pickup_mileage_;
        this.pickup_start_code = pickup_start_code_;
        this.dealer_dossier_number = dealer_dossier_number_;
        this.client_dealer_number = client_dealer_number_;
        this.invoice_date = invoice_date_;
        this.invoiced = invoiced_;
        this.rei_batch_tracking_number = rei_batch_tracking_number_;
        this.order_version = order_version_;
        this.last_comment_comment = last_comment_comment_;
        this.last_comment_creation_date = last_comment_creation_date_;
        this.last_comment_order_change = last_comment_order_change_;
        this.last_comment_updated_expected_delivery_date = last_comment_updated_expected_delivery_date_;
        this.last_comment_updated_order_status = last_comment_updated_order_status_;
        this.last_comment_user_id = last_comment_user_id_;
        this.product_id = product_id_;
        this.pricing_total_price = pricing_total_price_;
        this.pricing_delivery_costs = pricing_delivery_costs_;
        this.pricing_other_costs = pricing_other_costs_;
        this.pickup_previous_vehicle_dropoff = pickup_previous_vehicle_dropoff_;
        this.pickup_previous_vehicle_license_plate = pickup_previous_vehicle_license_plate_;
        this.pickup_previous_vehicle_owner = pickup_previous_vehicle_owner_;
        this.already_ordered = already_ordered_;
        this.modified = modified_;
        this.dossier_manager = dossier_manager_;
        this.agent_id = agent_id_;
        this.last_comment_agent_id = last_comment_agent_id_;
        this.pickup_driver_first_name = pickup_driver_first_name_;
        this.pickup_driver_last_name = pickup_driver_last_name_;
        this.pickup_driver_birth_date = pickup_driver_birth_date_;
        this.pickup_driver_birth_place = pickup_driver_birth_place_;
        this.pickup_proof_of_registration = pickup_proof_of_registration_;
        this.pickup_proof_of_insurance = pickup_proof_of_insurance_;
        this.pickup_certificate_of_conformity = pickup_certificate_of_conformity_;
        this.pickup_proof_of_roadworthiness = pickup_proof_of_roadworthiness_;
        this.pickup_legal_kit = pickup_legal_kit_;
        this.pickup_manual = pickup_manual_;
        this.pickup_fuel_card = pickup_fuel_card_;
        this.pickup_number_of_keys = pickup_number_of_keys_;
        this.client_registration_number_lessee = client_registration_number_lessee_;
        this.insurance_company_name = insurance_company_name_;
        this.insurance_company_email = insurance_company_email_;
        this.insured_value_price = insured_value_price_;
        this.tyrefitter_name = tyrefitter_name_;
        this.tyrefitter_email = tyrefitter_email_;
        this.proposed_delivery_date = proposed_delivery_date_;
        this.tax_stamp_costs = tax_stamp_costs_;
        this.inscription_costs = inscription_costs_;
        this.delivery_contact = delivery_contact_;
        this.linked_registration_id = linked_registration_id_;
        this.financial_lease_registration_info_date_last_registration = financial_lease_registration_info_date_last_registration_;
        this.registration_div_to_do_by = registration_div_to_do_by_;
        this.leasing_type = leasing_type_;
        this.busy_until = busy_until_;
        this.dealer_due_date = dealer_due_date_;
        this.leasing_company_due_date = leasing_company_due_date_;
        this.dealer_has_unseen_messages = dealer_has_unseen_messages_;
        this.leasing_company_has_unseen_messages = leasing_company_has_unseen_messages_;
        this.redactedGDPR = redactedGDPR_;
        this.dealer_has_unseen_documents = dealer_has_unseen_documents_;
        this.leasing_company_has_unseen_documents = leasing_company_has_unseen_documents_;
        this.vehicle_dto_id = vehicle_dto_id_;
        this.quote_reference = quote_reference_;
        this.pickup_address_line = pickup_address_line_;
        this.pickup_address_city = pickup_address_city_;
        this.pickup_address_postal_code = pickup_address_postal_code_;
        this.creation_date = creation_date_;
        this.digital_delivery_status = digital_delivery_status_;
        this.pickup_main_driver = pickup_main_driver_;
        this.to_archive_date = to_archive_date_;
    }
}
