package apiEngine.model.database;

public class Order_export_dto {

    private byte[] order_id;
    private int version;
    private String brand;
    private String client_name;
    private String dealer_dossier_number;
    private String dealer_name;
    private String driver_name;
    private String leasing_company_dossier_number;
    private String leasing_company_name;
    private int leasing_company_number;
    private String license_plate;
    private String vehicle_model_dutch;
    private String vehicle_model_french;
    private int dealer_number;
    private String product_name;
    private int user_id;
    private String vehicle_vin;
    private String dealer_invoice_name;
    private String dealer_vat_number;
    private int agent_id;
    private String vehicle_model_english;
    private String version_number;
    private String vehicle_version_dutch;
    private String vehicle_version_french;
    private String vehicle_version_english;
    private String vehicle_exterior_colour_dutch;
    private String vehicle_exterior_colour_french;
    private String vehicle_exterior_colour_english;
    private String vehicle_interior_colour_dutch;
    private String vehicle_interior_colour_french;
    private String vehicle_interior_colour_english;
    private double pricing_total_price;
    private String vehicle_vin_control_number;
    private double vehicle_co2_emissions_nedc;
    private int pickup_mileage;
    private int pickup_construction_year;
    private String pickup_key_code;
    private String pickup_start_code;
    private String pickup_previous_vehicle_license_plate;
    private String pickup_previous_vehicle_owner;
    private int pickup_number_of_keys;
    private String pickup_driver_first_name;
    private String pickup_driver_last_name;
    private String pickup_driver_birth_place;
    private String client_dealer_number;
    private String insurance_broker_name;
    private String insurance_company_email;
    private String tyre_fitter_name;
    private String tyre_fitter_email;
    private String vehicle_tyre_brand;
    private String vehicle_tyre_description;
    private String leasing_type;
    private String last_comment;
    private double vehicle_co2_emissions_wltp;
    private String driver_email;
    private String pickup_address_line;
    private String pickup_address_city;
    private String pickup_address_postal_code;

    public byte[] getorder_id()
    {
        return this.order_id;
    }
    public void setorder_id(byte[] value)
    {
        this.order_id = value;
    }

    public int getVersion(){
        return version;
    }

    public void setVersion(int version){
        this.version=version;
    }

    public String getBrand(){
        return brand;
    }

    public void setBrand(String brand){
        this.brand=brand;
    }

    public String getClient_name(){
        return client_name;
    }

    public void setClient_name(String client_name){
        this.client_name=client_name;
    }

    public String getDealer_dossier_number(){
        return dealer_dossier_number;
    }

    public void setDealer_dossier_number(String dealer_dossier_number){
        this.dealer_dossier_number=dealer_dossier_number;
    }

    public String getDealer_name(){
        return dealer_name;
    }

    public void setDealer_name(String dealer_name){
        this.dealer_name=dealer_name;
    }

    public String getDriver_name(){
        return driver_name;
    }

    public void setDriver_name(String driver_name){
        this.driver_name=driver_name;
    }

    public String getLeasing_company_dossier_number(){
        return leasing_company_dossier_number;
    }

    public void setLeasing_company_dossier_number(String leasing_company_dossier_number){
        this.leasing_company_dossier_number=leasing_company_dossier_number;
    }

    public String getLeasing_company_name(){
        return leasing_company_name;
    }

    public void setLeasing_company_name(String leasing_company_name){
        this.leasing_company_name=leasing_company_name;
    }

    public int getLeasing_company_number(){
        return leasing_company_number;
    }

    public void setLeasing_company_number(int leasing_company_number){
        this.leasing_company_number=leasing_company_number;
    }

    public String getLicense_plate(){
        return license_plate;
    }

    public void setLicense_plate(String license_plate){
        this.license_plate=license_plate;
    }

    public String getVehicle_model_dutch(){
        return vehicle_model_dutch;
    }

    public void setVehicle_model_dutch(String vehicle_model_dutch){
        this.vehicle_model_dutch=vehicle_model_dutch;
    }

    public String getVehicle_model_french(){
        return vehicle_model_french;
    }

    public void setVehicle_model_french(String vehicle_model_french){
        this.vehicle_model_french=vehicle_model_french;
    }

    public int getDealer_number(){
        return dealer_number;
    }

    public void setDealer_number(int dealer_number){
        this.dealer_number=dealer_number;
    }

    public String getProduct_name(){
        return product_name;
    }

    public void setProduct_name(String product_name){
        this.product_name=product_name;
    }

    public int getUser_id(){
        return user_id;
    }

    public void setUser_id(int user_id){
        this.user_id=user_id;
    }

    public String getVehicle_vin(){
        return vehicle_vin;
    }

    public void setVehicle_vin(String vehicle_vin){
        this.vehicle_vin=vehicle_vin;
    }

    public String getDealer_invoice_name(){
        return dealer_invoice_name;
    }

    public void setDealer_invoice_name(String dealer_invoice_name){
        this.dealer_invoice_name=dealer_invoice_name;
    }

    public String getDealer_vat_number(){
        return dealer_vat_number;
    }

    public void setDealer_vat_number(String dealer_vat_number){
        this.dealer_vat_number=dealer_vat_number;
    }

    public int getAgent_id(){
        return agent_id;
    }

    public void setAgent_id(int agent_id){
        this.agent_id=agent_id;
    }

    public String getVehicle_model_english(){
        return vehicle_model_english;
    }

    public void setVehicle_model_english(String vehicle_model_english){
        this.vehicle_model_english=vehicle_model_english;
    }

    public String getVersion_number(){
        return version_number;
    }

    public void setVersion_number(String version_number){
        this.version_number=version_number;
    }

    public String getVehicle_version_dutch(){
        return vehicle_version_dutch;
    }

    public void setVehicle_version_dutch(String vehicle_version_dutch){
        this.vehicle_version_dutch=vehicle_version_dutch;
    }

    public String getVehicle_version_french(){
        return vehicle_version_french;
    }

    public void setVehicle_version_french(String vehicle_version_french){
        this.vehicle_version_french=vehicle_version_french;
    }

    public String getVehicle_version_english(){
        return vehicle_version_english;
    }

    public void setVehicle_version_english(String vehicle_version_english){
        this.vehicle_version_english=vehicle_version_english;
    }

    public String getVehicle_exterior_colour_dutch(){
        return vehicle_exterior_colour_dutch;
    }

    public void setVehicle_exterior_colour_dutch(String vehicle_exterior_colour_dutch){
        this.vehicle_exterior_colour_dutch=vehicle_exterior_colour_dutch;
    }

    public String getVehicle_exterior_colour_french(){
        return vehicle_exterior_colour_french;
    }

    public void setVehicle_exterior_colour_french(String vehicle_exterior_colour_french){
        this.vehicle_exterior_colour_french=vehicle_exterior_colour_french;
    }

    public String getVehicle_exterior_colour_english(){
        return vehicle_exterior_colour_english;
    }

    public void setVehicle_exterior_colour_english(String vehicle_exterior_colour_english){
        this.vehicle_exterior_colour_english=vehicle_exterior_colour_english;
    }

    public String getVehicle_interior_colour_dutch(){
        return vehicle_interior_colour_dutch;
    }

    public void setVehicle_interior_colour_dutch(String vehicle_interior_colour_dutch){
        this.vehicle_interior_colour_dutch=vehicle_interior_colour_dutch;
    }

    public String getVehicle_interior_colour_french(){
        return vehicle_interior_colour_french;
    }

    public void setVehicle_interior_colour_french(String vehicle_interior_colour_french){
        this.vehicle_interior_colour_french=vehicle_interior_colour_french;
    }

    public String getVehicle_interior_colour_english(){
        return vehicle_interior_colour_english;
    }

    public void setVehicle_interior_colour_english(String vehicle_interior_colour_english){
        this.vehicle_interior_colour_english=vehicle_interior_colour_english;
    }

    public double getPricing_total_price(){
        return pricing_total_price;
    }

    public void setPricing_total_price(double pricing_total_price){
        this.pricing_total_price=pricing_total_price;
    }

    public String getVehicle_vin_control_number(){
        return vehicle_vin_control_number;
    }

    public void setVehicle_vin_control_number(String vehicle_vin_control_number){
        this.vehicle_vin_control_number=vehicle_vin_control_number;
    }

    public double getVehicle_co2_emissions_nedc(){
        return vehicle_co2_emissions_nedc;
    }

    public void setVehicle_co2_emissions_nedc(double vehicle_co2_emissions_nedc){
        this.vehicle_co2_emissions_nedc=vehicle_co2_emissions_nedc;
    }

    public int getPickup_mileage(){
        return pickup_mileage;
    }

    public void setPickup_mileage(int pickup_mileage){
        this.pickup_mileage=pickup_mileage;
    }

    public int getPickup_construction_year(){
        return pickup_construction_year;
    }

    public void setPickup_construction_year(int pickup_construction_year){
        this.pickup_construction_year=pickup_construction_year;
    }

    public String getPickup_key_code(){
        return pickup_key_code;
    }

    public void setPickup_key_code(String pickup_key_code){
        this.pickup_key_code=pickup_key_code;
    }

    public String getPickup_start_code(){
        return pickup_start_code;
    }

    public void setPickup_start_code(String pickup_start_code){
        this.pickup_start_code=pickup_start_code;
    }

    public String getPickup_previous_vehicle_license_plate(){
        return pickup_previous_vehicle_license_plate;
    }

    public void setPickup_previous_vehicle_license_plate(String pickup_previous_vehicle_license_plate){
        this.pickup_previous_vehicle_license_plate=pickup_previous_vehicle_license_plate;
    }

    public String getPickup_previous_vehicle_owner(){
        return pickup_previous_vehicle_owner;
    }

    public void setPickup_previous_vehicle_owner(String pickup_previous_vehicle_owner){
        this.pickup_previous_vehicle_owner=pickup_previous_vehicle_owner;
    }

    public int getPickup_number_of_keys(){
        return pickup_number_of_keys;
    }

    public void setPickup_number_of_keys(int pickup_number_of_keys){
        this.pickup_number_of_keys=pickup_number_of_keys;
    }

    public String getPickup_driver_first_name(){
        return pickup_driver_first_name;
    }

    public void setPickup_driver_first_name(String pickup_driver_first_name){
        this.pickup_driver_first_name=pickup_driver_first_name;
    }

    public String getPickup_driver_last_name(){
        return pickup_driver_last_name;
    }

    public void setPickup_driver_last_name(String pickup_driver_last_name){
        this.pickup_driver_last_name=pickup_driver_last_name;
    }

    public String getPickup_driver_birth_place(){
        return pickup_driver_birth_place;
    }

    public void setPickup_driver_birth_place(String pickup_driver_birth_place){
        this.pickup_driver_birth_place=pickup_driver_birth_place;
    }

    public String getClient_dealer_number(){
        return client_dealer_number;
    }

    public void setClient_dealer_number(String client_dealer_number){
        this.client_dealer_number=client_dealer_number;
    }

    public String getInsurance_broker_name(){
        return insurance_broker_name;
    }

    public void setInsurance_broker_name(String insurance_broker_name){
        this.insurance_broker_name=insurance_broker_name;
    }

    public String getInsurance_company_email(){
        return insurance_company_email;
    }

    public void setInsurance_company_email(String insurance_company_email){
        this.insurance_company_email=insurance_company_email;
    }

    public String getTyre_fitter_name(){
        return tyre_fitter_name;
    }

    public void setTyre_fitter_name(String tyre_fitter_name){
        this.tyre_fitter_name=tyre_fitter_name;
    }

    public String getTyre_fitter_email(){
        return tyre_fitter_email;
    }

    public void setTyre_fitter_email(String tyre_fitter_email){
        this.tyre_fitter_email=tyre_fitter_email;
    }

    public String getVehicle_tyre_brand(){
        return vehicle_tyre_brand;
    }

    public void setVehicle_tyre_brand(String vehicle_tyre_brand){
        this.vehicle_tyre_brand=vehicle_tyre_brand;
    }

    public String getVehicle_tyre_description(){
        return vehicle_tyre_description;
    }

    public void setVehicle_tyre_description(String vehicle_tyre_description){
        this.vehicle_tyre_description=vehicle_tyre_description;
    }

    public String getLeasing_type(){
        return leasing_type;
    }

    public void setLeasing_type(String leasing_type){
        this.leasing_type=leasing_type;
    }

    public String getLast_comment(){
        return last_comment;
    }

    public void setLast_comment(String last_comment){
        this.last_comment=last_comment;
    }

    public double getVehicle_co2_emissions_wltp(){
        return vehicle_co2_emissions_wltp;
    }

    public void setVehicle_co2_emissions_wltp(double vehicle_co2_emissions_wltp){
        this.vehicle_co2_emissions_wltp=vehicle_co2_emissions_wltp;
    }

    public String getDriver_email(){
        return driver_email;
    }

    public void setDriver_email(String driver_email){
        this.driver_email=driver_email;
    }

    public String getPickup_address_line(){
        return pickup_address_line;
    }

    public void setPickup_address_line(String pickup_address_line){
        this.pickup_address_line=pickup_address_line;
    }

    public String getPickup_address_city(){
        return pickup_address_city;
    }

    public void setPickup_address_city(String pickup_address_city){
        this.pickup_address_city=pickup_address_city;
    }

    public String getPickup_address_postal_code(){
        return pickup_address_postal_code;
    }

    public void setPickup_address_postal_code(String pickup_address_postal_code){
        this.pickup_address_postal_code=pickup_address_postal_code;
    }

    @Override
    public String toString() {
        return "Order_export_dto{" +
                "version=" + version + "\n" +
                "brand=" + brand + "\n" +
                "client_name=" + client_name + "\n" +
                "dealer_dossier_number=" + dealer_dossier_number + "\n" +
                "dealer_name=" + dealer_name + "\n" +
                "driver_name=" + driver_name + "\n" +
                "leasing_company_dossier_number=" + leasing_company_dossier_number + "\n" +
                "leasing_company_name=" + leasing_company_name + "\n" +
                "leasing_company_number=" + leasing_company_number + "\n" +
                "license_plate=" + license_plate + "\n" +
                "vehicle_model_dutch=" + vehicle_model_dutch + "\n" +
                "vehicle_model_french=" + vehicle_model_french + "\n" +
                "dealer_number=" + dealer_number + "\n" +
                "product_name=" + product_name + "\n" +
                "user_id=" + user_id + "\n" +
                "vehicle_vin=" + vehicle_vin + "\n" +
                "dealer_invoice_name=" + dealer_invoice_name + "\n" +
                "dealer_vat_number=" + dealer_vat_number + "\n" +
                "agent_id=" + agent_id + "\n" +
                "vehicle_model_english=" + vehicle_model_english + "\n" +
                "version_number=" + version_number + "\n" +
                "vehicle_version_dutch=" + vehicle_version_dutch + "\n" +
                "vehicle_version_french=" + vehicle_version_french + "\n" +
                "vehicle_version_english=" + vehicle_version_english + "\n" +
                "vehicle_exterior_colour_dutch=" + vehicle_exterior_colour_dutch + "\n" +
                "vehicle_exterior_colour_french=" + vehicle_exterior_colour_french + "\n" +
                "vehicle_exterior_colour_english=" + vehicle_exterior_colour_english + "\n" +
                "vehicle_interior_colour_dutch=" + vehicle_interior_colour_dutch + "\n" +
                "vehicle_interior_colour_french=" + vehicle_interior_colour_french + "\n" +
                "vehicle_interior_colour_english=" + vehicle_interior_colour_english + "\n" +
                "pricing_total_price=" + pricing_total_price + "\n" +
                "vehicle_vin_control_number=" + vehicle_vin_control_number + "\n" +
                "vehicle_co2_emissions_nedc=" + vehicle_co2_emissions_nedc + "\n" +
                "pickup_mileage=" + pickup_mileage + "\n" +
                "pickup_construction_year=" + pickup_construction_year + "\n" +
                "pickup_key_code=" + pickup_key_code + "\n" +
                "pickup_start_code=" + pickup_start_code + "\n" +
                "pickup_previous_vehicle_license_plate=" + pickup_previous_vehicle_license_plate + "\n" +
                "pickup_previous_vehicle_owner=" + pickup_previous_vehicle_owner + "\n" +
                "pickup_number_of_keys=" + pickup_number_of_keys + "\n" +
                "pickup_driver_first_name=" + pickup_driver_first_name + "\n" +
                "pickup_driver_last_name=" + pickup_driver_last_name + "\n" +
                "pickup_driver_birth_place=" + pickup_driver_birth_place + "\n" +
                "client_dealer_number=" + client_dealer_number + "\n" +
                "insurance_broker_name=" + insurance_broker_name + "\n" +
                "insurance_company_email=" + insurance_company_email + "\n" +
                "tyre_fitter_name=" + tyre_fitter_name + "\n" +
                "tyre_fitter_email=" + tyre_fitter_email + "\n" +
                "vehicle_tyre_brand=" + vehicle_tyre_brand + "\n" +
                "vehicle_tyre_description=" + vehicle_tyre_description + "\n" +
                "leasing_type=" + leasing_type + "\n" +
                "last_comment=" + last_comment + "\n" +
                "vehicle_co2_emissions_wltp=" + vehicle_co2_emissions_wltp +
                "driver_email=" + driver_email + "\n" +
                "pickup_address_line=" + pickup_address_line + "\n" +
                "pickup_address_city=" + pickup_address_city + "\n" +
                "pickup_address_postal_code=" + pickup_address_postal_code + "\n" +
                '}';
    }
}
