
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for CarIdentification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CarIdentification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Brand" type="{http://be/renta/ordering/communication/registrationimport}NullString" minOccurs="0"/&gt;
 *         &lt;element name="Model" type="{http://be/renta/ordering/communication/registrationimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="VIN" type="{http://be/renta/ordering/communication/registrationimport}NullString" minOccurs="0"/&gt;
 *         &lt;element name="VINControlNumber" type="{http://be/renta/ordering/communication/registrationimport}NullString" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarIdentification", namespace = "http://be/renta/ordering/communication/registrationimport", propOrder = {

})
public class CarIdentification {

    @XmlElement(name = "Brand", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String brand;
    @XmlElement(name = "Model", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected Translatable model;
    @XmlElement(name = "VIN", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String vin;
    @XmlElement(name = "VINControlNumber", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String vinControlNumber;

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setModel(Translatable value) {
        this.model = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIN() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIN(String value) {
        this.vin = value;
    }

    /**
     * Gets the value of the vinControlNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVINControlNumber() {
        return vinControlNumber;
    }

    /**
     * Sets the value of the vinControlNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVINControlNumber(String value) {
        this.vinControlNumber = value;
    }

}
