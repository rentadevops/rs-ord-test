
package apiEngine.model.post.registration;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for Delivery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Delivery"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="DesiredDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address" type="{http://be/renta/ordering/communication/orderimport}Address" minOccurs="0"/&gt;
 *         &lt;element name="Contact" type="{http://be/renta/ordering/communication/orderimport}Contact" minOccurs="0"/&gt;
 *         &lt;element name="LocationLanguageCode" type="{http://be/renta/ordering/communication/orderimport}LanguageCode" minOccurs="0"/&gt;
 *         &lt;element name="ContactPerson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationVAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Delivery", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Delivery {

    @XmlElement(name = "DesiredDate", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar desiredDate;
    @XmlElement(name = "LocationName", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String locationName;
    @XmlElement(name = "Address", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Address address;
    @XmlElement(name = "Contact", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Contact contact;
    @XmlElement(name = "LocationLanguageCode", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String locationLanguageCode;
    @XmlElement(name = "ContactPerson", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String contactPerson;
    @XmlElement(name = "LocationVAT", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String locationVAT;

    /**
     * Gets the value of the desiredDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDesiredDate() {
        return desiredDate;
    }

    /**
     * Sets the value of the desiredDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDesiredDate(XMLGregorianCalendar value) {
        this.desiredDate = value;
    }

    /**
     * Gets the value of the locationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Sets the value of the locationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.locationName = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Gets the value of the locationLanguageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationLanguageCode() {
        return locationLanguageCode;
    }

    /**
     * Sets the value of the locationLanguageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationLanguageCode(String value) {
        this.locationLanguageCode = value;
    }

    /**
     * Gets the value of the contactPerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Sets the value of the contactPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPerson(String value) {
        this.contactPerson = value;
    }

    /**
     * Gets the value of the locationVAT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationVAT() {
        return locationVAT;
    }

    /**
     * Sets the value of the locationVAT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationVAT(String value) {
        this.locationVAT = value;
    }

}
