
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for PedelecType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="PedelecType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PEDALING_ASSISTANCE_ONLY"/&gt;
 *     &lt;enumeration value="PEDALING_ASSISTANCE_AND_AUTONOMOUS"/&gt;
 *     &lt;enumeration value="NOT_APPLICABLE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PedelecType", namespace = "http://be/renta/ordering/communication/registrationimport")
@XmlEnum
public enum PedelecType {

    PEDALING_ASSISTANCE_ONLY,
    PEDALING_ASSISTANCE_AND_AUTONOMOUS,
    NOT_APPLICABLE;

    public String value() {
        return name();
    }

    public static PedelecType fromValue(String v) {
        return valueOf(v);
    }

}
