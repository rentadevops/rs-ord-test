
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for CancelRegistration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CancelRegistration"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="RegistrarDossierNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RegistrarRSNumber" type="{http://be/renta/ordering/communication/registrationimport}NullInt"/&gt;
 *         &lt;element name="UserEmail" type="{http://be/renta/ordering/communication/registrationimport}Email"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelRegistration", namespace = "http://be/renta/ordering/communication/registrationimport", propOrder = {

})
public class CancelRegistration {

    @XmlElement(name = "RegistrarDossierNumber", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected String registrarDossierNumber;
    @XmlElement(name = "RegistrarRSNumber", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected String registrarRSNumber;
    @XmlElement(name = "UserEmail", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected String userEmail;

    /**
     * Gets the value of the registrarDossierNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrarDossierNumber() {
        return registrarDossierNumber;
    }

    /**
     * Sets the value of the registrarDossierNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrarDossierNumber(String value) {
        this.registrarDossierNumber = value;
    }

    /**
     * Gets the value of the registrarRSNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrarRSNumber() {
        return registrarRSNumber;
    }

    /**
     * Sets the value of the registrarRSNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrarRSNumber(String value) {
        this.registrarRSNumber = value;
    }

    /**
     * Gets the value of the userEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Sets the value of the userEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserEmail(String value) {
        this.userEmail = value;
    }

}
