
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RegistrationInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationInformation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="UsedVehicle" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="LicensePlatePreviousOwner" type="{http://be/renta/ordering/communication/registrationimport}NullString" minOccurs="0"/&gt;
 *         &lt;element name="LastRegistrationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="LanguageRegistrationPapers" type="{http://be/renta/ordering/communication/registrationimport}LanguageRegistrationPapers"/&gt;
 *         &lt;element name="LicensePlateTakenOver" type="{http://be/renta/ordering/communication/registrationimport}NullString" minOccurs="0"/&gt;
 *         &lt;element name="LicensePlateFormat" type="{http://be/renta/ordering/communication/registrationimport}LicensePlateFormat"/&gt;
 *         &lt;element name="EmailGreenCard" type="{http://be/renta/ordering/communication/registrationimport}Email" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationInformation", namespace = "http://be/renta/ordering/communication/registrationimport", propOrder = {

})
public class RegistrationInformation {

    @XmlElement(name = "UsedVehicle", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected boolean usedVehicle;
    @XmlElement(name = "LicensePlatePreviousOwner", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String licensePlatePreviousOwner;
    @XmlElement(name = "LastRegistrationDate", namespace = "http://be/renta/ordering/communication/registrationimport")
    @XmlSchemaType(name = "date")
    protected String lastRegistrationDate;
    @XmlElement(name = "LanguageRegistrationPapers", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    //protected String languageRegistrationPapers;
    @XmlSchemaType(name = "normalizedString")
    protected LanguageRegistrationPapers languageRegistrationPapers;
    @XmlElement(name = "LicensePlateTakenOver", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String licensePlateTakenOver;
    @XmlElement(name = "LicensePlateFormat", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    @XmlSchemaType(name = "normalizedString")
    protected LicensePlateFormat licensePlateFormat;
    @XmlElement(name = "EmailGreenCard", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String emailGreenCard;
    @XmlElement(name = "FormNumber", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String formNumber;

    /**
     * Gets the value of the usedVehicle property.
     * 
     */
    public boolean isUsedVehicle() {
        return usedVehicle;
    }

    /**
     * Sets the value of the usedVehicle property.
     * 
     */
    public void setUsedVehicle(boolean value) {
        this.usedVehicle = value;
    }

    /**
     * Gets the value of the licensePlatePreviousOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlatePreviousOwner() {
        return licensePlatePreviousOwner;
    }

    /**
     * Sets the value of the licensePlatePreviousOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlatePreviousOwner(String value) {
        this.licensePlatePreviousOwner = value;
    }

    /**
     * Gets the value of the lastRegistrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastRegistrationDate() {
        return lastRegistrationDate;
    }

    /**
     * Sets the value of the lastRegistrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastRegistrationDate(String value) {
        this.lastRegistrationDate = value;
    }

    /**
     * Gets the value of the languageRegistrationPapers property.
     * 
     * @return
     *     possible object is
     *     {@link LanguageRegistrationPapers }
     *     
     */
    public LanguageRegistrationPapers getLanguageRegistrationPapers() {
        return languageRegistrationPapers;
    }

    /**
     * Sets the value of the languageRegistrationPapers property.
     * 
     * @param value
     *     allowed object is
     *     {@link LanguageRegistrationPapers }
     *     
     */
    public void setLanguageRegistrationPapers(LanguageRegistrationPapers value) {
        this.languageRegistrationPapers = value;
    }

    /**
     * Gets the value of the licensePlateTakenOver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlateTakenOver() {
        return licensePlateTakenOver;
    }

    /**
     * Sets the value of the licensePlateTakenOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlateTakenOver(String value) {
        this.licensePlateTakenOver = value;
    }

    /**
     * Gets the value of the licensePlateFormat property.
     * 
     * @return
     *     possible object is
     *     {@link LicensePlateFormat }
     *     
     */
    public LicensePlateFormat getLicensePlateFormat() {
        return licensePlateFormat;
    }

    /**
     * Sets the value of the licensePlateFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicensePlateFormat }
     *     
     */
    public void setLicensePlateFormat(LicensePlateFormat value) {
        this.licensePlateFormat = value;
    }

    /**
     * Gets the value of the emailGreenCard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailGreenCard() {
        return emailGreenCard;
    }

    /**
     * Sets the value of the emailGreenCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailGreenCard(String value) {
        this.emailGreenCard = value;
    }

    public String getFormNumber() {
        return formNumber;
    }

    /**
     * Sets the value of the formNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFormNumber(String value) {
        this.formNumber = value;
    }

}
