
package apiEngine.model.post.registration;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for Meta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Meta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="TransactionCode" type="{http://be/renta/ordering/communication/orderimport}TransactionCode"/&gt;
 *         &lt;element name="DossierNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LeasingCompanyRSNumber" type="{http://be/renta/ordering/communication/orderimport}NullInt"/&gt;
 *         &lt;element name="DealerRSNumber" type="{http://be/renta/ordering/communication/orderimport}NullLong"/&gt;
 *         &lt;element name="UserEmail" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="VersionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LogoReferenceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DateFirstRegistration" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="LeasingType" type="{http://be/renta/ordering/communication/orderimport}LeasingType" minOccurs="0"/&gt;
 *         &lt;element name="ConfigurationIdDealer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuoteReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Meta", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Meta {

    @XmlElement(name = "TransactionCode", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCode transactionCode;
    @XmlElement(name = "DossierNumber", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected String dossierNumber;
    @XmlElement(name = "LeasingCompanyRSNumber", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected String leasingCompanyRSNumber;
    @XmlElement(name = "DealerRSNumber", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected String dealerRSNumber;
    @XmlElement(name = "UserEmail", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected String userEmail;
    @XmlElement(name = "VersionNumber", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String versionNumber;
    @XmlElement(name = "TrackingNumber", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String trackingNumber;
    @XmlElement(name = "Comment", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String comment;
    @XmlElement(name = "LogoReferenceCode", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String logoReferenceCode;
    @XmlElement(name = "DateFirstRegistration", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateFirstRegistration;
    @XmlElement(name = "LeasingType", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlSchemaType(name = "string")
    protected LeasingType leasingType;
    @XmlElement(name = "ConfigurationIdDealer", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String configurationIdDealer;
    @XmlElement(name = "QuoteReference", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String quoteReference;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCode }
     *     
     */
    public TransactionCode getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCode }
     *     
     */
    public void setTransactionCode(TransactionCode value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the dossierNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDossierNumber() {
        return dossierNumber;
    }

    /**
     * Sets the value of the dossierNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDossierNumber(String value) {
        this.dossierNumber = value;
    }

    /**
     * Gets the value of the leasingCompanyRSNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeasingCompanyRSNumber() {
        return leasingCompanyRSNumber;
    }

    /**
     * Sets the value of the leasingCompanyRSNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeasingCompanyRSNumber(String value) {
        this.leasingCompanyRSNumber = value;
    }

    /**
     * Gets the value of the dealerRSNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerRSNumber() {
        return dealerRSNumber;
    }

    /**
     * Sets the value of the dealerRSNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerRSNumber(String value) {
        this.dealerRSNumber = value;
    }

    /**
     * Gets the value of the userEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Sets the value of the userEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserEmail(String value) {
        this.userEmail = value;
    }

    /**
     * Gets the value of the versionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionNumber() {
        return versionNumber;
    }

    /**
     * Sets the value of the versionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionNumber(String value) {
        this.versionNumber = value;
    }

    /**
     * Gets the value of the trackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * Sets the value of the trackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackingNumber(String value) {
        this.trackingNumber = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the logoReferenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogoReferenceCode() {
        return logoReferenceCode;
    }

    /**
     * Sets the value of the logoReferenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogoReferenceCode(String value) {
        this.logoReferenceCode = value;
    }

    /**
     * Gets the value of the dateFirstRegistration property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateFirstRegistration() {
        return dateFirstRegistration;
    }

    /**
     * Sets the value of the dateFirstRegistration property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateFirstRegistration(XMLGregorianCalendar value) {
        this.dateFirstRegistration = value;
    }

    /**
     * Gets the value of the leasingType property.
     * 
     * @return
     *     possible object is
     *     {@link LeasingType }
     *     
     */
    public LeasingType getLeasingType() {
        return leasingType;
    }

    /**
     * Sets the value of the leasingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LeasingType }
     *     
     */
    public void setLeasingType(LeasingType value) {
        this.leasingType = value;
    }

    /**
     * Gets the value of the configurationIdDealer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurationIdDealer() {
        return configurationIdDealer;
    }

    /**
     * Sets the value of the configurationIdDealer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurationIdDealer(String value) {
        this.configurationIdDealer = value;
    }

    /**
     * Gets the value of the quoteReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuoteReference() {
        return quoteReference;
    }

    /**
     * Sets the value of the quoteReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteReference(String value) {
        this.quoteReference = value;
    }

}
