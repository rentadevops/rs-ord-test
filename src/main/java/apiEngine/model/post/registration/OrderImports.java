
package apiEngine.model.post.registration;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Header" type="{http://be/renta/ordering/communication/orderimport}Header"/&gt;
 *         &lt;element name="Order" type="{http://be/renta/ordering/communication/orderimport}Order" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="BikeOrder" type="{http://be/renta/ordering/communication/orderimport}BikeOrder" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "order",
    "bikeOrder"
})
@XmlRootElement(name = "OrderImports", namespace = "http://be/renta/ordering/communication/orderimport")
public class OrderImports {

    @XmlElement(name = "Header", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected Header header;
    @XmlElement(name = "Order", namespace = "http://be/renta/ordering/communication/orderimport")
    protected List<Order> order;
    @XmlElement(name = "BikeOrder", namespace = "http://be/renta/ordering/communication/orderimport")
    protected List<BikeOrder> bikeOrder;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the order property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Order }
     * 
     * 
     */
    public List<Order> getOrder() {
        if (order == null) {
            order = new ArrayList<Order>();
        }
        return this.order;
    }

    /**
     * Gets the value of the bikeOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bikeOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBikeOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BikeOrder }
     * 
     * 
     */
    public List<BikeOrder> getBikeOrder() {
        if (bikeOrder == null) {
            bikeOrder = new ArrayList<BikeOrder>();
        }
        return this.bikeOrder;
    }

}
