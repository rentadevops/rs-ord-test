
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for PlateHolder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlateHolder"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="CompanyIdentification" type="{http://be/renta/ordering/communication/registrationimport}CompanyIdentification"/&gt;
 *         &lt;element name="Person" type="{http://be/renta/ordering/communication/registrationimport}Person"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlateHolder", namespace = "http://be/renta/ordering/communication/registrationimport", propOrder = {
    "companyIdentification",
    "person"
})
public class PlateHolder {

    @XmlElement(name = "CompanyIdentification", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected CompanyIdentification companyIdentification;
    @XmlElement(name = "Person", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected Person person;

    /**
     * Gets the value of the companyIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyIdentification }
     *     
     */
    public CompanyIdentification getCompanyIdentification() {
        return companyIdentification;
    }

    /**
     * Sets the value of the companyIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyIdentification }
     *     
     */
    public void setCompanyIdentification(CompanyIdentification value) {
        this.companyIdentification = value;
    }

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link Person }
     *     
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link Person }
     *     
     */
    public void setPerson(Person value) {
        this.person = value;
    }

}
