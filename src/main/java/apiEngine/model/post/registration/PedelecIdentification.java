
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for PedelecIdentification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PedelecIdentification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="SpeedPedelec" type="{http://be/renta/ordering/communication/registrationimport}TrueFalse" minOccurs="0"/&gt;
 *         &lt;element name="PedelecType" type="{http://be/renta/ordering/communication/registrationimport}PedelecType" minOccurs="0"/&gt;
 *         &lt;element name="VIN" type="{http://be/renta/ordering/communication/registrationimport}NullString" minOccurs="0"/&gt;
 *         &lt;element name="VINControlNumber" type="{http://be/renta/ordering/communication/registrationimport}NullString" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PedelecIdentification", namespace = "http://be/renta/ordering/communication/registrationimport", propOrder = {

})
public class PedelecIdentification {

    @XmlElement(name = "SpeedPedelec", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String speedPedelec;
    @XmlElement(name = "PedelecType", namespace = "http://be/renta/ordering/communication/registrationimport")
    @XmlSchemaType(name = "string")
    protected PedelecType pedelecType;
    @XmlElement(name = "VIN", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String vin;
    @XmlElement(name = "VINControlNumber", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected String vinControlNumber;

    /**
     * Gets the value of the speedPedelec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpeedPedelec() {
        return speedPedelec;
    }

    /**
     * Sets the value of the speedPedelec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpeedPedelec(String value) {
        this.speedPedelec = value;
    }

    /**
     * Gets the value of the pedelecType property.
     * 
     * @return
     *     possible object is
     *     {@link PedelecType }
     *     
     */
    public PedelecType getPedelecType() {
        return pedelecType;
    }

    /**
     * Sets the value of the pedelecType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PedelecType }
     *     
     */
    public void setPedelecType(PedelecType value) {
        this.pedelecType = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIN() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIN(String value) {
        this.vin = value;
    }

    /**
     * Gets the value of the vinControlNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVINControlNumber() {
        return vinControlNumber;
    }

    /**
     * Sets the value of the vinControlNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVINControlNumber(String value) {
        this.vinControlNumber = value;
    }

}
