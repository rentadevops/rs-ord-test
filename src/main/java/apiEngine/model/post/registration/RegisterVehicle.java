
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for RegisterVehicle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegisterVehicle"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Meta" type="{http://be/renta/ordering/communication/registrationimport}Meta"/&gt;
 *         &lt;element name="PlateHolder" type="{http://be/renta/ordering/communication/registrationimport}PlateHolder"/&gt;
 *         &lt;element name="Client" type="{http://be/renta/ordering/communication/registrationimport}Client" minOccurs="0"/&gt;
 *         &lt;element name="Driver" type="{http://be/renta/ordering/communication/registrationimport}Driver" minOccurs="0"/&gt;
 *         &lt;element name="Seller" type="{http://be/renta/ordering/communication/registrationimport}Seller"/&gt;
 *         &lt;element name="Vehicle" type="{http://be/renta/ordering/communication/registrationimport}Vehicle"/&gt;
 *         &lt;element name="RegistrationInformation" type="{http://be/renta/ordering/communication/registrationimport}RegistrationInformation"/&gt;
 *         &lt;element name="Insurance" type="{http://be/renta/ordering/communication/registrationimport}Insurance"/&gt;
 *         &lt;element name="Delivery" type="{http://be/renta/ordering/communication/registrationimport}Delivery" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegisterVehicle", namespace = "http://be/renta/ordering/communication/registrationimport", propOrder = {

})
public class RegisterVehicle {

    @XmlElement(name = "Meta", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected Meta meta;
    @XmlElement(name = "PlateHolder", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected PlateHolder plateHolder;
    @XmlElement(name = "Client", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected Client client;
    @XmlElement(name = "Driver", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected Driver driver;
    @XmlElement(name = "Seller", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected Seller seller;
    @XmlElement(name = "Vehicle", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected Vehicle vehicle;
    @XmlElement(name = "RegistrationInformation", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected RegistrationInformation registrationInformation;
    @XmlElement(name = "Insurance", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected Insurance insurance;
    @XmlElement(name = "Delivery", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected Delivery delivery;

    /**
     * Gets the value of the meta property.
     * 
     * @return
     *     possible object is
     *     {@link Meta }
     *     
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * Sets the value of the meta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Meta }
     *     
     */
    public void setMeta(Meta value) {
        this.meta = value;
    }

    /**
     * Gets the value of the plateHolder property.
     * 
     * @return
     *     possible object is
     *     {@link PlateHolder }
     *     
     */
    public PlateHolder getPlateHolder() {
        return plateHolder;
    }

    /**
     * Sets the value of the plateHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlateHolder }
     *     
     */
    public void setPlateHolder(PlateHolder value) {
        this.plateHolder = value;
    }

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link Client }
     *     
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link Client }
     *     
     */
    public void setClient(Client value) {
        this.client = value;
    }

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link Driver }
     *     
     */
    public Driver getDriver() {
        return driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Driver }
     *     
     */
    public void setDriver(Driver value) {
        this.driver = value;
    }

    /**
     * Gets the value of the seller property.
     * 
     * @return
     *     possible object is
     *     {@link Seller }
     *     
     */
    public Seller getSeller() {
        return seller;
    }

    /**
     * Sets the value of the seller property.
     * 
     * @param value
     *     allowed object is
     *     {@link Seller }
     *     
     */
    public void setSeller(Seller value) {
        this.seller = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the registrationInformation property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationInformation }
     *     
     */
    public RegistrationInformation getRegistrationInformation() {
        return registrationInformation;
    }

    /**
     * Sets the value of the registrationInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationInformation }
     *     
     */
    public void setRegistrationInformation(RegistrationInformation value) {
        this.registrationInformation = value;
    }

    /**
     * Gets the value of the insurance property.
     * 
     * @return
     *     possible object is
     *     {@link Insurance }
     *     
     */
    public Insurance getInsurance() {
        return insurance;
    }

    /**
     * Sets the value of the insurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Insurance }
     *     
     */
    public void setInsurance(Insurance value) {
        this.insurance = value;
    }

    /**
     * Gets the value of the delivery property.
     * 
     * @return
     *     possible object is
     *     {@link Delivery }
     *     
     */
    public Delivery getDelivery() {
        return delivery;
    }

    /**
     * Sets the value of the delivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Delivery }
     *     
     */
    public void setDelivery(Delivery value) {
        this.delivery = value;
    }

}
