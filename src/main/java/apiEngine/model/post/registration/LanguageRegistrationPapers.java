
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for LanguageRegistrationPapers.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="LanguageRegistrationPapers"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}normalizedString"&gt;
 *     &lt;enumeration value="nl"/&gt;
 *     &lt;enumeration value="fr"/&gt;
 *     &lt;enumeration value="de"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LanguageRegistrationPapers", namespace = "http://be/renta/ordering/communication/registrationimport")
@XmlEnum
public enum LanguageRegistrationPapers {

    @XmlEnumValue("nl")
    NL("nl"),
    @XmlEnumValue("fr")
    FR("fr"),
    @XmlEnumValue("de")
    DE("de");
    private final String value;

    LanguageRegistrationPapers(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LanguageRegistrationPapers fromValue(String v) {
        for (LanguageRegistrationPapers c: LanguageRegistrationPapers.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
