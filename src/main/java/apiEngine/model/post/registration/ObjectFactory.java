
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the apiEngine.model.post.registration package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: apiEngine.model.post.registration
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OrderImports }
     * 
     */
    public OrderImports createOrderImports() {
        return new OrderImports();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link BikeOrder }
     * 
     */
    public BikeOrder createBikeOrder() {
        return new BikeOrder();
    }

    /**
     * Create an instance of {@link Discount }
     * 
     */
    public Discount createDiscount() {
        return new Discount();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Translatable }
     * 
     */
    public Translatable createTranslatable() {
        return new Translatable();
    }

    /**
     * Create an instance of {@link Bike }
     * 
     */
    public Bike createBike() {
        return new Bike();
    }

    /**
     * Create an instance of {@link Insurance }
     * 
     */
    public Insurance createInsurance() {
        return new Insurance();
    }

    /**
     * Create an instance of {@link TyreFitter }
     * 
     */
    public TyreFitter createTyreFitter() {
        return new TyreFitter();
    }

    /**
     * Create an instance of {@link Meta }
     * 
     */
    public Meta createMeta() {
        return new Meta();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link Driver }
     * 
     */
    public Driver createDriver() {
        return new Driver();
    }

    /**
     * Create an instance of {@link Vehicle }
     * 
     */
    public Vehicle createVehicle() {
        return new Vehicle();
    }

    /**
     * Create an instance of {@link Pricing }
     * 
     */
    public Pricing createPricing() {
        return new Pricing();
    }

    /**
     * Create an instance of {@link Options }
     * 
     */
    public Options createOptions() {
        return new Options();
    }

    /**
     * Create an instance of {@link Option }
     * 
     */
    public Option createOption() {
        return new Option();
    }

    /**
     * Create an instance of {@link Promotions }
     * 
     */
    public Promotions createPromotions() {
        return new Promotions();
    }

    /**
     * Create an instance of {@link Promotion }
     * 
     */
    public Promotion createPromotion() {
        return new Promotion();
    }

    /**
     * Create an instance of {@link Delivery }
     * 
     */
    public Delivery createDelivery() {
        return new Delivery();
    }

    /**
     * Create an instance of {@link RegistrationInfo }
     * 
     */
    public RegistrationInfo createRegistrationInfo() {
        return new RegistrationInfo();
    }

    /**
     * Create an instance of {@link Buyback }
     * 
     */
    public Buyback createBuyback() {
        return new Buyback();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link TransferredLicensePlate }
     * 
     */
    public TransferredLicensePlate createTransferredLicensePlate() {
        return new TransferredLicensePlate();
    }

}
