
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for LeasingType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="LeasingType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="OPERATIONAL_LEASE"/&gt;
 *     &lt;enumeration value="FINANCIAL_LEASE"/&gt;
 *     &lt;enumeration value="RENTAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LeasingType", namespace = "http://be/renta/ordering/communication/orderimport")
@XmlEnum
public enum LeasingType {

    OPERATIONAL_LEASE,
    FINANCIAL_LEASE,
    RENTAL;

    public String value() {
        return name();
    }

    public static LeasingType fromValue(String v) {
        return valueOf(v);
    }

}
