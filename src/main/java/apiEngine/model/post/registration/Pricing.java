
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for Pricing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Pricing"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="TotalPrice" type="{http://be/renta/ordering/communication/orderimport}Amount"/&gt;
 *         &lt;element name="ListPrice" type="{http://be/renta/ordering/communication/orderimport}NullAmount" minOccurs="0"/&gt;
 *         &lt;element name="FleetDiscount" type="{http://be/renta/ordering/communication/orderimport}Discount" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryCosts" type="{http://be/renta/ordering/communication/orderimport}NullAmount" minOccurs="0"/&gt;
 *         &lt;element name="OtherCosts" type="{http://be/renta/ordering/communication/orderimport}NullAmount" minOccurs="0"/&gt;
 *         &lt;element name="Options" type="{http://be/renta/ordering/communication/orderimport}Options" minOccurs="0"/&gt;
 *         &lt;element name="Promotions" type="{http://be/renta/ordering/communication/orderimport}Promotions" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pricing", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Pricing {

    @XmlElement(name = "TotalPrice", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected String totalPrice;
    @XmlElement(name = "ListPrice", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String listPrice;
    @XmlElement(name = "FleetDiscount", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Discount fleetDiscount;
    @XmlElement(name = "DeliveryCosts", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String deliveryCosts;
    @XmlElement(name = "OtherCosts", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String otherCosts;
    @XmlElement(name = "Options", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Options options;
    @XmlElement(name = "Promotions", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Promotions promotions;

    /**
     * Gets the value of the totalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the value of the totalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPrice(String value) {
        this.totalPrice = value;
    }

    /**
     * Gets the value of the listPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListPrice() {
        return listPrice;
    }

    /**
     * Sets the value of the listPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListPrice(String value) {
        this.listPrice = value;
    }

    /**
     * Gets the value of the fleetDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link Discount }
     *     
     */
    public Discount getFleetDiscount() {
        return fleetDiscount;
    }

    /**
     * Sets the value of the fleetDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Discount }
     *     
     */
    public void setFleetDiscount(Discount value) {
        this.fleetDiscount = value;
    }

    /**
     * Gets the value of the deliveryCosts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryCosts() {
        return deliveryCosts;
    }

    /**
     * Sets the value of the deliveryCosts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryCosts(String value) {
        this.deliveryCosts = value;
    }

    /**
     * Gets the value of the otherCosts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherCosts() {
        return otherCosts;
    }

    /**
     * Sets the value of the otherCosts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherCosts(String value) {
        this.otherCosts = value;
    }

    /**
     * Gets the value of the options property.
     * 
     * @return
     *     possible object is
     *     {@link Options }
     *     
     */
    public Options getOptions() {
        return options;
    }

    /**
     * Sets the value of the options property.
     * 
     * @param value
     *     allowed object is
     *     {@link Options }
     *     
     */
    public void setOptions(Options value) {
        this.options = value;
    }

    /**
     * Gets the value of the promotions property.
     * 
     * @return
     *     possible object is
     *     {@link Promotions }
     *     
     */
    public Promotions getPromotions() {
        return promotions;
    }

    /**
     * Sets the value of the promotions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Promotions }
     *     
     */
    public void setPromotions(Promotions value) {
        this.promotions = value;
    }

}
