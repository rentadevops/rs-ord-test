
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for Option complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Option"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="OptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OptionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Name" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="ListPrice" type="{http://be/renta/ordering/communication/orderimport}NullAmount" minOccurs="0"/&gt;
 *         &lt;element name="Discount" type="{http://be/renta/ordering/communication/orderimport}Discount" minOccurs="0"/&gt;
 *         &lt;element name="DealerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DealerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApplicableVatPercentage" type="{http://be/renta/ordering/communication/orderimport}NullPercentage" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Option", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Option {

    @XmlElement(name = "OptionCode", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String optionCode;
    @XmlElement(name = "OptionType", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String optionType;
    @XmlElement(name = "Name", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable name;
    @XmlElement(name = "Description", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable description;
    @XmlElement(name = "ListPrice", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String listPrice;
    @XmlElement(name = "Discount", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Discount discount;
    @XmlElement(name = "DealerCode", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String dealerCode;
    @XmlElement(name = "DealerName", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String dealerName;
    @XmlElement(name = "ApplicableVatPercentage", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String applicableVatPercentage;

    /**
     * Gets the value of the optionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptionCode() {
        return optionCode;
    }

    /**
     * Sets the value of the optionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptionCode(String value) {
        this.optionCode = value;
    }

    /**
     * Gets the value of the optionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptionType() {
        return optionType;
    }

    /**
     * Sets the value of the optionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptionType(String value) {
        this.optionType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setName(Translatable value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setDescription(Translatable value) {
        this.description = value;
    }

    /**
     * Gets the value of the listPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListPrice() {
        return listPrice;
    }

    /**
     * Sets the value of the listPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListPrice(String value) {
        this.listPrice = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link Discount }
     *     
     */
    public Discount getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Discount }
     *     
     */
    public void setDiscount(Discount value) {
        this.discount = value;
    }

    /**
     * Gets the value of the dealerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * Sets the value of the dealerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCode(String value) {
        this.dealerCode = value;
    }

    /**
     * Gets the value of the dealerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerName() {
        return dealerName;
    }

    /**
     * Sets the value of the dealerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerName(String value) {
        this.dealerName = value;
    }

    /**
     * Gets the value of the applicableVatPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicableVatPercentage() {
        return applicableVatPercentage;
    }

    /**
     * Sets the value of the applicableVatPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicableVatPercentage(String value) {
        this.applicableVatPercentage = value;
    }

}
