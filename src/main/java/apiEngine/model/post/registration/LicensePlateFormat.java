
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for LicensePlateFormat.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="LicensePlateFormat"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}normalizedString"&gt;
 *     &lt;enumeration value="R"/&gt;
 *     &lt;enumeration value="S"/&gt;
 *     &lt;enumeration value="M"/&gt;
 *     &lt;enumeration value="P"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LicensePlateFormat", namespace = "http://be/renta/ordering/communication/registrationimport")
@XmlEnum
public enum LicensePlateFormat {

    R,
    S,
    M,
    P;

    public String value() {
        return name();
    }

    public static LicensePlateFormat fromValue(String v) {
        return valueOf(v);
    }

}
