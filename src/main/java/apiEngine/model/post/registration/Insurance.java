
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for Insurance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Insurance"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Email" type="{http://be/renta/ordering/communication/orderimport}Email" minOccurs="0"/&gt;
 *         &lt;element name="InsuredValuePrice" type="{http://be/renta/ordering/communication/orderimport}NullAmount"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Insurance", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Insurance {

    @XmlElement(name = "Name", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String name;
    @XmlElement(name = "Email", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String email;
    @XmlElement(name = "InsuredValuePrice", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected String insuredValuePrice;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the insuredValuePrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredValuePrice() {
        return insuredValuePrice;
    }

    /**
     * Sets the value of the insuredValuePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredValuePrice(String value) {
        this.insuredValuePrice = value;
    }

}
