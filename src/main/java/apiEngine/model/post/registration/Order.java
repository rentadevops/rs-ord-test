
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for Order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Order"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Meta" type="{http://be/renta/ordering/communication/orderimport}Meta"/&gt;
 *         &lt;element name="Client" type="{http://be/renta/ordering/communication/orderimport}Client" minOccurs="0"/&gt;
 *         &lt;element name="Driver" type="{http://be/renta/ordering/communication/orderimport}Driver" minOccurs="0"/&gt;
 *         &lt;element name="Vehicle" type="{http://be/renta/ordering/communication/orderimport}Vehicle" minOccurs="0"/&gt;
 *         &lt;element name="Pricing" type="{http://be/renta/ordering/communication/orderimport}Pricing"/&gt;
 *         &lt;element name="Delivery" type="{http://be/renta/ordering/communication/orderimport}Delivery" minOccurs="0"/&gt;
 *         &lt;element name="Buyback" type="{http://be/renta/ordering/communication/orderimport}Buyback" minOccurs="0"/&gt;
 *         &lt;element name="Insurance" type="{http://be/renta/ordering/communication/orderimport}Insurance" minOccurs="0"/&gt;
 *         &lt;element name="TyreFitter" type="{http://be/renta/ordering/communication/orderimport}TyreFitter" minOccurs="0"/&gt;
 *         &lt;element name="RegistrationInfo" type="{http://be/renta/ordering/communication/orderimport}RegistrationInfo" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Order {

    @XmlElement(name = "Meta", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected Meta meta;
    @XmlElement(name = "Client", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Client client;
    @XmlElement(name = "Driver", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Driver driver;
    @XmlElement(name = "Vehicle", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Vehicle vehicle;
    @XmlElement(name = "Pricing", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected Pricing pricing;
    @XmlElement(name = "Delivery", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Delivery delivery;
    @XmlElement(name = "Buyback", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Buyback buyback;
    @XmlElement(name = "Insurance", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Insurance insurance;
    @XmlElement(name = "TyreFitter", namespace = "http://be/renta/ordering/communication/orderimport")
    protected TyreFitter tyreFitter;
    @XmlElement(name = "RegistrationInfo", namespace = "http://be/renta/ordering/communication/orderimport")
    protected RegistrationInfo registrationInfo;

    /**
     * Gets the value of the meta property.
     * 
     * @return
     *     possible object is
     *     {@link Meta }
     *     
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * Sets the value of the meta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Meta }
     *     
     */
    public void setMeta(Meta value) {
        this.meta = value;
    }

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link Client }
     *     
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link Client }
     *     
     */
    public void setClient(Client value) {
        this.client = value;
    }

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link Driver }
     *     
     */
    public Driver getDriver() {
        return driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Driver }
     *     
     */
    public void setDriver(Driver value) {
        this.driver = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the pricing property.
     * 
     * @return
     *     possible object is
     *     {@link Pricing }
     *     
     */
    public Pricing getPricing() {
        return pricing;
    }

    /**
     * Sets the value of the pricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pricing }
     *     
     */
    public void setPricing(Pricing value) {
        this.pricing = value;
    }

    /**
     * Gets the value of the delivery property.
     * 
     * @return
     *     possible object is
     *     {@link Delivery }
     *     
     */
    public Delivery getDelivery() {
        return delivery;
    }

    /**
     * Sets the value of the delivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Delivery }
     *     
     */
    public void setDelivery(Delivery value) {
        this.delivery = value;
    }

    /**
     * Gets the value of the buyback property.
     * 
     * @return
     *     possible object is
     *     {@link Buyback }
     *     
     */
    public Buyback getBuyback() {
        return buyback;
    }

    /**
     * Sets the value of the buyback property.
     * 
     * @param value
     *     allowed object is
     *     {@link Buyback }
     *     
     */
    public void setBuyback(Buyback value) {
        this.buyback = value;
    }

    /**
     * Gets the value of the insurance property.
     * 
     * @return
     *     possible object is
     *     {@link Insurance }
     *     
     */
    public Insurance getInsurance() {
        return insurance;
    }

    /**
     * Sets the value of the insurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Insurance }
     *     
     */
    public void setInsurance(Insurance value) {
        this.insurance = value;
    }

    /**
     * Gets the value of the tyreFitter property.
     * 
     * @return
     *     possible object is
     *     {@link TyreFitter }
     *     
     */
    public TyreFitter getTyreFitter() {
        return tyreFitter;
    }

    /**
     * Sets the value of the tyreFitter property.
     * 
     * @param value
     *     allowed object is
     *     {@link TyreFitter }
     *     
     */
    public void setTyreFitter(TyreFitter value) {
        this.tyreFitter = value;
    }

    /**
     * Gets the value of the registrationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationInfo }
     *     
     */
    public RegistrationInfo getRegistrationInfo() {
        return registrationInfo;
    }

    /**
     * Sets the value of the registrationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationInfo }
     *     
     */
    public void setRegistrationInfo(RegistrationInfo value) {
        this.registrationInfo = value;
    }

}
