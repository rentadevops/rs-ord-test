
package apiEngine.model.post.registration;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Header" type="{http://be/renta/ordering/communication/registrationimport}Header"/&gt;
 *         &lt;element name="RegisterVehicle" type="{http://be/renta/ordering/communication/registrationimport}RegisterVehicle" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CancelRegistration" type="{http://be/renta/ordering/communication/registrationimport}CancelRegistration" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "registerVehicle",
    "cancelRegistration"
})
@XmlRootElement(name = "RegistrationImports", namespace = "http://be/renta/ordering/communication/registrationimport")
public class RegistrationImports {

    @XmlElement(name = "Header", namespace = "http://be/renta/ordering/communication/registrationimport", required = true)
    protected Header header;
    @XmlElement(name = "RegisterVehicle", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected RegisterVehicle registerVehicle;
    @XmlElement(name = "CancelRegistration", namespace = "http://be/renta/ordering/communication/registrationimport")
    protected List<CancelRegistration> cancelRegistration;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link Header }
     *     
     */
    public Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link Header }
     *     
     */
    public void setHeader(Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the registerVehicle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the registerVehicle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegisterVehicle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegisterVehicle }
     * 
     * 
     */
    public RegisterVehicle getRegisterVehicle() {
        if (registerVehicle == null) {
            registerVehicle = new RegisterVehicle();
        }
        return this.registerVehicle;
    }

    public void setRegisterVehicle(RegisterVehicle registerVehicle) {
        this.registerVehicle = registerVehicle;
    }

    /**
     * Gets the value of the cancelRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the cancelRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCancelRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CancelRegistration }
     * 
     * 
     */
    public List<CancelRegistration> getCancelRegistration() {
        if (cancelRegistration == null) {
            cancelRegistration = new ArrayList<CancelRegistration>();
        }
        return this.cancelRegistration;
    }

}
