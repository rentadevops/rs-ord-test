
package apiEngine.model.post.registration;

import java.util.HashMap;
import java.util.Map;
import javax.xml.namespace.QName;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for Vehicle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Vehicle"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Brand" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Model" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="Version" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="CylinderContent" type="{http://be/renta/ordering/communication/orderimport}NullInt" minOccurs="0"/&gt;
 *         &lt;element name="Power" type="{http://be/renta/ordering/communication/orderimport}NullInt" minOccurs="0"/&gt;
 *         &lt;element name="Weight" type="{http://be/renta/ordering/communication/orderimport}NullInt" minOccurs="0"/&gt;
 *         &lt;element name="ExteriorColor" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="InteriorColor" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="LanguagePapers" type="{http://be/renta/ordering/communication/orderimport}LanguageCode" minOccurs="0"/&gt;
 *         &lt;element name="TyreDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StockCar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="LicensePlateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Co2Emissions" type="{http://be/renta/ordering/communication/orderimport}NullAmount" minOccurs="0"/&gt;
 *         &lt;element name="Identification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransferredLicensePlate" type="{http://be/renta/ordering/communication/orderimport}TransferredLicensePlate" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *       &lt;anyAttribute/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vehicle", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Vehicle {

    @XmlElement(name = "Brand", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String brand;
    @XmlElement(name = "Model", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable model;
    @XmlElement(name = "Version", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable version;
    @XmlElement(name = "CylinderContent", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String cylinderContent;
    @XmlElement(name = "Power", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String power;
    @XmlElement(name = "Weight", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String weight;
    @XmlElement(name = "ExteriorColor", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable exteriorColor;
    @XmlElement(name = "InteriorColor", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable interiorColor;
    @XmlElement(name = "LanguagePapers", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String languagePapers;
    @XmlElement(name = "TyreDescription", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String tyreDescription;
    @XmlElement(name = "StockCar", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Boolean stockCar;
    @XmlElement(name = "LicensePlateNumber", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String licensePlateNumber;
    @XmlElement(name = "Co2Emissions", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String co2Emissions;
    @XmlElement(name = "Identification", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String identification;
    @XmlElement(name = "TransferredLicensePlate", namespace = "http://be/renta/ordering/communication/orderimport")
    protected TransferredLicensePlate transferredLicensePlate;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setModel(Translatable value) {
        this.model = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setVersion(Translatable value) {
        this.version = value;
    }

    /**
     * Gets the value of the cylinderContent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCylinderContent() {
        return cylinderContent;
    }

    /**
     * Sets the value of the cylinderContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCylinderContent(String value) {
        this.cylinderContent = value;
    }

    /**
     * Gets the value of the power property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPower() {
        return power;
    }

    /**
     * Sets the value of the power property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPower(String value) {
        this.power = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeight(String value) {
        this.weight = value;
    }

    /**
     * Gets the value of the exteriorColor property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getExteriorColor() {
        return exteriorColor;
    }

    /**
     * Sets the value of the exteriorColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setExteriorColor(Translatable value) {
        this.exteriorColor = value;
    }

    /**
     * Gets the value of the interiorColor property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getInteriorColor() {
        return interiorColor;
    }

    /**
     * Sets the value of the interiorColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setInteriorColor(Translatable value) {
        this.interiorColor = value;
    }

    /**
     * Gets the value of the languagePapers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguagePapers() {
        return languagePapers;
    }

    /**
     * Sets the value of the languagePapers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguagePapers(String value) {
        this.languagePapers = value;
    }

    /**
     * Gets the value of the tyreDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTyreDescription() {
        return tyreDescription;
    }

    /**
     * Sets the value of the tyreDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTyreDescription(String value) {
        this.tyreDescription = value;
    }

    /**
     * Gets the value of the stockCar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStockCar() {
        return stockCar;
    }

    /**
     * Sets the value of the stockCar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStockCar(Boolean value) {
        this.stockCar = value;
    }

    /**
     * Gets the value of the licensePlateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    /**
     * Sets the value of the licensePlateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlateNumber(String value) {
        this.licensePlateNumber = value;
    }

    /**
     * Gets the value of the co2Emissions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCo2Emissions() {
        return co2Emissions;
    }

    /**
     * Sets the value of the co2Emissions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCo2Emissions(String value) {
        this.co2Emissions = value;
    }

    /**
     * Gets the value of the identification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentification() {
        return identification;
    }

    /**
     * Sets the value of the identification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentification(String value) {
        this.identification = value;
    }

    /**
     * Gets the value of the transferredLicensePlate property.
     * 
     * @return
     *     possible object is
     *     {@link TransferredLicensePlate }
     *     
     */
    public TransferredLicensePlate getTransferredLicensePlate() {
        return transferredLicensePlate;
    }

    /**
     * Sets the value of the transferredLicensePlate property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferredLicensePlate }
     *     
     */
    public void setTransferredLicensePlate(TransferredLicensePlate value) {
        this.transferredLicensePlate = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
