
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for Buyback complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Buyback"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address" type="{http://be/renta/ordering/communication/orderimport}Address" minOccurs="0"/&gt;
 *         &lt;element name="Amount" type="{http://be/renta/ordering/communication/orderimport}NullAmount" minOccurs="0"/&gt;
 *         &lt;element name="ContractDuration" type="{http://be/renta/ordering/communication/orderimport}NullInt" minOccurs="0"/&gt;
 *         &lt;element name="Kilometers" type="{http://be/renta/ordering/communication/orderimport}NullInt" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Buyback", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Buyback {

    @XmlElement(name = "Name", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String name;
    @XmlElement(name = "Address", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Address address;
    @XmlElement(name = "Amount", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String amount;
    @XmlElement(name = "ContractDuration", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String contractDuration;
    @XmlElement(name = "Kilometers", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String kilometers;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Gets the value of the contractDuration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractDuration() {
        return contractDuration;
    }

    /**
     * Sets the value of the contractDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractDuration(String value) {
        this.contractDuration = value;
    }

    /**
     * Gets the value of the kilometers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKilometers() {
        return kilometers;
    }

    /**
     * Sets the value of the kilometers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKilometers(String value) {
        this.kilometers = value;
    }

}
