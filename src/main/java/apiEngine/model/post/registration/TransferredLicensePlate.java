
package apiEngine.model.post.registration;

import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for TransferredLicensePlate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferredLicensePlate"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Transferred" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="LicensePlateType" type="{http://be/renta/ordering/communication/orderimport}LicensePlateType" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransferredLicensePlate", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class TransferredLicensePlate {

    @XmlElement(name = "Transferred", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Boolean transferred;
    @XmlElement(name = "LicensePlateType", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlSchemaType(name = "string")
    protected LicensePlateType licensePlateType;

    /**
     * Gets the value of the transferred property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransferred() {
        return transferred;
    }

    /**
     * Sets the value of the transferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransferred(Boolean value) {
        this.transferred = value;
    }

    /**
     * Gets the value of the licensePlateType property.
     * 
     * @return
     *     possible object is
     *     {@link LicensePlateType }
     *     
     */
    public LicensePlateType getLicensePlateType() {
        return licensePlateType;
    }

    /**
     * Sets the value of the licensePlateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicensePlateType }
     *     
     */
    public void setLicensePlateType(LicensePlateType value) {
        this.licensePlateType = value;
    }

}
