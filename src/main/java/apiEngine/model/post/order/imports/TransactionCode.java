package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlEnum;
//import javax.xml.bind.annotation.XmlEnumValue;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

@XmlType(name = "TransactionCode")
@XmlEnum
public enum TransactionCode {

	@XmlEnumValue("CREATE")
	CREATE("CREATE"),

	@XmlEnumValue("UPDATE")
	UPDATE("UPDATE"),

	@XmlEnumValue("CONFIRM")
	CONFIRM("CONFIRM"),

	@XmlEnumValue("CANCEL")
	CANCEL("CANCEL"),

	@XmlEnumValue("LICENSE_PLATE")
	LICENSE_PLATE("LICENSE_PLATE");

	private final String value;

	TransactionCode(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static TransactionCode fromValue(String v) {
		for (TransactionCode c : TransactionCode.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
