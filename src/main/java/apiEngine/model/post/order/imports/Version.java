package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Version {
	
	@XmlElement(name="Dutch")
	private String Dutch;

	public Version() {
		
	}
	
	public Version(String Dutch) {
		super();
		this.Dutch = Dutch;
		}
	
	 // Getter Methods 

	 public String getDutch() {
	  return Dutch;
	 }

	 // Setter Methods 

	 public void setDutch(String Dutch) {
	  this.Dutch = Dutch;
	 }

}
