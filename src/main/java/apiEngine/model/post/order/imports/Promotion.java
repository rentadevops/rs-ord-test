package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.*;

@XmlRootElement(name="Promotion")
@XmlAccessorType(XmlAccessType.FIELD)
public class Promotion {
	
	@XmlElement(name="Name", required=false)
	private Name Name;
	@XmlElement(name="Description", required=false)
	private Description Description;
	@XmlElement(name="Discount", required=false)
	private Discount Discount;

	public Promotion() {
		
	}

	public Promotion(Name Name, Description Description, Discount Discount) {
		super();
		this.Name = Name;
		this.Description = Description;
		this.Discount = Discount;

		}
	
	 // Getter Methods 

	 public Name getName() {
	  return Name;
	 }

	 public Description getDescription() {
	  return Description;
	 }

	 public Discount getDiscount() {
	  return Discount;
	 }

	 // Setter Methods 

	 public void setName(Name Name) {
	  this.Name = Name;
	 }

	 public void setDescription(Description Description) {
	  this.Description = Description;
	 }

	 public void setDiscount(Discount Discount) {
	  this.Discount = Discount;
	 }

}
