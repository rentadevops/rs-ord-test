package apiEngine.model.post.order.imports;

import java.util.List;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Promotions {
	
	@XmlElement(name = "Promotion")
	private List<Promotion> promotions = null;
	
	//Promotion Promotion;
	
public Promotions() {
		
	}

public Promotions(Promotion Promotion, List<Promotion> Promotions) {
	super();
	this.promotions = Promotions;
	}


	 // Getter Methods 

	 public List<Promotion> getPromotions() {
	  return promotions;
	 }

	 // Setter Methods 

	 public void setPromotions(List<Promotion> promotions) {
	  this.promotions = promotions;
	 }

}
