package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Delivery {
	
	@XmlElement(name="DesiredDate", required=false)
	private String DesiredDate;
	@XmlElement(name="LocationName", required=false)
	private String LocationName;
	@XmlElement(name="Address", required=false)
	private Address Address;
	@XmlElement(name="Contact", required=false)
	private Contact Contact;
	@XmlElement(name="LocationLanguageCode", required=false)
	private String LocationLanguageCode;
	@XmlElement(name="ContactPerson", required=false)
	private String ContactPerson;
	@XmlElement(name="LocationVAT", required=false)
	private String LocationVAT;
	
	public Delivery() {
		
	}
	
	public Delivery(String DesiredDate, String LocationName, Address Address, Contact Contact, String LocationLanguageCode, String ContactPerson, String LocationVAT) {
		super();
		this.DesiredDate = DesiredDate;
		this.LocationName = LocationName;
		this.Address = Address;
		this.Contact = Contact;
		this.LocationLanguageCode = LocationLanguageCode;
		this.ContactPerson = ContactPerson;
		this.LocationVAT = LocationVAT;

		}


	 // Getter Methods 

	 public String getDesiredDate() {
	  return DesiredDate;
	 }

	 public String getLocationName() {
	  return LocationName;
	 }

	 public Address getAddress() {
	  return Address;
	 }

	 public Contact getContact() {
	  return Contact;
	 }

	 public String getLocationLanguageCode() {
	  return LocationLanguageCode;
	 }

	 public String getContactPerson() {
	  return ContactPerson;
	 }

	 public String getLocationVAT() {
	  return LocationVAT;
	 }

	 // Setter Methods 

	 public void setDesiredDate(String DesiredDate) {
	  this.DesiredDate = DesiredDate;
	 }

	 public void setLocationName(String LocationName) {
	  this.LocationName = LocationName;
	 }

	 public void setAddress(Address AddressObject) {
	  this.Address = AddressObject;
	 }

	 public void setContact(Contact ContactObject) {
	  this.Contact = ContactObject;
	 }

	 public void setLocationLanguageCode(String LocationLanguageCode) {
	  this.LocationLanguageCode = LocationLanguageCode;
	 }

	 public void setContactPerson(String ContactPerson) {
	  this.ContactPerson = ContactPerson;
	 }

	 public void setLocationVAT(String LocationVAT) {
	  this.LocationVAT = LocationVAT;
	 }

}
