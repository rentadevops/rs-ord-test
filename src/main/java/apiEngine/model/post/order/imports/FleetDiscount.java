package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class FleetDiscount {

	@XmlElement(name="Amount")
	private String Amount;
	@XmlElement(name="Percentage")
	private String Percentage;

	public FleetDiscount() {
		
	}

	public FleetDiscount(String Amount, String Percentage) {
		super();
		this.Amount = Amount;
		this.Percentage = Percentage;
		}
	
	 // Getter Methods 

	 public String getAmount() {
	  return Amount;
	 }

	 public String getPercentage() {
	  return Percentage;
	 }

	 // Setter Methods 

	 public void setAmount(String Amount) {
	  this.Amount = Amount;
	 }

	 public void setPercentage(String Percentage) {
	  this.Percentage = Percentage;
	 }
	
}
