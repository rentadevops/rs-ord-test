package apiEngine.model.post.order.imports.migration;

import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Version {
	
	@XmlElement(name="Dutch")
	private String Dutch;

	public Version() {
		
	}
	
	public Version(String Dutch) {
		super();
		this.Dutch = Dutch;
		}
	
	 // Getter Methods 

	 public String getDutch() {
	  return Dutch;
	 }

	 // Setter Methods 

	 public void setDutch(String Dutch) {
	  this.Dutch = Dutch;
	 }

}
