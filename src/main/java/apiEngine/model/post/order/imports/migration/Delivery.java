
package apiEngine.model.post.order.imports.migration;


//import javax.xml.bind.annotation.*;
//import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
//import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * ExpectedDeliveryDate is only applicable to MIGRATE transactions.
 * 
 * <p>Java class for Delivery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Delivery"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="DesiredDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="ExpectedDeliveryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address" type="{http://be/renta/ordering/communication/orderimport}Address" minOccurs="0"/&gt;
 *         &lt;element name="Contact" type="{http://be/renta/ordering/communication/orderimport}Contact" minOccurs="0"/&gt;
 *         &lt;element name="LocationLanguageCode" type="{http://be/renta/ordering/communication/orderimport}LanguageCode" minOccurs="0"/&gt;
 *         &lt;element name="ContactPerson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationVAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Delivery", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Delivery {

    @XmlElement(name = "DesiredDate", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlSchemaType(name = "date")
    private String DesiredDate;
    @XmlElement(name = "ExpectedDeliveryDate", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlSchemaType(name = "date")
    private String ExpectedDeliveryDate;
    @XmlElement(name = "LocationName", namespace = "http://be/renta/ordering/communication/orderimport")
    private String LocationName;
    @XmlElement(name = "Address", namespace = "http://be/renta/ordering/communication/orderimport")
    private Address Address;
    @XmlElement(name = "Contact", namespace = "http://be/renta/ordering/communication/orderimport")
    private Contact Contact;
    @XmlElement(name = "LocationLanguageCode", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    private String LocationLanguageCode;
    @XmlElement(name = "ContactPerson", namespace = "http://be/renta/ordering/communication/orderimport")
    private String ContactPerson;
    @XmlElement(name = "LocationVAT", namespace = "http://be/renta/ordering/communication/orderimport")
    private String LocationVAT;

    /**
     * Gets the value of the desiredDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesiredDate() {
        return DesiredDate;
    }

    /**
     * Sets the value of the desiredDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesiredDate(String value) {
        this.DesiredDate = value;
    }

    /**
     * Gets the value of the expectedDeliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedDeliveryDate() {
        return ExpectedDeliveryDate;
    }

    /**
     * Sets the value of the expectedDeliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedDeliveryDate(String value) {
        this.ExpectedDeliveryDate = value;
    }

    /**
     * Gets the value of the locationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return LocationName;
    }

    /**
     * Sets the value of the locationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.LocationName = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return Address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.Address = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return Contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.Contact = value;
    }

    /**
     * Gets the value of the locationLanguageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationLanguageCode() {
        return LocationLanguageCode;
    }

    /**
     * Sets the value of the locationLanguageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationLanguageCode(String value) {
        this.LocationLanguageCode = value;
    }

    /**
     * Gets the value of the contactPerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPerson() {
        return ContactPerson;
    }

    /**
     * Sets the value of the contactPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPerson(String value) {
        this.ContactPerson = value;
    }

    /**
     * Gets the value of the locationVAT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationVAT() {
        return LocationVAT;
    }

    /**
     * Sets the value of the locationVAT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationVAT(String value) {
        this.LocationVAT = value;
    }

}
