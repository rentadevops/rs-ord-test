package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import apiEngine.model.post.order.imports.migration.OrderStatus;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Meta {
	
	@XmlElement(name="TransactionCode", required=true) 
	private TransactionCode TransactionCode;
	@XmlElement(name="OrderStatus", required=true)
	private OrderStatus OrderStatus;
	@XmlElement(name="DossierNumber", required=true)
	 private String DossierNumber;
	@XmlElement(name="LeasingCompanyRSNumber", required=true)
	 private String LeasingCompanyRSNumber;
	@XmlElement(name="DealerRSNumber", required=true)
	 private String DealerRSNumber;
	@XmlElement(name="UserEmail", required=true)
	 private String UserEmail;
	@XmlElement(name="VersionNumber", required=false)
	 private String VersionNumber;
	@XmlElement(name="TrackingNumber", required=false)
	 private String TrackingNumber;
	@XmlElement(name="Comment", required=false)
	 private String Comment;
	@XmlElement(name="LogoReferenceCode", required=false)
	 private String LogoReferenceCode;
	@XmlElement(name="DateFirstRegistration", required=false)
	 private String DateFirstRegistration;
	@XmlElement(name="LeasingType", required=false)
	 private LeasingType LeasingType;
	@XmlElement(name="ConfigurationIdDealer", required=false)
	 private String ConfigurationIdDealer;
	@XmlElement(name="QuoteReference", required=false)
	 private String QuoteReference;

	public Meta() {
		
	}
	
	public Meta(TransactionCode TransactionCode, OrderStatus OrderStatus, String DossierNumber, String LeasingCompanyRSNumber, String DealerRSNumber, String UserEmail, String VersionNumber, String TrackingNumber, String Comment,
			String LogoReferenceCode, String DateFirstRegistration, LeasingType LeasingType, String ConfigurationIdDealer, String QuoteReference) {
		super();
		this.TransactionCode = TransactionCode;
		this.OrderStatus = OrderStatus;
		this.DossierNumber = DossierNumber;
		this.LeasingCompanyRSNumber = LeasingCompanyRSNumber;
		this.DealerRSNumber = DealerRSNumber;
		this.UserEmail = UserEmail;
		this.VersionNumber = VersionNumber;
		this.TrackingNumber = TrackingNumber;
		this.Comment = Comment;
		this.LogoReferenceCode = LogoReferenceCode;
		this.DateFirstRegistration = DateFirstRegistration;
		this.LeasingType = LeasingType;
		this.ConfigurationIdDealer = ConfigurationIdDealer;
		this.QuoteReference = QuoteReference;
		}
	
	 // Getter Methods 

	 public String getVersionNumber() {
		return VersionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		VersionNumber = versionNumber;
	}

	public OrderStatus getOrderStatus() {
		return OrderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		OrderStatus = orderStatus;
	}

	public String getLogoReferenceCode() {
		return LogoReferenceCode;
	}

	public void setLogoReferenceCode(String logoReferenceCode) {
		LogoReferenceCode = logoReferenceCode;
	}

	public String getDateFirstRegistration() {
		return DateFirstRegistration;
	}

	public void setDateFirstRegistration(String dateFirstRegistration) {
		DateFirstRegistration = dateFirstRegistration;
	}

	public String getConfigurationIdDealer() {
		return ConfigurationIdDealer;
	}

	public void setConfigurationIdDealer(String configurationIdDealer) {
		ConfigurationIdDealer = configurationIdDealer;
	}

	public String getQuoteReference() {
		return QuoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		QuoteReference = quoteReference;
	}

	public LeasingType getLeasingType() {
		return LeasingType;
	}

	public TransactionCode getTransactionCode() {
	  return TransactionCode;
	 }

	 public String getDossierNumber() {
	  return DossierNumber;
	 }

	 public String getLeasingCompanyRSNumber() {
	  return LeasingCompanyRSNumber;
	 }

	 public String getDealerRSNumber() {
	  return DealerRSNumber;
	 }

	 public String getUserEmail() {
	  return UserEmail;
	 }

	 public String getTrackingNumber() {
	  return TrackingNumber;
	 }

	 public String getComment() {
	  return Comment;
	 }

	 // Setter Methods 

	 public void setTransactionCode(TransactionCode TransactionCode) {
	  this.TransactionCode = TransactionCode;
	 }

	 public void setDossierNumber(String DossierNumber) {
	  this.DossierNumber = DossierNumber;
	 }

	 public void setLeasingCompanyRSNumber(String LeasingCompanyRSNumber) {
	  this.LeasingCompanyRSNumber = LeasingCompanyRSNumber;
	 }

	 public void setDealerRSNumber(String DealerRSNumber) {
	  this.DealerRSNumber = DealerRSNumber;
	 }

	 public void setUserEmail(String UserEmail) {
	  this.UserEmail = UserEmail;
	 }

	 public void setTrackingNumber(String TrackingNumber) {
	  this.TrackingNumber = TrackingNumber;
	 }

	 public void setComment(String Comment) {
	  this.Comment = Comment;
	 }
	 
	 public void setLeasingType(LeasingType LeasingType) {
		  this.LeasingType = LeasingType;
		 }

}
