package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Header {
	
	@XmlElement(name="CreatedDate") 
	private String CreatedDate;
	
	public Header() {
		
	}


	public Header(String CreatedDate) {
		super();
		this.CreatedDate = CreatedDate;
		}
	
	 // Getter Methods 

	 public String getCreatedDate() {
	  return CreatedDate;
	 }

	 // Setter Methods 

	 public void setCreatedDate(String CreatedDate) {
	  this.CreatedDate = CreatedDate;
	 }

}
