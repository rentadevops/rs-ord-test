package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Order {

	@XmlElement(name="Meta", required=true)	 
	private Meta Meta;
	@XmlElement(name="Client", required=false)
		 private Client Client;
	@XmlElement(name="Driver", required=false)
		 private Driver Driver;
	@XmlElement(name="Vehicle", required=false)
		 private Vehicle Vehicle;
	@XmlElement(name="Pricing", required=true)
		 private Pricing Pricing;
	@XmlElement(name="Delivery", required=false)
		 private Delivery Delivery;
	@XmlElement(name="Buyback", required=false)
		 private Buyback Buyback;
	@XmlElement(name = "Insurance")
    protected Insurance Insurance;
    @XmlElement(name = "TyreFitter")
    protected TyreFitter tyreFitter;
	@XmlElement(name="RegistrationInfo", required=false)
	 private RegistrationInfo RegistrationInfo;
		 
	//public Order() {
	//}
	
	/*
	 * public Order(Meta Meta, Client Client, Driver Driver, Vehicle Vehicle,
	 * Pricing Pricing, Delivery Delivery, Buyback Buyback, RegistrationInfo
	 * RegistrationInfo) { super(); this.Meta = Meta; this.Client = Client;
	 * this.Driver = Driver; this.Vehicle = Vehicle; this.Pricing = Pricing;
	 * this.Delivery = Delivery; this.Buyback = Buyback; this.RegistrationInfo =
	 * RegistrationInfo; }
	 */


		 // Getter Methods 

		 public Meta getMeta() {
		  return Meta;
		 }

		 public Client getClient() {
		  return Client;
		 }

		 public Driver getDriver() {
		  return Driver;
		 }

		 public Vehicle getVehicle() {
		  return Vehicle;
		 }

		 public Pricing getPricing() {
		  return Pricing;
		 }

		 public Delivery getDelivery() {
		  return Delivery;
		 }

		 public Buyback getBuyback() {
		  return Buyback;
		 }
		 
		 public Insurance getInsurance() {
			  return Insurance;
			 }
		 
		 public RegistrationInfo getRegistrationInfo() {
			 return RegistrationInfo;
		 }

		 // Setter Methods 

		 public void setMeta(Meta Meta) {
		  this.Meta = Meta;
		 }

		 public void setClient(Client Client) {
		  this.Client = Client;
		 }

		 public void setDriver(Driver Driver) {
		  this.Driver = Driver;
		 }

		 public void setVehicle(Vehicle Vehicle) {
		  this.Vehicle = Vehicle;
		 }

		 public void setPricing(Pricing Pricing) {
		  this.Pricing = Pricing;
		 }

		 public void setDelivery(Delivery Delivery) {
		  this.Delivery = Delivery;
		 }

		 public void setBuyback(Buyback Buyback) {
		  this.Buyback = Buyback;
		 }
		 
		 public void setInsurance(Insurance Insurance) {
			  this.Insurance = Insurance;
			 }
		 
		 public void setRegistrationInfo(RegistrationInfo RegistrationInfo) {
			  this.RegistrationInfo = RegistrationInfo;
			 }

}
