package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Option {

	@XmlElement(name = "OptionCode", required = false)
	private String OptionCode;
	@XmlElement(name = "OptionType", required = false)
	private String OptionType;
	@XmlElement(name = "Name", required = false)
	private Name Name;
	@XmlElement(name = "Description", required = false)
	private Description Description;
	@XmlElement(name = "ListPrice", required = false)
	private String ListPrice;
	@XmlElement(name = "Discount", required = false)
	private Discount Discount;
	@XmlElement(name = "DealerCode", required = false)
	private String DealerCode;
	@XmlElement(name = "DealerName", required = false)
	private String DealerName;
	@XmlElement(name = "ApplicableVatPercentage", required = false)
	private String ApplicableVatPercentage;

	public Option() {

	}

	public Option(String OptionCode, String OptionType, Name Name, Description Description, String ListPrice,
			Discount Discount, String DealerCode, String DealerName, String ApplicableVatPercentage) {
		super();
		this.OptionCode = OptionCode;
		this.OptionType = OptionType;
		this.Name = Name;
		this.Description = Description;
		this.ListPrice = ListPrice;
		this.Discount = Discount;
		this.DealerCode = DealerCode;
		this.DealerName = DealerName;
		this.ApplicableVatPercentage = ApplicableVatPercentage;
	}

	public String getOptionCode() {
		return OptionCode;
	}

	public void setOptionCode(String optionCode) {
		OptionCode = optionCode;
	}

	public String getOptionType() {
		return OptionType;
	}

	public void setOptionType(String optionType) {
		OptionType = optionType;
	}

	public Name getName() {
		return Name;
	}

	public void setName(Name name) {
		Name = name;
	}

	public Description getDescription() {
		return Description;
	}

	public void setDescription(Description description) {
		Description = description;
	}

	public String getListPrice() {
		return ListPrice;
	}

	public void setListPrice(String listPrice) {
		ListPrice = listPrice;
	}

	public Discount getDiscount() {
		return Discount;
	}

	public void setDiscount(Discount discount) {
		Discount = discount;
	}

	public String getDealerCode() {
		return DealerCode;
	}

	public void setDealerCode(String dealerCode) {
		DealerCode = dealerCode;
	}

	public String getDealerName() {
		return DealerName;
	}

	public void setDealerName(String dealerName) {
		DealerName = dealerName;
	}

	public String getApplicableVatPercentage() {
		return ApplicableVatPercentage;
	}

	public void setApplicableVatPercentage(String applicableVatPercentage) {
		ApplicableVatPercentage = applicableVatPercentage;
	}

	@Override
	public String toString() {
		String Naam = Name.getDutch();
		String Descriptie = Description.getDutch();
		String discAmount = Discount.getAmount();
		String perc = Discount.getPercentage();
		return String
				.format("OptionCode: " + OptionCode + " OptionType: " + OptionType + " ListPrice: " + ListPrice
						+ " DealerCode: " + DealerCode + " DealerName: " + DealerName)
				+ " ApplicableVatPercentage: " + ApplicableVatPercentage;
	}

}
