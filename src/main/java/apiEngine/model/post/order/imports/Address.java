package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Address {
	@XmlElement(name="StreetHouseNumber", required=false)
	private String StreetHouseNumber;
	@XmlElement(name="PostalCode", required=false)
	 private String PostalCode;
	@XmlElement(name="City", required=false)
	 private String City;

	public Address() {
		
	}

	public Address(String StreetHouseNumber, String PostalCode, String City) {
		super();
		this.StreetHouseNumber = StreetHouseNumber;
		this.PostalCode = PostalCode;
		this.City = City;

		}
	
	 // Getter Methods 

	 public String getStreetHouseNumber() {
	  return StreetHouseNumber;
	 }

	 public String getPostalCode() {
	  return PostalCode;
	 }

	 public String getCity() {
	  return City;
	 }

	 // Setter Methods 

	 public void setStreetHouseNumber(String StreetHouseNumber) {
	  this.StreetHouseNumber = StreetHouseNumber;
	 }

	 public void setPostalCode(String PostalCode) {
	  this.PostalCode = PostalCode;
	 }

	 public void setCity(String City) {
	  this.City = City;
	 }
	

}
