package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Contact {
	
	@XmlElement(name="Email", required=false)
	private String Email;
	@XmlElement(name="PhoneNumber", required=false)
	private String PhoneNumber;
	@XmlElement(name="MobileNumber", required=false)
	private String MobileNumber;
	@XmlElement(name="FaxNumber", required=false)
	private String FaxNumber;
	
	public Contact() {
		
	}


	public Contact(String Email, String PhoneNumber, String MobileNumber, String FaxNumber) {
		super();
		this.Email = Email;
		this.PhoneNumber = PhoneNumber;
		this.MobileNumber = MobileNumber;
		this.FaxNumber = FaxNumber;

		}
	
	 // Getter Methods 

	 public String getEmail() {
	  return Email;
	 }

	 public String getPhoneNumber() {
	  return PhoneNumber;
	 }

	 public String getMobileNumber() {
	  return MobileNumber;
	 }

	 public String getFaxNumber() {
	  return FaxNumber;
	 }

	 // Setter Methods 

	 public void setEmail(String Email) {
	  this.Email = Email;
	 }

	 public void setPhoneNumber(String PhoneNumber) {
	  this.PhoneNumber = PhoneNumber;
	 }

	 public void setMobileNumber(String MobileNumber) {
	  this.MobileNumber = MobileNumber;
	 }

	 public void setFaxNumber(String FaxNumber) {
	  this.FaxNumber = FaxNumber;
	 }

}
