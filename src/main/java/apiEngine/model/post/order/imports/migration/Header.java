
package apiEngine.model.post.order.imports.migration;

import jakarta.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;



//import jakarta.xml.bind.annotation.XmlSchemaType;
//import jakarta.xml.bind.annotation.XmlType;
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;


/**
 * <p>Java class for Header complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Header"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="CreatedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "Header", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {


public class Header {

    @XmlElement(name = "CreatedDate", required = true)
    //@XmlSchemaType(name = "date")
    private String CreatedDate;

    public Header() {

    }

    public Header(String CreatedDate) {
        //super();
        this.CreatedDate = CreatedDate;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }


    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

}
