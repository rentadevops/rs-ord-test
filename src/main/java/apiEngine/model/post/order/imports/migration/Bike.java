
package apiEngine.model.post.order.imports.migration;

//import jakarta.xml.bind.annotation.XmlAccessType;
//import jakarta.xml.bind.annotation.XmlAccessorType;
//import jakarta.xml.bind.annotation.XmlElement;
//import jakarta.xml.bind.annotation.XmlSchemaType;
//import jakarta.xml.bind.annotation.XmlType;
//import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
//import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


//import javax.xml.bind.annotation.*;
//import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
//import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>Java class for Bike complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Bike"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="FullInfoAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="LicensePlateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Brand" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Model" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="Version" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="Segment" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="Category" type="{http://be/renta/ordering/communication/orderimport}BikeCategory" minOccurs="0"/&gt;
 *         &lt;element name="Weight" type="{http://be/renta/ordering/communication/orderimport}NullInt" minOccurs="0"/&gt;
 *         &lt;element name="ExteriorColor" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="LanguagePapers" type="{http://be/renta/ordering/communication/orderimport}LanguageCode" minOccurs="0"/&gt;
 *         &lt;element name="TyreDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StockCar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Identification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FrameSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FrameSizeUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FrameType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Carbon" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ConstructionYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Gear" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BatteryPower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BatteryPowerUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CertifiedLock" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bike", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Bike {

    @XmlElement(name = "FullInfoAvailable", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Boolean fullInfoAvailable;
    @XmlElement(name = "LicensePlateNumber", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String licensePlateNumber;
    @XmlElement(name = "Brand", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String brand;
    @XmlElement(name = "Model", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable model;
    @XmlElement(name = "Version", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable version;
    @XmlElement(name = "Segment", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable segment;
    @XmlElement(name = "Category", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String category;
    @XmlElement(name = "Weight", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String weight;
    @XmlElement(name = "ExteriorColor", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable exteriorColor;
    @XmlElement(name = "LanguagePapers", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String languagePapers;
    @XmlElement(name = "TyreDescription", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String tyreDescription;
    @XmlElement(name = "StockCar", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Boolean stockCar;
    @XmlElement(name = "Identification", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String identification;
    @XmlElement(name = "FrameSize", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String frameSize;
    @XmlElement(name = "FrameSizeUnit", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String frameSizeUnit;
    @XmlElement(name = "FrameType", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String frameType;
    @XmlElement(name = "Carbon", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Boolean carbon;
    @XmlElement(name = "ConstructionYear", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Integer constructionYear;
    @XmlElement(name = "Gear", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String gear;
    @XmlElement(name = "BatteryPower", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String batteryPower;
    @XmlElement(name = "BatteryPowerUnit", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String batteryPowerUnit;
    @XmlElement(name = "CertifiedLock", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Boolean certifiedLock;

    /**
     * Gets the value of the fullInfoAvailable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFullInfoAvailable() {
        return fullInfoAvailable;
    }

    /**
     * Sets the value of the fullInfoAvailable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFullInfoAvailable(Boolean value) {
        this.fullInfoAvailable = value;
    }

    /**
     * Gets the value of the licensePlateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    /**
     * Sets the value of the licensePlateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlateNumber(String value) {
        this.licensePlateNumber = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setModel(Translatable value) {
        this.model = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setVersion(Translatable value) {
        this.version = value;
    }

    /**
     * Gets the value of the segment property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getSegment() {
        return segment;
    }

    /**
     * Sets the value of the segment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setSegment(Translatable value) {
        this.segment = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeight(String value) {
        this.weight = value;
    }

    /**
     * Gets the value of the exteriorColor property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getExteriorColor() {
        return exteriorColor;
    }

    /**
     * Sets the value of the exteriorColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setExteriorColor(Translatable value) {
        this.exteriorColor = value;
    }

    /**
     * Gets the value of the languagePapers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguagePapers() {
        return languagePapers;
    }

    /**
     * Sets the value of the languagePapers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguagePapers(String value) {
        this.languagePapers = value;
    }

    /**
     * Gets the value of the tyreDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTyreDescription() {
        return tyreDescription;
    }

    /**
     * Sets the value of the tyreDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTyreDescription(String value) {
        this.tyreDescription = value;
    }

    /**
     * Gets the value of the stockCar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStockCar() {
        return stockCar;
    }

    /**
     * Sets the value of the stockCar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStockCar(Boolean value) {
        this.stockCar = value;
    }

    /**
     * Gets the value of the identification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentification() {
        return identification;
    }

    /**
     * Sets the value of the identification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentification(String value) {
        this.identification = value;
    }

    /**
     * Gets the value of the frameSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrameSize() {
        return frameSize;
    }

    /**
     * Sets the value of the frameSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrameSize(String value) {
        this.frameSize = value;
    }

    /**
     * Gets the value of the frameSizeUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrameSizeUnit() {
        return frameSizeUnit;
    }

    /**
     * Sets the value of the frameSizeUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrameSizeUnit(String value) {
        this.frameSizeUnit = value;
    }

    /**
     * Gets the value of the frameType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrameType() {
        return frameType;
    }

    /**
     * Sets the value of the frameType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrameType(String value) {
        this.frameType = value;
    }

    /**
     * Gets the value of the carbon property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCarbon() {
        return carbon;
    }

    /**
     * Sets the value of the carbon property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCarbon(Boolean value) {
        this.carbon = value;
    }

    /**
     * Gets the value of the constructionYear property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConstructionYear() {
        return constructionYear;
    }

    /**
     * Sets the value of the constructionYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConstructionYear(Integer value) {
        this.constructionYear = value;
    }

    /**
     * Gets the value of the gear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGear() {
        return gear;
    }

    /**
     * Sets the value of the gear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGear(String value) {
        this.gear = value;
    }

    /**
     * Gets the value of the batteryPower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatteryPower() {
        return batteryPower;
    }

    /**
     * Sets the value of the batteryPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatteryPower(String value) {
        this.batteryPower = value;
    }

    /**
     * Gets the value of the batteryPowerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatteryPowerUnit() {
        return batteryPowerUnit;
    }

    /**
     * Sets the value of the batteryPowerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatteryPowerUnit(String value) {
        this.batteryPowerUnit = value;
    }

    /**
     * Gets the value of the certifiedLock property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCertifiedLock() {
        return certifiedLock;
    }

    /**
     * Sets the value of the certifiedLock property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCertifiedLock(Boolean value) {
        this.certifiedLock = value;
    }

}
