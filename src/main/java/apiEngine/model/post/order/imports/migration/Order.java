
package apiEngine.model.post.order.imports.migration;


//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for Order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Order"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Meta" type="{http://be/renta/ordering/communication/orderimport}Meta"/&gt;
 *         &lt;element name="Client" type="{http://be/renta/ordering/communication/orderimport}Client" minOccurs="0"/&gt;
 *         &lt;element name="Driver" type="{http://be/renta/ordering/communication/orderimport}Driver" minOccurs="0"/&gt;
 *         &lt;element name="Vehicle" type="{http://be/renta/ordering/communication/orderimport}Vehicle" minOccurs="0"/&gt;
 *         &lt;element name="Pricing" type="{http://be/renta/ordering/communication/orderimport}Pricing"/&gt;
 *         &lt;element name="Delivery" type="{http://be/renta/ordering/communication/orderimport}Delivery" minOccurs="0"/&gt;
 *         &lt;element name="Buyback" type="{http://be/renta/ordering/communication/orderimport}Buyback" minOccurs="0"/&gt;
 *         &lt;element name="Insurance" type="{http://be/renta/ordering/communication/orderimport}Insurance" minOccurs="0"/&gt;
 *         &lt;element name="TyreFitter" type="{http://be/renta/ordering/communication/orderimport}TyreFitter" minOccurs="0"/&gt;
 *         &lt;element name="RegistrationInfo" type="{http://be/renta/ordering/communication/orderimport}RegistrationInfo" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Order {

    @XmlElement(name = "Meta", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected Meta Meta;
    @XmlElement(name = "Client", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Client Client;
    @XmlElement(name = "Driver", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Driver Driver;
    @XmlElement(name = "Vehicle", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Vehicle Vehicle;
    @XmlElement(name = "Pricing", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected Pricing Pricing;
    @XmlElement(name = "Delivery", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Delivery Delivery;
    @XmlElement(name = "Buyback", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Buyback Buyback;
    @XmlElement(name = "Insurance", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Insurance Insurance;
    @XmlElement(name = "TyreFitter", namespace = "http://be/renta/ordering/communication/orderimport")
    protected TyreFitter TyreFitter;
    @XmlElement(name = "RegistrationInfo", namespace = "http://be/renta/ordering/communication/orderimport")
    protected RegistrationInfo RegistrationInfo;

    /**
     * Gets the value of the meta property.
     * 
     * @return
     *     possible object is
     *     {@link Meta }
     *     
     */
    public Meta getMeta() {
        return Meta;
    }

    /**
     * Sets the value of the meta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Meta }
     *     
     */
    public void setMeta(Meta value) {
        this.Meta = value;
    }

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link Client }
     *     
     */
    public Client getClient() {
        return Client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link Client }
     *     
     */
    public void setClient(Client value) {
        this.Client = value;
    }

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link Driver }
     *     
     */
    public Driver getDriver() {
        return Driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Driver }
     *     
     */
    public void setDriver(Driver value) {
        this.Driver = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return Vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.Vehicle = value;
    }

    /**
     * Gets the value of the pricing property.
     * 
     * @return
     *     possible object is
     *     {@link Pricing }
     *     
     */
    public Pricing getPricing() {
        return Pricing;
    }

    /**
     * Sets the value of the pricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pricing }
     *     
     */
    public void setPricing(Pricing value) {
        this.Pricing = value;
    }

    /**
     * Gets the value of the delivery property.
     * 
     * @return
     *     possible object is
     *     {@link Delivery }
     *     
     */
    public Delivery getDelivery() {
        return Delivery;
    }

    /**
     * Sets the value of the delivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Delivery }
     *     
     */
    public void setDelivery(Delivery value) {
        this.Delivery = value;
    }

    /**
     * Gets the value of the buyback property.
     * 
     * @return
     *     possible object is
     *     {@link Buyback }
     *     
     */
    public Buyback getBuyback() {
        return Buyback;
    }

    /**
     * Sets the value of the buyback property.
     * 
     * @param value
     *     allowed object is
     *     {@link Buyback }
     *     
     */
    public void setBuyback(Buyback value) {
        this.Buyback = value;
    }

    /**
     * Gets the value of the insurance property.
     * 
     * @return
     *     possible object is
     *     {@link Insurance }
     *     
     */
    public Insurance getInsurance() {
        return Insurance;
    }

    /**
     * Sets the value of the insurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Insurance }
     *     
     */
    public void setInsurance(Insurance value) {
        this.Insurance = value;
    }

    /**
     * Gets the value of the tyreFitter property.
     * 
     * @return
     *     possible object is
     *     {@link TyreFitter }
     *     
     */
    public TyreFitter getTyreFitter() {
        return TyreFitter;
    }

    /**
     * Sets the value of the tyreFitter property.
     * 
     * @param value
     *     allowed object is
     *     {@link TyreFitter }
     *     
     */
    public void setTyreFitter(TyreFitter value) {
        this.TyreFitter = value;
    }

    /**
     * Gets the value of the registrationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationInfo }
     *     
     */
    public RegistrationInfo getRegistrationInfo() {
        return RegistrationInfo;
    }

    /**
     * Sets the value of the registrationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationInfo }
     *     
     */
    public void setRegistrationInfo(RegistrationInfo value) {
        this.RegistrationInfo = value;
    }

}
