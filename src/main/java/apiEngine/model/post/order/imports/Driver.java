package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;



//import javax.xml.bind.annotation.XmlElement;
//
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Driver {
	
	@XmlElement(name="Name", required=false)
	private String Name;
	@XmlElement(name="Address", required=false)
	private Address Address;
	@XmlElement(name="Contact", required=false)
	private Contact Contact;
	@XmlElement(name="LanguageCode", required=false)
	private String LanguageCode;

	public Driver() {
		
	}

	public Driver(String Name, Address Address, Contact Contact, String LanguageCode) {
		super();
		this.Name = Name;
		this.Address = Address;
		this.Contact = Contact;
		this.LanguageCode = LanguageCode;
		}
	
	 // Getter Methods 

	 public String getName() {
	  return Name;
	 }

	 public Address getAddress() {
	  return Address;
	 }

	 public Contact getContact() {
	  return Contact;
	 }

	 public String getLanguageCode() {
	  return LanguageCode;
	 }

	 // Setter Methods 

	 public void setName(String Name) {
	  this.Name = Name;
	 }

	 public void setAddress(Address Address) {
	  this.Address = Address;
	 }

	 public void setContact(Contact Contact) {
	  this.Contact = Contact;
	 }

	 public void setLanguageCode(String LanguageCode) {
	  this.LanguageCode = LanguageCode;
	 }

}
