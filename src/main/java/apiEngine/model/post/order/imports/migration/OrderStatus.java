
package apiEngine.model.post.order.imports.migration;

//import jakarta.xml.bind.annotation.XmlEnum;
//import jakarta.xml.bind.annotation.XmlType;


//import javax.xml.bind.annotation.XmlEnum;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for OrderStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="OrderStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NEW"/&gt;
 *     &lt;enumeration value="ACCEPTED"/&gt;
 *     &lt;enumeration value="ORDERED"/&gt;
 *     &lt;enumeration value="VEHICLE_IN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OrderStatus", namespace = "http://be/renta/ordering/communication/orderimport")
@XmlEnum
public enum OrderStatus {

    NEW,
    ACCEPTED,
    ORDERED,
    VEHICLE_IN;

    public String value() {
        return name();
    }

    public static OrderStatus fromValue(String v) {
        return valueOf(v);
    }

}
