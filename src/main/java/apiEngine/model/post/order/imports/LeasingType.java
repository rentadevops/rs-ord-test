package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlEnum;
//import javax.xml.bind.annotation.XmlEnumValue;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;


	@XmlType(name = "LeasingType")
	@XmlEnum
	public enum LeasingType {

	    @XmlEnumValue("OPERATIONAL_LEASE")
	    OL("OPERATIONAL_LEASE"),
	    
	    @XmlEnumValue("FINANCIAL_LEASE")
	    FL("FINANCIAL_LEASE"),
	    
	    @XmlEnumValue("RENTAL")
	    R("RENTAL");
	    
	    private final String value;

	    LeasingType(String v) {
	        value = v;
	    }

	    public String value() {
	        return value;
	    }

	    public static LeasingType fromValue(String v) {
	        for (LeasingType c: LeasingType.values()) {
	            if (c.value.equals(v)) {
	                return c;
	            }
	        }
	        throw new IllegalArgumentException(v);
	    }

}

