package apiEngine.model.post.order.imports.migration;

import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="OrderImports")
public class OrderImports {

    @XmlElement(name="Header")
    private Header Header;
    @XmlElement(name="Order")
    private Order Order;
    @XmlAttribute(name="xmlns")
    private String _xmlns;

    public OrderImports() {

    }

    public OrderImports(Header Header, Order Order,  String _xmlns) {
        super();
        this.Header = Header;
        this.Order = Order;
        this._xmlns = _xmlns;

    }

    // Getter Methods

    public Header getHeader() {
        return Header;
    }

    public Order getOrder() {
        return Order;
    }

    public String get_xmlns() {
        return _xmlns;
    }

    // Setter Methods

    public void setHeader(Header HeaderObject) {
        this.Header = HeaderObject;
    }

    public void setOrder(Order OrderObject) {
        this.Order = OrderObject;
    }

    public void set_xmlns(String _xmlns) {
        this._xmlns = _xmlns;
    }
}
