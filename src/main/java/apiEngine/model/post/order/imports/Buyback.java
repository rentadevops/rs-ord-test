package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Buyback {

	@XmlElement(name="Name", required=false)
	private String Name;
	@XmlElement(name="Address", required=false)
	 private Address Address;
	@XmlElement(name="Amount", required=false)
	 private String Amount;
	@XmlElement(name="ContractDuration", required=false)
	 private String ContractDuration;
	@XmlElement(name="Kilometers", required=false)
	 private String Kilometers;
	
	public Buyback() {
		
	}

	public Buyback(String Name, Address Address, String Amount, String ContractDuration, String Kilometers) {
		super();
		this.Name = Name;
		this.Address = Address;
		this.Amount = Amount;
		this.ContractDuration = ContractDuration;
		this.Kilometers = Kilometers;

		}
	
	 // Getter Methods 

	 public String getName() {
	  return Name;
	 }

	 public Address getAddress() {
	  return Address;
	 }

	 public String getAmount() {
	  return Amount;
	 }

	 public String getContractDuration() {
	  return ContractDuration;
	 }
	 
	 public String getKilometers() {
		  return Kilometers;
		 }

	 // Setter Methods 

	 public void setName(String Name) {
	  this.Name = Name;
	 }

	 public void setAddress(Address Address) {
	  this.Address = Address;
	 }

	 public void setAmount(String Amount) {
	  this.Amount = Amount;
	 }

	 public void setContractDuration(String ContractDuration) {
	  this.ContractDuration = ContractDuration;
	 }
	 
	 public void setKilometers(String Kilometers) {
		  this.Kilometers = Kilometers;
		 }
	
}
