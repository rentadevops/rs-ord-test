
package apiEngine.model.post.order.imports.migration;

//import jakarta.xml.bind.annotation.XmlAccessType;
//import jakarta.xml.bind.annotation.XmlAccessorType;
//import jakarta.xml.bind.annotation.XmlElement;
//import jakarta.xml.bind.annotation.XmlSchemaType;
//import jakarta.xml.bind.annotation.XmlType;


//import javax.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for RegistrationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="RegistrationDivToDoBy" type="{http://be/renta/ordering/communication/orderimport}Applicant" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationInfo", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class RegistrationInfo {

    @XmlElement(name = "RegistrationDivToDoBy", namespace = "http://be/renta/ordering/communication/orderimport")
    @XmlSchemaType(name = "normalizedString")
    protected Applicant registrationDivToDoBy;

    /**
     * Gets the value of the registrationDivToDoBy property.
     * 
     * @return
     *     possible object is
     *     {@link Applicant }
     *     
     */
    public Applicant getRegistrationDivToDoBy() {
        return registrationDivToDoBy;
    }

    /**
     * Sets the value of the registrationDivToDoBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Applicant }
     *     
     */
    public void setRegistrationDivToDoBy(Applicant value) {
        this.registrationDivToDoBy = value;
    }

}
