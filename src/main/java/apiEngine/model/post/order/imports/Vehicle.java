package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Vehicle {

	@XmlElement(name="Brand", required=false)
	private String Brand;
	@XmlElement(name="Model", required=false)
	private Model Model;
	@XmlElement(name="Version", required=false)
	private Version Version;
	@XmlElement(name="CylinderContent", required=false)
	private String CylinderContent;
	@XmlElement(name="Power", required=false)
	private String Power;
	@XmlElement(name="LanguagePapers", required=false)
	private String LanguagePapers;
	@XmlElement(name="Weight", required=false)
	private String Weight;
	@XmlElement(name="TyreDescription", required=false)
	private String TyreDescription;
	@XmlElement(name="StockCar", required=false)
	private String StockCar;
	@XmlElement(name="ExteriorColor", required=false)
	private ExteriorColor ExteriorColor;
	@XmlElement(name="InteriorColor", required=false)
	private InteriorColor InteriorColor;
	@XmlElement(name="Co2Emissions", required=false)
	private String Co2Emissions;
	@XmlElement(name="Identification", required=false)
	private String Identification;
	@XmlElement(name="LicensePlateNumber", required=false)
	private String LicensePlateNumber;
	@XmlElement(name="TransferredLicensePlate", required=false)
	private String TransferredLicensePlate;

	public Vehicle() {
		
	}

	public Vehicle(String Brand, Model Model, Version Version, String CylinderContent, String Power, String LanguagePapers, String Weight, 
			String TyreDescription, String StockCar, ExteriorColor ExteriorColor, InteriorColor InteriorColor, String Co2Emissions, String Identification,
			String LicensePlateNumber, String TransferredLicensePlate) {
		super();
		this.Brand = Brand;
		this.Model = Model;
		this.Version = Version;
		this.CylinderContent = CylinderContent;
		this.Power = Power;
		this.LanguagePapers = LanguagePapers;
		this.Weight = Weight;
		this.TyreDescription = TyreDescription;
		this.StockCar = StockCar;
		this.ExteriorColor = ExteriorColor;
		this.InteriorColor = InteriorColor;
		this.Co2Emissions = Co2Emissions;
		this.Identification = Identification;
		this.LicensePlateNumber = LicensePlateNumber;
		this.TransferredLicensePlate = TransferredLicensePlate;
		}

	public String getBrand() {
		return Brand;
	}

	public void setBrand(String brand) {
		Brand = brand;
	}

	public Model getModel() {
		return Model;
	}

	public void setModel(Model model) {
		Model = model;
	}

	public Version getVersion() {
		return Version;
	}

	public void setVersion(Version version) {
		Version = version;
	}

	public String getCylinderContent() {
		return CylinderContent;
	}

	public void setCylinderContent(String cylinderContent) {
		CylinderContent = cylinderContent;
	}

	public String getPower() {
		return Power;
	}

	public void setPower(String power) {
		Power = power;
	}

	public String getLanguagePapers() {
		return LanguagePapers;
	}

	public void setLanguagePapers(String languagePapers) {
		LanguagePapers = languagePapers;
	}

	public String getWeight() {
		return Weight;
	}

	public void setWeight(String weight) {
		Weight = weight;
	}

	public String getTyreDescription() {
		return TyreDescription;
	}

	public void setTyreDescription(String tyreDescription) {
		TyreDescription = tyreDescription;
	}

	public String getStockCar() {
		return StockCar;
	}

	public void setStockCar(String stockCar) {
		StockCar = stockCar;
	}

	public ExteriorColor getExteriorColor() {
		return ExteriorColor;
	}

	public void setExteriorColor(ExteriorColor exteriorColor) {
		ExteriorColor = exteriorColor;
	}

	public InteriorColor getInteriorColor() {
		return InteriorColor;
	}

	public void setInteriorColor(InteriorColor interiorColor) {
		InteriorColor = interiorColor;
	}

	public String getCo2Emissions() {
		return Co2Emissions;
	}

	public void setCo2Emissions(String co2Emissions) {
		Co2Emissions = co2Emissions;
	}

	public String getIdentification() {
		return Identification;
	}

	public void setIdentification(String identification) {
		Identification = identification;
	}

	public String getLicensePlateNumber() {
		return LicensePlateNumber;
	}

	public void setLicensePlateNumber(String licensePlateNumber) {
		LicensePlateNumber = licensePlateNumber;
	}

	public String getTransferredLicensePlate() {
		return TransferredLicensePlate;
	}

	public void setTransferredLicensePlate(String transferredLicensePlate) {
		TransferredLicensePlate = transferredLicensePlate;
	}
	
	

}
