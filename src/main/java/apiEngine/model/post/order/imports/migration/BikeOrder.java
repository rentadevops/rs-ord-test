
package apiEngine.model.post.order.imports.migration;

//import jakarta.xml.bind.annotation.XmlAccessType;
//import jakarta.xml.bind.annotation.XmlAccessorType;
//import jakarta.xml.bind.annotation.XmlElement;
//import jakarta.xml.bind.annotation.XmlType;


//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for BikeOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BikeOrder"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Meta" type="{http://be/renta/ordering/communication/orderimport}Meta"/&gt;
 *         &lt;element name="Client" type="{http://be/renta/ordering/communication/orderimport}Client" minOccurs="0"/&gt;
 *         &lt;element name="Driver" type="{http://be/renta/ordering/communication/orderimport}Driver" minOccurs="0"/&gt;
 *         &lt;element name="Bike" type="{http://be/renta/ordering/communication/orderimport}Bike" minOccurs="0"/&gt;
 *         &lt;element name="Pricing" type="{http://be/renta/ordering/communication/orderimport}Pricing"/&gt;
 *         &lt;element name="Delivery" type="{http://be/renta/ordering/communication/orderimport}Delivery" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BikeOrder", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class BikeOrder {

    @XmlElement(name = "Meta", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected Meta meta;
    @XmlElement(name = "Client", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Client client;
    @XmlElement(name = "Driver", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Driver driver;
    @XmlElement(name = "Bike", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Bike bike;
    @XmlElement(name = "Pricing", namespace = "http://be/renta/ordering/communication/orderimport", required = true)
    protected Pricing pricing;
    @XmlElement(name = "Delivery", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Delivery delivery;

    /**
     * Gets the value of the meta property.
     * 
     * @return
     *     possible object is
     *     {@link Meta }
     *     
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * Sets the value of the meta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Meta }
     *     
     */
    public void setMeta(Meta value) {
        this.meta = value;
    }

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link Client }
     *     
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link Client }
     *     
     */
    public void setClient(Client value) {
        this.client = value;
    }

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link Driver }
     *     
     */
    public Driver getDriver() {
        return driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Driver }
     *     
     */
    public void setDriver(Driver value) {
        this.driver = value;
    }

    /**
     * Gets the value of the bike property.
     * 
     * @return
     *     possible object is
     *     {@link Bike }
     *     
     */
    public Bike getBike() {
        return bike;
    }

    /**
     * Sets the value of the bike property.
     * 
     * @param value
     *     allowed object is
     *     {@link Bike }
     *     
     */
    public void setBike(Bike value) {
        this.bike = value;
    }

    /**
     * Gets the value of the pricing property.
     * 
     * @return
     *     possible object is
     *     {@link Pricing }
     *     
     */
    public Pricing getPricing() {
        return pricing;
    }

    /**
     * Sets the value of the pricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pricing }
     *     
     */
    public void setPricing(Pricing value) {
        this.pricing = value;
    }

    /**
     * Gets the value of the delivery property.
     * 
     * @return
     *     possible object is
     *     {@link Delivery }
     *     
     */
    public Delivery getDelivery() {
        return delivery;
    }

    /**
     * Sets the value of the delivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Delivery }
     *     
     */
    public void setDelivery(Delivery value) {
        this.delivery = value;
    }

}
