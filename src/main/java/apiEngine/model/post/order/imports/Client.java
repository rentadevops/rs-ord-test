package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Client {
	
	@XmlElement(name="Name", required=false) 
	private String Name;
	@XmlElement(name="Address", required=false)
	private Address Address;
	@XmlElement(name="Contact", required=false)
	private Contact Contact;
	@XmlElement(name="ContactPerson", required=false)
	 private String ContactPerson;
	@XmlElement(name="LanguageCode", required=false)
	 private String LanguageCode;
	@XmlElement(name="VAT", required=false)
	 private String VAT;
	@XmlElement(name="RegistrationNumberLessee", required=false)
	 private String RegistrationNumberLessee;
	
	


	


	public Client() {
		
	}


	public Client(String Name, Address Address, Contact Contact, String ContactPerson, String LanguageCode, String VAT, String RegistrationNumberLessee) {
		super();
		this.Name = Name;
		this.Address = Address;
		this.Contact = Contact;
		this.ContactPerson = ContactPerson;
		this.LanguageCode = LanguageCode;
		this.VAT = VAT;
		this.RegistrationNumberLessee = RegistrationNumberLessee;
		}
	

	 // Getter Methods 

	 public String getName() {
	  return Name;
	 }

	 public Address getAddress() {
	  return Address;
	 }

	 public Contact getContact() {
	  return Contact;
	 }

	 public String getContactPerson() {
	  return ContactPerson;
	 }

	 public String getLanguageCode() {
	  return LanguageCode;
	 }

	 public String getVAT() {
	  return VAT;
	 }
	 
	 public String getRegistrationNumberLessee() {
			return RegistrationNumberLessee;
		}

	 // Setter Methods 

	 public void setName(String Name) {
	  this.Name = Name;
	 }

	 public void setAddress(Address Address) {
	  this.Address = Address;
	 }

	 public void setContact(Contact Contact) {
	  this.Contact = Contact;
	 }

	 public void setContactPerson(String ContactPerson) {
	  this.ContactPerson = ContactPerson;
	 }

	 public void setLanguageCode(String LanguageCode) {
	  this.LanguageCode = LanguageCode;
	 }

	 public void setVAT(String VAT) {
	  this.VAT = VAT;
	 }
	 
	 public void setRegistrationNumberLessee(String registrationNumberLessee) {
			RegistrationNumberLessee = registrationNumberLessee;
		}

}
