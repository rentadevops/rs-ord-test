
package apiEngine.model.post.order.imports.migration;


//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for Translatable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Translatable"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Dutch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="French" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="English" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Translatable", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Translatable {

    @XmlElement(name = "Dutch", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String Dutch;
    @XmlElement(name = "French", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String French;
    @XmlElement(name = "English", namespace = "http://be/renta/ordering/communication/orderimport")
    protected String English;

    /**
     * Gets the value of the dutch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutch() {
        return Dutch;
    }

    /**
     * Sets the value of the dutch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutch(String value) {
        this.Dutch = value;
    }

    /**
     * Gets the value of the french property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrench() {
        return French;
    }

    /**
     * Sets the value of the french property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrench(String value) {
        this.French = value;
    }

    /**
     * Gets the value of the english property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnglish() {
        return English;
    }

    /**
     * Sets the value of the english property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnglish(String value) {
        this.English = value;
    }

}
