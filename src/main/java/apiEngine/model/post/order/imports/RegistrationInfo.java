package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class RegistrationInfo {
	
	@XmlElement(name="RegistrationDivToDoBy")
	private String RegistrationDivToDoBy;

	public RegistrationInfo() {
		
	}
	
	public RegistrationInfo(String RegistrationDivToDoBy) {
		super();
		this.RegistrationDivToDoBy = RegistrationDivToDoBy;
		}
	
	 // Getter Methods 

	 public String getRegistrationDivToDoBy() {
	  return RegistrationDivToDoBy;
	 }

	 // Setter Methods 

	 public void setRegistrationDivToDoBy(String RegistrationDivToDoBy) {
	  this.RegistrationDivToDoBy = RegistrationDivToDoBy;
	 }

}
