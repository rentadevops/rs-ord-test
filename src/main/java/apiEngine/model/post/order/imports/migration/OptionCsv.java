package apiEngine.model.post.order.imports.migration;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class OptionCsv {
	

	private String OptionCode;
	private String OptionType;
	private String Name;
	private String Description;
	private String ListPrice;
	private String Amount;
	private String Percentage;
	private String DealerCode;
	private String DealerName;

	public OptionCsv() {
		
	}
	
	public OptionCsv(String OptionCode, String OptionType, String Name, String Description, String ListPrice, String Amount, String Percentage, String DealerCode, String DealerName) {
		super();
		this.OptionCode = OptionCode;
		this.OptionType = OptionType;
		this.Name = Name;
		this.Description = Description;
		this.ListPrice = ListPrice;
		this.Amount = Amount;
		this.Percentage = Percentage;
		this.DealerCode = DealerCode;
		this.DealerName = DealerName;
		}

	public String getOptionCode() {
		return OptionCode;
	}

	public void setOptionCode(String optionCode) {
		OptionCode = optionCode;
	}

	public String getOptionType() {
		return OptionType;
	}

	public void setOptionType(String optionType) {
		OptionType = optionType;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getListPrice() {
		return ListPrice;
	}

	public void setListPrice(String listPrice) {
		ListPrice = listPrice;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}
	
	public String getPercentage() {
		return Percentage;
	}

	public void setPercentage(String percentage) {
		Percentage = percentage;
	}

	public String getDealerCode() {
		return DealerCode;
	}

	public void setDealerCode(String dealerCode) {
		DealerCode = dealerCode;
	}

	public String getDealerName() {
		return DealerName;
	}

	public void setDealerName(String dealerName) {
		DealerName = dealerName;
	}
	
	 

}
