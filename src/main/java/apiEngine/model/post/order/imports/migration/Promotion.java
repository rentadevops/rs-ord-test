
package apiEngine.model.post.order.imports.migration;


//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for Promotion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Promotion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Name" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://be/renta/ordering/communication/orderimport}Translatable" minOccurs="0"/&gt;
 *         &lt;element name="Discount" type="{http://be/renta/ordering/communication/orderimport}Discount" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Promotion", namespace = "http://be/renta/ordering/communication/orderimport", propOrder = {

})
public class Promotion {

    @XmlElement(name = "Name", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable Name;
    @XmlElement(name = "Description", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Translatable Description;
    @XmlElement(name = "Discount", namespace = "http://be/renta/ordering/communication/orderimport")
    protected Discount Discount;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getName() {
        return Name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setName(Translatable value) {
        this.Name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link Translatable }
     *     
     */
    public Translatable getDescription() {
        return Description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translatable }
     *     
     */
    public void setDescription(Translatable value) {
        this.Description = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link Discount }
     *     
     */
    public Discount getDiscount() {
        return Discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Discount }
     *     
     */
    public void setDiscount(Discount value) {
        this.Discount = value;
    }

}
