package apiEngine.model.post.order.imports.migration;

import java.util.ArrayList;
import java.util.List;

//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlElementWrapper;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.*;

@XmlRootElement(name = "Options")
@XmlAccessorType(XmlAccessType.FIELD)
public class Options {

    //@XmlElementWrapper(name="Options")
    @XmlElement(name = "Option")
    private List<Option> Options = null;

    //@XmlElement(name = "Option")
    //private Option Option = null;


    public Options() {

    }

    public Options(Option Option, List<Option> Options) {
        super();
        this.Options = Options;
    }

    public List<Option> getOptions() {
        return Options;
    }

    public void setOptions(List<Option> options) {
        this.Options = options;
    }




}
