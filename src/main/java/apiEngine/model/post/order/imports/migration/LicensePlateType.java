
package apiEngine.model.post.order.imports.migration;

import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for LicensePlateType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="LicensePlateType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TRANSFERRED_LICENSE_PLATE"/&gt;
 *     &lt;enumeration value="SPECIAL_LICENSE_PLATE"/&gt;
 *     &lt;enumeration value="NORMAL_LICENSE_PLATE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LicensePlateType", namespace = "http://be/renta/ordering/communication/orderimport")
@XmlEnum
public enum LicensePlateType {

    TRANSFERRED_LICENSE_PLATE,
    SPECIAL_LICENSE_PLATE,
    NORMAL_LICENSE_PLATE;

    public String value() {
        return name();
    }

    public static LicensePlateType fromValue(String v) {
        return valueOf(v);
    }

}
