
package apiEngine.model.post.order.imports.migration;

//import jakarta.xml.bind.annotation.XmlEnum;
//import jakarta.xml.bind.annotation.XmlType;


//import javax.xml.bind.annotation.XmlEnum;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for TransactionCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="TransactionCode"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CREATE"/&gt;
 *     &lt;enumeration value="UPDATE"/&gt;
 *     &lt;enumeration value="CONFIRM"/&gt;
 *     &lt;enumeration value="CANCEL"/&gt;
 *     &lt;enumeration value="LICENSE_PLATE"/&gt;
 *     &lt;enumeration value="MIGRATE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TransactionCode", namespace = "http://be/renta/ordering/communication/orderimport")
@XmlEnum
public enum TransactionCode {

    CREATE,
    UPDATE,
    CONFIRM,
    CANCEL,
    LICENSE_PLATE,
    MIGRATE;

    public String value() {
        return name();
    }

    public static TransactionCode fromValue(String v) {
        return valueOf(v);
    }

}
