package apiEngine.model.post.order.imports;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class Pricing {
	
	@XmlElement(name="TotalPrice", required=true)
	private String TotalPrice;
	@XmlElement(name="ListPrice", required=false)
	private String ListPrice;
	@XmlElement(name="FleetDiscount", required=false)
	private FleetDiscount FleetDiscount;
	@XmlElement(name="DeliveryCosts", required=false)
	private String DeliveryCosts;
	@XmlElement(name="OtherCosts", required=false)
	private String OtherCosts;
	@XmlElement(name="Options", required=false)
	private Options Options;
	@XmlElement(name="Promotions", required=false)
	private Promotions Promotions;

	public Pricing() {
		
	}

	public Pricing(String TotalPrice, String ListPrice, FleetDiscount FleetDiscount, String DeliveryCosts, String OtherCosts, Options Options, Promotions Promotions) {
		super();
		this.TotalPrice = TotalPrice;
		this.ListPrice = ListPrice;
		this.FleetDiscount = FleetDiscount;
		this.DeliveryCosts = DeliveryCosts;
		this.OtherCosts = OtherCosts;
		this.Options = Options;
		this.Promotions = Promotions;


		}
	
	 // Getter Methods 

	 public String getTotalPrice() {
	  return TotalPrice;
	 }

	 public String getListPrice() {
	  return ListPrice;
	 }

	 public FleetDiscount getFleetDiscount() {
	  return FleetDiscount;
	 }

	 public String getDeliveryCosts() {
	  return DeliveryCosts;
	 }

	 public String getOtherCosts() {
	  return OtherCosts;
	 }

	 public Options getOptions() {
	  return Options;
	 }

	 public Promotions getPromotions() {
	  return Promotions;
	 }

	 // Setter Methods 

	 public void setTotalPrice(String TotalPrice) {
	  this.TotalPrice = TotalPrice;
	 }

	 public void setListPrice(String ListPrice) {
	  this.ListPrice = ListPrice;
	 }

	 public void setFleetDiscount(FleetDiscount FleetDiscount) {
	  this.FleetDiscount = FleetDiscount;
	 }

	 public void setDeliveryCosts(String DeliveryCosts) {
	  this.DeliveryCosts = DeliveryCosts;
	 }

	 public void setOtherCosts(String OtherCosts) {
	  this.OtherCosts = OtherCosts;
	 }

	 public void setOptions(Options Options) {
	  this.Options = Options;
	 }

	 public void setPromotions(Promotions Promotions) {
	  this.Promotions = Promotions;
	 }

}
