
package apiEngine.model.post.order.imports.migration;

//import jakarta.xml.bind.annotation.XmlEnum;
//import jakarta.xml.bind.annotation.XmlType;


//import javax.xml.bind.annotation.XmlEnum;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for Applicant.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="Applicant"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}normalizedString"&gt;
 *     &lt;enumeration value="CLIENT"/&gt;
 *     &lt;enumeration value="LC"/&gt;
 *     &lt;enumeration value="BROKER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Applicant", namespace = "http://be/renta/ordering/communication/orderimport")
@XmlEnum
public enum Applicant {

    CLIENT,
    LC,
    BROKER;

    public String value() {
        return name();
    }

    public static Applicant fromValue(String v) {
        return valueOf(v);
    }

}
