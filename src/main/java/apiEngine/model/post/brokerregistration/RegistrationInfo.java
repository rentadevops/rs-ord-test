package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistrationInfo {
    @JsonProperty("usedVehicle")
    private Boolean usedVehicle;
    @JsonProperty("priorLicensePlate")
    private String priorLicensePlate;
    @JsonProperty("lastRegistrationDate")
    private String lastRegistrationDate;
    @JsonProperty("orderFrontPlate")
    private Boolean orderFrontPlate;
    @JsonProperty("frontLicensePlateOrderDate")
    private String frontLicensePlateOrderDate;
    @JsonProperty("reuseLicensePlate")
    private Boolean reuseLicensePlate;
    @JsonProperty("registrationDocumentsLanguage")
    private String registrationDocumentsLanguage;
    @JsonProperty("licensePlate")
    private String licensePlate;
    @JsonProperty("licensePlateFormat")
    private String licensePlateFormat;
    @JsonProperty("emailGreenCard")
    private String emailGreenCard;

    @JsonProperty("usedVehicle")
    public Boolean getUsedVehicle() {
        return usedVehicle;
    }

    @JsonProperty("usedVehicle")
    public void setUsedVehicle(Boolean usedVehicle) {
        this.usedVehicle = usedVehicle;
    }

    @JsonProperty("priorLicensePlate")
    public String getPriorLicensePlate() {
        return priorLicensePlate;
    }

    @JsonProperty("priorLicensePlate")
    public void setPriorLicensePlate(String priorLicensePlate) {
        this.priorLicensePlate = priorLicensePlate;
    }

    @JsonProperty("lastRegistrationDate")
    public String getLastRegistrationDate() {
        return lastRegistrationDate;
    }

    @JsonProperty("lastRegistrationDate")
    public void setLastRegistrationDate(String lastRegistrationDate) {
        this.lastRegistrationDate = lastRegistrationDate;
    }

    @JsonProperty("orderFrontPlate")
    public Boolean getOrderFrontPlate() {
        return orderFrontPlate;
    }

    @JsonProperty("orderFrontPlate")
    public void setOrderFrontPlate(Boolean orderFrontPlate) {
        this.orderFrontPlate = orderFrontPlate;
    }

    @JsonProperty("frontLicensePlateOrderDate")
    public String getFrontLicensePlateOrderDate() {
        return frontLicensePlateOrderDate;
    }

    @JsonProperty("frontLicensePlateOrderDate")
    public void setFrontLicensePlateOrderDate(String frontLicensePlateOrderDate) {
        this.frontLicensePlateOrderDate = frontLicensePlateOrderDate;
    }

    @JsonProperty("reuseLicensePlate")
    public Boolean getReuseLicensePlate() {
        return reuseLicensePlate;
    }

    @JsonProperty("reuseLicensePlate")
    public void setReuseLicensePlate(Boolean reuseLicensePlate) {
        this.reuseLicensePlate = reuseLicensePlate;
    }

    @JsonProperty("registrationDocumentsLanguage")
    public String getRegistrationDocumentsLanguage() {
        return registrationDocumentsLanguage;
    }

    @JsonProperty("registrationDocumentsLanguage")
    public void setRegistrationDocumentsLanguage(String registrationDocumentsLanguage) {
        this.registrationDocumentsLanguage = registrationDocumentsLanguage;
    }

    @JsonProperty("licensePlate")
    public String getLicensePlate() {
        return licensePlate;
    }

    @JsonProperty("licensePlate")
    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @JsonProperty("licensePlateFormat")
    public String getLicensePlateFormat() {
        return licensePlateFormat;
    }

    @JsonProperty("licensePlateFormat")
    public void setLicensePlateFormat(String licensePlateFormat) {
        this.licensePlateFormat = licensePlateFormat;
    }

    @JsonProperty("emailGreenCard")
    public String getEmailGreenCard() {
        return emailGreenCard;
    }

    @JsonProperty("emailGreenCard")
    public void setEmailGreenCard(String emailGreenCard) {
        this.emailGreenCard = emailGreenCard;
    }
}
