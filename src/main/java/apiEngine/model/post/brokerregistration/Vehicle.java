package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Vehicle {
    @JsonProperty("vin")
    private Vin vin;
    @JsonProperty("vinControlNumber")
    private String vinControlNumber;

    @JsonProperty("vin")
    public Vin getVin() {
        return vin;
    }

    @JsonProperty("vin")
    public void setVin(Vin vin) {
        this.vin = vin;
    }

    @JsonProperty("vinControlNumber")
    public String getVinControlNumber() {
        return vinControlNumber;
    }

    @JsonProperty("vinControlNumber")
    public void setVinControlNumber(String vinControlNumber) {
        this.vinControlNumber = vinControlNumber;
    }
}
