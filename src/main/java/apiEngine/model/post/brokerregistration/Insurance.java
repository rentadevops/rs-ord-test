package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Insurance {
    @JsonProperty("insuranceCompanyFsmaNumber")
    private String insuranceCompanyFsmaNumber;
    @JsonProperty("insuranceBrokerFsmaNumber")
    private String insuranceBrokerFsmaNumber;
    @JsonProperty("referenceNumber")
    private String referenceNumber;
    @JsonProperty("beginDate")
    private String beginDate;
    @JsonProperty("registrationManagerUserId")
    private Integer registrationManagerUserId;

    @JsonProperty("insuranceCompanyFsmaNumber")
    public String getInsuranceCompanyFsmaNumber() {
        return insuranceCompanyFsmaNumber;
    }

    @JsonProperty("insuranceCompanyFsmaNumber")
    public void setInsuranceCompanyFsmaNumber(String insuranceCompanyFsmaNumber) {
        this.insuranceCompanyFsmaNumber = insuranceCompanyFsmaNumber;
    }

    @JsonProperty("insuranceBrokerFsmaNumber")
    public String getInsuranceBrokerFsmaNumber() {
        return insuranceBrokerFsmaNumber;
    }

    @JsonProperty("insuranceBrokerFsmaNumber")
    public void setInsuranceBrokerFsmaNumber(String insuranceBrokerFsmaNumber) {
        this.insuranceBrokerFsmaNumber = insuranceBrokerFsmaNumber;
    }

    @JsonProperty("referenceNumber")
    public String getReferenceNumber() {
        return referenceNumber;
    }

    @JsonProperty("referenceNumber")
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    @JsonProperty("beginDate")
    public String getBeginDate() {
        return beginDate;
    }

    @JsonProperty("beginDate")
    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    @JsonProperty("registrationManagerUserId")
    public Integer getRegistrationManagerUserId() {
        return registrationManagerUserId;
    }

    @JsonProperty("registrationManagerUserId")
    public void setRegistrationManagerUserId(Integer registrationManagerUserId) {
        this.registrationManagerUserId = registrationManagerUserId;
    }
}
