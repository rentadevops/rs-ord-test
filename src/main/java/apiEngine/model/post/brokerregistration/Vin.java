package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Vin {
    @JsonProperty("value")
    private String value;
    //@JsonProperty("last4Characters")
    //private String last4Characters;

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    //@JsonProperty("last4Characters")
    //public String getLast4Characters() {
    //    return last4Characters;
    //}

    //@JsonProperty("last4Characters")
    //public void setLast4Characters(String last4Characters) {
    //    this.last4Characters = last4Characters;
    //}
}
