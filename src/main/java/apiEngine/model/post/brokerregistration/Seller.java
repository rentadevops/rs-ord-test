package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Seller {
    @JsonProperty("name")
    private String name;
    @JsonProperty("vatNumber")
    private VatNumber__1 vatNumber;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("vatNumber")
    public VatNumber__1 getVatNumber() {
        return vatNumber;
    }

    @JsonProperty("vatNumber")
    public void setVatNumber(VatNumber__1 vatNumber) {
        this.vatNumber = vatNumber;
    }
}
