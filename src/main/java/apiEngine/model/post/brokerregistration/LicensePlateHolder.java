package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LicensePlateHolder {
    @JsonProperty("name")
    private String name;
    @JsonProperty("vatNumber")
    private VatNumber vatNumber;
    @JsonProperty("language")
    private String language;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("vatNumber")
    public VatNumber getVatNumber() {
        return vatNumber;
    }

    @JsonProperty("vatNumber")
    public void setVatNumber(VatNumber vatNumber) {
        this.vatNumber = vatNumber;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }
}
