package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Delivery {
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastNameOrCompanyName")
    private String lastNameOrCompanyName;
    @JsonProperty("street")
    private String street;
    @JsonProperty("houseNumber")
    private String houseNumber;
    @JsonProperty("boxNumber")
    private String boxNumber;
    @JsonProperty("postalCode")
    private String postalCode;
    @JsonProperty("city")
    private String city;
    //@JsonProperty("deliveryAddressNotSet")
    //private Boolean deliveryAddressNotSet;

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("lastNameOrCompanyName")
    public String getLastNameOrCompanyName() {
        return lastNameOrCompanyName;
    }

    @JsonProperty("lastNameOrCompanyName")
    public void setLastNameOrCompanyName(String lastNameOrCompanyName) {
        this.lastNameOrCompanyName = lastNameOrCompanyName;
    }

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    @JsonProperty("street")
    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("houseNumber")
    public String getHouseNumber() {
        return houseNumber;
    }

    @JsonProperty("houseNumber")
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    @JsonProperty("boxNumber")
    public String getBoxNumber() {
        return boxNumber;
    }

    @JsonProperty("boxNumber")
    public void setBoxNumber(String boxNumber) {
        this.boxNumber = boxNumber;
    }

    @JsonProperty("postalCode")
    public String getPostalCode() {
        return postalCode;
    }

    @JsonProperty("postalCode")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    //@JsonProperty("deliveryAddressNotSet")
    //public Boolean getDeliveryAddressNotSet() {
    //    return deliveryAddressNotSet;
    //}

    //@JsonProperty("deliveryAddressNotSet")
    //public void setDeliveryAddressNotSet(Boolean deliveryAddressNotSet) {
    //    this.deliveryAddressNotSet = deliveryAddressNotSet;
    //}
}
