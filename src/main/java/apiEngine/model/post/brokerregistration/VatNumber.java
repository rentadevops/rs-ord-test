package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VatNumber {
    @JsonProperty("value")
    private String value;
    //@JsonProperty("country")
    //private String country;

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    //@JsonProperty("country")
    //public String getCountry() {
    //    return country;
    //}

    //@JsonProperty("country")
    //public void setCountry(String country) {
    //    this.country = country;
    //}
}
