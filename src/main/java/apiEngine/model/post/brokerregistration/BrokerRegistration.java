package apiEngine.model.post.brokerregistration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BrokerRegistration {
    @JsonProperty("delivery")
    private Delivery delivery;
    @JsonProperty("licensePlateHolder")
    private LicensePlateHolder licensePlateHolder;
    @JsonProperty("registrationInfo")
    private RegistrationInfo registrationInfo;
    @JsonProperty("seller")
    private Seller seller;
    @JsonProperty("vehicle")
    private Vehicle vehicle;
    @JsonProperty("insurance")
    private Insurance insurance;
    @JsonProperty("leasingCompanyOrderDossierNumber")
    private String leasingCompanyOrderDossierNumber;
    @JsonProperty("plannedRegistrationDate")
    private String plannedRegistrationDate;
    @JsonProperty("formNumber")
    private String formNumber;

    @JsonProperty("delivery")
    public Delivery getDelivery() {
        return delivery;
    }

    @JsonProperty("delivery")
    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    @JsonProperty("licensePlateHolder")
    public LicensePlateHolder getLicensePlateHolder() {
        return licensePlateHolder;
    }

    @JsonProperty("licensePlateHolder")
    public void setLicensePlateHolder(LicensePlateHolder licensePlateHolder) {
        this.licensePlateHolder = licensePlateHolder;
    }

    @JsonProperty("registrationInfo")
    public RegistrationInfo getRegistrationInfo() {
        return registrationInfo;
    }

    @JsonProperty("registrationInfo")
    public void setRegistrationInfo(RegistrationInfo registrationInfo) {
        this.registrationInfo = registrationInfo;
    }

    @JsonProperty("seller")
    public Seller getSeller() {
        return seller;
    }

    @JsonProperty("seller")
    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    @JsonProperty("vehicle")
    public Vehicle getVehicle() {
        return vehicle;
    }

    @JsonProperty("vehicle")
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @JsonProperty("insurance")
    public Insurance getInsurance() {
        return insurance;
    }

    @JsonProperty("insurance")
    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    @JsonProperty("leasingCompanyOrderDossierNumber")
    public String getLeasingCompanyOrderDossierNumber() {
        return leasingCompanyOrderDossierNumber;
    }

    @JsonProperty("leasingCompanyOrderDossierNumber")
    public void setLeasingCompanyOrderDossierNumber(String leasingCompanyOrderDossierNumber) {
        this.leasingCompanyOrderDossierNumber = leasingCompanyOrderDossierNumber;
    }

    @JsonProperty("plannedRegistrationDate")
    public String getPlannedRegistrationDate() {
        return plannedRegistrationDate;
    }

    @JsonProperty("plannedRegistrationDate")
    public void setPlannedRegistrationDate(String plannedRegistrationDate) {
        this.plannedRegistrationDate = plannedRegistrationDate;
    }

    @JsonProperty("formNumber")
    public String getFormNumber() {
        return formNumber;
    }

    @JsonProperty("formNumber")
    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }
}
