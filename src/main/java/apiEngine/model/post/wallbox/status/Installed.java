package apiEngine.model.post.wallbox.status;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "evbNumber",
        "chargeCardNumber",
        "cardSent",
        "installationDate",
        "certificationDate"
})

public class Installed {
    @JsonProperty("evbNumber")
    private String evbNumber;
    @JsonProperty("chargeCardNumber")
    private String chargeCardNumber;
    @JsonProperty("cardSent")
    private Boolean cardSent;
    @JsonProperty("installationDate")
    private String installationDate;
    @JsonProperty("certificationDate")
    private String certificationDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("evbNumber")
    public String getEvbNumber() {
        return evbNumber;
    }

    @JsonProperty("evbNumber")
    public void setEvbNumber(String evbNumber) {
        this.evbNumber = evbNumber;
    }

    @JsonProperty("chargeCardNumber")
    public String getChargeCardNumber() {
        return chargeCardNumber;
    }

    @JsonProperty("chargeCardNumber")
    public void setChargeCardNumber(String chargeCardNumber) {
        this.chargeCardNumber = chargeCardNumber;
    }

    @JsonProperty("cardSent")
    public Boolean getCardSent() {
        return cardSent;
    }

    @JsonProperty("cardSent")
    public void setCardSent(Boolean cardSent) {
        this.cardSent = cardSent;
    }

    @JsonProperty("installationDate")
    public String getInstallationDate() {
        return installationDate;
    }

    @JsonProperty("installationDate")
    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    @JsonProperty("certificationDate")
    public String getCertificationDate() {
        return certificationDate;
    }

    @JsonProperty("certificationDate")
    public void setCertificationDate(String certificationDate) {
        this.certificationDate = certificationDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
