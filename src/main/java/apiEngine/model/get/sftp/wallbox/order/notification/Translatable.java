
package apiEngine.model.get.sftp.wallbox.order.notification;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for Translatable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Translatable"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="Dutch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="French" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="English" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Translatable", namespace = "http://be/renta/ordering/communication/wallboxnotification", propOrder = {

})
public class Translatable {

    @XmlElement(name = "Dutch", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected String dutch;
    @XmlElement(name = "French", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected String french;
    @XmlElement(name = "English", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected String english;

    /**
     * Gets the value of the dutch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutch() {
        return dutch;
    }

    /**
     * Sets the value of the dutch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutch(String value) {
        this.dutch = value;
    }

    /**
     * Gets the value of the french property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrench() {
        return french;
    }

    /**
     * Sets the value of the french property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrench(String value) {
        this.french = value;
    }

    /**
     * Gets the value of the english property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnglish() {
        return english;
    }

    /**
     * Sets the value of the english property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnglish(String value) {
        this.english = value;
    }

}
