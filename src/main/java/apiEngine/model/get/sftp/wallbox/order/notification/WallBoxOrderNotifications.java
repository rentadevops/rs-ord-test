
package apiEngine.model.get.sftp.wallbox.order.notification;

//import javax.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LeasingCompanyRSNumber" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="OrderNotification" type="{http://be/renta/ordering/communication/wallboxnotification}WallBoxOrderNotification" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "leasingCompanyRSNumber",
    "orderNotification"
})
@XmlRootElement(name = "WallBoxOrderNotifications", namespace = "http://be/renta/ordering/communication/wallboxnotification")
public class WallBoxOrderNotifications {

    @XmlElement(name = "LeasingCompanyRSNumber", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected long leasingCompanyRSNumber;
    @XmlElement(name = "OrderNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    protected List<WallBoxOrderNotification> orderNotification;

    /**
     * Gets the value of the leasingCompanyRSNumber property.
     * 
     */
    public long getLeasingCompanyRSNumber() {
        return leasingCompanyRSNumber;
    }

    /**
     * Sets the value of the leasingCompanyRSNumber property.
     * 
     */
    public void setLeasingCompanyRSNumber(long value) {
        this.leasingCompanyRSNumber = value;
    }

    /**
     * Gets the value of the orderNotification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orderNotification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderNotification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WallBoxOrderNotification }
     * 
     * 
     */
    public List<WallBoxOrderNotification> getOrderNotification() {
        if (orderNotification == null) {
            orderNotification = new ArrayList<WallBoxOrderNotification>();
        }
        return this.orderNotification;
    }

}
