
package apiEngine.model.get.sftp.wallbox.order.notification;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for WallBoxOrderNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WallBoxOrderNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="DealerAcceptedNotification" type="{http://be/renta/ordering/communication/wallboxnotification}DealerAcceptedNotification"/&gt;
 *         &lt;element name="NotAcceptedNotification" type="{http://be/renta/ordering/communication/wallboxnotification}NotAcceptedNotification"/&gt;
 *         &lt;element name="OrderedNotification" type="{http://be/renta/ordering/communication/wallboxnotification}OrderedNotification"/&gt;
 *         &lt;element name="ReadyToActivateNotification" type="{http://be/renta/ordering/communication/wallboxnotification}ReadyToActivateNotification"/&gt;
 *         &lt;element name="ActivatedNotification" type="{http://be/renta/ordering/communication/wallboxnotification}ActivatedNotification"/&gt;
 *         &lt;element name="CommentsNotification" type="{http://be/renta/ordering/communication/wallboxnotification}CommentsNotification"/&gt;
 *         &lt;element name="InstalledNotification" type="{http://be/renta/ordering/communication/wallboxnotification}InstalledNotification"/&gt;
 *         &lt;element name="CancelledNotification" type="{http://be/renta/ordering/communication/wallboxnotification}CancelledNotification"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WallBoxOrderNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification", propOrder = {
    "dealerAcceptedNotification",
    "notAcceptedNotification",
    "orderedNotification",
    "readyToActivateNotification",
    "activatedNotification",
    "commentsNotification",
    "installedNotification",
    "cancelledNotification"
})
public class WallBoxOrderNotification {

    @XmlElement(name = "DealerAcceptedNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected DealerAcceptedNotification dealerAcceptedNotification;
    @XmlElement(name = "NotAcceptedNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected NotAcceptedNotification notAcceptedNotification;
    @XmlElement(name = "OrderedNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected OrderedNotification orderedNotification;
    @XmlElement(name = "ReadyToActivateNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected ReadyToActivateNotification readyToActivateNotification;
    @XmlElement(name = "ActivatedNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected ActivatedNotification activatedNotification;
    @XmlElement(name = "CommentsNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected CommentsNotification commentsNotification;
    @XmlElement(name = "InstalledNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected InstalledNotification installedNotification;
    @XmlElement(name = "CancelledNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected CancelledNotification cancelledNotification;

    /**
     * Gets the value of the dealerAcceptedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link DealerAcceptedNotification }
     *     
     */
    public DealerAcceptedNotification getDealerAcceptedNotification() {
        return dealerAcceptedNotification;
    }

    /**
     * Sets the value of the dealerAcceptedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerAcceptedNotification }
     *     
     */
    public void setDealerAcceptedNotification(DealerAcceptedNotification value) {
        this.dealerAcceptedNotification = value;
    }

    /**
     * Gets the value of the notAcceptedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link NotAcceptedNotification }
     *     
     */
    public NotAcceptedNotification getNotAcceptedNotification() {
        return notAcceptedNotification;
    }

    /**
     * Sets the value of the notAcceptedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotAcceptedNotification }
     *     
     */
    public void setNotAcceptedNotification(NotAcceptedNotification value) {
        this.notAcceptedNotification = value;
    }

    /**
     * Gets the value of the orderedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link OrderedNotification }
     *     
     */
    public OrderedNotification getOrderedNotification() {
        return orderedNotification;
    }

    /**
     * Sets the value of the orderedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderedNotification }
     *     
     */
    public void setOrderedNotification(OrderedNotification value) {
        this.orderedNotification = value;
    }

    /**
     * Gets the value of the readyToActivateNotification property.
     * 
     * @return
     *     possible object is
     *     {@link ReadyToActivateNotification }
     *     
     */
    public ReadyToActivateNotification getReadyToActivateNotification() {
        return readyToActivateNotification;
    }

    /**
     * Sets the value of the readyToActivateNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadyToActivateNotification }
     *     
     */
    public void setReadyToActivateNotification(ReadyToActivateNotification value) {
        this.readyToActivateNotification = value;
    }

    /**
     * Gets the value of the activatedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link ActivatedNotification }
     *     
     */
    public ActivatedNotification getActivatedNotification() {
        return activatedNotification;
    }

    /**
     * Sets the value of the activatedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivatedNotification }
     *     
     */
    public void setActivatedNotification(ActivatedNotification value) {
        this.activatedNotification = value;
    }

    /**
     * Gets the value of the commentsNotification property.
     * 
     * @return
     *     possible object is
     *     {@link CommentsNotification }
     *     
     */
    public CommentsNotification getCommentsNotification() {
        return commentsNotification;
    }

    /**
     * Sets the value of the commentsNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommentsNotification }
     *     
     */
    public void setCommentsNotification(CommentsNotification value) {
        this.commentsNotification = value;
    }

    /**
     * Gets the value of the installedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link InstalledNotification }
     *     
     */
    public InstalledNotification getInstalledNotification() {
        return installedNotification;
    }

    /**
     * Sets the value of the installedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstalledNotification }
     *     
     */
    public void setInstalledNotification(InstalledNotification value) {
        this.installedNotification = value;
    }

    /**
     * Gets the value of the cancelledNotification property.
     * 
     * @return
     *     possible object is
     *     {@link CancelledNotification }
     *     
     */
    public CancelledNotification getCancelledNotification() {
        return cancelledNotification;
    }

    /**
     * Sets the value of the cancelledNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelledNotification }
     *     
     */
    public void setCancelledNotification(CancelledNotification value) {
        this.cancelledNotification = value;
    }

}
