
package apiEngine.model.get.sftp.wallbox.order.notification;

//import javax.xml.bind.annotation.XmlEnum;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

/**
 * <p>Java class for TransactionCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="TransactionCode"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NOT_ACCEPTED"/&gt;
 *     &lt;enumeration value="DEALER_ACCEPTED"/&gt;
 *     &lt;enumeration value="ORDERED"/&gt;
 *     &lt;enumeration value="INSTALLED"/&gt;
 *     &lt;enumeration value="READY_TO_ACTIVATE"/&gt;
 *     &lt;enumeration value="ACTIVATED"/&gt;
 *     &lt;enumeration value="COMMENTS"/&gt;
 *     &lt;enumeration value="CANCELLED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TransactionCode", namespace = "http://be/renta/ordering/communication/wallboxnotification")
@XmlEnum
public enum TransactionCode {

    NOT_ACCEPTED,
    DEALER_ACCEPTED,
    ORDERED,
    INSTALLED,
    READY_TO_ACTIVATE,
    ACTIVATED,
    COMMENTS,
    CANCELLED;

    public String value() {
        return name();
    }

    public static TransactionCode fromValue(String v) {
        return valueOf(v);
    }

}
