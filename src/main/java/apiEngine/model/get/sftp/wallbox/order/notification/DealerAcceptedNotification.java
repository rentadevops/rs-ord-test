
package apiEngine.model.get.sftp.wallbox.order.notification;

//import javax.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.*;
//import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DealerAcceptedNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DealerAcceptedNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://be/renta/ordering/communication/wallboxnotification}AbstractNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExpectedInstallationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerAcceptedNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification", propOrder = {
    "expectedInstallationDate"
})
public class DealerAcceptedNotification
    extends AbstractNotification
{

    @XmlElement(name = "ExpectedInstallationDate", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    @XmlSchemaType(name = "date")
    protected String expectedInstallationDate;

    /**
     * Gets the value of the expectedInstallationDate property.
     * 
     * @return
     *
     */
    public String getExpectedInstallationDate() {
        return expectedInstallationDate;
    }

    /**
     * Sets the value of the expectedInstallationDate property.
     * 
     * @param value
     *
     */
    public void setExpectedInstallationDate(String value) {
        this.expectedInstallationDate = value;
    }

}
