package apiEngine.model.get.sftp.order.notification;

import java.io.Serializable;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;

//import org.renta.javaclass.ConfirmedCancelNotification;
//import org.renta.javaclass.DealerAcceptedNotification;
//import org.renta.javaclass.DeliveredNotification;
//import org.renta.javaclass.LicensePlateNotification;
//import org.renta.javaclass.NotAcceptedNotification;
//import org.renta.javaclass.OrderedNotification;
//import org.renta.javaclass.UpdateNotification;
//import org.renta.javaclass.VehicleInNotification;

/**
 * <p>Java class for OrderNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderNotification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="DealerAcceptedNotification" type="{http://be/renta/ordering/communication/ordernotification}DealerAcceptedNotification"/>
 *         &lt;element name="OrderedNotification" type="{http://be/renta/ordering/communication/ordernotification}OrderedNotification"/>
 *         &lt;element name="VehicleInNotification" type="{http://be/renta/ordering/communication/ordernotification}VehicleInNotification"/>
 *         &lt;element name="LicensePlateNotification" type="{http://be/renta/ordering/communication/ordernotification}LicensePlateNotification"/>
 *         &lt;element name="DeliveredNotification" type="{http://be/renta/ordering/communication/ordernotification}DeliveredNotification"/>
 *         &lt;element name="UpdateNotification" type="{http://be/renta/ordering/communication/ordernotification}UpdateNotification"/>
 *         &lt;element name="NotAcceptedNotification" type="{http://be/renta/ordering/communication/ordernotification}NotAcceptedNotification"/>
 *         &lt;element name="ConfirmedCancelNotification" type="{http://be/renta/ordering/communication/ordernotification}ConfirmedCancelNotification"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderNotification", propOrder = {
		"dealerAcceptedNotification",
	    "bikeDealerAcceptedNotification",
	    "orderedNotification",
	    "bikeOrderedNotification",
	    "vehicleInNotification",
	    "bikeVehicleInNotification",
	    "licensePlateNotification",
	    "bikeLicensePlateNotification",
	    "deliveredNotification",
	    "bikeDeliveredNotification",
	    "updateNotification",
	    "bikeUpdateNotification",
	    "notAcceptedNotification",
	    "bikeNotAcceptedNotification",
	    "confirmedCancelNotification",
	    "bikeConfirmedCancelNotification",
	    "commentsNotification",
	    "bikeCommentsNotification"
})
public class OrderNotification
    implements Serializable
{

    private final static long serialVersionUID = 12343L;
    @XmlElement(name = "CommentsNotification")
    protected CommentsNotification commentsNotification;
    @XmlElement(name = "BikeCommentsNotification")
    protected CommentsNotification bikeCommentsNotification;
    @XmlElement(name = "DealerAcceptedNotification")
    protected DealerAcceptedNotification dealerAcceptedNotification;
    @XmlElement(name = "BikeDealerAcceptedNotification")
    protected BikeDealerAcceptedNotification bikeDealerAcceptedNotification;
    @XmlElement(name = "OrderedNotification")
    protected OrderedNotification orderedNotification;
    @XmlElement(name = "BikeOrderedNotification")
    protected OrderedNotification bikeOrderedNotification;
    @XmlElement(name = "VehicleInNotification")
    protected VehicleInNotification vehicleInNotification;
    @XmlElement(name = "BikeVehicleInNotification")
    protected BikeVehicleInNotification bikeVehicleInNotification;
    @XmlElement(name = "LicensePlateNotification")
    protected LicensePlateNotification licensePlateNotification;
    @XmlElement(name = "BikeLicensePlateNotification")
    protected LicensePlateNotification bikeLicensePlateNotification;
    @XmlElement(name = "DeliveredNotification")
    protected DeliveredNotification deliveredNotification;
    @XmlElement(name = "BikeDeliveredNotification")
    protected BikeDeliveredNotification bikeDeliveredNotification;
    @XmlElement(name = "UpdateNotification")
    protected UpdateNotification updateNotification;
    @XmlElement(name = "BikeUpdateNotification")
    protected UpdateNotification bikeUpdateNotification;
    @XmlElement(name = "NotAcceptedNotification")
    protected NotAcceptedNotification notAcceptedNotification;
    @XmlElement(name = "BikeNotAcceptedNotification")
    protected NotAcceptedNotification bikeNotAcceptedNotification;
    @XmlElement(name = "ConfirmedCancelNotification")
    protected ConfirmedCancelNotification confirmedCancelNotification;
    @XmlElement(name = "BikeConfirmedCancelNotification")
    protected ConfirmedCancelNotification bikeConfirmedCancelNotification;
    
    /**
     * Gets the value of the commentsNotification property.
     * 
     * @return
     *     possible object is
     *     {@link CommentsNotification }
     *     
     */
    public CommentsNotification getCommentsNotification() {
        return commentsNotification;
    }

    /**
     * Sets the value of the dealerAcceptedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerAcceptedNotification }
     *     
     */
    public void setCommentsNotification(CommentsNotification value) {
        this.commentsNotification = value;
    }
    
    /**
     * Gets the value of the bikeCommentsNotification property.
     * 
     * @return
     *     possible object is
     *     {@link CommentsNotification }
     *     
     */
    public CommentsNotification getBikeCommentsNotification() {
        return bikeCommentsNotification;
    }

    /**
     * Sets the value of the bikeCommentsNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommentsNotification }
     *     
     */
    public void setBikeCommentsNotification(CommentsNotification value) {
        this.bikeCommentsNotification = value;
    }

    /**
     * Gets the value of the dealerAcceptedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link DealerAcceptedNotification }
     *     
     */
    public DealerAcceptedNotification getDealerAcceptedNotification() {
        return dealerAcceptedNotification;
    }

    /**
     * Sets the value of the dealerAcceptedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerAcceptedNotification }
     *     
     */
    public void setDealerAcceptedNotification(DealerAcceptedNotification value) {
        this.dealerAcceptedNotification = value;
    }
    
    /**
     * Gets the value of the bikeDealerAcceptedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link BikeDealerAcceptedNotification }
     *     
     */
    public BikeDealerAcceptedNotification getBikeDealerAcceptedNotification() {
        return bikeDealerAcceptedNotification;
    }

    /**
     * Sets the value of the bikeDealerAcceptedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link BikeDealerAcceptedNotification }
     *     
     */
    public void setBikeDealerAcceptedNotification(BikeDealerAcceptedNotification value) {
        this.bikeDealerAcceptedNotification = value;
    }

    /**
     * Gets the value of the orderedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link OrderedNotification }
     *     
     */
    public OrderedNotification getOrderedNotification() {
        return orderedNotification;
    }

    /**
     * Sets the value of the orderedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderedNotification }
     *     
     */
    public void setOrderedNotification(OrderedNotification value) {
        this.orderedNotification = value;
    }
    
    /**
     * Gets the value of the bikeOrderedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link OrderedNotification }
     *     
     */
    public OrderedNotification getBikeOrderedNotification() {
        return bikeOrderedNotification;
    }

    /**
     * Sets the value of the bikeOrderedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderedNotification }
     *     
     */
    public void setBikeOrderedNotification(OrderedNotification value) {
        this.bikeOrderedNotification = value;
    }

    /**
     * Gets the value of the vehicleInNotification property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleInNotification }
     *     
     */
    public VehicleInNotification getVehicleInNotification() {
        return vehicleInNotification;
    }

    /**
     * Sets the value of the vehicleInNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleInNotification }
     *     
     */
    public void setVehicleInNotification(VehicleInNotification value) {
        this.vehicleInNotification = value;
    }
    
    /**
     * Gets the value of the bikeVehicleInNotification property.
     * 
     * @return
     *     possible object is
     *     {@link BikeVehicleInNotification }
     *     
     */
    public BikeVehicleInNotification getBikeVehicleInNotification() {
        return bikeVehicleInNotification;
    }

    /**
     * Sets the value of the bikeVehicleInNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link BikeVehicleInNotification }
     *     
     */
    public void setBikeVehicleInNotification(BikeVehicleInNotification value) {
        this.bikeVehicleInNotification = value;
    }

    /**
     * Gets the value of the licensePlateNotification property.
     * 
     * @return
     *     possible object is
     *     {@link LicensePlateNotification }
     *     
     */
    public LicensePlateNotification getLicensePlateNotification() {
        return licensePlateNotification;
    }

    /**
     * Sets the value of the licensePlateNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicensePlateNotification }
     *     
     */
    public void setLicensePlateNotification(LicensePlateNotification value) {
        this.licensePlateNotification = value;
    }
    
    /**
     * Gets the value of the bikeLicensePlateNotification property.
     * 
     * @return
     *     possible object is
     *     {@link LicensePlateNotification }
     *     
     */
    public LicensePlateNotification getBikeLicensePlateNotification() {
        return bikeLicensePlateNotification;
    }

    /**
     * Sets the value of the bikeLicensePlateNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicensePlateNotification }
     *     
     */
    public void setBikeLicensePlateNotification(LicensePlateNotification value) {
        this.bikeLicensePlateNotification = value;
    }

    /**
     * Gets the value of the deliveredNotification property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveredNotification }
     *     
     */
    public DeliveredNotification getDeliveredNotification() {
        return deliveredNotification;
    }

    /**
     * Sets the value of the deliveredNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveredNotification }
     *     
     */
    public void setDeliveredNotification(DeliveredNotification value) {
        this.deliveredNotification = value;
    }
    
    /**
     * Gets the value of the bikeDeliveredNotification property.
     * 
     * @return
     *     possible object is
     *     {@link BikeDeliveredNotification }
     *     
     */
    public BikeDeliveredNotification getBikeDeliveredNotification() {
        return bikeDeliveredNotification;
    }

    /**
     * Sets the value of the bikeDeliveredNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link BikeDeliveredNotification }
     *     
     */
    public void setBikeDeliveredNotification(BikeDeliveredNotification value) {
        this.bikeDeliveredNotification = value;
    }

    /**
     * Gets the value of the updateNotification property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateNotification }
     *     
     */
    public UpdateNotification getUpdateNotification() {
        return updateNotification;
    }

    /**
     * Sets the value of the updateNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateNotification }
     *     
     */
    public void setUpdateNotification(UpdateNotification value) {
        this.updateNotification = value;
    }
    
    /**
     * Gets the value of the bikeUpdateNotification property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateNotification }
     *     
     */
    public UpdateNotification getBikeUpdateNotification() {
        return bikeUpdateNotification;
    }

    /**
     * Sets the value of the bikeUpdateNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateNotification }
     *     
     */
    public void setBikeUpdateNotification(UpdateNotification value) {
        this.bikeUpdateNotification = value;
    }

    /**
     * Gets the value of the notAcceptedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link NotAcceptedNotification }
     *     
     */
    public NotAcceptedNotification getNotAcceptedNotification() {
        return notAcceptedNotification;
    }

    /**
     * Sets the value of the notAcceptedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotAcceptedNotification }
     *     
     */
    public void setNotAcceptedNotification(NotAcceptedNotification value) {
        this.notAcceptedNotification = value;
    }
    
    /**
     * Gets the value of the bikeNotAcceptedNotification property.
     * 
     * @return
     *     possible object is
     *     {@link NotAcceptedNotification }
     *     
     */
    public NotAcceptedNotification getBikeNotAcceptedNotification() {
        return bikeNotAcceptedNotification;
    }

    /**
     * Sets the value of the bikeNotAcceptedNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotAcceptedNotification }
     *     
     */
    public void setBikeNotAcceptedNotification(NotAcceptedNotification value) {
        this.bikeNotAcceptedNotification = value;
    }

    /**
     * Gets the value of the confirmedCancelNotification property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmedCancelNotification }
     *     
     */
    public ConfirmedCancelNotification getConfirmedCancelNotification() {
        return confirmedCancelNotification;
    }

    /**
     * Sets the value of the confirmedCancelNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmedCancelNotification }
     *     
     */
    public void setConfirmedCancelNotification(ConfirmedCancelNotification value) {
        this.confirmedCancelNotification = value;
    }
    
    /**
     * Gets the value of the bikeConfirmedCancelNotification property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmedCancelNotification }
     *     
     */
    public ConfirmedCancelNotification getBikeConfirmedCancelNotification() {
        return bikeConfirmedCancelNotification;
    }

    /**
     * Sets the value of the bikeConfirmedCancelNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmedCancelNotification }
     *     
     */
    public void setBikeConfirmedCancelNotification(ConfirmedCancelNotification value) {
        this.bikeConfirmedCancelNotification = value;
    }

}
