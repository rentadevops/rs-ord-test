package apiEngine.model.get.sftp.order.notification;

import jakarta.xml.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LeasingCompanyRSNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="OrderNotification" type="{http://be/renta/ordering/communication/ordernotification}OrderNotification" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "leasingCompanyRSNumber",
    "orderNotification"
})
@XmlRootElement(name = "OrderNotifications")
public class OrderNotifications
    implements Serializable
{

    private final static long serialVersionUID = 12343L;
    @XmlElement(name = "LeasingCompanyRSNumber")
    protected long leasingCompanyRSNumber;
    @XmlElement(name = "OrderNotification")
    protected List<OrderNotification> orderNotification;

    /**
     * Gets the value of the leasingCompanyRSNumber property.
     * 
     */
    public long getLeasingCompanyRSNumber() {
        return leasingCompanyRSNumber;
    }

    /**
     * Sets the value of the leasingCompanyRSNumber property.
     * 
     */
    public void setLeasingCompanyRSNumber(long value) {
        this.leasingCompanyRSNumber = value;
    }

    /**
     * Gets the value of the orderNotification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderNotification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderNotification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderNotification }
     * 
     * 
     */
    public List<OrderNotification> getOrderNotification() {
        if (orderNotification == null) {
            orderNotification = new ArrayList<OrderNotification>();
        }
        return this.orderNotification;
    }

}
