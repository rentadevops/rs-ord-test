@XmlSchema(
    namespace = "http://be/renta/ordering/communication/ordernotification",
    elementFormDefault = XmlNsForm.QUALIFIED)
package apiEngine.model.get.sftp.order.notification;

import jakarta.xml.bind.annotation.*;
