package apiEngine.model.get.sftp.order.notification;

//import javax.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.*;

public class OrderedNotification {

	@XmlElement(name="NotificationDate")
	private String NotificationDate;
	@XmlElement(name="Comments")
    private String Comments;
	@XmlElement(name="TransactionCode")
    private String TransactionCode;
	@XmlElement(name="DossierNumber")
    private String DossierNumber;
    
    public OrderedNotification() {
    	
    }
    
    public OrderedNotification(String NotificationDate, String Comments, String TransactionCode, String DossierNumber) {
    	super();
    	this.NotificationDate = NotificationDate;
    	this.Comments = Comments;
    	this.TransactionCode = TransactionCode;
    	this.DossierNumber = DossierNumber;
    }

    public String getNotificationDate ()
    {
        return NotificationDate;
    }

    public void setNotificationDate (String NotificationDate)
    {
        this.NotificationDate = NotificationDate;
    }

    public String getComments ()
    {
        return Comments;
    }

    public void setComments (String Comments)
    {
        this.Comments = Comments;
    }

    public String getTransactionCode ()
    {
        return TransactionCode;
    }

    public void setTransactionCode (String TransactionCode)
    {
        this.TransactionCode = TransactionCode;
    }

    public String getDossierNumber ()
    {
        return DossierNumber;
    }

    public void setDossierNumber (String DossierNumber)
    {
        this.DossierNumber = DossierNumber;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [NotificationDate = "+NotificationDate+", Comments = "+Comments+", TransactionCode = "+TransactionCode+", DossierNumber = "+DossierNumber+"]";
    }

}
