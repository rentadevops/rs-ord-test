package apiEngine.model.get.leasing.company.actor;

public class LeasingCompanyActor {
	
	private Address address;
	private String commercialName;
	private String contactEmail;
	private String contactFullName;
	private String contactMobileNumber;
	private String contactPhoneNumber;
	private String enterpriseNumber;
	private String invoiceName;
	private String language;
	private Integer rsNumber;

	/**
	* No args constructor for use in serialization
	*
	*/
	public LeasingCompanyActor() {
	}

	/**
	*
	* @param contactMobileNumber
	* @param address
	* @param invoiceName
	* @param contactFullName
	* @param contactEmail
	* @param enterpriseNumber
	* @param contactPhoneNumber
	* @param language
	* @param rsNumber
	* @param commercialName
	*/
	public LeasingCompanyActor(Address address, String commercialName, String contactEmail, String contactFullName, String contactMobileNumber, String contactPhoneNumber, String enterpriseNumber, String invoiceName, String language, Integer rsNumber) {
	super();
	this.address = address;
	this.commercialName = commercialName;
	this.contactEmail = contactEmail;
	this.contactFullName = contactFullName;
	this.contactMobileNumber = contactMobileNumber;
	this.contactPhoneNumber = contactPhoneNumber;
	this.enterpriseNumber = enterpriseNumber;
	this.invoiceName = invoiceName;
	this.language = language;
	this.rsNumber = rsNumber;
	}

	public Address getAddress() {
	return address;
	}

	public void setAddress(Address address) {
	this.address = address;
	}

	public String getCommercialName() {
	return commercialName;
	}

	public void setCommercialName(String commercialName) {
	this.commercialName = commercialName;
	}

	public String getContactEmail() {
	return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
	this.contactEmail = contactEmail;
	}

	public String getContactFullName() {
	return contactFullName;
	}

	public void setContactFullName(String contactFullName) {
	this.contactFullName = contactFullName;
	}

	public String getContactMobileNumber() {
	return contactMobileNumber;
	}

	public void setContactMobileNumber(String contactMobileNumber) {
	this.contactMobileNumber = contactMobileNumber;
	}

	public String getContactPhoneNumber() {
	return contactPhoneNumber;
	}

	public void setContactPhoneNumber(String contactPhoneNumber) {
	this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getEnterpriseNumber() {
	return enterpriseNumber;
	}

	public void setEnterpriseNumber(String enterpriseNumber) {
	this.enterpriseNumber = enterpriseNumber;
	}

	public String getInvoiceName() {
	return invoiceName;
	}

	public void setInvoiceName(String invoiceName) {
	this.invoiceName = invoiceName;
	}

	public String getLanguage() {
	return language;
	}

	public void setLanguage(String language) {
	this.language = language;
	}

	public Integer getRsNumber() {
	return rsNumber;
	}

	public void setRsNumber(Integer rsNumber) {
	this.rsNumber = rsNumber;
	}

}
