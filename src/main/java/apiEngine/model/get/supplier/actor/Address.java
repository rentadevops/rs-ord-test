package apiEngine.model.get.supplier.actor;

public class Address {
	
	private String street;
	private String houseNumber;
	private ZipCode zipCode;
	private String city;
	private String country;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Address() {
	}

	/**
	*
	* @param zipCode
	* @param country
	* @param city
	* @param street
	* @param houseNumber
	*/
	public Address(String street, String houseNumber, ZipCode zipCode, String city, String country) {
	super();
	this.street = street;
	this.houseNumber = houseNumber;
	this.zipCode = zipCode;
	this.city = city;
	this.country = country;
	}

	public String getStreet() {
	return street;
	}

	public void setStreet(String street) {
	this.street = street;
	}

	public String getHouseNumber() {
	return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
	this.houseNumber = houseNumber;
	}

	public ZipCode getZipCode() {
	return zipCode;
	}

	public void setZipCode(ZipCode zipCode) {
	this.zipCode = zipCode;
	}

	public String getCity() {
	return city;
	}

	public void setCity(String city) {
	this.city = city;
	}

	public String getCountry() {
	return country;
	}

	public void setCountry(String country) {
	this.country = country;
	}

}
