package apiEngine.model.get.supplier.actor;

public class ZipCode {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public ZipCode() {
	}

	/**
	*
	* @param value
	*/
	public ZipCode(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
	return value;
	}

	public void setValue(String value) {
	this.value = value;
	}

}
