package apiEngine.model.get.activitydefect.task.warranty;

public class Description__1 {
	public String de;
	public String en;
	public String fr;
	public String nl;
	
	public Description__1() {
		
	}

	public Description__1(String de, String en, String fr, String nl) {
	super();
	this.de = de;
	this.en = en;
	this.fr = fr;
	this.nl = nl;
	}

	public String getDe() {
		return de;
	}

	public void setDe(String de) {
		this.de = de;
	}

	public String getEn() {
		return en;
	}

	public void setEn(String en) {
		this.en = en;
	}

	public String getFr() {
		return fr;
	}

	public void setFr(String fr) {
		this.fr = fr;
	}

	public String getNl() {
		return nl;
	}

	public void setNl(String nl) {
		this.nl = nl;
	}
	
	
}
