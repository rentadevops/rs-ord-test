package apiEngine.model.get.activitydefect.task.warranty;

public class ActivityDefect {
	public String code;
	public Description description;

	public ActivityDefect() {
		
	}

	public ActivityDefect(String code, Description description) {
	super();
	this.code = code;
	this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}
	
	
}
