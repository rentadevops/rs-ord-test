package apiEngine.model.get.activitydefect.task.warranty;

public class ActivityTask {
	public String code;
	public Description__2 description;

	public ActivityTask() {
		
	}

	public ActivityTask(String code, Description__2 description) {
	super();
	this.code = code;
	this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Description__2 getDescription() {
		return description;
	}

	public void setDescription(Description__2 description) {
		this.description = description;
	}
	
	
}
