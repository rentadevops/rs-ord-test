package apiEngine.model.get.activitydefect.task.warranty;

public class ActivityDefectTaskWarranty {
	public ActivityDefect activityDefect;
	public ActivityDefinition activityDefinition;
	public ActivityTask activityTask;
	public Integer id;
	public Integer version;
	public Boolean warranty;

	public ActivityDefectTaskWarranty() {
		
	}

	public ActivityDefectTaskWarranty(ActivityDefect activityDefect, ActivityDefinition activityDefinition, ActivityTask activityTask, Integer id, Integer version, Boolean warranty) {
	super();
	this.activityDefect = activityDefect;
	this.activityDefinition = activityDefinition;
	this.activityTask = activityTask;
	this.id = id;
	this.version = version;
	this.warranty = warranty;
	}

	public ActivityDefect getActivityDefect() {
		return activityDefect;
	}

	public void setActivityDefect(ActivityDefect activityDefect) {
		this.activityDefect = activityDefect;
	}

	public ActivityDefinition getActivityDefinition() {
		return activityDefinition;
	}

	public void setActivityDefinition(ActivityDefinition activityDefinition) {
		this.activityDefinition = activityDefinition;
	}

	public ActivityTask getActivityTask() {
		return activityTask;
	}

	public void setActivityTask(ActivityTask activityTask) {
		this.activityTask = activityTask;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getWarranty() {
		return warranty;
	}

	public void setWarranty(Boolean warranty) {
		this.warranty = warranty;
	}
	
	
}
