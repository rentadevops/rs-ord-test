package apiEngine.model.get.activitydefect.task.warranty;

public class ActivityDefinition {
	public String code;
	public Description__1 description;

	public ActivityDefinition() {
		
	}

	public ActivityDefinition(String code, Description__1 description) {
	super();
	this.code = code;
	this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Description__1 getDescription() {
		return description;
	}

	public void setDescription(Description__1 description) {
		this.description = description;
	}
	
	
}
