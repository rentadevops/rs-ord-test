package apiEngine.model.get.registration.registration;

public class Vin {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
