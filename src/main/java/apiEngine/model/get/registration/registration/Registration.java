package apiEngine.model.get.registration.registration;

public class Registration {
    private String registrationId;
    private String registrationStatus;
    private String insuranceCompanyName;
    private String insuranceCompanyFsmaNumber;
    private Vin vin;
    private Object licensePlate;
    private String licensePlateHolderName;
    private String applicantName;
    private Boolean leasingCompanyHasUnseenMessages;
    private Boolean leasingCompanyHasUnseenDocuments;
    private String insuranceBrokerName;
    private String insuranceBrokerFsmaNumber;
    private String leasingCompanyOrderDossierNumber;
    private Long plannedRegistrationDate;
    private final static long serialVersionUID = 175968165251056075L;

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public String getInsuranceCompanyFsmaNumber() {
        return insuranceCompanyFsmaNumber;
    }

    public void setInsuranceCompanyFsmaNumber(String insuranceCompanyFsmaNumber) {
        this.insuranceCompanyFsmaNumber = insuranceCompanyFsmaNumber;
    }

    public Vin getVin() {
        return vin;
    }

    public void setVin(Vin vin) {
        this.vin = vin;
    }

    public Object getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(Object licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getLicensePlateHolderName() {
        return licensePlateHolderName;
    }

    public void setLicensePlateHolderName(String licensePlateHolderName) {
        this.licensePlateHolderName = licensePlateHolderName;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public Boolean getLeasingCompanyHasUnseenMessages() {
        return leasingCompanyHasUnseenMessages;
    }

    public void setLeasingCompanyHasUnseenMessages(Boolean leasingCompanyHasUnseenMessages) {
        this.leasingCompanyHasUnseenMessages = leasingCompanyHasUnseenMessages;
    }

    public Boolean getLeasingCompanyHasUnseenDocuments() {
        return leasingCompanyHasUnseenDocuments;
    }

    public void setLeasingCompanyHasUnseenDocuments(Boolean leasingCompanyHasUnseenDocuments) {
        this.leasingCompanyHasUnseenDocuments = leasingCompanyHasUnseenDocuments;
    }

    public String getInsuranceBrokerName() {
        return insuranceBrokerName;
    }

    public void setInsuranceBrokerName(String insuranceBrokerName) {
        this.insuranceBrokerName = insuranceBrokerName;
    }

    public String getInsuranceBrokerFsmaNumber() {
        return insuranceBrokerFsmaNumber;
    }

    public void setInsuranceBrokerFsmaNumber(String insuranceBrokerFsmaNumber) {
        this.insuranceBrokerFsmaNumber = insuranceBrokerFsmaNumber;
    }

    public String getLeasingCompanyOrderDossierNumber() {
        return leasingCompanyOrderDossierNumber;
    }

    public void setLeasingCompanyOrderDossierNumber(String leasingCompanyOrderDossierNumber) {
        this.leasingCompanyOrderDossierNumber = leasingCompanyOrderDossierNumber;
    }

    public Long getPlannedRegistrationDate() {
        return plannedRegistrationDate;
    }

    public void setPlannedRegistrationDate(Long plannedRegistrationDate) {
        this.plannedRegistrationDate = plannedRegistrationDate;
    }
}
