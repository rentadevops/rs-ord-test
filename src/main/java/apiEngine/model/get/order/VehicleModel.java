package apiEngine.model.get.order;

public class VehicleModel {
	
	public Translations translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public VehicleModel() {
	}

	/**
	*
	* @param translations
	*/
	public VehicleModel(Translations translations) {
	super();
	this.translations = translations;
	}

	public Translations getTranslations() {
		return translations;
	}

	public void setTranslations(Translations translations) {
		this.translations = translations;
	}

}
