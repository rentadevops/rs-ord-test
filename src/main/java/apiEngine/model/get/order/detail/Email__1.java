package apiEngine.model.get.order.detail;

public class Email__1 {
	
	public String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Email__1() {
	}

	/**
	*
	* @param value
	*/
	public Email__1(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
