package apiEngine.model.get.order.detail;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Translations__1 {
	
	private String additionalProp1;
	private String additionalProp2;
	private String additionalProp3;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Translations__1() {
	}

	/**
	*
	* @param additionalProp1
	* @param additionalProp3
	* @param additionalProp2
	*/
	public Translations__1(String additionalProp1, String additionalProp2, String additionalProp3) {
	super();
	this.additionalProp1 = additionalProp1;
	this.additionalProp2 = additionalProp2;
	this.additionalProp3 = additionalProp3;
	}

	public String getAdditionalProp1() {
		return additionalProp1;
	}

	public void setAdditionalProp1(String additionalProp1) {
		this.additionalProp1 = additionalProp1;
	}

	public String getAdditionalProp2() {
		return additionalProp2;
	}

	public void setAdditionalProp2(String additionalProp2) {
		this.additionalProp2 = additionalProp2;
	}

	public String getAdditionalProp3() {
		return additionalProp3;
	}

	public void setAdditionalProp3(String additionalProp3) {
		this.additionalProp3 = additionalProp3;
	}

}
