package apiEngine.model.get.order.detail;

public class Address__4 {

	public String city;
	public String country;
	public String houseNumber;
	public String street;
	public ZipCode__4 zipCode;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Address__4() {
	}

	/**
	*
	* @param country
	* @param zipCode
	* @param city
	* @param street
	* @param houseNumber
	*/
	public Address__4(String city, String country, String houseNumber, String street, ZipCode__4 zipCode) {
	super();
	this.city = city;
	this.country = country;
	this.houseNumber = houseNumber;
	this.street = street;
	this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public ZipCode__4 getZipCode() {
		return zipCode;
	}

	public void setZipCode(ZipCode__4 zipCode) {
		this.zipCode = zipCode;
	}
	
}
