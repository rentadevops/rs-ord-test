package apiEngine.model.get.order.detail;

import java.math.BigInteger;

public class Delivery {
	
	private Long actualDeliveryDate;
	private Address__3 address;
	private String contactEmail;
	private String contactPerson;
	private String deliveryContact;
	private Long desiredDate;
	private Long expectedDeliveryDate;
	private FaxNumber__3 faxNumber;
	private String locationLanguage;
	private String locationName;
	private String locationVatNumber;
	private MobileNumber__3 mobileNumber;
	private PhoneNumber__3 phoneNumber;
	private Long proposedDeliveryDate;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Delivery() {
	}

	/**
	*
	* @param proposedDeliveryDate
	* @param actualDeliveryDate
	* @param desiredDate
	* @param address
	* @param locationName
	* @param contactEmail
	* @param expectedDeliveryDate
	* @param mobileNumber
	* @param contactPerson
	* @param phoneNumber
	* @param deliveryContact
	* @param faxNumber
	* @param locationLanguage
	* @param locationVatNumber
	*/
	public Delivery(Long actualDeliveryDate, Address__3 address, String contactEmail, String contactPerson, String deliveryContact, Long desiredDate, Long expectedDeliveryDate, FaxNumber__3 faxNumber, String locationLanguage, String locationName, String locationVatNumber, MobileNumber__3 mobileNumber, PhoneNumber__3 phoneNumber, Long proposedDeliveryDate) {
	super();
	this.actualDeliveryDate = actualDeliveryDate;
	this.address = address;
	this.contactEmail = contactEmail;
	this.contactPerson = contactPerson;
	this.deliveryContact = deliveryContact;
	this.desiredDate = desiredDate;
	this.expectedDeliveryDate = expectedDeliveryDate;
	this.faxNumber = faxNumber;
	this.locationLanguage = locationLanguage;
	this.locationName = locationName;
	this.locationVatNumber = locationVatNumber;
	this.mobileNumber = mobileNumber;
	this.phoneNumber = phoneNumber;
	this.proposedDeliveryDate = proposedDeliveryDate;
	}

	public Long getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public void setActualDeliveryDate(Long actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}

	public Address__3 getAddress() {
		return address;
	}

	public void setAddress(Address__3 address) {
		this.address = address;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getDeliveryContact() {
		return deliveryContact;
	}

	public void setDeliveryContact(String deliveryContact) {
		this.deliveryContact = deliveryContact;
	}

	public Long getDesiredDate() {
		return desiredDate;
	}

	public void setDesiredDate(Long desiredDate) {
		this.desiredDate = desiredDate;
	}

	public Long getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(Long expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public FaxNumber__3 getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(FaxNumber__3 faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getLocationLanguage() {
		return locationLanguage;
	}

	public void setLocationLanguage(String locationLanguage) {
		this.locationLanguage = locationLanguage;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationVatNumber() {
		return locationVatNumber;
	}

	public void setLocationVatNumber(String locationVatNumber) {
		this.locationVatNumber = locationVatNumber;
	}

	public MobileNumber__3 getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(MobileNumber__3 mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public PhoneNumber__3 getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(PhoneNumber__3 phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getProposedDeliveryDate() {
		return proposedDeliveryDate;
	}

	public void setProposedDeliveryDate(Long proposedDeliveryDate) {
		this.proposedDeliveryDate = proposedDeliveryDate;
	}

}
