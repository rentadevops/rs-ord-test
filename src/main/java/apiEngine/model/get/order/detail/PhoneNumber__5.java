package apiEngine.model.get.order.detail;

public class PhoneNumber__5 {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public PhoneNumber__5() {
	}

	/**
	*
	* @param value
	*/
	public PhoneNumber__5(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
