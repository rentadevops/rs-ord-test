package apiEngine.model.get.order.detail;

public class Agent {
	
	public Boolean actionsInBulkAllowed;
	public Boolean active;
	public Address address;
	public String applicationVersion;
	public Boolean approvedTermsAndConditions;
	public BankAccount bankAccount;
	public String companyRegistrationNumber;
	public String contactPerson;
	public String dealerType;
	public Email email;
	public FaxNumber faxNumber;
	public Integer id;
	public InvoicingAddress invoicingAddress;
	public String invoicingName;
	public String language;
	public MobileNumber mobileNumber;
	public String name;
	public Integer number;
	public Integer parentDealerNumber;
	public PhoneNumber phoneNumber;
	public String rpr;
	public SupplierBrand supplierBrand;
	public String vatNumber;
	public Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Agent() {
	}

	/**
	*
	* @param applicationVersion
	* @param bankAccount
	* @param address
	* @param companyRegistrationNumber
	* @param mobileNumber
	* @param active
	* @param contactPerson
	* @param language
	* @param version
	* @param invoicingAddress
	* @param supplierBrand
	* @param approvedTermsAndConditions
	* @param number
	* @param phoneNumber
	* @param actionsInBulkAllowed
	* @param parentDealerNumber
	* @param rpr
	* @param name
	* @param dealerType
	* @param faxNumber
	* @param id
	* @param invoicingName
	* @param email
	* @param vatNumber
	*/
	public Agent(Boolean actionsInBulkAllowed, Boolean active, Address address, String applicationVersion, Boolean approvedTermsAndConditions, BankAccount bankAccount, String companyRegistrationNumber, String contactPerson, String dealerType, Email email, FaxNumber faxNumber, Integer id, InvoicingAddress invoicingAddress, String invoicingName, String language, MobileNumber mobileNumber, String name, Integer number, Integer parentDealerNumber, PhoneNumber phoneNumber, String rpr, SupplierBrand supplierBrand, String vatNumber, Integer version) {
	super();
	this.actionsInBulkAllowed = actionsInBulkAllowed;
	this.active = active;
	this.address = address;
	this.applicationVersion = applicationVersion;
	this.approvedTermsAndConditions = approvedTermsAndConditions;
	this.bankAccount = bankAccount;
	this.companyRegistrationNumber = companyRegistrationNumber;
	this.contactPerson = contactPerson;
	this.dealerType = dealerType;
	this.email = email;
	this.faxNumber = faxNumber;
	this.id = id;
	this.invoicingAddress = invoicingAddress;
	this.invoicingName = invoicingName;
	this.language = language;
	this.mobileNumber = mobileNumber;
	this.name = name;
	this.number = number;
	this.parentDealerNumber = parentDealerNumber;
	this.phoneNumber = phoneNumber;
	this.rpr = rpr;
	this.supplierBrand = supplierBrand;
	this.vatNumber = vatNumber;
	this.version = version;
	}

	public Boolean getActionsInBulkAllowed() {
		return actionsInBulkAllowed;
	}

	public void setActionsInBulkAllowed(Boolean actionsInBulkAllowed) {
		this.actionsInBulkAllowed = actionsInBulkAllowed;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public Boolean getApprovedTermsAndConditions() {
		return approvedTermsAndConditions;
	}

	public void setApprovedTermsAndConditions(Boolean approvedTermsAndConditions) {
		this.approvedTermsAndConditions = approvedTermsAndConditions;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getCompanyRegistrationNumber() {
		return companyRegistrationNumber;
	}

	public void setCompanyRegistrationNumber(String companyRegistrationNumber) {
		this.companyRegistrationNumber = companyRegistrationNumber;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getDealerType() {
		return dealerType;
	}

	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public FaxNumber getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(FaxNumber faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public InvoicingAddress getInvoicingAddress() {
		return invoicingAddress;
	}

	public void setInvoicingAddress(InvoicingAddress invoicingAddress) {
		this.invoicingAddress = invoicingAddress;
	}

	public String getInvoicingName() {
		return invoicingName;
	}

	public void setInvoicingName(String invoicingName) {
		this.invoicingName = invoicingName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public MobileNumber getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(MobileNumber mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getParentDealerNumber() {
		return parentDealerNumber;
	}

	public void setParentDealerNumber(Integer parentDealerNumber) {
		this.parentDealerNumber = parentDealerNumber;
	}

	public PhoneNumber getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(PhoneNumber phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRpr() {
		return rpr;
	}

	public void setRpr(String rpr) {
		this.rpr = rpr;
	}

	public SupplierBrand getSupplierBrand() {
		return supplierBrand;
	}

	public void setSupplierBrand(SupplierBrand supplierBrand) {
		this.supplierBrand = supplierBrand;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
