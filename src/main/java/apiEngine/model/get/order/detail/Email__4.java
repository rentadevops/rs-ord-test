package apiEngine.model.get.order.detail;

public class Email__4 {
	
	public String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Email__4() {
	}

	/**
	*
	* @param value
	*/
	public Email__4(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
