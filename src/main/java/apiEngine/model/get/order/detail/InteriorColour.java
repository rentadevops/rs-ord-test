package apiEngine.model.get.order.detail;

public class InteriorColour {
	
	private Translations translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public InteriorColour() {
	}

	/**
	*
	* @param translations
	*/
	public InteriorColour(Translations translations) {
	super();
	this.translations = translations;
	}

	public Translations getTranslations() {
	return translations;
	}

	public void setTranslations(Translations translations) {
	this.translations = translations;
	}

}
