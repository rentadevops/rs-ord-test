package apiEngine.model.get.order.detail;

public class BankAccount__1 {
	
	public Bic__1 bic;
	public Iban__1 iban;

	/**
	* No args constructor for use in serialization
	*
	*/
	public BankAccount__1() {
	}

	/**
	*
	* @param iban
	* @param bic
	*/
	public BankAccount__1(Bic__1 bic, Iban__1 iban) {
	super();
	this.bic = bic;
	this.iban = iban;
	}

	public Bic__1 getBic() {
		return bic;
	}

	public void setBic(Bic__1 bic) {
		this.bic = bic;
	}

	public Iban__1 getIban() {
		return iban;
	}

	public void setIban(Iban__1 iban) {
		this.iban = iban;
	}

}
