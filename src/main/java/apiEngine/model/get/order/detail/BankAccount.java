package apiEngine.model.get.order.detail;

public class BankAccount {
	
	public Bic bic;
	public Iban iban;

	/**
	* No args constructor for use in serialization
	*
	*/
	public BankAccount() {
	}

	/**
	*
	* @param iban
	* @param bic
	*/
	public BankAccount(Bic bic, Iban iban) {
	super();
	this.bic = bic;
	this.iban = iban;
	}

	public Bic getBic() {
		return bic;
	}

	public void setBic(Bic bic) {
		this.bic = bic;
	}

	public Iban getIban() {
		return iban;
	}

	public void setIban(Iban iban) {
		this.iban = iban;
	}

}
