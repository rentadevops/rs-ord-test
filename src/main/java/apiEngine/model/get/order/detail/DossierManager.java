package apiEngine.model.get.order.detail;

public class DossierManager {
	
	public String commercialName;
	public String companyName;
	public Integer id;
	public String loginString;
	public String name;
	public String userName;
	public String userRole;

	/**
	* No args constructor for use in serialization
	*
	*/
	public DossierManager() {
	}

	/**
	*
	* @param loginString
	* @param companyName
	* @param name
	* @param id
	* @param userName
	* @param userRole
	* @param commercialName
	*/
	public DossierManager(String commercialName, String companyName, Integer id, String loginString, String name, String userName, String userRole) {
	super();
	this.commercialName = commercialName;
	this.companyName = companyName;
	this.id = id;
	this.loginString = loginString;
	this.name = name;
	this.userName = userName;
	this.userRole = userRole;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginString() {
		return loginString;
	}

	public void setLoginString(String loginString) {
		this.loginString = loginString;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

}
