package apiEngine.model.get.order.detail;

public class Bic {
	
	public String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Bic() {
	}

	/**
	*
	* @param value
	*/
	public Bic(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
