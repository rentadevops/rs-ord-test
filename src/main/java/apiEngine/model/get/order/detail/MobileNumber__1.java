package apiEngine.model.get.order.detail;

import java.util.Optional;

public class MobileNumber__1 {
	
	private String value = null;

	/**
	* No args constructor for use in serialization
	*
	*/
	public MobileNumber__1() {
	}

	/**
	*
	* @param value
	*/
	public MobileNumber__1(String value) {
	super();
	this.value = value;
	}
	
	public Optional getValueOpt() { 
	    return Optional.ofNullable(value);
	}

	public String getValue() {
		return (String) getValueOpt().orElse(null); 
	}

	public void setValue(String value) {
		this.value = value;
	}

}
