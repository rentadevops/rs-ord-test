package apiEngine.model.get.order.detail;

public class ZipCode__4 {
	
	public String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public ZipCode__4() {
	}

	/**
	*
	* @param value
	*/
	public ZipCode__4(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
