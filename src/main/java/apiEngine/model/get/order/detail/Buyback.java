package apiEngine.model.get.order.detail;

public class Buyback {
	
	private Address__1 address;
	private Amount amount;
	private Boolean buyback;
	private Integer contractDuration;
	private Integer kilometers;
	private String name;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Buyback() {
	}

	/**
	*
	* @param amount
	* @param address
	* @param contractDuration
	* @param kilometers
	* @param name
	* @param buyback
	*/
	public Buyback(Address__1 address, Amount amount, Boolean buyback, Integer contractDuration, Integer kilometers, String name) {
	super();
	this.address = address;
	this.amount = amount;
	this.buyback = buyback;
	this.contractDuration = contractDuration;
	this.kilometers = kilometers;
	this.name = name;
	}

	public Address__1 getAddress() {
		return address;
	}

	public void setAddress(Address__1 address) {
		this.address = address;
	}

	public Amount getAmount() {
		return amount;
	}

	public void setAmount(Amount amount) {
		this.amount = amount;
	}

	public Boolean getBuyback() {
		return buyback;
	}

	public void setBuyback(Boolean buyback) {
		this.buyback = buyback;
	}

	public Integer getContractDuration() {
		return contractDuration;
	}

	public void setContractDuration(Integer contractDuration) {
		this.contractDuration = contractDuration;
	}

	public Integer getKilometers() {
		return kilometers;
	}

	public void setKilometers(Integer kilometers) {
		this.kilometers = kilometers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
