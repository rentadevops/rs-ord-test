package apiEngine.model.get.order.detail;

public class Iban {
	
	public String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Iban() {
	}

	/**
	*
	* @param value
	*/
	public Iban(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
