package apiEngine.model.get.order.detail;

public class TransferredLicensePlate {
	
	public Boolean transferred;
	public String type;

	/**
	* No args constructor for use in serialization
	*
	*/
	public TransferredLicensePlate() {
	}

	/**
	*
	* @param transferred
	* @param type
	*/
	public TransferredLicensePlate(Boolean transferred, String type) {
	super();
	this.transferred = transferred;
	this.type = type;
	}

	public Boolean getTransferred() {
		return transferred;
	}

	public void setTransferred(Boolean transferred) {
		this.transferred = transferred;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
