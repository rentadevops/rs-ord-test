package apiEngine.model.get.order.detail;

public class Description {
	
	public Translations__2 translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Description() {
	}

	/**
	*
	* @param translations
	*/
	public Description(Translations__2 translations) {
	super();
	this.translations = translations;
	}

	public Translations__2 getTranslations() {
		return translations;
	}

	public void setTranslations(Translations__2 translations) {
		this.translations = translations;
	}

}
