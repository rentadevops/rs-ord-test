package apiEngine.model.get.order.detail;

public class Percentage {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Percentage() {
	}

	/**
	*
	* @param value
	*/
	public Percentage(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
