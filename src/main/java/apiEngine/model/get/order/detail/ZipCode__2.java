package apiEngine.model.get.order.detail;

public class ZipCode__2 {
	
	public String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public ZipCode__2() {
	}

	/**
	*
	* @param value
	*/
	public ZipCode__2(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
