package apiEngine.model.get.order.detail;

public class OptionDescription {
	
	public Translations translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OptionDescription() {
	}

	/**
	*
	* @param translations
	*/
	public OptionDescription(Translations translations) {
	super();
	this.translations = translations;
	}

	public Translations getTranslations() {
		return translations;
	}

	public void setTranslations(Translations translations) {
		this.translations = translations;
	}

}
