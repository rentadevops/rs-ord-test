package apiEngine.model.get.order.detail;

public class Product {
	
	public Boolean defaultProduct;
	public Integer id;
	public Integer leasingCompanyId;
	public String logoFile;
	public String name;
	public String logoName;
	public String mobileLogoName;
	public String referenceCode;
	public Integer rumId;
	public String termsAndConditionsFile;
	public Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Product() {
	}

	/**
	*
	* @param logoFile
	* @param name
	* @param rumId
	* @param termsAndConditionsFile
	* @param id
	* @param leasingCompanyId
	* @param defaultProduct
	* @param referenceCode
	* @param version
	*/
	public Product(Boolean defaultProduct, Integer id, Integer leasingCompanyId, String logoFile, String name, String logoName, String mobileLogoName, String referenceCode, Integer rumId, String termsAndConditionsFile, Integer version) {
	super();
	this.defaultProduct = defaultProduct;
	this.id = id;
	this.leasingCompanyId = leasingCompanyId;
	this.logoFile = logoFile;
	this.name = name;
	this.logoName = logoName;
	this.mobileLogoName = mobileLogoName;
	this.referenceCode = referenceCode;
	this.rumId = rumId;
	this.termsAndConditionsFile = termsAndConditionsFile;
	this.version = version;
	}

	public Boolean getDefaultProduct() {
		return defaultProduct;
	}

	public void setDefaultProduct(Boolean defaultProduct) {
		this.defaultProduct = defaultProduct;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLeasingCompanyId() {
		return leasingCompanyId;
	}

	public void setLeasingCompanyId(Integer leasingCompanyId) {
		this.leasingCompanyId = leasingCompanyId;
	}

	public String getLogoFile() {
		return logoFile;
	}

	public void setLogoFile(String logoFile) {
		this.logoFile = logoFile;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}
	
	public String getLogoName() {
		return logoName;
	}

	public void setMobileLogoName(String mobileLogoName) {
		this.mobileLogoName = mobileLogoName;
	}

	public String getMobileLogoName() {
		return mobileLogoName;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Integer getRumId() {
		return rumId;
	}

	public void setRumId(Integer rumId) {
		this.rumId = rumId;
	}

	public String getTermsAndConditionsFile() {
		return termsAndConditionsFile;
	}

	public void setTermsAndConditionsFile(String termsAndConditionsFile) {
		this.termsAndConditionsFile = termsAndConditionsFile;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
