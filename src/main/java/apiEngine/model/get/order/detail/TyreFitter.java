package apiEngine.model.get.order.detail;

public class TyreFitter {
	
	public Email__6 email;
	public String name;

	/**
	* No args constructor for use in serialization
	*
	*/
	public TyreFitter() {
	}

	/**
	*
	* @param name
	* @param email
	*/
	public TyreFitter(Email__6 email, String name) {
	super();
	this.email = email;
	this.name = name;
	}

	public Email__6 getEmail() {
		return email;
	}

	public void setEmail(Email__6 email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
