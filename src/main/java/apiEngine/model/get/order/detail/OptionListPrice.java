package apiEngine.model.get.order.detail;

public class OptionListPrice {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OptionListPrice() {
	}

	/**
	*
	* @param value
	*/
	public OptionListPrice(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
