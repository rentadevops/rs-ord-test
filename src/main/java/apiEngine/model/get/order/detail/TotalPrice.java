package apiEngine.model.get.order.detail;

public class TotalPrice {
	
	private Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public TotalPrice() {
	}

	/**
	*
	* @param value
	*/
	public TotalPrice(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
