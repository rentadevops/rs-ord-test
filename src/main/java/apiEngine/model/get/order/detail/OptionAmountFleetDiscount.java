package apiEngine.model.get.order.detail;

public class OptionAmountFleetDiscount {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OptionAmountFleetDiscount() {
	}

	/**
	*
	* @param value
	*/
	public OptionAmountFleetDiscount(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
