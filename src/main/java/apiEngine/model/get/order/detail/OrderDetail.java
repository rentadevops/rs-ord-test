package apiEngine.model.get.order.detail;

import java.math.BigInteger;
import java.util.List;

public class OrderDetail {
	
	private Agent agent;
	private Boolean alreadyOrdered;
	private Boolean beingProcessed;
	private String busyUntil;
	private Buyback buyback;
	private Client client;
	private Dealer dealer;
	private Boolean dealerActionNeeded;
	private String dealerDossierNumber;
	private String dealerDueDate;
	private Boolean dealerHasUnseenDocuments;
	private Boolean dealerHasUnseenMessages;
	private Delivery delivery;
	private DossierManager dossierManager;
	private Driver driver;
	private FinancialLeaseRegistrationInfo financialLeaseRegistrationInfo;
	private InscriptionCosts inscriptionCosts;
	private Insurance insurance;
	private String invoiceDate;
	private Boolean invoiced;
	private LastComment lastComment;
	private LeasingCompany leasingCompany;
	private Boolean leasingCompanyActionNeeded;
	private String leasingCompanyDossierNumber;
	private String leasingCompanyDueDate;
	private Boolean leasingCompanyHasUnseenDocuments;
	private Boolean leasingCompanyHasUnseenMessages;
	private String leasingType;
	private String linkedRegistrationId;
	private Boolean modified;
	private List<OptionDTO> optionDTOs = null;
	private String orderId;
	private String orderStatus;
	private String orderType;
	private Integer orderVersion;
	private Pickup pickup;
	private Pricing pricing;
	private Product product;
	private List<PromotionDTO> promotionDTOs = null;
	private String quoteReference;
	private Boolean redactedGDPR;
	private String registrationDivToDoBy;
	private String reiBatchTrackingNumber;
	private List<String> linkedWallBoxOrders;
	private String linkedWallBoxOrderId;
	private TaxStampCosts taxStampCosts;
	private TyreFitter tyreFitter;
	private Vehicle vehicle;
	private Integer version;
	private String versionNumber;
	private String creationDate;
	private String digitalDeliveryStatus;
	private Boolean digitalDelivery;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OrderDetail() {
	}

	/**
	*
	* @param dossierManager
	* @param insurance
	* @param busyUntil
	* @param orderType
	* @param agent
	* @param dealerDueDate
	* @param orderId
	* @param reiBatchTrackingNumber
	* @param redactedGDPR
	* @param dealerDossierNumber
	* @param orderStatus
	* @param buyback
	* @param vehicle
	* @param dealerHasUnseenDocuments
	* @param client
	* @param modified
	* @param quoteReference
	* @param lastComment
	* @param alreadyOrdered
	* @param optionDTOs
	* @param taxStampCosts
	* @param delivery
	* @param leasingCompanyDossierNumber
	* @param linkedRegistrationId
	* @param product
	* @param leasingCompanyHasUnseenMessages
	* @param leasingCompanyActionNeeded
	* @param tyreFitter
	* @param invoiced
	* @param leasingCompany
	* @param leasingCompanyHasUnseenDocuments
	* @param pickup
	* @param orderVersion
	* @param invoiceDate
	* @param version
	* @param versionNumber
	* @param promotionDTOs
	* @param inscriptionCosts
	* @param leasingType
	* @param dealerHasUnseenMessages
	* @param leasingCompanyDueDate
	* @param driver
	* @param beingProcessed
	* @param dealer
	* @param dealerActionNeeded
	 * @param linkedWallBoxOrders;
	* @param linkedWallBoxOrderId;
	* @param registrationDivToDoBy
	* @param pricing
	* @param financialLeaseRegistrationInfo
	* @param creationDate;
	 * @param digitalDeliveryStatus;
	 * @param digitalDelivery;
	*/
	public OrderDetail(Agent agent, Boolean alreadyOrdered, Boolean beingProcessed, String busyUntil, Buyback buyback, Client client, Dealer dealer, Boolean dealerActionNeeded, String dealerDossierNumber, String dealerDueDate, Boolean dealerHasUnseenDocuments, Boolean dealerHasUnseenMessages, Delivery delivery, DossierManager dossierManager, Driver driver, FinancialLeaseRegistrationInfo financialLeaseRegistrationInfo, InscriptionCosts inscriptionCosts, Insurance insurance, String invoiceDate, Boolean invoiced, LastComment lastComment, LeasingCompany leasingCompany, Boolean leasingCompanyActionNeeded, String leasingCompanyDossierNumber, String leasingCompanyDueDate, Boolean leasingCompanyHasUnseenDocuments, Boolean leasingCompanyHasUnseenMessages, String leasingType, String linkedRegistrationId, Boolean modified, List<OptionDTO> optionDTOs, String orderId, String orderStatus, String orderType, Integer orderVersion, Pickup pickup, Pricing pricing, Product product, List<PromotionDTO> promotionDTOs, String quoteReference, Boolean redactedGDPR, List<String> linkedWallBoxOrders, String linkedWallBoxOrderId, String registrationDivToDoBy, String reiBatchTrackingNumber, TaxStampCosts taxStampCosts, TyreFitter tyreFitter, Vehicle vehicle,Integer version, String versionNumber, String creationDate, String digitalDeliveryStatus, Boolean digitalDelivery) {
	super();
	this.agent = agent;
	this.alreadyOrdered = alreadyOrdered;
	this.beingProcessed = beingProcessed;
	this.busyUntil = busyUntil;
	this.buyback = buyback;
	this.client = client;
	this.dealer = dealer;
	this.dealerActionNeeded = dealerActionNeeded;
	this.dealerDossierNumber = dealerDossierNumber;
	this.dealerDueDate = dealerDueDate;
	this.dealerHasUnseenDocuments = dealerHasUnseenDocuments;
	this.dealerHasUnseenMessages = dealerHasUnseenMessages;
	this.delivery = delivery;
	this.dossierManager = dossierManager;
	this.driver = driver;
	this.financialLeaseRegistrationInfo = financialLeaseRegistrationInfo;
	this.inscriptionCosts = inscriptionCosts;
	this.insurance = insurance;
	this.invoiceDate = invoiceDate;
	this.invoiced = invoiced;
	this.lastComment = lastComment;
	this.leasingCompany = leasingCompany;
	this.leasingCompanyActionNeeded = leasingCompanyActionNeeded;
	this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
	this.leasingCompanyDueDate = leasingCompanyDueDate;
	this.leasingCompanyHasUnseenDocuments = leasingCompanyHasUnseenDocuments;
	this.leasingCompanyHasUnseenMessages = leasingCompanyHasUnseenMessages;
	this.leasingType = leasingType;
	this.linkedRegistrationId = linkedRegistrationId;
	this.modified = modified;
	this.optionDTOs = optionDTOs;
	this.orderId = orderId;
	this.orderStatus = orderStatus;
	this.orderType = orderType;
	this.orderVersion = orderVersion;
	this.pickup = pickup;
	this.pricing = pricing;
	this.product = product;
	this.promotionDTOs = promotionDTOs;
	this.quoteReference = quoteReference;
	this.redactedGDPR = redactedGDPR;
	this.registrationDivToDoBy = registrationDivToDoBy;
	this.reiBatchTrackingNumber = reiBatchTrackingNumber;
	this.taxStampCosts = taxStampCosts;
	this.linkedWallBoxOrders = linkedWallBoxOrders;
	this.linkedWallBoxOrderId = linkedWallBoxOrderId;
	this.tyreFitter = tyreFitter;
	this.vehicle = vehicle;
	this.version = version;
	this.versionNumber = versionNumber;
	this.creationDate = creationDate;
	this.digitalDeliveryStatus = digitalDeliveryStatus;
	this.digitalDelivery = digitalDelivery;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Boolean getAlreadyOrdered() {
		return alreadyOrdered;
	}

	public void setAlreadyOrdered(Boolean alreadyOrdered) {
		this.alreadyOrdered = alreadyOrdered;
	}

	public Boolean getBeingProcessed() {
		return beingProcessed;
	}

	public void setBeingProcessed(Boolean beingProcessed) {
		this.beingProcessed = beingProcessed;
	}

	public String getBusyUntil() {
		return busyUntil;
	}

	public void setBusyUntil(String busyUntil) {
		this.busyUntil = busyUntil;
	}

	public Buyback getBuyback() {
		return buyback;
	}

	public void setBuyback(Buyback buyback) {
		this.buyback = buyback;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}

	public Boolean getDealerActionNeeded() {
		return dealerActionNeeded;
	}

	public void setDealerActionNeeded(Boolean dealerActionNeeded) {
		this.dealerActionNeeded = dealerActionNeeded;
	}

	public String getDealerDossierNumber() {
		return dealerDossierNumber;
	}

	public void setDealerDossierNumber(String dealerDossierNumber) {
		this.dealerDossierNumber = dealerDossierNumber;
	}

	public String getDealerDueDate() {
		return dealerDueDate;
	}

	public void setDealerDueDate(String dealerDueDate) {
		this.dealerDueDate = dealerDueDate;
	}

	public Boolean getDealerHasUnseenDocuments() {
		return dealerHasUnseenDocuments;
	}

	public void setDealerHasUnseenDocuments(Boolean dealerHasUnseenDocuments) {
		this.dealerHasUnseenDocuments = dealerHasUnseenDocuments;
	}

	public Boolean getDealerHasUnseenMessages() {
		return dealerHasUnseenMessages;
	}

	public void setDealerHasUnseenMessages(Boolean dealerHasUnseenMessages) {
		this.dealerHasUnseenMessages = dealerHasUnseenMessages;
	}
	
	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public DossierManager getDossierManager() {
		return dossierManager;
	}

	public void setDossierManager(DossierManager dossierManager) {
		this.dossierManager = dossierManager;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	
	public FinancialLeaseRegistrationInfo getFinancialLeaseRegistrationInfo() {
		return financialLeaseRegistrationInfo;
	}

	public void setFinancialLeaseRegistrationInfo(FinancialLeaseRegistrationInfo financialLeaseRegistrationInfo) {
		this.financialLeaseRegistrationInfo = financialLeaseRegistrationInfo;
	}

	public InscriptionCosts getInscriptionCosts() {
		return inscriptionCosts;
	}

	public void setInscriptionCosts(InscriptionCosts inscriptionCosts) {
		this.inscriptionCosts = inscriptionCosts;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Boolean getInvoiced() {
		return invoiced;
	}

	public void setInvoiced(Boolean invoiced) {
		this.invoiced = invoiced;
	}

	public LastComment getLastComment() {
		return lastComment;
	}

	public void setLastComment(LastComment lastComment) {
		this.lastComment = lastComment;
	}

	public LeasingCompany getLeasingCompany() {
		return leasingCompany;
	}

	public void setLeasingCompany(LeasingCompany leasingCompany) {
		this.leasingCompany = leasingCompany;
	}
	
	public Boolean getLeasingCompanyActionNeeded() {
		return leasingCompanyActionNeeded;
	}

	public void setLeasingCompanyActionNeeded(Boolean leasingCompanyActionNeeded) {
		this.leasingCompanyActionNeeded = leasingCompanyActionNeeded;
	}

	public String getLeasingCompanyDossierNumber() {
		return leasingCompanyDossierNumber;
	}

	public void setLeasingCompanyDossierNumber(String leasingCompanyDossierNumber) {
		this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
	}

	public String getLeasingCompanyDueDate() {
		return leasingCompanyDueDate;
	}

	public void setLeasingCompanyDueDate(String leasingCompanyDueDate) {
		this.leasingCompanyDueDate = leasingCompanyDueDate;
	}

	public Boolean getLeasingCompanyHasUnseenDocuments() {
		return leasingCompanyHasUnseenDocuments;
	}

	public void setLeasingCompanyHasUnseenDocuments(Boolean leasingCompanyHasUnseenDocuments) {
		this.leasingCompanyHasUnseenDocuments = leasingCompanyHasUnseenDocuments;
	}

	public Boolean getLeasingCompanyHasUnseenMessages() {
		return leasingCompanyHasUnseenMessages;
	}

	public void setLeasingCompanyHasUnseenMessages(Boolean leasingCompanyHasUnseenMessages) {
		this.leasingCompanyHasUnseenMessages = leasingCompanyHasUnseenMessages;
	}

	public String getLeasingType() {
		return leasingType;
	}

	public void setLeasingType(String leasingType) {
		this.leasingType = leasingType;
	}

	public String getLinkedRegistrationId() {
		return linkedRegistrationId;
	}

	public void setLinkedRegistrationId(String linkedRegistrationId) {
		this.linkedRegistrationId = linkedRegistrationId;
	}

	public String getLinkedWallBoxOrderId() {
		return linkedWallBoxOrderId;
	}

	public void setLinkedWallBoxOrderId(String linkedWallBoxOrderId) {
		this.linkedWallBoxOrderId = linkedWallBoxOrderId;
	}

	public Boolean getModified() {
		return modified;
	}

	public void setModified(Boolean modified) {
		this.modified = modified;
	}

	public List<OptionDTO> getOptionDTOs() {
		return optionDTOs;
	}

	public void setOptionDTOs(List<OptionDTO> optionDTOs) {
		this.optionDTOs = optionDTOs;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Integer getOrderVersion() {
		return orderVersion;
	}

	public void setOrderVersion(Integer orderVersion) {
		this.orderVersion = orderVersion;
	}

	public Pickup getPickup() {
		return pickup;
	}

	public void setPickup(Pickup pickup) {
		this.pickup = pickup;
	}

	public Pricing getPricing() {
		return pricing;
	}

	public void setPricing(Pricing pricing) {
		this.pricing = pricing;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<PromotionDTO> getPromotionDTOs() {
		return promotionDTOs;
	}

	public void setPromotionDTOs(List<PromotionDTO> promotionDTOs) {
		this.promotionDTOs = promotionDTOs;
	}

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	public Boolean getRedactedGDPR() {
		return redactedGDPR;
	}

	public void setRedactedGDPR(Boolean redactedGDPR) {
		this.redactedGDPR = redactedGDPR;
	}

	public String getRegistrationDivToDoBy() {
		return registrationDivToDoBy;
	}

	public void setRegistrationDivToDoBy(String registrationDivToDoBy) {
		this.registrationDivToDoBy = registrationDivToDoBy;
	}

	public String getReiBatchTrackingNumber() {
		return reiBatchTrackingNumber;
	}

	public void setReiBatchTrackingNumber(String reiBatchTrackingNumber) {
		this.reiBatchTrackingNumber = reiBatchTrackingNumber;
	}

	public TaxStampCosts getTaxStampCosts() {
		return taxStampCosts;
	}

	public void setTaxStampCosts(TaxStampCosts taxStampCosts) {
		this.taxStampCosts = taxStampCosts;
	}

	public TyreFitter getTyreFitter() {
		return tyreFitter;
	}

	public void setTyreFitter(TyreFitter tyreFitter) {
		this.tyreFitter = tyreFitter;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public List<String> getLinkedWallBoxOrders() {
		return linkedWallBoxOrders;
	}

	public void setLinkedWallBoxOrders(List<String> linkedWallBoxOrders) {
		this.linkedWallBoxOrders = linkedWallBoxOrders;
	}

	public String getDigitalDeliveryStatus() {
		return digitalDeliveryStatus;
	}

	public void setDigitalDeliveryStatus(String digitalDeliveryStatus) {
		this.digitalDeliveryStatus = digitalDeliveryStatus;
	}

	public Boolean getDigitalDelivery() {
		return digitalDelivery;
	}

	public void setDigitalDelivery(Boolean digitalDelivery) {
		this.digitalDelivery = digitalDelivery;
	}
}
