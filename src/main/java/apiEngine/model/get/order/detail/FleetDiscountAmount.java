package apiEngine.model.get.order.detail;

public class FleetDiscountAmount {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public FleetDiscountAmount() {
	}

	/**
	*
	* @param value
	*/
	public FleetDiscountAmount(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
