package apiEngine.model.get.order.detail;

public class Amount {
	
	private Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Amount() {
	}

	/**
	*
	* @param value
	*/
	public Amount(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
