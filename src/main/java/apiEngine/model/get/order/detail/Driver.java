package apiEngine.model.get.order.detail;

public class Driver {
	
	private String address;
	private String city;
	private Email__3 email;
	private String languageCode;
	private MobileNumber__4 mobileNumber;
	private String name;
	private PhoneNumber__4 phoneNumber;
	private String postalCode;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Driver() {
	}

	/**
	*
	* @param address
	* @param phoneNumber
	* @param city
	* @param mobileNumber
	* @param postalCode
	* @param name
	* @param languageCode
	* @param email
	*/
	public Driver(String address, String city, Email__3 email, String languageCode, MobileNumber__4 mobileNumber, String name, PhoneNumber__4 phoneNumber, String postalCode) {
	super();
	this.address = address;
	this.city = city;
	this.email = email;
	this.languageCode = languageCode;
	this.mobileNumber = mobileNumber;
	this.name = name;
	this.phoneNumber = phoneNumber;
	this.postalCode = postalCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Email__3 getEmail() {
		return email;
	}

	public void setEmail(Email__3 email) {
		this.email = email;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public MobileNumber__4 getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(MobileNumber__4 mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PhoneNumber__4 getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(PhoneNumber__4 phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

}
