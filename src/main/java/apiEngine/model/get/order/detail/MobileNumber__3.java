package apiEngine.model.get.order.detail;

public class MobileNumber__3 {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public MobileNumber__3() {
	}

	/**
	*
	* @param value
	*/
	public MobileNumber__3(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
