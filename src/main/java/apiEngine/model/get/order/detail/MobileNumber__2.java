package apiEngine.model.get.order.detail;

public class MobileNumber__2 {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public MobileNumber__2() {
	}

	/**
	*
	* @param value
	*/
	public MobileNumber__2(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
