package apiEngine.model.get.order.detail;

public class Co2EmissionsNedc {
	
	private Double value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Co2EmissionsNedc() {
	}

	/**
	*
	* @param value
	*/
	public Co2EmissionsNedc(Double value) {
	super();
	this.value = value;
	}

	public Double getValue() {
	return value;
	}

	public void setValue(Double value) {
	this.value = value;
	}

}
