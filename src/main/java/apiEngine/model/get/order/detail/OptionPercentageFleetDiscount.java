package apiEngine.model.get.order.detail;

public class OptionPercentageFleetDiscount {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OptionPercentageFleetDiscount() {
	}

	/**
	*
	* @param value
	*/
	public OptionPercentageFleetDiscount(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
