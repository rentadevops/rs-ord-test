package apiEngine.model.get.order.detail;

public class Co2EmissionsWltp {
	
	private Double value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Co2EmissionsWltp() {
	}

	/**
	*
	* @param value
	*/
	public Co2EmissionsWltp(Double value) {
	super();
	this.value = value;
	}

	public Double getValue() {
	return value;
	}

	public void setValue(Double value) {
	this.value = value;
	}

}
