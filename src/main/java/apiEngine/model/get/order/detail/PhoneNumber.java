package apiEngine.model.get.order.detail;

public class PhoneNumber {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public PhoneNumber() {
	}

	/**
	*
	* @param value
	*/
	public PhoneNumber(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
