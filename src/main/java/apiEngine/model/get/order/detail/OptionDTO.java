package apiEngine.model.get.order.detail;

public class OptionDTO {
	
	public ApplicableVatPercentage applicableVatPercentage;
	public Integer id;
	public OptionAmountFleetDiscount optionAmountFleetDiscount;
	public String optionCodes;
	public OptionDescription optionDescription;
	public OptionListPrice optionListPrice;
	public OptionName optionName;
	public OptionPercentageFleetDiscount optionPercentageFleetDiscount;
	public String optionSupplierName;
	public String optionSupplierNumber;
	public String optionType;
	public Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OptionDTO() {
	}

	/**
	*
	* @param optionType
	* @param applicableVatPercentage
	* @param optionListPrice
	* @param optionCodes
	* @param optionAmountFleetDiscount
	* @param optionPercentageFleetDiscount
	* @param optionDescription
	* @param optionSupplierNumber
	* @param id
	* @param optionName
	* @param version
	* @param optionSupplierName
	*/
	public OptionDTO(ApplicableVatPercentage applicableVatPercentage, Integer id, OptionAmountFleetDiscount optionAmountFleetDiscount, String optionCodes, OptionDescription optionDescription, OptionListPrice optionListPrice, OptionName optionName, OptionPercentageFleetDiscount optionPercentageFleetDiscount, String optionSupplierName, String optionSupplierNumber, String optionType, Integer version) {
	super();
	this.applicableVatPercentage = applicableVatPercentage;
	this.id = id;
	this.optionAmountFleetDiscount = optionAmountFleetDiscount;
	this.optionCodes = optionCodes;
	this.optionDescription = optionDescription;
	this.optionListPrice = optionListPrice;
	this.optionName = optionName;
	this.optionPercentageFleetDiscount = optionPercentageFleetDiscount;
	this.optionSupplierName = optionSupplierName;
	this.optionSupplierNumber = optionSupplierNumber;
	this.optionType = optionType;
	this.version = version;
	}

	public ApplicableVatPercentage getApplicableVatPercentage() {
		return applicableVatPercentage;
	}

	public void setApplicableVatPercentage(ApplicableVatPercentage applicableVatPercentage) {
		this.applicableVatPercentage = applicableVatPercentage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OptionAmountFleetDiscount getOptionAmountFleetDiscount() {
		return optionAmountFleetDiscount;
	}

	public void setOptionAmountFleetDiscount(OptionAmountFleetDiscount optionAmountFleetDiscount) {
		this.optionAmountFleetDiscount = optionAmountFleetDiscount;
	}

	public String getOptionCodes() {
		return optionCodes;
	}

	public void setOptionCodes(String optionCodes) {
		this.optionCodes = optionCodes;
	}

	public OptionDescription getOptionDescription() {
		return optionDescription;
	}

	public void setOptionDescription(OptionDescription optionDescription) {
		this.optionDescription = optionDescription;
	}

	public OptionListPrice getOptionListPrice() {
		return optionListPrice;
	}

	public void setOptionListPrice(OptionListPrice optionListPrice) {
		this.optionListPrice = optionListPrice;
	}

	public OptionName getOptionName() {
		return optionName;
	}

	public void setOptionName(OptionName optionName) {
		this.optionName = optionName;
	}

	public OptionPercentageFleetDiscount getOptionPercentageFleetDiscount() {
		return optionPercentageFleetDiscount;
	}

	public void setOptionPercentageFleetDiscount(OptionPercentageFleetDiscount optionPercentageFleetDiscount) {
		this.optionPercentageFleetDiscount = optionPercentageFleetDiscount;
	}

	public String getOptionSupplierName() {
		return optionSupplierName;
	}

	public void setOptionSupplierName(String optionSupplierName) {
		this.optionSupplierName = optionSupplierName;
	}

	public String getOptionSupplierNumber() {
		return optionSupplierNumber;
	}

	public void setOptionSupplierNumber(String optionSupplierNumber) {
		this.optionSupplierNumber = optionSupplierNumber;
	}

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
