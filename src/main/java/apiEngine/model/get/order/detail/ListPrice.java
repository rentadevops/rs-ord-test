package apiEngine.model.get.order.detail;

public class ListPrice {
	
	private Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public ListPrice() {
	}

	/**
	*
	* @param value
	*/
	public ListPrice(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
