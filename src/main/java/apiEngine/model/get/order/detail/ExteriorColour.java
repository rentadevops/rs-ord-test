package apiEngine.model.get.order.detail;

public class ExteriorColour {
	
	private Translations translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public ExteriorColour() {
	}

	/**
	*
	* @param translations
	*/
	public ExteriorColour(Translations translations) {
	super();
	this.translations = translations;
	}

	public Translations getTranslations() {
		return translations;
	}

	public void setTranslations(Translations translations) {
		this.translations = translations;
	}

}
