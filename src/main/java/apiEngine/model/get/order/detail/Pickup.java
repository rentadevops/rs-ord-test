package apiEngine.model.get.order.detail;

import java.math.BigInteger;

public class Pickup {
	
	private Boolean certificateOfConformity;
	private Integer constructionYear;
	private String dateFirstRegistration;
	private String driverBirthDate;
	private String driverBirthPlace;
	private String driverFirstName;
	private String driverLastName;
	private Boolean fuelCard;
	private String keyCode;
	private Boolean legalKit;
	private Boolean manual;
	private Mileage mileage;
	private Integer numberOfKeys;
	private Boolean previousVehicleDropoff;
	private String previousVehicleLicensePlate;
	private String previousVehicleOwner;
	private Boolean proofOfInsurance;
	private Boolean proofOfRegistration;
	private Boolean proofOfRoadworthiness;
	private String startCode;
	private Boolean mainDriver;
	private String address;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Pickup() {
	}

	/**
	*
	* @param startCode
	* @param constructionYear
	* @param previousVehicleDropoff
	* @param numberOfKeys
	* @param manual
	* @param previousVehicleLicensePlate
	* @param keyCode
	* @param certificateOfConformity
	* @param proofOfInsurance
	* @param driverBirthPlace
	* @param proofOfRoadworthiness
	* @param driverFirstName
	* @param driverLastName
	* @param previousVehicleOwner
	* @param fuelCard
	* @param dateFirstRegistration
	* @param legalKit
	* @param proofOfRegistration
	* @param driverBirthDate
	* @param mileage
	*/
	public Pickup(Boolean certificateOfConformity, Integer constructionYear, String dateFirstRegistration, String driverBirthDate, String driverBirthPlace, String driverFirstName, String driverLastName, Boolean fuelCard, String keyCode, Boolean legalKit, Boolean manual, Mileage mileage, Integer numberOfKeys, Boolean previousVehicleDropoff, String previousVehicleLicensePlate, String previousVehicleOwner, Boolean proofOfInsurance, Boolean proofOfRegistration, Boolean proofOfRoadworthiness, String startCode, Boolean mainDriver, String address) {
	super();
	this.certificateOfConformity = certificateOfConformity;
	this.constructionYear = constructionYear;
	this.dateFirstRegistration = dateFirstRegistration;
	this.driverBirthDate = driverBirthDate;
	this.driverBirthPlace = driverBirthPlace;
	this.driverFirstName = driverFirstName;
	this.driverLastName = driverLastName;
	this.fuelCard = fuelCard;
	this.keyCode = keyCode;
	this.legalKit = legalKit;
	this.manual = manual;
	this.mileage = mileage;
	this.numberOfKeys = numberOfKeys;
	this.previousVehicleDropoff = previousVehicleDropoff;
	this.previousVehicleLicensePlate = previousVehicleLicensePlate;
	this.previousVehicleOwner = previousVehicleOwner;
	this.proofOfInsurance = proofOfInsurance;
	this.proofOfRegistration = proofOfRegistration;
	this.proofOfRoadworthiness = proofOfRoadworthiness;
	this.startCode = startCode;
	this.mainDriver = mainDriver;
	this.address = address;
	}

	public Boolean getCertificateOfConformity() {
		return certificateOfConformity;
	}

	public void setCertificateOfConformity(Boolean certificateOfConformity) {
		this.certificateOfConformity = certificateOfConformity;
	}

	public Integer getConstructionYear() {
		return constructionYear;
	}

	public void setConstructionYear(Integer constructionYear) {
		this.constructionYear = constructionYear;
	}

	public String getDateFirstRegistration() {
		return dateFirstRegistration;
	}

	public void setDateFirstRegistration(String dateFirstRegistration) {
		this.dateFirstRegistration = dateFirstRegistration;
	}

	public String getDriverBirthDate() {
		return driverBirthDate;
	}

	public void setDriverBirthDate(String driverBirthDate) {
		this.driverBirthDate = driverBirthDate;
	}

	public String getDriverBirthPlace() {
		return driverBirthPlace;
	}

	public void setDriverBirthPlace(String driverBirthPlace) {
		this.driverBirthPlace = driverBirthPlace;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverLastName() {
		return driverLastName;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}

	public Boolean getFuelCard() {
		return fuelCard;
	}

	public void setFuelCard(Boolean fuelCard) {
		this.fuelCard = fuelCard;
	}

	public String getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public Boolean getLegalKit() {
		return legalKit;
	}

	public void setLegalKit(Boolean legalKit) {
		this.legalKit = legalKit;
	}

	public Boolean getManual() {
		return manual;
	}

	public void setManual(Boolean manual) {
		this.manual = manual;
	}

	public Mileage getMileage() {
		return mileage;
	}

	public void setMileage(Mileage mileage) {
		this.mileage = mileage;
	}

	public Integer getNumberOfKeys() {
		return numberOfKeys;
	}

	public void setNumberOfKeys(Integer numberOfKeys) {
		this.numberOfKeys = numberOfKeys;
	}

	public Boolean getPreviousVehicleDropoff() {
		return previousVehicleDropoff;
	}

	public void setPreviousVehicleDropoff(Boolean previousVehicleDropoff) {
		this.previousVehicleDropoff = previousVehicleDropoff;
	}

	public String getPreviousVehicleLicensePlate() {
		return previousVehicleLicensePlate;
	}

	public void setPreviousVehicleLicensePlate(String previousVehicleLicensePlate) {
		this.previousVehicleLicensePlate = previousVehicleLicensePlate;
	}

	public String getPreviousVehicleOwner() {
		return previousVehicleOwner;
	}

	public void setPreviousVehicleOwner(String previousVehicleOwner) {
		this.previousVehicleOwner = previousVehicleOwner;
	}

	public Boolean getProofOfInsurance() {
		return proofOfInsurance;
	}

	public void setProofOfInsurance(Boolean proofOfInsurance) {
		this.proofOfInsurance = proofOfInsurance;
	}

	public Boolean getProofOfRegistration() {
		return proofOfRegistration;
	}

	public void setProofOfRegistration(Boolean proofOfRegistration) {
		this.proofOfRegistration = proofOfRegistration;
	}

	public Boolean getProofOfRoadworthiness() {
		return proofOfRoadworthiness;
	}

	public void setProofOfRoadworthiness(Boolean proofOfRoadworthiness) {
		this.proofOfRoadworthiness = proofOfRoadworthiness;
	}

	public String getStartCode() {
		return startCode;
	}

	public void setStartCode(String startCode) {
		this.startCode = startCode;
	}

	public Boolean getMainDriver() {
		return mainDriver;
	}

	public void setMainDriver(Boolean mainDriver) {
		this.mainDriver = mainDriver;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
