package apiEngine.model.get.order.detail;

public class Vin {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Vin() {
	}

	/**
	*
	* @param value
	*/
	public Vin(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
