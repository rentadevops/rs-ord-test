package apiEngine.model.get.order.detail;

public class OtherCosts {
	
	private Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OtherCosts() {
	}

	/**
	*
	* @param value
	*/
	public OtherCosts(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
