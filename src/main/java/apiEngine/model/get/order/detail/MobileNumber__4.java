package apiEngine.model.get.order.detail;

public class MobileNumber__4 {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public MobileNumber__4() {
	}

	/**
	*
	* @param value
	*/
	public MobileNumber__4(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
