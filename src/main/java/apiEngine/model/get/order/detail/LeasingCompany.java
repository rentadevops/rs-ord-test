package apiEngine.model.get.order.detail;

public class LeasingCompany {
	
	private String commercialName;
	private Integer id;
	private Integer rsNumber;

	/**
	* No args constructor for use in serialization
	*
	*/
	public LeasingCompany() {
	}

	/**
	*
	* @param rsNumber
	* @param id
	* @param commercialName
	*/
	public LeasingCompany(String commercialName, Integer id, Integer rsNumber) {
	super();
	this.commercialName = commercialName;
	this.id = id;
	this.rsNumber = rsNumber;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRsNumber() {
		return rsNumber;
	}

	public void setRsNumber(Integer rsNumber) {
		this.rsNumber = rsNumber;
	}

}
