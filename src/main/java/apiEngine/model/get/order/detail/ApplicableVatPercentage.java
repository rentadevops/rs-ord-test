package apiEngine.model.get.order.detail;

public class ApplicableVatPercentage {

	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public ApplicableVatPercentage() {
	}

	/**
	*
	* @param value
	*/
	public ApplicableVatPercentage(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
}
