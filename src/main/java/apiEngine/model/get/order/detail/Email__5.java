package apiEngine.model.get.order.detail;

public class Email__5 {
	
	public String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Email__5() {
	}

	/**
	*
	* @param value
	*/
	public Email__5(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
