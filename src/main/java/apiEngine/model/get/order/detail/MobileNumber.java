package apiEngine.model.get.order.detail;

public class MobileNumber {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public MobileNumber() {
	}

	/**
	*
	* @param value
	*/
	public MobileNumber(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
