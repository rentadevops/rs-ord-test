package apiEngine.model.get.order.detail;

public class InsuredValuePrice {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public InsuredValuePrice() {
	}

	/**
	*
	* @param value
	*/
	public InsuredValuePrice(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
