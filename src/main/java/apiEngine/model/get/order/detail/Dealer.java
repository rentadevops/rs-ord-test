package apiEngine.model.get.order.detail;

public class Dealer {
	
	private Boolean actionsInBulkAllowed;
	private Boolean active;
	private Address__2 address;
	private String applicationVersion;
	private Boolean approvedTermsAndConditions;
	private BankAccount__1 bankAccount;
	private String companyRegistrationNumber;
	private String contactPerson;
	private String dealerType;
	private String dealerSearchIndex;
	private Email__2 email;
	private FaxNumber__2 faxNumber;
	private Integer id;
	private InvoicingAddress__1 invoicingAddress;
	private String invoicingName;
	private String language;
	private MobileNumber__2 mobileNumber;
	private String name;
	private Integer number;
	private Integer parentDealerNumber;
	private PhoneNumber__2 phoneNumber;
	private String rpr;
	private SupplierBrand__1 supplierBrand;
	private String vatNumber;
	private Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Dealer() {
	}

	/**
	*
	* @param applicationVersion
	* @param bankAccount
	* @param address
	* @param companyRegistrationNumber
	* @param mobileNumber
	* @param active
	* @param contactPerson
	* @param language
	* @param version
	* @param invoicingAddress
	* @param supplierBrand
	* @param approvedTermsAndConditions
	* @param number
	* @param phoneNumber
	* @param actionsInBulkAllowed
	* @param parentDealerNumber
	* @param rpr
	* @param name
	* @param dealerType
	* @param dealerSearchIndex
	* @param faxNumber
	* @param id
	* @param invoicingName
	* @param email
	* @param vatNumber
	*/
	public Dealer(Boolean actionsInBulkAllowed, Boolean active, Address__2 address, String applicationVersion, Boolean approvedTermsAndConditions, BankAccount__1 bankAccount, String companyRegistrationNumber, String contactPerson, String dealerType, String dealerSearchIndex, Email__2 email, FaxNumber__2 faxNumber, Integer id, InvoicingAddress__1 invoicingAddress, String invoicingName, String language, MobileNumber__2 mobileNumber, String name, Integer number, Integer parentDealerNumber, PhoneNumber__2 phoneNumber, String rpr, SupplierBrand__1 supplierBrand, String vatNumber, Integer version) {
	super();
	this.actionsInBulkAllowed = actionsInBulkAllowed;
	this.active = active;
	this.address = address;
	this.applicationVersion = applicationVersion;
	this.approvedTermsAndConditions = approvedTermsAndConditions;
	this.bankAccount = bankAccount;
	this.companyRegistrationNumber = companyRegistrationNumber;
	this.contactPerson = contactPerson;
	this.dealerType = dealerType;
	this.dealerSearchIndex = dealerSearchIndex;
	this.email = email;
	this.faxNumber = faxNumber;
	this.id = id;
	this.invoicingAddress = invoicingAddress;
	this.invoicingName = invoicingName;
	this.language = language;
	this.mobileNumber = mobileNumber;
	this.name = name;
	this.number = number;
	this.parentDealerNumber = parentDealerNumber;
	this.phoneNumber = phoneNumber;
	this.rpr = rpr;
	this.supplierBrand = supplierBrand;
	this.vatNumber = vatNumber;
	this.version = version;
	}

	public Boolean getActionsInBulkAllowed() {
		return actionsInBulkAllowed;
	}

	public void setActionsInBulkAllowed(Boolean actionsInBulkAllowed) {
		this.actionsInBulkAllowed = actionsInBulkAllowed;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Address__2 getAddress() {
		return address;
	}

	public void setAddress(Address__2 address) {
		this.address = address;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public Boolean getApprovedTermsAndConditions() {
		return approvedTermsAndConditions;
	}

	public void setApprovedTermsAndConditions(Boolean approvedTermsAndConditions) {
		this.approvedTermsAndConditions = approvedTermsAndConditions;
	}

	public BankAccount__1 getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount__1 bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getCompanyRegistrationNumber() {
		return companyRegistrationNumber;
	}

	public void setCompanyRegistrationNumber(String companyRegistrationNumber) {
		this.companyRegistrationNumber = companyRegistrationNumber;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getDealerType() {
		return dealerType;
	}

	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}

	public String getDealerSearchIndex() {
		return dealerSearchIndex;
	}

	public void setDealerSearchIndex(String dealerSearchIndex) {
		this.dealerSearchIndex = dealerSearchIndex;
	}

	public Email__2 getEmail() {
		return email;
	}

	public void setEmail(Email__2 email) {
		this.email = email;
	}

	public FaxNumber__2 getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(FaxNumber__2 faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public InvoicingAddress__1 getInvoicingAddress() {
		return invoicingAddress;
	}

	public void setInvoicingAddress(InvoicingAddress__1 invoicingAddress) {
		this.invoicingAddress = invoicingAddress;
	}

	public String getInvoicingName() {
		return invoicingName;
	}

	public void setInvoicingName(String invoicingName) {
		this.invoicingName = invoicingName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public MobileNumber__2 getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(MobileNumber__2 mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getParentDealerNumber() {
		return parentDealerNumber;
	}

	public void setParentDealerNumber(Integer parentDealerNumber) {
		this.parentDealerNumber = parentDealerNumber;
	}

	public PhoneNumber__2 getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(PhoneNumber__2 phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRpr() {
		return rpr;
	}

	public void setRpr(String rpr) {
		this.rpr = rpr;
	}

	public SupplierBrand__1 getSupplierBrand() {
		return supplierBrand;
	}

	public void setSupplierBrand(SupplierBrand__1 supplierBrand) {
		this.supplierBrand = supplierBrand;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
