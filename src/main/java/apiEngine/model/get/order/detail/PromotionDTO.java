package apiEngine.model.get.order.detail;

public class PromotionDTO {
	
	private Amount__1 amount;
	private Description description;
	private Integer id;
	private Name name;
	private Percentage percentage;
	private Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public PromotionDTO() {
	}

	/**
	*
	* @param amount
	* @param percentage
	* @param name
	* @param description
	* @param id
	* @param version
	*/
	public PromotionDTO(Amount__1 amount, Description description, Integer id, Name name, Percentage percentage, Integer version) {
	super();
	this.amount = amount;
	this.description = description;
	this.id = id;
	this.name = name;
	this.percentage = percentage;
	this.version = version;
	}

	public Amount__1 getAmount() {
		return amount;
	}

	public void setAmount(Amount__1 amount) {
		this.amount = amount;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public Percentage getPercentage() {
		return percentage;
	}

	public void setPercentage(Percentage percentage) {
		this.percentage = percentage;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
