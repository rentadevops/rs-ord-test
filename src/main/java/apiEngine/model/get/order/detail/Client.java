package apiEngine.model.get.order.detail;

import java.util.Optional;

public class Client {
	
	private String address = null;
	private String city = null;
	private String contactPerson = null;
	private String dealerNumber = null;
	private Email__1 email = null;
	private FaxNumber__1 faxNumber = null;
	private String languageCode = null;
	private MobileNumber__1 mobileNumber = null;
	private String name = null;
	private PhoneNumber__1 phoneNumber = null;
	private String postalCode = null;
	private Integer registrationNumber = null;
	private String vatNumber = null;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Client() {
	}

	/**
	*
	* @param address
	* @param city
	* @param mobileNumber
	* @param postalCode
	* @param contactPerson
	* @param languageCode
	* @param dealerNumber
	* @param phoneNumber
	* @param registrationNumber
	* @param name
	* @param faxNumber
	* @param email
	* @param vatNumber
	*/
	public Client(String address, String city, String contactPerson, String dealerNumber, Email__1 email, FaxNumber__1 faxNumber, String languageCode, MobileNumber__1 mobileNumber, String name, PhoneNumber__1 phoneNumber, String postalCode, Integer registrationNumber, String vatNumber) {
	super();
	this.address = address;
	this.city = city;
	this.contactPerson = contactPerson;
	this.dealerNumber = dealerNumber;
	this.email = email;
	this.faxNumber = faxNumber;
	this.languageCode = languageCode;
	this.mobileNumber = mobileNumber;
	this.name = name;
	this.phoneNumber = phoneNumber;
	this.postalCode = postalCode;
	this.registrationNumber = registrationNumber;
	this.vatNumber = vatNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getDealerNumber() {
		return dealerNumber;
	}

	public void setDealerNumber(String dealerNumber) {
		this.dealerNumber = dealerNumber;
	}

	public Email__1 getEmail() {
		return email;
	}

	public void setEmail(Email__1 email) {
		this.email = email;
	}

	public FaxNumber__1 getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(FaxNumber__1 faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public Optional getMobileNumberOpt() {
		return Optional.ofNullable(mobileNumber);
	}
	
	public MobileNumber__1 getMobileNumber() {
		return (MobileNumber__1) getMobileNumberOpt().orElse(null); 
	}

	public void setMobileNumber(MobileNumber__1 mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PhoneNumber__1 getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(PhoneNumber__1 phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Integer getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(Integer registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

}
