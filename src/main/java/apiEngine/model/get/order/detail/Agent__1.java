package apiEngine.model.get.order.detail;

public class Agent__1 {
	
	public Boolean actionsInBulkAllowed;
	public Boolean active;
	public Address__4 address;
	public String applicationVersion;
	public Boolean approvedTermsAndConditions;
	public BankAccount__2 bankAccount;
	public String companyRegistrationNumber;
	public String contactPerson;
	public String dealerType;
	public Email__5 email;
	public FaxNumber__4 faxNumber;
	public Integer id;
	public InvoicingAddress__2 invoicingAddress;
	public String invoicingName;
	public String language;
	public MobileNumber__5 mobileNumber;
	public String name;
	public Integer number;
	public Integer parentDealerNumber;
	public PhoneNumber__5 phoneNumber;
	public String rpr;
	public SupplierBrand__2 supplierBrand;
	public String vatNumber;
	public Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Agent__1() {
	}

	/**
	*
	* @param applicationVersion
	* @param bankAccount
	* @param address
	* @param companyRegistrationNumber
	* @param mobileNumber
	* @param active
	* @param contactPerson
	* @param language
	* @param version
	* @param invoicingAddress
	* @param supplierBrand
	* @param approvedTermsAndConditions
	* @param number
	* @param phoneNumber
	* @param actionsInBulkAllowed
	* @param parentDealerNumber
	* @param rpr
	* @param name
	* @param dealerType
	* @param faxNumber
	* @param id
	* @param invoicingName
	* @param email
	* @param vatNumber
	*/
	public Agent__1(Boolean actionsInBulkAllowed, Boolean active, Address__4 address, String applicationVersion, Boolean approvedTermsAndConditions, BankAccount__2 bankAccount, String companyRegistrationNumber, String contactPerson, String dealerType, Email__5 email, FaxNumber__4 faxNumber, Integer id, InvoicingAddress__2 invoicingAddress, String invoicingName, String language, MobileNumber__5 mobileNumber, String name, Integer number, Integer parentDealerNumber, PhoneNumber__5 phoneNumber, String rpr, SupplierBrand__2 supplierBrand, String vatNumber, Integer version) {
	super();
	this.actionsInBulkAllowed = actionsInBulkAllowed;
	this.active = active;
	this.address = address;
	this.applicationVersion = applicationVersion;
	this.approvedTermsAndConditions = approvedTermsAndConditions;
	this.bankAccount = bankAccount;
	this.companyRegistrationNumber = companyRegistrationNumber;
	this.contactPerson = contactPerson;
	this.dealerType = dealerType;
	this.email = email;
	this.faxNumber = faxNumber;
	this.id = id;
	this.invoicingAddress = invoicingAddress;
	this.invoicingName = invoicingName;
	this.language = language;
	this.mobileNumber = mobileNumber;
	this.name = name;
	this.number = number;
	this.parentDealerNumber = parentDealerNumber;
	this.phoneNumber = phoneNumber;
	this.rpr = rpr;
	this.supplierBrand = supplierBrand;
	this.vatNumber = vatNumber;
	this.version = version;
	}

	public Boolean getActionsInBulkAllowed() {
		return actionsInBulkAllowed;
	}

	public void setActionsInBulkAllowed(Boolean actionsInBulkAllowed) {
		this.actionsInBulkAllowed = actionsInBulkAllowed;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Address__4 getAddress() {
		return address;
	}

	public void setAddress(Address__4 address) {
		this.address = address;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public Boolean getApprovedTermsAndConditions() {
		return approvedTermsAndConditions;
	}

	public void setApprovedTermsAndConditions(Boolean approvedTermsAndConditions) {
		this.approvedTermsAndConditions = approvedTermsAndConditions;
	}

	public BankAccount__2 getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount__2 bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getCompanyRegistrationNumber() {
		return companyRegistrationNumber;
	}

	public void setCompanyRegistrationNumber(String companyRegistrationNumber) {
		this.companyRegistrationNumber = companyRegistrationNumber;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getDealerType() {
		return dealerType;
	}

	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}

	public Email__5 getEmail() {
		return email;
	}

	public void setEmail(Email__5 email) {
		this.email = email;
	}

	public FaxNumber__4 getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(FaxNumber__4 faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public InvoicingAddress__2 getInvoicingAddress() {
		return invoicingAddress;
	}

	public void setInvoicingAddress(InvoicingAddress__2 invoicingAddress) {
		this.invoicingAddress = invoicingAddress;
	}

	public String getInvoicingName() {
		return invoicingName;
	}

	public void setInvoicingName(String invoicingName) {
		this.invoicingName = invoicingName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public MobileNumber__5 getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(MobileNumber__5 mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getParentDealerNumber() {
		return parentDealerNumber;
	}

	public void setParentDealerNumber(Integer parentDealerNumber) {
		this.parentDealerNumber = parentDealerNumber;
	}

	public PhoneNumber__5 getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(PhoneNumber__5 phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRpr() {
		return rpr;
	}

	public void setRpr(String rpr) {
		this.rpr = rpr;
	}

	public SupplierBrand__2 getSupplierBrand() {
		return supplierBrand;
	}

	public void setSupplierBrand(SupplierBrand__2 supplierBrand) {
		this.supplierBrand = supplierBrand;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
