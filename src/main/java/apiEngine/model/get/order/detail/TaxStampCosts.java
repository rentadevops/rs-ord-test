package apiEngine.model.get.order.detail;

public class TaxStampCosts {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public TaxStampCosts() {
	}

	/**
	*
	* @param value
	*/
	public TaxStampCosts(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
