package apiEngine.model.get.order.detail;

public class Address__1 {

	private String addressLine;
	private String city;
	private String postalCode;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Address__1() {
	}

	/**
	*
	* @param city
	* @param postalCode
	* @param addressLine
	*/
	public Address__1(String addressLine, String city, String postalCode) {
	super();
	this.addressLine = addressLine;
	this.city = city;
	this.postalCode = postalCode;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
}
