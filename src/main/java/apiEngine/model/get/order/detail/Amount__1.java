package apiEngine.model.get.order.detail;

public class Amount__1 {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Amount__1() {
	}

	/**
	*
	* @param value
	*/
	public Amount__1(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
