package apiEngine.model.get.order.detail;

public class BankAccount__2 {
	
	public Bic__2 bic;
	public Iban__2 iban;

	/**
	* No args constructor for use in serialization
	*
	*/
	public BankAccount__2() {
	}

	/**
	*
	* @param iban
	* @param bic
	*/
	public BankAccount__2(Bic__2 bic, Iban__2 iban) {
	super();
	this.bic = bic;
	this.iban = iban;
	}

	public Bic__2 getBic() {
		return bic;
	}

	public void setBic(Bic__2 bic) {
		this.bic = bic;
	}

	public Iban__2 getIban() {
		return iban;
	}

	public void setIban(Iban__2 iban) {
		this.iban = iban;
	}

}
