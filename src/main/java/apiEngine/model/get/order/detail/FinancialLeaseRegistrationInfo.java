package apiEngine.model.get.order.detail;

public class FinancialLeaseRegistrationInfo {
	
	public String dateLastRegistration;

	/**
	* No args constructor for use in serialization
	*
	*/
	public FinancialLeaseRegistrationInfo() {
	}

	/**
	*
	* @param dateLastRegistration
	*/
	public FinancialLeaseRegistrationInfo(String dateLastRegistration) {
	super();
	this.dateLastRegistration = dateLastRegistration;
	}

	public String getDateLastRegistration() {
		return dateLastRegistration;
	}

	public void setDateLastRegistration(String dateLastRegistration) {
		this.dateLastRegistration = dateLastRegistration;
	}

}
