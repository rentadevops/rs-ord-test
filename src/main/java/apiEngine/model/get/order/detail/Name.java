package apiEngine.model.get.order.detail;

public class Name {
	
	public Translations__3 translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Name() {
	}

	/**
	*
	* @param translations
	*/
	public Name(Translations__3 translations) {
	super();
	this.translations = translations;
	}

	public Translations__3 getTranslations() {
		return translations;
	}

	public void setTranslations(Translations__3 translations) {
		this.translations = translations;
	}

}
