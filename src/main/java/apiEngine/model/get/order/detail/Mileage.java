package apiEngine.model.get.order.detail;

public class Mileage {
	
	public Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Mileage() {
	}

	/**
	*
	* @param value
	*/
	public Mileage(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
