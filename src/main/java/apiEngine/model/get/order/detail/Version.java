package apiEngine.model.get.order.detail;

public class Version {
	
	private Translations translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Version() {
	}

	/**
	*
	* @param translations
	*/
	public Version(Translations translations) {
	super();
	this.translations = translations;
	}

	public Translations getTranslations() {
		return translations;
	}

	public void setTranslations(Translations translations) {
		this.translations = translations;
	}

}
