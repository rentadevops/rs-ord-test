package apiEngine.model.get.order.detail;

public class SupplierBrand__1 {
	
	public String name;
	public Integer rumId;
	public Integer supplierBrandId;
	public Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public SupplierBrand__1() {
	}

	/**
	*
	* @param name
	* @param rumId
	* @param supplierbrandId
	* @param version
	*/
	public SupplierBrand__1(String name, Integer rumId, Integer supplierBrandId, Integer version) {
	super();
	this.name = name;
	this.rumId = rumId;
	this.supplierBrandId = supplierBrandId;
	this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRumId() {
		return rumId;
	}

	public void setRumId(Integer rumId) {
		this.rumId = rumId;
	}

	public Integer getSupplierBrandId() {
		return supplierBrandId;
	}

	public void setSupplierbrandId(Integer supplierBrandId) {
		this.supplierBrandId = supplierBrandId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
