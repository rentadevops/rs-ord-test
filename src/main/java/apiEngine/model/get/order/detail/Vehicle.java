package apiEngine.model.get.order.detail;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Vehicle {
	
	//public String @type;
	private String type;
	private Integer id;
	private String brand;
	private Model model;
	private Version version;
	private ExteriorColour exteriorColour;
	private String licensePlate;
	private TransferredLicensePlate transferredLicensePlate;
	private Vin vin;
	private String vinControlNumber;
	private String identification;
	private Boolean stockVehicle;
	private Integer weight;
	private String tyreDescription;
	private String languagePapers;
	private Integer cylinderContent;
	private Integer power;
	private String tyreBrand;
	private String tyreType;
	private InteriorColour interiorColour;
	private Co2EmissionsNedc co2EmissionsNedc;
	private Co2EmissionsWltp co2EmissionsWltp;
	private Object numberOfSeats;
	private Object fuelLabel;
	private Object kindLabel;
	private Object massOnCoupling;
	private Object authorizedMassMax;
	private Object dealerVehicleInfo;
	private Long firstRegistrationDate;
	private Integer constructionYear;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	* No args constructor for use in serialization
	*
	*/
	public Vehicle() {
	}

	/**
	*
	* @param stockVehicle
	* @param co2EmissionsNedc
	* @param dealerVehicleInfo
	* @param tyreBrand
	* @param tyreType
	* @param type
	* @param interiorColour
	* @param licensePlate
	* @param identification
	* @param languagePapers
	* @param model
	* @param vin
	* @param id
	* @param power
	* @param brand
	* @param exteriorColour
	* @param transferredLicensePlate
	* @param kindLabel
	* @param massOnCoupling
	* @param weight
	* @param tyreDescription
	* @param co2EmissionsWltp
	* @param version
	* @param numberOfSeats
	* @param vinControlNumber
	* @param fuelLabel
	* @param authorizedMassMax
	* @param cylinderContent
	*/
	public Vehicle(String type, Integer id, String brand, Model model, Version version, ExteriorColour exteriorColour, String licensePlate, TransferredLicensePlate transferredLicensePlate, Vin vin, String vinControlNumber, String identification, Boolean stockVehicle, Integer weight, String tyreDescription, String languagePapers, Integer cylinderContent, Integer power, String tyreBrand, String tyreType, InteriorColour interiorColour, Co2EmissionsNedc co2EmissionsNedc, Co2EmissionsWltp co2EmissionsWltp, Object numberOfSeats, Object fuelLabel, Object kindLabel, Object massOnCoupling, Object authorizedMassMax, Object dealerVehicleInfo, Long firstRegistrationDate, Integer constructionYear) {
		super();
		this.type = type;
		this.id = id;
		this.brand = brand;
		this.model = model;
		this.version = version;
		this.exteriorColour = exteriorColour;
		this.licensePlate = licensePlate;
		this.transferredLicensePlate = transferredLicensePlate;
		this.vin = vin;
		this.vinControlNumber = vinControlNumber;
		this.identification = identification;
		this.stockVehicle = stockVehicle;
		this.weight = weight;
		this.tyreDescription = tyreDescription;
		this.languagePapers = languagePapers;
		this.cylinderContent = cylinderContent;
		this.power = power;
		this.tyreBrand = tyreBrand;
		this.tyreType = tyreType;
		this.interiorColour = interiorColour;
		this.co2EmissionsNedc = co2EmissionsNedc;
		this.co2EmissionsWltp = co2EmissionsWltp;
		this.numberOfSeats = numberOfSeats;
		this.fuelLabel = fuelLabel;
		this.kindLabel = kindLabel;
		this.massOnCoupling = massOnCoupling;
		this.authorizedMassMax = authorizedMassMax;
		this.dealerVehicleInfo = dealerVehicleInfo;
		this.firstRegistrationDate = firstRegistrationDate;
		this.constructionYear = constructionYear;
		}

	public String getType() {
		return type;
		}

		public void setType(String type) {
		this.type = type;
		}

		public Integer getId() {
		return id;
		}

		public void setId(Integer id) {
		this.id = id;
		}

		public String getBrand() {
		return brand;
		}

		public void setBrand(String brand) {
		this.brand = brand;
		}

		public Model getModel() {
		return model;
		}

		public void setModel(Model model) {
		this.model = model;
		}

		public Version getVersion() {
		return version;
		}

		public void setVersion(Version version) {
		this.version = version;
		}

		public ExteriorColour getExteriorColour() {
		return exteriorColour;
		}

		public void setExteriorColour(ExteriorColour exteriorColour) {
		this.exteriorColour = exteriorColour;
		}

		public String getLicensePlate() {
		return licensePlate;
		}

		public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
		}

		public TransferredLicensePlate getTransferredLicensePlate() {
		return transferredLicensePlate;
		}

		public void setTransferredLicensePlate(TransferredLicensePlate transferredLicensePlate) {
		this.transferredLicensePlate = transferredLicensePlate;
		}

		public Vin getVin() {
		return vin;
		}

		public void setVin(Vin vin) {
		this.vin = vin;
		}

		public String getVinControlNumber() {
		return vinControlNumber;
		}

		public void setVinControlNumber(String vinControlNumber) {
		this.vinControlNumber = vinControlNumber;
		}

		public String getIdentification() {
		return identification;
		}

		public void setIdentification(String identification) {
		this.identification = identification;
		}

		public Boolean getStockVehicle() {
		return stockVehicle;
		}

		public void setStockVehicle(Boolean stockVehicle) {
		this.stockVehicle = stockVehicle;
		}

		public Integer getWeight() {
		return weight;
		}

		public void setWeight(Integer weight) {
		this.weight = weight;
		}

		public String getTyreDescription() {
		return tyreDescription;
		}

		public void setTyreDescription(String tyreDescription) {
		this.tyreDescription = tyreDescription;
		}

		public String getLanguagePapers() {
		return languagePapers;
		}

		public void setLanguagePapers(String languagePapers) {
		this.languagePapers = languagePapers;
		}

		public Integer getCylinderContent() {
		return cylinderContent;
		}

		public void setCylinderContent(Integer cylinderContent) {
		this.cylinderContent = cylinderContent;
		}

		public Integer getPower() {
		return power;
		}

		public void setPower(Integer power) {
		this.power = power;
		}

		public String getTyreBrand() {
		return tyreBrand;
		}

		public void setTyreBrand(String tyreBrand) {
		this.tyreBrand = tyreBrand;
		}

		public String getTyreType() {
		return tyreType;
		}

		public void setTyreType(String tyreType) {
		this.tyreType = tyreType;
		}

		public InteriorColour getInteriorColour() {
		return interiorColour;
		}

		public void setInteriorColour(InteriorColour interiorColour) {
		this.interiorColour = interiorColour;
		}

		public Co2EmissionsNedc getCo2EmissionsNedc() {
		return co2EmissionsNedc;
		}

		public void setCo2EmissionsNedc(Co2EmissionsNedc co2EmissionsNedc) {
		this.co2EmissionsNedc = co2EmissionsNedc;
		}

		public Co2EmissionsWltp getCo2EmissionsWltp() {
		return co2EmissionsWltp;
		}

		public void setCo2EmissionsWltp(Co2EmissionsWltp co2EmissionsWltp) {
		this.co2EmissionsWltp = co2EmissionsWltp;
		}

		public Object getNumberOfSeats() {
		return numberOfSeats;
		}

		public void setNumberOfSeats(Object numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
		}

		public Object getFuelLabel() {
		return fuelLabel;
		}

		public void setFuelLabel(Object fuelLabel) {
		this.fuelLabel = fuelLabel;
		}

		public Object getKindLabel() {
		return kindLabel;
		}

		public void setKindLabel(Object kindLabel) {
		this.kindLabel = kindLabel;
		}

		public Object getMassOnCoupling() {
		return massOnCoupling;
		}

		public void setMassOnCoupling(Object massOnCoupling) {
		this.massOnCoupling = massOnCoupling;
		}

		public Object getAuthorizedMassMax() {
		return authorizedMassMax;
		}

		public void setAuthorizedMassMax(Object authorizedMassMax) {
		this.authorizedMassMax = authorizedMassMax;
		}

		public Object getDealerVehicleInfo() {
		return dealerVehicleInfo;
		}

		public void setDealerVehicleInfo(Object dealerVehicleInfo) {
		this.dealerVehicleInfo = dealerVehicleInfo;
		}
		
		public Long getFirstRegistrationDate() {
			return firstRegistrationDate;
			}

		public void setFirstRegistrationDate(Long firstRegistrationDate) {
		this.firstRegistrationDate = firstRegistrationDate;
		}
		
		public Integer getConstructionYear() {
			return constructionYear;
			}

		public void setConstructionYear(Integer constructionYear) {
		this.constructionYear = constructionYear;
		}
		



}
