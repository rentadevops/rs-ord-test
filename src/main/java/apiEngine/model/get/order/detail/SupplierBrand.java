package apiEngine.model.get.order.detail;

public class SupplierBrand {
	
	public String name;
	public Integer rumId;
	public Integer supplierbrandId;
	public Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public SupplierBrand() {
	}

	/**
	*
	* @param name
	* @param rumId
	* @param supplierbrandId
	* @param version
	*/
	public SupplierBrand(String name, Integer rumId, Integer supplierbrandId, Integer version) {
	super();
	this.name = name;
	this.rumId = rumId;
	this.supplierbrandId = supplierbrandId;
	this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRumId() {
		return rumId;
	}

	public void setRumId(Integer rumId) {
		this.rumId = rumId;
	}

	public Integer getSupplierbrandId() {
		return supplierbrandId;
	}

	public void setSupplierbrandId(Integer supplierbrandId) {
		this.supplierbrandId = supplierbrandId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
