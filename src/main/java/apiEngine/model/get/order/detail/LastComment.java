package apiEngine.model.get.order.detail;

import java.math.BigInteger;

public class LastComment {
	
	public Agent__1 agent;
	public String comment;
	public BigInteger creationDate;
	public String orderChange;
	public Integer updatedExpectedDeliveryDate;
	public String updatedOrderStatus;
	public User user;

	/**
	* No args constructor for use in serialization
	*
	*/
	public LastComment() {
	}

	/**
	*
	* @param updatedExpectedDeliveryDate
	* @param agent
	* @param updatedOrderStatus
	* @param comment
	* @param creationDate
	* @param orderChange
	* @param user
	*/
	public LastComment(Agent__1 agent, String comment, BigInteger creationDate, String orderChange, Integer updatedExpectedDeliveryDate, String updatedOrderStatus, User user) {
	super();
	this.agent = agent;
	this.comment = comment;
	this.creationDate = creationDate;
	this.orderChange = orderChange;
	this.updatedExpectedDeliveryDate = updatedExpectedDeliveryDate;
	this.updatedOrderStatus = updatedOrderStatus;
	this.user = user;
	}

	public Agent__1 getAgent() {
		return agent;
	}

	public void setAgent(Agent__1 agent) {
		this.agent = agent;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public BigInteger getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(BigInteger creationDate) {
		this.creationDate = creationDate;
	}

	public String getOrderChange() {
		return orderChange;
	}

	public void setOrderChange(String orderChange) {
		this.orderChange = orderChange;
	}

	public Integer getUpdatedExpectedDeliveryDate() {
		return updatedExpectedDeliveryDate;
	}

	public void setUpdatedExpectedDeliveryDate(Integer updatedExpectedDeliveryDate) {
		this.updatedExpectedDeliveryDate = updatedExpectedDeliveryDate;
	}

	public String getUpdatedOrderStatus() {
		return updatedOrderStatus;
	}

	public void setUpdatedOrderStatus(String updatedOrderStatus) {
		this.updatedOrderStatus = updatedOrderStatus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
