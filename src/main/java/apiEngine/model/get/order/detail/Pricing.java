package apiEngine.model.get.order.detail;

public class Pricing {
	
	private DeliveryCosts deliveryCosts;
	private FleetDiscountAmount fleetDiscountAmount;
	private FleetDiscountPercentage fleetDiscountPercentage;
	private ListPrice listPrice;
	private OtherCosts otherCosts;
	private TotalPrice totalPrice;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Pricing() {
	}

	/**
	*
	* @param otherCosts
	* @param deliveryCosts
	* @param fleetDiscountAmount
	* @param totalPrice
	* @param fleetDiscountPercentage
	* @param listPrice
	*/
	public Pricing(DeliveryCosts deliveryCosts, FleetDiscountAmount fleetDiscountAmount, FleetDiscountPercentage fleetDiscountPercentage, ListPrice listPrice, OtherCosts otherCosts, TotalPrice totalPrice) {
	super();
	this.deliveryCosts = deliveryCosts;
	this.fleetDiscountAmount = fleetDiscountAmount;
	this.fleetDiscountPercentage = fleetDiscountPercentage;
	this.listPrice = listPrice;
	this.otherCosts = otherCosts;
	this.totalPrice = totalPrice;
	}

	public DeliveryCosts getDeliveryCosts() {
		return deliveryCosts;
	}

	public void setDeliveryCosts(DeliveryCosts deliveryCosts) {
		this.deliveryCosts = deliveryCosts;
	}

	public FleetDiscountAmount getFleetDiscountAmount() {
		return fleetDiscountAmount;
	}

	public void setFleetDiscountAmount(FleetDiscountAmount fleetDiscountAmount) {
		this.fleetDiscountAmount = fleetDiscountAmount;
	}

	public FleetDiscountPercentage getFleetDiscountPercentage() {
		return fleetDiscountPercentage;
	}

	public void setFleetDiscountPercentage(FleetDiscountPercentage fleetDiscountPercentage) {
		this.fleetDiscountPercentage = fleetDiscountPercentage;
	}

	public ListPrice getListPrice() {
		return listPrice;
	}

	public void setListPrice(ListPrice listPrice) {
		this.listPrice = listPrice;
	}

	public OtherCosts getOtherCosts() {
		return otherCosts;
	}

	public void setOtherCosts(OtherCosts otherCosts) {
		this.otherCosts = otherCosts;
	}

	public TotalPrice getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(TotalPrice totalPrice) {
		this.totalPrice = totalPrice;
	}

}
