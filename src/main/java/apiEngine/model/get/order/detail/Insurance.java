package apiEngine.model.get.order.detail;

public class Insurance {
	
	public Email__4 email;
	public InsuredValuePrice insuredValuePrice;
	public String name;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Insurance() {
	}

	/**
	*
	* @param insuredValuePrice
	* @param name
	* @param email
	*/
	public Insurance(Email__4 email, InsuredValuePrice insuredValuePrice, String name) {
	super();
	this.email = email;
	this.insuredValuePrice = insuredValuePrice;
	this.name = name;
	}

	public Email__4 getEmail() {
		return email;
	}

	public void setEmail(Email__4 email) {
		this.email = email;
	}

	public InsuredValuePrice getInsuredValuePrice() {
		return insuredValuePrice;
	}

	public void setInsuredValuePrice(InsuredValuePrice insuredValuePrice) {
		this.insuredValuePrice = insuredValuePrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
