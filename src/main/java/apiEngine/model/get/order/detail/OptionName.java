package apiEngine.model.get.order.detail;

public class OptionName {
	
	public Translations__1 translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public OptionName() {
	}

	/**
	*
	* @param translations
	*/
	public OptionName(Translations__1 translations) {
	super();
	this.translations = translations;
	}

	public Translations__1 getTranslations() {
		return translations;
	}

	public void setTranslations(Translations__1 translations) {
		this.translations = translations;
	}

}
