package apiEngine.model.get.order.notifications.feed;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import helpers.AssertHelper;

public class Vehicle {
	final Logger logger = LogManager.getLogger(Vehicle.class);
	private String batteryPower;
	private String batteryPowerUnit;
	private String bikeCategory;
	private String brand;
	private Boolean carbon;
	private Boolean certifiedLock;
	private String constructionYear;
	private String co2EmissionsNedc;
	private String co2EmissionsWltp;
	private Boolean certificateOfConformity;
	private Boolean fuelCard;
	private Boolean legalKit;
	private Boolean manual;
	private Boolean proofOfInsurance;
	private Boolean proofOfRegistration;
	private Boolean proofOfRoadworthiness;
	private Integer mileage;
	private String startCode;
	private String keyCode;
	private Integer numberOfKeys;
	private Boolean previousVehicleDropoff;
	private String previousVehicleOwner;
	private String previousVehicleLicensePlate;
	private String tyreBrand;
	private String tyreType;
	private String driverFirstName;
	private String driverLastName;
	private String driverBirthDate;
	private String driverBirthPlace;
	private Integer previousVehicleOwnerLeasingCompanyRsNumber;
	private String dateFirstRegistration;
	private String dateLastRegistration;
	private ExteriorColour exteriorColour;
	private InteriorColour interiorColour;
	private Integer engineDisplacement;
	private Integer power;
	private Integer maxWeight;
	private String fuelType;
	private String frameSize;
	private String frameSizeUnit;
	private String frameType;
	private Boolean fullDetailsAvailable;
	private String gear;
	private String identification;
	private String languagePapers;
	private String licensePlate;
	private Model model;
	private Segment segment;
	private Boolean stockVehicle;
	private String tyreDescription;
	private Version version;
	private Vin vin;
	private String vinControlNumber;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getBatteryPower() {
		return batteryPower;
	}

	public void setBatteryPower(String batteryPower) {
		this.batteryPower = batteryPower;
	}

	public String getBatteryPowerUnit() {
		return batteryPowerUnit;
	}

	public void setBatteryPowerUnit(String batteryPowerUnit) {
		this.batteryPowerUnit = batteryPowerUnit;
	}

	public String getBikeCategory() {
		return bikeCategory;
	}

	public void setBikeCategory(String bikeCategory) {
		this.bikeCategory = bikeCategory;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Boolean getCarbon() {
		return carbon;
	}

	public void setCarbon(Boolean carbon) {
		this.carbon = carbon;
	}

	public Boolean getCertifiedLock() {
		return certifiedLock;
	}

	public void setCertifiedLock(Boolean certifiedLock) {
		this.certifiedLock = certifiedLock;
	}

	public String getConstructionYear() {
		return constructionYear;
	}

	public void setConstructionYear(String constructionYear) {
		this.constructionYear = constructionYear;
	}

	public String getCo2EmissionsNedc() {
		Locale loc = new Locale("en", "US");
		NumberFormat nf = NumberFormat.getNumberInstance(loc);
		DecimalFormat df = (DecimalFormat)nf;
		String pattern = "#,###.##";
		df.applyPattern(pattern);
		//DecimalFormat myFormatter = new DecimalFormat(pattern);
		Double DoubleNEDC = Double.parseDouble(co2EmissionsNedc);
		co2EmissionsNedc = df.format(DoubleNEDC).toString();
		logger.debug("co2EmissionsNedc: " + co2EmissionsNedc);
		return co2EmissionsNedc;
	}

	public void setCo2EmissionsNedc(String co2EmissionsNedc) {
		this.co2EmissionsNedc = co2EmissionsNedc;
	}

	public String getCo2EmissionsWltp() {
		Locale loc = new Locale("en", "US");
		NumberFormat nf = NumberFormat.getNumberInstance(loc);
		DecimalFormat df = (DecimalFormat)nf;
		String pattern = "#,###.##";
		df.applyPattern(pattern);
		logger.debug("co2EmissionsWltp voor delete comma: " + co2EmissionsWltp);
		String co2EmissionsWltpTmp = co2EmissionsWltp.replace(",", "");
		logger.debug("co2EmissionsWltpTmp voor cast naar double: " + co2EmissionsWltpTmp);
		//DecimalFormat myFormatter = new DecimalFormat(pattern);
		Double DoubleWLTP = Double.parseDouble(co2EmissionsWltpTmp);
		co2EmissionsWltpTmp = df.format(DoubleWLTP).toString();
		logger.debug("co2EmissionsWltpTmp: " + co2EmissionsWltpTmp);
		co2EmissionsWltp = co2EmissionsWltpTmp;
		return co2EmissionsWltp;
	}

	public void setCo2EmissionsWltp(String co2EmissionsWltp) {
		this.co2EmissionsWltp = co2EmissionsWltp;
	}

	public Boolean getCertificateOfConformity() {
		return certificateOfConformity;
	}

	public void setCertificateOfConformity(Boolean certificateOfConformity) {
		this.certificateOfConformity = certificateOfConformity;
	}

	public Boolean getFuelCard() {
		return fuelCard;
	}

	public void setFuelCard(Boolean fuelCard) {
		this.fuelCard = fuelCard;
	}

	public Boolean getLegalKit() {
		return legalKit;
	}

	public void setLegalKit(Boolean legalKit) {
		this.legalKit = legalKit;
	}

	public Boolean getManual() {
		return manual;
	}

	public void setManual(Boolean manual) {
		this.manual = manual;
	}

	public Boolean getProofOfInsurance() {
		return proofOfInsurance;
	}

	public void setProofOfInsurance(Boolean proofOfInsurance) {
		this.proofOfInsurance = proofOfInsurance;
	}

	public Boolean getProofOfRegistration() {
		return proofOfRegistration;
	}

	public void setProofOfRegistration(Boolean proofOfRegistration) {
		this.proofOfRegistration = proofOfRegistration;
	}

	public Boolean getProofOfRoadworthiness() {
		return proofOfRoadworthiness;
	}

	public void setProofOfRoadworthiness(Boolean proofOfRoadworthiness) {
		this.proofOfRoadworthiness = proofOfRoadworthiness;
	}

	public Integer getMileage() {
		return mileage;
	}

	public void setMileage(Integer mileage) {
		this.mileage = mileage;
	}

	public String getStartCode() {
		return startCode;
	}

	public void setStartCode(String startCode) {
		this.startCode = startCode;
	}

	public String getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public Integer getNumberOfKeys() {
		return numberOfKeys;
	}

	public void setNumberOfKeys(Integer numberOfKeys) {
		this.numberOfKeys = numberOfKeys;
	}

	public Boolean getPreviousVehicleDropoff() {
		return previousVehicleDropoff;
	}

	public void setPreviousVehicleDropoff(Boolean previousVehicleDropoff) {
		this.previousVehicleDropoff = previousVehicleDropoff;
	}

	public String getPreviousVehicleOwner() {
		return previousVehicleOwner;
	}

	public void setPreviousVehicleOwner(String previousVehicleOwner) {
		this.previousVehicleOwner = previousVehicleOwner;
	}

	public String getPreviousVehicleLicensePlate() {
		return previousVehicleLicensePlate;
	}

	public void setPreviousVehicleLicensePlate(String previousVehicleLicensePlate) {
		this.previousVehicleLicensePlate = previousVehicleLicensePlate;
	}

	public String getTyreBrand() {
		return tyreBrand;
	}

	public void setTyreBrand(String tyreBrand) {
		this.tyreBrand = tyreBrand;
	}

	public String getTyreType() {
		return tyreType;
	}

	public void setTyreType(String tyreType) {
		this.tyreType = tyreType;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverLastName() {
		return driverLastName;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}

	public String getDriverBirthDate() {
		return driverBirthDate;
	}

	public void setDriverBirthDate(String driverBirthDate) {
		this.driverBirthDate = driverBirthDate;
	}

	public String getDriverBirthPlace() {
		return driverBirthPlace;
	}

	public void setDriverBirthPlace(String driverBirthPlace) {
		this.driverBirthPlace = driverBirthPlace;
	}

	public Integer getPreviousVehicleOwnerLeasingCompanyRsNumber() {
		return previousVehicleOwnerLeasingCompanyRsNumber;
	}

	public void setPreviousVehicleOwnerLeasingCompanyRsNumber(Integer previousVehicleOwnerLeasingCompanyRsNumber) {
		this.previousVehicleOwnerLeasingCompanyRsNumber = previousVehicleOwnerLeasingCompanyRsNumber;
	}

	public String getDateFirstRegistration() {
		return dateFirstRegistration;
	}

	public void setDateFirstRegistration(String dateFirstRegistration) {
		this.dateFirstRegistration = dateFirstRegistration;
	}

	public String getDateLastRegistration() {
		return dateLastRegistration;
	}

	public void setDateLastRegistration(String dateLastRegistration) {
		this.dateLastRegistration = dateLastRegistration;
	}

	public ExteriorColour getExteriorColour() {
		return exteriorColour;
	}

	public void setExteriorColour(ExteriorColour exteriorColour) {
		this.exteriorColour = exteriorColour;
	}

	public InteriorColour getInteriorColour() {
		return interiorColour;
	}

	public void setInteriorColour(InteriorColour interiorColour) {
		this.interiorColour = interiorColour;
	}

	public Integer getEngineDisplacement() {
		return engineDisplacement;
	}

	public void setEngineDisplacement(Integer engineDisplacement) {
		this.engineDisplacement = engineDisplacement;
	}

	public Integer getPower() {
		return power;
	}

	public void setPower(Integer power) {
		this.power = power;
	}

	public Integer getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(Integer maxWeight) {
		this.maxWeight = maxWeight;
	}
	
	public String getFrameSize() {
		return frameSize;
	}

	public void setFrameSize(String frameSize) {
		this.frameSize = frameSize;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getFrameSizeUnit() {
		return frameSizeUnit;
	}

	public void setFrameSizeUnit(String frameSizeUnit) {
		this.frameSizeUnit = frameSizeUnit;
	}

	public String getFrameType() {
		return frameType;
	}

	public void setFrameType(String frameType) {
		this.frameType = frameType;
	}

	public Boolean getFullDetailsAvailable() {
		return fullDetailsAvailable;
	}

	public void setFullDetailsAvailable(Boolean fullDetailsAvailable) {
		this.fullDetailsAvailable = fullDetailsAvailable;
	}

	public String getGear() {
		return gear;
	}

	public void setGear(String gear) {
		this.gear = gear;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getLanguagePapers() {
		return languagePapers;
	}

	public void setLanguagePapers(String languagePapers) {
		this.languagePapers = languagePapers;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	public Boolean getStockVehicle() {
		return stockVehicle;
	}

	public void setStockVehicle(Boolean stockVehicle) {
		this.stockVehicle = stockVehicle;
	}

	public String getTyreDescription() {
		return tyreDescription;
	}

	public void setTyreDescription(String tyreDescription) {
		this.tyreDescription = tyreDescription;
	}

	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		this.version = version;
	}

	public Vin getVin() {
		return vin;
	}

	public void setVin(Vin vin) {
		this.vin = vin;
	}

	public String getVinControlNumber() {
		return vinControlNumber;
	}

	public void setVinControlNumber(String vinControlNumber) {
		this.vinControlNumber = vinControlNumber;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
