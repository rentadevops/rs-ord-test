package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Address__1 {
	private String addressLine;
	private String city;
	private String postalCode;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getAddressLine() {
	return addressLine;
	}

	public void setAddressLine(String addressLine) {
	this.addressLine = addressLine;
	}

	public String getCity() {
	return city;
	}

	public void setCity(String city) {
	this.city = city;
	}

	public String getPostalCode() {
	return postalCode;
	}

	public void setPostalCode(String postalCode) {
	this.postalCode = postalCode;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
