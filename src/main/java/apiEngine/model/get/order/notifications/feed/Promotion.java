package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Promotion {
	private Description__1 description;
	private Discount__1 discount;
	private Integer id;
	private Name__1 name;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Description__1 getDescription() {
	return description;
	}

	public void setDescription(Description__1 description) {
	this.description = description;
	}

	public Discount__1 getDiscount() {
	return discount;
	}

	public void setDiscount(Discount__1 discount) {
	this.discount = discount;
	}

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public Name__1 getName() {
	return name;
	}

	public void setName(Name__1 name) {
	this.name = name;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
