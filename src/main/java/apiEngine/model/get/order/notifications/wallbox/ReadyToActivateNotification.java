
package apiEngine.model.get.order.notifications.wallbox;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for ReadyToActivateNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReadyToActivateNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://be/renta/ordering/communication/wallboxnotification}AbstractNotification"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReadyToActivateNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification")
public class ReadyToActivateNotification
    extends AbstractNotification
{


}
