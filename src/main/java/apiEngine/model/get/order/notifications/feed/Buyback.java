package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Buyback {
	private Address__3 address;
	private String name;
	private Integer contractDuration;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Address__3 getAddress() {
	return address;
	}

	public void setAddress(Address__3 address) {
	this.address = address;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public Integer getContractDuration() {
	return contractDuration;
	}

	public void setContractDuration(Integer contractDuration) {
	this.contractDuration = contractDuration;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
