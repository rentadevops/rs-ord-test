package apiEngine.model.get.order.notifications.feed;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Pricing {
	final Logger logger = LogManager.getLogger(Pricing.class);
	private DeliveryCosts deliveryCosts;
	private FleetDiscount fleetDiscount;
	private String listPrice;
	private List<Option> options = null;
	private List<Promotion> promotions = null;
	private String totalPrice;
	private String pricingTotalPriceTemp;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public DeliveryCosts getDeliveryCosts() {
		Locale loc = new Locale("en", "EN");
		DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
		dfs.setCurrencySymbol("\u20ac");
		decimalFormat.setDecimalFormatSymbols(dfs);
		if(listPrice.startsWith("€")) {
			listPrice = listPrice.replace("€" , "");
			listPrice = listPrice.replace("," , "");
		} else {
			listPrice = listPrice.replace("," , ".");
		}

		Double DoubleListPrice = Double.parseDouble(listPrice);
		listPrice = decimalFormat.format(DoubleListPrice).toString();
		logger.debug("pricingListPrice: " + listPrice);

		return deliveryCosts;
	}

	public void setDeliveryCosts(DeliveryCosts deliveryCosts) {
	this.deliveryCosts = deliveryCosts;
	}

	public FleetDiscount getFleetDiscount() {
	return fleetDiscount;
	}

	public void setFleetDiscount(FleetDiscount fleetDiscount) {
	this.fleetDiscount = fleetDiscount;
	}

	public String getListPrice() {
		//Locale loc = new Locale("en", "EN");
		//DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
		//DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
		//dfs.setCurrencySymbol("\u20ac");
		//decimalFormat.setDecimalFormatSymbols(dfs);
		//Double DoubleListPrice = Double.parseDouble(listPrice);
		//listPrice = decimalFormat.format(DoubleListPrice).toString();
		//logger.debug("listPrice: " + listPrice);
	//return listPrice;

		Locale loc = new Locale("en", "EN");
		DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
		dfs.setCurrencySymbol("\u20ac");
		decimalFormat.setDecimalFormatSymbols(dfs);
		if(listPrice.startsWith("€")) {
			listPrice = listPrice.replace("€" , "");
			listPrice = listPrice.replace("," , "");
		} else {
			listPrice = listPrice.replace("," , ".");
		}

		Double DoubleListPrice = Double.parseDouble(listPrice);
		listPrice = decimalFormat.format(DoubleListPrice).toString();
		logger.debug("pricingListPrice: " + listPrice);
		return listPrice;
	}

	public void setListPrice(String listPrice) {
	this.listPrice = listPrice;
	}

	public List<Option> getOptions() {
	return options;
	}

	public void setOptions(List<Option> options) {
	this.options = options;
	}

	public List<Promotion> getPromotions() {
	return promotions;
	}

	public void setPromotions(List<Promotion> promotions) {
	this.promotions = promotions;
	}

	public String getTotalPrice() {
		//Locale loc = new Locale("en", "EN");
		//DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
		//DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
		//dfs.setCurrencySymbol("\u20ac");
		//decimalFormat.setDecimalFormatSymbols(dfs);
		//logger.debug("totalPrice in getter voor cast naar double: " + totalPrice);
		//Double DoubleTotalPrice = Double.parseDouble(totalPrice);
		//totalPrice = decimalFormat.format(DoubleTotalPrice).toString();
		//logger.debug("totalPrice: " + totalPrice);
	//return totalPrice;
		pricingTotalPriceTemp = totalPrice;
		Locale loc = new Locale("en", "EN");
		DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
		dfs.setCurrencySymbol("\u20ac");
		decimalFormat.setDecimalFormatSymbols(dfs);
		logger.debug("pricingTotalPriceTemp in getter voor change van comma naar punt: " + pricingTotalPriceTemp);
		if(pricingTotalPriceTemp.startsWith("€")) {
			pricingTotalPriceTemp = pricingTotalPriceTemp.replace("€" , "");
			pricingTotalPriceTemp = pricingTotalPriceTemp.replace("," , "");
		} else {
			pricingTotalPriceTemp = pricingTotalPriceTemp.replace("," , ".");
		}

		logger.debug("pricingTotalPriceTemp in getter: " + pricingTotalPriceTemp);
		Double d = Double.parseDouble(pricingTotalPriceTemp);
		pricingTotalPriceTemp = decimalFormat.format(d).toString();

		//Number number = decimalFormat.parse(pricingTotalPrice);
		logger.debug("pricingTotalPrice: " + pricingTotalPriceTemp);
		totalPrice = pricingTotalPriceTemp;
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
	this.totalPrice = totalPrice;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
