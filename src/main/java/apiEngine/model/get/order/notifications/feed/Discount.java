package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Discount {
	private Integer amount;
	private Integer percentage;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Integer getAmount() {
	return amount;
	}

	public void setAmount(Integer amount) {
	this.amount = amount;
	}

	public Integer getPercentage() {
	return percentage;
	}

	public void setPercentage(Integer percentage) {
	this.percentage = percentage;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	
	public String toString() { 
	    return "amount: '" + this.amount + "', percentage: '" + this.percentage + "'";
	} 

}
