package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class WallBox {
	private Discount discount;
	private String brand;
	private Model model;
	private Description description;
	private Double listPrice;
	private Double vat;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Discount getDiscount() {
	return discount;
	}

	public void setDiscount(Discount discount) {
	this.discount = discount;
	}

	public String getBrand() {
	return brand;
	}

	public void setBrand(String brand) {
	this.brand = brand;
	}

	public Model getModel() {
	return model;
	}

	public void setModel(Model model) {
	this.model = model;
	}

	public Description getDescription() {
	return description;
	}

	public void setDescription(Description description) {
	this.description = description;
	}

	public Double getListPrice() {
	return listPrice;
	}

	public void setListPrice(Double listPrice) {
	this.listPrice = listPrice;
	}

	public Double getVat() {
	return vat;
	}

	public void setVat(Double vat) {
	this.vat = vat;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
