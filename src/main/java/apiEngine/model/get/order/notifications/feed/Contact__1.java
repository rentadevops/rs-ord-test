package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Contact__1 {
	private String email;
	private String mobileNumber;
	private String phoneNumber;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getEmail() {
	return email;
	}

	public void setEmail(String email) {
	this.email = email;
	}

	public String getMobileNumber() {
	return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
	this.mobileNumber = mobileNumber;
	}

	public String getPhoneNumber() {
	return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
