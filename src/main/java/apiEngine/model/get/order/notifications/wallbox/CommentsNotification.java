
package apiEngine.model.get.order.notifications.wallbox;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;


/**
 * <p>Java class for CommentsNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommentsNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://be/renta/ordering/communication/wallboxnotification}AbstractNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommentsNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification", propOrder = {
    "comments"
})
public class CommentsNotification
    extends AbstractNotification
{

    @XmlElement(name = "Comments", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    protected String comments;

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

}
