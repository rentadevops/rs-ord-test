package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Translations__6 {
	private String de;
	private String en;
	private String fr;
	private String nl;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getDe() {
	return de;
	}

	public void setDe(String de) {
	this.de = de;
	}

	public String getEn() {
	return en;
	}

	public void setEn(String en) {
	this.en = en;
	}

	public String getFr() {
	return fr;
	}

	public void setFr(String fr) {
	this.fr = fr;
	}

	public String getNl() {
	return nl;
	}

	public void setNl(String nl) {
	this.nl = nl;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
