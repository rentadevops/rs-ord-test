package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Option {
	private String code;
	private Description description;
	private Discount discount;
	private Integer id;
	private Integer listPrice;
	private Name name;
	private String type;
	private Integer vat;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getCode() {
	return code;
	}

	public void setCode(String code) {
	this.code = code;
	}

	public Description getDescription() {
	return description;
	}

	public void setDescription(Description description) {
	this.description = description;
	}

	public Discount getDiscount() {
	return discount;
	}

	public void setDiscount(Discount discount) {
	this.discount = discount;
	}

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public Integer getListPrice() {
	return listPrice;
	}

	public void setListPrice(Integer listPrice) {
	this.listPrice = listPrice;
	}

	public Name getName() {
	return name;
	}

	public void setName(Name name) {
	this.name = name;
	}

	public String getType() {
	return type;
	}

	public void setType(String type) {
	this.type = type;
	}

	public Integer getVat() {
	return vat;
	}

	public void setVat(Integer vat) {
	this.vat = vat;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	
	
	public String toString() { 
	    return "code: '" + this.code + "', description: '" + this.description + "', discount: '" + this.discount + "', id: '" + this.id + "', listPrice: '" + this.listPrice + "'"
	    		+ ", name: '" + this.name + "', type: '" + this.type + "', vat: '" + this.vat + "'";
	} 

}
