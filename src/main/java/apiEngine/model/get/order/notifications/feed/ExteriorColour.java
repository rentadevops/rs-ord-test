package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class ExteriorColour {
	private String dutch;
	private String english;
	private String french;
	private Translations__4 translations;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getDutch() {
	return dutch;
	}

	public void setDutch(String dutch) {
	this.dutch = dutch;
	}

	public String getEnglish() {
	return english;
	}

	public void setEnglish(String english) {
	this.english = english;
	}

	public String getFrench() {
	return french;
	}

	public void setFrench(String french) {
	this.french = french;
	}

	public Translations__4 getTranslations() {
	return translations;
	}

	public void setTranslations(Translations__4 translations) {
	this.translations = translations;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
