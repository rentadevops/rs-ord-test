package apiEngine.model.get.order.notifications.feed;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Discount__1 {
	final Logger logger = LogManager.getLogger(Discount__1.class);
	private String amount;
	private String percentage;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getAmount() {
		//Locale loc = new Locale("en", "EN");
		//DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
		//DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
		//dfs.setCurrencySymbol("\u20ac");
		//decimalFormat.setDecimalFormatSymbols(dfs);
		amount = amount.replace("." , ",");
		//Double DoubleAmount = Double.parseDouble(amount);
		//amount = decimalFormat.format(DoubleAmount).toString();
		logger.debug("DiscountAmount: " + amount);
	return amount;
	}

	public void setAmount(String amount) {
	this.amount = amount;
	}

	public String getPercentage() {
	return percentage;
	}

	public void setPercentage(String percentage) {
	this.percentage = percentage;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
