
package apiEngine.model.get.order.notifications.wallbox;

//import javax.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.*;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AbstractNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCode" type="{http://be/renta/ordering/communication/wallboxnotification}TransactionCode"/&gt;
 *         &lt;element name="NotificationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="DossierNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification", propOrder = {
    "transactionCode",
    "notificationDate",
    "dossierNumber"
})
@XmlSeeAlso({
    DealerAcceptedNotification.class,
    NotAcceptedNotification.class,
    OrderedNotification.class,
    ReadyToActivateNotification.class,
    ActivatedNotification.class,
    CommentsNotification.class,
    InstalledNotification.class,
    CancelledNotification.class
})
public abstract class AbstractNotification {

    @XmlElement(name = "TransactionCode", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCode transactionCode;
    @XmlElement(name = "NotificationDate", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar notificationDate;
    @XmlElement(name = "DossierNumber", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    protected String dossierNumber;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCode }
     *     
     */
    public TransactionCode getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCode }
     *     
     */
    public void setTransactionCode(TransactionCode value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the notificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNotificationDate() {
        return notificationDate;
    }

    /**
     * Sets the value of the notificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNotificationDate(XMLGregorianCalendar value) {
        this.notificationDate = value;
    }

    /**
     * Gets the value of the dossierNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDossierNumber() {
        return dossierNumber;
    }

    /**
     * Sets the value of the dossierNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDossierNumber(String value) {
        this.dossierNumber = value;
    }

}
