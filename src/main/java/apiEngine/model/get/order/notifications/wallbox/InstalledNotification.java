
package apiEngine.model.get.order.notifications.wallbox;

//import javax.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.*;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InstalledNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstalledNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://be/renta/ordering/communication/wallboxnotification}AbstractNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;sequence&gt;
 *           &lt;element name="EVBNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="ChargeCardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="SentCard" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *           &lt;element name="InstallationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *           &lt;element name="CertificationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstalledNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification", propOrder = {
    "evbNumber",
    "chargeCardNumber",
    "sentCard",
    "installationDate",
    "certificationDate"
})
public class InstalledNotification
    extends AbstractNotification
{

    @XmlElement(name = "EVBNumber", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    protected String evbNumber;
    @XmlElement(name = "ChargeCardNumber", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    protected String chargeCardNumber;
    @XmlElement(name = "SentCard", namespace = "http://be/renta/ordering/communication/wallboxnotification")
    protected boolean sentCard;
    @XmlElement(name = "InstallationDate", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar installationDate;
    @XmlElement(name = "CertificationDate", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar certificationDate;

    /**
     * Gets the value of the evbNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVBNumber() {
        return evbNumber;
    }

    /**
     * Sets the value of the evbNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVBNumber(String value) {
        this.evbNumber = value;
    }

    /**
     * Gets the value of the chargeCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCardNumber() {
        return chargeCardNumber;
    }

    /**
     * Sets the value of the chargeCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCardNumber(String value) {
        this.chargeCardNumber = value;
    }

    /**
     * Gets the value of the sentCard property.
     * 
     */
    public boolean isSentCard() {
        return sentCard;
    }

    /**
     * Sets the value of the sentCard property.
     * 
     */
    public void setSentCard(boolean value) {
        this.sentCard = value;
    }

    /**
     * Gets the value of the installationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInstallationDate() {
        return installationDate;
    }

    /**
     * Sets the value of the installationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInstallationDate(XMLGregorianCalendar value) {
        this.installationDate = value;
    }

    /**
     * Gets the value of the certificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertificationDate() {
        return certificationDate;
    }

    /**
     * Sets the value of the certificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertificationDate(XMLGregorianCalendar value) {
        this.certificationDate = value;
    }

}
