package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class InteriorColour {
	private String dutch;
	private Object french;
	private Object english;
	private Translations__6 translations;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getDutch() {
	return dutch;
	}

	public void setDutch(String dutch) {
	this.dutch = dutch;
	}

	public Object getFrench() {
	return french;
	}

	public void setFrench(Object french) {
	this.french = french;
	}

	public Object getEnglish() {
	return english;
	}

	public void setEnglish(Object english) {
	this.english = english;
	}

	public Translations__6 getTranslations() {
	return translations;
	}

	public void setTranslations(Translations__6 translations) {
	this.translations = translations;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
