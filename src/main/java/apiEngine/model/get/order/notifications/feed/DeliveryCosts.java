package apiEngine.model.get.order.notifications.feed;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DeliveryCosts {
	final Logger logger = LogManager.getLogger(DeliveryCosts.class);
	private String value;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getValue() {
		//Kopieren van totalPrice of listPrice
		Locale loc = new Locale("en", "EN");
		DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
		dfs.setCurrencySymbol("\u20ac");
		decimalFormat.setDecimalFormatSymbols(dfs);
		//value = value.replace(",", ".");
		//Double DoublePricingDeliveryCosts = Double.parseDouble(value);
		//value = decimalFormat.format(DoublePricingDeliveryCosts).toString();
		// logger.debug("pricingDeliveryCosts: " + pricingDeliveryCosts);

		//return value;


		if(value.startsWith("€")) {
			value = value.replace("€" , "");
			value = value.replace("," , "");
		} else {
			value = value.replace("," , ".");
		}

		Double DoubleValue = Double.parseDouble(value);
		value = decimalFormat.format(DoubleValue).toString();
		logger.debug("deliveryCosts value: " + value);

		return value;


	}

	public void setValue(String value) {
		this.value = value;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
