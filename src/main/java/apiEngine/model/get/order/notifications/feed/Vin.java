package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Vin {
	private String value;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getValue() {
	return value;
	}

	public void setValue(String value) {
	this.value = value;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
