
package apiEngine.model.get.order.notifications.wallbox;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlRegistry;
//import javax.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.*;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.renta package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.renta
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WallBoxOrderNotifications }
     * 
     */
    public WallBoxOrderNotifications createWallBoxOrderNotifications() {
        return new WallBoxOrderNotifications();
    }

    /**
     * Create an instance of {@link WallBoxOrderNotification }
     * 
     */
    public WallBoxOrderNotification createWallBoxOrderNotification() {
        return new WallBoxOrderNotification();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link Discount }
     * 
     */
    public Discount createDiscount() {
        return new Discount();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Translatable }
     * 
     */
    public Translatable createTranslatable() {
        return new Translatable();
    }

    /**
     * Create an instance of {@link DealerAcceptedNotification }
     * 
     */
    public DealerAcceptedNotification createDealerAcceptedNotification() {
        return new DealerAcceptedNotification();
    }

    /**
     * Create an instance of {@link NotAcceptedNotification }
     * 
     */
    public NotAcceptedNotification createNotAcceptedNotification() {
        return new NotAcceptedNotification();
    }

    /**
     * Create an instance of {@link OrderedNotification }
     * 
     */
    public OrderedNotification createOrderedNotification() {
        return new OrderedNotification();
    }

    /**
     * Create an instance of {@link ReadyToActivateNotification }
     * 
     */
    public ReadyToActivateNotification createReadyToActivateNotification() {
        return new ReadyToActivateNotification();
    }

    /**
     * Create an instance of {@link ActivatedNotification }
     * 
     */
    public ActivatedNotification createActivatedNotification() {
        return new ActivatedNotification();
    }

    /**
     * Create an instance of {@link CommentsNotification }
     * 
     */
    public CommentsNotification createCommentsNotification() {
        return new CommentsNotification();
    }

    /**
     * Create an instance of {@link InstalledNotification }
     * 
     */
    public InstalledNotification createInstalledNotification() {
        return new InstalledNotification();
    }

    /**
     * Create an instance of {@link CancelledNotification }
     * 
     */
    public CancelledNotification createCancelledNotification() {
        return new CancelledNotification();
    }

}
