package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Driver {
	private Address__2 address;
	private Contact__2 contact;
	private String fullName;
	private String languageCode;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Address__2 getAddress() {
	return address;
	}

	public void setAddress(Address__2 address) {
	this.address = address;
	}

	public Contact__2 getContact() {
	return contact;
	}

	public void setContact(Contact__2 contact) {
	this.contact = contact;
	}

	public String getFullName() {
	return fullName;
	}

	public void setFullName(String fullName) {
	this.fullName = fullName;
	}

	public String getLanguageCode() {
	return languageCode;
	}

	public void setLanguageCode(String languageCode) {
	this.languageCode = languageCode;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
