package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Client {
	private Address address;
	private Contact contact;
	private String contactPerson;
	private String languageCode;
	private String name;
	private String vatNumber;
	private String registrationNumberLessee;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Address getAddress() {
	return address;
	}

	public void setAddress(Address address) {
	this.address = address;
	}

	public Contact getContact() {
	return contact;
	}

	public void setContact(Contact contact) {
	this.contact = contact;
	}

	public String getContactPerson() {
	return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
	this.contactPerson = contactPerson;
	}

	public String getLanguageCode() {
	return languageCode;
	}

	public void setLanguageCode(String languageCode) {
	this.languageCode = languageCode;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getRegistrationNumberLessee() {
	return registrationNumberLessee;
	}

	public void setRegistrationNumberLessee(String registrationNumberLessee) {
	this.registrationNumberLessee = registrationNumberLessee;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
