package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Address__2 {
	private String city;
	private String postalCode;
	private String streetHouseNumber;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getCity() {
	return city;
	}

	public void setCity(String city) {
	this.city = city;
	}

	public String getPostalCode() {
	return postalCode;
	}

	public void setPostalCode(String postalCode) {
	this.postalCode = postalCode;
	}

	public String getStreetHouseNumber() {
	return streetHouseNumber;
	}

	public void setStreetHouseNumber(String streetHouseNumber) {
	this.streetHouseNumber = streetHouseNumber;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
