package apiEngine.model.get.order.notifications.feed;

//import apiEngine.model.get.order.detail.Amount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class OrderNotificationFeed {
	final Logger logger = LogManager.getLogger(OrderNotificationFeed.class);
	private Client client;
	private String comment;
	private Delivery delivery;
	private Driver driver;
	private Integer id;
	private LeasingCompany leasingCompany;
	private String notificationType;
	private String orderDate;
	private WallBox wallBox;
	private String orderId;
	private String orderStatus;
	private String actualDeliveryDate;
	private String expectedDeliveryDate;
	private String expectedInstallationDate;
	private Pricing pricing;
	private String quoteReference;
	private Supplier supplier;
	private Sender sender;
	private Vehicle vehicle;
	private Buyback buyback;
	private String leasingType;
	private String registrationDivToDoBy;
	private String evbNumber;
	private String chargeCardNumber;
	private Boolean sentCard;
	private String installationDate;
	private String certificationDate;
	private String activationDate;
	private String countryCode;
	private String tyreFitter;
	private Insurance insurance;
	private Integer inscriptionCosts;
	private Long taxStampCosts;
	private String licensePlateType;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();


	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LeasingCompany getLeasingCompany() {
		return leasingCompany;
	}

	public void setLeasingCompany(LeasingCompany leasingCompany) {
		this.leasingCompany = leasingCompany;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public WallBox getWallBox() {
		return wallBox;
	}

	public void setWallBox(WallBox wallBox) {
		this.wallBox = wallBox;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public void setActualDeliveryDate(String actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}

	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	
	public String getExpectedInstallationDate() {
		return expectedInstallationDate;
	}

	public void setExpectedInstallationDate(String expectedInstallationDate) {
		this.expectedInstallationDate = expectedInstallationDate;
	}

	public Pricing getPricing() {
		return pricing;
	}

	public void setPricing(Pricing pricing) {
		this.pricing = pricing;
	}

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Buyback getBuyback() {
		return buyback;
	}

	public void setBuyback(Buyback buyback) {
		this.buyback = buyback;
	}

	public String getLeasingType() {
		return leasingType;
	}

	public void setLeasingType(String leasingType) {
		this.leasingType = leasingType;
	}

	public String getRegistrationDivToDoBy() {
		return registrationDivToDoBy;
	}

	public void setRegistrationDivToDoBy(String registrationDivToDoBy) {
		this.registrationDivToDoBy = registrationDivToDoBy;
	}

	public String getEvbNumber() {
		logger.debug("evbNumber in getter in OrderNotificationFeed: " + evbNumber);
		return evbNumber;
	}

	public void setEvbNumber(String evbNumber) {
		this.evbNumber = evbNumber;
	}

	public String getChargeCardNumber() {
		return chargeCardNumber;
	}

	public void setChargeCardNumber(String chargeCardNumber) {
		this.chargeCardNumber = chargeCardNumber;
	}

	public Boolean getSentCard() {
		return sentCard;
	}

	public void setSentCard(Boolean sentCard) {
		this.sentCard = sentCard;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}

	public String getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(String certificationDate) {
		this.certificationDate = certificationDate;
	}

	public String getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getTyreFitter() {
		return tyreFitter;
	}

	public void setTyreFitter(String tyreFitter) {
		this.tyreFitter = tyreFitter;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public Integer getInscriptionCosts() {
		return inscriptionCosts;
	}

	public void setInscriptionCosts(Integer inscriptionCosts) {
		this.inscriptionCosts = inscriptionCosts;
	}

	public Long getTaxStampCosts() {
		return taxStampCosts;
	}

	public void setTaxStampCosts(Long taxStampCosts) {
		this.taxStampCosts = taxStampCosts;
	}

	public String getLicensePlateType() {
		return licensePlateType;
	}

	public void setLicensePlateType(String licensePlateType) {
		this.licensePlateType = licensePlateType;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public String toString() {
		return "Leasing Company: '" + this.leasingCompany.toString() + "', " +
				"notificationType: '" + this.notificationType + "', " +
				"orderDate: '" + this.orderDate + "', " +
				"orderId: '" + this.orderId + "', " +
				"orderStatus: '" + this.orderStatus + "', " +
				"actualDeliveryDate: '" + this.actualDeliveryDate + "', " +
				"expectedDeliveryDate: '" + this.expectedDeliveryDate + "', " +
				"expectedInstallationDate: '" + this.expectedInstallationDate + "', " +
				"leasingType: '" + this.leasingType + "', " +
				"evbNumber: '" + this.evbNumber + "', " +
				"chargeCardNumber: '" + this.chargeCardNumber + "', " +
				"sentCard: '" + this.sentCard + "', " +
				"installationDate: '" + this.installationDate + "', " +
				"countryCode: '" + this.countryCode + "', " +
				"tyreFitter: '" + this.tyreFitter + "', " +
				"insurance: '" + this.insurance + "', " +
				"inscriptionCosts: '" + this.inscriptionCosts + "', " +
				"taxStampCosts: '" + this.taxStampCosts + "', " +

				"certificationDate: '" + this.certificationDate + "'";
	}
}
