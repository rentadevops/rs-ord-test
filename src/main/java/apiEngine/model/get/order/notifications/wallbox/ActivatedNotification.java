
package apiEngine.model.get.order.notifications.wallbox;

//import javax.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.*;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ActivatedNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivatedNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://be/renta/ordering/communication/wallboxnotification}AbstractNotification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActivationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivatedNotification", namespace = "http://be/renta/ordering/communication/wallboxnotification", propOrder = {
    "activationDate"
})
public class ActivatedNotification
    extends AbstractNotification
{

    @XmlElement(name = "ActivationDate", namespace = "http://be/renta/ordering/communication/wallboxnotification", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar activationDate;

    /**
     * Gets the value of the activationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivationDate() {
        return activationDate;
    }

    /**
     * Sets the value of the activationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivationDate(XMLGregorianCalendar value) {
        this.activationDate = value;
    }

}
