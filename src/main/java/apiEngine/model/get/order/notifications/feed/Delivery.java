package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pageobjects.api.API_GetOrderDetailById;

public class Delivery {
	final Logger logger = LogManager.getLogger(Delivery.class);
	private Address__1 address;
	private Contact__1 contact;
	private String contactPerson;
	private String desiredInstallationDate;
	private String desiredDeliveryDate;
	private String languageCode;
	private String locationName;
	private String vatNumber;
	private String proposedDeliveryDate;
	private String deliveryContact;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Address__1 getAddress() {
	return address;
	}

	public void setAddress(Address__1 address) {
	this.address = address;
	}

	public Contact__1 getContact() {
	return contact;
	}

	public void setContact(Contact__1 contact) {
	this.contact = contact;
	}

	public String getContactPerson() {
	return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
	this.contactPerson = contactPerson;
	}

	public String getDesiredInstallationDate() {
		desiredInstallationDate = desiredInstallationDate.replace("T00:00:00", "");
	return desiredInstallationDate;
	}

	public void setDesiredInstallationDate(String desiredInstallationDate) {
	this.desiredInstallationDate = desiredInstallationDate;
	}
	
	public String getDesiredDeliveryDate() {
		desiredDeliveryDate = desiredDeliveryDate.replace("T00:00:00", "");
		logger.debug("desiredDeliveryDate Getter: " + desiredDeliveryDate);
	return desiredDeliveryDate;
	}

	public void setDesiredDeliveryDate(String desiredDeliveryDate) {
	this.desiredDeliveryDate = desiredDeliveryDate;
	}

	public String getLanguageCode() {
	return languageCode;
	}

	public void setLanguageCode(String languageCode) {
	this.languageCode = languageCode;
	}

	public String getLocationName() {
	return locationName;
	}

	public void setLocationName(String locationName) {
	this.locationName = locationName;
	}

	public String getVatNumber() {
	return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
	this.vatNumber = vatNumber;
	}

	public String getProposedDeliveryDate() {
		return proposedDeliveryDate;
	}

	public void setProposedDeliveryDate(String proposedDeliveryDate) {
		this.proposedDeliveryDate = proposedDeliveryDate;
	}

	public String getDeliveryContact() {
		return deliveryContact;
	}

	public void setDeliveryContact(String deliveryContact) {
		this.deliveryContact = deliveryContact;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
