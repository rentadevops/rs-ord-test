package apiEngine.model.get.order.notifications.feed;

public class InsuredValuePrice {
	
	public Double value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public InsuredValuePrice() {
	}

	/**
	*
	* @param value
	*/
	public InsuredValuePrice(Double value) {
	super();
	this.value = value;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
