package apiEngine.model.get.order.notifications.feed;

import java.util.HashMap;
import java.util.Map;

public class Supplier {
	private String name;
	private String number;
	private String dossierNumber;
	private String vatNumber;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getNumber() {
	return number;
	}

	public void setNumber(String number) {
	this.number = number;
	}

	public String getDossierNumber() {
	return dossierNumber;
	}

	public void setDossierNumber(String dossierNumber) {
	this.dossierNumber = dossierNumber;
	}

	public String getVatNumber() {
	return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
	this.vatNumber = vatNumber;
	}

	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
