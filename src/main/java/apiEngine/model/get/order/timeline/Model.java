package apiEngine.model.get.order.timeline;

public class Model {
	
	private Translations translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Model() {
	}

	/**
	*
	* @param translations
	*/
	public Model(Translations translations) {
	super();
	this.translations = translations;
	}

	public Translations getTranslations() {
	return translations;
	}

	public void setTranslations(Translations translations) {
	this.translations = translations;
	}

}
