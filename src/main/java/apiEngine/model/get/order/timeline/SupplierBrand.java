package apiEngine.model.get.order.timeline;

public class SupplierBrand {
	
	private String name;
	private Integer rumId;
	private Integer supplierBrandId;
	private Integer version;

	/**
	* No args constructor for use in serialization
	*
	*/
	public SupplierBrand() {
	}

	/**
	*
	* @param supplierBrandId
	* @param name
	* @param rumId
	* @param version
	*/
	public SupplierBrand(String name, Integer rumId, Integer supplierBrandId, Integer version) {
	super();
	this.name = name;
	this.rumId = rumId;
	this.supplierBrandId = supplierBrandId;
	this.version = version;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public Integer getRumId() {
	return rumId;
	}

	public void setRumId(Integer rumId) {
	this.rumId = rumId;
	}

	public Integer getSupplierBrandId() {
	return supplierBrandId;
	}

	public void setSupplierBrandId(Integer supplierBrandId) {
	this.supplierBrandId = supplierBrandId;
	}

	public Integer getVersion() {
	return version;
	}

	public void setVersion(Integer version) {
	this.version = version;
	}

}
