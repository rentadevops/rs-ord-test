package apiEngine.model.get.order.timeline.wallbox;

import java.util.ArrayList;
import java.util.List;

public class WallBoxOrderTimeLine {
    private String id;
    private String currentStatus;
    private String creationDate;
    private String linkedOrderId;
    private Integer supplierId;
    private Integer leasingCompanyId;
    private Object followUpDates;
    private DeliveryDates deliveryDates;
    private WallBox wallBox;
    private List<History> history = new ArrayList<History>();
    private String leasingCompanyDossierNumber;
    private Object leasingCompanyFollowUpDate;
    private Object supplierFollowUpDate;
    private Float totalPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLinkedOrderId() {
        return linkedOrderId;
    }

    public void setLinkedOrderId(String linkedOrderId) {
        this.linkedOrderId = linkedOrderId;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public Integer getLeasingCompanyId() {
        return leasingCompanyId;
    }

    public void setLeasingCompanyId(Integer leasingCompanyId) {
        this.leasingCompanyId = leasingCompanyId;
    }

    public Object getFollowUpDates() {
        return followUpDates;
    }

    public void setFollowUpDates(Object followUpDates) {
        this.followUpDates = followUpDates;
    }

    public DeliveryDates getDeliveryDates() {
        return deliveryDates;
    }

    public void setDeliveryDates(DeliveryDates deliveryDates) {
        this.deliveryDates = deliveryDates;
    }

    public WallBox getWallBox() {
        return wallBox;
    }

    public void setWallBox(WallBox wallBox) {
        this.wallBox = wallBox;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public String getLeasingCompanyDossierNumber() {
        return leasingCompanyDossierNumber;
    }

    public void setLeasingCompanyDossierNumber(String leasingCompanyDossierNumber) {
        this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
    }

    public Object getLeasingCompanyFollowUpDate() {
        return leasingCompanyFollowUpDate;
    }

    public void setLeasingCompanyFollowUpDate(Object leasingCompanyFollowUpDate) {
        this.leasingCompanyFollowUpDate = leasingCompanyFollowUpDate;
    }

    public Object getSupplierFollowUpDate() {
        return supplierFollowUpDate;
    }

    public void setSupplierFollowUpDate(Object supplierFollowUpDate) {
        this.supplierFollowUpDate = supplierFollowUpDate;
    }

    public Float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }
}
