package apiEngine.model.get.order.timeline.wallbox;

public class Translations {
    private String nl;
    private Object en;
    private Object fr;
    private String de;

    public String getNl() {
        return nl;
    }

    public void setNl(String nl) {
        this.nl = nl;
    }

    public Object getEn() {
        return en;
    }

    public void setEn(Object en) {
        this.en = en;
    }

    public Object getFr() {
        return fr;
    }

    public void setFr(Object fr) {
        this.fr = fr;
    }

    public String getDe() {
        return de;
    }

    public void setDe(String de) {
        this.de = de;
    }
}
