package apiEngine.model.get.order.timeline.wallbox;

public class WallBox {
    private String brand;
    private Model model;
    private Description description;
    private String evbNumber;
    private String chargeCardNumber;
    private Boolean cardSent;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public String getEvbNumber() {
        return evbNumber;
    }

    public void setEvbNumber(String evbNumber) {
        this.evbNumber = evbNumber;
    }

    public String getChargeCardNumber() {
        return chargeCardNumber;
    }

    public void setChargeCardNumber(String chargeCardNumber) {
        this.chargeCardNumber = chargeCardNumber;
    }

    public Boolean getCardSent() {
        return cardSent;
    }

    public void setCardSent(Boolean cardSent) {
        this.cardSent = cardSent;
    }
}
