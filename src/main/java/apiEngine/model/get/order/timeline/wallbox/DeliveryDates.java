package apiEngine.model.get.order.timeline.wallbox;

public class DeliveryDates {
    private String activationDate;
    private String certificationDate;
    private Object desiredInstallationDate;
    private String expectedInstallationDate;
    private String installationDate;

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getCertificationDate() {
        return certificationDate;
    }

    public void setCertificationDate(String certificationDate) {
        this.certificationDate = certificationDate;
    }

    public Object getDesiredInstallationDate() {
        return desiredInstallationDate;
    }

    public void setDesiredInstallationDate(Object desiredInstallationDate) {
        this.desiredInstallationDate = desiredInstallationDate;
    }

    public String getExpectedInstallationDate() {
        return expectedInstallationDate;
    }

    public void setExpectedInstallationDate(String expectedInstallationDate) {
        this.expectedInstallationDate = expectedInstallationDate;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }
}
