package apiEngine.model.get.order.timeline.wallbox;

public class Model {
    private Translations translations;

    public Translations getTranslations() {
        return translations;
    }

    public void setTranslations(Translations translations) {
        this.translations = translations;
    }
}
