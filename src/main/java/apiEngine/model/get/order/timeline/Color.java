package apiEngine.model.get.order.timeline;

public class Color {
	
	private Translations__1 translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Color() {
	}

	/**
	*
	* @param translations
	*/
	public Color(Translations__1 translations) {
	super();
	this.translations = translations;
	}

	public Translations__1 getTranslations() {
	return translations;
	}

	public void setTranslations(Translations__1 translations) {
	this.translations = translations;
	}

}
