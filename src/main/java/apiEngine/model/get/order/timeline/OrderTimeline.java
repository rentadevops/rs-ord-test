package apiEngine.model.get.order.timeline;

import java.util.List;

public class OrderTimeline {
	
	private Agent agent;
	private String creationDate;
	private String currentStatus;
	private String dealerDossierNumber;
	private String deliveryContact;
	private DeliveryDates deliveryDates;
	private Integer eventVersion;
	private FollowUpDates followUpDates;
	private List<History> history = null;
	private String id;
	private InscriptionCosts inscriptionCosts;
	private Boolean leasingCompanyActionNeeded;
	private String leasingCompanyDossierNumber;
	private Integer leasingCompanyId;
	private String leasingType;
	private String linkedRegistrationId;
	private String linkedOrderId;
	private String orderType;
	private Boolean previousVehicleDropoff;
	private Long proposedDeliveryDate;
	private String registrationDivToDoBy;
	private Boolean supplierActionNeeded;
	private Integer supplierId;
	private TaxStampCosts taxStampCosts;
	private TotalPrice totalPrice;
	private Vehicle vehicle;
	private Integer version;
	private WallBox wallBox;
	private String digitalDeliveryStatus;



	private Object leasingCompanyFollowUpDate;
	private Object supplierFollowUpDate;


	/**
	* No args constructor for use in serialization
	*
	*/
	public OrderTimeline() {
	}

	/**
	*
	* @param proposedDeliveryDate
	* @param linkedRegistrationId
	* @param orderType
	* @param agent
	* @param supplierId
	* @param leasingCompanyActionNeeded
	* @param currentStatus
	* @param totalPrice
	* @param eventVersion
	* @param previousVehicleDropoff
	* @param history
	* @param leasingCompanyId
	* @param creationDate
	* @param version
	* @param vehicle
	* @param inscriptionCosts
	* @param leasingType
	* @param supplierActionNeeded
	* @param deliveryContact
	* @param registrationDivToDoBy
	* @param id
	* @param taxStampCosts
	* @param deliveryDates
	* @param followUpDates
	* @param digitalDeliveryStatus
	*/
	public OrderTimeline(Agent agent, String creationDate, String currentStatus, String dealerDossierNumber, String deliveryContact, DeliveryDates deliveryDates, Integer eventVersion, FollowUpDates followUpDates, List<History> history, String id, InscriptionCosts inscriptionCosts, String leasingCompanyDossierNumber, Boolean leasingCompanyActionNeeded, Integer leasingCompanyId, String leasingType, String linkedRegistrationId, String linkedOrderId, String orderType, Boolean previousVehicleDropoff, Long proposedDeliveryDate, String registrationDivToDoBy, Boolean supplierActionNeeded, Integer supplierId, TaxStampCosts taxStampCosts, TotalPrice totalPrice, Vehicle vehicle, Integer version, WallBox wallBox, Object leasingCompanyFollowUpDate, Object supplierFollowUpDate, String digitalDeliveryStatus) {
	super();
	this.agent = agent;
	this.creationDate = creationDate;
	this.currentStatus = currentStatus;
	this.dealerDossierNumber = dealerDossierNumber;
	this.deliveryContact = deliveryContact;
	this.deliveryDates = deliveryDates;
	this.eventVersion = eventVersion;
	this.followUpDates = followUpDates;
	this.history = history;
	this.id = id;
	this.inscriptionCosts = inscriptionCosts;
	this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
	this.leasingCompanyActionNeeded = leasingCompanyActionNeeded;
	this.leasingCompanyId = leasingCompanyId;
	this.leasingType = leasingType;
	this.linkedRegistrationId = linkedRegistrationId;
	this.linkedOrderId = linkedOrderId;
	this.orderType = orderType;
	this.previousVehicleDropoff = previousVehicleDropoff;
	this.proposedDeliveryDate = proposedDeliveryDate;
	this.registrationDivToDoBy = registrationDivToDoBy;
	this.supplierActionNeeded = supplierActionNeeded;
	this.supplierId = supplierId;
	this.taxStampCosts = taxStampCosts;
	this.totalPrice = totalPrice;
	this.vehicle = vehicle;
	this.version = version;
	this.wallBox = wallBox;
	this.leasingCompanyFollowUpDate = leasingCompanyFollowUpDate;
	this.supplierFollowUpDate = supplierFollowUpDate;
	this.digitalDeliveryStatus = digitalDeliveryStatus;
	}

	public Agent getAgent() {
	return agent;
	}

	public void setAgent(Agent agent) {
	this.agent = agent;
	}

	public String getCreationDate() {
	return creationDate;
	}

	public void setCreationDate(String creationDate) {
	this.creationDate = creationDate;
	}

	public String getCurrentStatus() {
	return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
	this.currentStatus = currentStatus;
	}
	
	public String getDealerDossierNumber() {
		return dealerDossierNumber;
		}

		public void setDealerDossierNumber(String dealerDossierNumber) {
		this.dealerDossierNumber = dealerDossierNumber;
		}

	public String getDeliveryContact() {
	return deliveryContact;
	}

	public void setDeliveryContact(String deliveryContact) {
	this.deliveryContact = deliveryContact;
	}

	public DeliveryDates getDeliveryDates() {
	return deliveryDates;
	}

	public void setDeliveryDates(DeliveryDates deliveryDates) {
	this.deliveryDates = deliveryDates;
	}

	public Integer getEventVersion() {
	return eventVersion;
	}

	public void setEventVersion(Integer eventVersion) {
	this.eventVersion = eventVersion;
	}

	public FollowUpDates getFollowUpDates() {
	return followUpDates;
	}

	public void setFollowUpDates(FollowUpDates followUpDates) {
	this.followUpDates = followUpDates;
	}

	public List<History> getHistory() {
	return history;
	}

	public void setHistory(List<History> history) {
	this.history = history;
	}

	public String getId() {
	return id;
	}

	public void setId(String id) {
	this.id = id;
	}

	public InscriptionCosts getInscriptionCosts() {
	return inscriptionCosts;
	}

	public void setInscriptionCosts(InscriptionCosts inscriptionCosts) {
	this.inscriptionCosts = inscriptionCosts;
	}
	
	public String getLeasingCompanyDossierNumber() {
		return leasingCompanyDossierNumber;
		}

		public void setLeasingCompanyDossierNumber(String leasingCompanyDossierNumber) {
		this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
		}

	public Boolean getLeasingCompanyActionNeeded() {
	return leasingCompanyActionNeeded;
	}

	public void setLeasingCompanyActionNeeded(Boolean leasingCompanyActionNeeded) {
	this.leasingCompanyActionNeeded = leasingCompanyActionNeeded;
	}

	public Integer getLeasingCompanyId() {
	return leasingCompanyId;
	}

	public void setLeasingCompanyId(Integer leasingCompanyId) {
	this.leasingCompanyId = leasingCompanyId;
	}

	public String getLeasingType() {
	return leasingType;
	}

	public void setLeasingType(String leasingType) {
	this.leasingType = leasingType;
	}

	public String getLinkedRegistrationId() {
	return linkedRegistrationId;
	}

	public void setLinkedRegistrationId(String linkedRegistrationId) {
	this.linkedRegistrationId = linkedRegistrationId;
	}

	public String getLinkedOrderId() {
		return linkedOrderId;
	}

	public void setLinkedOrderId(String linkedOrderId) {
		this.linkedOrderId = linkedOrderId;
	}

	public String getOrderType() {
	return orderType;
	}

	public void setOrderType(String orderType) {
	this.orderType = orderType;
	}

	public Boolean getPreviousVehicleDropoff() {
	return previousVehicleDropoff;
	}

	public void setPreviousVehicleDropoff(Boolean previousVehicleDropoff) {
	this.previousVehicleDropoff = previousVehicleDropoff;
	}

	public Long getProposedDeliveryDate() {
	return proposedDeliveryDate;
	}

	public void setProposedDeliveryDate(Long proposedDeliveryDate) {
	this.proposedDeliveryDate = proposedDeliveryDate;
	}

	public String getRegistrationDivToDoBy() {
	return registrationDivToDoBy;
	}

	public void setRegistrationDivToDoBy(String registrationDivToDoBy) {
	this.registrationDivToDoBy = registrationDivToDoBy;
	}

	public Boolean getSupplierActionNeeded() {
	return supplierActionNeeded;
	}

	public void setSupplierActionNeeded(Boolean supplierActionNeeded) {
	this.supplierActionNeeded = supplierActionNeeded;
	}

	public Integer getSupplierId() {
	return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
	this.supplierId = supplierId;
	}

	public TaxStampCosts getTaxStampCosts() {
	return taxStampCosts;
	}

	public void setTaxStampCosts(TaxStampCosts taxStampCosts) {
	this.taxStampCosts = taxStampCosts;
	}

	public TotalPrice getTotalPrice() {
	return totalPrice;
	}

	public void setTotalPrice(TotalPrice totalPrice) {
	this.totalPrice = totalPrice;
	}

	public Vehicle getVehicle() {
	return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
	this.vehicle = vehicle;
	}

	public Integer getVersion() {
	return version;
	}

	public void setVersion(Integer version) {
	this.version = version;
	}

	public WallBox getWallBox() {
		return wallBox;
	}

	public void setWallBox(WallBox wallBox) {
		this.wallBox = wallBox;
	}

	public Object getLeasingCompanyFollowUpDate() {
		return leasingCompanyFollowUpDate;
	}

	public void setLeasingCompanyFollowUpDate(Object leasingCompanyFollowUpDate) {
		this.leasingCompanyFollowUpDate = leasingCompanyFollowUpDate;
	}

	public Object getSupplierFollowUpDate() {
		return supplierFollowUpDate;
	}

	public void setSupplierFollowUpDate(Object supplierFollowUpDate) {
		this.supplierFollowUpDate = supplierFollowUpDate;
	}

	public String getDigitalDeliveryStatus() {
		return digitalDeliveryStatus;
	}

	public void setDigitalDeliveryStatus(String digitalDeliveryStatus) {
		this.digitalDeliveryStatus = digitalDeliveryStatus;
	}
}
