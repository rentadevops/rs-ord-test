package apiEngine.model.get.order.timeline;

public class Info {
	
	private Object newStatus;
	private Object attachmentFileName;
	private String comment;
	private Object agentId;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Info() {
	}

	/**
	*
	* @param agentId
	* @param attachmentFileName
	* @param newStatus
	* @param comment
	*/
	public Info(Object newStatus, Object attachmentFileName, String comment, Object agentId) {
	super();
	this.newStatus = newStatus;
	this.attachmentFileName = attachmentFileName;
	this.comment = comment;
	this.agentId = agentId;
	}

	public Object getNewStatus() {
	return newStatus;
	}

	public void setNewStatus(Object newStatus) {
	this.newStatus = newStatus;
	}

	public Object getAttachmentFileName() {
	return attachmentFileName;
	}

	public void setAttachmentFileName(Object attachmentFileName) {
	this.attachmentFileName = attachmentFileName;
	}

	public String getComment() {
	return comment;
	}

	public void setComment(String comment) {
	this.comment = comment;
	}

	public Object getAgentId() {
	return agentId;
	}

	public void setAgentId(Object agentId) {
	this.agentId = agentId;
	}

}
