package apiEngine.model.get.order.timeline;

public class Description {
    private Translations__1 translations;

    public Translations__1 getTranslations() {
        return translations;
    }

    public void setTranslations(Translations__1 translations) {
        this.translations = translations;
    }
}
