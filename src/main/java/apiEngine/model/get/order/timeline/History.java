package apiEngine.model.get.order.timeline;

public class History {
	
	private Integer version;
	private Integer id;
	private String date;
	private Integer userId;
	private String userFullName;
	private String type;
	private Info info;

	/**
	* No args constructor for use in serialization
	*
	*/
	public History() {
	}

	/**
	*
	* @param date
	* @param userFullName
	* @param id
	* @param type
	* @param version
	* @param userId
	* @param info
	*/
	public History(Integer version, Integer id, String date, Integer userId, String userFullName, String type, Info info) {
	super();
	this.version = version;
	this.id = id;
	this.date = date;
	this.userId = userId;
	this.userFullName = userFullName;
	this.type = type;
	this.info = info;
	}

	public Integer getVersion() {
	return version;
	}

	public void setVersion(Integer version) {
	this.version = version;
	}

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public String getDate() {
	return date;
	}

	public void setDate(String date) {
	this.date = date;
	}

	public Integer getUserId() {
	return userId;
	}

	public void setUserId(Integer userId) {
	this.userId = userId;
	}

	public String getUserFullName() {
	return userFullName;
	}

	public void setUserFullName(String userFullName) {
	this.userFullName = userFullName;
	}

	public String getType() {
	return type;
	}

	public void setType(String type) {
	this.type = type;
	}

	public Info getInfo() {
	return info;
	}

	public void setInfo(Info info) {
	this.info = info;
	}

}
