package apiEngine.model.get.order.timeline;

public class InscriptionCosts {
	
	private Integer value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public InscriptionCosts() {
	}

	/**
	*
	* @param value
	*/
	public InscriptionCosts(Integer value) {
	super();
	this.value = value;
	}

	public Integer getValue() {
	return value;
	}

	public void setValue(Integer value) {
	this.value = value;
	}

}
