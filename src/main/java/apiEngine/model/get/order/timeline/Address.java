package apiEngine.model.get.order.timeline;

public class Address {
	
	private String city;
	private String country;
	private String houseNumber;
	private String street;
	private ZipCode zipCode;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Address() {
	}

	/**
	*
	* @param country
	* @param zipCode
	* @param city
	* @param street
	* @param houseNumber
	*/
	public Address(String city, String country, String houseNumber, String street, ZipCode zipCode) {
	super();
	this.city = city;
	this.country = country;
	this.houseNumber = houseNumber;
	this.street = street;
	this.zipCode = zipCode;
	}

	public String getCity() {
	return city;
	}

	public void setCity(String city) {
	this.city = city;
	}

	public String getCountry() {
	return country;
	}

	public void setCountry(String country) {
	this.country = country;
	}

	public String getHouseNumber() {
	return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
	this.houseNumber = houseNumber;
	}

	public String getStreet() {
	return street;
	}

	public void setStreet(String street) {
	this.street = street;
	}

	public ZipCode getZipCode() {
	return zipCode;
	}

	public void setZipCode(ZipCode zipCode) {
	this.zipCode = zipCode;
	}

}
