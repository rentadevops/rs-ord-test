package apiEngine.model.get.order.timeline;

public class Interior {
	
	private Translations__2 translations;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Interior() {
	}

	/**
	*
	* @param translations
	*/
	public Interior(Translations__2 translations) {
	super();
	this.translations = translations;
	}

	public Translations__2 getTranslations() {
	return translations;
	}

	public void setTranslations(Translations__2 translations) {
	this.translations = translations;
	}

}
