package apiEngine.model.get.order.timeline;

public class Email {
	
	private String value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Email() {
	}

	/**
	*
	* @param value
	*/
	public Email(String value) {
	super();
	this.value = value;
	}

	public String getValue() {
	return value;
	}

	public void setValue(String value) {
	this.value = value;
	}

}
