package apiEngine.model.get.order.timeline;

public class InvoicingAddress {
	
	private String city;
	private String country;
	private String houseNumber;
	private String street;
	private ZipCode__1 zipCode;

	/**
	* No args constructor for use in serialization
	*
	*/
	public InvoicingAddress() {
	}

	/**
	*
	* @param country
	* @param zipCode
	* @param city
	* @param street
	* @param houseNumber
	*/
	public InvoicingAddress(String city, String country, String houseNumber, String street, ZipCode__1 zipCode) {
	super();
	this.city = city;
	this.country = country;
	this.houseNumber = houseNumber;
	this.street = street;
	this.zipCode = zipCode;
	}

	public String getCity() {
	return city;
	}

	public void setCity(String city) {
	this.city = city;
	}

	public String getCountry() {
	return country;
	}

	public void setCountry(String country) {
	this.country = country;
	}

	public String getHouseNumber() {
	return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
	this.houseNumber = houseNumber;
	}

	public String getStreet() {
	return street;
	}

	public void setStreet(String street) {
	this.street = street;
	}

	public ZipCode__1 getZipCode() {
	return zipCode;
	}

	public void setZipCode(ZipCode__1 zipCode) {
	this.zipCode = zipCode;
	}

}
