package apiEngine.model.get.order.timeline;

public class Translations__1 {
	
	private String de;
	private String en;
	private String fr;
	private String nl;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Translations__1() {
	}

	/**
	*
	* @param de
	* @param en
	* @param fr
	* @param nl
	*/
	public Translations__1(String de, String en, String fr, String nl) {
	super();
	this.de = de;
	this.en = en;
	this.fr = fr;
	this.nl = nl;
	}

	public String getDe() {
	return de;
	}

	public void setDe(String de) {
	this.de = de;
	}

	public String getEn() {
	return en;
	}

	public void setEn(String en) {
	this.en = en;
	}

	public String getFr() {
	return fr;
	}

	public void setFr(String fr) {
	this.fr = fr;
	}

	public String getNl() {
	return nl;
	}

	public void setNl(String nl) {
	this.nl = nl;
	}

}
