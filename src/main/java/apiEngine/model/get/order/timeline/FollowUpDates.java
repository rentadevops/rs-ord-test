package apiEngine.model.get.order.timeline;

public class FollowUpDates {
	
	private Long leasingCompanyFollowUpDate;
	private Long supplierFollowUpDate;

	/**
	* No args constructor for use in serialization
	*
	*/
	public FollowUpDates() {
	}

	/**
	*
	* @param supplierFollowUpDate
	* @param leasingCompanyFollowUpDate
	*/
	public FollowUpDates(Long leasingCompanyFollowUpDate, Long supplierFollowUpDate) {
	super();
	this.leasingCompanyFollowUpDate = leasingCompanyFollowUpDate;
	this.supplierFollowUpDate = supplierFollowUpDate;
	}

	public Long getLeasingCompanyFollowUpDate() {
	return leasingCompanyFollowUpDate;
	}

	public void setLeasingCompanyFollowUpDate(Long leasingCompanyFollowUpDate) {
	this.leasingCompanyFollowUpDate = leasingCompanyFollowUpDate;
	}

	public Long getSupplierFollowUpDate() {
	return supplierFollowUpDate;
	}

	public void setSupplierFollowUpDate(Long supplierFollowUpDate) {
	this.supplierFollowUpDate = supplierFollowUpDate;
	}

}
