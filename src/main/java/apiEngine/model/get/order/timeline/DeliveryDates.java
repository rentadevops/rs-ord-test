package apiEngine.model.get.order.timeline;

public class DeliveryDates {

	private Long desiredDeliveryDate;
	private Long expectedDeliveryDate;
	private Long actualDeliveryDate;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public DeliveryDates() {
	}

	/**
	 *
	 * @param desiredDeliveryDate
	 * @param actualDeliveryDate
	 * @param expectedDeliveryDate
	 */
	public DeliveryDates(Long desiredDeliveryDate, Long expectedDeliveryDate, Long actualDeliveryDate) {
		super();
		this.desiredDeliveryDate = desiredDeliveryDate;
		this.expectedDeliveryDate = expectedDeliveryDate;
		this.actualDeliveryDate = actualDeliveryDate;
	}

	public Long getDesiredDeliveryDate() {
		return desiredDeliveryDate;
	}

	public void setDesiredDeliveryDate(Long desiredDeliveryDate) {
		this.desiredDeliveryDate = desiredDeliveryDate;
	}

	public Long getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(Long expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public Long getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public void setActualDeliveryDate(Long actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}

}
