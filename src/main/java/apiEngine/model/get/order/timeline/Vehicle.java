package apiEngine.model.get.order.timeline;

public class Vehicle {
	
	private String brand;
	private Model model;
	private String vinNumber;
	private String vinControlNumber;
	private String identificationCode;
	private String licensePlate;
	private Boolean stockVehicle;
	private Integer mileage;
	private String languageDocuments;
	private String keyCode;
	private String startCode;
	private Object motorType;
	private Integer power;
	private Integer engineCapacity;
	private Double co2EmissionsNedc;
	private Double co2EmissionsWltp;
	private Color color;
	private Interior interior;
	private String tyreDescription;
	private Long firstRegistrationDate;
	private Integer constructionYear;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Vehicle() {
	}

	/**
	*
	* @param identificationCode
	* @param stockVehicle
	* @param firstRegistrationDate
	* @param startCode
	* @param co2EmissionsNedc
	* @param color
	* @param constructionYear
	* @param tyreDescription
	* @param co2EmissionsWltp
	* @param engineCapacity
	* @param interior
	* @param languageDocuments
	* @param keyCode
	* @param licensePlate
	* @param vinControlNumber
	* @param vinNumber
	* @param model
	* @param power
	* @param brand
	* @param mileage
	* @param motorType
	*/
	public Vehicle(String brand, Model model, String vinNumber, String vinControlNumber, String identificationCode, String licensePlate, Boolean stockVehicle, Integer mileage, String languageDocuments, String keyCode, String startCode, Object motorType, Integer power, Integer engineCapacity, Double co2EmissionsNedc, Double co2EmissionsWltp, Color color, Interior interior, String tyreDescription, Long firstRegistrationDate, Integer constructionYear) {
	super();
	this.brand = brand;
	this.model = model;
	this.vinNumber = vinNumber;
	this.vinControlNumber = vinControlNumber;
	this.identificationCode = identificationCode;
	this.licensePlate = licensePlate;
	this.stockVehicle = stockVehicle;
	this.mileage = mileage;
	this.languageDocuments = languageDocuments;
	this.keyCode = keyCode;
	this.startCode = startCode;
	this.motorType = motorType;
	this.power = power;
	this.engineCapacity = engineCapacity;
	this.co2EmissionsNedc = co2EmissionsNedc;
	this.co2EmissionsWltp = co2EmissionsWltp;
	this.color = color;
	this.interior = interior;
	this.tyreDescription = tyreDescription;
	this.firstRegistrationDate = firstRegistrationDate;
	this.constructionYear = constructionYear;
	}

	public String getBrand() {
	return brand;
	}

	public void setBrand(String brand) {
	this.brand = brand;
	}

	public Model getModel() {
	return model;
	}

	public void setModel(Model model) {
	this.model = model;
	}

	public String getVinNumber() {
	return vinNumber;
	}

	public void setVinNumber(String vinNumber) {
	this.vinNumber = vinNumber;
	}

	public String getVinControlNumber() {
	return vinControlNumber;
	}

	public void setVinControlNumber(String vinControlNumber) {
	this.vinControlNumber = vinControlNumber;
	}

	public String getIdentificationCode() {
	return identificationCode;
	}

	public void setIdentificationCode(String identificationCode) {
	this.identificationCode = identificationCode;
	}

	public String getLicensePlate() {
	return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
	this.licensePlate = licensePlate;
	}

	public Boolean getStockVehicle() {
	return stockVehicle;
	}

	public void setStockVehicle(Boolean stockVehicle) {
	this.stockVehicle = stockVehicle;
	}

	public Integer getMileage() {
	return mileage;
	}

	public void setMileage(Integer mileage) {
	this.mileage = mileage;
	}

	public String getLanguageDocuments() {
	return languageDocuments;
	}

	public void setLanguageDocuments(String languageDocuments) {
	this.languageDocuments = languageDocuments;
	}

	public String getKeyCode() {
	return keyCode;
	}

	public void setKeyCode(String keyCode) {
	this.keyCode = keyCode;
	}

	public String getStartCode() {
	return startCode;
	}

	public void setStartCode(String startCode) {
	this.startCode = startCode;
	}

	public Object getMotorType() {
	return motorType;
	}

	public void setMotorType(Object motorType) {
	this.motorType = motorType;
	}

	public Integer getPower() {
	return power;
	}

	public void setPower(Integer power) {
	this.power = power;
	}

	public Integer getEngineCapacity() {
	return engineCapacity;
	}

	public void setEngineCapacity(Integer engineCapacity) {
	this.engineCapacity = engineCapacity;
	}

	public Double getCo2EmissionsNedc() {
	return co2EmissionsNedc;
	}

	public void setCo2EmissionsNedc(Double co2EmissionsNedc) {
	this.co2EmissionsNedc = co2EmissionsNedc;
	}

	public Double getCo2EmissionsWltp() {
	return co2EmissionsWltp;
	}

	public void setCo2EmissionsWltp(Double co2EmissionsWltp) {
	this.co2EmissionsWltp = co2EmissionsWltp;
	}

	public Color getColor() {
	return color;
	}

	public void setColor(Color color) {
	this.color = color;
	}

	public Interior getInterior() {
	return interior;
	}

	public void setInterior(Interior interior) {
	this.interior = interior;
	}

	public String getTyreDescription() {
	return tyreDescription;
	}

	public void setTyreDescription(String tyreDescription) {
	this.tyreDescription = tyreDescription;
	}

	public Long getFirstRegistrationDate() {
	return firstRegistrationDate;
	}

	public void setFirstRegistrationDate(Long firstRegistrationDate) {
	this.firstRegistrationDate = firstRegistrationDate;
	}

	public Integer getConstructionYear() {
	return constructionYear;
	}

	public void setConstructionYear(Integer constructionYear) {
	this.constructionYear = constructionYear;
	}

}
