package apiEngine.model.get.order.timeline;

public class TotalPrice {
	
	private Double value;

	/**
	* No args constructor for use in serialization
	*
	*/
	public TotalPrice() {
	}

	/**
	*
	* @param value
	*/
	public TotalPrice(Double value) {
	super();
	this.value = value;
	}

	public Double getValue() {
	return value;
	}

	public void setValue(Double value) {
	this.value = value;
	}

}
