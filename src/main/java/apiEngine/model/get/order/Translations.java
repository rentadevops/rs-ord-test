package apiEngine.model.get.order;

public class Translations {
	
	public String de;
	public String en;
	public String fr;
	public String nl;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Translations() {
	}

	/**
	*
	* @param de
	* @param en
	* @param fr
	* @param nl
	*/
	public Translations(String de, String en, String fr, String nl) {
	super();
	this.de = de;
	this.en = en;
	this.fr = fr;
	this.nl = nl;
	}

	public String getDe() {
		return de;
	}

	public void setDe(String de) {
		this.de = de;
	}

	public String getEn() {
		return en;
	}

	public void setEn(String en) {
		this.en = en;
	}

	public String getFr() {
		return fr;
	}

	public void setFr(String fr) {
		this.fr = fr;
	}

	public String getNl() {
		return nl;
	}

	public void setNl(String nl) {
		this.nl = nl;
	}

}
