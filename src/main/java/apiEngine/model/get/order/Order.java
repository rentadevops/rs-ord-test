package apiEngine.model.get.order;

import apiEngine.model.get.order.detail.Agent;

public class Order {

	public Integer version;
	public String id;
	public String orderId;
	public String creationDate;
	public String status;
	public String brand;
	public Integer orderVersion;
	public Integer userId;
	public String leasingCompanyDossierNumber;
	public Object dealerDossierNumber;
	public Long orderDate;
	public String orderStatus;
	public String dealerName;
	public Integer dealerNumber;
	public Object agentName;
	public Object agentNumber;
	public String leasingCompanyName;
	public String deliveryContactName;
	public String supplierName;
	public Integer supplierNumber;
	public Integer leasingCompanyNumber;
	public String leasingCompanyCountry;
	public String clientName;
	public String driverName;
	public String vehicleMake;
	public VehicleModel vehicleModel;
	public Model model;
	public Long leasingCompanyDueDate;
	public Long dealerDueDate;
	public Boolean leasingCompanyHasUnseenMessages;
	public Boolean dealerHasUnseenMessages;
	public Long leasingCompanyRank;
	public Long dealerRank;
	public Object busyUntil;
	public Boolean alreadyOrdered;
	public String licensePlate;
	public Boolean leasingCompanyHasUnseenDocuments;
	public Boolean dealerHasUnseenDocuments;
	public String orderType;
	public Boolean modified;
	public Boolean leasingCompanyActionNeeded;
	public Boolean dealerActionNeeded;
	public Boolean beingProcessed;
	public Boolean supplierHasUnseenMessages;
	public Boolean supplierHasUnseenDocuments;
	public String dossierManager;
	public Agent agent;
	public String leasingCompanyFollowUpDate;
	public String supplierFollowUpDate;
	public Boolean supplierActionNeeded;
	public String expectedDeliveryDate;

	/**
	* No args constructor for use in serialization
	*
	*/
	public Order() {
	}

	/**
	*
	* @param busyUntil
	* @param orderType
	* @param agentName
	 * @param agentNumber
	* @param id
	 * @param orderId
	 * @param creationDate
	 * @param status
	 * @param brand
	* @param clientName
	* @param dealerDueDate
	* @param dealerDossierNumber
	* @param orderStatus
	* @param leasingCompanyRank
	* @param dealerRank
	* @param licensePlate
	* @param dealerHasUnseenDocuments
	* @param vehicleModel
	 * @param model
	* @param modified
	* @param alreadyOrdered
	* @param leasingCompanyDossierNumber
	* @param dealerName
	* @param leasingCompanyHasUnseenMessages
	* @param leasingCompanyActionNeeded
	* @param leasingCompanyHasUnseenDocuments
	* @param orderVersion
	* @param leasingCompanyName
	 * @param supplierName
	 * @param supplierNumber
	 * @param deliveryContactName
	* @param version
	* @param userId
	* @param dealerNumber
	* @param vehicleMake
	* @param leasingCompanyCountry
	* @param leasingCompanyNumber
	* @param leasingCompanyDueDate
	* @param dealerHasUnseenMessages
	* @param beingProcessed
	* @param dealerActionNeeded
	* @param driverName
	* @param orderDate
	 * @param supplierHasUnseenMessages
	 * @param supplierHasUnseenDocuments
	 * @param dossierManager
	 * @param agent
	 * @param leasingCompanyFollowUpDate
	 * @param supplierFollowUpDate
	 * @param supplierActionNeeded
	 * @param expectedDeliveryDate;
	*/
	public Order(Integer version, String id, String orderId, String creationDate, String status, String brand, Integer orderVersion, Integer userId, String leasingCompanyDossierNumber, Object dealerDossierNumber, Long orderDate, String orderStatus, String dealerName, Integer dealerNumber, Object agentName,Object agentNumber, String leasingCompanyName, String supplierName, Integer supplierNumber, String deliveryContactName, Integer leasingCompanyNumber, String leasingCompanyCountry, String clientName, String driverName, String vehicleMake, VehicleModel vehicleModel, Model model, Long leasingCompanyDueDate, Long dealerDueDate, Boolean leasingCompanyHasUnseenMessages, Boolean dealerHasUnseenMessages, Long leasingCompanyRank, Long dealerRank, Object busyUntil, Boolean alreadyOrdered, String licensePlate, Boolean leasingCompanyHasUnseenDocuments, Boolean dealerHasUnseenDocuments, String orderType, Boolean modified, Boolean leasingCompanyActionNeeded, Boolean dealerActionNeeded, Boolean beingProcessed, Boolean supplierHasUnseenMessages, Boolean supplierHasUnseenDocuments, String dossierManager, Agent agent, String leasingCompanyFollowUpDate, String supplierFollowUpDate, Boolean supplierActionNeeded, String expectedDeliveryDate) {
	super();
	this.version = version;
	this.id = id;
		this.orderId = orderId;
		this.creationDate = creationDate;
	this.orderVersion = orderVersion;
	this.userId = userId;
	this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
	this.dealerDossierNumber = dealerDossierNumber;
	this.orderDate = orderDate;
	this.status = status;
	this.brand = brand;
	this.orderStatus = orderStatus;
	this.dealerName = dealerName;
	this.dealerNumber = dealerNumber;
	this.agentName = agentName;
		this.agentNumber = agentNumber;
	this.leasingCompanyName = leasingCompanyName;
	this.leasingCompanyNumber = leasingCompanyNumber;
	this.leasingCompanyCountry = leasingCompanyCountry;
		this.supplierName = supplierName;
		this.supplierNumber = supplierNumber;
		this.deliveryContactName = deliveryContactName;
	this.clientName = clientName;
	this.driverName = driverName;
	this.vehicleMake = vehicleMake;
	this.vehicleModel = vehicleModel;
		this.model = model;
	this.leasingCompanyDueDate = leasingCompanyDueDate;
	this.dealerDueDate = dealerDueDate;
	this.leasingCompanyHasUnseenMessages = leasingCompanyHasUnseenMessages;
	this.dealerHasUnseenMessages = dealerHasUnseenMessages;
	this.leasingCompanyRank = leasingCompanyRank;
	this.dealerRank = dealerRank;
	this.busyUntil = busyUntil;
	this.alreadyOrdered = alreadyOrdered;
	this.licensePlate = licensePlate;
	this.leasingCompanyHasUnseenDocuments = leasingCompanyHasUnseenDocuments;
	this.dealerHasUnseenDocuments = dealerHasUnseenDocuments;
	this.orderType = orderType;
	this.modified = modified;
	this.leasingCompanyActionNeeded = leasingCompanyActionNeeded;
	this.dealerActionNeeded = dealerActionNeeded;
	this.beingProcessed = beingProcessed;
	this.supplierHasUnseenMessages = supplierHasUnseenMessages;
	this.supplierHasUnseenDocuments = supplierHasUnseenDocuments;
	this.dossierManager = dossierManager;
	this.agent = agent;
	this.leasingCompanyFollowUpDate = leasingCompanyFollowUpDate;
	this.supplierFollowUpDate = supplierFollowUpDate;
	this.supplierActionNeeded = supplierActionNeeded;
	this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getId() {
		return id;
	}

	public void setId(String Id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String OrderId) {
		this.orderId = orderId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String CreationDate) {
		this.creationDate = creationDate;
	}

	public Integer getOrderVersion() {
		return orderVersion;
	}

	public void setOrderVersion(Integer orderVersion) {
		this.orderVersion = orderVersion;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLeasingCompanyDossierNumber() {
		return leasingCompanyDossierNumber;
	}

	public void setLeasingCompanyDossierNumber(String leasingCompanyDossierNumber) {
		this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
	}

	public Object getDealerDossierNumber() {
		return dealerDossierNumber;
	}

	public void setDealerDossierNumber(Object dealerDossierNumber) {
		this.dealerDossierNumber = dealerDossierNumber;
	}

	public Long getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Long orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public Integer getDealerNumber() {
		return dealerNumber;
	}

	public void setDealerNumber(Integer dealerNumber) {
		this.dealerNumber = dealerNumber;
	}

	public Object getAgentName() {
		return agentName;
	}

	public void setAgentName(Object agentName) {
		this.agentName = agentName;
	}

	public Object getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(Object agentNumber) {
		this.agentNumber = agentNumber;
	}

	public String getLeasingCompanyName() {
		return leasingCompanyName;
	}

	public void setLeasingCompanyName(String leasingCompanyName) {
		this.leasingCompanyName = leasingCompanyName;
	}

	public Integer getLeasingCompanyNumber() {
		return leasingCompanyNumber;
	}

	public void setLeasingCompanyNumber(Integer leasingCompanyNumber) {
		this.leasingCompanyNumber = leasingCompanyNumber;
	}

	public String getLeasingCompanyCountry() {
		return leasingCompanyCountry;
	}

	public void setLeasingCompanyCountry(String leasingCompanyCountry) {
		this.leasingCompanyCountry = leasingCompanyCountry;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Integer getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(Integer supplierNumber) {
		this.leasingCompanyNumber = supplierNumber;
	}

	public String getDeliveryContactName() {
		return deliveryContactName;
	}

	public void setDeliveryContactName(String deliveryContactName) {
		this.deliveryContactName = deliveryContactName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public VehicleModel getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(VehicleModel vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model Model) {
		this.model = model;
	}

	public Long getLeasingCompanyDueDate() {
		return leasingCompanyDueDate;
	}

	public void setLeasingCompanyDueDate(Long leasingCompanyDueDate) {
		this.leasingCompanyDueDate = leasingCompanyDueDate;
	}

	public Long getDealerDueDate() {
		return dealerDueDate;
	}

	public void setDealerDueDate(Long dealerDueDate) {
		this.dealerDueDate = dealerDueDate;
	}

	public Boolean getLeasingCompanyHasUnseenMessages() {
		return leasingCompanyHasUnseenMessages;
	}

	public void setLeasingCompanyHasUnseenMessages(Boolean leasingCompanyHasUnseenMessages) {
		this.leasingCompanyHasUnseenMessages = leasingCompanyHasUnseenMessages;
	}

	public Boolean getDealerHasUnseenMessages() {
		return dealerHasUnseenMessages;
	}

	public void setDealerHasUnseenMessages(Boolean dealerHasUnseenMessages) {
		this.dealerHasUnseenMessages = dealerHasUnseenMessages;
	}

	public Long getLeasingCompanyRank() {
		return leasingCompanyRank;
	}

	public void setLeasingCompanyRank(Long leasingCompanyRank) {
		this.leasingCompanyRank = leasingCompanyRank;
	}

	public Long getDealerRank() {
		return dealerRank;
	}

	public void setDealerRank(Long dealerRank) {
		this.dealerRank = dealerRank;
	}

	public Object getBusyUntil() {
		return busyUntil;
	}

	public void setBusyUntil(Object busyUntil) {
		this.busyUntil = busyUntil;
	}

	public Boolean getAlreadyOrdered() {
		return alreadyOrdered;
	}

	public void setAlreadyOrdered(Boolean alreadyOrdered) {
		this.alreadyOrdered = alreadyOrdered;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Boolean getLeasingCompanyHasUnseenDocuments() {
		return leasingCompanyHasUnseenDocuments;
	}

	public void setLeasingCompanyHasUnseenDocuments(Boolean leasingCompanyHasUnseenDocuments) {
		this.leasingCompanyHasUnseenDocuments = leasingCompanyHasUnseenDocuments;
	}

	public Boolean getDealerHasUnseenDocuments() {
		return dealerHasUnseenDocuments;
	}

	public void setDealerHasUnseenDocuments(Boolean dealerHasUnseenDocuments) {
		this.dealerHasUnseenDocuments = dealerHasUnseenDocuments;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Boolean getModified() {
		return modified;
	}

	public void setModified(Boolean modified) {
		this.modified = modified;
	}

	public Boolean getLeasingCompanyActionNeeded() {
		return leasingCompanyActionNeeded;
	}

	public void setLeasingCompanyActionNeeded(Boolean leasingCompanyActionNeeded) {
		this.leasingCompanyActionNeeded = leasingCompanyActionNeeded;
	}

	public Boolean getDealerActionNeeded() {
		return dealerActionNeeded;
	}

	public void setDealerActionNeeded(Boolean dealerActionNeeded) {
		this.dealerActionNeeded = dealerActionNeeded;
	}

	public Boolean getBeingProcessed() {
		return beingProcessed;
	}

	public void setBeingProcessed(Boolean beingProcessed) {
		this.beingProcessed = beingProcessed;
	}

	public Boolean getSupplierHasUnseenMessages() {
		return supplierHasUnseenMessages;
	}

	public void setSupplierHasUnseenMessages(Boolean supplierHasUnseenMessages) {
		this.supplierHasUnseenMessages = supplierHasUnseenMessages;
	}

	public Boolean getSupplierHasUnseenDocuments() {
		return supplierHasUnseenDocuments;
	}

	public void setSupplierHasUnseenDocuments(Boolean supplierHasUnseenDocuments) {
		this.supplierHasUnseenDocuments = supplierHasUnseenDocuments;
	}

	public String getDossierManager() {
		return dossierManager;
	}

	public void setDossierManager(String dossierManager) {
		this.dossierManager = dossierManager;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public String getLeasingCompanyFollowUpDate() {
		return leasingCompanyFollowUpDate;
	}

	public void setLeasingCompanyFollowUpDate(String leasingCompanyFollowUpDate) {
		this.leasingCompanyFollowUpDate = leasingCompanyFollowUpDate;
	}

	public String getSupplierFollowUpDate() {
		return supplierFollowUpDate;
	}

	public void setSupplierFollowUpDate(String supplierFollowUpDate) {
		this.supplierFollowUpDate = supplierFollowUpDate;
	}

	public Boolean getSupplierActionNeeded() {
		return supplierActionNeeded;
	}

	public void setSupplierActionNeeded(Boolean supplierActionNeeded) {
		this.supplierActionNeeded = supplierActionNeeded;
	}

}
