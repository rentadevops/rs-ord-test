package apiEngine.model.put.registration;

public class Registration {
    private String registrationId;
    private String leasingCompanyOrderDossierNumber;
    private String registrarDossierNumber;
    private String formNumber;
    private Lessee lessee;
    private LicensePlateHolder licensePlateHolder;
    private Supplier supplier;
    private Insurance insurance;
    private Vehicle vehicle;
    private Delivery delivery;
    private Driver driver;

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getLeasingCompanyOrderDossierNumber() {
        return leasingCompanyOrderDossierNumber;
    }

    public void setLeasingCompanyOrderDossierNumber(String leasingCompanyOrderDossierNumber) {
        this.leasingCompanyOrderDossierNumber = leasingCompanyOrderDossierNumber;
    }

    public String getRegistrarDossierNumber() {
        return registrarDossierNumber;
    }

    public void setRegistrarDossierNumber(String registrarDossierNumber) {
        this.registrarDossierNumber = registrarDossierNumber;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public Lessee getLessee() {
        return lessee;
    }

    public void setLessee(Lessee lessee) {
        this.lessee = lessee;
    }

    public LicensePlateHolder getLicensePlateHolder() {
        return licensePlateHolder;
    }

    public void setLicensePlateHolder(LicensePlateHolder licensePlateHolder) {
        this.licensePlateHolder = licensePlateHolder;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
