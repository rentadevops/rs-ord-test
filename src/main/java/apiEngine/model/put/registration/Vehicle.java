package apiEngine.model.put.registration;

public class Vehicle {
    private String brand;
    private String model;
    private String vin;
    private String vinControlNumber;
    private Integer catalogueValue;
    private Integer catalogueValueWithoutDiscount;
    private Integer biv;
    private String costCentre;
    private Boolean usedVehicle;
    private String priorLicensePlate;
    private Long lastRegistrationDate;
    private String registrationDocumentsLanguage;
    private String emailGreenCard;
    private Boolean reuseLicensePlate;
    private String licensePlate;
    private String licensePlateFormat;
    private Boolean frontPlateOrdered;
    private Boolean speedPedelecFlag;
    private String pedalingAssistance;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVinControlNumber() {
        return vinControlNumber;
    }

    public void setVinControlNumber(String vinControlNumber) {
        this.vinControlNumber = vinControlNumber;
    }

    public Integer getCatalogueValue() {
        return catalogueValue;
    }

    public void setCatalogueValue(Integer catalogueValue) {
        this.catalogueValue = catalogueValue;
    }

    public Integer getCatalogueValueWithoutDiscount() {
        return catalogueValueWithoutDiscount;
    }

    public void setCatalogueValueWithoutDiscount(Integer catalogueValueWithoutDiscount) {
        this.catalogueValueWithoutDiscount = catalogueValueWithoutDiscount;
    }

    public Integer getBiv() {
        return biv;
    }

    public void setBiv(Integer biv) {
        this.biv = biv;
    }

    public String getCostCentre() {
        return costCentre;
    }

    public void setCostCentre(String costCentre) {
        this.costCentre = costCentre;
    }

    public Boolean getUsedVehicle() {
        return usedVehicle;
    }

    public void setUsedVehicle(Boolean usedVehicle) {
        this.usedVehicle = usedVehicle;
    }

    public String getPriorLicensePlate() {
        return priorLicensePlate;
    }

    public void setPriorLicensePlate(String priorLicensePlate) {
        this.priorLicensePlate = priorLicensePlate;
    }

    public Long getLastRegistrationDate() {
        return lastRegistrationDate;
    }

    public void setLastRegistrationDate(Long lastRegistrationDate) {
        this.lastRegistrationDate = lastRegistrationDate;
    }

    public String getRegistrationDocumentsLanguage() {
        return registrationDocumentsLanguage;
    }

    public void setRegistrationDocumentsLanguage(String registrationDocumentsLanguage) {
        this.registrationDocumentsLanguage = registrationDocumentsLanguage;
    }

    public String getEmailGreenCard() {
        return emailGreenCard;
    }

    public void setEmailGreenCard(String emailGreenCard) {
        this.emailGreenCard = emailGreenCard;
    }

    public Boolean getReuseLicensePlate() {
        return reuseLicensePlate;
    }

    public void setReuseLicensePlate(Boolean reuseLicensePlate) {
        this.reuseLicensePlate = reuseLicensePlate;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getLicensePlateFormat() {
        return licensePlateFormat;
    }

    public void setLicensePlateFormat(String licensePlateFormat) {
        this.licensePlateFormat = licensePlateFormat;
    }

    public Boolean getFrontPlateOrdered() {
        return frontPlateOrdered;
    }

    public void setFrontPlateOrdered(Boolean frontPlateOrdered) {
        this.frontPlateOrdered = frontPlateOrdered;
    }

    public Boolean getSpeedPedelecFlag() {
        return speedPedelecFlag;
    }

    public void setSpeedPedelecFlag(Boolean speedPedelecFlag) {
        this.speedPedelecFlag = speedPedelecFlag;
    }

    public String getPedalingAssistance() {
        return pedalingAssistance;
    }

    public void setPedalingAssistance(String pedalingAssistance) {
        this.pedalingAssistance = pedalingAssistance;
    }
}
