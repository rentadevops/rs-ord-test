package apiEngine.model.put.registration;

public class Insurance {
    private String insuranceCompanyFsmaNumber;
    private String insuranceBrokerFsmaNumber;
    private String referenceNumber;
    private Long beginDate;
    private Long plannedRegistrationDate;
    //private String insuranceCompanyName;

    public String getInsuranceCompanyFsmaNumber() {
        return insuranceCompanyFsmaNumber;
    }

    public void setInsuranceCompanyFsmaNumber(String insuranceCompanyFsmaNumber) {
        this.insuranceCompanyFsmaNumber = insuranceCompanyFsmaNumber;
    }

    public String getInsuranceBrokerFsmaNumber() {
        return insuranceBrokerFsmaNumber;
    }

    public void setInsuranceBrokerFsmaNumber(String insuranceBrokerFsmaNumber) {
        this.insuranceBrokerFsmaNumber = insuranceBrokerFsmaNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Long beginDate) {
        this.beginDate = beginDate;
    }

    public Long getPlannedRegistrationDate() {
        return plannedRegistrationDate;
    }

    public void setPlannedRegistrationDate(Long plannedRegistrationDate) {
        this.plannedRegistrationDate = plannedRegistrationDate;
    }

    //public String getInsuranceCompanyName() {
    //    return insuranceCompanyName;
    //}

    //public void setInsuranceCompanyName(String insuranceCompanyName) {
    //    this.insuranceCompanyName = insuranceCompanyName;
    //}
}
