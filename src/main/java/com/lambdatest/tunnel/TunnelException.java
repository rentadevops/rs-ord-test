package com.lambdatest.tunnel;

public class TunnelException extends Exception {

    TunnelException(String message) {
        super(message);
    }

}
