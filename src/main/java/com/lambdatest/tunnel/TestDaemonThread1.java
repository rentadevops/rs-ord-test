package com.lambdatest.tunnel;

import com.lambdatest.httpserver.HttpServer;

import java.io.IOException;

public class TestDaemonThread1 {
    public Integer port = null;
    HttpServer httpServer;

    public void setDaemon(Boolean bool) {
        if (Thread.currentThread().isDaemon()) {//checking for daemon thread
            httpServer = new HttpServer();
            try {
                port = HttpServer.findAvailablePort();
//                utils.logger(port.toString());
                System.out.println(port);
                int[] myIntArray = {port};
                httpServer.main(myIntArray);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("user thread work");
        }
    }

    public void start() {
        Thread.currentThread().start();

    }
}
