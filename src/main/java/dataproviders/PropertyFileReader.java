package dataproviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import enums.DriverType;
import enums.EnvironmentType;
import pageobjects.ui.ORD_Orders_Page;

public class PropertyFileReader {
	final Logger logger = LogManager.getLogger(PropertyFileReader.class);

	private Properties properties;
	private final String propertyFilePath = "src/test/resources/properties.properties";

	public PropertyFileReader() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				logger.error("Following exception was raised", e);
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("Configuration.properties not found at " + propertyFilePath, e);
		}
	}

	public String getProperty(String prop) throws FileNotFoundException, IOException {
		String property = properties.getProperty(prop);
		if (property != null)
			return property;
		else
			throw new RuntimeException("Property not specified in the properties.properties file.");
	}

	public String getPortalUrl() {
		String portal_url = properties.getProperty("portal_url");
		if (portal_url != null)
			return portal_url;
		else
			throw new RuntimeException("portal_url not specified in the properties.properties file.");
	}
	
	public String getDriverPath() {
		String driverPath = properties.getProperty("driverPath");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("driverPath not specified in the properties.properties file.");
	}
	
	public String getDriverPathFirefox() {
		String driverPath = properties.getProperty("driverPathFirefox");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("driverPath not specified in the properties.properties file.");
	}
	
	public String getRemoteDriverPath() {
		String driverPath = properties.getProperty("driverRemotePath");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("driverPath not specified in the properties.properties file.");
	}

	public long getImplicitlyWait() {
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if (implicitlyWait != null)
			return Long.parseLong(implicitlyWait);
		else
			throw new RuntimeException("implicitlyWait not specified in the properties.properties file.");
	}

	public String getApplicationUrl() {
		String url = properties.getProperty("url");
		if (url != null)
			return url;
		else
			throw new RuntimeException("url not specified in the properties.properties file.");
	}
	
	public String getMrtUrl() {
		String mrt_url = properties.getProperty("mrt_url");
		if (mrt_url != null)
			return mrt_url;
		else
			throw new RuntimeException("portal_url not specified in the properties.properties file.");
	}
	
	public String getLeasingCompany() {
		String leasing_company = properties.getProperty("leasing_company");
		if (leasing_company != null)
			return leasing_company;
		else
			throw new RuntimeException("leasing_company not specified in the properties.properties file.");
	}

	public String getLeasingCompanyWb() {
		String leasing_company = properties.getProperty("leasing_company_wb");
		if (leasing_company != null)
			return leasing_company;
		else
			throw new RuntimeException("leasing_company not specified in the properties.properties file.");
	}
	
	public String getLeasingCompanyLux() {
		String leasing_company = properties.getProperty("leasing_company_lux");
		if (leasing_company != null)
			return leasing_company;
		else
			throw new RuntimeException("leasing_company not specified in the properties.properties file.");
	}
	
	public String getDealer() {
		String dealer = properties.getProperty("dealer");
		if (dealer != null)
			return dealer;
		else
			throw new RuntimeException("dealer not specified in the properties.properties file.");
	}

	public String getDealer_wb() {
		String dealer = properties.getProperty("dealer_wb");
		if (dealer != null)
			return dealer;
		else
			throw new RuntimeException("dealer not specified in the properties.properties file.");
	}
	
	
	public DriverType getBrowser() { 
		String browserName = properties.getProperty("browser"); 
		if(browserName == null || browserName.equals("chrome")) return DriverType.CHROME; 
		else
			if(browserName.equalsIgnoreCase("firefox")) return DriverType.FIREFOX; 
			else
				if(browserName.equals("edge")) return DriverType.EDGE; 
				else
					if(browserName.equals("opera")) return DriverType.OPERA; 
					else
					 throw new RuntimeException("Browser Name Key value in Configuration.properties is not matched : " + browserName); 
	}
	  
	public EnvironmentType getEnvironment() { 
		String environmentName = properties.getProperty("environment"); 
		if(environmentName == null || environmentName.equalsIgnoreCase("local")) return EnvironmentType.LOCAL; 
		else
			if(environmentName.equals("remote")) return EnvironmentType.REMOTE; 
			else
				throw new RuntimeException("Environment Type Key value in Configuration.properties is not matched : "+ environmentName); 
	}
	 
		 
	public Boolean getBrowserWindowSize() {
		String windowSize = properties.getProperty("windowMaximize");
		if(windowSize != null) return Boolean.valueOf(windowSize);
		return true; 
	}
	
	
}
