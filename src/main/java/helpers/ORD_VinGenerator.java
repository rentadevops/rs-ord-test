package helpers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.Stream;

public class ORD_VinGenerator {
	public static final Random RANDOM = new Random();
    public static final String PREFIXES_FILE_NAME = "vin-prefixes.txt";
    public static final String ALLOWED_CHARS = "0123456789ABCDEFGHJKLMNPRSTUVWXYZ";
    public static final String ALLOWED_NUMS = "0123456789";

    public ORD_VinGenerator() {
        
    }

    /**
     * Generate random VIN
     *
     * @return randomly generated VIN
     */
    public String getRandomVin() {
        final Vin vinYear = getRandomVinStart();
        final StringBuilder vinBuilder = new StringBuilder();

        vinBuilder.append(vinYear.getWmi());
        vinBuilder.append(vinYear.getVds());
        vinBuilder.append(getRandomVinChar());
        vinBuilder.append(vinYear.getYear());

        for (int i = 0; i < 3; i++) {
            vinBuilder.append(getRandomVinChar());
        }
        
        for (int i = 0; i < 4; i++) {
            vinBuilder.append(getRandomVinNum());
        }

        final String vin = vinBuilder.toString();

        return vin;
    }

    public static char getRandomVinChar() {
        // '33' is length of ALLOWED_CHARS
        return ALLOWED_CHARS.charAt(RANDOM.nextInt(33));
    }
    
    public static char getRandomVinNum() {
        // '10' is length of ALLOWED_NUMS
        return ALLOWED_NUMS.charAt(RANDOM.nextInt(10));
    }

    public static Vin getRandomVinStart() {
        // '62178' is a number of lines in the file; + 1 to avoid reading of first line
        final int lineToRead = RANDOM.nextInt(62177) + 1;

        try (final Stream<String> stream = Files.lines(Paths.get(ClassLoader.getSystemResource(PREFIXES_FILE_NAME).toURI()))) {
            final String line = stream.skip(lineToRead)
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Problem occurred while read line " + lineToRead + " from " + PREFIXES_FILE_NAME));
            final String[] fields = line.split(" {3}");

            return new Vin(fields[0].trim(), fields[1].trim(), fields[2].trim());
        } catch (URISyntaxException | IOException | SecurityException e) {
            throw new RuntimeException("Problem occurred while reading " + PREFIXES_FILE_NAME, e);
        }
    }

    /**
     * Helper class used internally to save parts of VIN
     */
    public static class Vin {
        /**
         * WMI - (World Manufacturers Identification)
         */
        private final String wmi;

        /**
         * VDS (Vehicle Description Section)
         */
        private final String vds;

        /**
         * Year of model
         */
        private final String year;

        public Vin(final String wmi,
                   final String vds,
                   final String year) {
            this.wmi = wmi;
            this.vds = vds;
            this.year = year;
        }

        public String getWmi() {
            return this.wmi;
        }

        public String getVds() {
            return this.vds;
        }

        public String getYear() {
            return this.year;
        }
    }
}
