package helpers;

import java.time.Duration;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.ValidateJsonResponse;


public class ORD_Commands_Helper {
	final Logger logger = LogManager.getLogger(ORD_Commands_Helper.class);

	WebDriver driver;

	public ORD_Commands_Helper(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void select_sublease_from_list (WebElement subleaseListLocator, String subleaseVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", subleaseListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", subleaseListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_sublease(subleaseVariable)));
		mglh.get_input_sublease(subleaseVariable).click();
	}
	
	public void select_vehicle_type_from_list (WebElement vehicleTypeListLocator, String vehicleTypeVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", vehicleTypeListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", vehicleTypeListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_vehicle_type(vehicleTypeVariable)));
		mglh.get_input_vehicle_type(vehicleTypeVariable).click();
	}
	
	public void select_max_rent_class_from_list (WebElement maxRentClassListLocator, String maxRentClassVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", maxRentClassListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", maxRentClassListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_max_rent_class(maxRentClassVariable)));
		mglh.get_input_max_rent_class(maxRentClassVariable).click();
	}
	
	public void select_current_tyres_from_list (WebElement currentTyresListLocator, String currentTyresVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", currentTyresListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", currentTyresListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_current_tyres(currentTyresVariable)));
		mglh.get_input_current_tyres(currentTyresVariable).click();
	}
	
	public void select_brand_from_list (WebElement brandListLocator, String brandVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", brandListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", brandListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_brand(brandVariable)));
		mglh.get_input_brand(brandVariable).click();
	}
	
	public void select_model_from_list (WebElement modelListLocator, String modelVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		Integer seconden = 30;
		boolean succes = false;
		long endWaitTime = System.currentTimeMillis() + seconden*1000;
		//WebElement element = null;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", modelListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", modelListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_model(modelVariable)));
		while (System.currentTimeMillis() < endWaitTime && !succes) {
			if(mglh.get_input_model(modelVariable).isDisplayed()) {
				mglh.get_input_model(modelVariable).click();
				succes = true;
				break;
			}
			else {
				executor.executeScript("arguments[0].click();", modelListLocator);
			}
		}
		//mglh.get_input_model(modelVariable).click();
	}
	
	public void select_fuel_from_list (WebElement fuelListLocator, String fuelVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", fuelListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", fuelListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_fuel(fuelVariable)));
		mglh.get_input_fuel(fuelVariable).click();
	}
	
	public void select_activity_from_list (WebElement activityListLocator, String activityVariable) throws InterruptedException {
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		activityListLocator.click();
		Thread.sleep(2000);
		activityListLocator.sendKeys(activityVariable);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_select_activity(activityVariable)));	
		mglh.get_select_activity(activityVariable).click();
	}
	
	public void select_supplier_group_from_list (WebElement supplierGroupListLocator, String supplierGroupVariable) throws InterruptedException {
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		supplierGroupListLocator.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_supplier_group(supplierGroupVariable)));
		mglh.get_input_supplier_group(supplierGroupVariable).click();
	}
	
	public void select_activity_group_from_list (WebElement activityGroupListLocator, String activityGroupVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		if(System.getProperty("os.name").toLowerCase().equalsIgnoreCase("linux")) {
			Thread.sleep(1000);
			if (System.getProperty("BrowserName").equals("firefox")) {
				logger.debug("browser firefox, os linux");
				executor.executeScript("arguments[0].click();", activityGroupListLocator);
				Thread.sleep(500);
				executor.executeScript("arguments[0].click();", activityGroupListLocator);
			} else {
				logger.debug("browser chrome, os linux");
				executor.executeScript("arguments[0].click();", activityGroupListLocator);
				Thread.sleep(500);
				executor.executeScript("arguments[0].click();", activityGroupListLocator);
			}
			//activityGroupListLocator.click();
			Thread.sleep(1000);
		} else {
			if(System.getProperty("BrowserName").toLowerCase().equalsIgnoreCase("firefox")) {
				logger.debug("browser firefox, os windows");
				executor.executeScript("arguments[0].click();", activityGroupListLocator);
				//Thread.sleep(500);
				//executor.executeScript("arguments[0].click();", activityGroupListLocator);

			}
			System.out.println("browser chrome, os windows");
			executor.executeScript("arguments[0].click();", activityGroupListLocator);
			Thread.sleep(500);
			executor.executeScript("arguments[0].click();", activityGroupListLocator);

		}
		
		
		//executor.executeScript("arguments[0].click();", activityGroupListLocator);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_activity_group(activityGroupVariable)));	
		mglh.get_input_activity_group(activityGroupVariable).click();
	}
	
	public void select_task_from_list (WebElement taskListLocator, String taskVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", taskListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", taskListLocator);
		Thread.sleep(500);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_task(taskVariable)));
		mglh.get_input_task(taskVariable).click();
	}
	
	public void select_threshold_from_list (WebElement thresholdListLocator, String thresholdVariable) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
		executor.executeScript("arguments[0].click();", thresholdListLocator);
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", thresholdListLocator);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(mglh.get_input_priority_threshold(thresholdVariable)));
		mglh.get_input_priority_threshold(thresholdVariable).click();
	}
	
	public void get_into_view (String menuItem) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		ORD_Get_Locations_Helper mglh = new ORD_Get_Locations_Helper(driver);
		
		executor.executeScript("arguments[0].click();", mglh.get_menu_item(menuItem));
	}
	
	public void click_element_JS (WebElement webElement) throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		
			executor.executeScript("arguments[0].click();", webElement);

	}
	
	public String percentage_trimmer(String stringWithPercentage) {
	    if (stringWithPercentage != null && stringWithPercentage.length() > 0 && stringWithPercentage.charAt(stringWithPercentage.length() - 1) == '%') {
	    	stringWithPercentage = stringWithPercentage.substring(0, stringWithPercentage.length() - 1);
	    }
	    return stringWithPercentage;
	}
	
}

