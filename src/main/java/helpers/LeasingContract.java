package helpers;

public class LeasingContract 
{
    private String Contract_Status;
    private String License_Plate;
    private String Vehicle_Identification_Number;
    private String SubleaseCode;
    private String Vehicle_Type_Code;
    private String Vehicle_Brand;
    private String Vehicle_Model;
    private String Vehicle_Version;
    private String Fuel;
    private String Date_First_Registration;
    private String Date_Start_Of_Contract;
    private String Date_End_Of_Contract;
    private String Number_Of_Kilometers_End_Of_Contract;
    private String Maintenance_Allowed;
    //private String Activity_Code_Last_Maintenance;
    //private String Date_Last_Maintenance;
    private String Automatic_Approval_Allowed;
    private String Repair_Allowed;
    private String Contract_Number;
    private String Contract_Information;
    
    public LeasingContract(){
    }
     
    public LeasingContract(String Contract_Status, String License_Plate, String Vehicle_Identification_Number, String SubleaseCode, String Vehicle_Type_Code
    		, String Vehicle_Brand, String Vehicle_Model, String Vehicle_Version, String Fuel, String Date_First_Registration, String Date_Start_Of_Contract
    		, String Date_End_Of_Contract, String Number_Of_Kilometers_End_Of_Contract, String Maintenance_Allowed
    		, String Automatic_Approval_Allowed
    		, String Repair_Allowed, String Contract_Number, String Contract_Information) {
        super();
        this.Contract_Status = Contract_Status;
        this.License_Plate = License_Plate;
        this.Vehicle_Identification_Number = Vehicle_Identification_Number;
        this.SubleaseCode = SubleaseCode;
        this.Vehicle_Type_Code = Vehicle_Type_Code;
        this.Vehicle_Brand = Vehicle_Brand;
        this.Vehicle_Model = Vehicle_Model;
        this.Vehicle_Version = Vehicle_Version;
        this.Fuel = Fuel;
        this.Date_First_Registration = Date_First_Registration;
        this.Date_Start_Of_Contract = Date_Start_Of_Contract;
        this.Date_End_Of_Contract = Date_End_Of_Contract;
        this.Number_Of_Kilometers_End_Of_Contract = Number_Of_Kilometers_End_Of_Contract;
        this.Maintenance_Allowed = Maintenance_Allowed;
        //this.Activity_Code_Last_Maintenance = Activity_Code_Last_Maintenance;
        //this.Date_Last_Maintenance = Date_Last_Maintenance;
        this.Automatic_Approval_Allowed = Automatic_Approval_Allowed;
        this.Repair_Allowed = Repair_Allowed;
        this.Contract_Number = Contract_Number;
        this.Contract_Information = Contract_Information;
    }
     
    
 
    

	public String getContract_Status() {
		return Contract_Status;
	}

	public String getLicense_Plate() {
		return License_Plate;
	}

	public String getVehicle_Identification_Number() {
		return Vehicle_Identification_Number;
	}

	public String getSubleaseCode() {
		return SubleaseCode;
	}

	public String getVehicle_Type_Code() {
		return Vehicle_Type_Code;
	}

	public String getVehicle_Brand() {
		return Vehicle_Brand;
	}

	public String getVehicle_Model() {
		return Vehicle_Model;
	}

	public String getVehicle_Version() {
		return Vehicle_Version;
	}

	public String getFuel() {
		return Fuel;
	}

	public String getDate_First_Registration() {
		return Date_First_Registration;
	}

	public String getDate_Start_Of_Contract() {
		return Date_Start_Of_Contract;
	}

	public String getDate_End_Of_Contract() {
		return Date_End_Of_Contract;
	}

	public String getNumber_Of_Kilometers_End_Of_Contract() {
		return Number_Of_Kilometers_End_Of_Contract;
	}

	public String getMaintenance_Allowed() {
		return Maintenance_Allowed;
	}

	



	public String getAutomatic_Approval_Allowed() {
		return Automatic_Approval_Allowed;
	}

	public String getRepair_Allowed() {
		return Repair_Allowed;
	}

	public String getContract_Number() {
		return Contract_Number;
	}

	public String getContract_Information() {
		return Contract_Information;
	}

	@Override
    public String toString() {
        return "Leasing Contract [Contract_Status=" + Contract_Status + ", License_Plate="
                + License_Plate + ", Vehicle_Identification_Number=" + Vehicle_Identification_Number + ", SubleaseCode="
                + SubleaseCode + ", Vehicle_Type_Code=" + Vehicle_Type_Code  + ", Vehicle_Brand=" + Vehicle_Brand 
                + ", Vehicle_Model=" + Vehicle_Model + ", Vehicle_Version_Fuel=" + Vehicle_Version + ", Fuel=" + Fuel + ", Date_First_Registration=" + Date_First_Registration 
                + ", Date_Start_Of_Contract=" + Date_Start_Of_Contract + ", Date_End_Of_Contract=" + Date_End_Of_Contract + ", Number_Of_Kilometers_End_Of_Contract=" + Number_Of_Kilometers_End_Of_Contract 
                + ", Maintenance_Allowed=" + Maintenance_Allowed +  ", Automatic_Approval_Allowed=" + Automatic_Approval_Allowed 
                + ", Repair_Allowed=" + Repair_Allowed + ", Contract_Number=" + Contract_Number + ", Contract_Information=" + Contract_Information 
                + "]";
    }
    
    
}