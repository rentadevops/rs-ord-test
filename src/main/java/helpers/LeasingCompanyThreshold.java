package helpers;

public class LeasingCompanyThreshold 
{
    private String Mutation_Code;
    private String Activity_Code;
    private String Task_Code;
    private String Sublease_Code;
    private String Vehicle_Brand;
    private String Vehicle_Model;
    private String Date_First_Registration;
    private String Fuel_Code;
    private String Power;
    private String Cylinder_Content;
    private String Transmission;
    private String Threshold_Amount;

    
    public LeasingCompanyThreshold(){
    }
     
    public LeasingCompanyThreshold(String Mutation_Code, String Activity_Code, String Task_Code, String Sublease_Code, String Vehicle_Brand
    		, String Vehicle_Model, String Date_First_Registration, String Fuel_Code, String Power, String Cylinder_Content, String Transmission
    		, String Threshold_Amount) {
        super();
        this.Mutation_Code = Mutation_Code;
        this.Activity_Code = Activity_Code;
        this.Task_Code = Task_Code;
        this.Sublease_Code = Sublease_Code;
        this.Vehicle_Brand = Vehicle_Brand;
        this.Vehicle_Model = Vehicle_Model;
        this.Date_First_Registration = Date_First_Registration;
        this.Fuel_Code = Fuel_Code;
        this.Power = Power;
        this.Cylinder_Content = Cylinder_Content;
        this.Transmission = Transmission;
        this.Threshold_Amount = Threshold_Amount;
        
    }

	public String getMutation_Code() {
		return Mutation_Code;
	}

	public String getActivity_Code() {
		return Activity_Code;
	}

	public String getTask_Code() {
		return Task_Code;
	}

	public String getSublease_Code() {
		return Sublease_Code;
	}

	public String getVehicle_Brand() {
		return Vehicle_Brand;
	}

	public String getVehicle_Model() {
		return Vehicle_Model;
	}

	public String getDate_First_Registration() {
		return Date_First_Registration;
	}

	public String getFuel_Code() {
		return Fuel_Code;
	}

	public String getPower() {
		return Power;
	}

	public String getCylinder_Content() {
		return Cylinder_Content;
	}

	public String getTransmission() {
		return Transmission;
	}

	public String getThreshold_Amount() {
		return Threshold_Amount;
	}


	@Override
    public String toString() {
        return "Leasing Company Threshold [Mutation_Code=" + Mutation_Code + ", Activity_Code="
                + Activity_Code + ", Task_Code=" + Task_Code + ", Sublease_Code="
                + Sublease_Code + ", Vehicle_Brand=" + Vehicle_Brand  + ", Vehicle_Model=" + Vehicle_Model 
                + ", Date_First_Registration=" + Date_First_Registration + ", Fuel_Code=" + Fuel_Code + ", Power=" + Power + ", Cylinder_Content=" + Cylinder_Content 
                + ", Transmission=" + Transmission + ", Threshold_Amount=" + Threshold_Amount 
                + "]"; 
    }
    
    
}