package helpers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ORD_DatePicker_Helper {
	

	WebDriver driver;
	public ORD_DatePicker_Helper(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_year;
	
	public WebElement select_year(String year) {
		 return select_year = driver.findElement(By.xpath("//div[(text()='" + year + "')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_month;
	
	public WebElement select_month(String month) {
		System.out.println("odh select month: " + month);
		 return select_month = driver.findElement(By.xpath("//div[(text()='" + month + "')]"));
	}

	@FindBy(how = How.XPATH, using = "")
	private WebElement select_month_fr;

	public WebElement select_month_fr(String month) {
		return select_month = driver.findElement(By.xpath("//div[(text()='" + month + "')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_day;
	
	public WebElement select_day(String day) {
		 return select_day = driver.findElement(By.xpath("//div[contains(@class, 'mat-calendar-body-cell')][(text()='" + day + "')]"));
	}
	
	public String pick_month_firstRegistrationDate(String date) throws InterruptedException {
		String splitter[] = date.split("-");
		String month = splitter[0];
		return month;
	}
	
	public String pick_year_firstRegistrationDate(String date) throws InterruptedException {
		String splitter[] = date.split("-");
		String year = splitter[1];
		return " " + year + " ";
	}
	
	
	
	public String pick_date_first_registration_year(Integer minusMonths) throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now().minusMonths(minusMonths);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("/");
		String year = splitter[2];
		return " " + year + " ";
	}

	public String pick_date_wanted_registration(Integer plusDays) throws InterruptedException {

		LocalDateTime ldt = LocalDateTime.now().plusDays(plusDays);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("/");
		String day = splitter[0];
		return " " + day + " ";
	}
	
	
		public String pick_date_first_registration_month(Integer minusMonths) throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now().minusMonths(minusMonths);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("/");
		String month = splitter[1];
		if(System.getProperty("language").equals("fr")) {
			month = transform_month_fr(month);
		} else {
			month = transform_month(month);
		}

		return month;
	}
	
	public String pick_start_date_year() throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("-");
		String year = splitter[2];
		return " " + year + " ";
	}
	
public String pick_start_date_month() throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("-");
		String month = splitter[1];
		month = translate_month_upper(month);
		return month;
	}
	
	public String pick_end_date_year() throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now().plusYears(5);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("-");
		String year = splitter[2];
		return " " + year + " ";
	}
	
	public String pick_end_date_month() throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now().plusYears(5);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("-");
		String month = splitter[1];
		month = translate_month_upper(month);
		return month;
	}
	
public String pick_date_next_inspection_year() throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now().plusMonths(6);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("-");
		String year = splitter[2];
		return " " + year + " ";
	}
	
	public String pick_date_next_inspection_month() throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now().plusMonths(6);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("-");
		String month = splitter[1];
		month = translate_month_upper(month);
		return month;
	}
	
	public String pick_day() throws InterruptedException {
		
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
		String formatter = format.format(ldt);
		String splitter[] = formatter.split("-");
		String day = splitter[0]; 
		day = StringUtils.stripStart(day,"0");
		//if(day.equals("31")) {
		//	day = "30";
		//}
		return " " + day + " ";
	}
	
	public String transform_month(String month) {
		
		switch(month) {
		case "01":
			month = " JAN ";
			break;
		case "02":
			month = " FEB ";
			break;
		case "03":
			month = " MAR ";
			break;
		case "04":
			month = " APR ";
			break;
		case "05":
			month = " MAY ";
			break;
		case "06":
			month = " JUN ";
			break;
		case "07":
			month = " JUL ";
			break;
		case "08":
			month = " AUG ";
			break;
		case "09":
			month = " SEP ";
			break;
		case "10":
			month = " OCT ";
			break;
		case "11":
			month = " NOV ";
			break;
		case "12":
			month = " DEC ";
			break;
	}
		
	return month;

	}

	public String transform_month_fr(String month) {

		switch(month) {
			case "01":
				month = " JANV. ";
				break;
			case "02":
				month = " FÉVR. ";
				break;
			case "03":
				month = " MARS ";
				break;
			case "04":
				month = " AVR. ";
				break;
			case "05":
				month = " MAI ";
				break;
			case "06":
				month = " JUIN ";
				break;
			case "07":
				month = " JUIL. ";
				break;
			case "08":
				month = " AOÛT ";
				break;
			case "09":
				month = " SEPT. ";
				break;
			case "10":
				month = " OCT. ";
				break;
			case "11":
				month = " NOV. ";
				break;
			case "12":
				month = " DÉC. ";
				break;
		}

		return month;

	}
	
	public String translate_month(String month) {
	
		switch(month) {
		case "Jan":
			month = " JAN. ";
			break;
		case "JAN":
			month = " JAN ";
			break;
		case "Feb":
			month = " FEB. ";
			break;
		case "FEB":
			month = " FEB ";
			break;
		case "Mar":
			month = " MRT. ";
			break;
		case "MAR":
			month = " MAR ";
			break;
		case "Apr":
			month = " APR. ";
			break;
		case "APR":
			month = " APR ";
			break;
		case "May":
			month = " MEI ";
			break;
		case "MAY":
			month = " MAY ";
			break;
		case "Jun":
			month = " JUN. ";
			break;
		case "JUN":
			month = " JUN ";
			break;
		case "Jul":
			month = " JUL. ";
			break;
		case "JUL":
			month = " JUL ";
			break;
		case "Aug":
			month = " AUG. ";
			break;
		case "AUG":
			month = " AUG ";
			break;
		case "Sep":
			month = " SEP. ";
			break;
		case "SEP":
			month = " SEP ";
			break;
		case "Okt":
			month = " OKT. ";
			break;
		case "Oct":
			month = " OKT. ";
			break;
		case "OCT":
			month = " OCT ";
			break;
		case "Nov":
			month = " NOV. ";
			break;
		case "NOV":
			month = " NOV ";
			break;
		case "Dec":
			month = " DEC. ";
			break;
		case "DEC":
			month = " DEC ";
			break;
	}
		
	return month;

	}
	
	public String translate_month_upper(String month) {
		
		switch(month) {
		case "Jan":
			month = " JAN ";
			break;
		case "Feb":
			month = " FEB ";
			break;
		case "Mar":
			month = " MAR ";
			break;
		case "Apr":
			month = " APR ";
			break;
		case "May":
			month = " MAY ";
			break;
		case "Jun":
			month = " JUN ";
			break;
		case "Jul":
			month = " JUL ";
			break;
		case "Aug":
			month = " AUG ";
			break;
		case "Sep":
			month = " SEP ";
			break;
		case "Oct":
			month = " OCT ";
			break;
		case "Nov":
			month = " NOV ";
			break;
		case "Dec":
			month = " DEC ";
			break;
	}
		
	return month;

	}
	
	
}
