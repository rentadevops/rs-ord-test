package helpers;

import java.util.Random;

public class ORD_LicensplateNumberGenerator {
	private static final Random RANDOM = new Random();
	private static final String ALLOWED_CHARS = "ABCDEFGHJKLMNPRSTUVWXYZ";
	private static final String ALLOWED_NUMS = "123456789";
	private static final String[] NOT_ALLOWED_COMBINATIONS = { "AAP", "AAS", "AEL", "ALA", "ANE", "ASS", "BEB", "BIT",
			"BOM", "BOY", "BSP", "BUB", "BWP", "BYT", "CAP", "CDF", "CDH", "CDV", "CON", "CSP", "CUB", "CUL", "CUT",
			"CVP", "DCD", "DIK", "DOM", "FDF", "FOK", "FOL", "FOU", "FUC", "FUK", "GAT", "GAY", "GEK", "GOD", "HIV",
			"HOL", "JEK", "KAK", "KKQ", "KUL", "KUT", "LAF", "LDD", "LSP", "LUL", "MAS", "MCC", "MDP", "MOR", "MOU",
			"MST", "NIC", "NIK", "NIQ", "NVA", "PDB", "PDO", "PET", "PFF", "PIK", "PIN", "PIP", "PIS", "PJU", "PKK",
			"POT", "PRL", "PSB", "PSC", "PSL", "PTB", "PUE", "PUT", "PVV", "PYK", "PYN", "PYP", "PYS", "ROM", "SEX",
			"SOA", "SOT", "SPA", "SUL", "TAK", "TET", "TIT", "TUE", "VCD", "VIH", "VLD", "VMO", "VNV", "ZAC", "zak",
			"ZOT" };
	String NOT_ALLOWED;

	public ORD_LicensplateNumberGenerator() {

	}

	/**
	 * Generate random LicenseNumber
	 *
	 * @return randomly generated LicenseNumber
	 */
	public String getRandomLicenseplateNumber() {

		final StringBuilder licBuilder = new StringBuilder();

		for (int i = 0; i < 1; i++) {
			licBuilder.append(getRandomLicNum());
		}

		licBuilder.append("-");

		Boolean magniet = false;
		String substr = null;
		for (int j = 0; j < NOT_ALLOWED_COMBINATIONS.length && !magniet; j++) {
			substr = "";
			for (int i = 0; i < 3; i++) {
				substr = substr + getRandomLicChar();
				// licBuilder.append(getRandomLicNum());
			}
			if (substr.equals(NOT_ALLOWED_COMBINATIONS[j])) {
				magniet = false;
			} else {
				licBuilder.append(substr);
				break;
			}
		}
		licBuilder.append("-");

		
			for (int i = 0; i < 3; i++) {
				licBuilder.append(getRandomLicNum());
			}

		final String licenseNumber = licBuilder.toString();

		return licenseNumber;
	}

	private static char getRandomLicChar() {
		// '33' is length of ALLOWED_CHARS
		return ALLOWED_CHARS.charAt(RANDOM.nextInt(23));
	}

	private static char getRandomLicNum() {
		// '33' is length of ALLOWED_CHARS
		return ALLOWED_NUMS.charAt(RANDOM.nextInt(9));
	}

}
