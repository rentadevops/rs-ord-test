package helpers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ORD_Get_Locations_Helper {
	final Logger logger = LogManager.getLogger(ORD_Get_Locations_Helper.class);

	WebDriver driver;

	public ORD_Get_Locations_Helper(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "")
	private WebElement select_menu_item;
	
	public WebElement get_menu_item(String menuItem) {
		 driver.findElement(By.xpath("//a[contains(text(), '" + menuItem + "')]"));
		 return select_menu_item;
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement edit_vehicle_type;

	public WebElement get_edit_vehicle_type(String vehicle_type) {
		return edit_vehicle_type = driver.findElement(By.xpath("//span[contains(.,'" + vehicle_type + "')]/parent::mat-option"));
	}

	@FindBy(how = How.XPATH, using = "")
	private WebElement select_fuel;

	public WebElement get_input_fuel(String fuel) {
		return select_fuel = driver.findElement(By.xpath("//span[contains(.,'" + fuel + "')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_brand;

	public WebElement get_input_brand(String brand) {
		return select_brand = driver.findElement(By.xpath("//span[contains(.,'" + brand + "')]"));
	}

	@FindBy(how = How.XPATH, using = "")
	private WebElement select_sublease_type;

	public WebElement get_input_sublease_type(String sublease_type) {
		return select_sublease_type = driver.findElement(By.xpath("//span[contains(.,'" + sublease_type + "')]/parent::mat-option"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_activity_group;

	public WebElement get_input_activity_group(String activity_group) {
		return select_activity_group = driver.findElement(By.xpath("//span[contains(.,'" + activity_group + " -" + "')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_supplier_group;

	public WebElement get_input_supplier_group(String supplier_group) {
		return select_supplier_group = driver.findElement(By.xpath("//span[contains(.,'" + supplier_group + "')]"));
	}

	@FindBy(how = How.XPATH, using = "")
	private WebElement select_max_rent_class;

	public WebElement get_input_max_rent_class(String max_rent_class) {
		return select_max_rent_class = driver.findElement(By.xpath("//mat-option/span[contains(.,'"+max_rent_class+"')]"));
	}

	@FindBy(how = How.XPATH, using = "")
	private WebElement select_current_tyres;

	public WebElement get_input_current_tyres(String current_tyres) {
		return select_current_tyres = driver.findElement(By.xpath("//span[text()=" + "' "+current_tyres+" ']"));
	}
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='numberOfKilometersEndOfContract']")
	public WebElement input_number_of_kilometers_end_of_contract;
	
	//public WebElement input_number_of_kilometers_end_of_contract() {
	//	return input_number_of_kilometers_end_of_contract = driver.findElement(By.xpath("//input[@formcontrolname='numberOfKilometersEndOfContract']"));
	//}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_sublease;

	public WebElement get_input_sublease(String sublease) {
		return select_sublease = driver.findElement(By.xpath("//mat-option/span[contains(.,'"+sublease+"')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_vehicle_type;

	public WebElement get_input_vehicle_type(String vehicleType) {
		return select_vehicle_type = driver.findElement(By.xpath("//mat-option/span[contains(.,'"+vehicleType+"')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_priority_threshold;

	public WebElement get_input_priority_threshold(String priority_threshold) {
		return select_priority_threshold = driver.findElement(By.xpath("//span[contains(.,' "+priority_threshold+" ')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_rsnumber;

	public WebElement get_input_rsnumber(String rsNumber) {
		return select_rsnumber = driver.findElement(By.xpath("//span[contains(.,' "+rsNumber+" ')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_task;

	public WebElement get_input_task(String task) {
		return select_task = driver.findElement(By.xpath("//span[contains(.,' "+task+" ')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_activity;

	public WebElement get_select_activity(String activity) {
		return select_activity = driver.findElement(By.xpath("//span[contains(.,' "+activity+" ')]"));
	}
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement select_model;

	public WebElement get_input_model(String model) {
		return select_model = driver.findElement(By.xpath("//span[contains(.,' "+model+" ')]"));
	}
	
	
	
	public WebElement getElementWithIndex(String str) {

		WebElement element = null;

		List<WebElement> elements = driver.findElements(By.xpath("//span[contains(.,'" + str + "')]"));
		logger.debug("elements size: " + elements.size());
		if (elements.size() > 0) {
			for (int i = elements.size(); i > 0; i--) {
				logger.debug("elementText: " + driver.findElement(By.xpath("(//span[contains(.,'" + str + "')])[" + i + "]")).getText().toString());
				if (driver.findElement(By.xpath("(//span[contains(.,'" + str + "')])[" + i + "]")).getText().toString()
						.contains(str)) {
					element = driver.findElement(By.xpath("(//span[contains(.,'" + str + "')])[" + i + "]"));
					break;
				}
			}
		}

		return element;
	}
	
	public WebElement getElementWithIndex(String str, WebElement we) throws InterruptedException {
		Integer seconden = 30;
		boolean succes = false;
		long endWaitTime = System.currentTimeMillis() + seconden*1000;
		WebElement element = null;
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		while (System.currentTimeMillis() < endWaitTime && !succes) {
			//TODO if browser is firefox, click without javascript
			if (System.getProperty("BrowserName").equals("firefox")) {
				jse.executeScript("arguments[0].click();", we);
				Thread.sleep(1000);
				jse.executeScript("arguments[0].click();", we);
			}
			else {
				jse.executeScript("arguments[0].click();", we);
			}
			
		List<WebElement> elements = driver.findElements(By.xpath("//span[contains(.,'" + str + "')]"));
		logger.debug("String: " + str + " list size: " + elements.size());
		if (elements.size() > 0) {
			for (int i = elements.size(); i > 0; i--) {
				if(i>1) {
					i=1+1;
				}
				if (driver.findElement(By.xpath("(//span[contains(.,'" + str + "')])[" + i + "]")).getText()
						.contains(str)) {
					element = driver.findElement(By.xpath("(//span[contains(.,'" + str + "')])[" + i + "]"));
					//logger.debug("xpath: " + element.toString());
					succes = true;
					break;
				}
			}
		}
		Thread.sleep(2000);
		}
		return element;
	}
	
}
