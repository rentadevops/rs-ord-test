package helpers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.Stream;

public class ORD_ContractNumberGenerator {
	private static final Random RANDOM = new Random();
    private static final String ALLOWED_CHARS = "ABCDEFGHJKLMNPRSTUVWXYZ";
    private static final String ALLOWED_NUMS = "0123456789";

    public ORD_ContractNumberGenerator() {
        
    }

    /**
     * Generate random VIN
     *
     * @return randomly generated VIN
     */
    public String getRandomContractNumber() {
        final StringBuilder contractBuilder = new StringBuilder();

        for (int i = 0; i < 7; i++) {
            contractBuilder.append(getRandomVinChar());
        }
        
        for (int i = 0; i < 10; i++) {
            contractBuilder.append(getRandomVinNum());
        }

        final String contractnumber = contractBuilder.toString();

        return contractnumber;
    }

    private static char getRandomVinChar() {
        // '23' is length of ALLOWED_CHARS
        return ALLOWED_CHARS.charAt(RANDOM.nextInt(23));
    }
    
    private static char getRandomVinNum() {
        // '10' is length of ALLOWED_NUMS
        return ALLOWED_NUMS.charAt(RANDOM.nextInt(10));
    }


}
