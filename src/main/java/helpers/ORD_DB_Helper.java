 package helpers;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import org.apache.logging.log4j.LogManager;

import com.google.common.collect.ImmutableList;



public class ORD_DB_Helper {
	final Logger logger = LogManager.getLogger(ORD_DB_Helper.class);

	static String JDBC_DRIVER = "com.mysql.jdbc.driver";
	private String DB_URL = "jdbc:mysql://172.29.121.160:3306/s4t_ord";

	// Database credentials
	private String USER = "antonissea";
	private String PASS = "!FN88je63lg";

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs;
	ArrayList <int[]> result = new ArrayList<int[]>();
	
	public ORD_DB_Helper() {

	}

	public ORD_DB_Helper(String DB_URL, String USER, String PASS) {
		this.DB_URL = DB_URL;
		this.USER = USER;
		this.PASS = PASS;
	}

	public Connection open() {
	        try {
	            //logger.debug("Connecting to database...");
	            conn = DriverManager.getConnection(DB_URL, USER,PASS);
	            //logger.debug("Creating statement...");
	            stmt = conn.createStatement();
	        } catch (SQLException ex) {
	            logger.error("SQLException", ex);
	        }
			return conn;
	    }
	    
	    public void close() {
	        try {
	            stmt.close();
	            conn.close();
	        } catch (SQLException ex) {
	        	logger.error("SQLException", ex);
	        }
	    }
	    
	    public List<List<Object>> query(String query) throws SQLException {
	        //try {
	            String sql;
	            sql = query;
	            ResultSet rs = stmt.executeQuery(sql); // DML
	            
	            //logger.debug("readrows: " + readRows(rs));
	            
	            //List<List<Object>> lst = readRows(rs);

	        //} catch (SQLException ex) {
	            //Logger.getLogger(FMS_DB_Helper.class.getName()).log(Level.SEVERE, null, ex);
	        //}
			return readRows(rs);
	       

	    }
	    
	    private static List<List<Object>> readRows(ResultSet rs) throws SQLException
	    {
	        ImmutableList.Builder<List<Object>> rows = ImmutableList.builder();
	        int columnCount = rs.getMetaData().getColumnCount();
	        while (rs.next()) {
	            List<Object> row = new ArrayList<>();
	            for (int i = 1; i <= columnCount; i++) {
	                row.add(rs.getObject(i));
	            }
	            rows.add(row);
	        }
	        return rows.build();
	    }
}
