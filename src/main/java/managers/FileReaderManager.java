package managers;

//import dataproviders.ConfigFileReader;
//import dataproviders.PropertyFileReader;

import dataproviders.ConfigFileReader;
import dataproviders.PropertyFileReader;

public class FileReaderManager {

	private static FileReaderManager fileReaderManager = new FileReaderManager();
	 private static ConfigFileReader configFileReader;
	 private static PropertyFileReader propertyFileReader;
	 
	 public FileReaderManager() {
	 }
	 
	 public static FileReaderManager getInstance( ) {
	       return fileReaderManager;
	 }
	
	 public PropertyFileReader getPropertyFileReader() {
		 return (propertyFileReader == null) ? new PropertyFileReader() : propertyFileReader;
		 }
	 
	 public ConfigFileReader getConfigFileReader() {
		 return (configFileReader == null) ? new ConfigFileReader() : configFileReader;
		 }
}