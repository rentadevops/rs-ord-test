package managers;

import java.io.IOException;
import apiEngine.model.get.order.notifications.wallbox.WallBoxOrderNotification;
import apiEngine.model.get.order.notifications.wallbox.WallBoxOrderNotifications;
import cucumber.TestContext;
import objects.database.DB_Query;
import org.openqa.selenium.WebDriver;

import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.order.OrderList;
import apiEngine.model.get.order.detail.OrderDetail;
import apiEngine.model.get.order.notifications.feed.OrderNotificationFeed;
import apiEngine.model.get.order.notifications.feed.OrderNotificationsFeed;
import apiEngine.model.get.order.timeline.OrderTimeline;
import apiEngine.model.get.sftp.order.notification.OrderNotifications;
import apiEngine.model.get.supplier.actor.SupplierActor;
import pageobjects.api.*;
import pageobjects.ftp.*;
import pageobjects.ui.*;

public class PageObjectManager {
	
	private Portal_Page portalRSPage;
	
	private final WebDriver driver;
	//ZIE MRT-NEW PageObjectManager voor voorbeeld
	private ORD_Topbar topBar;
	private ORD_Registration_Page registrationPage;
	private ORD_Orders_Page ordersPage;
	private DB_Query dbQuery;
	private ORD_Order_Page orderPage;
	private ORD_Order_Details_Page detailsPage;
	private Validate_Pdf validate_Pdf;
	private ORD_Wallbox_Order_Details_Page wallboxDetailsPage;
	private API_GetOrderByDossierNumber getOrderByDossierNumber;
	private API_GetOrderDetailById getOrderDetailById;
	private API_GetLeasingCompanyActorById getLeasingCompanyActorById;
	private API_GetSupplierActorById getSupplierActorById;
	private API_GetOrderTimelineById getOrderTimelineById;
	private API_GetOrderNotifications getOrderNotificationsFeed;
	private API_GetWallBoxOrderNotifications getWallBoxOrderNotifications;
	private FTP_PostOrderImportsRequest postOrderImportsRequest;
	private FTP_PostUpdateOrderImportsRequest postUpdateOrderImportsRequest;
	private FTP_PostOrderImportsMigrate postOrderImportsMigrate;
	private FTP_PostWallBoxImports postWallBoxImports;
	private API_PostWallBoxOrderAccepted postWallBoxOrderAccepted;
	private API_PostWallBoxOrderInstalled postWallBoxOrderInstalled;
	private API_PostWallBoxOrderReadyToActivate postWallBoxOrderReadyToActivate;
	private API_PostWallBoxOrderActivated postWallBoxOrderActivated;
	private FTP_PostOrderImportsNotification postOrderImportsNotification;
	private FTP_RegistrationImports registrationImports;
	private FTP_GetOrderNotifications getOrderNotifications;
	private FTP_GetWallBoxOrderNotifications getWallBoxOrderNotificationsFtp;
	private API_PutRegistration putRegistration;
	private API_PostBrokerRegistration postBrokerRegistration;
	private API_GetRegistration getRegistration;
	private API_GetBrokerRegistration getBrokerRegistration;
	OrderList order;
	OrderDetail orderDetail;
	LeasingCompanyActor leasingCompanyActor;
	SupplierActor supplierActor;
	OrderTimeline orderTimeline;
	OrderNotificationsFeed orderNotificationsFeed;
	OrderNotificationFeed orderNotificationFeed;
	OrderNotifications orderNotifications;
	WallBoxOrderNotifications wallBoxOrderNotifications;
	WallBoxOrderNotification wallBoxOrderNotification;
	ORD_SetSystemProperties_Page setSystemProperties;
	apiEngine.model.get.sftp.wallbox.order.notification.WallBoxOrderNotifications wallBoxOrderNotificationsFtp;
	apiEngine.model.get.sftp.wallbox.order.notification.WallBoxOrderNotification wallBoxOrderNotificationFtp;
	TestContext testContext;
	
	
	public PageObjectManager(WebDriver driver) {
		 
		 this.driver = driver;
		 }
	
	public Portal_Page getPortalRS_Page(){
		 return (portalRSPage == null) ? portalRSPage = new Portal_Page(driver) : portalRSPage;
		 }
	
	public ORD_Topbar getORD_topbar(){
		 return (topBar == null) ? topBar = new ORD_Topbar(driver) : topBar;
		 }

	public ORD_SetSystemProperties_Page setORD_SetSystemProperties_Page() throws InterruptedException{
		return (setSystemProperties == null) ? setSystemProperties = new ORD_SetSystemProperties_Page(driver) : setSystemProperties;
	}

	public ORD_Registration_Page getORD_Registration_Page() {
		 return (registrationPage == null) ? registrationPage = new ORD_Registration_Page(driver) : registrationPage;
		 }
	
	public ORD_Orders_Page getORD_Orders_Page() throws InterruptedException{
		 return (ordersPage == null) ? ordersPage = new ORD_Orders_Page(driver) : ordersPage;
		 }

	public DB_Query getDB_Query() {
		return (dbQuery == null) ? dbQuery = new DB_Query(driver) : dbQuery;
	}
	
	public ORD_Order_Page getORD_Order_Page() throws InterruptedException, IOException {
		 return (orderPage == null) ? orderPage = new ORD_Order_Page(driver) : orderPage;
		 }
	
	public ORD_Order_Details_Page getORD_Order_Details_Page() throws InterruptedException{
		 return (detailsPage == null) ? detailsPage = new ORD_Order_Details_Page(driver) : detailsPage;
		 }

	public Validate_Pdf getValidate_Pdf() throws InterruptedException{
		return (validate_Pdf == null) ? validate_Pdf = new Validate_Pdf(testContext) : validate_Pdf;
	}

	public ORD_Wallbox_Order_Details_Page getORD_Wallbox_Order_Details_Page() throws InterruptedException{
		return (wallboxDetailsPage == null) ? wallboxDetailsPage = new ORD_Wallbox_Order_Details_Page(driver) : wallboxDetailsPage;
	}
	
	public API_GetOrderByDossierNumber getAPI_GetOrderByDossierNumber() throws InterruptedException{
		 return (getOrderByDossierNumber == null) ? getOrderByDossierNumber = new API_GetOrderByDossierNumber(order) : getOrderByDossierNumber;
		 }
	
	public API_GetOrderDetailById getAPI_GetOrderDetailById() throws InterruptedException{
		 return (getOrderDetailById == null) ? getOrderDetailById = new API_GetOrderDetailById(orderDetail) : getOrderDetailById;
		 }
	
	public API_GetOrderNotifications getAPI_GetOrderNotifications() throws InterruptedException{
		 return (getOrderNotificationsFeed == null) ? getOrderNotificationsFeed = new API_GetOrderNotifications(orderNotificationsFeed) : getOrderNotificationsFeed;
		 }

	public API_GetWallBoxOrderNotifications getAPI_GetWallBoxOrderNotifications() throws InterruptedException{
		return (getWallBoxOrderNotifications == null) ? getWallBoxOrderNotifications = new API_GetWallBoxOrderNotifications(wallBoxOrderNotifications) : getWallBoxOrderNotifications;
	}
	
	public API_GetLeasingCompanyActorById getAPI_GetLeasingCompanyActorById() throws InterruptedException{
		 return (getLeasingCompanyActorById == null) ? getLeasingCompanyActorById = new API_GetLeasingCompanyActorById(leasingCompanyActor) : getLeasingCompanyActorById;
		 }
	
	public API_GetSupplierActorById getAPI_GetSupplierActorById() throws InterruptedException{
		 return (getSupplierActorById == null) ? getSupplierActorById = new API_GetSupplierActorById(supplierActor) : getSupplierActorById;
		 }

	public API_GetOrderTimelineById getAPI_GetOrderTimelineById() throws InterruptedException{
		 return (getOrderTimelineById == null) ? getOrderTimelineById = new API_GetOrderTimelineById(orderTimeline) : getOrderTimelineById;
		 }
	
	public FTP_PostOrderImportsRequest postFTP_PostOrderImportsRequest() throws InterruptedException{
		 return (postOrderImportsRequest == null) ? postOrderImportsRequest = new FTP_PostOrderImportsRequest(driver) : postOrderImportsRequest;
		 }

	public FTP_PostUpdateOrderImportsRequest postFTP_PostUpdateOrderImportsRequest() throws InterruptedException{
		return (postUpdateOrderImportsRequest == null) ? postUpdateOrderImportsRequest = new FTP_PostUpdateOrderImportsRequest(driver) : postUpdateOrderImportsRequest;
	}

	public FTP_PostOrderImportsMigrate postFTP_PostOrderImportsMigrate() throws InterruptedException{
		return (postOrderImportsMigrate == null) ? postOrderImportsMigrate = new FTP_PostOrderImportsMigrate(driver) : postOrderImportsMigrate;
	}

	public FTP_RegistrationImports postFTP_RegistrationImports() throws InterruptedException{
		return (registrationImports == null) ? registrationImports = new FTP_RegistrationImports() : registrationImports;
	}
	
	public FTP_PostWallBoxImports postFTP_PostWallBoxImports() throws InterruptedException{
		 return (postWallBoxImports == null) ? postWallBoxImports = new FTP_PostWallBoxImports() : postWallBoxImports;
		 }

	public API_PostWallBoxOrderAccepted postAPI_PostWallBoxOrderAccepted() {
		return (postWallBoxOrderAccepted == null) ? postWallBoxOrderAccepted = new API_PostWallBoxOrderAccepted() : postWallBoxOrderAccepted;
	}

	public API_PostWallBoxOrderInstalled postAPI_PostWallBoxOrderInstalled() {
		return (postWallBoxOrderInstalled == null) ? postWallBoxOrderInstalled = new API_PostWallBoxOrderInstalled() : postWallBoxOrderInstalled;
	}

	public API_PutRegistration putAPI_PutRegistration() {
		return (putRegistration == null) ? putRegistration = new API_PutRegistration() : putRegistration;
	}

	public API_PostBrokerRegistration postAPI_PostBrokerRegistration() {
		return (postBrokerRegistration == null) ? postBrokerRegistration = new API_PostBrokerRegistration() : postBrokerRegistration;
	}

	public API_GetRegistration getAPI_GetRegistration() throws InterruptedException{
		return (getRegistration == null) ? getRegistration = new API_GetRegistration() : getRegistration;
	}

	public API_GetBrokerRegistration getAPI_GetBrokerRegistration() throws InterruptedException, IOException {
		return (getBrokerRegistration == null) ? getBrokerRegistration = new API_GetBrokerRegistration() : getBrokerRegistration;
	}

	public API_PostWallBoxOrderReadyToActivate postAPI_PostWallBoxOrderReadyToActivate() {
		return (postWallBoxOrderReadyToActivate == null) ? postWallBoxOrderReadyToActivate = new API_PostWallBoxOrderReadyToActivate() : postWallBoxOrderReadyToActivate;
	}

	public API_PostWallBoxOrderActivated postAPI_PostWallBoxOrderActivated() {
		return (postWallBoxOrderActivated == null) ? postWallBoxOrderActivated = new API_PostWallBoxOrderActivated() : postWallBoxOrderActivated;
	}
	
	public FTP_PostOrderImportsNotification postFTP_PostOrderImportsNotification() throws InterruptedException{
		 return (postOrderImportsNotification == null) ? postOrderImportsNotification = new FTP_PostOrderImportsNotification() : postOrderImportsNotification;
		 }
	
	public FTP_GetOrderNotifications getFTP_GetOrderNotifications() throws InterruptedException{
		 return (getOrderNotifications == null) ? getOrderNotifications = new FTP_GetOrderNotifications(orderNotifications) : getOrderNotifications;
		 }

	public FTP_GetWallBoxOrderNotifications getFTP_GetWallBoxOrderNotifications() {
		return (getWallBoxOrderNotificationsFtp == null) ? getWallBoxOrderNotificationsFtp = new FTP_GetWallBoxOrderNotifications(wallBoxOrderNotificationsFtp) : getWallBoxOrderNotificationsFtp;
	}

	@Override
	public String toString() {
		return "PageObjectManager{" +
				"portalRSPage=" + portalRSPage +
				", driver=" + driver +
				", topBar=" + topBar +
				", registrationPage=" + registrationPage +
				", ordersPage=" + ordersPage +
				", dbQuery=" + dbQuery +
				", orderPage=" + orderPage +
				", detailsPage=" + detailsPage +
				", validate_Pdf=" + validate_Pdf +
				", wallboxDetailsPage=" + wallboxDetailsPage +
				", getOrderByDossierNumber=" + getOrderByDossierNumber +
				", getOrderDetailById=" + getOrderDetailById +
				", getLeasingCompanyActorById=" + getLeasingCompanyActorById +
				", getSupplierActorById=" + getSupplierActorById +
				", getOrderTimelineById=" + getOrderTimelineById +
				", getOrderNotificationsFeed=" + getOrderNotificationsFeed +
				", getWallBoxOrderNotifications=" + getWallBoxOrderNotifications +
				", postOrderImportsRequest=" + postOrderImportsRequest +
				", postUpdateOrderImportsRequest=" + postUpdateOrderImportsRequest +
				", postOrderImportsMigrate=" + postOrderImportsMigrate +
				", postWallBoxImports=" + postWallBoxImports +
				", postWallBoxOrderAccepted=" + postWallBoxOrderAccepted +
				", postWallBoxOrderInstalled=" + postWallBoxOrderInstalled +
				", postWallBoxOrderReadyToActivate=" + postWallBoxOrderReadyToActivate +
				", postWallBoxOrderActivated=" + postWallBoxOrderActivated +
				", postOrderImportsNotification=" + postOrderImportsNotification +
				", registrationImports=" + registrationImports +
				", getOrderNotifications=" + getOrderNotifications +
				", getWallBoxOrderNotificationsFtp=" + getWallBoxOrderNotificationsFtp +
				", putRegistration=" + putRegistration +
				", postBrokerRegistration=" + postBrokerRegistration +
				", getRegistration=" + getRegistration +
				", getBrokerRegistration=" + getBrokerRegistration +
				", order=" + order +
				", orderDetail=" + orderDetail +
				", leasingCompanyActor=" + leasingCompanyActor +
				", supplierActor=" + supplierActor +
				", orderTimeline=" + orderTimeline +
				", orderNotificationsFeed=" + orderNotificationsFeed +
				", orderNotificationFeed=" + orderNotificationFeed +
				", orderNotifications=" + orderNotifications +
				", wallBoxOrderNotifications=" + wallBoxOrderNotifications +
				", wallBoxOrderNotification=" + wallBoxOrderNotification +
				", setSystemProperties=" + setSystemProperties +
				", wallBoxOrderNotificationsFtp=" + wallBoxOrderNotificationsFtp +
				", wallBoxOrderNotificationFtp=" + wallBoxOrderNotificationFtp +
				", testContext=" + testContext +
				'}';
	}
}
