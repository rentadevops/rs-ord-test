package managers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.qeagle.devtools.services.ChromeDevToolsService;
//import io.github.bonigarcia.wdm.WebDriverManager;
import com.lambdatest.tunnel.Tunnel;
//import com.lambdatest.tunnel.TunnelException;
import com.lambdatest.tunnel.TunnelException;
import io.appium.java_client.AppiumDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import net.minidev.json.JSONAware;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
//import org.mockserver.client.server.MockServerClient;
import org.mockserver.client.MockServerClient;
//import org.mockserver.integration.ClientAndServer;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.matchers.Times;
import org.mockserver.model.Header;
import org.mockserver.model.MediaType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chrome.*;
//import org.openqa.selenium.chromium.AddHasNetworkConditions;
//import org.openqa.selenium.chromium.ChromiumDriver;
//import org.openqa.selenium.chromium.ChromiumNetworkConditions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.DevTools.*;
import org.openqa.selenium.devtools.HasDevTools;
//import org.openqa.selenium.devtools.v107.log.Log;
import org.openqa.selenium.devtools.DevTools.*;
//import org.openqa.selenium.devtools.v107.network.Network;
//import org.openqa.selenium.devtools.v107.network.model.ConnectionType;
//import org.openqa.selenium.devtools.v107.network.model.RequestId;
//import org.openqa.selenium.devtools.v109.log.Log;
//import org.openqa.selenium.devtools.v109.network.Network;
//import org.openqa.selenium.devtools.v109.network.model.RequestId;
//import org.openqa.selenium.devtools.v109.log.Log;
//import org.openqa.selenium.devtools.v109.network.Network;
//import org.openqa.selenium.devtools.v109.network.model.RequestId;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.*;

//import com.qeagle.devtools.protocol.types.log.LogEntry;
//import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.*;
//import org.openqa.selenium.remote.http.HttpClient;
import org.openqa.selenium.safari.SafariDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import java.util.*;
import java.util.AbstractCollection;

import enums.DriverType;
import enums.EnvironmentType;
//import com.qeagle.devtools.protocol.types.log.LogEntry;
//import io.github.bonigarcia.wdm.WebDriverManager;
//import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;;

import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockserver.integration.ClientAndServer.startClientAndServer ;
import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.StringBody.exact;
//import static org.openqa.selenium.chrome.ChromeOptions.LOGGING_PREFS;
import static org.openqa.selenium.remote.CapabilityType.*;
//import static org.openqa.selenium.devtools..Network.emulateNetworkConditions;
//import static org.openqa.selenium.devtools.v107.network.Network.loadingFailed;
import org.openqa.selenium.mobile.NetworkConnection;
//import org.testng.annotations.Test;
import io.opentelemetry.api.trace.Span;

public class WebDriverManager_ORD {
	final Logger logger = LogManager.getLogger(WebDriverManager_ORD.class);


	private WebDriver driver;
	private RemoteWebDriver remoteDriver;
	private static DriverType driverType;
	private static EnvironmentType environmentType;
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
	private static final String SAFARI_DRIVER_PROPERTY = "webdriver.safari.driver";
	// private static String OS = System.getProperty("os.name").toLowerCase();
	public static String Browser = "";
	private ClientAndServer mockServer;
	private MockServerClient mockServerClient;
	JSONObject json;
	JSONObject jsonReq;
	String jsonRequestStr;
	public static RemoteWebDriver connection;
	Tunnel t;

	public WebDriverManager_ORD() {
		System.setProperty("BrowserName", "chrome");
		System.setProperty("pdfbox.fontcache", "/tmp");
		driverType = FileReaderManager.getInstance().getPropertyFileReader().getBrowser();
		Browser = System.getProperty("BrowserName");

		logger.debug("System property browser: " + Browser);
		// environmentType =
		// FileReaderManager.getInstance().getConfigReader().getEnvironment();
	}

	public WebDriver getDriver() throws Exception {
		if (driver == null)
			driver = createDriver();
		return driver;
	}

	private WebDriver createDriver() throws Exception {
		String OS = System.getProperty("os.name").toLowerCase();
		logger.debug("os.name: " + OS);
		if (OS.equals("windows 10")) {
			//environmentType = EnvironmentType.LOCAL;
			environmentType = EnvironmentType.REMOTE;
		} else {
			environmentType = EnvironmentType.REMOTE;
		}
		switch (environmentType) {
			case LOCAL:
				//driver = createLocalDriver();
				driver = connectLambdaTest1();
				break;
			case REMOTE:
				//driver = createRemoteDriver();
				driver = connectLambdaTest1();
				break;
		}
		//driver = createLocalDriver();
		return driver;
	}

	private WebDriver createRemoteDriver() throws MalformedURLException, TunnelException {
		DesiredCapabilities capabilities = new DesiredCapabilities(Browser, "", Platform.ANY);
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.BROWSER, Level.ALL);
		//capabilities.setCapability(LOGGING_PREFS, logPrefs);
		//capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);


		//capabilities.setCapability("video", true);
		//capabilities.setCapability("video", "true");
		//System.setProperty("java.awt.headless", "true");
		//System.setProperty("java.net.preferIPv4Stack", "true");

		switch (Browser) {
			case "firefox":
				// System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,
				// "true");
				// System.setProperty(FIREFOX_DRIVER_PROPERTY,
				// FileReaderManager.getInstance().getPropertyFileReader().getRemoteDriverPath());
				// DesiredCapabilities capabilities = new DesiredCapabilities(Browser, "",
				// Platform.ANY);

				FirefoxOptions browserOptionsFF = new FirefoxOptions();
				//browserOptions.setPlatformName("Windows 10");
				//browserOptions.setPlatformName("LINUX");

				// add comment




				if (driver == null) {

				} else {
					driver.quit();
					driver = null;
				}
				//driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), browserOptions);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				//String titleFF = driver.getTitle();
				//logger.debug("Page Title: " + titleFF);

				break;
			// case CHROME :
			case "chrome":
				Browser = "chrome";
				/*String username = System.getenv("LT_USERNAME") == null ? "a.antonisse" : System.getenv("LT_USERNAME");
				String accesskey = System.getenv("LT_ACCESS_KEY") == null ? "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0" : System.getenv("LT_ACCESS_KEY");

				DesiredCapabilities capability = new DesiredCapabilities();
				capability.setCapability("browserName", "chrome");
				capability.setCapability("browserVersion","109.0");
				//capability.setCapability("build", "Jenkins Test");
				capability.setCapability("platformName", "Windows 10");
				//capability.setCapability("network", "true");
				ChromeOptions browserOptions = new ChromeOptions();
				browserOptions.setPlatformName("Windows 10");
				browserOptions.setBrowserVersion("109.0");

				//
				HashMap<String, Object> ltOptions = new HashMap<String, Object>();
				ltOptions.put("network", true);
				ltOptions.put("w3c", true);
				//capability.setCapability("enabledCustomTranslation", "true");
				//capability.setCapability("build", "Jenkins Test");
				//ChromeOptions browserOptions = new ChromeOptions();
				//HashMap<String, Object> ltOptions = new HashMap<String, Object>();
				ltOptions.put("project", "Jenkins Test");
				ltOptions.put("selenium_version", "4.8.0");
				ltOptions.put("terminal", true);
				ltOptions.put("console", true);
				browserOptions.setCapability("LT:Options", ltOptions);
				String gridURL = "@hub.lambdatest.com/wd/hub";
				System.out.println(gridURL);
				//connection = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
				connection = new RemoteWebDriver(new URL("https://" + username + ":" + accesskey + gridURL), capability);
				System.out.println(capability);
				System.out.println(connection);
				connection.get("https://google.com");
				String title = connection.getTitle();
				System.out.println("Page Title: " + title);*/




				System.setProperty("webdriver.http.factory", "jdk-http-client");
				ChromeOptions optionsChromeRemote = new ChromeOptions();
				optionsChromeRemote.setBinary("Drivers/chromedriver.exe");
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("download.default_directory", "/home/jenkins/agent/workspace/");
				optionsChromeRemote.setExperimentalOption("prefs", prefs);
				optionsChromeRemote.addArguments("--disable-dev-shm-usage");
				optionsChromeRemote.addArguments("--no-sandbox");

				DesiredCapabilities cap = new DesiredCapabilities(Browser, "109", Platform.LINUX);
				cap.setCapability("browserName", "chrome");
				cap.setCapability("browserVersion","109.0");
				cap.setCapability("platformName", "Windows 10");
				logPrefs = new LoggingPreferences();
				logPrefs.enable(LogType.BROWSER, Level.ALL);
				cap.setCapability(ChromeOptions.CAPABILITY, optionsChromeRemote);
				cap.setCapability("browserName", "chrome");
				cap.setCapability("browserVersion","109.0");
				cap.setCapability("platformName", "Windows 10");
				cap.setCapability("tunnel", true);

				logger.debug("Hallo?");
				long before = System.nanoTime();
				try {
					HttpClient client = HttpClient.newHttpClient();
					HttpRequest request = HttpRequest.newBuilder(URI.create("http://localhost:4444/wd/hub"))
							.timeout(Duration.ofSeconds(10))
							.build();
					HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
					System.out.println("response :" + response);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					System.out.println("elapsed secs :" + ((System.nanoTime() - before)/1000_000_000));
				}

				t = new Tunnel();
				HashMap<String, String> options = new HashMap<String, String>();
				options.put("user", "a.antonisse");
				options.put("key", "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0");

				System.out.println(options.get("user"));

				//start tunnel
				t.start(options);



				driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);


				logger.debug("driver remote", driver.toString());
				driver.manage().window().maximize();


				String title = driver.getTitle();
				logger.debug("Page Title: " + title);

				break;
			case "Edge":
				driver = new EdgeDriver();
				break;
			case "Safari":
				driver = new SafariDriver();
				break;
		}

		logger.debug("Driver: " + driver.toString());
		return driver;
	}

	private WebDriver createLocalDriver() throws MalformedURLException {
		if (Browser != null) {
			logger.debug("BrowserName: " + Browser);
			switch (Browser) {
				case "firefox":
					System.setProperty("BrowserName", "firefox");
					System.setProperty(FIREFOX_DRIVER_PROPERTY,
							FileReaderManager.getInstance().getPropertyFileReader().getDriverPathFirefox());
					driver = new FirefoxDriver();
					break;
				case "chrome":
					logger.debug("user.dir: " + System.getProperty("user.dir"));
					System.setProperty("BrowserName", "chrome");
					System.setProperty(CHROME_DRIVER_PROPERTY,
							FileReaderManager.getInstance().getPropertyFileReader().getDriverPath());
					DesiredCapabilities caps = new DesiredCapabilities(Browser, "", Platform.ANY);
					LoggingPreferences logPreferences = new LoggingPreferences();
					logPreferences.enable(LogType.BROWSER, Level.ALL);
					//logPreferences.addPreferences("performance", "ALL");
					//caps.setCapability(CapabilityType.LOGGING_PREFS, logPreferences);
					//caps['loggingPrefs'] = {"performance": "ALL"};

					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					logger.debug("user.dir: " + System.getProperty("user.dir"));
					chromePrefs.put("profile.default_content_settings.popups", 0);
					//chromePrefs.put("download.default_directory", System.getProperty("user.dir") + "/src/test/resources/runtime/Pdf");
					chromePrefs.put("download.default_directory", "C:\\Users\\AdAntonisse\\git\\RsOrdTest\\src\\test\\resources\\runtime\\Pdf");
					ChromeOptions options = new ChromeOptions();
					options.setExperimentalOption("prefs", chromePrefs);
					//WebDriver driver;
					WebDriverManager.chromedriver().setup();
					driver = new ChromeDriver(options);
					driver.manage().window().maximize();
					driver.manage().timeouts().implicitlyWait(
							FileReaderManager.getInstance().getPropertyFileReader().getImplicitlyWait(), TimeUnit.SECONDS);

					//BEGIN DEVTOOLS BLOK
					/*DevTools devTools = ((ChromeDriver) driver).getDevTools();
					devTools.createSession();

					devTools.send(Log.enable());
					devTools.addListener(Network.requestWillBeSent(),
							entry -> {
								System.out.println("Request URI : " + entry.getRequest().getUrl() + "\n"
										+ " With method : " + entry.getRequest().getMethod() + "\n"
										+ " and headers : " + entry.getRequest().getHeaders() + "\n"
								);
								entry.getRequest().getMethod();
								if (entry.getRequest().getHasPostData().get()) {
									jsonRequestStr = entry.getRequest().getPostData().get();
								}

							});
					JSONParser parser = new JSONParser(JSONParser.MODE_PERMISSIVE);
					try {
						System.out.println("Request Body: \n" + jsonRequestStr + "\n");

						//String jsonStrReq = devTools.send(Network.getResponseBody(requestIds[0])).getBody().toString();
						//try {
						jsonReq = new JSONObject(jsonRequestStr);
						System.out.println("json object Request: " + json.toString());
						JSONAware jsonModel = (JSONAware) parser.parse(jsonRequestStr);
						System.out.println("jsonModel Request: " + jsonModel.toJSONString());
						System.out.println("jsonobjecttostring request: " + jsonReq.toString(2));
					} catch (Exception e) {
						logger.debug("Geen Json body in request");
					}
					final RequestId[] requestIds = new RequestId[1];
					devTools.addListener(Network.responseReceived(), responseReceived -> {
						//if (responseReceived.getResponse().getUrl().contains("api.zoomcar.com")) {
						System.out.println("URL: " + responseReceived.getResponse().getUrl());
						System.out.println("Status: " + responseReceived.getResponse().getStatus());
						System.out.println("Type: " + responseReceived.getType().toJson());
						responseReceived.getResponse().getHeaders().toJson().forEach((k, v) -> System.out.println((k + ":" + v)));
						requestIds[0] = responseReceived.getRequestId();

						try {
							System.out.println("Response Body: \n" + devTools.send(Network.getResponseBody(requestIds[0])).getBody() + "\n");

							String jsonStr = devTools.send(Network.getResponseBody(requestIds[0])).getBody().toString();
							json = new JSONObject(jsonStr);
							System.out.println("json object: " + json.toString());
							JSONAware jsonModel = (JSONAware) parser.parse(jsonStr);
							System.out.println("jsonModel: " + jsonModel.toJSONString());
							System.out.println("jsonobjecttostring: " + json.toString(2));
						} catch (Exception e) {
							logger.debug("Geen Json body in response");
						}

					});

					devTools.addListener(Log.entryAdded(), logEntry -> {
						System.out.println("================");
						System.out.println("Log: " + logEntry.toString());
						System.out.println("Source: " + logEntry.getSource());
						System.out.println("Text: " + logEntry.getText());
						System.out.println("Timestamp: " + logEntry.getTimestamp());
						System.out.println("Level: " + logEntry.getLevel());
						System.out.println("URL: " + logEntry.getUrl());
					});                    //driver.get("https://demo.nopcommerce.com/");

					devTools.send(Network.enable(Optional.of(1000000), Optional.empty(), Optional.empty()));*/
					//EINDE DEVTOOLS BLOK
					logger.debug("driver in webmanager: " + driver.toString());
					return driver;
			}
			logger.debug("driver in webmanager: " + driver.toString());
			return driver;



			}
		 else {
			switch (driverType) {
				case FIREFOX:
					System.setProperty("BrowserName", "firefox");
					System.setProperty(FIREFOX_DRIVER_PROPERTY,
							FileReaderManager.getInstance().getPropertyFileReader().getDriverPath());

					DesiredCapabilities capabilities = new DesiredCapabilities(Browser, "", Platform.ANY);
					LoggingPreferences logPrefs = new LoggingPreferences();
					logPrefs.enable(LogType.BROWSER, Level.ALL);
					//capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);

					FirefoxOptions options = new FirefoxOptions();
					FirefoxProfile profile = new FirefoxProfile();
					options.setProfile(profile);
					driver = new FirefoxDriver(options);
					break;
				case CHROME:
						System.setProperty("BrowserName", "chrome");
					System.setProperty(CHROME_DRIVER_PROPERTY,
							FileReaderManager.getInstance().getPropertyFileReader().getDriverPath());
					DesiredCapabilities caps = new DesiredCapabilities(Browser, "", Platform.ANY);
					LoggingPreferences logPreferences = new LoggingPreferences();
					logPreferences.enable(LogType.BROWSER, Level.ALL);
					//logPreferences.addPreferences("performance", "ALL");
					//caps.setCapability(CapabilityType.LOGGING_PREFS, logPreferences);
					//caps['loggingPrefs'] = {"performance": "ALL"};


					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					logger.debug("user.dir: " + System.getProperty("user.dir"));
					chromePrefs.put("profile.default_content_settings.popups", 0);
					//chromePrefs.put("download.default_directory", System.getProperty("user.dir") + "/src/test/resources/runtime/Pdf");
					chromePrefs.put("download.default_directory", "C:\\Users\\AdAntonisse\\git\\RsOrdTest\\src\\test\\resources\\runtime\\Pdf");
					ChromeOptions chromeOptions = new ChromeOptions();
					chromeOptions.setExperimentalOption("prefs", chromePrefs);
					//chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPreferences);
					//caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
					//driver = new ChromeDriver(chromeOptions);


					//WebDriver driver;
					//WebDriverManager.chromedriver().setup();
					driver = new ChromeDriver(chromeOptions);
					driver.manage().window().maximize();


					/*MockServerClient mockServerClient = startClientAndServer(9999);
					//defineMockServerBehaviour(mockServerClient);




					//defineMockServerBehaviour(mockServerClient);

				//public static void defineMockServerBehaviour(MockServerClient mockServer) {

					mockServerClient.when(
							request()
									.withMethod("GET")
									.withPath("/default-delivery-information")
					).respond(
							response()
									.withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
									.withHeader(Header.header("Content-Type", "application/xml")));
									//.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
									//		"<Response>\n" +
									//		"    <responseStatus>OK</responseStatus>\n" +
									//		"</Response>"));*/


					/*mockServer = startClientAndServer(1080);
					//mockServerClient = new MockServerClient("localhost", 1080);
					//new MockServerClient("127.0.0.1", 1080)
					mockServer
							.when(
									request()
											//.withMethod("POST")
											.withPath("/default-delivery-information")
											.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString()),
											//.withBody(("{*}")),
									Times.exactly(1)
							)
							.respond(
									response()
											.withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
											.withHeaders(
													new Header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
											)
													//new Header("Cache-Control", "public, max-age=86400"))
											.withBody("{ message: 'You have been mocked' }")
											.withDelay(TimeUnit.SECONDS,1)
							);*/

					//BEGIN DEVTOOLS BLOK
					/*DevTools devTools = ((ChromeDriver)driver).getDevTools();
					devTools.createSession();

					devTools.send(Log.enable());

					devTools.addListener(Network.requestWillBeSent(),
							entry -> {
								System.out.println("Request URI : " + entry.getRequest().getUrl()+"\n"
										+ " With method : "+entry.getRequest().getMethod() + "\n"
								+ " and headers : " + entry.getRequest().getHeaders() + "\n"
								);
								entry.getRequest().getMethod();
								if(entry.getRequest().getHasPostData().get()) {
									jsonRequestStr = entry.getRequest().getPostData().get();
								}

							});
					JSONParser parser = new JSONParser(JSONParser.MODE_PERMISSIVE);
					try {
						System.out.println("Request Body: \n" + jsonRequestStr + "\n");

						//String jsonStrReq = devTools.send(Network.getResponseBody(requestIds[0])).getBody().toString();
						//try {
						jsonReq = new JSONObject(jsonRequestStr);
						System.out.println("json object Request: " + json.toString());
						JSONAware jsonModel = (JSONAware) parser.parse(jsonRequestStr);
						System.out.println("jsonModel Request: " + jsonModel.toJSONString());
						System.out.println("jsonobjecttostring request: " + jsonReq.toString(2));
					} catch(Exception e) {
						logger.debug("Geen Json body in request");
					}
					final RequestId[] requestIds = new RequestId[1];
					devTools.addListener(Network.responseReceived(), responseReceived -> {
						//if (responseReceived.getResponse().getUrl().contains("api.zoomcar.com")) {
							System.out.println("URL: " + responseReceived.getResponse().getUrl());
							System.out.println("Status: " + responseReceived.getResponse().getStatus());
							System.out.println("Type: " + responseReceived.getType().toJson());
							responseReceived.getResponse().getHeaders().toJson().forEach((k, v) -> System.out.println((k + ":" + v)));
							requestIds[0] = responseReceived.getRequestId();
								try {
									System.out.println("Response Body: \n" + devTools.send(Network.getResponseBody(requestIds[0])).getBody() + "\n");

									String jsonStr = devTools.send(Network.getResponseBody(requestIds[0])).getBody().toString();
									//try {
										json = new JSONObject(jsonStr);
										System.out.println("json object: " + json.toString());
										JSONAware jsonModel = (JSONAware) parser.parse(jsonStr);
										System.out.println("jsonModel: " + jsonModel.toJSONString());
										System.out.println("jsonobjecttostring: " + json.toString(2));
									} catch(Exception e) {
									logger.debug("Geen Json body in response");
									}

					});

					devTools.addListener(Log.entryAdded(), logEntry -> {
						System.out.println("================");
						System.out.println("Log: " + logEntry.toString());
						System.out.println("Source: " + logEntry.getSource());
						System.out.println("Text: " + logEntry.getText());
						System.out.println("Timestamp: " + logEntry.getTimestamp());
						System.out.println("Level: " + logEntry.getLevel());
						System.out.println("URL: " + logEntry.getUrl());
					});

					devTools.send(Network.enable(Optional.of(1000000), Optional.empty(), Optional.empty()));*/
				//EINDE DEVTOOLS BLOK


			//break;
				//case EDGE:
				//	EdgeDriver edgedriver = new EdgeDriver();
				//	break;
				//case OPERA:
				//	OperaDriver operadriver = new OperaDriver();
				//	break;

		}
		//logger.debug("driver in webmanager: " + driver.toString());
		return driver;
	}


/*		if (FileReaderManager.getInstance().getPropertyFileReader().getBrowserWindowSize())
			driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(
				FileReaderManager.getInstance().getPropertyFileReader().getImplicitlyWait(), TimeUnit.SECONDS);*/
		//return driver;
}

	private WebDriver connectLambdaTest() throws IOException {






		String username = System.getenv("LT_USERNAME") == null ? "a.antonisse" : System.getenv("LT_USERNAME");
		String accesskey = System.getenv("LT_ACCESS_KEY") == null ? "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0" : System.getenv("LT_ACCESS_KEY");

		//DesiredCapabilities capability = new DesiredCapabilities();
		//capability.setCapability("browserName", "chrome");
		//capability.setCapability("browserVersion","109.0");
		//capability.setCapability("build", "Jenkins Test");
		//capability.setCapability("platformName", "Windows 10");
		//capability.setCapability("network", "true");
		//ChromeOptions browserOptions = new ChromeOptions();
		//browserOptions.setPlatformName("Windows 10");
		//browserOptions.setBrowserVersion("109.0");

		//
		//HashMap<String, Object> ltOptions = new HashMap<String, Object>();
		//ltOptions.put("network", true);
		//ltOptions.put("w3c", true);
		//capability.setCapability("enabledCustomTranslation", "true");
		//capability.setCapability("build", "Jenkins Test");
		//ChromeOptions browserOptions = new ChromeOptions();
		//HashMap<String, Object> ltOptions = new HashMap<String, Object>();
		//ltOptions.put("project", "Jenkins Test");
		//ltOptions.put("selenium_version", "4.5.0");
		//ltOptions.put("terminal", true);
		//ltOptions.put("console", true);
		//browserOptions.setCapability("LT:Options", ltOptions);

		/*ChromeOptions browserOptions = new ChromeOptions();
		browserOptions.setPlatformName("Windows 10");
		browserOptions.setBrowserVersion("109.0");
		HashMap<String, Object> ltOptions = new HashMap<String, Object>();
		ltOptions.put("username", "a.antonisse");
		ltOptions.put("accessKey", "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0");
		ltOptions.put("project", "Untitled");
		//ltOptions.put("selenium_version", "4.0.0");
		//ltOptions.put("w3c", true);
		browserOptions.setCapability("LT:Options", ltOptions);*/






		//DesiredCapabilities capability = new DesiredCapabilities();
		//capability.setCapability(CapabilityType.BROWSER_NAME, "chrome");





		//capability.setCapability(CapabilityType.VERSION,"4.8.0");
		//capability.setCapability(CapabilityType.PLATFORM, "Linux");

		//capability.setCapability("build", "ORD Test Build");

		//capability.setCapability("network", true);
		//capability.setCapability("video", true);
		//capability.setCapability("console", true);
		//capability.setCapability("visual", true);
		//capability.setCapability("w3c", true);

		//ChromeOptions browserOptions = new ChromeOptions();
		ChromeOptions co = new ChromeOptions();
		ChromeDriverService service = ChromeDriverService.createDefaultService();
		//DesiredCapabilities capabilities = new DesiredCapabilities();

		co.setPlatformName("Windows 10");
		co.setBrowserVersion("110.0");

		Map<String, Object> cloudOptions = new HashMap<>();
		cloudOptions.put("build", "ORD Lambda Test");
		cloudOptions.put("name", "ORD");
		//co.setCapability("build", "ORD Lambda Test");
		cloudOptions.put("network", true);
		cloudOptions.put("video", true);
		cloudOptions.put("console", true);
		cloudOptions.put("visual", true);
		cloudOptions.put("w3c", true);
		co.setCapability("cloud:options", cloudOptions);


		//capabilities.setPlatformName("Windows 10");
		//capabilities.setBrowserVersion("110.0");
		/*HashMap<String, Object> ltOptions = new HashMap<String, Object>();
		ltOptions.put("username", "a.antonisse");
		ltOptions.put("accessKey", "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0");
		//ltOptions.put("seCdp", true);
		ltOptions.put("network", true);
		ltOptions.put("build", "ORD Lambda Test");
		ltOptions.put("project", "ORD");
		ltOptions.put("tunnel", true);
		ltOptions.put("selenium_version", "4.0.0");
		ltOptions.put("w3c", true);
		chromecaps.setCapability("LT:Options", ltOptions);*/

		System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
		WebDriverManager.chromedriver().setup();

		ChromeDriverService cds = new ChromeDriverService.Builder()
				.usingDriverExecutable(new File("/Drivers/chromedriver.exe"))
				.usingAnyFreePort()
				.build();
		cds.start();
		logger.debug("chromedriverservice gestart: " + cds.toString());




		String gridURL = "@hub.lambdatest.com/wd/hub";
		System.out.println(gridURL);
		//connection = new RemoteWebDriver(new URL("https://" + username + ":" + accesskey + gridURL), co);
		//ChromeDriver driver = new ChromeDriver(new URL("https://" + username + ":" + accesskey + gridURL), co);
		driver = new RemoteWebDriver(service.getUrl(), co);

		//System.out.println(connection.getSessionId());
		//connection.get("https://google.com");
		//String title = connection.getTitle();
		//System.out.println("Page Title: " + title);
		//driver = connection;
		return driver;
	}

	public WebDriver connectLambdaTest1() throws Exception {

		//String username = System.getenv("LT_USERNAME") == null ? "YOUR LT_USERNAME" : System.getenv("LT_USERNAME");
		//String accesskey = System.getenv("LT_ACCESS_KEY") == null ? "YOUR LT_ACCESS_KEY" : System.getenv("LT_ACCESS_KEY");
		String username = System.getenv("LT_USERNAME") == null ? "a.antonisse" : System.getenv("LT_USERNAME");
		String accesskey = System.getenv("LT_ACCESS_KEY") == null ? "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0" : System.getenv("LT_ACCESS_KEY");


		DesiredCapabilities capability = new DesiredCapabilities();
		capability.setCapability(CapabilityType.BROWSER_NAME, "chrome");
		capability.setCapability(CapabilityType.VERSION,"109.0");
		capability.setCapability(CapabilityType.PLATFORM, "Windows 10");

		capability.setCapability("build", "Cucumber Sample Build");

		capability.setCapability("network", true);
		capability.setCapability("video", true);
		capability.setCapability("console", true);
		capability.setCapability("visual", true);

		String gridURL = "https://" + username + ":" + accesskey + "@hub.lambdatest.com/wd/hub";
		System.out.println(gridURL);
		connection = new RemoteWebDriver(new URL(gridURL), capability);
		System.out.println(capability);
		System.out.println(connection.getSessionId());
		driver = connection;
		return driver;
	}


	public void closeDriver() {
		logger.debug("CloseDriver webdrivermanger: ");
		//driver.close();
		//mockServer.stop();
		driver.quit();
	}

	public void closeDriverFF() {
		logger.debug("CloseDriverFF webdrivermanger: ");
		//driver.close();
		driver.quit();
	}

}
