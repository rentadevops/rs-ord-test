package cucumber;

import java.util.HashMap;
import java.util.Map;
import enums.Context;
import enums.ContextGetLeasingCompanyActorByIdApi;
import enums.ContextGetSupplierActorByIdApi;
import enums.ContextOrderDetailUI;
import enums.ContextOrderUI;
import enums.ContextPostOrderImportsRequest;
import pageobjects.ftp.FTP_PostOrderImportsRequest;

public class ScenarioContext {

	private  Map<String, Object> scenarioContext;
	 
    public ScenarioContext(){
        scenarioContext = new HashMap<String, Object>();
    }
    
    public void setContext(Context key, Object value) {
    	scenarioContext.put(key.toString(), value);
    }
    
    public Object getContext(Context key){
        return scenarioContext.get(key.toString());
    }
 
    public Boolean isContains(Context key){
        return scenarioContext.containsKey(key.toString());
    }
    
    public void setContext(ContextOrderDetailUI key, Object value) {
        scenarioContext.put(key.toString(), value);
    }

    public Object getContext(ContextOrderDetailUI key){
        return scenarioContext.get(key.toString());
    }

    public Boolean isContains(ContextOrderDetailUI key){
        return scenarioContext.containsKey(key.toString());
    }
    
    public void setContext(ContextPostOrderImportsRequest key, Object value) {
        scenarioContext.put(key.toString(), value);
    }

    public Object getContext(ContextPostOrderImportsRequest key){
        return scenarioContext.get(key.toString());
    }

    public Boolean isContains(ContextPostOrderImportsRequest key){
        return scenarioContext.containsKey(key.toString());
    }
    
    public void setContext(ContextOrderUI key, Object value) {
        scenarioContext.put(key.toString(), value);
    }

    public Object getContext(ContextOrderUI key){
        return scenarioContext.get(key.toString());
    }

    public Boolean isContains(ContextOrderUI key){
        return scenarioContext.containsKey(key.toString());
    }
    
    public void setContext(ContextGetLeasingCompanyActorByIdApi key, Object value) {
        scenarioContext.put(key.toString(), value);
    }

    public Object getContext(ContextGetLeasingCompanyActorByIdApi key){
        return scenarioContext.get(key.toString());
    }

    public Boolean isContains(ContextGetLeasingCompanyActorByIdApi key){
        return scenarioContext.containsKey(key.toString());
    }
    
    public void setContext(ContextGetSupplierActorByIdApi key, Object value) {
        scenarioContext.put(key.toString(), value);
    }

    public Object getContext(ContextGetSupplierActorByIdApi key){
        return scenarioContext.get(key.toString());
    }

    public Boolean isContains(ContextGetSupplierActorByIdApi key){
        return scenarioContext.containsKey(key.toString());
    }

    
}
