package cucumber;
 
import java.io.IOException;
import java.net.MalformedURLException;

import managers.PageObjectManager;
import managers.WebDriverManager_ORD;

 
public class TestContext {
 private WebDriverManager_ORD webDriverManager_ORD;
 private PageObjectManager pageObjectManager;
 public ScenarioContext scenarioContext;
  
 public TestContext() throws Exception {
  webDriverManager_ORD = new WebDriverManager_ORD();
 pageObjectManager = new PageObjectManager(webDriverManager_ORD.getDriver());
 scenarioContext = new ScenarioContext();
  }
 
 public WebDriverManager_ORD getWebDriverManager() {
 return webDriverManager_ORD;
 }
 
 public PageObjectManager getPageObjectManager() {
 return pageObjectManager;
 }
 
 public ScenarioContext getScenarioContext() {
	 return scenarioContext;
 }
 
}