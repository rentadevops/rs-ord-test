package pageobjects.android;

//import io.appium.java_client.AppiumBy;

import cucumber.TestContext;
import helpers.AssertHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.testng.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.ui.ORD_Order_Page;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ui.Validate_Mobile_Vehicle_Info;
import utils.AndroidActions;
import utils.AppiumUtils;

import java.io.*;
import java.time.Duration;
import java.util.*;


//import static jdk.internal.net.http.common.Utils.getIntegerProperty;


//import static org.openqa.selenium.support.PageFactory.initElements;

public class NewVehicleInfo extends AndroidActions {
    final public Logger logger = LogManager.getLogger(NewVehicleInfo.class);
    AppiumDriver driver;
    TestContext testContet;
    ORD_Order_Page orderPage;
    FTP_PostOrderImportsRequest postOrderImportsRequest;
    Validate_Mobile_Vehicle_Info validateMobileVehicleInfo;
    //PostOrderImportsRequest postOrderImportsRequest;
    //public final static int IMPLICIT_WAIT = getIntegerProperty("implicitWait", 30);
    AssertHelper ah = new AssertHelper();
    String merk;
    String model;
    String versie;
    String exterieur;
    String interieur;
    String banden;
    String vin;
    String plaat;

    public NewVehicleInfo(AppiumDriver driver) throws InterruptedException {
        super(driver);
        this.driver = driver;
        AppiumFieldDecorator ad = new AppiumFieldDecorator(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);


        try {
            //PageFactory.initElements(driver, this);
            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
            //PageFactory.initElements(new AppiumFieldDecorator(driver, IMPLICIT_WAIT, TimeUnit.SECONDS), this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public NewVehicleInfo(TestContext testContext) throws InterruptedException, IOException {
        super(testContext);
        orderPage = testContext.getPageObjectManager().getORD_Order_Page();
        postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();

    }

    @FindBy(how = How.XPATH, using = "//h1")
    private WebElement we_lbl_title;

    @FindBy(how = How.XPATH, using = "//div[text()='Brand']/following-sibling::div")
    private WebElement we_brand;

    @FindBy(how = How.XPATH, using = "//div[text()='Merk']/following-sibling::div")
    private WebElement we_brand_nl;

    @FindBy(how = How.XPATH, using = "//div[text()='Brand']/following-sibling::div")
    private WebElement we_brand_en;

    @FindBy(how = How.XPATH, using = "//div[text()='Marque']/following-sibling::div")
    private WebElement we_brand_fr;

    @FindBy(how = How.XPATH, using = "//div[text()='Model']/following-sibling::div")
    private WebElement we_model;

    @FindBy(how = How.XPATH, using = "//div[text()='Version']/following-sibling::div")
    private WebElement we_version;

    @FindBy(how = How.XPATH, using = "//div[text()='Exterior']/following-sibling::div")
    private WebElement we_exterior;

    @FindBy(how = How.XPATH, using = "//div[text()='Interior']/following-sibling::div")
    private WebElement we_interior;

    @FindBy(how = How.XPATH, using = "//div[text()='Tyres']/following-sibling::div")
    private WebElement we_tyres;

    @FindBy(how = How.XPATH, using = "//div[text()='Vin']/following-sibling::div")
    private WebElement we_vin;

    @FindBy(how = How.XPATH, using = "//div[text()='Plate']/following-sibling::div")
    private WebElement we_plate;

    @FindBy(how = How.XPATH, using = "//div[@id='receivedEquipment']/span")
    private WebElement we_received_equipment;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='new-vehicle-next-page-btn']")
    private WebElement we_btn_new_vehicle_next;



    String str = "Certificate of Conformity \n" +
            " 2  keys \n" +
            "Registration certificate ";


    public void validate_new_vehicle() throws IOException, InterruptedException {
        //TestContext testContext;
        //logger.debug("postOrderImportsRequest.getVehicleBrand(): " + postOrderImportsRequest.getVehicleBrand());
        //Thread.sleep(5000);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        //wait.until(ExpectedConditions.visibilityOf(we_lbl_title));

        Map<String, String> map = new HashMap<>();
        //map.put("Title", we_lbl_title.getText());
        //map.put("Merk", we_brand_nl.getText());

        if (System.getProperty("appLanguage").equals("nl")) {
            //logger.debug("Start validate new vehicle nederlands");
            //String merkStr = orderImportsRequest.getVehicleBrand();
            //ah.assertEquals("Title: ","Ingeleverd voertuig", we_lbl_title.getText());
            //ah.assertEquals("Merk: ",merkStr, we_brand_nl.getText());

            map.put("Title", we_lbl_title.getText());
            map.put("Merk", we_brand_nl.getText());
            //validateMobileVehicleInfo.validateVehicleInfo(map);
            try {
                File VehicleMap=new File("./src/test/resources/runtime/VehicleMap");
                FileOutputStream fos=new FileOutputStream(VehicleMap);
                ObjectOutputStream oos=new ObjectOutputStream(fos);

                oos.writeObject(map);
                oos.flush();
                oos.close();
                fos.close();
                //validateMobileVehicleInfo.validateVehicleInfo();
                validateVehicleInfo();
            } catch(Exception e) {logger.debug("Error aanroep validatie???");}


            we_btn_new_vehicle_next.click();

        }
        if (System.getProperty("appLanguage").equals("en")) {
            logger.debug("Start validate new vehicle engels");
            //String merkStr = orderImportsRequest.getVehicleBrand();
            //ah.assertEquals("Title: ","Your new vehicle", we_lbl_title.getText());
            //ah.assertEquals("Merk: ",merkStr, we_brand.getText());
            Thread.sleep(1000);
            map.put("Title", we_lbl_title.getText());
            Thread.sleep(1000);
            map.put("Brand", we_brand_en.getText());
            try {
                File VehicleMap=new File("./src/test/resources/runtime/VehicleMap");
                FileOutputStream fos=new FileOutputStream(VehicleMap);
                ObjectOutputStream oos=new ObjectOutputStream(fos);

                oos.writeObject(map);
                oos.flush();
                oos.close();
                fos.close();
                //validateMobileVehicleInfo.validateVehicleInfo();
                validateVehicleInfo();
            } catch(Exception e) {}



            we_btn_new_vehicle_next.click();

        }
        if (System.getProperty("appLanguage").equals("fr")) {
            logger.debug("Start validate new vehicle frans");
            //String merkStr = orderImportsRequest.getVehicleBrand();
            //ah.assertEquals("Title: ","Votre nouveau véhicule", we_lbl_title.getText());
            //ah.assertEquals("Marque: ",merkStr, we_brand_fr.getText());

            map.put("Title", we_lbl_title.getText());
            map.put("Merk", we_brand_nl.getText());
            try {
                File VehicleMap=new File("./src/test/resources/runtime/VehicleMap");
                FileOutputStream fos=new FileOutputStream(VehicleMap);
                ObjectOutputStream oos=new ObjectOutputStream(fos);

                oos.writeObject(map);
                oos.flush();
                oos.close();
                fos.close();
                //validateMobileVehicleInfo.validateVehicleInfo();
                validateVehicleInfo();
            } catch(Exception e) {}


            we_btn_new_vehicle_next.click();

        }
    }

    public void validateVehicleInfo() {
        logger.debug("Start validateVehicleInfo");
        try {
            File toRead=new File("./src/test/resources/runtime/VehicleMap");
            FileInputStream fis=new FileInputStream(toRead);
            ObjectInputStream ois=new ObjectInputStream(fis);

            HashMap<String,String> mapInFile=(HashMap<String,String>)ois.readObject();

            ois.close();
            fis.close();
            //print All data in MAP
            //for(Map.Entry<String,String> m :mapInFile.entrySet()){
            //	System.out.println(m.getKey()+" : "+m.getValue());
            //}
            mapInFile.forEach((k, v) -> {
                logger.debug("Key: " + k + ", Value: " + v);
                if (k.equals("Titel") | k.equals("Title")) {
                    ah.assertEquals("Title: ", "Your new vehicle", v);
                }
                if (k.equals("Brand: ")) {
                    ah.assertEquals("Brand: ", postOrderImportsRequest.getVehicleBrand(), v);
                }
            });
        } catch(Exception e) {}
    }

        }

