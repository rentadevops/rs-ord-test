package pageobjects.android;

//import io.appium.java_client.AppiumBy;

import cucumber.TestContext;
import helpers.AssertHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.ITestResult;
//import org.testng.Reporter;
import pageobjects.ui.ORD_Order_Page;
import utils.AndroidActions;
import utils.AppiumUtils;

import java.io.IOException;
import java.time.Duration;


//import static jdk.internal.net.http.common.Utils.getIntegerProperty;


//import static org.openqa.selenium.support.PageFactory.initElements;

public class Confirm extends AndroidActions {
    final Logger logger = LogManager.getLogger(Confirm.class);
    AppiumDriver driver;
    TestContext testContext;
    ORD_Order_Page orderPage;
    //PostOrderImportsRequest postOrderImportsRequest;
    //public final static int IMPLICIT_WAIT = getIntegerProperty("implicitWait", 30);
    AssertHelper ah = new AssertHelper();



    public Confirm(AppiumDriver driver) {
    super(driver);
    this.driver = driver;
        AppiumFieldDecorator ad = new AppiumFieldDecorator(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

        try {
            //PageFactory.initElements(driver, this);
            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
            //PageFactory.initElements(new AppiumFieldDecorator(driver, IMPLICIT_WAIT, TimeUnit.SECONDS), this);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @FindBy(how = How.XPATH, using = "//p")
    private WebElement we_confirmation_text;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='confirmation-confirm-btn']")
    private WebElement we_btn_confirm;





public void validate_confirm() throws IOException, InterruptedException {

    //WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    Thread.sleep(2000);
    if(System.getProperty("appLanguage").equals("nl")) {
        logger.debug("Start validate confirm nederlands");
        String confirm_text = " Hierbij bevestig ik dat ik het beschreven voertuig heb ontvangen en verklaar ik, gemachtigd door de huurder, dat het voertuig overeenstemt met wat werd besteld, en vrij is van zichtbare gebreken. De bevestiging leidt tot het begin van het leasingcontract.";
        ah.assertEquals("Confirm text: ",confirm_text.trim(), we_confirmation_text.getText().trim());

        logger.debug(ah.processAllAssertions());
        //driver.executeScript("lambda-status=passed");
        //driver.executeScript('lambda-status=passed');
        //Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
        Thread.sleep(1000);
        we_btn_confirm.click();

    }
    else if(System.getProperty("appLanguage").equals("en")) {
        logger.debug("Start validate confirm engels");
        String confirm_text = " I hereby confirm that I have received the described vehicle and, authorized by the lessee, I declare that the vehicle corresponds to what was ordered and is free of visible defects. Confirmation results in the start of the leasing contract.";
        ah.assertEquals("Confirm text: ",confirm_text.trim(), we_confirmation_text.getText().trim());

        logger.debug(ah.processAllAssertions());
        //driver.executeScript("lambda-status=passed");
        //Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
        Thread.sleep(1000);
        we_btn_confirm.click();

    }
    else if(System.getProperty("appLanguage").equals("fr")) {
        logger.debug("Start validate confirm french");
        //WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p")));
        String confirm_text = " Par la présente, je confirme avoir reçu le véhicule décrit et, autorisé par le locataire, je déclare que le véhicule correspond à ce qui a été commandé et est exempt de défauts visibles. La confirmation entraîne le début du contrat de leasing.";
        ah.assertEquals("Confirm text: ",confirm_text.trim(), we_confirmation_text.getText().trim());

        logger.debug(ah.processAllAssertions());
        Thread.sleep(1000);
        //driver.executeScript('lambda-status=failed');
        //driver.executeScript("lambda-status=passed");
        //Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
        Thread.sleep(1000);
        we_btn_confirm.click();

    }

    //driver.rotate(ScreenOrientation.PORTRAIT);
    }



}
