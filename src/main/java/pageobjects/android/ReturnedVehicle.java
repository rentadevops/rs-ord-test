package pageobjects.android;

//import io.appium.java_client.AppiumBy;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.AndroidActions;
import utils.AppiumUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


//import static jdk.internal.net.http.common.Utils.getIntegerProperty;


//import static org.openqa.selenium.support.PageFactory.initElements;

public class ReturnedVehicle extends AndroidActions {
    final Logger logger = LogManager.getLogger(ReturnedVehicle.class);
    AppiumDriver driver;
    String licensePlate;
    //List<WebElement> els;
    //public final static int IMPLICIT_WAIT = getIntegerProperty("implicitWait", 30);
    AndroidActions aa = new AndroidActions(driver);
    ReturnedVehicle returnedVehicle;
    String errorTextLicensePlate_nl = " De nummerplaat van het ingeleverde voertuig mag niet dezelfde zijn als die van het nieuwe voertuig ";
    String errorTextLicensePlate_en = "License plate returned vehicle cannot be the same as that of the new vehicle";

    public ReturnedVehicle(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        AppiumFieldDecorator ad = new AppiumFieldDecorator(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

        try {
            //PageFactory.initElements(driver, this);
            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
            //PageFactory.initElements(new AppiumFieldDecorator(driver, IMPLICIT_WAIT, TimeUnit.SECONDS), this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    @FindBy(how = How.XPATH, using = "//mat-slide-toggle[@formcontrolname='hasPreviousVehicle']")
    private WebElement toggle_has_previous_vehicle;

    @FindBy(how = How.XPATH, using = "//mat-slide-toggle[@formcontrolname='hasPreviousVehicle']//input")
    private WebElement toggle_previous_vehicle_checked;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='returned-vehicle-next-page-btn']")
    private WebElement btn_license_plate_next;

    @FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='leasingCompanyRsNumber']")
    private WebElement list_owner;

    @FindBy(how = How.XPATH, using = "//span[contains(.,'ALD')]")
    private WebElement option_ALD_Automotive;

    @FindBy(how = How.XPATH, using = "//input[@formcontrolname='licensePlate']")
    private WebElement input_license_plate;

    @FindBy(how = How.XPATH, using = "//mat-error")
    private WebElement error_license_plate;

    //@FindBy(how = How.XPATH, using = "//button[@data-testid='license-plate-next-page-btn']")
    //private WebElement btn_license_plate_next;

    String pass = "lambda-status=passed";
    String fail = "lambda-status=failed";

    public void setTogglePreviousVehicleTrue() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try {
            //Thread.sleep(3000);
            wait.until(ExpectedConditions.visibilityOf(toggle_previous_vehicle_checked));
            if (toggle_previous_vehicle_checked.getAttribute("aria-checked").equals("true")) {
                logger.debug("is al gechecked, niet klikken");
            } else {
                logger.debug("is niet gechecked, klikken");
                toggle_has_previous_vehicle.click();
            }
            btn_license_plate_next.click();
            //Thread.sleep(3000);
            wait.until(ExpectedConditions.visibilityOf(btn_license_plate_next));
            //btn_license_plate_next.click();
        } catch (Exception e) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(fail);
        }
    }

    public void setTogglePreviousVehicleFalse() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        if(toggle_previous_vehicle_checked.getAttribute("aria-checked").equals("false")) {
            logger.debug("is al uit, niet klikken");
        } else {
            toggle_has_previous_vehicle.click();
            logger.debug("is gechecked, uit klikken");
        }
        btn_license_plate_next.click();
        //Thread.sleep(3000);
        wait.until(ExpectedConditions.visibilityOf(btn_license_plate_next));
        //btn_license_plate_next.click();
    }

    public void setOwner() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try {
            //Thread.sleep(3000);
            wait.until(ExpectedConditions.visibilityOf(list_owner));
            list_owner.click();
            //Thread.sleep(1000);
            wait.until(ExpectedConditions.visibilityOf(option_ALD_Automotive));
            option_ALD_Automotive.click();
        } catch(Exception e) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(fail);
        }
    }

    public void input_license_plate() throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        //Properties properties = new Properties();
        //InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/licenseplate.properties");
        //properties.load(inputDriver);
        //licensePlate = properties.getProperty("LicensePlate");
        //licensePlate = "1FFF333";
        try {
            Properties properties = new Properties();
            InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/licenseplate.properties");
            properties.load(inputDriver);
            licensePlate = properties.getProperty("LicensePlate");
            //licensePlate = "1FFF333";
            logger.debug("licensePlate via returneVehicle getlicensePlate: " + licensePlate);
            input_license_plate.sendKeys(licensePlate);

            if(error_license_plate.isDisplayed()) {
                if(System.getProperty("appLanguage").equals("nl")) {
                    Assert.assertEquals(errorTextLicensePlate_nl.trim(), error_license_plate.getText().trim());
                    logger.debug("assert nl op error text licenseplate: ");
                    licensePlate = "1FFF333";
                }
                else if(System.getProperty("appLanguage").equals("en")) {
                    Assert.assertEquals(errorTextLicensePlate_en.trim(), error_license_plate.getText().trim());
                    logger.debug("assert en op error text licenseplate: ");
                    licensePlate = "1FFF333";
                }
                input_license_plate.clear();
                Thread.sleep(1000);
                input_license_plate.sendKeys(licensePlate);
            }

            //scrollToEndAction();
            //driver.hideKeyboard();
            //Thread.sleep(3000);
            wait.until(ExpectedConditions.visibilityOf(btn_license_plate_next));
            btn_license_plate_next.click();
            //driver.hideKeyboard();
        } catch(Exception e) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(fail);
        }
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }
}
