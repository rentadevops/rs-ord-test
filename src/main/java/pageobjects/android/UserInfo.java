package pageobjects.android;

//import io.appium.java_client.AppiumBy;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ExecutesMethod;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.testng.ITestResult;
//import org.testng.Reporter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.AndroidActions;

import java.time.Duration;


//import static jdk.internal.net.http.common.Utils.getIntegerProperty;


//import static org.openqa.selenium.support.PageFactory.initElements;

public class UserInfo extends AndroidActions {
    final Logger logger = LogManager.getLogger(UserInfo.class);
    AppiumDriver driver;
    //public final static int IMPLICIT_WAIT = getIntegerProperty("implicitWait", 30);
    String driverBirthday = "1965-06-19";

    public UserInfo(AppiumDriver driver) {
    super(driver);
    this.driver = driver;
        AppiumFieldDecorator ad = new AppiumFieldDecorator(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

        try {
            //PageFactory.initElements(driver, this);
            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
            //PageFactory.initElements(new AppiumFieldDecorator(driver, IMPLICIT_WAIT, TimeUnit.SECONDS), this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @FindBy(how = How.XPATH, using = "//input[@formcontrolname='email']")
    private WebElement input_email;

    @FindBy(how = How.XPATH, using = "//input[@formcontrolname='birthDate']")
    private WebElement list_birthday;

    @FindBy(how = How.XPATH, using = "//button[@aria-label='Previous 24 years']")
    private WebElement btn_previous_24_years;

    @FindBy(how = How.XPATH, using = "//button[@aria-label='1965']/div[contains(.,'1965')]")
    private WebElement input_birthyear;

    @FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'Jun 1965')]")
    private WebElement input_birthmonth;

    @FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'jun. 1965')]")
    private WebElement input_birthmonth_nl;

    @FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'Jun 1965')]")
    private WebElement input_birthmonth_en;

    @FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'juin 1965')]")
    private WebElement input_birthmonth_fr;

    @FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'Jun 1965')]")
    private WebElement input_birthday;

    @FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'jun. 1965')]/div[contains(.,'19')]")
    private WebElement input_birthday_nl;

    @FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'Jun 1965')]/div[contains(.,'19')]")
    private WebElement input_birthday_en;

    @FindBy(how = How.XPATH, using = "//button[@aria-label = 'juin 1965']/div[contains(.,'19')]")
    private WebElement input_birthday_fr;

    @FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='mainDriver']")
    private WebElement list_isdriver;

    @FindBy(how = How.XPATH, using = "//mat-option[@value='true']")
    private WebElement opt_isdriver_yes;

    @FindBy(how = How.XPATH, using = "//mat-option[contains(.,'Oui')]")
    private WebElement opt_isdriver_yes_fr;

    @FindBy(how = How.XPATH, using = "//mat-option[contains(.,'No')]")
    private WebElement opt_isdriver_no;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='user-info-next-page-btn']")
    private WebElement btn_to_next_page;


    String pass = "lambda-status=passed";
    String fail = "lambda-status=failed";


    //WebElement btn_continue = driver.findElement(By.xpath("//button[contains(.,'')]"));
    //WebElement list_birthday = driver.findElement(By.xpath("//input[@formcontrolname='birthDate']"));
    //WebElement btn_previous_24_years = driver.findElement(By.xpath("//button[@aria-label='Previous 24 years']"));
    //WebElement input_birthyear = driver.findElement(By.xpath("//td[@aria-label='1965']/div"));
    //WebElement input_birthmonth = driver.findElement(By.xpath("//td[contains(@aria-label, 'June')]/div"));
    //WebElement input_birthday = driver.findElement(By.xpath("//td[contains(@aria-label, 'June 19')]/div"));
    //WebElement input_email = driver.findElement(By.xpath("//input[@formcontrolname='email']"));
    //WebElement list_isdriver = driver.findElement(By.xpath("//mat-select[@formcontrolname='isMainDriver']"));
    //WebElement opt_isdriver_yes = driver.findElement(By.xpath("//mat-option[contains(.,'Yes')]"));
    //WebElement opt_isdriver_no = driver.findElement(By.xpath("//mat-option[contains(.,'No')]"));



    public void click_btn_continue() throws InterruptedException {
        //Thread.sleep(3000);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(btn_to_next_page));
        //btn_to_next_page.click();
        //driver.hideKeyboard();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_to_next_page);
        //driver.hideKeyboard();
    }

    public void input_birthday(String year, String month, String day) throws InterruptedException {
        //driver.executeScript("lambda-status==failed");
        //Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try {
            //System.setProperty("appLanguage", "nl");
            if (System.getProperty("appLanguage").equals("en")) {
                //logger.debug("birthdy month language: " + System.getProperty("language"));
                list_birthday.click();
                //btn_previous_24_years.click();
                input_birthyear.click();
                input_birthmonth_en.click();
                input_birthday_en.click();


            } else if (System.getProperty("appLanguage").equals("nl")) {
                //logger.debug("birthdy month language: " + System.getProperty("language"));
                list_birthday.click();
                //btn_previous_24_years.click();
                input_birthyear.click();
                //Thread.sleep(1000);
                wait.until(ExpectedConditions.visibilityOf(input_birthmonth_nl));
                input_birthmonth_nl.click();
                //Thread.sleep(1000);
                wait.until(ExpectedConditions.visibilityOf(input_birthday_nl));
                input_birthday_nl.click();
            } else {
                //logger.debug("birthdy month language: " + System.getProperty("language"));
                list_birthday.click();
                //btn_previous_24_years.click();
                input_birthyear.click();
                //Thread.sleep(1000);
                wait.until(ExpectedConditions.visibilityOf(input_birthmonth));
                input_birthmonth.click();
                //Thread.sleep(1000);
                wait.until(ExpectedConditions.visibilityOf(input_birthday));
                input_birthday.click();
            }
        }catch(Exception e) {
            logger.debug("input bitrhdat niet gelukt");
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(fail);
        }
    }

    public void input_email() {
        input_email.sendKeys("a.antonisse@rentasolutions.org");
        //driver.hideKeyboard;


    }

    public void select_isdriver() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        //Thread.sleep(1000);
        wait.until(ExpectedConditions.visibilityOf(list_isdriver));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", list_isdriver);
        //list_isdriver.click();
        Thread.sleep(1000);
        if(System.getProperty("appLanguage").equals("fr")) {
            opt_isdriver_yes_fr.click();
        } else {
            opt_isdriver_yes.click();
            //logger.debug("op isDriver yes geklikt");


        }
        //Thread.sleep(1000);
        //((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_to_next_page);
        //btn_to_next_page.click();
        //Thread.sleep(1000);
    }


    public String getDriverBirthday() {
        return driverBirthday;
    }

    public void setDriverBirthday(String driverBirthday) {
        this.driverBirthday = driverBirthday;
    }
}
