package pageobjects.android;

//import io.appium.java_client.AppiumBy;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.testng.Assert;
import utils.AndroidActions;
import utils.AppiumUtils;

import java.util.ArrayList;
import java.util.List;


//import static jdk.internal.net.http.common.Utils.getIntegerProperty;


//import static org.openqa.selenium.support.PageFactory.initElements;

public class Welcome extends AndroidActions {
    final Logger logger = LogManager.getLogger(Welcome.class);
    AppiumDriver driver;
    //public final static int IMPLICIT_WAIT = getIntegerProperty("implicitWait", 30);

    public Welcome(AppiumDriver driver) {
    super(driver);
    this.driver = driver;
        AppiumFieldDecorator ad = new AppiumFieldDecorator(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

        try {
            //PageFactory.initElements(driver, this);
            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
            //PageFactory.initElements(new AppiumFieldDecorator(driver, IMPLICIT_WAIT, TimeUnit.SECONDS), this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    @FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='welcome-language-selector-form-field']")
    private WebElement list_languages;

    @FindBy(how = How.XPATH, using = "//mat-option[@role='option']")
    public List<WebElement> language_list;

    @FindBy(how = How.XPATH, using = "//mat-option[@role='option']/span[contains(.,'Nederlands')]")
    public WebElement language_nederlands;

    @FindBy(how = How.XPATH, using = "//mat-option[@role='option']/span[contains(.,'English')]")
    public WebElement language_english;

    @FindBy(how = How.XPATH, using = "//mat-option[@role='option']/span[contains(.,'Deutsch')]")
    public WebElement language_deutsch;

    @FindBy(how = How.XPATH, using = "//mat-option[@role='option']/span[contains(.,'Français')]")
    public WebElement language_francais;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='welcome-continue-btn']")
    public WebElement btn_continue;



    //WebElement btn_continue = driver.findElement(By.xpath("//button[contains(.,'')]"));
    //WebElement list_birthday = driver.findElement(By.xpath("//input[@formcontrolname='birthDate']"));
    //WebElement btn_previous_24_years = driver.findElement(By.xpath("//button[@aria-label='Previous 24 years']"));
    //WebElement input_birthyear = driver.findElement(By.xpath("//td[@aria-label='1965']/div"));
    //WebElement input_birthmonth = driver.findElement(By.xpath("//td[contains(@aria-label, 'June')]/div"));
    //WebElement input_birthday = driver.findElement(By.xpath("//td[contains(@aria-label, 'June 19')]/div"));
    //WebElement input_email = driver.findElement(By.xpath("//input[@formcontrolname='email']"));
    //WebElement list_isdriver = driver.findElement(By.xpath("//mat-select[@formcontrolname='isMainDriver']"));
    //WebElement opt_isdriver_yes = driver.findElement(By.xpath("//mat-option[contains(.,'Yes')]"));
    //WebElement opt_isdriver_no = driver.findElement(By.xpath("//mat-option[contains(.,'No')]"));


    String pass = "lambda-status=passed";
    String fail = "lambda-status=failed";

public void selectLanguage() {
    list_languages.click();
    List<WebElement> elementList = language_list;
    Assert.assertTrue(language_list.size() == 4);
    List<String> langList = new ArrayList<>();
    langList.add("English");
    langList.add("Nederlands");
    langList.add("Français");
    langList.add("Deutsch");

    for (int i = 0; i < language_list.size(); i++) {
        for (int j = 0; j < langList.size(); j++) {
            if(language_list.get(i).getText().equals(langList.get(j).toString())) {
                String removedStr = langList.remove(j);

                //logger.debug("langList in if in for: " + langList);
            }

        }

    }
    logger.debug("langList: " + langList);
    //Assert.assertTrue(langList.size() == 0);
        try {
            String appLang = System.getProperty("appLanguage");
            //appLang = "nl"; //Alleen voor geisoleerd MobileBrowserTest, anders uitzetten
            //System.setProperty("appLanguage", appLang);
            //logger.debug("language voor pdf: " + appLang);
            if (appLang.equals("nl")) {
                language_nederlands.click();
            } else if (appLang.equals("en")) {
                language_english.click();
            } else if (appLang.equals("de")) {
                language_deutsch.click();
            } else if (appLang.equals("fr")) {
                language_francais.click();
            }
        } catch(Exception e) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(fail);
        }

    btn_continue.click();
    }



}
