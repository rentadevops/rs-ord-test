package pageobjects.android;

//import io.appium.java_client.AppiumBy;

import apiEngine.model.post.order.imports.OptionCsv;
import cucumber.TestContext;
import helpers.AssertHelper;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pageobjects.ui.ORD_Order_Page;
import utils.AndroidActions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//import static jdk.internal.net.http.common.Utils.getIntegerProperty;


//import static org.openqa.selenium.support.PageFactory.initElements;

public class OptionsList extends AndroidActions {
    final Logger logger = LogManager.getLogger(OptionsList.class);
    AppiumDriver driver;
    TestContext testContext;
    ORD_Order_Page orderPage;
    //PostOrderImportsRequest postOrderImportsRequest;
    //public final static int IMPLICIT_WAIT = getIntegerProperty("implicitWait", 30);
    AssertHelper ah = new AssertHelper();
    List<WebElement> images;
    List<List<String>> records;
    List<String> recordsDel;
    List<String> recordsDelCopy;
    List<List<String>> recordsCopy;
    List<WebElement> imagesCopy;


    public OptionsList(AppiumDriver driver) {
    super(driver);
    this.driver = driver;
        AppiumFieldDecorator ad = new AppiumFieldDecorator(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

        try {
            //PageFactory.initElements(driver, this);
            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
            //PageFactory.initElements(new AppiumFieldDecorator(driver, IMPLICIT_WAIT, TimeUnit.SECONDS), this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FindBy(how = How.XPATH, using = "//h1")
    private WebElement we_lbl_title;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='new-vehicle-next-page-btn']")
    private WebElement we_btn_new_vehicle_next;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='option-list-next-page-btn']")
    private WebElement we_btn_options_next;

    @FindBy(how = How.XPATH, using = "//button[@routerlink='/confirmation']")
    private WebElement we_btn_options_next_fr;


    String pass = "lambda-status=passed";
    String fail = "lambda-status=failed";


public void validate_option_list() throws IOException, InterruptedException {

    Thread.sleep(2000);
    if (System.getProperty("appLanguage").equals("nl")) {
        System.out.println("Start validate option list nederlands");
        ah.assertEquals("Title: ", "Lijst met opties", we_lbl_title.getText());

        OptionCsv oc = new OptionCsv();
        records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("./src/test/resources/runtime/Option.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                records.add(Arrays.asList(values));
            }
            logger.debug("Record 1: " + records.get(1));
        }

        recordsDel = new ArrayList<>();

        try {
            images = driver.findElements(AppiumBy.tagName("li"));
            logger.debug("images size: " + images.size());
        } catch (UnreachableBrowserException e) {

            logger.debug(e.toString());
        }
        //List<WebElement> images = driver.findElements(AppiumBy.className("ng-star-inserted"));
        //System.out.println("images size: " + images.size());
        for (int i = 0; i < records.size(); i++) {
            recordsDel.add(records.get(i).get(3));

        }
        recordsDelCopy = recordsDel;
        imagesCopy = images;
        if (imagesCopy.equals(recordsDelCopy) == true) {
            logger.debug(" Array Lists are equal");
        } else {
            recordsDelCopy.retainAll(imagesCopy);
            logger.debug(" images: " + imagesCopy.toString());
            logger.debug(" recordsDel: " + recordsDelCopy.toString());
        }
        if (images.equals(recordsDel) == true) {
            logger.debug(" Array List are equal");
        } else {
            images.retainAll(recordsDel);
            logger.debug(" images: " + images.toString());
            logger.debug(" recordsDel: " + recordsDel.toString());
        }
        ah.assertTrue("", images.size() == 0);
        ah.assertTrue("", recordsDel.size() == 0);

        logger.debug(ah.processAllAssertions());


        //ah.processAllAssertions();
        //btn_new_vehicle_next.click();
        we_btn_options_next.click();

    } else if (System.getProperty("appLanguage").equals("en")) {
        logger.debug("Start validate option list engels");
        ah.assertEquals("Title: ", "Options list", we_lbl_title.getText());

        OptionCsv oc = new OptionCsv();
        records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("./src/test/resources/runtime/Option.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                records.add(Arrays.asList(values));
            }
            logger.debug("Record 1: " + records.get(1));
        }

        recordsDel = new ArrayList<>();

        try {
            images = driver.findElements(AppiumBy.tagName("li"));
            logger.debug("images size: " + images.size());
        } catch (UnreachableBrowserException e) {

            logger.debug(e.toString());
        }
        //List<WebElement> images = driver.findElements(AppiumBy.className("ng-star-inserted"));
        //System.out.println("images size: " + images.size());
        for (int i = 0; i < records.size(); i++) {
            recordsDel.add(records.get(i).get(3));

        }
        recordsDelCopy = recordsDel;
        imagesCopy = images;
        if (imagesCopy.equals(recordsDelCopy) == true) {
            logger.debug(" Array Lists are equal");
        } else {
            recordsDelCopy.retainAll(imagesCopy);
            logger.debug(" images: " + imagesCopy.toString());
            logger.debug(" recordsDel: " + recordsDelCopy.toString());
        }
        if (images.equals(recordsDel) == true) {
            logger.debug(" Array List are equal");
        } else {
            images.retainAll(recordsDel);
            logger.debug(" images: " + images.toString());
            logger.debug(" recordsDel: " + recordsDel.toString());
        }
        ah.assertTrue("", images.size() == 0);
        ah.assertTrue("", recordsDel.size() == 0);

        logger.debug(ah.processAllAssertions());


    }



    logger.debug(ah.processAllAssertions());
    Thread.sleep(1000);
    try {
        we_btn_options_next.click();
    } catch(Exception e) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(fail);
    }
    if (System.getProperty("appLanguage").equals("fr")) {
        System.out.println("Start validate option list frans");
        ah.assertEquals("Title: ", "Liste des options", we_lbl_title.getText());
        logger.debug(ah.processAllAssertions());
        Thread.sleep(1000);
        //we_btn_new_vehicle_next.click();
        //swipeAction(we_btn_options_next, "UP");
        //scrollToEndAction();
        Thread.sleep(1000);
        //we_btn_options_next_fr.click();
    }

}
    }




