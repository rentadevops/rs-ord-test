package pageobjects.android;

//import io.appium.java_client.AppiumBy;

import cucumber.TestContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pageobjects.ui.ORD_Order_Details_Page;
import pageobjects.ui.ORD_Order_Page;
import utils.AndroidActions;
import utils.AppiumUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


//import static jdk.internal.net.http.common.Utils.getIntegerProperty;


//import static org.openqa.selenium.support.PageFactory.initElements;

public class Vehicle extends AndroidActions {
    final Logger logger = LogManager.getLogger(Vehicle.class);
    AppiumDriver driver;
    TestContext testContext;
    ORD_Order_Page orderPage;
    String licensePlate;
    //public final static int IMPLICIT_WAIT = getIntegerProperty("implicitWait", 30);

    public Vehicle(AppiumDriver driver) {
    super(driver);
    this.driver = driver;
        AppiumFieldDecorator ad = new AppiumFieldDecorator(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

        try {
            //PageFactory.initElements(driver, this);
            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
            //PageFactory.initElements(new AppiumFieldDecorator(driver, IMPLICIT_WAIT, TimeUnit.SECONDS), this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    @FindBy(how = How.XPATH, using = "//input[@placeholder='License plate']")
    private WebElement input_license_plate;

    @FindBy(how = How.XPATH, using = "//input[@placeholder='Nummerplaat']")
    private WebElement input_license_plate_nl;

    @FindBy(how = How.XPATH, using = "//input[@placeholder=\"Plaque d'immatriculation\"]")
    private WebElement input_license_plate_fr;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='license-plate-next-page-btn']")
    private WebElement btn_license_plate_next;

    String pass = "lambda-status=passed";
    String fail = "lambda-status=failed";

public void input_license_plate() throws IOException, InterruptedException {
    try {
        Thread.sleep(3000);
        Properties properties = new Properties();
        InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/licenseplate.properties");
        properties.load(inputDriver);
        licensePlate = properties.getProperty("LicensePlate");
        //System.setProperty("appLanguage", "en"); //only activate when execute script standalone
        if (System.getProperty("appLanguage").equals("fr")) {
            input_license_plate_fr.sendKeys(licensePlate);
        } else if (System.getProperty("appLanguage").equals("nl")) {
            input_license_plate_nl.sendKeys(licensePlate);
        } else {
            input_license_plate.click();
            Thread.sleep(1000);
            input_license_plate.sendKeys(licensePlate);
        }
        //AndroidActions aa = new AndroidActions(driver);
        //aa.scrollToEndAction();
        //btn_license_plate_next.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_license_plate_next);
        //driver.hideKeyboard();

    } catch (Exception e) {
        logger.debug("input licenseplate niet gelukt");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(fail);
    }

}
}
