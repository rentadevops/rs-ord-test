package pageobjects.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.model.get.leasing.company.actor.Address;
import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.order.detail.Agent;
import apiEngine.model.get.order.detail.Buyback;
import apiEngine.model.get.order.detail.Client;
import apiEngine.model.get.order.detail.Dealer;
import apiEngine.model.get.order.detail.Delivery;
import apiEngine.model.get.order.detail.DossierManager;
import apiEngine.model.get.order.detail.Driver;
import apiEngine.model.get.order.detail.FinancialLeaseRegistrationInfo;
import apiEngine.model.get.order.detail.InscriptionCosts;
import apiEngine.model.get.order.detail.Insurance;
import apiEngine.model.get.order.detail.LastComment;
import apiEngine.model.get.order.detail.LeasingCompany;
import apiEngine.model.get.order.detail.OptionDTO;
import apiEngine.model.get.order.detail.OrderDetail;
import apiEngine.model.get.order.detail.Pickup;
import apiEngine.model.get.order.detail.Pricing;
import apiEngine.model.get.order.detail.Product;
import apiEngine.model.get.order.detail.PromotionDTO;
import apiEngine.model.get.order.detail.TaxStampCosts;
import apiEngine.model.get.order.detail.TyreFitter;
import apiEngine.model.get.order.detail.Vehicle;
import apiEngine.model.get.supplier.actor.SupplierActor;




public class API_GetSupplierActorById {
	final Logger logger = LogManager.getLogger(API_GetSupplierActorById.class);

	
	private Address address;
	private String commercialName;
	private String contactEmail;
	private String contactFullName;
	private String contactMobileNumber;
	private String contactPhoneNumber;
	private String enterpriseNumber;
	private String invoiceName;
	private String language;
	private String rsNumber;
	private String street;
	private String houseNr;
	private String zipcode;
	private String city;
	private String country;
	private String billingAddress;

	
	public API_GetSupplierActorById(SupplierActor supplierActor) {
		
	}
	
	public void get_supplier_actor_by_id_data(SupplierActor supplierActor) throws ParseException {
		
		invoiceName = supplierActor.getInvoiceName(); //Name registered office
		enterpriseNumber = supplierActor.getEnterpriseNumber();
		Integer rsNumberInt = supplierActor.getRsNumber();
		rsNumber = rsNumberInt.toString();
		street = supplierActor.getAddress().getStreet();
		houseNr = supplierActor.getAddress().getHouseNumber();
		zipcode = supplierActor.getAddress().getZipCode().getValue();
		city = supplierActor.getAddress().getCity();
		country = supplierActor.getAddress().getCountry();
		billingAddress = street + " " + houseNr + " , " + zipcode + " " + city;
		logger.debug("billingAddress: " + billingAddress);
		contactMobileNumber = supplierActor.getContactMobileNumber();
		contactPhoneNumber = supplierActor.getContactPhoneNumber();
		language = supplierActor.getLanguage();
		commercialName = supplierActor.getCommercialName();
		contactFullName = supplierActor.getContactFullName();
		contactEmail = supplierActor.getContactEmail();


	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactFullName() {
		return contactFullName;
	}

	public void setContactFullName(String contactFullName) {
		this.contactFullName = contactFullName;
	}

	public String getContactMobileNumber() {
		return contactMobileNumber;
	}

	public void setContactMobileNumber(String contactMobileNumber) {
		this.contactMobileNumber = contactMobileNumber;
	}

	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getEnterpriseNumber() {
		return enterpriseNumber;
	}

	public void setEnterpriseNumber(String enterpriseNumber) {
		this.enterpriseNumber = enterpriseNumber;
	}

	public String getInvoiceName() {
		return invoiceName;
	}

	public void setInvoiceName(String invoiceName) {
		this.invoiceName = invoiceName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getRsNumber() {
		return rsNumber;
	}

	public void setRsNumber(String rsNumber) {
		this.rsNumber = rsNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNr() {
		return houseNr;
	}

	public void setHouseNr(String houseNr) {
		this.houseNr = houseNr;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	
}
		
		