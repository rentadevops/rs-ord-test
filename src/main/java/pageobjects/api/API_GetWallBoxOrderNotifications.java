package pageobjects.api;

import apiEngine.model.get.order.VehicleModel;
//import apiEngine.model.get.order.notifications.feed.OrderNotificationFeed;
//import apiEngine.model.get.order.notifications.feed.OrderNotificationsFeed;
import apiEngine.model.get.order.notifications.feed.OrderNotificationFeed;
import apiEngine.model.get.order.notifications.wallbox.WallBoxOrderNotification;
import apiEngine.model.get.order.notifications.wallbox.WallBoxOrderNotifications;
import apiEngine.model.get.sftp.order.notification.OrderNotification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class API_GetWallBoxOrderNotifications {
	final Logger logger = LogManager.getLogger(API_GetWallBoxOrderNotifications.class);

	public Integer id;
	public String orderId;
	public Integer orderVersion;
	public Integer userId;
	public String leasingCompanyDossierNumber;
	public Object dealerDossierNumber;
	public Long orderDate;
	public String orderStatus;
	public String dealerName;
	public Integer dealerNumber;
	public Object agent;
	public String leasingCompanyName;
	public Integer leasingCompanyNumber;
	public String leasingCompanyCountry;
	public String clientName;
	public String driverName;
	public String vehicleMake;
	public VehicleModel vehicleModel;
	public Long leasingCompanyDueDate;
	public Long dealerDueDate;
	public Boolean leasingCompanyHasUnseenMessages;
	public Boolean dealerHasUnseenMessages;
	public Long leasingCompanyRank;
	public Long dealerRank;
	public Object busyUntil;
	public Boolean alreadyOrdered;
	public Object licensePlate;
	public Boolean leasingCompanyHasUnseenDocuments;
	public Boolean dealerHasUnseenDocuments;
	public String orderType;
	public Boolean modified;
	public Boolean leasingCompanyActionNeeded;
	public Boolean dealerActionNeeded;
	public Boolean beingProcessed;
	OrderNotificationFeed orderNotification;
	WallBoxOrderNotification wallBoxOrderNotification;
	public String lastId;
	public Integer Id;
	public Integer lastIdInt;

		public API_GetWallBoxOrderNotifications(WallBoxOrderNotifications wallBoxOrderNotifications) {
			
		}
		
		public WallBoxOrderNotification get_wall_box_order_notifications(List<API_GetOrderNotifications> wallBoxOrderNotifications) throws IOException {
			
			for(int i = 0;i < wallBoxOrderNotifications.size(); i++) {
				logger.debug("id: " + wallBoxOrderNotifications.get(i).getId());
				//if(dosNr.equals(orderList.get(i).leasingCompanyDossierNumber.toString())) {
				//	order = orderList.get(i);
				//	orderId = orderList.get(i).orderId.toString();
				//}
			}

			WallBoxOrderNotification wallBoxOrderNotification = new WallBoxOrderNotification();
			//wallBoxOrderNotification = wallBoxOrderNotifications.get(0);
			lastId = wallBoxOrderNotification.getDealerAcceptedNotification().toString();
			Properties p = new Properties();
	        p.put("LastId", lastId);
	        
	        File fDir = new File("./src/test/resources/runtime/feedlastid.properties");
			Boolean b = fDir.mkdirs();
			logger.debug("fDir.mkdirs(): " + b);
			
	        
	        File f = new File("./src/test/resources/runtime/feedlastid.properties");
	        f.createNewFile();
	        FileOutputStream fos = new FileOutputStream(f, false);
	        p.store(fos, "");
	        logger.debug("lastId " + lastId +  " naar proprty file geschreven");
	        logger.debug("orderNotification 0: " + orderNotification);
			return wallBoxOrderNotification;
			

						
		}

	public WallBoxOrderNotification get_wall_box_order_notifications_feed(List<WallBoxOrderNotification> wallBoxOrderNotifications) throws IOException {

		for(int i = 0;i < wallBoxOrderNotifications.size(); i++) {
			logger.debug("id: " + wallBoxOrderNotifications.get(i).getOrderedNotification());
			//if(dosNr.equals(orderList.get(i).leasingCompanyDossierNumber.toString())) {
			//	order = orderList.get(i);
			//	orderId = orderList.get(i).orderId.toString();
			//}
		}

		wallBoxOrderNotification = wallBoxOrderNotifications.get(0);
		//lastId = wallBoxOrderNotification.getId().toString();
		Properties p = new Properties();
		p.put("LastId", lastId);

		File fDir = new File("./src/test/resources/runtime/feedlastid.properties");
		Boolean b = fDir.mkdirs();
		logger.debug("fDir.mkdirs(): " + b);


		File f = new File("./src/test/resources/runtime/feedlastid.properties");
		f.createNewFile();
		FileOutputStream fos = new FileOutputStream(f, false);
		p.store(fos, "");
		logger.debug("lastId " + lastId +  " naar proprty file geschreven");
		logger.debug("WallBoxOrderNotification 0: " + wallBoxOrderNotification);
		return wallBoxOrderNotification;



	}
		
public String get_order_notifications_last_id(List<OrderNotificationFeed> orderNotifications, String propId) throws IOException {
			lastIdInt = Integer.parseInt(propId);
			logger.debug("orderNotifications size: " + orderNotifications.size());
			for(int i = 0;i < orderNotifications.size(); i++) {
				logger.debug("id: " + orderNotifications.get(i).getId());
				
				Id = orderNotifications.get(i).getId();
				if(Id > lastIdInt) {
					lastIdInt = Id;
				}
				//if(dosNr.equals(orderList.get(i).leasingCompanyDossierNumber.toString())) {
				//	order = orderList.get(i);
				//	orderId = orderList.get(i).orderId.toString();
				//}
			}

			orderNotification = orderNotifications.get(0);
			//logger.debug("ordernotification desiredDeliveryDate: " + orderNotifications.get(0).getDelivery().getDesiredDeliveryDate());
			lastId = lastIdInt.toString();
			logger.debug("lastId: " + lastId);
			Properties p = new Properties();
	        p.put("LastId", lastId);
	        File f = new File("./src/test/resources/runtime/feedlastid.properties");
	        f.createNewFile();
	        FileOutputStream fos = new FileOutputStream(f, false);
	        p.store(fos, "");
	        logger.debug("lastId " + lastId +  " naar proprty file geschreven");
			return lastId;
			

						
		}

		public OrderNotificationFeed getOrderNotification() {
			return orderNotification;
		}
		
		public Integer getId() {
			return id;
		}

	}





