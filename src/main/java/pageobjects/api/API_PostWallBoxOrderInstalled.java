package pageobjects.api;

import apiEngine.Endpoints;
import apiEngine.model.post.wallbox.status.Installed;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import jakarta.xml.bind.JAXBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

//import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class API_PostWallBoxOrderInstalled {
	final Logger logger = LogManager.getLogger(API_PostWallBoxOrderInstalled.class);

	private String xmlns;
	private String dosNr;


	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());
	//String expectedInstallationDate = dateToday;
	String remoteDir;
	String id;
	String status;


	//String dateTodayMinus3Months = simpleDateFormat.format(ldt);

	String evbNumber;
	String chargeCardNumber;
	Boolean cardSent;
	String installationDate;
	String certificationDate;
	Installed installed;


		public API_PostWallBoxOrderInstalled() {
			
		}
		
		public void postWallBoxOrderInstalled(String id) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException {
			id = System.getProperty("wallBoxOrderId");
			logger.debug("orderId in post_wallbox_order_accepted: " + id);

			evbNumber = "EVB-NUMBER-0001";
			setEvbNumber(evbNumber);
			chargeCardNumber = "CCN-NUMBER-901902903904905906";
			setChargeCardNumber(chargeCardNumber);
			cardSent = true;
			setCardSent(cardSent);
			installationDate = dateToday;
			setInstallationDate(installationDate);
			certificationDate = dateToday;
			setCertificationDate(certificationDate);
			installed = new Installed();
			installed.setEvbNumber(evbNumber);
			installed.setChargeCardNumber(chargeCardNumber);
			installed.setCardSent(cardSent);
			installed.setInstallationDate(installationDate);
			installed.setCertificationDate(certificationDate);
			logger.debug("postWallBoxOrderInstalled installed:" + installed);

			status = Endpoints.postWallBoxOrderInstalled(id, installed);
			logger.debug("response postWallBoxOrderInstalled:" + status);
			Assert.assertEquals(status.trim(), "HTTP/1.1 200");
			Thread.sleep(5000);

			}

	public String getEvbNumber() {
		logger.debug("EvbNumber in getter postWallBoxInstalled:" + evbNumber);
		return evbNumber;
	}

	public void setEvbNumber(String evbNumber) {
		this.evbNumber = evbNumber;
	}

	public String getChargeCardNumber() {
		return chargeCardNumber;
	}

	public void setChargeCardNumber(String chargeCardNumber) {
		this.chargeCardNumber = chargeCardNumber;
	}

	public Boolean getCardSent() {
		return cardSent;
	}

	public void setCardSent(Boolean cardSent) {
		this.cardSent = cardSent;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}

	public String getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(String certificationDate) {
		this.certificationDate = certificationDate;
	}
}





