package pageobjects.api;

import apiEngine.Endpoints;
import apiEngine.model.post.wallbox.status.Installed;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import jakarta.xml.bind.JAXBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

//import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class API_PostWallBoxOrderReadyToActivate {
	final Logger logger = LogManager.getLogger(API_PostWallBoxOrderReadyToActivate.class);

	private String xmlns;
	private String dosNr;


	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());
	//String expectedInstallationDate = dateToday;
	String remoteDir;
	String status;

		public API_PostWallBoxOrderReadyToActivate() {
			
		}
		
		public void postWallBoxOrderReadyToActivate(String id) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException {
			//id = System.getProperty("wallBoxOrderId");
			//logger.debug("orderId in post_wallbox_order_accepted: " + id);



			status = Endpoints.postWallBoxOrderReadyToActivate(id);
			logger.debug("response postWallBoxOrderReadyToActivate:" + status);
			Assert.assertEquals(status.trim(), "HTTP/1.1 200");
			Thread.sleep(5000);

			}

}





