package pageobjects.api;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.model.get.order.Order;
import apiEngine.model.get.order.OrderList;
import apiEngine.model.get.order.VehicleModel;

public class API_GetOrderByDossierNumber {
	final Logger logger = LogManager.getLogger(API_GetOrderByDossierNumber.class);
	
	public Integer version;
	public String orderId;
	public Integer orderVersion;
	public Integer userId;
	public String leasingCompanyDossierNumber;
	public Object dealerDossierNumber;
	public Long orderDate;
	public String orderStatus;
	public String dealerName;
	public Integer dealerNumber;
	public Object agent;
	public String leasingCompanyName;
	public Integer leasingCompanyNumber;
	public String leasingCompanyCountry;
	public String clientName;
	public String driverName;
	public String vehicleMake;
	public VehicleModel vehicleModel;
	public Long leasingCompanyDueDate;
	public Long dealerDueDate;
	public Boolean leasingCompanyHasUnseenMessages;
	public Boolean dealerHasUnseenMessages;
	public Long leasingCompanyRank;
	public Long dealerRank;
	public Object busyUntil;
	public Boolean alreadyOrdered;
	public Object licensePlate;
	public Boolean leasingCompanyHasUnseenDocuments;
	public Boolean dealerHasUnseenDocuments;
	public String orderType;
	public Boolean modified;
	public Boolean leasingCompanyActionNeeded;
	public Boolean dealerActionNeeded;
	public Boolean beingProcessed;
	Order order;

		public API_GetOrderByDossierNumber(OrderList orderList) {
			
		}
		
		public Order get_order_by_dossiernumber(List<Order> orderList, String dosNr) {

			logger.debug("OrderList size  = " + orderList.size());
			for(int i = 0;i < orderList.size(); i++) {
				logger.debug("dosNr: " + dosNr + " leasingCompanyDossierNumber: " + orderList.get(i).leasingCompanyDossierNumber.toString());
				if(dosNr.equals(orderList.get(i).leasingCompanyDossierNumber.toString())) {
					order = orderList.get(i);
					orderId = order.getOrderId();
					logger.debug("Get orderId " + orderId);
				}
			}
			

			return order;
			

						
		}

		public Order getOrder() {
			return order;
		}


	public String getOrderId() {
			logger.debug("Get orderId in getter API_GetOrderByDossierNumber: " + orderId);
			return orderId;
		}


	}





