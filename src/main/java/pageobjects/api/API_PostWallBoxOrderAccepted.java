package pageobjects.api;

import apiEngine.Endpoints;
import apiEngine.model.post.order.imports.*;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import common.SftpConnect;
import helpers.ORD_ContractNumberGenerator;
import jakarta.xml.bind.JAXBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;
import java.io.*;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class API_PostWallBoxOrderAccepted {
	final Logger logger = LogManager.getLogger(API_PostWallBoxOrderAccepted.class);

	private String xmlns;
	private String dosNr;


	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());
	String expectedInstallationDate = dateToday;
	String remoteDir;
	String id;
	String status;


		public API_PostWallBoxOrderAccepted() {
			
		}
		
		public void postWallBoxOrderAccepted(String id) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException {
			String json = "{\n" +
					"  \"expectedInstallationDate\": \"" + expectedInstallationDate +"\"\n" +
					"}";
			status = Endpoints.postWallBoxOrderAccepted(id, json);
			logger.debug("response postWallBoxOrderAccepted:" + status);
			Assert.assertEquals(status.trim(), "HTTP/1.1 200");
			Thread.sleep(5000);

			}

	public String getExpectedInstallationDate() {
		return expectedInstallationDate;
	}

	public void setExpectedInstallationDate(String expectedInstallationDate) {
		this.expectedInstallationDate = expectedInstallationDate;
	}
}





