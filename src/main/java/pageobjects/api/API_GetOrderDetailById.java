package pageobjects.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.model.get.order.detail.Agent;
import apiEngine.model.get.order.detail.Buyback;
import apiEngine.model.get.order.detail.Client;
import apiEngine.model.get.order.detail.Dealer;
import apiEngine.model.get.order.detail.Delivery;
import apiEngine.model.get.order.detail.DossierManager;
import apiEngine.model.get.order.detail.Driver;
import apiEngine.model.get.order.detail.FinancialLeaseRegistrationInfo;
import apiEngine.model.get.order.detail.InscriptionCosts;
import apiEngine.model.get.order.detail.Insurance;
import apiEngine.model.get.order.detail.LastComment;
import apiEngine.model.get.order.detail.LeasingCompany;
import apiEngine.model.get.order.detail.MobileNumber;
import apiEngine.model.get.order.detail.MobileNumber__1;
import apiEngine.model.get.order.detail.OptionDTO;
import apiEngine.model.get.order.detail.OrderDetail;
import apiEngine.model.get.order.detail.Pickup;
import apiEngine.model.get.order.detail.Pricing;
import apiEngine.model.get.order.detail.Product;
import apiEngine.model.get.order.detail.PromotionDTO;
import apiEngine.model.get.order.detail.TaxStampCosts;
import apiEngine.model.get.order.detail.TyreFitter;
import apiEngine.model.get.order.detail.Vehicle;





public class API_GetOrderDetailById {
	final Logger logger = LogManager.getLogger(API_GetOrderDetailById.class);

	
	Agent agent;
	Boolean alreadyOrdered;
	Boolean beingProcessed;
	Integer busyUntil;
	Buyback buyback;
	Client client;
	Dealer dealer;
	Boolean dealerActionNeeded;
	String dealerDossierNumber;
	Integer dealerDueDate;
	Boolean dealerHasUnseenDocuments;
	Boolean dealerHasUnseenMessages;
	Delivery delivery;
	DossierManager dossierManager;
	Driver driver;
	FinancialLeaseRegistrationInfo financialLeaseRegistrationInfo;
	InscriptionCosts inscriptionCosts;
	Insurance insurance;
	Integer invoiceDate;
	Boolean invoiced;
	LastComment lastComment;
	LeasingCompany leasingCompany;
	Boolean leasingCompanyActionNeeded;
	String leasingCompanyDossierNumber;
	Integer leasingCompanyDueDate;
	Boolean leasingCompanyHasUnseenDocuments;
	Boolean leasingCompanyHasUnseenMessages;
	String leasingType;
	String linkedRegistrationId;
	Boolean modified;
	List<OptionDTO> optionDTOs = null;
	String orderId;
	String orderStatus;
	String orderType;
	Integer orderVersion;
	Pickup pickup;
	Pricing pricing;
	Product product;
	List<PromotionDTO> promotionDTOs = null;
	String quoteReference;
	Boolean redactedGDPR;
	String registrationDivToDoBy;
	Integer reiBatchTrackingNumber;
	TaxStampCosts taxStampCosts;
	TyreFitter tyreFitter;
	Vehicle vehicle;
	Integer version;
	String versionNumber;
	
	String clientName = null;
	String clientEnterpriseNumber = null;
	String clientStreet = null;
	String clientPostalCode = null;
	String clientCity = null;
	String clientBillingAddress = null;
	String clientContactPerson = null;
	MobileNumber__1 clientMobileNumberObj = null;
	String clientMobileNumber = null;
	String clientPhoneNumber = null;
	String clientLanguage = null;
	
	String driverName;
	String driverStreet;
	String driverPostalCode;
	String driverCity;
	String driverAddress;
	String driverContactPerson;
	String driverMobileNumber;
	String driverPhoneNumber;
	String driverLanguage;
	
	String vehicleDescription;
	String vehicleBrand;
	String vehicleModel;
	String vehicleVIN;
	String vehicleVINControlNumber;
	Integer vehicleModelYear;
	String vehicleIdentificationCode;
	String vehicleLicensePlate;
	Boolean vehicleStock;
	String vehicleMileage; //wordt teruggegeven in ordertimeline api
	String vehicleLanguageDocuments;
	String vehicleKeycode;
	String vehicleStartcode;
	
	String motorType;
	String power;
	String engineCapacity;
	String nedcValue;
	String wltpValue;
	String interiorColor;
	String tyreSpecifications;
	String tyreBrand;
	String tyreType;
	String weight;
	String exteriorColor;
	
	String vehiclePrice;
	String discount;
	String deliveryCosts;
	String otherCosts;
	String totalPrice;
	
	//Delivery information
	String desiredDeliveryDate;
	String expectedDeliveryDate;
	String actualDeliveryDate;
	String firstRegistrationDate; //komt uit timeline api
	
	//Different delivery address
	String deliveryLocationName;
	String deliveryEnterpriseNumber;
	String deliveryAddressLine; //street + housenumber
	String deliveryPostalCode;
	String deliveryCity;
	String deliveryAddress;
	
	//Returned vehicle information
	Boolean returnedVehicle;
	String registeredOnTheName;
	String returnedVehicleLicenseplate;
	
	//Equipment and enclosed documents
	Boolean registrationCertificate;
	Boolean insuranceCertificate;
	Boolean certificateOfConformity;
	Boolean technicalInspectionCertificate;
	Boolean legalKit;
	Boolean serviceManual;
	Boolean fuelCard;
	Integer numberOfKeys;
	
	//Driver information
	String returnedVehicleDriverName;
	String returnedVehicleDriverDateOfBirth;
	String returnedVehicleDriverBirthplace;
	
	//Buyback
	Boolean buybackApplicable;
	String buybackName;
	String buybackAddress;
	Integer contractDuration;
	Integer totalMileage;
	Integer buybackAmount;
	

	
	public API_GetOrderDetailById(OrderDetail orderDetail) {
		
	}
	
	public void get_order_detail_by_id_data(OrderDetail orderDetail) throws ParseException, NullPointerException {
		
		Date d;
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		
		//General data	
		orderStatus = orderDetail.getOrderStatus();
		logger.debug("orderStatus: " + orderStatus);
		leasingCompanyDossierNumber = orderDetail.getLeasingCompanyDossierNumber(); //LC ordernumber
		dealerDossierNumber = orderDetail.getDealerDossierNumber();
		
		//Lessee
		clientName = orderDetail.getClient().getName();
		logger.debug("clientName: " + clientName);
		clientEnterpriseNumber = orderDetail.getClient().getVatNumber();
		logger.debug("clientEnterpriseNumber: " + clientEnterpriseNumber);
		clientStreet = orderDetail.getClient().getAddress();
		logger.debug("clientStreet: " + clientStreet);
		clientPostalCode = orderDetail.getClient().getPostalCode();
		logger.debug("clientPostalCode: " + clientPostalCode);
		clientCity = orderDetail.getClient().getCity();
		logger.debug("clientCity: " + clientCity);
		clientBillingAddress = clientStreet + " , " + clientPostalCode + " " + clientCity;
		clientContactPerson = orderDetail.getClient().getContactPerson();
		if(orderDetail.getClient().getMobileNumber() == null) {
			clientMobileNumber = null;
		} else {
			clientMobileNumber = orderDetail.getClient().getMobileNumber().getValue();
		}
		if(orderDetail.getClient().getPhoneNumber() == null) {
			clientPhoneNumber = null;
		} else {
			clientPhoneNumber = orderDetail.getClient().getMobileNumber().getValue();
		}
		clientLanguage = orderDetail.getClient().getLanguageCode();

		//Driver
		if(orderDetail.getDriver() == null) {
		
		} 
		else {
		driverName = orderDetail.getDriver().getName();
		driverStreet = orderDetail.getDriver().getAddress();
		driverPostalCode = orderDetail.getDriver().getPostalCode();
		driverCity = orderDetail.getDriver().getCity();
		driverAddress = driverStreet + " , " + driverPostalCode + " " + driverCity;
		driverContactPerson = orderDetail.getDriver().getName();
		driverMobileNumber = orderDetail.getDriver().getMobileNumber().getValue();
		driverPhoneNumber = orderDetail.getDriver().getPhoneNumber().getValue();
		driverLanguage = orderDetail.getDriver().getLanguageCode();
		}
		
		//Vehicle description
		vehicleBrand = orderDetail.getVehicle().getBrand();
		vehicleModel = orderDetail.getVehicle().getModel().getTranslations().getEn();
		vehicleDescription = vehicleBrand + " " + vehicleModel;
		vehicleVIN = orderDetail.getVehicle().getVin().getValue();
		vehicleVINControlNumber = orderDetail.getVehicle().getVinControlNumber();
		vehicleModelYear = orderDetail.getVehicle().getConstructionYear();
		vehicleIdentificationCode = orderDetail.getVehicle().getIdentification();
		vehicleLicensePlate = orderDetail.getVehicle().getLicensePlate();
		vehicleStock = orderDetail.getVehicle().getStockVehicle();
		vehicleLanguageDocuments = orderDetail.getVehicle().getLanguagePapers();
		vehicleKeycode = orderDetail.getPickup().getKeyCode();
		vehicleStartcode = orderDetail.getPickup().getStartCode();

		//Technical specifications
		if(orderDetail.getVehicle().getVersion() == null) {
			motorType = null;
		} else {
		motorType = orderDetail.getVehicle().getVersion().getTranslations().getEn();
		}
		power = orderDetail.getVehicle().getPower().toString();
		engineCapacity = orderDetail.getVehicle().getCylinderContent().toString();
		nedcValue = orderDetail.getVehicle().getCo2EmissionsNedc().toString();
		wltpValue = orderDetail.getVehicle().getCo2EmissionsWltp().toString();
		
		if(orderDetail.getVehicle().getInteriorColour() == null) {
			interiorColor = null;
		} else {
			interiorColor = orderDetail.getVehicle().getInteriorColour().getTranslations().getEn();
		}
		tyreSpecifications = orderDetail.getVehicle().getTyreDescription();
		tyreBrand = orderDetail.getVehicle().getTyreBrand();
		tyreType = orderDetail.getVehicle().getTyreType();
		weight = orderDetail.getVehicle().getWeight().toString();
		if(orderDetail.getVehicle().getExteriorColour() == null) {
			exteriorColor = null;
		} else {
			exteriorColor = orderDetail.getVehicle().getExteriorColour().getTranslations().getEn();
		}
		
		//Pricing and options
		vehiclePrice = orderDetail.getPricing().getListPrice().getValue().toString();
		if(orderDetail.getPromotionDTOs().size() > 0) {
			discount = orderDetail.getPromotionDTOs().get(0).getAmount().getValue().toString();
		} else {
			discount = null;
		}
		if(orderDetail.getPricing().getDeliveryCosts() == null) {
			deliveryCosts = null;
		} else {
			deliveryCosts = orderDetail.getPricing().getDeliveryCosts().getValue().toString();
		}

		if(orderDetail.getPricing().getOtherCosts() == null) {
			otherCosts = null;
		} else {
			otherCosts = orderDetail.getPricing().getOtherCosts().getValue().toString();
		}

		if(orderDetail.getPricing().getTotalPrice() == null) {
			totalPrice = null;
		} else {
			totalPrice = orderDetail.getPricing().getTotalPrice().getValue().toString();
		}

		
		//Delivery information
		if(orderDetail.getDelivery().getDesiredDate() == null) {
			desiredDeliveryDate = null;
		} else {
		Long desiredDeliveryDateLong = orderDetail.getDelivery().getDesiredDate();
		d = new Date( desiredDeliveryDateLong );
		desiredDeliveryDate = simpleDateFormat.format(d);
		logger.debug("desiredDeliveryDate: " + desiredDeliveryDate);
		}

		//expectedDeliveryDate
		if(orderDetail.getDelivery().getExpectedDeliveryDate() == null) {
			expectedDeliveryDate = null;
		} else {
		Long expectedDeliveryDateLong = orderDetail.getDelivery().getExpectedDeliveryDate();
		d = new Date( expectedDeliveryDateLong );
		expectedDeliveryDate = simpleDateFormat.format(d);
		logger.debug("expectedDeliveryDate: " + expectedDeliveryDate);
		}

		//actualDeliveryDate
		if(orderDetail.getDelivery().getActualDeliveryDate() == null) {
			actualDeliveryDate = null;
		} else {
		Long actualDeliveryDateLong = orderDetail.getDelivery().getActualDeliveryDate();
		d = new Date( actualDeliveryDateLong );
		actualDeliveryDate = simpleDateFormat.format(d);
		logger.debug("actualDeliveryDate: " + actualDeliveryDate);
		}
		
		
		//Delivery different address
		deliveryLocationName = orderDetail.getDelivery().getLocationName();
		deliveryEnterpriseNumber = orderDetail.getDelivery().getLocationVatNumber();
		if(orderDetail.getDelivery().getAddress() == null) {
			deliveryAddressLine = null;
			deliveryPostalCode = null;
			deliveryCity = null;
			deliveryAddress = null;
		} else {
		deliveryAddressLine = orderDetail.getDelivery().getAddress().getAddressLine();
		deliveryPostalCode = orderDetail.getDelivery().getAddress().getPostalCode();
		deliveryCity = orderDetail.getDelivery().getAddress().getCity();
		deliveryAddress = deliveryAddressLine + " , " + deliveryPostalCode + " " + deliveryCity;
		}
		
		//Returned vehicle information
		returnedVehicle = orderDetail.getPickup().getPreviousVehicleDropoff();
		registeredOnTheName = orderDetail.getPickup().getPreviousVehicleOwner();
		returnedVehicleLicenseplate = orderDetail.getPickup().getPreviousVehicleLicensePlate();
		
		//Equipment and enclosed documents
		registrationCertificate = orderDetail.getPickup().getProofOfRegistration();
		insuranceCertificate = orderDetail.getPickup().getProofOfInsurance();
		certificateOfConformity = orderDetail.getPickup().getCertificateOfConformity();
		technicalInspectionCertificate = orderDetail.getPickup().getProofOfRoadworthiness();
		legalKit = orderDetail.getPickup().getLegalKit();
		serviceManual = orderDetail.getPickup().getManual();
		fuelCard = orderDetail.getPickup().getFuelCard();
		numberOfKeys = orderDetail.getPickup().getNumberOfKeys();
		
		//Driver information
		String returnedVehicleDriverFirstName = orderDetail.getPickup().getDriverFirstName();
		String returnedVehicleDriverLastName = orderDetail.getPickup().getDriverLastName();
		returnedVehicleDriverName = returnedVehicleDriverFirstName + " " + returnedVehicleDriverLastName;
		
		//Long returnedVehicleDriverDateOfBirthLong = orderDetail.getPickup().getDriverBirthDate();
		//d = new Date( returnedVehicleDriverDateOfBirthLong );
		//returnedVehicleDriverDateOfBirth = simpleDateFormat.format(d);
		returnedVehicleDriverDateOfBirth = orderDetail.getPickup().getDriverBirthDate();
	
		returnedVehicleDriverBirthplace = orderDetail.getPickup().getDriverBirthPlace();
	
		//Buyback
		buybackApplicable = orderDetail.getBuyback().getBuyback();
		buybackName = orderDetail.getBuyback().getName();
		if(orderDetail.getBuyback().getAddress() == null) {
			String buybackAddressLine = null;
			String buybackAddressPostalcode = null;
			String buybackAddressCity = null;
			buybackAddress = null;
		} else {
		String buybackAddressLine = orderDetail.getBuyback().getAddress().getAddressLine();
		String buybackAddressPostalcode = orderDetail.getBuyback().getAddress().getPostalCode();
		String buybackAddressCity = orderDetail.getBuyback().getAddress().getCity();
		buybackAddress = buybackAddressLine + " , " + buybackAddressPostalcode + " " + buybackAddressCity;
		}
		contractDuration = orderDetail.getBuyback().getContractDuration();
		totalMileage = orderDetail.getBuyback().getKilometers();
		if(orderDetail.getBuyback().getAmount() == null) {
			buybackAmount = null;
		} else {
		buybackAmount = orderDetail.getBuyback().getAmount().getValue();
		}

	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Boolean getAlreadyOrdered() {
		return alreadyOrdered;
	}

	public void setAlreadyOrdered(Boolean alreadyOrdered) {
		this.alreadyOrdered = alreadyOrdered;
	}

	public Boolean getBeingProcessed() {
		return beingProcessed;
	}

	public void setBeingProcessed(Boolean beingProcessed) {
		this.beingProcessed = beingProcessed;
	}

	public Integer getBusyUntil() {
		return busyUntil;
	}

	public void setBusyUntil(Integer busyUntil) {
		this.busyUntil = busyUntil;
	}

	public Buyback getBuyback() {
		return buyback;
	}

	public void setBuyback(Buyback buyback) {
		this.buyback = buyback;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}

	public Boolean getDealerActionNeeded() {
		return dealerActionNeeded;
	}

	public void setDealerActionNeeded(Boolean dealerActionNeeded) {
		this.dealerActionNeeded = dealerActionNeeded;
	}

	public String getDealerDossierNumber() {
		return dealerDossierNumber;
	}

	public void setDealerDossierNumber(String dealerDossierNumber) {
		this.dealerDossierNumber = dealerDossierNumber;
	}

	public Integer getDealerDueDate() {
		return dealerDueDate;
	}

	public void setDealerDueDate(Integer dealerDueDate) {
		this.dealerDueDate = dealerDueDate;
	}

	public Boolean getDealerHasUnseenDocuments() {
		return dealerHasUnseenDocuments;
	}

	public void setDealerHasUnseenDocuments(Boolean dealerHasUnseenDocuments) {
		this.dealerHasUnseenDocuments = dealerHasUnseenDocuments;
	}

	public Boolean getDealerHasUnseenMessages() {
		return dealerHasUnseenMessages;
	}

	public void setDealerHasUnseenMessages(Boolean dealerHasUnseenMessages) {
		this.dealerHasUnseenMessages = dealerHasUnseenMessages;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public DossierManager getDossierManager() {
		return dossierManager;
	}

	public void setDossierManager(DossierManager dossierManager) {
		this.dossierManager = dossierManager;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public FinancialLeaseRegistrationInfo getFinancialLeaseRegistrationInfo() {
		return financialLeaseRegistrationInfo;
	}

	public void setFinancialLeaseRegistrationInfo(FinancialLeaseRegistrationInfo financialLeaseRegistrationInfo) {
		this.financialLeaseRegistrationInfo = financialLeaseRegistrationInfo;
	}

	public InscriptionCosts getInscriptionCosts() {
		return inscriptionCosts;
	}

	public void setInscriptionCosts(InscriptionCosts inscriptionCosts) {
		this.inscriptionCosts = inscriptionCosts;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	public Integer getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Integer invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Boolean getInvoiced() {
		return invoiced;
	}

	public void setInvoiced(Boolean invoiced) {
		this.invoiced = invoiced;
	}

	public LastComment getLastComment() {
		return lastComment;
	}

	public void setLastComment(LastComment lastComment) {
		this.lastComment = lastComment;
	}

	public LeasingCompany getLeasingCompany() {
		return leasingCompany;
	}

	public void setLeasingCompany(LeasingCompany leasingCompany) {
		this.leasingCompany = leasingCompany;
	}

	public Boolean getLeasingCompanyActionNeeded() {
		return leasingCompanyActionNeeded;
	}

	public void setLeasingCompanyActionNeeded(Boolean leasingCompanyActionNeeded) {
		this.leasingCompanyActionNeeded = leasingCompanyActionNeeded;
	}

	public String getLeasingCompanyDossierNumber() {
		return leasingCompanyDossierNumber;
	}

	public void setLeasingCompanyDossierNumber(String leasingCompanyDossierNumber) {
		this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
	}

	public Integer getLeasingCompanyDueDate() {
		return leasingCompanyDueDate;
	}

	public void setLeasingCompanyDueDate(Integer leasingCompanyDueDate) {
		this.leasingCompanyDueDate = leasingCompanyDueDate;
	}

	public Boolean getLeasingCompanyHasUnseenDocuments() {
		return leasingCompanyHasUnseenDocuments;
	}

	public void setLeasingCompanyHasUnseenDocuments(Boolean leasingCompanyHasUnseenDocuments) {
		this.leasingCompanyHasUnseenDocuments = leasingCompanyHasUnseenDocuments;
	}

	public Boolean getLeasingCompanyHasUnseenMessages() {
		return leasingCompanyHasUnseenMessages;
	}

	public void setLeasingCompanyHasUnseenMessages(Boolean leasingCompanyHasUnseenMessages) {
		this.leasingCompanyHasUnseenMessages = leasingCompanyHasUnseenMessages;
	}

	public String getLeasingType() {
		return leasingType;
	}

	public void setLeasingType(String leasingType) {
		this.leasingType = leasingType;
	}

	public String getLinkedRegistrationId() {
		return linkedRegistrationId;
	}

	public void setLinkedRegistrationId(String linkedRegistrationId) {
		this.linkedRegistrationId = linkedRegistrationId;
	}

	public Boolean getModified() {
		return modified;
	}

	public void setModified(Boolean modified) {
		this.modified = modified;
	}

	public List<OptionDTO> getOptionDTOs() {
		return optionDTOs;
	}

	public void setOptionDTOs(List<OptionDTO> optionDTOs) {
		this.optionDTOs = optionDTOs;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Integer getOrderVersion() {
		return orderVersion;
	}

	public void setOrderVersion(Integer orderVersion) {
		this.orderVersion = orderVersion;
	}

	public Pickup getPickup() {
		return pickup;
	}

	public void setPickup(Pickup pickup) {
		this.pickup = pickup;
	}

	public Pricing getPricing() {
		return pricing;
	}

	public void setPricing(Pricing pricing) {
		this.pricing = pricing;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<PromotionDTO> getPromotionDTOs() {
		return promotionDTOs;
	}

	public void setPromotionDTOs(List<PromotionDTO> promotionDTOs) {
		this.promotionDTOs = promotionDTOs;
	}

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	public Boolean getRedactedGDPR() {
		return redactedGDPR;
	}

	public void setRedactedGDPR(Boolean redactedGDPR) {
		this.redactedGDPR = redactedGDPR;
	}

	public String getRegistrationDivToDoBy() {
		return registrationDivToDoBy;
	}

	public void setRegistrationDivToDoBy(String registrationDivToDoBy) {
		this.registrationDivToDoBy = registrationDivToDoBy;
	}

	public Integer getReiBatchTrackingNumber() {
		return reiBatchTrackingNumber;
	}

	public void setReiBatchTrackingNumber(Integer reiBatchTrackingNumber) {
		this.reiBatchTrackingNumber = reiBatchTrackingNumber;
	}

	public TaxStampCosts getTaxStampCosts() {
		return taxStampCosts;
	}

	public void setTaxStampCosts(TaxStampCosts taxStampCosts) {
		this.taxStampCosts = taxStampCosts;
	}

	public TyreFitter getTyreFitter() {
		return tyreFitter;
	}

	public void setTyreFitter(TyreFitter tyreFitter) {
		this.tyreFitter = tyreFitter;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String lesseeName) {
		this.clientName = lesseeName;
	}

	public String getClientEnterpriseNumber() {
		return clientEnterpriseNumber;
	}

	public void setClientEnterpriseNumber(String clientEnterpriseNumber) {
		this.clientEnterpriseNumber = clientEnterpriseNumber;
	}

	public String getClientStreet() {
		return clientStreet;
	}

	public void setClientStreet(String clientStreet) {
		this.clientStreet = clientStreet;
	}

	public String getClientPostalCode() {
		return clientPostalCode;
	}

	public void setClientPostalCode(String clientPostalCode) {
		this.clientPostalCode = clientPostalCode;
	}

	public String getClientCity() {
		return clientCity;
	}

	public void setClientCity(String clientCity) {
		this.clientCity = clientCity;
	}

	public String getClientBillingAddress() {
		return clientBillingAddress;
	}

	public void setClientBillingAddress(String clientBillingAddress) {
		this.clientBillingAddress = clientBillingAddress;
	}

	public String getClientContactPerson() {
		return clientContactPerson;
	}

	public void setClientContactPerson(String clientContactPerson) {
		this.clientContactPerson = clientContactPerson;
	}

	public String getClientMobileNumber() {
		return clientMobileNumber;
	}

	public void setClientMobileNumber(String clientMobileNumber) {
		this.clientMobileNumber = clientMobileNumber;
	}

	public String getClientPhoneNumber() {
		return clientPhoneNumber;
	}

	public void setClientPhoneNumber(String clientPhoneNumber) {
		this.clientPhoneNumber = clientPhoneNumber;
	}

	public String getClientLanguage() {
		return clientLanguage;
	}

	public void setClientLanguage(String lesseeLanguage) {
		this.clientLanguage = clientLanguage;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverStreet() {
		return driverStreet;
	}

	public void setDriverStreet(String driverStreet) {
		this.driverStreet = driverStreet;
	}

	public String getDriverPostalCode() {
		return driverPostalCode;
	}

	public void setDriverPostalCode(String driverPostalCode) {
		this.driverPostalCode = driverPostalCode;
	}

	public String getDriverCity() {
		return driverCity;
	}

	public void setDriverCity(String driverCity) {
		this.driverCity = driverCity;
	}

	public String getDriverAddress() {
		return driverAddress;
	}

	public void setDriverAddress(String driverAddress) {
		this.driverAddress = driverAddress;
	}

	public String getDriverContactPerson() {
		return driverContactPerson;
	}

	public void setDriverContactPerson(String driverContactPerson) {
		this.driverContactPerson = driverContactPerson;
	}

	public String getDriverMobileNumber() {
		return driverMobileNumber;
	}

	public void setDriverMobileNumber(String driverMobileNumber) {
		this.driverMobileNumber = driverMobileNumber;
	}

	public String getDriverPhoneNumber() {
		return driverPhoneNumber;
	}

	public void setDriverPhoneNumber(String driverPhoneNumber) {
		this.driverPhoneNumber = driverPhoneNumber;
	}

	public String getDriverLanguage() {
		return driverLanguage;
	}

	public void setDriverLanguage(String driverLanguage) {
		this.driverLanguage = driverLanguage;
	}

	public String getVehicleDescription() {
		return vehicleDescription;
	}

	public void setVehicleDescription(String vehicleDescription) {
		this.vehicleDescription = vehicleDescription;
	}

	public String getVehicleBrand() {
		return vehicleBrand;
	}

	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleVIN() {
		return vehicleVIN;
	}

	public void setVehicleVIN(String vehicleVIN) {
		this.vehicleVIN = vehicleVIN;
	}

	public String getVehicleVINControlNumber() {
		return vehicleVINControlNumber;
	}

	public void setVehicleVINControlNumber(String vehicleVINControlNumber) {
		this.vehicleVINControlNumber = vehicleVINControlNumber;
	}

	public Integer getVehicleModelYear() {
		return vehicleModelYear;
	}

	public void setVehicleModelYear(Integer vehicleModelYear) {
		this.vehicleModelYear = vehicleModelYear;
	}

	public String getVehicleIdentificationCode() {
		return vehicleIdentificationCode;
	}

	public void setVehicleIdentificationCode(String vehicleIdentificationCode) {
		this.vehicleIdentificationCode = vehicleIdentificationCode;
	}

	public String getVehicleLicensePlate() {
		return vehicleLicensePlate;
	}

	public void setVehicleLicensePlate(String vehicleLicensePlate) {
		this.vehicleLicensePlate = vehicleLicensePlate;
	}

	public Boolean getVehicleStock() {
		return vehicleStock;
	}

	public void setVehicleStock(Boolean vehicleStock) {
		this.vehicleStock = vehicleStock;
	}

	public String getVehicleMileage() {
		return vehicleMileage;
	}

	public void setVehicleMileage(String vehicleMileage) {
		this.vehicleMileage = vehicleMileage;
	}

	public String getVehicleLanguageDocuments() {
		return vehicleLanguageDocuments;
	}

	public void setVehicleLanguageDocuments(String vehicleLanguageDocuments) {
		this.vehicleLanguageDocuments = vehicleLanguageDocuments;
	}

	public String getVehicleKeycode() {
		return vehicleKeycode;
	}

	public void setVehicleKeycode(String vehicleKeycode) {
		this.vehicleKeycode = vehicleKeycode;
	}

	public String getVehicleStartcode() {
		return vehicleStartcode;
	}

	public void setVehicleStartcode(String vehicleStartcode) {
		this.vehicleStartcode = vehicleStartcode;
	}

	public String getMotorType() {
		return motorType;
	}

	public void setMotorType(String motorType) {
		this.motorType = motorType;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	public String getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(String engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public String getNedcValue() {
		return nedcValue;
	}

	public void setNedcValue(String nedcValue) {
		this.nedcValue = nedcValue;
	}

	public String getWltpValue() {
		return wltpValue;
	}

	public void setWltpValue(String wltpValue) {
		this.wltpValue = wltpValue;
	}

	public String getInteriorColor() {
		return interiorColor;
	}

	public void setInteriorColor(String interiorColor) {
		this.interiorColor = interiorColor;
	}

	public String getTyreSpecifications() {
		return tyreSpecifications;
	}

	public void setTyreSpecifications(String tyreSpecifications) {
		this.tyreSpecifications = tyreSpecifications;
	}

	public String getTyreBrand() {
		return tyreBrand;
	}

	public void setTyreBrand(String tyreBrand) {
		this.tyreBrand = tyreBrand;
	}

	public String getTyreType() {
		return tyreType;
	}

	public void setTyreType(String tyreType) {
		this.tyreType = tyreType;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getExteriorColor() {
		return exteriorColor;
	}

	public void setExteriorColor(String exteriorColor) {
		this.exteriorColor = exteriorColor;
	}

	public String getVehiclePrice() {
		return vehiclePrice;
	}

	public void setVehiclePrice(String vehiclePrice) {
		this.vehiclePrice = vehiclePrice;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getDeliveryCosts() {
		return deliveryCosts;
	}

	public void setDeliveryCosts(String deliveryCosts) {
		this.deliveryCosts = deliveryCosts;
	}

	public String getOtherCosts() {
		return otherCosts;
	}

	public void setOtherCosts(String otherCosts) {
		this.otherCosts = otherCosts;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getDesiredDeliveryDate() {
		return desiredDeliveryDate;
	}

	public void setDesiredDeliveryDate(String desiredDeliveryDate) {
		this.desiredDeliveryDate = desiredDeliveryDate;
	}

	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public String getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public void setActualDeliveryDate(String actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}

	public String getFirstRegistrationDate() {
		return firstRegistrationDate;
	}

	public void setFirstRegistrationDate(String firstRegistrationDate) {
		this.firstRegistrationDate = firstRegistrationDate;
	}

	public String getDeliveryLocationName() {
		return deliveryLocationName;
	}

	public void setDeliveryLocationName(String deliveryLocationName) {
		this.deliveryLocationName = deliveryLocationName;
	}

	public String getDeliveryEnterpriseNumber() {
		return deliveryEnterpriseNumber;
	}

	public void setDeliveryEnterpriseNumber(String deliveryEnterpriseNumber) {
		this.deliveryEnterpriseNumber = deliveryEnterpriseNumber;
	}

	public String getDeliveryAddressLine() {
		return deliveryAddressLine;
	}

	public void setDeliveryAddressLine(String deliveryAddressLine) {
		this.deliveryAddressLine = deliveryAddressLine;
	}

	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}

	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}
	
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Boolean getReturnedVehicle() {
		return returnedVehicle;
	}

	public void setReturnedVehicle(Boolean returnedVehicle) {
		this.returnedVehicle = returnedVehicle;
	}

	public String getRegisteredOnTheName() {
		return registeredOnTheName;
	}

	public void setRegisteredOnTheName(String registeredOnTheName) {
		this.registeredOnTheName = registeredOnTheName;
	}

	public String getReturnedVehicleLicenseplate() {
		return returnedVehicleLicenseplate;
	}

	public void setReturnedVehicleLicenseplate(String returnedVehicleLicenseplate) {
		this.returnedVehicleLicenseplate = returnedVehicleLicenseplate;
	}

	public Boolean getRegistrationCertificate() {
		return registrationCertificate;
	}

	public void setRegistrationCertificate(Boolean registrationCertificate) {
		this.registrationCertificate = registrationCertificate;
	}

	public Boolean getInsuranceCertificate() {
		return insuranceCertificate;
	}

	public void setInsuranceCertificate(Boolean insuranceCertificate) {
		this.insuranceCertificate = insuranceCertificate;
	}

	public Boolean getCertificateOfConformity() {
		return certificateOfConformity;
	}

	public void setCertificateOfConformity(Boolean certificateOfConformity) {
		this.certificateOfConformity = certificateOfConformity;
	}

	public Boolean getTechnicalInspectionCertificate() {
		return technicalInspectionCertificate;
	}

	public void setTechnicalInspectionCertificate(Boolean technicalInspectionCertificate) {
		this.technicalInspectionCertificate = technicalInspectionCertificate;
	}

	public Boolean getLegalKit() {
		return legalKit;
	}

	public void setLegalKit(Boolean legalKit) {
		this.legalKit = legalKit;
	}

	public Boolean getServiceManual() {
		return serviceManual;
	}

	public void setServiceManual(Boolean serviceManual) {
		this.serviceManual = serviceManual;
	}

	public Boolean getFuelCard() {
		return fuelCard;
	}

	public void setFuelCard(Boolean fuelCard) {
		this.fuelCard = fuelCard;
	}

	public Integer getNumberOfKeys() {
		return numberOfKeys;
	}

	public void setNumberOfKeys(Integer numberOfKeys) {
		this.numberOfKeys = numberOfKeys;
	}

	public String getReturnedVehicleDriverName() {
		return returnedVehicleDriverName;
	}

	public void setReturnedVehicleDriverName(String returnedVehicleDriverName) {
		this.returnedVehicleDriverName = returnedVehicleDriverName;
	}

	public String getReturnedVehicleDriverDateOfBirth() {
		return returnedVehicleDriverDateOfBirth;
	}

	public void setReturnedVehicleDriverDateOfBirth(String returnedVehicleDriverDateOfBirth) {
		this.returnedVehicleDriverDateOfBirth = returnedVehicleDriverDateOfBirth;
	}

	public String getReturnedVehicleDriverBirthplace() {
		return returnedVehicleDriverBirthplace;
	}

	public void setReturnedVehicleDriverBirthplace(String returnedVehicleDriverBirthplace) {
		this.returnedVehicleDriverBirthplace = returnedVehicleDriverBirthplace;
	}

	public Boolean getBuybackApplicable() {
		return buybackApplicable;
	}

	public void setBuybackApplicable(Boolean buybackApplicable) {
		this.buybackApplicable = buybackApplicable;
	}

	public String getBuybackName() {
		return buybackName;
	}

	public void setBuybackName(String buybackName) {
		this.buybackName = buybackName;
	}

	public String getBuybackAddress() {
		return buybackAddress;
	}

	public void setBuybackAddress(String buybackAddress) {
		this.buybackAddress = buybackAddress;
	}

	public Integer getContractDuration() {
		return contractDuration;
	}

	public void setContractDuration(Integer contractDuration) {
		this.contractDuration = contractDuration;
	}

	public Integer getTotalMileage() {
		return totalMileage;
	}

	public void setTotalMileage(Integer totalMileage) {
		this.totalMileage = totalMileage;
	}

	public Integer getBuybackAmount() {
		return buybackAmount;
	}

	public void setBuybackAmount(Integer buybackAmount) {
		this.buybackAmount = buybackAmount;
	}

	public Logger getLogger() {
		return logger;
	}
	
	
}
		
		