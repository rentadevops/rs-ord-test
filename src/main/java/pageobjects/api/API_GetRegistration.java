package pageobjects.api;

import apiEngine.model.get.leasing.company.actor.Address;
import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.registration.registration.Registration;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import managers.WebDriverManager_ORD;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;


public class API_GetRegistration {
	final Logger logger = LogManager.getLogger(API_GetRegistration.class);


	//private Address lcAddress;
	private Response response;



	public API_GetRegistration() {
		
	}
	
	public void get_registration_data(Response response) throws ParseException {
		
		//lcCommercialName = leasingCompanyActor.getCommercialName();
		//lcInvoiceName = leasingCompanyActor.getInvoiceName(); //Name registered office
		//JsonPath jsonPathEvaluator = response.jsonPath();
		//ResponseBody body1 = response.body();

	setResponse(response);
	logger.debug("response get registration: " + response.asPrettyString());
	//Registration reg = response.body().as(Registration);

	}

	//public Address getLcAddress() {
	//	return lcAddress;
	//}


	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
}
		
		