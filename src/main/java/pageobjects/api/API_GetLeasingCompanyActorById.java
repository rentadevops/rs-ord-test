package pageobjects.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.model.get.leasing.company.actor.Address;
import apiEngine.model.get.leasing.company.actor.LeasingCompanyActor;
import apiEngine.model.get.order.detail.Agent;
import apiEngine.model.get.order.detail.Buyback;
import apiEngine.model.get.order.detail.Client;
import apiEngine.model.get.order.detail.Dealer;
import apiEngine.model.get.order.detail.Delivery;
import apiEngine.model.get.order.detail.DossierManager;
import apiEngine.model.get.order.detail.Driver;
import apiEngine.model.get.order.detail.FinancialLeaseRegistrationInfo;
import apiEngine.model.get.order.detail.InscriptionCosts;
import apiEngine.model.get.order.detail.Insurance;
import apiEngine.model.get.order.detail.LastComment;
import apiEngine.model.get.order.detail.LeasingCompany;
import apiEngine.model.get.order.detail.OptionDTO;
import apiEngine.model.get.order.detail.OrderDetail;
import apiEngine.model.get.order.detail.Pickup;
import apiEngine.model.get.order.detail.Pricing;
import apiEngine.model.get.order.detail.Product;
import apiEngine.model.get.order.detail.PromotionDTO;
import apiEngine.model.get.order.detail.TaxStampCosts;
import apiEngine.model.get.order.detail.TyreFitter;
import apiEngine.model.get.order.detail.Vehicle;
import managers.WebDriverManager_ORD;




public class API_GetLeasingCompanyActorById {
	final Logger logger = LogManager.getLogger(WebDriverManager.class);

	
	private Address lcAddress;
	private String lcCommercialName;
	private String lcContactEmail;
	private String lcContactFullName;
	private String lcContactMobileNumber;
	private String lcContactPhoneNumber;
	private String lcEnterpriseNumber;
	private String lcInvoiceName;
	private String lcLanguage;
	private String lcRsNumber;
	private String lcStreet;
	private String lcHouseNr;
	private String lcZipcode;
	private String lcCity;
	private String lcCountry;
	private String lcBillingAddress;

	
	public API_GetLeasingCompanyActorById(LeasingCompanyActor leasingCompanyActor) {
		
	}
	
	public void get_leasing_company_actor_by_id_data(LeasingCompanyActor leasingCompanyActor) throws ParseException {
		
		lcCommercialName = leasingCompanyActor.getCommercialName();
		lcInvoiceName = leasingCompanyActor.getInvoiceName(); //Name registered office
		lcEnterpriseNumber = leasingCompanyActor.getEnterpriseNumber();
		Integer lcRsNumberInt;
		lcRsNumberInt = leasingCompanyActor.getRsNumber();
		lcRsNumber = lcRsNumberInt.toString();
		lcStreet = leasingCompanyActor.getAddress().getStreet();
		lcHouseNr = leasingCompanyActor.getAddress().getHouseNumber();
		lcZipcode = leasingCompanyActor.getAddress().getZipCode().getValue();
		lcCity = leasingCompanyActor.getAddress().getCity();
		lcCountry = leasingCompanyActor.getAddress().getCountry();
		lcBillingAddress = lcStreet + " " + lcHouseNr + " , " + lcZipcode + " " + lcCity;
		logger.debug("billingAddress: " + lcBillingAddress);
		lcContactMobileNumber = leasingCompanyActor.getContactMobileNumber();
		lcContactPhoneNumber = leasingCompanyActor.getContactPhoneNumber();
		lcContactEmail = leasingCompanyActor.getContactEmail();
		lcContactFullName = leasingCompanyActor.getContactFullName();
		lcLanguage = leasingCompanyActor.getLanguage();

	}

	public Address getLcAddress() {
		return lcAddress;
	}

	public String getLcCommercialName() {
		return lcCommercialName;
	}

	public String getLcContactEmail() {
		return lcContactEmail;
	}

	public String getLcContactFullName() {
		return lcContactFullName;
	}

	public String getLcContactMobileNumber() {
		return lcContactMobileNumber;
	}

	public String getLcContactPhoneNumber() {
		return lcContactPhoneNumber;
	}

	public String getLcEnterpriseNumber() {
		return lcEnterpriseNumber;
	}

	public String getLcInvoiceName() {
		return lcInvoiceName;
	}

	public String getLcLanguage() {
		return lcLanguage;
	}

	public String getLcRsNumber() {
		return lcRsNumber;
	}

	public String getLcStreet() {
		return lcStreet;
	}

	public String getLcHouseNr() {
		return lcHouseNr;
	}

	public String getLcZipcode() {
		return lcZipcode;
	}

	public String getLcCity() {
		return lcCity;
	}
	
	public String getLcCountry() {
		return lcCountry;
	}

	public String getLcBillingAddress() {
		return lcBillingAddress;
	}
	
}
		
		