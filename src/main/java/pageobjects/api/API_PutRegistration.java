package pageobjects.api;

import apiEngine.Endpoints;
//import apiEngine.model.post.registration.LicensePlateFormat;
//import apiEngine.model.post.wallbox.status.Installed;
import apiEngine.model.put.registration.*;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import helpers.ORD_ContractNumberGenerator;
import helpers.ORD_DatePicker_Helper;
import io.restassured.response.Response;
import jakarta.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

//import javax.xml.bind.JAXBException;
import java.io.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class API_PutRegistration {
	final Logger logger = LogManager.getLogger(API_PutRegistration.class);

	private String xmlns;
	private String dosNr;


	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());
	WebDriver driver;
	ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);


	Registration registration;
	private String leasingCompanyOrderDossierNumber;
	private String registrarDossierNumber;
	private String formNumber;
	private String lesseeName;
	private String lesseeEnterpriseNumber;
	private String licensePlateHolderName;
	private String licensePlateHolderEnterpriseNumber;
	private String supplierName;
	private String supplierEnterpriseNumber;
	private String insuranceBrokerFsmaNumber;
	private String insuranceCompanyFsmaNumber;
	private String insuranceCompanyName;
	private String insuranceReferenceNumber;
	private Long insuranceBeginDate;
	private Long insurancePlannedRegistrationDate;
	private String driverLastName;
	private String driverFirstName;
	private String driverAddressStreet;
	private String driverAddressHouseNumber;
	private String driverAddressBoxNumber;
	private String driverAddressPostalCode;
	private String driverAddressCity;
	private String driverLanguageCode;
	private String driverMobileNumber;
	private String vehicleBrand;
	private String vehicleModel;
	private String vehicleVIN;
	private String vehicleVINControlNumber;
	private Integer vehicleCatalogueValue;
	private Integer vehicleCatalogueValueWithoutDiscount;
	private Integer vehicleBiv;
	private String vehicleCostCentre;
	private String vehicleUsedVehicle;
	private String vehiclePriorLicensePlate;
	private Long vehicleLastRegistrationDate;
	private String vehicleRegistrationDocumentsLanguage;
	private String vehicleEmailGreenCard;
	private String vehicleReuseLicensePlate;
	private String vehicleLicensePlate;
	private String vehicleLicensePlateFormat;
	private String vehicleFrontPlateOrdered;
	private String vehicleSpeedPedelecFlag;
	private String vehiclePedalingAssistance;
	private String deliveryBoxNumber;
	private String deliveryCity;
	private String deliveryFirstName;
	private String deliveryHouseNumber;
	private String deliveryPostalCode;
	private String deliveryStreet;
	private String deliveryLastNameOrCompanyName;
	private String deliveryADDressType;
	private Response res;

	String status;
	String number;
	String testString = "INSURANCE-REFERENCE-NUMBER-";


		public API_PutRegistration() {
			
		}
		
		public void putRegistration(List<List<String>> data) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, ParseException {
			//ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
			//registrarDossierNumber = mcng.getRandomContractNumber();
			Properties p = new Properties();
			//p.put("DossierNumber", registrarDossierNumber);
			//FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/dossiernumber.properties");
			//p.store(fos, "");
			//logger.debug("dosNr " + registrarDossierNumber +  " naar proprty file geschreven");

			Properties properties = new Properties();

			//InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
			//properties.load(inputDriver);
			//registrarDossierNumber = properties.getProperty("DossierNumber");
			setRegistrarDossierNumber("TJULDTU3670366955");
			//logger.debug("dosNr " + registrarDossierNumber +  " van proprty file gelezen");
			//leasingCompanyOrderDossierNumber = "PZHHBDA6458428015";
			//setLeasingCompanyOrderDossierNumber(leasingCompanyOrderDossierNumber);
			//formNumber = "911182223";
			formNumber = data.get(1).get(2);
			logger.debug("formNumber voor de if: " + formNumber);
			if (StringUtils.isEmpty(formNumber)) {
			//if (formNumber.length() > 0) {
			//if(formNumber.isEmpty() || formNumber.equals("") || formNumber == null) {

			} else {
				setFormNumber(formNumber);
			}

			Lessee lessee = new Lessee();
			lesseeName = data.get(1).get(3);
			lessee.setName(lesseeName);
			LicensePlateHolder licensePlateHolder = new LicensePlateHolder();
			licensePlateHolderName = data.get(1).get(5);
			licensePlateHolder.setName(licensePlateHolderName);
			licensePlateHolderEnterpriseNumber = data.get(1).get(6);
			licensePlateHolder.setEnterpriseNumber(licensePlateHolderEnterpriseNumber);
			Supplier supplier = new Supplier();
			supplierName = data.get(1).get(7);
			supplier.setName(supplierName);
			supplierEnterpriseNumber = data.get(1).get(8);
			supplier.setEnterpriseNumber(supplierEnterpriseNumber);
			Insurance insurance = new Insurance();
			insuranceCompanyFsmaNumber = data.get(1).get(10);
			insurance.setInsuranceCompanyFsmaNumber(insuranceCompanyFsmaNumber);
			insuranceBrokerFsmaNumber = data.get(1).get(9);
			insurance.setInsuranceBrokerFsmaNumber(insuranceBrokerFsmaNumber);

			InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/referencenumber.properties");
			properties.load(inputDriver);
			number = properties.getProperty("number");
			Integer intNumber = Integer.parseInt(number);
			intNumber++;
			number = intNumber.toString();


			p = new Properties();
			p.put("number", number);
			FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/referencenumber.properties");
			p.store(fos, "");
			logger.debug("number " + number);
			insuranceReferenceNumber = testString + number;
			//insuranceReferenceNumber = "999993344";
			insurance.setReferenceNumber(insuranceReferenceNumber);
			String insuranceBeginDateStr = data.get(1).get(12);
			Long insuranceBeginDateLong = Long.parseLong(insuranceBeginDateStr);
			insurance.setBeginDate(insuranceBeginDateLong);

			//String dt = "2008-01-01";  // Start date
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			//c.add(Calendar.DATE, 1);  // number of days to add

			if(System.getProperty("insuranceType").equals("planned")) {
				c.add(Calendar.DATE, 1);
				insurancePlannedRegistrationDate = c.getTimeInMillis();
				logger.debug("insurancePlannedRegistrationDate: " + insurancePlannedRegistrationDate);
				insurance.setPlannedRegistrationDate(insurancePlannedRegistrationDate);
			} else {
				insurancePlannedRegistrationDate = c.getTimeInMillis();
				logger.debug("insurancePlannedRegistrationDate: " + insurancePlannedRegistrationDate);
				//insurancePlannedRegistrationDate = 1585918077790L;
				insurance.setPlannedRegistrationDate(insurancePlannedRegistrationDate);
			}

			//insurance.setInsuranceCompanyName("AG Insurance FSMA 5 digit");



			Vehicle vehicle = new Vehicle();
			vehicleBrand = data.get(1).get(23);
			vehicle.setBrand(vehicleBrand);
			vehicleModel = data.get(1).get(24);
			vehicle.setModel(vehicleModel);
			vehicleVIN = data.get(1).get(25);
			vehicle.setVin(vehicleVIN);
			vehicleVINControlNumber = data.get(1).get(26);
			vehicle.setVinControlNumber(vehicleVINControlNumber);
			String vehicleCatalogueValueStr = data.get(1).get(27);
			if(vehicleCatalogueValueStr != null) {
				Integer vehicleCatalogueValueInt = Integer.parseInt(vehicleCatalogueValueStr);
				vehicle.setCatalogueValue(vehicleCatalogueValueInt);
			}
			String vehicleCatalogueValueWithoutDiscountStr = data.get(1).get(28);
			if(vehicleCatalogueValueWithoutDiscountStr != null){
				Integer vehicleCatalogueValueWithoutDiscountInt = Integer.parseInt(vehicleCatalogueValueWithoutDiscountStr);
				vehicle.setCatalogueValueWithoutDiscount(vehicleCatalogueValueWithoutDiscountInt);
			}

			String vehicleBivStr = data.get(1).get(29);
			if(vehicleBivStr != null){
				Integer vehicleBivInt = Integer.parseInt(vehicleBivStr);
				vehicle.setBiv(vehicleBivInt);
			}


			vehicleCostCentre = data.get(1).get(30);
			vehicle.setCostCentre(vehicleCostCentre);
			vehicleUsedVehicle = data.get(1).get(31);
			vehicle.setUsedVehicle(Boolean.parseBoolean(vehicleUsedVehicle));
			vehiclePriorLicensePlate = data.get(1).get(32);
			vehicle.setPriorLicensePlate(vehiclePriorLicensePlate);
			String vehicleLastRegistrationDateStr = data.get(1).get(33);
			if(vehicleLastRegistrationDateStr == null) {
				vehicle.setLastRegistrationDate(null);
			} else {
				vehicleLastRegistrationDate = Long.parseLong(vehicleLastRegistrationDateStr);
				vehicle.setLastRegistrationDate(vehicleLastRegistrationDate);
			}

			vehicleRegistrationDocumentsLanguage = data.get(1).get(34);
			vehicle.setRegistrationDocumentsLanguage(vehicleRegistrationDocumentsLanguage);
			vehicleEmailGreenCard = data.get(1).get(35);
			vehicle.setEmailGreenCard(vehicleEmailGreenCard);
			vehicleReuseLicensePlate = data.get(1).get(36);
			vehicle.setReuseLicensePlate(Boolean.parseBoolean(vehicleReuseLicensePlate));
			vehicleLicensePlate = data.get(1).get(37);
			//vehicle.setLicensePlate(vehicleLicensePlate);
			vehicleLicensePlateFormat = data.get(1).get(38);
			vehicle.setLicensePlateFormat(vehicleLicensePlateFormat);
			vehicleFrontPlateOrdered = data.get(1).get(39);
			vehicle.setFrontPlateOrdered(Boolean.parseBoolean(vehicleFrontPlateOrdered));
			Delivery delivery = new Delivery();
			deliveryBoxNumber = data.get(1).get(42);
			delivery.setBoxNumber(deliveryBoxNumber);
			deliveryCity = data.get(1).get(43);
			delivery.setCity(deliveryCity);
			deliveryHouseNumber = data.get(1).get(45);
			delivery.setHouseNumber(deliveryHouseNumber);
			deliveryPostalCode = data.get(1).get(46);
			delivery.setPostalCode(deliveryPostalCode);
			deliveryStreet = data.get(1).get(48);
			delivery.setStreet(deliveryStreet);
			deliveryADDressType = data.get(1).get(49);
			delivery.setDeliveryAddressType(deliveryADDressType);
			deliveryLastNameOrCompanyName = data.get(1).get(47);
			delivery.setLastNameOrCompanyName(deliveryLastNameOrCompanyName);
			Driver driver = new Driver();
			driverLastName = data.get(1).get(14);
			driver.setLastName(driverLastName);
			driverFirstName = data.get(1).get(15);
			driver.setFirstName(driverFirstName);
			driverAddressStreet = data.get(1).get(16);
			driver.setStreet(driverAddressStreet);
			driverAddressHouseNumber = data.get(1).get(17);
			driver.setHouseNumber(driverAddressHouseNumber);
			driverAddressBoxNumber = data.get(1).get(18);
			driver.setBoxNumber(driverAddressBoxNumber);
			driverAddressPostalCode = data.get(1).get(19);
			driver.setPostalCode(driverAddressPostalCode);
			driverAddressCity = data.get(1).get(20);
			driver.setCity(driverAddressCity);
			driverLanguageCode = data.get(1).get(21);
			driver.setLanguageCode(driverLanguageCode);
			driverMobileNumber = data.get(1).get(22);
			driver.setMobileNumber(driverMobileNumber);

			Registration registration = new Registration();
			//registration.setLeasingCompanyOrderDossierNumber(leasingCompanyOrderDossierNumber);
			registration.setRegistrationId("a1f50fd1-5c2f-464b-90c7-69c89cb4b233");
			registration.setRegistrarDossierNumber(registrarDossierNumber);
			registration.setFormNumber(formNumber);
			registration.setLessee(lessee);
			registration.setLicensePlateHolder(licensePlateHolder);
			registration.setSupplier(supplier);
			registration.setInsurance(insurance);
			registration.setVehicle(vehicle);
			registration.setDelivery(delivery);
			registration.setDriver(driver);
			logger.debug("putRegistration: " + registration);

			Response res = Endpoints.putRegistration(registration);
			logger.debug("statusLine PutRegistration:" + res.statusLine());
			logger.debug("Response PutRegistration:" + res.asPrettyString());
			//Assert.assertEquals("200",res.statusCode());
			Thread.sleep(5000);
			setRes(res);

			}

	public Response getRes() {
			logger.debug("response in putRegistration getter: " + res.asPrettyString());
		return res;
	}

	public void setRes(Response res) {
		this.res = res;
	}

	public String getLeasingCompanyOrderDossierNumber() {
		return leasingCompanyOrderDossierNumber;
	}

	public void setLeasingCompanyOrderDossierNumber(String leasingCompanyOrderDossierNumber) {
		this.leasingCompanyOrderDossierNumber = leasingCompanyOrderDossierNumber;
	}

	public String getRegistrarDossierNumber() {
		return registrarDossierNumber;
	}

	public void setRegistrarDossierNumber(String registrarDossierNumber) {
		this.registrarDossierNumber = registrarDossierNumber;
	}

	public String getFormNumber() {
		return formNumber;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	public String getLesseeName() {
		return lesseeName;
	}

	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName;
	}

	public String getLesseeEnterpriseNumber() {
		return lesseeEnterpriseNumber;
	}

	public void setLesseeEnterpriseNumber(String lesseeEnterpriseNumber) {
		this.lesseeEnterpriseNumber = lesseeEnterpriseNumber;
	}

	public String getLicensePlateHolderName() {
		return licensePlateHolderName;
	}

	public void setLicensePlateHolderName(String licensePlateHolderName) {
		this.licensePlateHolderName = licensePlateHolderName;
	}

	public String getLicensePlateHolderEnterpriseNumber() {
		return licensePlateHolderEnterpriseNumber;
	}

	public void setLicensePlateHolderEnterpriseNumber(String licensePlateHolderEnterpriseNumber) {
		this.licensePlateHolderEnterpriseNumber = licensePlateHolderEnterpriseNumber;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierEnterpriseNumber() {
		return supplierEnterpriseNumber;
	}

	public void setSupplierEnterpriseNumber(String supplierEnterpriseNumber) {
		this.supplierEnterpriseNumber = supplierEnterpriseNumber;
	}

	public String getInsuranceBrokerFsmaNumber() {
		return insuranceBrokerFsmaNumber;
	}

	public void setInsuranceBrokerFsmaNumber(String insuranceBrokerFsmaNumber) {
		this.insuranceBrokerFsmaNumber = insuranceBrokerFsmaNumber;
	}

	public String getInsuranceCompanyFsmaNumber() {
		return insuranceCompanyFsmaNumber;
	}

	public void setInsuranceCompanyFsmaNumber(String insuranceCompanyFsmaNumber) {
		this.insuranceCompanyFsmaNumber = insuranceCompanyFsmaNumber;
	}

	public String getInsuranceReferenceNumber() {
		return insuranceReferenceNumber;
	}

	public void setInsuranceReferenceNumber(String insuranceReferenceNumber) {
		this.insuranceReferenceNumber = insuranceReferenceNumber;
	}

	public Long getInsuranceBeginDate() {
		return insuranceBeginDate;
	}

	public void setInsuranceBeginDate(Long insuranceBeginDate) {
		this.insuranceBeginDate = insuranceBeginDate;
	}

	public Long getInsurancePlannedRegistrationDate() {
		return insurancePlannedRegistrationDate;
	}

	public void setInsurancePlannedRegistrationDate(Long insurancePlannedRegistrationDate) {
		this.insurancePlannedRegistrationDate = insurancePlannedRegistrationDate;
	}

	public String getDriverLastName() {
		return driverLastName;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverAddressStreet() {
		return driverAddressStreet;
	}

	public void setDriverAddressStreet(String driverAddressStreet) {
		this.driverAddressStreet = driverAddressStreet;
	}

	public String getDriverAddressHouseNumber() {
		return driverAddressHouseNumber;
	}

	public void setDriverAddressHouseNumber(String driverAddressHouseNumber) {
		this.driverAddressHouseNumber = driverAddressHouseNumber;
	}

	public String getDriverAddressBoxNumber() {
		return driverAddressBoxNumber;
	}

	public void setDriverAddressBoxNumber(String driverAddressBoxNumber) {
		this.driverAddressBoxNumber = driverAddressBoxNumber;
	}

	public String getDriverAddressPostalCode() {
		return driverAddressPostalCode;
	}

	public void setDriverAddressPostalCode(String driverAddressPostalCode) {
		this.driverAddressPostalCode = driverAddressPostalCode;
	}

	public String getDriverAddressCity() {
		return driverAddressCity;
	}

	public void setDriverAddressCity(String driverAddressCity) {
		this.driverAddressCity = driverAddressCity;
	}

	public String getDriverLanguageCode() {
		return driverLanguageCode;
	}

	public void setDriverLanguageCode(String driverLanguageCode) {
		this.driverLanguageCode = driverLanguageCode;
	}

	public String getDriverMobileNumber() {
		return driverMobileNumber;
	}

	public void setDriverMobileNumber(String driverMobileNumber) {
		this.driverMobileNumber = driverMobileNumber;
	}

	public String getVehicleBrand() {
		return vehicleBrand;
	}

	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleVIN() {
		return vehicleVIN;
	}

	public void setVehicleVIN(String vehicleVIN) {
		this.vehicleVIN = vehicleVIN;
	}

	public String getVehicleVINControlNumber() {
		return vehicleVINControlNumber;
	}

	public void setVehicleVINControlNumber(String vehicleVINControlNumber) {
		this.vehicleVINControlNumber = vehicleVINControlNumber;
	}

	public Integer getVehicleCatalogueValue() {
		return vehicleCatalogueValue;
	}

	public void setVehicleCatalogueValue(Integer vehicleCatalogueValue) {
		this.vehicleCatalogueValue = vehicleCatalogueValue;
	}

	public Integer getVehicleCatalogueValueWithoutDiscount() {
		return vehicleCatalogueValueWithoutDiscount;
	}

	public void setVehicleCatalogueValueWithoutDiscount(Integer vehicleCatalogueValueWithoutDiscount) {
		this.vehicleCatalogueValueWithoutDiscount = vehicleCatalogueValueWithoutDiscount;
	}

	public Integer getVehicleBiv() {
		return vehicleBiv;
	}

	public void setVehicleBiv(Integer vehicleBiv) {
		this.vehicleBiv = vehicleBiv;
	}

	public String getVehicleCostCentre() {
		return vehicleCostCentre;
	}

	public void setVehicleCostCentre(String vehicleCostCentre) {
		this.vehicleCostCentre = vehicleCostCentre;
	}

	public String getVehicleUsedVehicle() {
		return vehicleUsedVehicle;
	}

	public void setVehicleUsedVehicle(String vehicleUsedVehicle) {
		this.vehicleUsedVehicle = vehicleUsedVehicle;
	}

	public String getVehiclePriorLicensePlate() {
		return vehiclePriorLicensePlate;
	}

	public void setVehiclePriorLicensePlate(String vehiclePriorLicensePlate) {
		this.vehiclePriorLicensePlate = vehiclePriorLicensePlate;
	}

	public Long getVehicleLastRegistrationDate() {
		return vehicleLastRegistrationDate;
	}

	public void setVehicleLastRegistrationDate(Long vehicleLastRegistrationDate) {
		this.vehicleLastRegistrationDate = vehicleLastRegistrationDate;
	}

	public String getVehicleRegistrationDocumentsLanguage() {
		return vehicleRegistrationDocumentsLanguage;
	}

	public void setVehicleRegistrationDocumentsLanguage(String vehicleRegistrationDocumentsLanguage) {
		this.vehicleRegistrationDocumentsLanguage = vehicleRegistrationDocumentsLanguage;
	}

	public String getVehicleEmailGreenCard() {
		return vehicleEmailGreenCard;
	}

	public void setVehicleEmailGreenCard(String vehicleEmailGreenCard) {
		this.vehicleEmailGreenCard = vehicleEmailGreenCard;
	}

	public String getVehicleReuseLicensePlate() {
		return vehicleReuseLicensePlate;
	}

	public void setVehicleReuseLicensePlate(String vehicleReuseLicensePlate) {
		this.vehicleReuseLicensePlate = vehicleReuseLicensePlate;
	}

	public String getVehicleLicensePlate() {
		return vehicleLicensePlate;
	}

	public void setVehicleLicensePlate(String vehicleLicensePlate) {
		this.vehicleLicensePlate = vehicleLicensePlate;
	}

	public String getVehicleLicensePlateFormat() {
		return vehicleLicensePlateFormat;
	}

	public void setVehicleLicensePlateFormat(String vehicleLicensePlateFormat) {
		this.vehicleLicensePlateFormat = vehicleLicensePlateFormat;
	}

	public String getVehicleFrontPlateOrdered() {
		return vehicleFrontPlateOrdered;
	}

	public void setVehicleFrontPlateOrdered(String vehicleFrontPlateOrdered) {
		this.vehicleFrontPlateOrdered = vehicleFrontPlateOrdered;
	}

	public String getVehicleSpeedPedelecFlag() {
		return vehicleSpeedPedelecFlag;
	}

	public void setVehicleSpeedPedelecFlag(String vehicleSpeedPedelecFlag) {
		this.vehicleSpeedPedelecFlag = vehicleSpeedPedelecFlag;
	}

	public String getVehiclePedalingAssistance() {
		return vehiclePedalingAssistance;
	}

	public void setVehiclePedalingAssistance(String vehiclePedalingAssistance) {
		this.vehiclePedalingAssistance = vehiclePedalingAssistance;
	}

	public String getDeliveryBoxNumber() {
		return deliveryBoxNumber;
	}

	public void setDeliveryBoxNumber(String deliveryBoxNumber) {
		this.deliveryBoxNumber = deliveryBoxNumber;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public String getDeliveryFirstName() {
		return deliveryFirstName;
	}

	public void setDeliveryFirstName(String deliveryFirstName) {
		this.deliveryFirstName = deliveryFirstName;
	}

	public String getDeliveryHouseNumber() {
		return deliveryHouseNumber;
	}

	public void setDeliveryHouseNumber(String deliveryHouseNumber) {
		this.deliveryHouseNumber = deliveryHouseNumber;
	}

	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}

	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public void setDeliveryStreet(String deliveryStreet) {
		this.deliveryStreet = deliveryStreet;
	}

	public String getDeliveryLastNameOrCompanyName() {
		return deliveryLastNameOrCompanyName;
	}

	public void setDeliveryLastNameOrCompanyName(String deliveryLastNameOrCompanyName) {
		this.deliveryLastNameOrCompanyName = deliveryLastNameOrCompanyName;
	}
}





