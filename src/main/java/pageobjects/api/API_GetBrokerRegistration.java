package pageobjects.api;

import apiEngine.Endpoints;
import cucumber.TestContext;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Properties;


public class API_GetBrokerRegistration {
	final Logger logger = LogManager.getLogger(API_GetBrokerRegistration.class);
	API_PutRegistration putRegistration;
	API_GetRegistration getRegistration;
	API_PostBrokerRegistration postBrokerRegistration;
	API_GetBrokerRegistration getBrokerRegistration;

	//private Address lcAddress;
	private Response response;
	private TestContext testContext;
	String insuranceReferenceNumber;

	public API_GetBrokerRegistration(TestContext testContext) throws InterruptedException, IOException {
		this.testContext = testContext;
		//orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		//getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		//registrationImports = testContext.getPageObjectManager().postFTP_RegistrationImports();
		putRegistration = testContext.getPageObjectManager().putAPI_PutRegistration();
		getRegistration = testContext.getPageObjectManager().getAPI_GetRegistration();
		postBrokerRegistration = testContext.getPageObjectManager().postAPI_PostBrokerRegistration();
		getBrokerRegistration = testContext.getPageObjectManager().getAPI_GetBrokerRegistration();


	}



	public API_GetBrokerRegistration() throws IOException {
/*		Response res = getBrokerRegistration.getResponse();
		logger.debug("GetBrokerRegistrationApiSteps respomse:" + res.asPrettyString());
		//Response res = getBrokerRegistration.getResponse();
		JsonPath jsonPathEvaluator = res.jsonPath();
		String registrationId = jsonPathEvaluator.getString("registrationId");
		logger.debug("registrationId in PostRegistrationComplete: " + registrationId);
		String vatNumber = jsonPathEvaluator.getString("licensePlateHolder.vatNumber.value");
		logger.debug("vatNumber invoer:" + vatNumber);
		String jsonBodyNationalId = "{\n" +
				"  \"nationalId\": {\n" +
				"    \"value\": \"25020139913\"\n" +
				"  }\n" +
				"}";
		String jsonBodyEmpty = "{\n" +
				"  \"nationalId\": null\n" +
				"}";
		if(vatNumber == null) {
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyNationalId);
			setResponse(response);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		} else {
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyEmpty);
			setResponse(response);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		}*/
		
	}

	public Response get_broker_registration() throws IOException {
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("src/test/resources/runtime/insurancereferencenumber.properties");
		properties.load(inputDriver);
		insuranceReferenceNumber = properties.getProperty("insurancereferencenumber");
		//Response res = Endpoints.getBrokerRegistration(insuranceReferenceNumber);
		//logger.debug("GetBrokerRegistrationApiSteps respomse:" + res.asPrettyString());
		//Response res = getBrokerRegistration.getResponse();
		//JsonPath jsonPathEvaluator = res.jsonPath();

		Properties p = new Properties();
		InputStream is = new FileInputStream("src/test/resources/runtime/registrationId.properties");
		properties.load(is);
		String registrationId = p.getProperty("registrationId");
		//String registrationId = jsonPathEvaluator.getString("registrationId");
		//logger.debug("registrationId in PostRegistrationComplete: " + registrationId);
		//String vatNumber = jsonPathEvaluator.getString("licensePlateHolder.vatNumber.value");
		//logger.debug("vatNumber invoer:" + vatNumber);
		String jsonBodyNationalId = "{\n" +
				"  \"nationalId\": {\n" +
				"    \"value\": \"25020139913\"\n" +
				"  }\n" +
				"}";
		String jsonBodyEmpty = "{\n" +
				"  \"nationalId\": null\n" +
				"}";

		response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyNationalId);
		logger.debug("EndPoint response putBrokerRegistrationApproved: " + response.asPrettyString());
		/*if(vatNumber == null | vatNumber.isEmpty()) {
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyNationalId);
			setResponse(response);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		} else {
			//response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyEmpty);
			response = Endpoints.putBrokerRegistrationApproved(registrationId, jsonBodyNationalId);
			setResponse(response);
			logger.debug("response putBrokerRegistrationApproved:" + response.asPrettyString());
		}*/
		return response;
	}
	
	public void get_broker_registration_data(Response response) throws ParseException, IOException {

		
		//lcCommercialName = leasingCompanyActor.getCommercialName();
		//lcInvoiceName = leasingCompanyActor.getInvoiceName(); //Name registered office
		//JsonPath jsonPathEvaluator = response.jsonPath();
		//ResponseBody body1 = response.body();

	setResponse(response);
	logger.debug("response get registration: " + response.asPrettyString());
	//Registration reg = response.body().as(Registration);

	}

	//public Address getLcAddress() {
	//	return lcAddress;
	//}


	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
}
		
		