package pageobjects.api;

import apiEngine.Endpoints;
import apiEngine.model.post.brokerregistration.*;
//import apiEngine.model.put.registration.*;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import helpers.ORD_ContractNumberGenerator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import jakarta.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import javax.xml.bind.JAXBException;
import java.io.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class API_PostBrokerRegistration {
	final Logger logger = LogManager.getLogger(API_PostBrokerRegistration.class);

	private String xmlns;
	private String dosNr;


	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());


	BrokerRegistration brokerRegistration;
	private String leasingCompanyOrderDossierNumber;
	private String registrarDossierNumber;
	private String formNumber;
	private String lesseeName;
	private String lesseeEnterpriseNumber;
	private String licensePlateHolderName;
	private String licensePlateHolderEnterpriseNumber;
	private String supplierName;
	private String supplierEnterpriseNumber;
	private String insuranceBrokerFsmaNumber;
	private String insuranceCompanyFsmaNumber;
	private String insuranceReferenceNumber;
	private Long insuranceBeginDate;
	private Long insurancePlannedRegistrationDate;
	private String driverLastName;
	private String driverFirstName;
	private String driverAddressStreet;
	private String driverAddressHouseNumber;
	private String driverAddressBoxNumber;
	private String driverAddressPostalCode;
	private String driverAddressCity;
	private String driverLanguageCode;
	private String driverMobileNumber;
	private String vehicleBrand;
	private String vehicleModel;
	private String vehicleVIN;
	private String vehicleVINControlNumber;
	private Integer vehicleCatalogueValue;
	private Integer vehicleCatalogueValueWithoutDiscount;
	private Integer vehicleBiv;
	private String vehicleCostCentre;
	private String vehicleUsedVehicle;
	private String vehiclePriorLicensePlate;
	private Long vehicleLastRegistrationDate;
	private String vehicleRegistrationDocumentsLanguage;
	private String vehicleEmailGreenCard;
	private String vehicleReuseLicensePlate;
	private String vehicleLicensePlate;
	private String vehicleLicensePlateFormat;
	private String vehicleFrontPlateOrdered;
	private String vehicleSpeedPedelecFlag;
	private String vehiclePedalingAssistance;
	private String deliveryBoxNumber;
	private String deliveryCity;
	private String deliveryFirstName;
	private String deliveryHouseNumber;
	private String deliveryPostalCode;
	private String deliveryStreet;
	private String deliveryLastNameOrCompanyName;
	private String registrationInfoUsedVehicle;
	private String registrationInfoPriorLicensePlate;
	private String registrationInfoLastRegistrationDate;
	private String registrationInfoFrontPlateOrdered;
	private String registrationInfoReuseLicensePlate;
	private String registrationInfoRegistrationDocumentsLanguage;
	private String registrationInfolicensePlate;
	private String registrationInfoLicensePlateFormat;
	private String registrationInfoEmailGreenCard;
	private String sellerName;
	private String sellerVat;
	private String registrationManagerUserId;
	private Long plannedRegistrationDate;
	private Response res;

	String status;
	String number;
	String testString = "INS-REF-NUM-";
	FileOutputStream fos;


		public API_PostBrokerRegistration() {
			
		}
		
		public void postBrokerRegistration(List<List<String>> data) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, ParseException {
			ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();


			Properties properties = new Properties();

			InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
			properties.load(inputDriver);
			if(data.get(1).get(0) == null) {

			} else {
				//leasingCompanyOrderDossierNumber = "PZHHBDA6458428015";
				//setLeasingCompanyOrderDossierNumber(leasingCompanyOrderDossierNumber);
			}
			//formNumber = "911182223";
			formNumber = data.get(1).get(2);
			logger.debug("formNumber voor de if: " + formNumber);
			if (StringUtils.isEmpty(formNumber)) {
			//if (formNumber.length() > 0) {
			//if(formNumber.isEmpty() || formNumber.equals("") || formNumber == null) {

			} else {
				setFormNumber(formNumber);
			}

			Delivery delivery = new Delivery();
			deliveryFirstName = data.get(1).get(3);
			deliveryBoxNumber = data.get(1).get(7);
			delivery.setBoxNumber(deliveryBoxNumber);
			deliveryCity = data.get(1).get(9);
			delivery.setCity(deliveryCity);
			deliveryHouseNumber = data.get(1).get(6);
			delivery.setHouseNumber(deliveryHouseNumber);
			deliveryPostalCode = data.get(1).get(8);
			delivery.setPostalCode(deliveryPostalCode);
			deliveryStreet = data.get(1).get(5);
			delivery.setStreet(deliveryStreet);
			deliveryLastNameOrCompanyName = data.get(1).get(4);
			delivery.setLastNameOrCompanyName(deliveryLastNameOrCompanyName);

			LicensePlateHolder licensePlateHolder = new LicensePlateHolder();
			licensePlateHolderName = data.get(1).get(11);
			licensePlateHolder.setName(licensePlateHolderName);
			VatNumber vatNumber = new VatNumber();
			licensePlateHolderEnterpriseNumber = data.get(1).get(12);
			if(licensePlateHolderEnterpriseNumber == null) {

			} else {
				vatNumber.setValue(licensePlateHolderEnterpriseNumber);
				licensePlateHolder.setVatNumber(vatNumber);
			}


			RegistrationInfo registrationInfo = new RegistrationInfo();
			registrationInfoUsedVehicle = data.get(1).get(13);
			Boolean registrationInfoUsedVehicleBool = Boolean.parseBoolean(registrationInfoUsedVehicle);
			registrationInfo.setUsedVehicle(registrationInfoUsedVehicleBool);
			registrationInfoPriorLicensePlate = data.get(1).get(14);
			registrationInfo.setPriorLicensePlate(registrationInfoPriorLicensePlate);
			registrationInfoLastRegistrationDate = data.get(1).get(15);
			registrationInfo.setLastRegistrationDate(registrationInfoLastRegistrationDate);
			registrationInfoFrontPlateOrdered = data.get(1).get(16);
			Boolean registrationInfoFrontPlateOrderedBool = Boolean.parseBoolean(registrationInfoFrontPlateOrdered);
			registrationInfo.setOrderFrontPlate(registrationInfoFrontPlateOrderedBool);
			registrationInfoReuseLicensePlate = data.get(1).get(17);
			Boolean registrationInfoReuseLicensePlateBool = Boolean.parseBoolean(registrationInfoReuseLicensePlate);
			registrationInfo.setReuseLicensePlate(registrationInfoReuseLicensePlateBool);
			registrationInfoRegistrationDocumentsLanguage = data.get(1).get(18);
			registrationInfo.setRegistrationDocumentsLanguage(registrationInfoRegistrationDocumentsLanguage);
			registrationInfolicensePlate = data.get(1).get(19);
			registrationInfo.setLicensePlate(registrationInfolicensePlate);
			registrationInfoLicensePlateFormat = data.get(1).get(20);
			registrationInfo.setLicensePlateFormat(registrationInfoLicensePlateFormat);
			registrationInfoEmailGreenCard = data.get(1).get(21);
			registrationInfo.setEmailGreenCard(registrationInfoEmailGreenCard);

			Seller seller = new Seller();
			sellerName = data.get(1).get(22);
			seller.setName(sellerName);
			VatNumber__1 vatNumber__1 = new VatNumber__1();
			sellerVat = data.get(1).get(23);
			vatNumber__1.setValue(sellerVat);
			seller.setVatNumber(vatNumber__1);

			Vehicle vehicle = new Vehicle();
			vehicleVIN = data.get(1).get(24);
			Vin vin = new Vin();
			//vehicle.setVin(vehicleVIN);
			vin.setValue(vehicleVIN);
			vehicle.setVin(vin);
			vehicleVINControlNumber = data.get(1).get(25);
			vehicle.setVinControlNumber(vehicleVINControlNumber);

			Insurance insurance = new Insurance();
			insuranceCompanyFsmaNumber = data.get(1).get(26);
			insurance.setInsuranceCompanyFsmaNumber(insuranceCompanyFsmaNumber);
			insuranceBrokerFsmaNumber = data.get(1).get(27);
			insurance.setInsuranceBrokerFsmaNumber(insuranceBrokerFsmaNumber);

			inputDriver = new FileInputStream("./src/test/resources/runtime/referencenumber.properties");
			properties.load(inputDriver);
			number = properties.getProperty("number");
			Integer intNumber = Integer.parseInt(number);
			intNumber++;
			number = intNumber.toString();


			Properties p = new Properties();
			p.put("number", number);
			fos = new FileOutputStream("./src/test/resources/runtime/referencenumber.properties");
			p.store(fos, "");
			logger.debug("number " + number);
			insuranceReferenceNumber = testString + number;
			Properties prop = new Properties();
			prop.put("insurancereferencenumber", insuranceReferenceNumber);
			fos = new FileOutputStream("./src/test/resources/runtime/insurancereferencenumber.properties");
			p.store(fos, "");
			insurance.setReferenceNumber(insuranceReferenceNumber);
			String insuranceBeginDateStr = data.get(1).get(29);
			insuranceBeginDate = Long.parseLong(insuranceBeginDateStr);
			insurance.setBeginDate(insuranceBeginDateStr);
			registrationManagerUserId = data.get(1).get(30);
			if(registrationManagerUserId == null) {

			} else {
				Integer registrationManagerUserIdInt = Integer.parseInt(registrationManagerUserId);
				insurance.setRegistrationManagerUserId(registrationManagerUserIdInt);
			}


			//String dt = "2008-01-01";  // Start date
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			//c.add(Calendar.DATE, 1);  // number of days to add

			plannedRegistrationDate = c.getTimeInMillis();
			logger.debug("insurancePlannedRegistrationDate: " + plannedRegistrationDate);
			//insurancePlannedRegistrationDate = 1585918077790L;
			setPlannedRegistrationDate(plannedRegistrationDate);




			BrokerRegistration brokerRegistration = new BrokerRegistration();
			//registration.setLeasingCompanyOrderDossierNumber(leasingCompanyOrderDossierNumber);
			//registration.setRegistrarDossierNumber(registrarDossierNumber);
			brokerRegistration.setFormNumber(formNumber);
			brokerRegistration.setPlannedRegistrationDate(plannedRegistrationDate.toString());
			brokerRegistration.setDelivery(delivery);
			brokerRegistration.setLicensePlateHolder(licensePlateHolder);
			brokerRegistration.setRegistrationInfo(registrationInfo);
			brokerRegistration.setSeller(seller);
			brokerRegistration.setVehicle(vehicle);
			brokerRegistration.setInsurance(insurance);


			logger.debug("postBrokerRegistration: " + brokerRegistration);

			Response res = Endpoints.postBrokerRegistration(brokerRegistration);
			logger.debug("statusLine PostRegistration:" + res.statusLine());
			JsonPath jsonPathEvaluator = res.jsonPath();
			String registrationId = jsonPathEvaluator.getString("registrationId");
			Properties pro = new Properties();
			pro.put("registrationId", registrationId);
			fos = new FileOutputStream("./src/test/resources/runtime/registrationId.properties");
			p.store(fos, "");
			//logger.debug("res.asPrettyString:" + res.asPrettyString());
			//Assert.assertEquals("200",res.statusCode());
			Thread.sleep(5000);
			setRes(res);

			}

	public Response getRes() {
			logger.debug("response in putRegistration getter: " + res.asPrettyString());
		return res;
	}

	public void setRes(Response res) {
		this.res = res;
	}

	public String getLeasingCompanyOrderDossierNumber() {
		return leasingCompanyOrderDossierNumber;
	}

	public void setLeasingCompanyOrderDossierNumber(String leasingCompanyOrderDossierNumber) {
		this.leasingCompanyOrderDossierNumber = leasingCompanyOrderDossierNumber;
	}

	public String getRegistrarDossierNumber() {
		return registrarDossierNumber;
	}

	public void setRegistrarDossierNumber(String registrarDossierNumber) {
		this.registrarDossierNumber = registrarDossierNumber;
	}

	public String getFormNumber() {
		return formNumber;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	public String getLesseeName() {
		return lesseeName;
	}

	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName;
	}

	public String getLesseeEnterpriseNumber() {
		return lesseeEnterpriseNumber;
	}

	public void setLesseeEnterpriseNumber(String lesseeEnterpriseNumber) {
		this.lesseeEnterpriseNumber = lesseeEnterpriseNumber;
	}

	public String getLicensePlateHolderName() {
		return licensePlateHolderName;
	}

	public void setLicensePlateHolderName(String licensePlateHolderName) {
		this.licensePlateHolderName = licensePlateHolderName;
	}

	public String getLicensePlateHolderEnterpriseNumber() {
		return licensePlateHolderEnterpriseNumber;
	}

	public void setLicensePlateHolderEnterpriseNumber(String licensePlateHolderEnterpriseNumber) {
		this.licensePlateHolderEnterpriseNumber = licensePlateHolderEnterpriseNumber;
	}

	public Long getPlannedRegistrationDate() {
		return plannedRegistrationDate;
	}

	public void setPlannedRegistrationDate(Long plannedRegistrationDate) {
		this.plannedRegistrationDate = plannedRegistrationDate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierEnterpriseNumber() {
		return supplierEnterpriseNumber;
	}

	public void setSupplierEnterpriseNumber(String supplierEnterpriseNumber) {
		this.supplierEnterpriseNumber = supplierEnterpriseNumber;
	}

	public String getInsuranceBrokerFsmaNumber() {
		return insuranceBrokerFsmaNumber;
	}

	public void setInsuranceBrokerFsmaNumber(String insuranceBrokerFsmaNumber) {
		this.insuranceBrokerFsmaNumber = insuranceBrokerFsmaNumber;
	}

	public String getInsuranceCompanyFsmaNumber() {
		return insuranceCompanyFsmaNumber;
	}

	public void setInsuranceCompanyFsmaNumber(String insuranceCompanyFsmaNumber) {
		this.insuranceCompanyFsmaNumber = insuranceCompanyFsmaNumber;
	}

	public String getInsuranceReferenceNumber() {
			logger.debug("insuranceReferenceNumber in getter: " + insuranceReferenceNumber);
		return insuranceReferenceNumber;
	}

	public void setInsuranceReferenceNumber(String insuranceReferenceNumber) {
		this.insuranceReferenceNumber = insuranceReferenceNumber;
	}

	public Long getInsuranceBeginDate() {
		return insuranceBeginDate;
	}

	public void setInsuranceBeginDate(Long insuranceBeginDate) {
		this.insuranceBeginDate = insuranceBeginDate;
	}

	public Long getInsurancePlannedRegistrationDate() {
		return insurancePlannedRegistrationDate;
	}

	public void setInsurancePlannedRegistrationDate(Long insurancePlannedRegistrationDate) {
		this.insurancePlannedRegistrationDate = insurancePlannedRegistrationDate;
	}

	public String getDriverLastName() {
		return driverLastName;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverAddressStreet() {
		return driverAddressStreet;
	}

	public void setDriverAddressStreet(String driverAddressStreet) {
		this.driverAddressStreet = driverAddressStreet;
	}

	public String getDriverAddressHouseNumber() {
		return driverAddressHouseNumber;
	}

	public void setDriverAddressHouseNumber(String driverAddressHouseNumber) {
		this.driverAddressHouseNumber = driverAddressHouseNumber;
	}

	public String getDriverAddressBoxNumber() {
		return driverAddressBoxNumber;
	}

	public void setDriverAddressBoxNumber(String driverAddressBoxNumber) {
		this.driverAddressBoxNumber = driverAddressBoxNumber;
	}

	public String getDriverAddressPostalCode() {
		return driverAddressPostalCode;
	}

	public void setDriverAddressPostalCode(String driverAddressPostalCode) {
		this.driverAddressPostalCode = driverAddressPostalCode;
	}

	public String getDriverAddressCity() {
		return driverAddressCity;
	}

	public void setDriverAddressCity(String driverAddressCity) {
		this.driverAddressCity = driverAddressCity;
	}

	public String getDriverLanguageCode() {
		return driverLanguageCode;
	}

	public void setDriverLanguageCode(String driverLanguageCode) {
		this.driverLanguageCode = driverLanguageCode;
	}

	public String getDriverMobileNumber() {
		return driverMobileNumber;
	}

	public void setDriverMobileNumber(String driverMobileNumber) {
		this.driverMobileNumber = driverMobileNumber;
	}

	public String getVehicleBrand() {
		return vehicleBrand;
	}

	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleVIN() {
		return vehicleVIN;
	}

	public void setVehicleVIN(String vehicleVIN) {
		this.vehicleVIN = vehicleVIN;
	}

	public String getVehicleVINControlNumber() {
		return vehicleVINControlNumber;
	}

	public void setVehicleVINControlNumber(String vehicleVINControlNumber) {
		this.vehicleVINControlNumber = vehicleVINControlNumber;
	}

	public Integer getVehicleCatalogueValue() {
		return vehicleCatalogueValue;
	}

	public void setVehicleCatalogueValue(Integer vehicleCatalogueValue) {
		this.vehicleCatalogueValue = vehicleCatalogueValue;
	}

	public Integer getVehicleCatalogueValueWithoutDiscount() {
		return vehicleCatalogueValueWithoutDiscount;
	}

	public void setVehicleCatalogueValueWithoutDiscount(Integer vehicleCatalogueValueWithoutDiscount) {
		this.vehicleCatalogueValueWithoutDiscount = vehicleCatalogueValueWithoutDiscount;
	}

	public Integer getVehicleBiv() {
		return vehicleBiv;
	}

	public void setVehicleBiv(Integer vehicleBiv) {
		this.vehicleBiv = vehicleBiv;
	}

	public String getVehicleCostCentre() {
		return vehicleCostCentre;
	}

	public void setVehicleCostCentre(String vehicleCostCentre) {
		this.vehicleCostCentre = vehicleCostCentre;
	}

	public String getVehicleUsedVehicle() {
		return vehicleUsedVehicle;
	}

	public void setVehicleUsedVehicle(String vehicleUsedVehicle) {
		this.vehicleUsedVehicle = vehicleUsedVehicle;
	}

	public String getVehiclePriorLicensePlate() {
		return vehiclePriorLicensePlate;
	}

	public void setVehiclePriorLicensePlate(String vehiclePriorLicensePlate) {
		this.vehiclePriorLicensePlate = vehiclePriorLicensePlate;
	}

	public Long getVehicleLastRegistrationDate() {
		return vehicleLastRegistrationDate;
	}

	public void setVehicleLastRegistrationDate(Long vehicleLastRegistrationDate) {
		this.vehicleLastRegistrationDate = vehicleLastRegistrationDate;
	}

	public String getVehicleRegistrationDocumentsLanguage() {
		return vehicleRegistrationDocumentsLanguage;
	}

	public void setVehicleRegistrationDocumentsLanguage(String vehicleRegistrationDocumentsLanguage) {
		this.vehicleRegistrationDocumentsLanguage = vehicleRegistrationDocumentsLanguage;
	}

	public String getVehicleEmailGreenCard() {
		return vehicleEmailGreenCard;
	}

	public void setVehicleEmailGreenCard(String vehicleEmailGreenCard) {
		this.vehicleEmailGreenCard = vehicleEmailGreenCard;
	}

	public String getVehicleReuseLicensePlate() {
		return vehicleReuseLicensePlate;
	}

	public void setVehicleReuseLicensePlate(String vehicleReuseLicensePlate) {
		this.vehicleReuseLicensePlate = vehicleReuseLicensePlate;
	}

	public String getVehicleLicensePlate() {
		return vehicleLicensePlate;
	}

	public void setVehicleLicensePlate(String vehicleLicensePlate) {
		this.vehicleLicensePlate = vehicleLicensePlate;
	}

	public String getVehicleLicensePlateFormat() {
		return vehicleLicensePlateFormat;
	}

	public void setVehicleLicensePlateFormat(String vehicleLicensePlateFormat) {
		this.vehicleLicensePlateFormat = vehicleLicensePlateFormat;
	}

	public String getVehicleFrontPlateOrdered() {
		return vehicleFrontPlateOrdered;
	}

	public void setVehicleFrontPlateOrdered(String vehicleFrontPlateOrdered) {
		this.vehicleFrontPlateOrdered = vehicleFrontPlateOrdered;
	}

	public String getVehicleSpeedPedelecFlag() {
		return vehicleSpeedPedelecFlag;
	}

	public void setVehicleSpeedPedelecFlag(String vehicleSpeedPedelecFlag) {
		this.vehicleSpeedPedelecFlag = vehicleSpeedPedelecFlag;
	}

	public String getVehiclePedalingAssistance() {
		return vehiclePedalingAssistance;
	}

	public void setVehiclePedalingAssistance(String vehiclePedalingAssistance) {
		this.vehiclePedalingAssistance = vehiclePedalingAssistance;
	}

	public String getDeliveryBoxNumber() {
		return deliveryBoxNumber;
	}

	public void setDeliveryBoxNumber(String deliveryBoxNumber) {
		this.deliveryBoxNumber = deliveryBoxNumber;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public String getDeliveryFirstName() {
		return deliveryFirstName;
	}

	public void setDeliveryFirstName(String deliveryFirstName) {
		this.deliveryFirstName = deliveryFirstName;
	}

	public String getDeliveryHouseNumber() {
		return deliveryHouseNumber;
	}

	public void setDeliveryHouseNumber(String deliveryHouseNumber) {
		this.deliveryHouseNumber = deliveryHouseNumber;
	}

	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}

	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public void setDeliveryStreet(String deliveryStreet) {
		this.deliveryStreet = deliveryStreet;
	}

	public String getDeliveryLastNameOrCompanyName() {
		return deliveryLastNameOrCompanyName;
	}

	public void setDeliveryLastNameOrCompanyName(String deliveryLastNameOrCompanyName) {
		this.deliveryLastNameOrCompanyName = deliveryLastNameOrCompanyName;
	}
}





