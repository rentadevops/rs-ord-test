package pageobjects.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import apiEngine.model.get.order.timeline.wallbox.WallBoxOrderTimeLine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.model.get.order.timeline.Agent;
import apiEngine.model.get.order.timeline.DeliveryDates;
import apiEngine.model.get.order.timeline.FollowUpDates;
import apiEngine.model.get.order.timeline.History;
import apiEngine.model.get.order.timeline.InscriptionCosts;
import apiEngine.model.get.order.timeline.OrderTimeline;
import apiEngine.model.get.order.timeline.TaxStampCosts;
import apiEngine.model.get.order.timeline.TotalPrice;
import apiEngine.model.get.order.timeline.Vehicle;

public class API_GetOrderTimelineById {
	final Logger logger = LogManager.getLogger(API_GetOrderTimelineById.class);

	
	Agent agent;
	Integer creationDate;
	String currentStatus;
	String deliveryContact;
	DeliveryDates deliveryDates;
	Integer eventVersion;
	FollowUpDates followUpDates;
	List<History> history = null;
	String id;
	InscriptionCosts inscriptionCosts;
	Boolean leasingCompanyActionNeeded;
	Integer leasingCompanyId;
	String leasingType;
	String linkedRegistrationId;
	String orderType;
	Boolean previousVehicleDropoff;
	Integer proposedDeliveryDate;
	String registrationDivToDoBy;
	Boolean supplierActionNeeded;
	Integer supplierId;
	TaxStampCosts taxStampCosts;
	TotalPrice totalPrice;
	Vehicle vehicle;
	Integer version;
	
	Integer modelYear;
	Integer mileage;

	
	public API_GetOrderTimelineById(OrderTimeline orderTimeline) {
		
	}
	
	public void get_order_timeline_by_id_data(OrderTimeline orderTimeline) throws ParseException {
		
		modelYear = orderTimeline.getVehicle().getConstructionYear();
		logger.debug("modelYear: " + modelYear.toString());
		mileage = orderTimeline.getVehicle().getMileage();
		
		
		//String FirstRegistrationDateToFormat = leasingCompanyContract.vehicle.dateFirstRegistration;
		//Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(FirstRegistrationDateToFormat);
		//String FirstRegistrationDateFormatted = new SimpleDateFormat("dd/MM/yyyy").format(date1);
		//FirstRegistrationDate = FirstRegistrationDateFormatted;
		
		//if(leasingCompanyContract.vehicle.febiacInfo.version != null) {
			//Version = leasingCompanyContract.vehicle.febiacInfo.version;
		//}
		
	}

	public void get_wallbox_order_timeline_by_id_data(WallBoxOrderTimeLine wallBoxOrderTimeline) throws ParseException {



	}
}
		
		