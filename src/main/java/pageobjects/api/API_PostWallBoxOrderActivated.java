package pageobjects.api;

import apiEngine.Endpoints;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import jakarta.xml.bind.JAXBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

//import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class API_PostWallBoxOrderActivated {
	final Logger logger = LogManager.getLogger(API_PostWallBoxOrderActivated.class);

	private String xmlns;
	private String dosNr;


	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());

	public static java.util.Date addMonths(java.util.Date today, int monthsToAdd) {
		if (today != null) {
			java.util.Calendar c = java.util.Calendar.getInstance();
			c.setTime(today);
			c.add(Calendar.MONTH, monthsToAdd);
			return c.getTime();
		} else {
			return null;
		}
	}

	public static java.util.Date addDays(java.util.Date today, int daysToAdd) {
		if (today != null) {
			java.util.Calendar c = java.util.Calendar.getInstance();
			c.setTime(today);
			c.add(Calendar.DAY_OF_YEAR, daysToAdd);
			return c.getTime();
		} else {
			return null;
		}
	}

	Date cal = addMonths(new Date(), 1);
	Date calPlusDays = addDays(new Date(), 3);
	String dateTodayPlusOneMonth = simpleDateFormat.format(cal);
	String dateTodayPlus3Days = simpleDateFormat.format(calPlusDays);
	String activationDate = dateTodayPlus3Days;
	String remoteDir;
	String id;
	String status;


		public API_PostWallBoxOrderActivated() {
			
		}
		
		public void postWallBoxOrderActivated(String id) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException {
			String json = "{\n" +
					"  \"activationDate\": \"" + activationDate +"\"\n" +
					"}";
			status = Endpoints.postWallBoxOrderActivated(id, json);
			logger.debug("response postWallBoxOrderActivated:" + status);
			Assert.assertEquals(status.trim(), "HTTP/1.1 200");
			Thread.sleep(5000);

			}

	public String getActivationDate() {
		return activationDate;
	}

	public void setActivationDateDate(String activationDate) {
		this.activationDate = activationDate;
	}
}





