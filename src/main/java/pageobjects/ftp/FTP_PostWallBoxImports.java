package pageobjects.ftp;

import java.io.*;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import apiEngine.model.post.wallbox.imports.Address;
import apiEngine.model.post.wallbox.imports.Client;
import apiEngine.model.post.wallbox.imports.Contact;
import apiEngine.model.post.wallbox.imports.Delivery;
import apiEngine.model.post.wallbox.imports.Discount;
import apiEngine.model.post.wallbox.imports.Header;
import apiEngine.model.post.wallbox.imports.Meta;
import apiEngine.model.post.wallbox.imports.Pricing;
import apiEngine.model.post.wallbox.imports.TransactionCode;
import apiEngine.model.post.wallbox.imports.Translatable;
import apiEngine.model.post.wallbox.imports.Vehicle;
import apiEngine.model.post.wallbox.imports.WallBox;
import apiEngine.model.post.wallbox.imports.WallBoxImports;
import apiEngine.model.post.wallbox.imports.WallBoxOrder;
import common.SftpConnect;
import cucumber.TestContext;
import helpers.ORD_ContractNumberGenerator;
import io.cucumber.datatable.DataTable;
import managers.PageObjectManager;
import managers.WebDriverManager_ORD;
import pageobjects.api.API_GetOrderByDossierNumber;

public class FTP_PostWallBoxImports {
	final Logger logger = LogManager.getLogger(FTP_PostWallBoxImports.class);
	
	private String xmlns;
	//Meta
	private String dosNr;
	private String dosNrWB;
	private String transactionCode;
	private String leasingCompanyRSNumber;
	private String DealerRSNumber;
	private String userEmail;
	private String versionNumber;
	private String linkedOrderNumber;
	private String comment;
	//Client
	private String clientName;
	private String clientContactPerson;
	private String clientBillingAddress;
	private String clientLanguageCode;
	private String clientVAT;
	private String clientStreetHouseNumber;
	private String clientPostalCode;
	private String clientCity;
	private String clientContactEmail;
	private String clientContactPhoneNumber;
	private String clientContactMobileNumber;
	private String vehicleBrand;
	private String vehicleModel;
	private String wallboxBrand;
	private String wallboxModel;
	private String wallboxDescription;
	private String pricingTotalPrice;
	private String pricingTotalPriceInput;
	private String pricingListPrice;
	private String pricingListPriceInput;
	private String pricingDiscountAmount;
	private String pricingDiscountPercentage;
	private String pricingApplicableVatPercentage;

	private String deliveryDesiredDate;
	private String deliveryLocationLanguageCode;
	private String deliveryStreetHouseNumber;
	private String deliveryPostalCode;
	private String deliveryCity;
	private String deliveryContactEmail;
	private String deliveryContactPhoneNumber;
	private String deliveryContactMobileNumber;
	private String deliveryContactPerson;
	private String deliveryLocationVAT;
	private String leasingType;



	API_GetOrderByDossierNumber getOrderByDossierNumber;
	FTP_PostOrderImportsRequest por;

		public FTP_PostWallBoxImports() throws InterruptedException {
			//getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		}
		
		public void upload_wallboxImports(String leasingType) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, JAXBException {
			
			//List<List<String>> data = table.asLists();
			
			//TODO implement data in feature file and datatable
			WallBoxImports wallBoxImports = null;;
			
			xmlns = "http://be/renta/ordering/communication/wallboximport";
			transactionCode = "CREATE";
			//leasingCompanyRSNumber = "799189";
			leasingCompanyRSNumber = "799230";
			//DealerRSNumber = "730086";
			DealerRSNumber = "800404";
			userEmail = "rstest2@rentasolutions.org";
			versionNumber = "1.0";
			linkedOrderNumber = "16-03-22TestWB5";
			//linkedOrderNumber = getOrderByDossierNumber.leasingCompanyDossierNumber;
			Properties properties = new Properties();

			InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
			properties.load(inputDriver);
			dosNr = properties.getProperty("DossierNumber");
			logger.debug("dosNr " + dosNr +  " van proprty file gelezen");
			//linkedOrderNumber = dosNr;
			linkedOrderNumber = "16-03-22TestWB5";

			logger.debug("linkedOrderNumber: " + linkedOrderNumber);
					comment = "I create a wallbox order";
			clientName = "A x Ω";
			clientContactPerson = "MENNEKE PEACE";
			clientLanguageCode = "nl";
			clientVAT = "BE0414325701";
			clientStreetHouseNumber = "Robert Schumanplein 2-4";
			clientPostalCode = "1040";
			clientCity = "ETTERBEEK";
			clientBillingAddress = clientStreetHouseNumber + " , " + clientPostalCode + " " + clientCity;
			clientContactEmail = "contact@test.be";
			clientContactPhoneNumber = "0031765733060";
			clientContactMobileNumber = "0475772277";
			vehicleBrand = "BMW";
			vehicleModel = "X1 15";
			wallboxBrand = "Pulsar";
			wallboxModel = "Pulsar Plus";
			wallboxDescription = "Wallbox Pulsar Plus white charger";
			pricingTotalPriceInput = "26795,46";
			pricingListPriceInput = "28801,66";
			pricingDiscountAmount = "120,00";
			pricingDiscountPercentage = "10,00 %";
			pricingApplicableVatPercentage = "19,91 %";
			deliveryDesiredDate = "2020-08-15";
			deliveryLocationLanguageCode = "nl";
			deliveryContactPerson = "Tim De Leverancier";
			deliveryLocationVAT = "BE0720879353";
			deliveryStreetHouseNumber = "Mechelbaan 2";
			deliveryPostalCode = "2500";
			deliveryCity = "Lier";
			deliveryContactEmail = "contactlevering@mail.be";
			deliveryContactPhoneNumber = "015317531";
			deliveryContactMobileNumber = "0477771177";

			
			System.setProperty("leasingType", leasingType);

			
			ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
			dosNrWB = mcng.getRandomContractNumber();
			Properties p = new Properties();
	        p.put("DossierNumber", dosNrWB);
	        FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/dossiernumberWB.properties");
	        p.store(fos, "");
	        logger.debug("dosNrWB " + dosNrWB +  " naar proprty file geschreven");
			
	        wallBoxImports = new WallBoxImports();
			Header header = new Header();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			   LocalDateTime now = LocalDateTime.now();  
			   header.setCreatedDate(dtf.format(now));
			   WallBoxOrder wallBoxOrder = new WallBoxOrder();
				Meta meta = new Meta();
					meta.setTransactionCode(TransactionCode.CREATE);
					meta.setDossierNumber(dosNrWB);
					meta.setLeasingCompanyRSNumber(leasingCompanyRSNumber);
					meta.setDealerRSNumber(DealerRSNumber);
					meta.setUserEmail(userEmail);
					meta.setVersionNumber(versionNumber);
					meta.setLinkedOrderNumber(linkedOrderNumber);
					meta.setComment(comment);
				Client client = new Client();
				client.setName(clientName);
				client.setContactPerson(clientContactPerson);
				client.setLanguageCode(clientLanguageCode);
				client.setVAT(clientVAT);
					Address addressClient = new Address();
					addressClient.setStreetHouseNumber(clientStreetHouseNumber);
					addressClient.setPostalCode(clientPostalCode);
					addressClient.setCity(clientCity);
					Contact contactClient = new Contact();
					contactClient.setEmail(clientContactEmail);
					contactClient.setPhoneNumber(clientContactPhoneNumber);
					contactClient.setMobileNumber(clientContactMobileNumber);
				client.setAddress(addressClient);
				client.setContact(contactClient);
				Vehicle vehicle = new Vehicle();
				vehicle.setBrand(vehicleBrand);
				Translatable translatable = new Translatable();
					translatable.setDutch(vehicleModel);
					vehicle.setModel(translatable);
				WallBox wallBox = new WallBox();
					wallBox.setBrand(wallboxBrand);
					translatable = new Translatable();
					translatable.setDutch(wallboxModel);
					wallBox.setModel(translatable);
					translatable = new Translatable();
					translatable.setDutch(wallboxDescription);
					wallBox.setDescription(translatable);
				Pricing pricing = new Pricing();
				pricing.setTotalPrice(pricingTotalPriceInput);
				pricing.setListPrice(pricingListPriceInput);
					Discount discount = new Discount();
					discount.setAmount(pricingDiscountAmount);
					discount.setPercentage(pricingDiscountPercentage);
				pricing.setDiscount(discount);
				pricing.setApplicableVatPercentage(pricingApplicableVatPercentage);


				Delivery delivery = new Delivery();
				delivery.setDesiredDate(deliveryDesiredDate);
				delivery.setContactPerson(deliveryContactPerson);
				delivery.setLocationVAT(deliveryLocationVAT);
					Address addressDelivery = new Address();
					addressDelivery.setStreetHouseNumber(deliveryStreetHouseNumber);
					addressDelivery.setPostalCode(deliveryPostalCode);
					addressDelivery.setCity(deliveryCity);
					Contact contactDelivery = new Contact();
					contactDelivery.setEmail(deliveryContactEmail);
					contactDelivery.setPhoneNumber(deliveryContactPhoneNumber);
					contactDelivery.setMobileNumber(deliveryContactMobileNumber);
					delivery.setAddress(addressDelivery);
					delivery.setContact(contactDelivery);


					wallBoxOrder.setMeta(meta);
					wallBoxOrder.setClient(client);
					wallBoxOrder.setVehicle(vehicle);
					wallBoxOrder.setWallBox(wallBox);
					wallBoxOrder.setPricing(pricing);
					wallBoxOrder.setDelivery(delivery);
					List<WallBoxOrder> listWallBoxOrder= new ArrayList<>();
					listWallBoxOrder.add(wallBoxOrder);

		        
					//wallBoxImports.set_xmlns(xmlns);
					wallBoxImports.setHeader(header);
					wallBoxImports.setWallBoxOrder(listWallBoxOrder);
			
				
				 JAXBContext jaxbContext = JAXBContext.newInstance(wallBoxImports.getClass());
		            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		            //jaxbMarshaller.marshal(data, System.out);
		            StringWriter sw = new StringWriter();
		            jaxbMarshaller.marshal(wallBoxImports, sw);
		            String xml = sw.toString();
		            logger.debug("Xml: " + xml);
		            
		            BufferedWriter writer = new BufferedWriter(new FileWriter("./src/test/resources/runtime/uploadWallBoxOrder.xml"));
		            writer.write(xml);
		            
		            writer.close();
		            
		            SftpConnect sc = new SftpConnect();
		            ChannelSftp channelSftp = sc.setupJsch();
		    	    channelSftp.connect();
		    	 
		    	    String localFile = "./src/test/resources/runtime/uploadWallBoxOrder.xml";

		    	    String remoteDir = "/799189/ORD/in";
		    	 
		    	    channelSftp.put(localFile, remoteDir);
		    	 
		    	    channelSftp.exit();
				
				
			}

		

		public String getXmlns() {
			return xmlns;
		}

		public void setXmlns(String xmlns) {
			this.xmlns = xmlns;
		}

		public String getDosNrWB() {
			logger.debug("dosNrWB in getter: " + dosNrWB);
			return dosNrWB;
		}

		public void setDosNrWB(String dosNrWB) {
			this.dosNrWB = dosNrWB;
		}

		public String getLeasingType() {

			return leasingType;
		}

		public void setLeasingType(String leasingType) {
		this.leasingType = leasingType;
	}

		public String getTransactionCode() {
			return transactionCode;
		}

		public void setTransactionCode(String transactionCode) {
			this.transactionCode = transactionCode;
		}

		public String getLeasingCompanyRSNumber() {
			return leasingCompanyRSNumber;
		}

		public void setLeasingCompanyRSNumber(String leasingCompanyRSNumber) {
			this.leasingCompanyRSNumber = leasingCompanyRSNumber;
		}

		public String getDealerRSNumber() {
			return DealerRSNumber;
		}

		public void setDealerRSNumber(String dealerRSNumber) {
			DealerRSNumber = dealerRSNumber;
		}

		public String getUserEmail() {
			return userEmail;
		}

		public void setUserEmail(String userEmail) {
			this.userEmail = userEmail;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public String getClientName() {
			return clientName;
		}

		public void setClientName(String clientName) {
			this.clientName = clientName;
		}
		
		public String getClientBillingAddress() {
			return clientBillingAddress;
		}

		public void setClientBillingAddress(String clientBillingAddress) {
			this.clientBillingAddress = clientBillingAddress;
		}

		public String getClientContactPerson() {
			return clientContactPerson;
		}

		public void setClientContactPerson(String clientContactPerson) {
			this.clientContactPerson = clientContactPerson;
		}

		public String getClientLanguageCode() {
			if(clientLanguageCode.equals("nl")) {
				clientLanguageCode = "DUTCH";
			}
			if(clientLanguageCode.equals("fr")) {
				clientLanguageCode = "FRENCH";
			}
			if(clientLanguageCode.equals("en")) {
				clientLanguageCode = "ENGLISH";
			}
			if(clientLanguageCode.equals("de")) {
				clientLanguageCode = "DEUTSCH";
			}
			return clientLanguageCode;
		}

		public void setClientLanguageCode(String clientLanguageCode) {
			this.clientLanguageCode = clientLanguageCode;
		}

		public String getClientVAT() {
			return clientVAT;
		}

		public void setClientVAT(String clientVAT) {
			this.clientVAT = clientVAT;
		}

		public String getClientStreetHouseNumber() {
			return clientStreetHouseNumber;
		}

		public void setClientStreetHouseNumber(String clientStreetHouseNumber) {
			this.clientStreetHouseNumber = clientStreetHouseNumber;
		}

		public String getClientPostalCode() {
			return clientPostalCode;
		}

		public void setClientPostalCode(String clientPostalCode) {
			this.clientPostalCode = clientPostalCode;
		}

		public String getClientCity() {
			return clientCity;
		}

		public void setClientCity(String clientCity) {
			this.clientCity = clientCity;
		}

		public String getClientContactEmail() {
			return clientContactEmail;
		}

		public void setClientContactEmail(String clientContactEmail) {
			this.clientContactEmail = clientContactEmail;
		}

		public String getClientContactPhoneNumber() {
			return clientContactPhoneNumber;
		}

		public void setClientContactPhoneNumber(String clientContactPhoneNumber) {
			this.clientContactPhoneNumber = clientContactPhoneNumber;
		}

		public String getClientContactMobileNumber() {
			return clientContactMobileNumber;
		}

		public void setClientContactMobileNumber(String clientContactMobileNumber) {
			this.clientContactMobileNumber = clientContactMobileNumber;
		}


		public String getVehicleBrand() {
			return vehicleBrand;
		}

		public void setVehicleBrand(String vehicleBrand) {
			this.vehicleBrand = vehicleBrand;
		}


		public String getVehicleModel() {
			return vehicleModel;
		}

		public void setVehicleModel(String vehicleModel) {
			this.vehicleModel = vehicleModel;
		}

		public String getWallboxModel() {
		return wallboxModel;
	}

		public void setWallboxModel(String wallboxModel) {
		this.wallboxModel = wallboxModel;
	}

		public String getWallboxBrand() {
		return wallboxBrand;
	}

		public void setWallboxBrand(String wallboxBrand) {
		this.wallboxBrand = wallboxBrand;
	}

		public String getWallboxDescription() {
		return wallboxDescription;
	}

		public void setWallboxDescription(String wallboxDescription) {
		this.wallboxDescription = wallboxDescription;
	}

		public String getPricingTotalPrice() {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			pricingTotalPriceInput = pricingTotalPriceInput.replace("," , ".");
			Double DoublePricingTotalPrice = Double.parseDouble(pricingTotalPriceInput);
			pricingTotalPrice = decimalFormat.format(DoublePricingTotalPrice).toString();
			logger.debug("pricingTotalPrice: " + pricingTotalPrice);
			return pricingTotalPrice;
		}

		public void setPricingTotalPrice(String pricingTotalPrice) {
			this.pricingTotalPrice = pricingTotalPrice;
		}

		public String getPricingListPrice() {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			pricingListPriceInput = pricingListPriceInput.replace("," , ".");
			Double DoubleListPrice = Double.parseDouble(pricingListPriceInput);
			pricingListPrice = decimalFormat.format(DoubleListPrice).toString();
			logger.debug("pricingListPrice: " + pricingListPrice);
			return pricingListPrice;
		}

		public void setPricingListPrice(String pricingListPrice) {
			this.pricingListPrice = pricingListPrice;
		}

		public String getPricingDiscountAmount() {
			return pricingDiscountAmount;
		}

		public void setPricingDiscountAmount(String pricingDiscountAmount) {
			this.pricingDiscountAmount = pricingDiscountAmount;
		}

		public String getPricingDiscountPercentage() {
			return pricingDiscountPercentage;
		}

		public void setPricingDiscountPercentage(String pricingDiscountPercentage) {
			this.pricingDiscountPercentage = pricingDiscountPercentage;
		}


		public String getDeliveryDesiredDate() {
			return deliveryDesiredDate;
		}

		public void setDeliveryDesiredDate(String deliveryDesiredDate) {
			this.deliveryDesiredDate = deliveryDesiredDate;
		}

		public String getDeliveryLocationLanguageCode() {
			if(deliveryLocationLanguageCode.equals("nl")) {
				deliveryLocationLanguageCode = "Dutch";
			}
			if(deliveryLocationLanguageCode.equals("fr")) {
				deliveryLocationLanguageCode = "French";
			}
			if(deliveryLocationLanguageCode.equals("en")) {
				deliveryLocationLanguageCode = "English";
			}
			if(deliveryLocationLanguageCode.equals("de")) {
				deliveryLocationLanguageCode = "Deutsch";
			}
			return deliveryLocationLanguageCode;
		}

		public void setDeliveryLocationLanguageCode(String deliveryLocationLanguageCode) {
			this.deliveryLocationLanguageCode = deliveryLocationLanguageCode;
		}

		public String getDeliveryContactPerson() {
			return deliveryContactPerson;
		}

		public void setDeliveryContactPerson(String deliveryContactPerson) {
			this.deliveryContactPerson = deliveryContactPerson;
		}

		public String getDeliveryLocationVAT() {
			return deliveryLocationVAT;
		}

		public void setDeliveryLocationVAT(String deliveryLocationVAT) {
			this.deliveryLocationVAT = deliveryLocationVAT;
		}

		public String getDeliveryStreetHouseNumber() {
			return deliveryStreetHouseNumber;
		}

		public void setDeliveryStreetHouseNumber(String deliveryStreetHouseNumber) {
			this.deliveryStreetHouseNumber = deliveryStreetHouseNumber;
		}

		public String getDeliveryPostalCode() {
			return deliveryPostalCode;
		}

		public void setDeliveryPostalCode(String deliveryPostalCode) {
			this.deliveryPostalCode = deliveryPostalCode;
		}

		public String getDeliveryCity() {
			return deliveryCity;
		}

		public void setDeliveryCity(String deliveryCity) {
			this.deliveryCity = deliveryCity;
		}

		public String getDeliveryContactEmail() {
			return deliveryContactEmail;
		}

		public void setDeliveryContactEmail(String deliveryContactEmail) {
			this.deliveryContactEmail = deliveryContactEmail;
		}

		public String getDeliveryContactPhoneNumber() {
			return deliveryContactPhoneNumber;
		}

		public void setDeliveryContactPhoneNumber(String deliveryContactPhoneNumber) {
			this.deliveryContactPhoneNumber = deliveryContactPhoneNumber;
		}

		public String getDeliveryContactMobileNumber() {
			return deliveryContactMobileNumber;
		}

		public void setDeliveryContactMobileNumber(String deliveryContactMobileNumber) {
			this.deliveryContactMobileNumber = deliveryContactMobileNumber;
		}

	}





