package pageobjects.ftp;


//import apiEngine.model.post.order.imports.migration.*;
import apiEngine.model.post.order.imports.migration.*;
import com.github.javafaker.Faker;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import common.SftpConnect;
import helpers.ORD_ContractNumberGenerator;
import io.cucumber.datatable.DataTable;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;
import java.io.*;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class FTP_PostOrderImportsMigrate {
	final Logger logger = LogManager.getLogger(FTP_PostOrderImportsMigrate.class);
	WebDriver driver;
	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());

	private String xmlns;
	private String dosNr;
	private String transactionCode;
	private OrderStatus orderStatus;
	private String dealerDossierNumber;
	private String leasingCompanyRSNumber;
	private String leasingCompanyRSNumberLux;
	private String DealerRSNumber;
	private String userEmail;
	private String userEmailLux;
	private String trackingNumber;
	private String versionNumber;
	private String dateFirstRegistration;
	private String comment;
	private String clientName;
	private String clientNameLux;
	private String clientContactPerson;
	private String clientContactPersonLux;
	private String clientBillingAddress;
	private String clientLanguageCode;
	private String clientLanguageCodeLux;
	private String clientVAT;
	private String clientVATLux;
	private String clientStreetHouseNumber;
	private String clientPostalCode;
	private String clientCity;
	private String clientStreetHouseNumberLux;
	private String clientPostalCodeLux;
	private String clientCityLux;
	private String clientContactEmail;
	private String clientContactEmailLux;
	private String clientContactPhoneNumber;
	private String clientContactPhoneNumberLux;
	private String clientContactMobileNumber;
	private String clientContactMobileNumberLux;
	private String clientContactFaxNumber;
	private String clientContactFaxNumberLux;
	private String registrationNumberLessee;
	private String driverStreetHouseNumber;
	private String driverPostalCode;
	private String driverCity;
	private String driverAddress;
	private String driverContactEmail;
	private String driverContactPhoneNumber;
	private String driverContactMobileNumber;
	private String driverContactEmailLux;
	private String driverContactPhoneNumberLux;
	private String driverContactMobileNumberLux;
	private String driverContactFaxNumber;
	private String driverNameImport;
	private String driverNameImportLux;
	private String driverLanguageCode;
	private String driverLanguageCodeLux;
	private String vehicleBrand;
	private String vehicleBrandLux;
	private String vehicleCylinderContent;
	private String vehicleCylinderContentLux;
	private String vehiclePower;
	private String vehiclePowerLux;
	private String vehicleLanguagePapers;
	private String vehicleLanguagePapersLux;
	private String vehicleWeight;
	private String vehicleWeightLux;
	private String vehicleTyreDescription;
	private String vehicleTyreDescriptionLux;
	private String vehicleStockCar;
	private String vehicleStockCarLux;
	private String vehicleCo2Emissions;
	private String vehicleCo2EmissionsLux;
	private String vehicleIdentification;
	private String vehicleModel;
	private String vehicleModelLux;
	private String vehicleVersion;
	private String vehicleExteriorColor;
	private String vehicleExteriorColorLux;
	private String vehicleInteriorColor;
	private String vehicleInteriorColorLux;
	private String vehicleDescription;
	private String vehicleDescriptionLux;
	private String pricingTotalPrice = null;
	private String pricingTotalPriceTemp = null;
	private String pricingTotalPriceLux = null;
	private String pricingTotalPriceInput;
	private String pricingListPrice;
	private String pricingListPriceInputLux;
	private String pricingListPriceInput;
	private String pricingFleetDiscount;
	private String pricingFleetDiscountAmount;
	private String pricingFleetDiscountPercentage;
	private String pricingFleetDiscountAmountLux;
	private String pricingFleetDiscountPercentageLux;
	private String pricingDeliveryCostsIni;
	private String pricingDeliveryCosts;
	private String pricingOtherCosts;
	private String pricingDiscountAmount;
	private String pricingDiscountPercentage;
	private String promotionName;
	private String promotionDescription;
	private String promotionDiscountAmount;
	private String deliveryDesiredDate;
	private String deliveryDesiredDateLux;
	private String deliveryLocationName;
	private String deliveryLocationLanguageCode;
	private String deliveryContactPerson;
	private String deliveryLocationVAT;
	private String deliveryStreetHouseNumber;
	private String deliveryPostalCode;
	private String deliveryCity;
	private String deliveryLocationAddress;
	private String deliveryContactEmail;
	private String deliveryContactPhoneNumber;
	private String deliveryContactMobileNumber;
	private String deliveryContactFaxNumber;
	private String insuranceName;
	private String insuranceEmail;
	private String insuranceValuePrice;
	private String buybackName;
	private String buybackAmount;
	private String buybackContractDuration;
	private String buybackStreetHouseNumber;
	private String buybackPostalCode;
	private String buybackCity;
	private Applicant registrationDivToDoBy;
	private String leasingType;
	private Options options;
	private Insurance insurance;
	private Promotions promotions;
	private RegistrationInfo registrationInfo;
	private String vehicleVin;
	private String vehicleVinControlNumber;

	//String pattern = "yyyy-MM-dd";
	//SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	//String dateToday = simpleDateFormat.format(new Date());
	String remoteDir;
	String vlgNrStr;
	Integer volgNr;

		public FTP_PostOrderImportsMigrate(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
			
		}
		
		public void upload_migrateOrderImportsRequest(String leasingType, List<List<String>> data) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, InterruptedException, JAXBException, JAXBException {
			
			//List<List<String>> data = table.asLists();
			Faker faker = new Faker(new Locale("nl_BE"));
			
			//TODO implement data in feature file and datatable
			OrderImports orderImports = null;
			
			xmlns = "http://be/renta/ordering/communication/orderimport";
			transactionCode = data.get(1).get(1);
			String stat = data.get(1).get(2);
			if(stat.equals("NEW")) {
				orderStatus = OrderStatus.NEW;
			} else if(stat.equals("ACCEPTED")) {
				orderStatus = OrderStatus.ACCEPTED;
			}  else if(stat.equals("ORDERED")) {
				orderStatus = OrderStatus.ORDERED;
			}  else if(stat.equals("VEHICLE_IN")) {
				orderStatus = OrderStatus.VEHICLE_IN;
			}

			//dealerDossierNumber = data.get(1).get(3);
			//dealerDossierNumber = faker.bothify("??????????-######");
			dealerDossierNumber = faker.bothify("UPLOAD-######");
			//leasingCompanyRSNumber = "799189";
			leasingCompanyRSNumber = data.get(1).get(4);
			//leasingCompanyRSNumberLux = "799330";
			leasingCompanyRSNumberLux = "799340";
			DealerRSNumber = data.get(1).get(5);
			userEmail = data.get(1).get(6);
			userEmailLux = "ui_team@rentasolutions.org";
			//trackingNumber = data.get(1).get(7);
			trackingNumber = faker.internet().uuid();
			//versionNumber = data.get(1).get(8);
			versionNumber = String.valueOf(faker.number().numberBetween(1, 100));
			dateFirstRegistration = data.get(1).get(9);
			//dateFirstRegistration = simpleDateFormat.format(faker.date());
			comment = faker.lorem().sentence();
			//clientName = data.get(1).get(10);
			clientName = faker.company().name();
			clientNameLux = "Deloitte Luxembourg";
			//clientContactPerson = data.get(1).get(11);
			clientContactPerson = faker.name().name();
			clientLanguageCode = data.get(1).get(12);
			clientLanguageCodeLux = "de";
			clientVAT = data.get(1).get(13);
			//clientVAT = faker.finance().iban();
			//clientStreetHouseNumber = data.get(1).get(14);
			clientStreetHouseNumber = faker.address().streetName() + " " + faker.address().buildingNumber();
			//clientPostalCode = data.get(1).get(15);
			clientPostalCode = faker.address().zipCode();
			//clientCity = data.get(1).get(16);
			clientCity = faker.address().cityName();
			clientBillingAddress = clientStreetHouseNumber + " , " + clientPostalCode + " " + clientCity;
			//clientContactEmail = data.get(1).get(17);
			clientContactEmail = faker.internet().safeEmailAddress();
			//clientContactPhoneNumber = data.get(1).get(18);
			clientContactPhoneNumber = faker.phoneNumber().phoneNumber();
			//clientContactMobileNumber = data.get(1).get(19);
			clientContactMobileNumber = faker.phoneNumber().cellPhone();
			//clientContactFaxNumber = data.get(1).get(20);
			clientContactFaxNumber = faker.phoneNumber().phoneNumber();
			//driverStreetHouseNumber = data.get(1).get(21);
			driverStreetHouseNumber = faker.address().streetName() + " " + faker.address().buildingNumber();
			//driverPostalCode = data.get(1).get(22);
			driverPostalCode = faker.address().zipCode();
			//driverCity = data.get(1).get(23);
			driverCity = faker.address().cityName();
			driverAddress = driverStreetHouseNumber + " , " + driverPostalCode + " " + driverCity;
			//driverContactEmail = data.get(1).get(24);
			driverContactEmail = faker.internet().safeEmailAddress();
			//driverContactPhoneNumber = data.get(1).get(25);
			driverContactPhoneNumber = faker.phoneNumber().phoneNumber();
			//driverContactMobileNumber = data.get(1).get(26);
			driverContactMobileNumber = faker.phoneNumber().cellPhone();
			//driverContactFaxNumber = data.get(1).get(27);
			driverContactFaxNumber = faker.phoneNumber().phoneNumber();
			//driverNameImport = data.get(1).get(28);
			driverNameImport = faker.name().firstName();
			driverLanguageCode = data.get(1).get(29);
			driverLanguageCodeLux = "nl";

			vehicleBrand = data.get(1).get(30);

			vehicleCylinderContent = data.get(1).get(31);
			vehiclePower = data.get(1).get(32);
			vehicleLanguagePapers = data.get(1).get(33);
			vehicleWeight = data.get(1).get(34);
			vehicleTyreDescription = data.get(1).get(35);
			vehicleStockCar =  data.get(1).get(36);
			vehicleCo2Emissions = data.get(1).get(37);
			vehicleIdentification = data.get(1).get(38);
			vehicleModel = data.get(1).get(39);
			vehicleVersion = data.get(1).get(40);
			vehicleExteriorColor = data.get(1).get(41);
			vehicleInteriorColor = data.get(1).get(42);
			vehicleDescription = vehicleBrand + " " + vehicleModel;
			
			vehicleBrandLux = "Mercedes";
			vehicleCylinderContentLux = "1999";
			vehiclePowerLux = "194";
			vehicleLanguagePapersLux = "de";
			vehicleWeightLux = "2645";
			vehicleTyreDescriptionLux = "225/60 R18 104W";
			vehicleStockCarLux =  "false";
			vehicleCo2EmissionsLux = "57,00";
			vehicleModelLux = "E 300 DE Executive";
			vehicleExteriorColorLux = "bloody red";
			vehicleInteriorColorLux = "I want to paint it black";
			vehicleDescriptionLux = vehicleBrandLux + " " + vehicleModelLux;
			
			pricingTotalPriceInput = data.get(1).get(43);
			pricingTotalPriceLux = "79881,01";
			logger.debug("pricingTotalPriceLux init: " + pricingTotalPriceLux);
			pricingListPriceInput = data.get(1).get(44);
			pricingListPriceInputLux = "85361,59";
			pricingDeliveryCostsIni = data.get(1).get(45);
			pricingDeliveryCosts = data.get(1).get(46);
			pricingOtherCosts = data.get(1).get(47);
			pricingDiscountAmount = data.get(1).get(48);
			pricingDiscountPercentage = data.get(1).get(49);
			pricingFleetDiscountAmount = data.get(1).get(50);
			pricingFleetDiscountPercentage = data.get(1).get(51);
			promotionName = data.get(1).get(52);
			promotionDescription = data.get(1).get(53);
			promotionDiscountAmount = data.get(1).get(54);
			deliveryDesiredDate = data.get(1).get(55);
			deliveryDesiredDateLux = dateToday;
			deliveryLocationName = data.get(1).get(56);
			deliveryLocationLanguageCode = data.get(1).get(57);
			deliveryContactPerson = data.get(1).get(58);
			deliveryLocationVAT = data.get(1).get(59);
			deliveryStreetHouseNumber = data.get(1).get(60);
			deliveryPostalCode = data.get(1).get(61);
			deliveryCity = data.get(1).get(62);
			deliveryLocationAddress = deliveryStreetHouseNumber + " , " + deliveryPostalCode + " " + deliveryCity;
			deliveryContactEmail = data.get(1).get(63);
			deliveryContactPhoneNumber = data.get(1).get(64);
			deliveryContactMobileNumber = data.get(1).get(65);
			deliveryContactFaxNumber = data.get(1).get(66);
			insuranceName = data.get(1).get(67);
			insuranceEmail = data.get(1).get(68);
			insuranceValuePrice = data.get(1).get(69);
			buybackName = data.get(1).get(70);
			buybackAmount = data.get(1).get(71);
			buybackContractDuration = data.get(1).get(72);
			buybackStreetHouseNumber = data.get(1).get(73);
			buybackPostalCode = data.get(1).get(74);
			buybackCity = data.get(1).get(75);
			String applic = data.get(1).get(76);
			if(applic.equals("CLIENT")) {
				registrationDivToDoBy = Applicant.CLIENT;
			}
			vehicleVin = data.get(1).get(77);
			vehicleVinControlNumber = data.get(1).get(78);

			System.setProperty("leasingType", leasingType);

			if (leasingType.isEmpty() || leasingType.equals("OPERATIONAL_LEASE")) {
				logger.debug("Start operational lease");
			
			ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
			//Bulk orders
				Properties propIn = new Properties();
				FileInputStream fis = new
						FileInputStream("./src/test/resources/runtime/dossiernumberbulkvolgnummer.properties");
				logger.debug("fis: " + fis);
				propIn.load(fis);
				String vlgNrStr = propIn.getProperty("volgnummer");
				logger.debug("vlgNrStr: " + vlgNrStr);
				if(vlgNrStr == null || vlgNrStr.equals("null")) {
					logger.debug("dosNrBulkStr volgnummer kon niet worden bepaald ");
					//vlgNrStr = String.valueOf(9004);
				} else {
				logger.debug("vlgNrStr: " + vlgNrStr);
				if(!vlgNrStr.isEmpty() && vlgNrStr != null) {
					volgNr = Integer.parseInt(vlgNrStr);
					logger.debug("volgNr voor +1: " + volgNr);
					if (volgNr >= 9000) {
						volgNr++;
						logger.debug("volgNr na +1: " + volgNr);
					}
				}
				}

				logger.debug("Start dosNrBulk");
				String dosNrBulk = "MIGRATE-";
				//int dosNrBulkInt = 9000;
				String dosNrBulkStr = String.valueOf(volgNr);
				logger.debug("dosNrBulkStr: " + dosNrBulkStr);
				String dosNr = dosNrBulk + dosNrBulkStr;
				logger.debug("dosNr bulk: " + dosNr);
				//Properties prop = new Properties();
				propIn.put("volgnummer", dosNrBulkStr);
				FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/dossiernumberbulkvolgnummer.properties");
				propIn.store(fos, "");
				//dosNr = mcng.getRandomContractNumber();
				logger.debug("dosNr naar dossiernummerbulk?" + propIn.toString());
				Properties p = new Properties();
				p.put("DossierNumber", dosNr);
				fos = new FileOutputStream("./src/test/resources/runtime/dossiernumber.properties");
				p.store(fos, "");
				logger.debug("dosNr " + dosNr +  " naar proprty file geschreven");

				//dosNr = mcng.getRandomContractNumber();
			//Properties p = new Properties();
	        //p.put("DossierNumber", dosNr);
	        //fos = new FileOutputStream("./src/test/resources/runtime/dossiernumber.properties");
	        //p.store(fos, "");
	        //logger.debug("dosNr " + dosNr +  " naar proprty file geschreven");


				logger.debug("Start write file TestInfo");
			setDosNr(dosNr);
				File file = new File("./src/test/resources/runtime/TestInfo.csv");
				FileWriter fw = new FileWriter(file, true);
				fw.append(dosNr);
				fw.append(",");
				fw.close();

				FileOutputStream fileOut = null;
				FileInputStream fileIn = null;
			Properties propTot = new Properties();

			Thread.sleep(1000);
			file = new File("./src/test/resources/runtime/dossiernumberTotal.properties");
				fileIn = new FileInputStream(file);
				propTot.load(fileIn);
				propTot.setProperty("DossierNumber", dosNr);
				fileOut = new FileOutputStream(file, true);
				propTot.store(fileOut, "Updated properties");

			
			orderImports = new OrderImports();
			Header header = new Header();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			   LocalDateTime now = LocalDateTime.now();  
			   header.setCreatedDate(dtf.format(now));
			Order order = new Order();
				Meta meta = new Meta();
					meta.setTransactionCode(TransactionCode.MIGRATE);
					meta.setOrderStatus(orderStatus);
					meta.setDossierNumber(dosNr);
					meta.setLeasingCompanyRSNumber(leasingCompanyRSNumber);
					meta.setDealerDossierNumber(dealerDossierNumber);
					meta.setDealerRSNumber(DealerRSNumber);
					meta.setUserEmail(userEmail);
					meta.setTrackingNumber(trackingNumber);
					meta.setComment(comment);
					meta.setLeasingType(LeasingType.OPERATIONAL_LEASE);
				Client client = new Client();
				client.setName(clientName);
				client.setContactPerson(clientContactPerson);
				client.setLanguageCode(clientLanguageCode);
				client.setVAT(clientVAT);
					Address addressClient = new Address();
					addressClient.setStreetHouseNumber(clientStreetHouseNumber);
					addressClient.setPostalCode(clientPostalCode);
					addressClient.setCity(clientCity);
					Contact contactClient = new Contact();
					contactClient.setEmail(clientContactEmail);
					contactClient.setPhoneNumber(clientContactPhoneNumber);
					contactClient.setMobileNumber(clientContactMobileNumber);
					contactClient.setFaxNumber(clientContactFaxNumber);
				client.setAddress(addressClient);
				client.setContact(contactClient);
				Driver driver = new Driver();
					Address addressDriver = new Address();
					addressDriver.setStreetHouseNumber(driverStreetHouseNumber);
					addressDriver.setPostalCode(driverPostalCode);
					addressDriver.setCity(driverCity);
					Contact contactDriver = new Contact();
					contactDriver.setEmail(driverContactEmail);
					contactDriver.setPhoneNumber(driverContactPhoneNumber);
					contactDriver.setMobileNumber(driverContactMobileNumber);
					contactDriver.setFaxNumber(driverContactFaxNumber);
					driver.setName(driverNameImport);
					driver.setLanguageCode(driverLanguageCode);
					driver.setAddress(addressDriver);
					driver.setContact(contactDriver);
				Vehicle vehicle = new Vehicle();
				vehicle.setBrand(vehicleBrand);
				vehicle.setCylinderContent(vehicleCylinderContent);
				vehicle.setPower(vehiclePower);
				vehicle.setLanguagePapers(vehicleLanguagePapers);
				vehicle.setWeight(vehicleWeight);
				vehicle.setTyreDescription(vehicleTyreDescription);
				vehicle.setStockCar(vehicleStockCar);
				vehicle.setCo2Emissions(vehicleCo2Emissions);
				vehicle.setIdentification(vehicleIdentification);
					Translatable model = new Translatable();
						model.setDutch(vehicleModel);
					Translatable version = new Translatable();
					version.setDutch(vehicleVersion);
					Translatable exteriorColor = new Translatable();
					exteriorColor.setDutch(vehicleExteriorColor);
					Translatable interiorColor = new Translatable();
					interiorColor.setDutch(vehicleInteriorColor);
				vehicle.setModel(model);
				vehicle.setVersion(version);
				vehicle.setExteriorColor(exteriorColor);
				vehicle.setInteriorColor(interiorColor);
				//vehicle.setVinNumber(vehicleVin);
				//vehicle.setVinControlNumber(vehicleVinControlNumber);
				Pricing pricing = new Pricing();
				pricingTotalPriceTemp = pricingTotalPriceInput;
				pricing.setTotalPrice(pricingTotalPriceTemp);
				logger.debug("pricingTotalPriceTemp operational lease: " + pricingTotalPriceTemp);
				pricingListPrice = pricingListPriceInput;
				pricing.setListPrice(pricingListPrice);
				pricing.setDeliveryCosts(pricingDeliveryCosts);
				pricing.setOtherCosts(pricingOtherCosts);
					FleetDiscount fleetDiscount = new FleetDiscount();
					fleetDiscount.setAmount(pricingDiscountAmount);
					fleetDiscount.setPercentage(pricingDiscountPercentage);
					options = new Options();
					options.getOptions();
						Option option = new Option();
					Translatable nameOption = new Translatable();
					Translatable descriptionOption = new Translatable();
							Discount discountOption = new Discount();
							
							ICsvBeanReader beanReader = null;
						    CellProcessor[] processors = new CellProcessor[] {
						            new NotNull(), // OptionCode
						    		new NotNull(), // OptionType
						            new Optional(), // Name
						            new Optional(), // Description
						            new NotNull(), // ListPrice
						            new NotNull(), // Discount Amount
						            new NotNull(), // Discount percentage
						            new Optional(), // DealerCode
						            new Optional() // DealerName
						            
						    };

						    beanReader = new CsvBeanReader(new FileReader("src/test/resources/runtime/Option.csv"),
					                CsvPreference.STANDARD_PREFERENCE);
					        String[] header1 = beanReader.getHeader(true);
					        
					        OptionCsv optionBean = null;
					        List<OptionCsv> tmpOptions = new ArrayList<>();
					        
					        while ((optionBean = beanReader.read(OptionCsv.class, header1, processors)) != null) {
					        	logger.debug("deserialized description: " + optionBean.getDescription().toString());
					            //System.out.println("deserialized description: " + optionBean.getDescription().toString());
					            tmpOptions.add(optionBean);
					            logger.debug("tmpOptions to string: "+ tmpOptions.toString());
					            logger.debug("tmpOptions size: "+ tmpOptions.size());
					        }
					        
					        options.setOptions(new ArrayList<Option>());
					        for(int i = 0; i < tmpOptions.size(); i++) {
					        	option = new Option();
								Translatable oameOption = new Translatable();
								//nameOption = new Name();
								descriptionOption = new Translatable();
								discountOption = new Discount();
					        	option.setOptionCode(tmpOptions.get(i).getOptionCode());
					        	option.setOptionType(tmpOptions.get(i).getOptionType());
					        	nameOption.setDutch(tmpOptions.get(i).getName());
					        	option.setName(nameOption);
					        	descriptionOption.setDutch(tmpOptions.get(i).getDescription());
					        	option.setDescription(descriptionOption);
					        	option.setListPrice(tmpOptions.get(i).getListPrice());
					        	discountOption.setAmount(tmpOptions.get(i).getAmount());
					        	discountOption.setPercentage(tmpOptions.get(i).getPercentage());
					        	option.setDiscount(discountOption);
					        	option.setDealerCode(tmpOptions.get(i).getDealerCode());
					        	option.setDealerName(tmpOptions.get(i).getDealerName());

					        	options.getOptions().add(option);
					        	
					        	
					        }
					List<Promotion> promoList = new ArrayList<>();
					Promotions promotions = new Promotions();
						Promotion promotion = new Promotion();
				Translatable namePromotion = new Translatable();
							namePromotion.setDutch(promotionName);
				Translatable descriptionPromotion = new Translatable();
							descriptionPromotion.setDutch(promotionDescription);
							Discount discountPromotion = new Discount();
							discountPromotion.setAmount(promotionDiscountAmount);
						promotion.setName(namePromotion);
						promotion.setDescription(descriptionPromotion);
						promotion.setDiscount(discountPromotion);
						promoList.add(promotion);
					promotions.setPromotions(promoList);
					setPromotions(promotions);
				pricing.setOptions(options);
				pricing.setPromotions(promotions);
				Delivery delivery = new Delivery();
				delivery.setDesiredDate(deliveryDesiredDate);
				if(orderStatus.equals(OrderStatus.NEW)) {
					delivery.setExpectedDeliveryDate(null);
				} else {
					delivery.setExpectedDeliveryDate(dateToday);
				}
				delivery.setLocationName(deliveryLocationName);
				delivery.setLocationLanguageCode(deliveryLocationLanguageCode);
				delivery.setContactPerson(deliveryContactPerson);
				delivery.setLocationVAT(deliveryLocationVAT);
					Address addressDelivery = new Address();
					addressDelivery.setStreetHouseNumber(deliveryStreetHouseNumber);
					addressDelivery.setPostalCode(deliveryPostalCode);
					addressDelivery.setCity(deliveryCity);
					Contact contactDelivery = new Contact();
					contactDelivery.setEmail(deliveryContactEmail);
					contactDelivery.setPhoneNumber(deliveryContactPhoneNumber);
					contactDelivery.setMobileNumber(deliveryContactMobileNumber);
					contactDelivery.setFaxNumber(deliveryContactFaxNumber);
					delivery.setAddress(addressDelivery);
					delivery.setContact(contactDelivery);
				Buyback buyback = new Buyback();
				buyback.setName(buybackName);
				buyback.setAmount(buybackAmount);
				buyback.setContractDuration(buybackContractDuration);
					Address addressBuyback = new Address();
					addressBuyback.setStreetHouseNumber(buybackStreetHouseNumber);
					addressBuyback.setPostalCode(buybackPostalCode);
					addressBuyback.setCity(buybackCity);
					buyback.setAddress(addressBuyback);

					order.setMeta(meta);
					order.setClient(client);
					order.setDriver(driver);
					order.setVehicle(vehicle);
					order.setPricing(pricing);
					order.setDelivery(delivery);
					order.setBuyback(buyback);
		        
		        //orderImports.set_xmlns(xmlns);
				orderImports.setHeader(header);
				orderImports.setOrder(order);
			}
			
			else if (leasingType.isEmpty() || leasingType.equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
				pricingTotalPriceTemp = null;
				logger.debug("pricingTotalPriceTemp operational lease luxembourg: " + pricingTotalPriceTemp);
				ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
				dosNr = mcng.getRandomContractNumber();
				Properties p = new Properties();
		        p.put("DossierNumber", dosNr);
		        FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/dossiernumber.properties");
		        p.store(fos, "");
		        logger.debug("dosNr " + dosNr +  " naar proprty file geschreven");
				
				orderImports = new OrderImports();
				Header header = new Header();
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
				   LocalDateTime now = LocalDateTime.now();  
				   header.setCreatedDate(dtf.format(now));
				Order order = new Order();
					Meta meta = new Meta();
						meta.setTransactionCode(TransactionCode.CREATE);
						meta.setDossierNumber(dosNr);
						leasingCompanyRSNumber = leasingCompanyRSNumberLux;
						meta.setLeasingCompanyRSNumber(leasingCompanyRSNumber);
						meta.setDealerRSNumber(DealerRSNumber);
						userEmail = userEmailLux;
						meta.setUserEmail(userEmail);
						meta.setVersionNumber(versionNumber);
						meta.setDateFirstRegistration(dateFirstRegistration);
					Client client = new Client();
					clientName = clientNameLux;
					client.setName(clientName);
					clientContactPerson = clientContactPersonLux;
					client.setContactPerson(clientContactPerson);
					clientLanguageCode = clientLanguageCodeLux;
					client.setLanguageCode(clientLanguageCode);
					clientVAT = clientVATLux;
					client.setVAT(clientVAT);
					client.setRegistrationNumberLessee(registrationNumberLessee);
						Address addressClient = new Address();
						clientStreetHouseNumber = clientStreetHouseNumberLux;
						addressClient.setStreetHouseNumber(clientStreetHouseNumber);
						clientPostalCode = clientPostalCodeLux;
						addressClient.setPostalCode(clientPostalCode);
						clientCity = clientCityLux;
						addressClient.setCity(clientCity);
						Contact contactClient = new Contact();
						clientContactEmail = clientContactEmailLux;
						contactClient.setEmail(clientContactEmail);
						clientContactPhoneNumber = clientContactPhoneNumberLux;
						contactClient.setPhoneNumber(clientContactPhoneNumber);
						clientContactMobileNumber = clientContactMobileNumberLux;
						contactClient.setMobileNumber(clientContactMobileNumber);
						clientContactFaxNumber = clientContactFaxNumberLux;
						contactClient.setFaxNumber(clientContactFaxNumber);
					client.setAddress(addressClient);
					client.setContact(contactClient);
					//Driver driver = new Driver();
						//Address addressDriver = new Address();
						//addressDriver.setStreetHouseNumber(driverStreetHouseNumber);
						//addressDriver.setPostalCode(driverPostalCode);
						//addressDriver.setCity(driverCity);
						//Contact contactDriver = new Contact();
						//driverContactEmail = driverContactEmailLux;
						//contactDriver.setEmail(driverContactEmail);
						//driverContactPhoneNumber = driverContactPhoneNumberLux;
						//contactDriver.setPhoneNumber(driverContactPhoneNumber);
						//driverContactMobileNumber = driverContactMobileNumberLux;
						//contactDriver.setMobileNumber(driverContactMobileNumber);
						//contactDriver.setFaxNumber(driverContactFaxNumber);
						//driverNameImport = driverNameImportLux;
						//driver.setName(driverNameImport);
						//driverLanguageCode = driverLanguageCodeLux;
						//driver.setLanguageCode(driverLanguageCode);
						//driver.setAddress(addressDriver);
						//driver.setContact(contactDriver);
					Vehicle vehicle = new Vehicle();
					vehicleBrand = vehicleBrandLux;
					vehicle.setBrand(vehicleBrand);
					vehicleCylinderContent = vehicleCylinderContentLux;
					vehicle.setCylinderContent(vehicleCylinderContent);
					vehiclePower = vehiclePowerLux;
					vehicle.setPower(vehiclePower);
					vehicleLanguagePapers = vehicleLanguagePapersLux;
					vehicle.setLanguagePapers(vehicleLanguagePapers);
					vehicleWeight = vehicleWeightLux;
					vehicle.setWeight(vehicleWeight);
					vehicleTyreDescription = vehicleTyreDescriptionLux;
					vehicle.setTyreDescription(vehicleTyreDescription);
					vehicleStockCar = vehicleStockCarLux;
					vehicle.setStockCar(vehicleStockCar);
					vehicleCo2Emissions = vehicleCo2EmissionsLux;
					vehicle.setCo2Emissions(vehicleCo2Emissions);
					//vehicle.setIdentification(vehicleIdentification);
						Translatable model = new Translatable();
							vehicleModel = vehicleModelLux;
							model.setDutch(vehicleModel);
						//Version version = new Version();
						//version.setDutch(vehicleVersion);
				Translatable exteriorColor = new Translatable();
						vehicleExteriorColor = vehicleExteriorColorLux;
						exteriorColor.setDutch(vehicleExteriorColor);
				Translatable interiorColor = new Translatable();
						vehicleInteriorColor = vehicleInteriorColorLux;
						interiorColor.setDutch(vehicleInteriorColor);
					vehicle.setModel(model);
					//vehicle.setVersion(version);
					vehicle.setExteriorColor(exteriorColor);
					vehicle.setInteriorColor(interiorColor);
					setPricingTotalPrice(null);
					Pricing pricing = new Pricing();
					pricing.setTotalPrice(null);
					pricingTotalPriceTemp = pricingTotalPriceLux;

				logger.debug("pricingTotalPriceTemp wordt pricingTotalPriceLux: " + pricingTotalPriceTemp);

					pricing.setTotalPrice(pricingTotalPriceTemp);
				setPricingTotalPrice(pricingTotalPriceTemp);
				logger.debug("pricingTotalPriceTemp operational lease luxembourg na copy van Lux: " + pricingTotalPriceTemp);
					pricingListPrice = pricingListPriceInputLux;
					pricing.setListPrice(pricingListPrice);
					//pricing.setDeliveryCosts(pricingDeliveryCosts);
					//pricing.setOtherCosts(pricingOtherCosts);
						FleetDiscount fleetDiscount = new FleetDiscount();
						pricingFleetDiscountAmount = pricingFleetDiscountAmountLux;
						fleetDiscount.setAmount(pricingFleetDiscountAmount);
						pricingFleetDiscountPercentage = pricingFleetDiscountPercentageLux;
						fleetDiscount.setPercentage(pricingFleetDiscountPercentage);
						Discount discount = new Discount();
						discount.setAmount("50");
						discount.setPercentage("3");
						pricing.setFleetDiscount(discount);
						options = new Options();
						options.getOptions();
							Option option = new Option();
								Translatable nameOption = new Translatable();
				Translatable descriptionOption = new Translatable();
								Discount discountOption = new Discount();
								
								ICsvBeanReader beanReader = null;
							    CellProcessor[] processors = new CellProcessor[] {
							            new NotNull(), // OptionCode
							    		new NotNull(), // OptionType
							            new Optional(), // Name
							            new Optional(), // Description
							            new NotNull(), // ListPrice
							            new NotNull(), // Discount Amount
							            new NotNull(), // Discount percentage
							            new Optional(), // DealerCode
							            new Optional() // DealerName
							            
							    };

							    beanReader = new CsvBeanReader(new FileReader("src/test/resources/runtime/OptionLux.csv"),
						                CsvPreference.STANDARD_PREFERENCE);
						        String[] header1 = beanReader.getHeader(true);
						        
						        OptionCsv optionBean = null;
						        List<OptionCsv> tmpOptions = new ArrayList<>();
						        
						        while ((optionBean = beanReader.read(OptionCsv.class, header1, processors)) != null) {
						        	logger.debug("deserialized description: " + optionBean.getDescription().toString());
						            //System.out.println("deserialized description: " + optionBean.getDescription().toString());
						            tmpOptions.add(optionBean);
						            logger.debug("tmpOptions to string: "+ tmpOptions.toString());
						            logger.debug("tmpOptions size: "+ tmpOptions.size());
						        }
						        
						        options.setOptions(new ArrayList<Option>());
						        for(int i = 0; i < tmpOptions.size(); i++) {
						        	option = new Option();
									nameOption = new Translatable();
									descriptionOption = new Translatable();
									discountOption = new Discount();
						        	//option.setOptionCode(tmpOptions.get(i).getOptionCode());
						        	option.setOptionType(tmpOptions.get(i).getOptionType());
						        	nameOption.setDutch(tmpOptions.get(i).getName());
						        	option.setName(nameOption);
						        	descriptionOption.setDutch(tmpOptions.get(i).getDescription());
						        	option.setDescription(descriptionOption);
						        	option.setListPrice(tmpOptions.get(i).getListPrice());
						        	discountOption.setAmount(tmpOptions.get(i).getAmount());
						        	discountOption.setPercentage(tmpOptions.get(i).getPercentage());
						        	option.setDiscount(discountOption);
						        	//option.setDealerCode(tmpOptions.get(i).getDealerCode());
						        	//option.setDealerName(tmpOptions.get(i).getDealerName());

						        	options.getOptions().add(option);
						        	
						        	
						        }
								/*
								 * List<Promotion> promoList = new ArrayList<>(); Promotions promotions = new
								 * Promotions(); Promotion promotion = new Promotion(); Name namePromotion = new
								 * Name(); namePromotion.setDutch(promotionName); Description
								 * descriptionPromotion = new Description();
								 * descriptionPromotion.setDutch(promotionDescription); Discount
								 * discountPromotion = new Discount();
								 * discountPromotion.setAmount(promotionDiscountAmount);
								 * promotion.setName(namePromotion);
								 * promotion.setDescription(descriptionPromotion);
								 * promotion.setDiscount(discountPromotion); promoList.add(promotion);
								 * promotions.setPromotions(promoList); setPromotions(promotions);
								 */
					pricing.setOptions(options);
					//pricing.setPromotions(promotions);
					Delivery delivery = new Delivery();
					deliveryDesiredDate = deliveryDesiredDateLux;
					delivery.setDesiredDate(deliveryDesiredDate);
					//delivery.setLocationName(deliveryLocationName);
					//delivery.setLocationLanguageCode(deliveryLocationLanguageCode);
					//delivery.setContactPerson(deliveryContactPerson);
					//delivery.setLocationVAT(deliveryLocationVAT);
						//Address addressDelivery = new Address();
						//addressDelivery.setStreetHouseNumber(deliveryStreetHouseNumber);
						//addressDelivery.setPostalCode(deliveryPostalCode);
						//addressDelivery.setCity(deliveryCity);
						//Contact contactDelivery = new Contact();
						//contactDelivery.setEmail(deliveryContactEmail);
						//contactDelivery.setPhoneNumber(deliveryContactPhoneNumber);
						//contactDelivery.setMobileNumber(deliveryContactMobileNumber);
						//contactDelivery.setFaxNumber(deliveryContactFaxNumber);
						//delivery.setAddress(addressDelivery);
						//delivery.setContact(contactDelivery);
					Insurance insurance = new Insurance();
					insurance.setName(insuranceName);
					insurance.setEmail(insuranceEmail);
					insurance.setInsuredValuePrice(insuranceValuePrice);


						order.setMeta(meta);
						order.setClient(client);
						//order.setDriver(driver);
						order.setVehicle(vehicle);
						order.setPricing(pricing);
						order.setDelivery(delivery);
						order.setInsurance(insurance);
			        
			        orderImports.set_xmlns(xmlns);
					orderImports.setHeader(header);
					orderImports.setOrder(order);
				}
			
			else if (leasingType.equals("FINANCIAL_LEASE")) {
				
				ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
				dosNr = mcng.getRandomContractNumber();
				Properties p = new Properties();
		        p.put("DossierNumber", dosNr);
		        FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/dossiernumber.properties");
		        p.store(fos, "");
		        logger.debug("dosNr " + dosNr +  " naar proprty file geschreven");
				
				orderImports = new OrderImports();
				Header header = new Header();
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
				   LocalDateTime now = LocalDateTime.now();  
				   header.setCreatedDate(dtf.format(now));
				Order order = new Order();
					Meta meta = new Meta();
						meta.setTransactionCode(TransactionCode.CREATE);
						meta.setDossierNumber(dosNr);
						meta.setLeasingCompanyRSNumber(leasingCompanyRSNumber);
						meta.setDealerRSNumber(DealerRSNumber);
						meta.setUserEmail(userEmail);
						meta.setLeasingType(LeasingType.FINANCIAL_LEASE);
					Client client = new Client();
					client.setName(clientName);
					client.setVAT(clientVAT);
					Vehicle vehicle = new Vehicle();
					vehicle.setBrand(vehicleBrand);
				Translatable model = new Translatable();
							model.setDutch(vehicleModel);
					vehicle.setModel(model);
					Pricing pricing = new Pricing();
				pricingTotalPrice = pricingTotalPriceInput;
					pricing.setTotalPrice(pricingTotalPriceInput);
					setPricingTotalPrice(pricingTotalPriceInput);
				logger.debug("SET pricingTotalPrice financial lease to pricingTotalPriceInput: " + pricing.getTotalPrice());
					pricing.setListPrice(pricingListPriceInput);
					setPricingListPrice(pricingListPriceInput);
					RegistrationInfo registrationInfo = new RegistrationInfo();
						registrationInfo.setRegistrationDivToDoBy(registrationDivToDoBy);
					order.setMeta(meta);
					order.setClient(client);
					order.setVehicle(vehicle);
					order.setPricing(pricing);
					order.setRegistrationInfo(registrationInfo);
			        
			        //orderImports.set_xmlns(xmlns);
					orderImports.setHeader(header);
					orderImports.setOrder(order);
				}
				
				 JAXBContext jaxbContext = JAXBContext.newInstance(orderImports.getClass());
		            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
					jaxbMarshaller.setProperty(jaxbMarshaller.JAXB_ENCODING, "windows-1252");
		            //jaxbMarshaller.marshal(data, System.out);
		            StringWriter sw = new StringWriter();
		            jaxbMarshaller.marshal(orderImports, sw);
		            String xml = sw.toString();
		            logger.debug("Xml: " + xml);
		            
		            BufferedWriter writer = new BufferedWriter(new FileWriter("./src/test/resources/runtime/uploadORD.xml", false));
		            writer.write(xml);
		            
		            writer.close();
		            
		            SftpConnect sc = new SftpConnect();
		            ChannelSftp channelSftp = sc.setupJsch();
		    	    channelSftp.connect();
		    	 
		    	    String localFile = "./src/test/resources/runtime/uploadORD.xml";
		    	    if(leasingType.equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
		    	    	remoteDir = "/799330/ORD/in";
		    	    } else {
						//remoteDir = "/799120/ORD/in"; //This is specific for Arval
						remoteDir = "/799230/ORD/in"; //This is for most tests
		    	    	//remoteDir = "/799189/ORD/in";
		    	    }
		    	    
		    	 
		    	    channelSftp.put(localFile, remoteDir);

					//channelSftp.exit();
		    	    channelSftp.disconnect();

				
				
			}

		

		public String getXmlns() {
			return xmlns;
		}

		public void setXmlns(String xmlns) {
			this.xmlns = xmlns;
		}

		public String getDosNr() {
			logger.debug("dosNr in getter: " + dosNr);
			return dosNr;
		}

		public void setDosNr(String dosNr) {
			this.dosNr = dosNr;
		}

		public String getTransactionCode() {
			return transactionCode;
		}

		public void setTransactionCode(String transactionCode) {
			this.transactionCode = transactionCode;
		}

		public String getLeasingCompanyRSNumber() {
			return leasingCompanyRSNumber;
		}

		public void setLeasingCompanyRSNumber(String leasingCompanyRSNumber) {
			this.leasingCompanyRSNumber = leasingCompanyRSNumber;
		}

		public String getDealerRSNumber() {
			return DealerRSNumber;
		}

		public void setDealerRSNumber(String dealerRSNumber) {
			DealerRSNumber = dealerRSNumber;
		}

		public String getUserEmail() {
			return userEmail;
		}

		public void setUserEmail(String userEmail) {
			this.userEmail = userEmail;
		}

		public String getTrackingNumber() {
			return trackingNumber;
		}

		public void setTrackingNumber(String trackingNumber) {
			this.trackingNumber = trackingNumber;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public String getClientName() {
			logger.debug("clientName in getter: " + clientName);
			return clientName;
		}

		public void setClientName(String clientName) {
			this.clientName = clientName;
		}
		
		public String getClientBillingAddress() {
			return clientBillingAddress;
		}

		public void setClientBillingAddress(String clientBillingAddress) {
			this.clientBillingAddress = clientBillingAddress;
		}

		public String getClientContactPerson() {
			return clientContactPerson;
		}

		public void setClientContactPerson(String clientContactPerson) {
			this.clientContactPerson = clientContactPerson;
		}

		public String getClientLanguageCode() {
			if (clientLanguageCode != null) {
				if (clientLanguageCode.equals("nl")) {
					clientLanguageCode = "DUTCH";
				}
				if (clientLanguageCode.equals("fr")) {
					clientLanguageCode = "FRENCH";
				}
				if (clientLanguageCode.equals("en")) {
					clientLanguageCode = "ENGLISH";
				}
				if (clientLanguageCode.equals("de")) {
					clientLanguageCode = "DEUTSCH";
				} //else {
				//	clientLanguageCode = "―";
				//}


			}
			return clientLanguageCode;
		}

		public void setClientLanguageCode(String clientLanguageCode) {
			this.clientLanguageCode = clientLanguageCode;
		}

		public String getClientVAT() {
			return clientVAT;
		}

		public void setClientVAT(String clientVAT) {
			this.clientVAT = clientVAT;
		}

		public String getClientStreetHouseNumber() {
			return clientStreetHouseNumber;
		}

		public void setClientStreetHouseNumber(String clientStreetHouseNumber) {
			this.clientStreetHouseNumber = clientStreetHouseNumber;
		}

		public String getClientPostalCode() {
			return clientPostalCode;
		}

		public void setClientPostalCode(String clientPostalCode) {
			this.clientPostalCode = clientPostalCode;
		}

		public String getClientCity() {
			return clientCity;
		}

		public void setClientCity(String clientCity) {
			this.clientCity = clientCity;
		}

		public String getClientContactEmail() {
			return clientContactEmail;
		}

		public void setClientContactEmail(String clientContactEmail) {
			this.clientContactEmail = clientContactEmail;
		}

		public String getClientContactPhoneNumber() {
			return clientContactPhoneNumber;
		}

		public void setClientContactPhoneNumber(String clientContactPhoneNumber) {
			this.clientContactPhoneNumber = clientContactPhoneNumber;
		}

		public String getClientContactMobileNumber() {
			return clientContactMobileNumber;
		}

		public void setClientContactMobileNumber(String clientContactMobileNumber) {
			this.clientContactMobileNumber = clientContactMobileNumber;
		}

		public String getClientContactFaxNumber() {
			return clientContactFaxNumber;
		}

		public void setClientContactFaxNumber(String clientContactFaxNumber) {
			this.clientContactFaxNumber = clientContactFaxNumber;
		}

		public String getDriverStreetHouseNumber() {
			return driverStreetHouseNumber;
		}

		public void setDriverStreetHouseNumber(String driverStreetHouseNumber) {
			this.driverStreetHouseNumber = driverStreetHouseNumber;
		}

		public String getDriverPostalCode() {
			return driverPostalCode;
		}

		public void setDriverPostalCode(String driverPostalCode) {
			this.driverPostalCode = driverPostalCode;
		}

		public String getDriverCity() {
			return driverCity;
		}

		public void setDriverCity(String driverCity) {
			this.driverCity = driverCity;
		}
		
		public String getDriverAddress() {
			return driverAddress;
		}

		public void setDriverAddress(String driverAddress) {
			this.driverCity = driverAddress;
		}

		public String getDriverContactEmail() {
			return driverContactEmail;
		}

		public void setDriverContactEmail(String driverContactEmail) {
			this.driverContactEmail = driverContactEmail;
		}

		public String getDriverContactPhoneNumber() {
			return driverContactPhoneNumber;
		}

		public void setDriverContactPhoneNumber(String driverContactPhoneNumber) {
			this.driverContactPhoneNumber = driverContactPhoneNumber;
		}

		public String getDriverContactMobileNumber() {
			return driverContactMobileNumber;
		}

		public void setDriverContactMobileNumber(String driverContactMobileNumber) {
			this.driverContactMobileNumber = driverContactMobileNumber;
		}

		public String getDriverContactFaxNumber() {
			return driverContactFaxNumber;
		}

		public void setDriverContactFaxNumber(String driverContactFaxNumber) {
			this.driverContactFaxNumber = driverContactFaxNumber;
		}
		
		public String getDriverNameImport() {
			return driverNameImport;
		}

		public void setDriverNameImport(String driverNameImport) {
			this.driverNameImport = driverNameImport;
		}
		
		public String getDriverLanguageCode() {
			if(driverLanguageCode.equals("nl")) {
				driverLanguageCode = "DUTCH";
			}
			if(driverLanguageCode.equals("fr")) {
				driverLanguageCode = "FRENCH";
			}
			if(driverLanguageCode.equals("en")) {
				driverLanguageCode = "ENGLISH";
			}
			if(driverLanguageCode.equals("de")) {
				driverLanguageCode = "DEUTSCH";
			}
			return driverLanguageCode;
		}

		public void setDriverLanguageCode(String driverLanguageCode) {
			this.driverLanguageCode = driverLanguageCode;
		}

		public String getVehicleBrand() {
			return vehicleBrand;
		}

		public void setVehicleBrand(String vehicleBrand) {
			this.vehicleBrand = vehicleBrand;
		}

		public String getVehicleCylinderContent() {
			return vehicleCylinderContent;
		}

		public void setVehicleCylinderContent(String vehicleCylinderContent) {
			this.vehicleCylinderContent = vehicleCylinderContent;
		}

		public String getVehiclePower() {
			return vehiclePower;
		}

		public void setVehiclePower(String vehiclePower) {
			this.vehiclePower = vehiclePower;
		}

		public String getVehicleLanguagePapers() {
			if(vehicleLanguagePapers.equals("nl")) {
				vehicleLanguagePapers = "Dutch";
			}
			if(vehicleLanguagePapers.equals("fr")) {
				vehicleLanguagePapers = "French";
			}
			if(vehicleLanguagePapers.equals("en")) {
				vehicleLanguagePapers = "English";
			}
			if(vehicleLanguagePapers.equals("de")) {
				vehicleLanguagePapers = "German";
			} else {
				vehicleLanguagePapers = "―";
			}
			return vehicleLanguagePapers;
		}

		public void setVehicleLanguagePapers(String vehicleLanguagePapers) {
			this.vehicleLanguagePapers = vehicleLanguagePapers;
		}

		public String getVehicleWeight() {
			return vehicleWeight;
		}

		public void setVehicleWeight(String vehicleWeight) {
			this.vehicleWeight = vehicleWeight;
		}

		public String getVehicleTyreDescription() {
			return vehicleTyreDescription;
		}

		public void setVehicleTyreDescription(String vehicleTyreDescription) {
			this.vehicleTyreDescription = vehicleTyreDescription;
		}

		public String getVehicleStockCar() {
			return vehicleStockCar;
		}

		public void setVehicleStockCar(String vehicleStockCar) {
			this.vehicleStockCar = vehicleStockCar;
		}

		public String getVehicleCo2Emissions() {
			return vehicleCo2Emissions;
		}

		public void setVehicleCo2Emissions(String vehicleCo2Emissions) {
			this.vehicleCo2Emissions = vehicleCo2Emissions;
		}

		public String getVehicleIdentification() {
			return vehicleIdentification;
		}

		public void setVehicleIdentification(String vehicleIdentification) {
			this.vehicleIdentification = vehicleIdentification;
		}

		public String getVehicleModel() {
			return vehicleModel;
		}

		public void setVehicleModel(String vehicleModel) {
			this.vehicleModel = vehicleModel;
		}

		public String getVehicleVersion() {
			return vehicleVersion;
		}

		public void setVehicleVersion(String vehicleVersion) {
			this.vehicleVersion = vehicleVersion;
		}

		public String getVehicleExteriorColor() {
			return vehicleExteriorColor;
		}

		public void setVehicleExteriorColor(String vehicleExteriorColor) {
			this.vehicleExteriorColor = vehicleExteriorColor;
		}

		public String getVehicleInteriorColor() {
			return vehicleInteriorColor;
		}

		public void setVehicleInteriorColor(String vehicleInteriorColor) {
			this.vehicleInteriorColor = vehicleInteriorColor;
		}
		
		public String getVehicDescription() {
			return vehicleDescription;
		}

		public void setVehicleDescription(String vehicleDescription) {
			this.vehicleDescription = vehicleDescription;
		}

		public String getPricingTotalPrice() throws ParseException {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			//pricingTotalPriceTemp = pricingTotalPriceInput;
			if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
				logger.debug("pricingTotalPriceTemp in getter voor change van comma naar punt: " + pricingTotalPriceLux);
				if(pricingTotalPriceLux.startsWith("€")) {
					pricingTotalPriceLux = pricingTotalPriceLux.replace("€" , "");
					pricingTotalPriceLux = pricingTotalPriceLux.replace("," , "");
				} else {
					pricingTotalPriceLux = pricingTotalPriceLux.replace("," , ".");
				}

				logger.debug("pricingTotalPriceTemp in getter: " + pricingTotalPriceLux);
				Double d = Double.parseDouble(pricingTotalPriceLux);
				pricingTotalPriceLux = decimalFormat.format(d).toString();

				//Number number = decimalFormat.parse(pricingTotalPrice);
				logger.debug("pricingTotalPrice: " + pricingTotalPriceLux);
				pricingTotalPrice = pricingTotalPriceLux;
			} else {
				if(pricingTotalPriceInput.startsWith("€")) {
					pricingTotalPriceInput = pricingTotalPriceInput.replace("€" , "");
					pricingTotalPriceInput = pricingTotalPriceInput.replace("," , "");
				} else {
					pricingTotalPriceInput = pricingTotalPriceInput.replace("," , ".");
				}

				logger.debug("pricingTotalPriceTemp in getter: " + pricingTotalPriceInput);
				Double d = Double.parseDouble(pricingTotalPriceInput);
				pricingTotalPriceInput = decimalFormat.format(d).toString();

				//Number number = decimalFormat.parse(pricingTotalPrice);
				logger.debug("pricingTotalPrice: " + pricingTotalPriceInput);
				pricingTotalPrice = pricingTotalPriceInput;
			}

			return pricingTotalPrice;
		}

		public void setPricingTotalPrice(String pricingTotalPrice) {
			this.pricingTotalPrice = pricingTotalPrice;
		}

		public String getPricingListPrice() {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			if(pricingListPrice.startsWith("€")) {
				pricingListPrice = pricingListPrice.replace("€" , "");
				pricingListPrice = pricingListPrice.replace("," , "");
			} else {
				pricingListPrice = pricingListPrice.replace("," , ".");
			}

			Double DoubleListPrice = Double.parseDouble(pricingListPrice);
			pricingListPrice = decimalFormat.format(DoubleListPrice).toString();
			logger.debug("pricingListPrice: " + pricingListPrice);
			return pricingListPrice;
		}

		public void setPricingListPrice(String pricingListPrice) {
			this.pricingListPrice = pricingListPrice;
		}

		public String getPricingDeliveryCosts() {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			if(pricingDeliveryCostsIni.startsWith("€")) {
				pricingDeliveryCostsIni = pricingDeliveryCostsIni.replace("€" , "");
				pricingDeliveryCostsIni = pricingDeliveryCostsIni.replace("," , "");
			} else {
				pricingDeliveryCostsIni = pricingDeliveryCostsIni.replace("," , ".");
			}

			logger.debug("String pricingDeliveryCostsIni: " + pricingDeliveryCostsIni);
			Double DoublePricingDeliveryCosts = Double.parseDouble(pricingDeliveryCostsIni);
			logger.debug("Double pricingDeliveryCosts: " + DoublePricingDeliveryCosts);
			pricingDeliveryCosts = decimalFormat.format(DoublePricingDeliveryCosts).toString();
			logger.debug("pricingDeliveryCosts: " + pricingDeliveryCosts);
			return pricingDeliveryCosts;
		}

		public void setPricingDeliveryCosts(String pricingDeliveryCosts) {
			this.pricingDeliveryCosts = pricingDeliveryCosts;
		}

		public String getPricingOtherCosts() {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			if(pricingOtherCosts.startsWith("€")) {
				pricingOtherCosts = pricingOtherCosts.replace("€" , "");
				pricingOtherCosts = pricingOtherCosts.replace("," , "");
			} else {
				pricingOtherCosts = pricingOtherCosts.replace("," , ".");
			}

			Double DoublePricingOtherCosts = Double.parseDouble(pricingOtherCosts);
			pricingOtherCosts = decimalFormat.format(DoublePricingOtherCosts).toString();
			logger.debug("pricingOtherCosts: " + pricingOtherCosts);
			return pricingOtherCosts;
		}

		public void setPricingOtherCosts(String pricingOtherCosts) {
			this.pricingOtherCosts = pricingOtherCosts;
		}

		public String getPricingDiscountAmount() {
			return pricingDiscountAmount;
		}

		public void setPricingDiscountAmount(String pricingDiscountAmount) {
			this.pricingDiscountAmount = pricingDiscountAmount;
		}

		public String getPricingDiscountPercentage() {
			return pricingDiscountPercentage;
		}

		public void setPricingDiscountPercentage(String pricingDiscountPercentage) {
			this.pricingDiscountPercentage = pricingDiscountPercentage;
		}

		public String getPromotionName() {
			return promotionName;
		}

		public void setPromotionName(String promotionName) {
			this.promotionName = promotionName;
		}

		public String getPromotionDescription() {
			return promotionDescription;
		}

		public void setPromotionDescription(String promotionDescription) {
			this.promotionDescription = promotionDescription;
		}

		public String getPromotionDiscountAmount() {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			promotionDiscountAmount = promotionDiscountAmount.replace("," , ".");
			Double DoublePromotionDiscountAmount = Double.parseDouble(promotionDiscountAmount);
			promotionDiscountAmount = decimalFormat.format(DoublePromotionDiscountAmount).toString();
			logger.debug("promotionDiscountAmount: " + promotionDiscountAmount);
			return promotionDiscountAmount;
		}

		public void setPromotionDiscountAmount(String promotionDiscountAmount) {
			this.promotionDiscountAmount = promotionDiscountAmount;
		}

		public String getDeliveryDesiredDate() {
			return deliveryDesiredDate;
		}

		public void setDeliveryDesiredDate(String deliveryDesiredDate) {
			this.deliveryDesiredDate = deliveryDesiredDate;
		}

		public String getDeliveryLocationName() {
			return deliveryLocationName;
		}

		public void setDeliveryLocationNameApi(String deliveryLocationName) {
			this.deliveryLocationName = deliveryLocationName;
		}

		public String getDeliveryLocationLanguageCode() {
			if(deliveryLocationLanguageCode.equals("nl")) {
				deliveryLocationLanguageCode = "Dutch";
			}
			if(deliveryLocationLanguageCode.equals("fr")) {
				deliveryLocationLanguageCode = "French";
			}
			if(deliveryLocationLanguageCode.equals("en")) {
				deliveryLocationLanguageCode = "English";
			}
			if(deliveryLocationLanguageCode.equals("de")) {
				deliveryLocationLanguageCode = "Deutsch";
			} else {
				deliveryLocationLanguageCode = "―";

			}
			return deliveryLocationLanguageCode;
		}

		public void setDeliveryLocationLanguageCode(String deliveryLocationLanguageCode) {
			this.deliveryLocationLanguageCode = deliveryLocationLanguageCode;
		}

		public String getDeliveryContactPerson() {
			return deliveryContactPerson;
		}

		public void setDeliveryContactPerson(String deliveryContactPerson) {
			this.deliveryContactPerson = deliveryContactPerson;
		}

		public String getDeliveryLocationVAT() {
			return deliveryLocationVAT;
		}

		public void setDeliveryLocationVAT(String deliveryLocationVAT) {
			this.deliveryLocationVAT = deliveryLocationVAT;
		}

		public String getDeliveryStreetHouseNumber() {
			return deliveryStreetHouseNumber;
		}

		public void setDeliveryStreetHouseNumber(String deliveryStreetHouseNumber) {
			this.deliveryStreetHouseNumber = deliveryStreetHouseNumber;
		}

		public String getDeliveryPostalCode() {
			return deliveryPostalCode;
		}

		public void setDeliveryPostalCode(String deliveryPostalCode) {
			this.deliveryPostalCode = deliveryPostalCode;
		}

		public String getDeliveryCity() {
			return deliveryCity;
		}

		public void setDeliveryCity(String deliveryCity) {
			this.deliveryCity = deliveryCity;
		}
		
		public String getDeliveryLocationAddress() {
			return deliveryLocationAddress;
		}

		public void setDeliveryLocationAddress(String deliveryLocationAddress) {
			this.deliveryLocationAddress = deliveryLocationAddress;
		}

		public String getDeliveryContactEmail() {
			return deliveryContactEmail;
		}

		public void setDeliveryContactEmail(String deliveryContactEmail) {
			this.deliveryContactEmail = deliveryContactEmail;
		}

		public String getDeliveryContactPhoneNumber() {
			return deliveryContactPhoneNumber;
		}

		public void setDeliveryContactPhoneNumber(String deliveryContactPhoneNumber) {
			this.deliveryContactPhoneNumber = deliveryContactPhoneNumber;
		}

		public String getDeliveryContactMobileNumber() {
			return deliveryContactMobileNumber;
		}

		public void setDeliveryContactMobileNumber(String deliveryContactMobileNumber) {
			this.deliveryContactMobileNumber = deliveryContactMobileNumber;
		}

		public String getDeliveryContactFaxNumber() {
			return deliveryContactFaxNumber;
		}

		public void setDeliveryContactFaxNumber(String deliveryContactFaxNumber) {
			this.deliveryContactFaxNumber = deliveryContactFaxNumber;
		}

		public String getBuybackName() {
			return buybackName;
		}

		public void setBuybackName(String buybackName) {
			this.buybackName = buybackName;
		}

		public String getBuybackAmount() {
			Locale loc = new Locale("en", "EN");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(loc);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(loc);
			dfs.setCurrencySymbol("\u20ac");
			decimalFormat.setDecimalFormatSymbols(dfs);
			buybackAmount = buybackAmount.replace("," , ".");
			Double DoubleBuybackAmount = Double.parseDouble(buybackAmount);
			buybackAmount = decimalFormat.format(DoubleBuybackAmount).toString();
			logger.debug("buybackAmount: " + buybackAmount);

			return buybackAmount;
		}

		public void setBuybackAmount(String buybackAmount) {
			this.buybackAmount = buybackAmount;
		}

		public String getBuybackContractDuration() {
			return buybackContractDuration;
		}

		public void setBuybackContractDuration(String buybackContractDuration) {
			this.buybackContractDuration = buybackContractDuration;
		}

		public String getBuybackStreetHouseNumber() {
			return buybackStreetHouseNumber;
		}

		public void setBuybackStreetHouseNumber(String buybackStreetHouseNumber) {
			this.buybackStreetHouseNumber = buybackStreetHouseNumber;
		}

		public String getBuybackPostalCode() {
			return buybackPostalCode;
		}

		public void setBuybackPostalCode(String buybackPostalCode) {
			this.buybackPostalCode = buybackPostalCode;
		}

		public String getBuybackCity() {
			return buybackCity;
		}

		public void setBuybackCity(String buybackCity) {
			this.buybackCity = buybackCity;
		}
		
		public String getLeasingType() {
			return leasingType;
		}

		public void setLeasingType(String leasingType) {
			this.leasingType = leasingType;
		}

		public Logger getLogger() {
			return logger;
		}

		public Options getOptions() {
			return options;
		}

		public void setOptions(Options options) {
			this.options = options;
		}
		
		public Promotions getPromotions() {
			return promotions;
		}

		public void setPromotions(Promotions promotions) {
			this.promotions = promotions;
		}
		

		
		
		

	}





