package pageobjects.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;

import jakarta.xml.bind.JAXBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import apiEngine.model.get.sftp.order.notification.CommentsNotification;
import apiEngine.model.get.sftp.order.notification.DealerAcceptedNotification;
import apiEngine.model.get.sftp.order.notification.DeliveredNotification;
import apiEngine.model.get.sftp.order.notification.LicensePlateNotification;
import apiEngine.model.get.sftp.order.notification.OrderNotification;
import apiEngine.model.get.sftp.order.notification.OrderNotifications;
import apiEngine.model.get.sftp.order.notification.OrderedNotification;
import apiEngine.model.get.sftp.order.notification.VehicleInNotification;

import com.jcraft.jsch.SftpATTRS;

import common.SftpConnect;
import cucumber.TestContext;
import enums.ContextOrderDetailUI;
import enums.ContextPostOrderImportsRequest;
import helpers.AssertHelper;
import pageobjects.ui.ORD_Order_Page;

public class FTP_PostOrderImportsNotification {
	final Logger logger = LogManager.getLogger(FTP_PostOrderImportsNotification.class);
	
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	FTP_GetOrderNotifications getOrderNotifications;
	ORD_Order_Page orderPage;
	private AssertHelper ah = new AssertHelper();

	String fileName;
	String remoteDir;
	String localFile;
	boolean success = false;
	InputStream inputStream;
	OutputStream out1 = null;
	public Vector<LsEntry> fileList;
	
	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());

	public FTP_PostOrderImportsNotification() throws InterruptedException {
		//testContext = context;
		//orderPage = testContext.getPageObjectManager().getORD_Order_Page();
		//postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		//getOrderNotifications = testContext.getPageObjectManager().getFTP_GetOrderNotifications();

	}

	public void delete_orderImportsNotification() throws JSchException, SftpException {
		SftpConnect sc = new SftpConnect();
		ChannelSftp channelSftp = sc.setupJsch();
		channelSftp.connect();
		logger.debug("Aantal files in directory: "
				+ channelSftp.ls("/799230/ORD/out/.done/*.*").size());
		channelSftp.rm("/799230/ORD/out/.done/*.*");
		logger.debug("Aantal files in directory na delete: "
				+ channelSftp.ls("/799230/ORD/out/.done/*.xml").size());
		logger.debug("Aantal files in directory: "
				+ channelSftp.ls("/799330/ORD/out/.done/*.*").size());
		channelSftp.rm("/799330/ORD/out/.done/*.*");
		logger.debug("Aantal files in directory na delete: "
				+ channelSftp.ls("/799330/ORD/out/.done/*.xml").size());
		logger.debug("Aantal files in directory: "
				+ channelSftp.ls("/799340/ORD/out/.done/*.*").size());
		channelSftp.rm("/799340/ORD/out/.done/*.*");
		logger.debug("Aantal files in directory na delete: "
				+ channelSftp.ls("/799340/ORD/out/.done/*.xml").size());

	}

	public boolean download_orderImportsNotification() throws FileNotFoundException, IOException, SQLException,
			JAXBException, JSchException, SftpException, InterruptedException, NullPointerException, ParseException {

		logger.debug("Start download_orderImportsNotification");
		
		Properties properties = new Properties();
        InputStream inputDriver = new FileInputStream("src/test/resources/runtime/filenumber.properties");
        properties.load(inputDriver);
        String fileNrProp = properties.getProperty("FileNumber");
        Integer fileNrInt = Integer.parseInt(fileNrProp);
        logger.debug("fileNrInt: " + fileNrInt);

		//DateFormat df = new SimpleDateFormat("HHmm");
		//DateFormat df1 = new SimpleDateFormat("yyyyMMdd");
		//String startTime = df.format(new Date());
		//String startDate = df1.format(new Date());
		SftpConnect sc = new SftpConnect();
		ChannelSftp channelSftp = sc.setupJsch();
		channelSftp.connect();

		if(System.getProperty("wallbox").equals("Yes")) {
			localFile = "src/test/resources/runtime/sftpdownloads/wallbox_order_update.xml";
		} else {
			localFile = "src/test/resources/runtime/sftpdownloads/order_update.xml";
		}
		//String localFile = "src/test/resources/runtime/sftpdownloads/wallbox_order_update.xml";
		String localDir = "src/test/resources/runtime/sftpdownloads";
		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			//remoteDir = "/799330/ORD/out/.done/";
			remoteDir = "/799340/ORD/out/.done/";
		} else if(System.getProperty("wallbox").equals("Yes")) {
			remoteDir = "/799189/ORD/out/.done/";
		}
		else {
			remoteDir = "/799230/ORD/out/.done/";
		}

		File fDir = new File(localDir);
		Boolean b = fDir.mkdirs();
		logger.debug("fDir.mkdirs(): " + b);
		File f = new File(localFile);
		//logger.debug("File exists: " + f.exists());
		

		long startWaitTime = System.currentTimeMillis();
		startWaitTime = Math.round(startWaitTime / 1000);
		long endWaitTime = System.currentTimeMillis() + 300000;

		success = false;
		while (System.currentTimeMillis() < endWaitTime && !success) {
			try {
				Vector<LsEntry> filelist;
				if (System.getProperty("leasingType").equals("WALLBOX")) {
					filelist = channelSftp.ls(remoteDir + "wallbox_order_update*.xml");
				} else {
					filelist = channelSftp.ls(remoteDir + "order_update*.xml");
				}


				String nextName = null;
				LsEntry lsEntry = null;
				int nextTime;
				int time;

				for (Object sftpFile : filelist) {
					lsEntry = (ChannelSftp.LsEntry) sftpFile;
					SftpATTRS attrsNew = lsEntry.getAttrs();
					time = attrsNew.getMTime();
					//logger.debug("time: " + time + " startWaitTime: " + startWaitTime);
					if (filelist.size() == 1 && time > startWaitTime) {
						fileName = lsEntry.getFilename();
						//logger.debug("fileName from filelist = 1: " + fileName);
						success = true;
					} else {
						nextName = lsEntry.getFilename();
						//logger.debug("nextName: " + nextName);
						String fileNrNext = nextName.substring(nextName.length() - 8, nextName.length() - 4);
						Integer fileNextInt = Integer.parseInt(fileNrNext);
						//logger.debug("fileNextInt: " + fileNextInt);
						SftpATTRS attrs = lsEntry.getAttrs();
						nextTime = attrs.getMTime();
						//logger.debug("attrs nextTime: " + nextTime + " startWaitTime: " + startWaitTime);
						if (nextTime > startWaitTime && fileNextInt > fileNrInt) {
							fileName = nextName;
							logger.debug("fileName: " + fileName);
							success = true;
						}
					}
				}
			} catch (Exception e) {
				logger.debug("Ophalen van order_update files is mislukt.");

			}
			Thread.sleep(5000);
		}

		if (fileName != null) {
			logger.debug("File name is .... " + fileName);
			//channelSftp.get(remoteDir + fileName, "src/test/resources/runtime/sftpdownloads/order_update.xml");
			channelSftp.get(remoteDir + fileName, localFile);
			String fileNr = fileName.substring(fileName.length() - 8, fileName.length() - 4);


			Properties p = new Properties();
			p.put("FileNumber", fileNr);
			logger.debug("FileNumber: " + fileNr);
			FileOutputStream fos = new FileOutputStream("src/test/resources/runtime/filenumber.properties");
			p.store(fos, "");
		}

		channelSftp.exit();



		logger.debug("End download_orderImportsNotification");
		return success;
	}
	
	public boolean download_orderImportsNotificationLux() throws FileNotFoundException, IOException, SQLException,
	JAXBException, JSchException, SftpException, InterruptedException, NullPointerException, ParseException {

logger.debug("Start download_orderImportsNotification");

Properties properties = new Properties();
InputStream inputDriver = new FileInputStream("src/test/resources/runtime/filenumber.properties");
properties.load(inputDriver);
String fileNrProp = properties.getProperty("FileNumber");
Integer fileNrInt = Integer.parseInt(fileNrProp);
logger.debug("fileNrInt: " + fileNrInt);

//DateFormat df = new SimpleDateFormat("HHmm");
//DateFormat df1 = new SimpleDateFormat("yyyyMMdd");
//String startTime = df.format(new Date());
//String startDate = df1.format(new Date());
SftpConnect sc = new SftpConnect();
ChannelSftp channelSftp = sc.setupJsch();
channelSftp.connect();

String localFile = "src/test/resources/runtime/sftpdownloads/order_update.xml";
String localDir = "src/test/resources/runtime/sftpdownloads";
String remoteDir = "/799340/ORD/out/.done/";
		//String remoteDir = "/799330/ORD/out/.done/";
File fDir = new File(localDir);
Boolean b = fDir.mkdirs();
logger.debug("fDir.mkdirs(): " + b);
File f = new File(localFile);

//f.createNewFile();
logger.debug("File exists: " + f.exists());


long startWaitTime = System.currentTimeMillis();
startWaitTime = Math.round(startWaitTime / 1000);
long endWaitTime = System.currentTimeMillis() + 300000;

success = false;
while (System.currentTimeMillis() < endWaitTime && !success) {
	try {
		Vector<LsEntry> filelist = channelSftp.ls(remoteDir + "order_update*.xml");

		String nextName = null;
		LsEntry lsEntry = null;
		int nextTime;
		int time;

		for (Object sftpFile : filelist) {
			lsEntry = (ChannelSftp.LsEntry) sftpFile;
			SftpATTRS attrsNew = lsEntry.getAttrs();
			time = attrsNew.getMTime();
			logger.debug("time: " + time + " startWaitTime: " + startWaitTime);
			if (filelist.size() == 1 && time > startWaitTime) {
				fileName = lsEntry.getFilename();
				logger.debug("fileName from filelist = 1: " + fileName);
				success = true;
			} else {
				nextName = lsEntry.getFilename();
				logger.debug("nextName: " + nextName);
				String fileNrNext = nextName.substring(nextName.length() - 8, nextName.length() - 4);
				Integer fileNextInt = Integer.parseInt(fileNrNext);
				logger.debug("fileNextInt: " + fileNextInt);
				SftpATTRS attrs = lsEntry.getAttrs();
				nextTime = attrs.getMTime();
				logger.debug("attrs nextTime: " + nextTime + " startWaitTime: " + startWaitTime);
				if (nextTime > startWaitTime && fileNextInt > fileNrInt) {
					fileName = nextName;
					logger.debug("fileName: " + fileName);
					success = true;
				}
			}
		}
	} catch (Exception e) {
		logger.debug("Ophalen van order_update files is mislukt.");

	}
	Thread.sleep(5000);
}

if (fileName != null) {
	logger.debug("File name is .... " + fileName);
	//channelSftp.get(remoteDir + fileName, "src/test/resources/runtime/sftpdownloads/order_update.xml");
	channelSftp.get(remoteDir + fileName, localFile);
}

channelSftp.exit();
		if (fileName != null) {

			String fileNr = fileName.substring(fileName.length() - 8, fileName.length() - 4);
			Properties p = new Properties();
			p.put("FileNumber", fileNr);
			logger.debug("FileNumber: " + fileNr);
			FileOutputStream fos = new FileOutputStream("src/test/resources/runtime/filenumber.properties");
			p.store(fos, "");
		}




return success;
}
	
	public void reset_fileNr() throws IOException {
		Properties p = new Properties();
        p.put("FileNumber", "0000");
        //logger.debug("FileNumber: " + fileNr);
        FileOutputStream fos = new FileOutputStream("src/test/resources/runtime/filenumber.properties");
        p.store(fos, "");
	}

}
