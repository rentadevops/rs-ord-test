package pageobjects.ftp;

import apiEngine.model.post.registration.*;
//import apiEngine.model.post.wallbox.imports.*;
import apiEngine.model.post.wallbox.imports.WallBoxOrder;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import common.SftpConnect;
import helpers.ORD_ContractNumberGenerator;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.api.API_GetOrderByDossierNumber;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;
import java.io.*;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class FTP_RegistrationImports {
	final Logger logger = LogManager.getLogger(FTP_RegistrationImports.class);

	private String xmlns;
	//Meta
	private String registrarDossierNumber;
	private String registrarRSNumber;
	private String userEmail;
	private String versionNumber;
	private String linkedOrderNumber;
	private String comments;
	private String scheduledRegistrationDate;
	//PlateHolder
	private String companyIdentificationVATNumber;
	private String companyIdentificationName;
	private String plateHolderPersonLastName;
	private String plateHolderPersonFirstName;
	private String plateHolderPersonAddressStreet;
	private String plateHolderPersonAddressHouseNumber;
	private String plateHolderPersonAddressBoxNumber;
	private String plateHolderPersonAddressPostalCode;
	private String plateHolderPersonAddressCity;
	private String plateHolderPersonAddressCountry;
	private String plateHolderPersonLanguage;
	private String plateHolderPersonRRN;
	private String plateHolderPersonBirthDate;
	//Client
	private String clientName;
	private String clientVAT;
	//Driver
	private String driverLastName;
	private String driverFirstName;
	private String driverAddressStreet;
	private String driverAddressHouseNumber;
	private String driverAddressBoxNumber;
	private String driverAddressPostalCode;
	private String driverAddressCity;
	private String driverAddressCountry;
	private String driverLanguage;
	private String driverPhoneNumber;
	//Seller
	private String sellerVATNumber;
	private String sellerName;
	//Vehicle
	private String vehicleCarIdentificationBrand;
	private String vehicleCarIdentificationModelDutch;
	private String vehicleCarIdentificationModelFrench;
	private String vehicleCarIdentificationModelEnglish;
	private String vehicleCarIdentificationVIN;
	private String vehicleCarIdentificationVINControlNumber;

	private String vehiclePedelecIdentification;
	private String vehiclePedelecIdentificationPedelecType;
	private String vehiclePedelecIdentificationVIN;
	private String vehiclePedelecIdentificationVINControlNumber;
	//Registration Information

	private String registrationInformationUsedVehicle;
	private String registrationInformationLicensePlatePreviousOwner;
	private String registrationInformationLastRegistrationDate;
	private String registrationInformationLanguageRegistrationPapers;
	private String registrationInformationLicensePlateTakenOver;
	private String registrationInformationLicensePlateFormat;
	private String registrationInformationEmailGreenCard;
	private String registrationInformationFormNumber;
	//Insurance
	private String insuranceBrokerFsmaNumber;
	private String insuranceCompanyFsmaNumber;
	private String insuranceReferenceNumber;
	private String insuranceBeginDate;
	private String insuranceEndDate;
	private String insuranceBIV;
	private String insuranceInsuredValue;
	private String insuranceInsuredValueDiscountExcluded;
	private String insuranceCostCenterClient;

	private String deliveryCompanyName;
	private String deliveryLastName;
	private String deliveryFirstName;
	private String deliveryRushDelivery;
	private String deliveryRequestFrontPlate;
	private String deliveryAddressStreet;
	private String deliveryAddressHouseNumber;
	private String deliveryAddressBoxNumber;
	private String deliveryAddressPostalCode;
	private String deliveryAddressCity;
	private String deliveryAddressCountry;



	API_GetOrderByDossierNumber getOrderByDossierNumber;
	FTP_PostOrderImportsRequest por;

	String number;
	String testString = "INSURANCE-REFERENCE-NUMBER-";
	FileOutputStream fos;

		public FTP_RegistrationImports() throws InterruptedException {
			//getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		}

	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String now = simpleDateFormat.format(new Date());
		
		public void upload_registrationImports(List<List<String>> data) throws FileNotFoundException, IOException, SQLException, JAXBException, JSchException, SftpException, JAXBException {
			
			//List<List<String>> data = table.asLists();
			
			//TODO implement data in feature file and datatable
			RegistrationImports registrationImports = null;;
			
			//xmlns = "http://be/renta/ordering/communication/registrationimport";

			registrarRSNumber = data.get(1).get(1);;
			userEmail = data.get(1).get(2);
			versionNumber = data.get(1).get(3);
			//linkedOrderNumber = "CJLEAJL2865621156";
			//linkedOrderNumber = getOrderByDossierNumber.leasingCompanyDossierNumber;
			Properties properties = new Properties();

			InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
			properties.load(inputDriver);
			registrarDossierNumber = properties.getProperty("DossierNumber");
			logger.debug("dosNr " + registrarDossierNumber +  " van proprty file gelezen");
			linkedOrderNumber = data.get(1).get(4);
			comments = data.get(1).get(5);
			String scheduledRegistrationDateStr = data.get(1).get(7);
			if(scheduledRegistrationDateStr.equals("now")) {
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				scheduledRegistrationDate = simpleDateFormat.format(new Date());
			}
			//scheduledRegistrationDate = data.get(1).get(7);
			companyIdentificationName = data.get(1).get(9);
			companyIdentificationVATNumber = data.get(1).get(8);
			clientName = data.get(1).get(10);
			clientVAT = data.get(1).get(11);
			driverLastName = data.get(1).get(12);
			driverFirstName = data.get(1).get(13);
			driverAddressStreet = data.get(1).get(14);
			driverAddressHouseNumber = data.get(1).get(15);
			driverAddressBoxNumber = data.get(1).get(16);
			driverAddressPostalCode = data.get(1).get(17);
			driverAddressCity = data.get(1).get(18);
			driverAddressCountry = data.get(1).get(19);
			driverLanguage = data.get(1).get(20);
			driverPhoneNumber = data.get(1).get(21);
			sellerName = data.get(1).get(22);
			sellerVATNumber = data.get(1).get(23);
			vehicleCarIdentificationBrand = data.get(1).get(24);
			vehicleCarIdentificationModelDutch = data.get(1).get(25);
			vehicleCarIdentificationModelEnglish = data.get(1).get(26);
			vehicleCarIdentificationVIN = data.get(1).get(27);
			vehicleCarIdentificationVINControlNumber = data.get(1).get(28);
			registrationInformationUsedVehicle = data.get(1).get(29);
			registrationInformationLicensePlatePreviousOwner = data.get(1).get(30);
			registrationInformationLastRegistrationDate = data.get(1).get(31);
			registrationInformationLanguageRegistrationPapers = data.get(1).get(32);
			registrationInformationLicensePlateTakenOver = data.get(1).get(33);
			registrationInformationLicensePlateFormat = LicensePlateFormat.R.value();
			registrationInformationEmailGreenCard = data.get(1).get(35);
			registrationInformationFormNumber = data.get(1).get(36);
			insuranceBrokerFsmaNumber = data.get(1).get(37);
			insuranceCompanyFsmaNumber = data.get(1).get(38);

			inputDriver = new FileInputStream("./src/test/resources/runtime/referencenumber.properties");
			properties.load(inputDriver);
			number = properties.getProperty("number");
			Integer intNumber = Integer.parseInt(number);
			intNumber++;
			number = intNumber.toString();


			Properties p = new Properties();
			p.put("number", number);
			fos = new FileOutputStream("./src/test/resources/runtime/referencenumber.properties");
			p.store(fos, "");
			logger.debug("number " + number);
			insuranceReferenceNumber = testString + number;
			//insuranceReferenceNumber = "999993344";
			String insuranceBeginDateStr = data.get(1).get(7);
			if(insuranceBeginDateStr.equals("now")) {
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				insuranceBeginDate = simpleDateFormat.format(new Date());
			}
			//insuranceBeginDate = data.get(1).get(40);
			insuranceEndDate = data.get(1).get(41);
			insuranceBIV = data.get(1).get(42);
			insuranceInsuredValue = data.get(1).get(43);
			insuranceInsuredValueDiscountExcluded = data.get(1).get(44);
			insuranceCostCenterClient = data.get(1).get(45);
			deliveryCompanyName = data.get(1).get(46);
			deliveryLastName = data.get(1).get(47);
			deliveryFirstName = data.get(1).get(48);
			deliveryRushDelivery = data.get(1).get(49);
			deliveryRequestFrontPlate = data.get(1).get(50);
			deliveryAddressStreet = data.get(1).get(51);
			deliveryAddressHouseNumber = data.get(1).get(52);
			deliveryAddressBoxNumber = data.get(1).get(53);
			deliveryAddressPostalCode = data.get(1).get(54);
			deliveryAddressCity = data.get(1).get(55);
			deliveryAddressCountry = data.get(1).get(56);



			
			//System.setProperty("leasingType", leasingType);

			
			ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
			registrarDossierNumber = mcng.getRandomContractNumber();
			p = new Properties();
	        p.put("DossierNumber", registrarDossierNumber);
	        FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/dossiernumber.properties");
	        p.store(fos, "");
	        logger.debug("dosNr " + registrarDossierNumber +  " naar proprty file geschreven");
			
	        registrationImports = new RegistrationImports();
			Header header = new Header();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			   LocalDateTime now = LocalDateTime.now();  
			   header.setCreatedDate(dtf.format(now));
			   RegisterVehicle registerVehicle = new RegisterVehicle();
				Meta meta = new Meta();
					//meta.setRegistrarDossierNumber(registrarDossierNumber);
					//meta.setRegistrarRSNumber(registrarRSNumber);
					meta.setUserEmail(userEmail);
					//meta.setLinkedOrderNumber(linkedOrderNumber);
					//meta.setComments(comments);
					//meta.setScheduledRegistrationDate(dtf.format(now));
				PlateHolder plateHolder = new PlateHolder();
					CompanyIdentification companyIdentification = new CompanyIdentification();
						companyIdentification.setVATNumber(companyIdentificationVATNumber);
						companyIdentification.setName(companyIdentificationName);
						plateHolder.setCompanyIdentification(companyIdentification);
				Client client = new Client();
					client.setName(clientName);
					//client.setVATNumber(clientVAT);
					Driver driver = new Driver();
					//driver.setLastName(deliveryLastName);
					//driver.setFirstName(driverFirstName);
					Address address = new Address();
						//address.setStreet(driverAddressStreet);
						//address.setHouseNumber(driverAddressHouseNumber);
						//address.setBoxNumber(driverAddressBoxNumber);
						address.setPostalCode(driverAddressPostalCode);
						address.setCity(driverAddressCity);
						//driver.setLanguage(driverLanguage);
						//driver.setPhoneNumber(driverPhoneNumber);
						driver.setAddress(address);
				Seller seller = new Seller();
					seller.setName(sellerName);
					seller.setVATNumber(sellerVATNumber);
				Vehicle vehicle = new Vehicle();
					CarIdentification carIdentification = new CarIdentification();
					carIdentification.setBrand(vehicleCarIdentificationBrand);
					Translatable translatable = new Translatable();
					translatable.setDutch(vehicleCarIdentificationModelDutch);
					translatable.setEnglish(vehicleCarIdentificationModelEnglish);
					translatable.setFrench(vehicleCarIdentificationModelFrench);
					carIdentification.setModel(translatable);
					carIdentification.setVIN(vehicleCarIdentificationVIN);
					carIdentification.setVINControlNumber(vehicleCarIdentificationVINControlNumber);
					//vehicle.setCarIdentification(carIdentification);
				RegistrationInformation registrationInformation = new RegistrationInformation();
					registrationInformation.setUsedVehicle(Boolean.parseBoolean(registrationInformationUsedVehicle));
					registrationInformation.setLicensePlatePreviousOwner(registrationInformationLicensePlatePreviousOwner);
					registrationInformation.setLastRegistrationDate(registrationInformationLastRegistrationDate);
					//LanguageRegistrationPapers LanguageRegistrationPapers = new LanguageRegistrationPapers();
					registrationInformation.setLanguageRegistrationPapers(LanguageRegistrationPapers.fromValue("nl"));
					//registrationInformation.setLicensePlateTakenOver(registrationInformationLicensePlateTakenOver);
					registrationInformation.setLicensePlateFormat(LicensePlateFormat.R);
					registrationInformation.setEmailGreenCard(registrationInformationEmailGreenCard);
					registrationInformation.setFormNumber(registrationInformationFormNumber);
					Insurance insurance = new Insurance();
						//insurance.setInsuranceBrokerFsmaNumber(insuranceBrokerFsmaNumber);
						//insurance.setInsuranceCompanyFsmaNumber(insuranceCompanyFsmaNumber);
						//insurance.setInsuranceReferenceNumber(insuranceReferenceNumber);
						//insurance.setBeginDate(insuranceBeginDate);
						//insurance.setEndDate(insuranceEndDate);
						//insurance.setBIV(insuranceBIV);
						//insurance.setInsuredValue(insuranceInsuredValue);
						//insurance.setCostCenterClient(insuranceCostCenterClient);
					Delivery delivery = new Delivery();
						//delivery.setCompanyName(deliveryCompanyName);
						//delivery.setLastName(deliveryLastName);
						//delivery.setFirstName(deliveryFirstName);
						//delivery.setRushDelivery(Boolean.parseBoolean(deliveryRushDelivery));
						//delivery.setRequestFrontPlate(Boolean.parseBoolean(deliveryRequestFrontPlate));
						Address delAddress = new Address();
						//	delAddress.setStreet(deliveryAddressStreet);
						//	delAddress.setHouseNumber(deliveryAddressHouseNumber);
						//	delAddress.setBoxNumber(deliveryAddressBoxNumber);
							delAddress.setPostalCode(deliveryAddressPostalCode);
							delAddress.setCity(deliveryAddressCity);
							//delAddress.setCountry(deliveryAddressCountry);
						delivery.setAddress(delAddress);

			registerVehicle.setMeta(meta);
			registerVehicle.setPlateHolder(plateHolder);
			registerVehicle.setClient(client);
			registerVehicle.setDriver(driver);
			registerVehicle.setSeller(seller);
			registerVehicle.setVehicle(vehicle);
			registerVehicle.setRegistrationInformation(registrationInformation);
			registerVehicle.setInsurance(insurance);
			registerVehicle.setDelivery(delivery);
						List<RegisterVehicle> listRegisterVehicle= new ArrayList<>();
			listRegisterVehicle.add(registerVehicle);



					//wallBoxImports.set_xmlns(xmlns);
					registrationImports.setHeader(header);
					registrationImports.setRegisterVehicle(registerVehicle);

			
				
				 JAXBContext jaxbContext = JAXBContext.newInstance(registrationImports.getClass());
		            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		            //jaxbMarshaller.marshal(data, System.out);
		            StringWriter sw = new StringWriter();
		            jaxbMarshaller.marshal(registrationImports, sw);
		            String xml = sw.toString();
		            logger.debug("Xml: " + xml);
		            
		            BufferedWriter writer = new BufferedWriter(new FileWriter("./src/test/resources/runtime/uploadRegistration.xml"));
		            writer.write(xml);
		            
		            writer.close();
		            
		            SftpConnect sc = new SftpConnect();
		            ChannelSftp channelSftp = sc.setupJsch();
		    	    channelSftp.connect();
		    	 
		    	    String localFile = "./src/test/resources/runtime/uploadRegistration.xml";

		    	    //String remoteDir = "/799189/ORD/in";
					String remoteDir = "/799340/ORD/in";
		    	 
		    	    channelSftp.put(localFile, remoteDir);
		    	 
		    	    channelSftp.exit();
				
				
			}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	public String getRegistrarDossierNumber() {
		return registrarDossierNumber;
	}

	public void setRegistrarDossierNumber(String registrarDossierNumber) {
		this.registrarDossierNumber = registrarDossierNumber;
	}

	public String getRegistrarRSNumber() {
		return registrarRSNumber;
	}

	public void setRegistrarRSNumber(String registrarRSNumber) {
		this.registrarRSNumber = registrarRSNumber;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getLinkedOrderNumber() {
		return linkedOrderNumber;
	}

	public void setLinkedOrderNumber(String linkedOrderNumber) {
		this.linkedOrderNumber = linkedOrderNumber;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getScheduledRegistrationDate() {
		return scheduledRegistrationDate;
	}

	public void setScheduledRegistrationDate(String scheduledRegistrationDate) {
		this.scheduledRegistrationDate = scheduledRegistrationDate;
	}

	public String getCompanyIdentificationVATNumber() {
		return companyIdentificationVATNumber;
	}

	public void setCompanyIdentificationVATNumber(String companyIdentificationVATNumber) {
		this.companyIdentificationVATNumber = companyIdentificationVATNumber;
	}

	public String getCompanyIdentificationName() {
		return companyIdentificationName;
	}

	public void setCompanyIdentificationName(String companyIdentificationName) {
		this.companyIdentificationName = companyIdentificationName;
	}

	public String getPlateHolderPersonLastName() {
		return plateHolderPersonLastName;
	}

	public void setPlateHolderPersonLastName(String plateHolderPersonLastName) {
		this.plateHolderPersonLastName = plateHolderPersonLastName;
	}

	public String getPlateHolderPersonFirstName() {
		return plateHolderPersonFirstName;
	}

	public void setPlateHolderPersonFirstName(String plateHolderPersonFirstName) {
		this.plateHolderPersonFirstName = plateHolderPersonFirstName;
	}

	public String getPlateHolderPersonAddressStreet() {
		return plateHolderPersonAddressStreet;
	}

	public void setPlateHolderPersonAddressStreet(String plateHolderPersonAddressStreet) {
		this.plateHolderPersonAddressStreet = plateHolderPersonAddressStreet;
	}

	public String getPlateHolderPersonAddressHouseNumber() {
		return plateHolderPersonAddressHouseNumber;
	}

	public void setPlateHolderPersonAddressHouseNumber(String plateHolderPersonAddressHouseNumber) {
		this.plateHolderPersonAddressHouseNumber = plateHolderPersonAddressHouseNumber;
	}

	public String getPlateHolderPersonAddressBoxNumber() {
		return plateHolderPersonAddressBoxNumber;
	}

	public void setPlateHolderPersonAddressBoxNumber(String plateHolderPersonAddressBoxNumber) {
		this.plateHolderPersonAddressBoxNumber = plateHolderPersonAddressBoxNumber;
	}

	public String getPlateHolderPersonAddressPostalCode() {
		return plateHolderPersonAddressPostalCode;
	}

	public void setPlateHolderPersonAddressPostalCode(String plateHolderPersonAddressPostalCode) {
		this.plateHolderPersonAddressPostalCode = plateHolderPersonAddressPostalCode;
	}

	public String getPlateHolderPersonAddressCity() {
		return plateHolderPersonAddressCity;
	}

	public void setPlateHolderPersonAddressCity(String plateHolderPersonAddressCity) {
		this.plateHolderPersonAddressCity = plateHolderPersonAddressCity;
	}

	public String getPlateHolderPersonAddressCountry() {
		return plateHolderPersonAddressCountry;
	}

	public void setPlateHolderPersonAddressCountry(String plateHolderPersonAddressCountry) {
		this.plateHolderPersonAddressCountry = plateHolderPersonAddressCountry;
	}

	public String getPlateHolderPersonLanguage() {
		return plateHolderPersonLanguage;
	}

	public void setPlateHolderPersonLanguage(String plateHolderPersonLanguage) {
		this.plateHolderPersonLanguage = plateHolderPersonLanguage;
	}

	public String getPlateHolderPersonRRN() {
		return plateHolderPersonRRN;
	}

	public void setPlateHolderPersonRRN(String plateHolderPersonRRN) {
		this.plateHolderPersonRRN = plateHolderPersonRRN;
	}

	public String getPlateHolderPersonBirthDate() {
		return plateHolderPersonBirthDate;
	}

	public void setPlateHolderPersonBirthDate(String plateHolderPersonBirthDate) {
		this.plateHolderPersonBirthDate = plateHolderPersonBirthDate;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientVAT() {
		return clientVAT;
	}

	public void setClientVAT(String clientVAT) {
		this.clientVAT = clientVAT;
	}

	public String getDriverLastName() {
		return driverLastName;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverAddressStreet() {
		return driverAddressStreet;
	}

	public void setDriverAddressStreet(String driverAddressStreet) {
		this.driverAddressStreet = driverAddressStreet;
	}

	public String getDriverAddressHouseNumber() {
		return driverAddressHouseNumber;
	}

	public void setDriverAddressHouseNumber(String driverAddressHouseNumber) {
		this.driverAddressHouseNumber = driverAddressHouseNumber;
	}

	public String getDriverAddressBoxNumber() {
		return driverAddressBoxNumber;
	}

	public void setDriverAddressBoxNumber(String driverAddressBoxNumber) {
		this.driverAddressBoxNumber = driverAddressBoxNumber;
	}

	public String getDriverAddressPostalCode() {
		return driverAddressPostalCode;
	}

	public void setDriverAddressPostalCode(String driverAddressPostalCode) {
		this.driverAddressPostalCode = driverAddressPostalCode;
	}

	public String getDriverAddressCity() {
		return driverAddressCity;
	}

	public void setDriverAddressCity(String driverAddressCity) {
		this.driverAddressCity = driverAddressCity;
	}

	public String getDriverAddressCountry() {
		return driverAddressCountry;
	}

	public void setDriverAddressCountry(String driverAddressCountry) {
		this.driverAddressCountry = driverAddressCountry;
	}

	public String getDriverLanguage() {
		return driverLanguage;
	}

	public void setDriverLanguage(String driverLanguage) {
		this.driverLanguage = driverLanguage;
	}

	public String getDriverPhoneNumber() {
		return driverPhoneNumber;
	}

	public void setDriverPhoneNumber(String driverPhoneNumber) {
		this.driverPhoneNumber = driverPhoneNumber;
	}

	public String getSellerVATNumber() {
		return sellerVATNumber;
	}

	public void setSellerVATNumber(String sellerVATNumber) {
		this.sellerVATNumber = sellerVATNumber;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getVehicleCarIdentificationBrand() {
		return vehicleCarIdentificationBrand;
	}

	public void setVehicleCarIdentificationBrand(String vehicleCarIdentificationBrand) {
		this.vehicleCarIdentificationBrand = vehicleCarIdentificationBrand;
	}

	public String getVehicleCarIdentificationModelDutch() {
		return vehicleCarIdentificationModelDutch;
	}

	public void setVehicleCarIdentificationModelDutch(String vehicleCarIdentificationModelDutch) {
		this.vehicleCarIdentificationModelDutch = vehicleCarIdentificationModelDutch;
	}

	public String getVehicleCarIdentificationModelFrench() {
		return vehicleCarIdentificationModelFrench;
	}

	public void setVehicleCarIdentificationModelFrench(String vehicleCarIdentificationModelFrench) {
		this.vehicleCarIdentificationModelFrench = vehicleCarIdentificationModelFrench;
	}

	public String getVehicleCarIdentificationModelEnglish() {
		return vehicleCarIdentificationModelEnglish;
	}

	public void setVehicleCarIdentificationModelEnglish(String vehicleCarIdentificationModelEnglish) {
		this.vehicleCarIdentificationModelEnglish = vehicleCarIdentificationModelEnglish;
	}

	public String getVehicleCarIdentificationVIN() {
		return vehicleCarIdentificationVIN;
	}

	public void setVehicleCarIdentificationVIN(String vehicleCarIdentificationVIN) {
		this.vehicleCarIdentificationVIN = vehicleCarIdentificationVIN;
	}

	public String getVehicleCarIdentificationVINControlNumber() {
		return vehicleCarIdentificationVINControlNumber;
	}

	public void setVehicleCarIdentificationVINControlNumber(String vehicleCarIdentificationVINControlNumber) {
		this.vehicleCarIdentificationVINControlNumber = vehicleCarIdentificationVINControlNumber;
	}

	public String getVehiclePedelecIdentification() {
		return vehiclePedelecIdentification;
	}

	public void setVehiclePedelecIdentification(String vehiclePedelecIdentification) {
		this.vehiclePedelecIdentification = vehiclePedelecIdentification;
	}

	public String getVehiclePedelecIdentificationPedelecType() {
		return vehiclePedelecIdentificationPedelecType;
	}

	public void setVehiclePedelecIdentificationPedelecType(String vehiclePedelecIdentificationPedelecType) {
		this.vehiclePedelecIdentificationPedelecType = vehiclePedelecIdentificationPedelecType;
	}

	public String getVehiclePedelecIdentificationVIN() {
		return vehiclePedelecIdentificationVIN;
	}

	public void setVehiclePedelecIdentificationVIN(String vehiclePedelecIdentificationVIN) {
		this.vehiclePedelecIdentificationVIN = vehiclePedelecIdentificationVIN;
	}

	public String getVehiclePedelecIdentificationVINControlNumber() {
		return vehiclePedelecIdentificationVINControlNumber;
	}

	public void setVehiclePedelecIdentificationVINControlNumber(String vehiclePedelecIdentificationVINControlNumber) {
		this.vehiclePedelecIdentificationVINControlNumber = vehiclePedelecIdentificationVINControlNumber;
	}

	public String getRegistrationInformationUsedVehicle() {
		return registrationInformationUsedVehicle;
	}

	public void setRegistrationInformationUsedVehicle(String registrationInformationUsedVehicle) {
		this.registrationInformationUsedVehicle = registrationInformationUsedVehicle;
	}

	public String getRegistrationInformationLicensePlatePreviousOwner() {
		return registrationInformationLicensePlatePreviousOwner;
	}

	public void setRegistrationInformationLicensePlatePreviousOwner(String registrationInformationLicensePlatePreviousOwner) {
		this.registrationInformationLicensePlatePreviousOwner = registrationInformationLicensePlatePreviousOwner;
	}

	public String getRegistrationInformationLastRegistrationDate() {
		return registrationInformationLastRegistrationDate;
	}

	public void setRegistrationInformationLastRegistrationDate(String registrationInformationLastRegistrationDate) {
		this.registrationInformationLastRegistrationDate = registrationInformationLastRegistrationDate;
	}

	public String getRegistrationInformationLanguageRegistrationPapers() {
		return registrationInformationLanguageRegistrationPapers;
	}

	public void setRegistrationInformationLanguageRegistrationPapers(String registrationInformationLanguageRegistrationPapers) {
		this.registrationInformationLanguageRegistrationPapers = registrationInformationLanguageRegistrationPapers;
	}

	public String getRegistrationInformationLicensePlateTakenOver() {
		return registrationInformationLicensePlateTakenOver;
	}

	public void setRegistrationInformationLicensePlateTakenOver(String registrationInformationLicensePlateTakenOver) {
		this.registrationInformationLicensePlateTakenOver = registrationInformationLicensePlateTakenOver;
	}

	public String getRegistrationInformationLicensePlateFormat() {
		return registrationInformationLicensePlateFormat;
	}

	public void setRegistrationInformationLicensePlateFormat(String registrationInformationLicensePlateFormat) {
		this.registrationInformationLicensePlateFormat = registrationInformationLicensePlateFormat;
	}

	public String getRegistrationInformationEmailGreenCard() {
		return registrationInformationEmailGreenCard;
	}

	public void setRegistrationInformationEmailGreenCard(String registrationInformationEmailGreenCard) {
		this.registrationInformationEmailGreenCard = registrationInformationEmailGreenCard;
	}

	public String getRegistrationInformationFormNumber() {
		return registrationInformationFormNumber;
	}

	public void setRegistrationInformationFormNumber(String registrationInformationFormNumber) {
		this.registrationInformationFormNumber = registrationInformationFormNumber;
	}

	public String getInsuranceBrokerFsmaNumber() {
		return insuranceBrokerFsmaNumber;
	}

	public void setInsuranceBrokerFsmaNumber(String insuranceBrokerFsmaNumber) {
		this.insuranceBrokerFsmaNumber = insuranceBrokerFsmaNumber;
	}

	public String getInsuranceCompanyFsmaNumber() {
		return insuranceCompanyFsmaNumber;
	}

	public void setInsuranceCompanyFsmaNumber(String insuranceCompanyFsmaNumber) {
		this.insuranceCompanyFsmaNumber = insuranceCompanyFsmaNumber;
	}

	public String getInsuranceReferenceNumber() {
		return insuranceReferenceNumber;
	}

	public void setInsuranceReferenceNumber(String insuranceReferenceNumber) {
		this.insuranceReferenceNumber = insuranceReferenceNumber;
	}

	public String getInsuranceBeginDate() {
		return insuranceBeginDate;
	}

	public void setInsuranceBeginDate(String insuranceBeginDate) {
		this.insuranceBeginDate = insuranceBeginDate;
	}

	public String getInsuranceEndDate() {
		return insuranceEndDate;
	}

	public void setInsuranceEndDate(String insuranceEndDate) {
		this.insuranceEndDate = insuranceEndDate;
	}

	public String getInsuranceBIV() {
		return insuranceBIV;
	}

	public void setInsuranceBIV(String insuranceBIV) {
		this.insuranceBIV = insuranceBIV;
	}

	public String getInsuranceInsuredValue() {
		return insuranceInsuredValue;
	}

	public void setInsuranceInsuredValue(String insuranceInsuredValue) {
		this.insuranceInsuredValue = insuranceInsuredValue;
	}

	public String getInsuranceInsuredValueDiscountExcluded() {
		return insuranceInsuredValueDiscountExcluded;
	}

	public void setInsuranceInsuredValueDiscountExcluded(String insuranceInsuredValueDiscountExcluded) {
		this.insuranceInsuredValueDiscountExcluded = insuranceInsuredValueDiscountExcluded;
	}

	public String getInsuranceCostCenterClient() {
		return insuranceCostCenterClient;
	}

	public void setInsuranceCostCenterClient(String insuranceCostCenterClient) {
		this.insuranceCostCenterClient = insuranceCostCenterClient;
	}

	public String getDeliveryCompanyName() {
		return deliveryCompanyName;
	}

	public void setDeliveryCompanyName(String deliveryCompanyName) {
		this.deliveryCompanyName = deliveryCompanyName;
	}

	public String getDeliveryLastName() {
		return deliveryLastName;
	}

	public void setDeliveryLastName(String deliveryLastName) {
		this.deliveryLastName = deliveryLastName;
	}

	public String getDeliveryFirstName() {
		return deliveryFirstName;
	}

	public void setDeliveryFirstName(String deliveryFirstName) {
		this.deliveryFirstName = deliveryFirstName;
	}

	public String getDeliveryRushDelivery() {
		return deliveryRushDelivery;
	}

	public void setDeliveryRushDelivery(String deliveryRushDelivery) {
		this.deliveryRushDelivery = deliveryRushDelivery;
	}

	public String getDeliveryRequestFrontPlate() {
		return deliveryRequestFrontPlate;
	}

	public void setDeliveryRequestFrontPlate(String deliveryRequestFrontPlate) {
		this.deliveryRequestFrontPlate = deliveryRequestFrontPlate;
	}

	public String getDeliveryAddressStreet() {
		return deliveryAddressStreet;
	}

	public void setDeliveryAddressStreet(String deliveryAddressStreet) {
		this.deliveryAddressStreet = deliveryAddressStreet;
	}

	public String getDeliveryAddressHouseNumber() {
		return deliveryAddressHouseNumber;
	}

	public void setDeliveryAddressHouseNumber(String deliveryAddressHouseNumber) {
		this.deliveryAddressHouseNumber = deliveryAddressHouseNumber;
	}

	public String getDeliveryAddressBoxNumber() {
		return deliveryAddressBoxNumber;
	}

	public void setDeliveryAddressBoxNumber(String deliveryAddressBoxNumber) {
		this.deliveryAddressBoxNumber = deliveryAddressBoxNumber;
	}

	public String getDeliveryAddressPostalCode() {
		return deliveryAddressPostalCode;
	}

	public void setDeliveryAddressPostalCode(String deliveryAddressPostalCode) {
		this.deliveryAddressPostalCode = deliveryAddressPostalCode;
	}

	public String getDeliveryAddressCity() {
		return deliveryAddressCity;
	}

	public void setDeliveryAddressCity(String deliveryAddressCity) {
		this.deliveryAddressCity = deliveryAddressCity;
	}

	public String getDeliveryAddressCountry() {
		return deliveryAddressCountry;
	}

	public void setDeliveryAddressCountry(String deliveryAddressCountry) {
		this.deliveryAddressCountry = deliveryAddressCountry;
	}
}





