package pageobjects.ftp;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import apiEngine.model.get.order.detail.Agent;
import apiEngine.model.get.order.detail.Buyback;
import apiEngine.model.get.order.detail.Client;
import apiEngine.model.get.order.detail.Dealer;
import apiEngine.model.get.order.detail.Delivery;
import apiEngine.model.get.order.detail.DossierManager;
import apiEngine.model.get.order.detail.Driver;
import apiEngine.model.get.order.detail.FinancialLeaseRegistrationInfo;
import apiEngine.model.get.order.detail.InscriptionCosts;
import apiEngine.model.get.order.detail.Insurance;
import apiEngine.model.get.order.detail.LastComment;
import apiEngine.model.get.order.detail.LeasingCompany;
import apiEngine.model.get.order.detail.OptionDTO;
import apiEngine.model.get.order.detail.OrderDetail;
import apiEngine.model.get.order.detail.Pickup;
import apiEngine.model.get.order.detail.Pricing;
import apiEngine.model.get.order.detail.Product;
import apiEngine.model.get.order.detail.PromotionDTO;
import apiEngine.model.get.order.detail.TaxStampCosts;
import apiEngine.model.get.order.detail.TyreFitter;
import apiEngine.model.get.order.detail.Vehicle;
import apiEngine.model.get.sftp.order.notification.CommentsNotification;
import apiEngine.model.get.sftp.order.notification.DealerAcceptedNotification;
import apiEngine.model.get.sftp.order.notification.DeliveredNotification;
import apiEngine.model.get.sftp.order.notification.LicensePlateNotification;
import apiEngine.model.get.sftp.order.notification.OrderNotifications;
import apiEngine.model.get.sftp.order.notification.OrderedNotification;
import apiEngine.model.get.sftp.order.notification.VehicleInNotification;
import apiEngine.model.post.order.imports.LeasingType;
import cucumber.TestContext;
import helpers.AssertHelper;
import pageobjects.ui.ORD_Order_Page;





public class FTP_GetOrderNotifications {
	final Logger logger = LogManager.getLogger(FTP_GetOrderNotifications.class);
	
	TestContext testContext;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	ORD_Order_Page orderPage;
	private AssertHelper ah = new AssertHelper();

	String fileName;
	boolean success = false;
	InputStream inputStream;
	OutputStream out1 = null;
	
	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());
	String localFile = "./src/test/resources/runtime/sftpdownloads/order_update.xml";

	


		String comnotTransactionCode;
		String comnotNotificationDate;
		String comnotComments;
		String comnotDossierNumber;
		String deanotTransactionCode;
		String deanotNotificationDate;
		String deanotComments;
		String deanotDossierNumber;
		String ordnotTransactionCode;
		String ordnotNotificationDate;
		String ordnotDossierNumber;
		String ordnotComments;
		String vinnotTransactionCode;
		String vinnotNotificationDate;
		String vinnotDossierNumber;
		String vinnotVinNumber;
		String vinnotVinControlNumber;
		String vinnotWeight;
		String vinnotCylinderContent;
		String vinnotPower;
		String vinnotCo2Emissions;
		String vinnotComments;
		String vinnotDateFirstRegistration;
		String vinnotDateLastRegistration;
		String vinnotFuel;
		String vinnotLicensePlateNumber;
		String licnotTransactionCode;
		String licnotNotificationDate;
		String licnotDossierNumber;
		String licnotLicensePlateNumber;
		String licnotCo2Emissions;
		String licnotDateFirstRegistration;
		String delnotTransactionCode;
		String delnotNotificationDate;
		String delnotDossierNumber;
		String delnotCo2Emissions;
		String delnotDateFirstRegistration;
		Integer delnotMileage;
		Integer delnotConstructionYear;
		String delnotKeyCode;
		String delnotStartCode;
		Boolean delnotPreviousVehicleDropoff;
		String delnotPreviousVehicleLicensePlate;
		String delnotPreviousVehicleOwner;
		String delnotActualDeliveryDate;
		String delnotDriverFirstName;
		String delnotDriverLastName;
		String delnotDriverBirthDate;
		String delnotDriverBirthPlace;
		Boolean delnotRegistrationCertificate;
		Boolean delnotInsuranceCertificate;
		Boolean delnotCertificateOfConformity;
		Boolean delnotTechnicalInspectionCertificate;
		Boolean delnotLegalKit;
		Boolean delnotServiceManual;
		Boolean delnotFuelCard;
		String delnotNumberOfKeys;
		String delnotTyreSpecification;
		String delnotTyreBrand;
		String delnotTyreType;
		String delnotExteriorColor;
		String delnotInteriorColor;

		
	public FTP_GetOrderNotifications(OrderNotifications orderNotifications) {
		
	}
	
	public void get_order_notifications_data(OrderNotifications orderNotifications) throws ParseException, NullPointerException, JAXBException {
		
		JAXBContext jaxbContext;

		jaxbContext = JAXBContext.newInstance(OrderNotifications.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		// OrderNotifications orderNotifications = (OrderNotifications)
		// jaxbUnmarshaller.unmarshal(new
		// File("./src/test/resources/runtime/sftpdownloads/order_update_nn.xml"));
		orderNotifications = (OrderNotifications) jaxbUnmarshaller
				.unmarshal(new File(localFile));
		logger.debug("orderNotifications: " + orderNotifications);
		for (int i=0;i < orderNotifications.getOrderNotification().size(); i++) {
		CommentsNotification comnot = orderNotifications.getOrderNotification().get(i).getCommentsNotification();
		DealerAcceptedNotification deanot = orderNotifications.getOrderNotification().get(i).getDealerAcceptedNotification();
		OrderedNotification ordnot = orderNotifications.getOrderNotification().get(i).getOrderedNotification();
		VehicleInNotification vinnot = orderNotifications.getOrderNotification().get(i).getVehicleInNotification();
		LicensePlateNotification licnot = orderNotifications.getOrderNotification().get(i).getLicensePlateNotification();
		DeliveredNotification delnot = orderNotifications.getOrderNotification().get(i).getDeliveredNotification();
		if (comnot != null) {
			//String tc = orderNotifications.getOrderNotification().get(0).getCommentsNotification().getTransactionCode();
			comnotTransactionCode = comnot.getTransactionCode();
			//ah.assertEquals("TransactionCode ", comnotTransactionCode, "COMMENTS");
			//logger.debug("TransactionCode: " + comnotTransactionCode);
			comnotNotificationDate = comnot.getNotificationDate();
			//ah.assertEquals("NotificationDate ", comnotNotificationDate, dateToday);
			//logger.debug("NotificationDate: " + comnotNotificationDate);
			comnotComments = comnot.getComments();
			//logger.debug("Comments: " + comnotComments);
			comnotDossierNumber = comnot.getDossierNumber();
			//ah.assertEquals("DossierNumber ", comnotDossierNumber, postOrderImportsRequest.getDosNr());
			//logger.debug("DossierNumber: " + comnotDossierNumber);
		} else if (deanot != null) {
			deanotTransactionCode = deanot.getTransactionCode();
			//ah.assertEquals("TransactionCode ", deanotTransactionCode, "DEALER_ACCEPTED");
			//logger.debug("TransactionCode: " + deanotTransactionCode);
			deanotNotificationDate = deanot.getNotificationDate();
			//ah.assertEquals("NotificationDate ", deanotNotificationDate, dateToday);
			//logger.debug("NotificationDate: " + deanotNotificationDate);
			deanotComments = deanot.getComments();
			//logger.debug("Comments: " + deanotComments);
			deanotDossierNumber = deanot.getDossierNumber();
			//ah.assertEquals("DossierNumber ", deanotDossierNumber, postOrderImportsRequest.getDosNr());
			//logger.debug("DossierNumber: " + deanotDossierNumber);
		} else if (ordnot != null) {
			ordnotTransactionCode = ordnot.getTransactionCode();
			//logger.debug("TransactionCode: " + ordnotTransactionCode);
			ordnotNotificationDate = ordnot.getNotificationDate();
			//logger.debug("NotificationDate: " + ordnotNotificationDate);
			ordnotDossierNumber = ordnot.getDossierNumber();
			//logger.debug("DossierNumber: " + ordnotDossierNumber);
			ordnotComments = ordnot.getComments();
			//logger.debug("Comments: " + ordnotComments);
		} else if (vinnot != null) {
			vinnotTransactionCode = vinnot.getTransactionCode();
			//logger.debug("TransactionCode: " + vinnotTransactionCode);
			vinnotNotificationDate = vinnot.getNotificationDate();
			//logger.debug("NotificationDate: " + vinnotNotificationDate);
			vinnotDossierNumber = vinnot.getDossierNumber();
			//logger.debug("DossierNumber: " + vinnotDossierNumber);
			vinnotVinNumber = vinnot.getVinNumber();
			//logger.debug("VinNumber: " + vinnotVinNumber);
			vinnotVinControlNumber = vinnot.getVinControlNumber();
			//logger.debug("VinControlNumber: " + vinnotVinControlNumber);
			vinnotWeight = vinnot.getWeight();
			//logger.debug("vinnotWeight: " + vinnotWeight);
			vinnotCylinderContent = vinnot.getCylinderContent();
			//logger.debug("vinnotCylinderContent: " + vinnotCylinderContent);
			vinnotPower = vinnot.getPower();
			//logger.debug("vinnotPower: " + vinnotPower);
			vinnotCo2Emissions = vinnot.getCo2Emissions();
			//logger.debug("vinnotCo2Emissions: " + vinnotCo2Emissions);
			vinnotComments = vinnot.getComments();
			//logger.debug("vinnotComments: " + vinnotComments);
			vinnotDateFirstRegistration = vinnot.getDateFirstRegistration();
			//logger.debug("vinnotDateFirstRegistration: " + vinnotDateFirstRegistration);
			vinnotDateLastRegistration = vinnot.getDateLastRegistration();
			//logger.debug("vinnotDateLastRegistration: " + vinnotDateLastRegistration);
			vinnotFuel = vinnot.getFuel();
			//logger.debug("vinnotFuel: " + vinnotFuel);
			vinnotLicensePlateNumber = vinnot.getLicensePlateNumber();
			//logger.debug("vinnotLicensePlateNumber: " + vinnotLicensePlateNumber);
		} else if (licnot != null) {
			licnotTransactionCode = licnot.getTransactionCode();
			//logger.debug("licnotTransactionCode: " + licnotTransactionCode);
			licnotNotificationDate = licnot.getNotificationDate();
			//logger.debug("licnotNotificationDate: " + licnotNotificationDate);
			licnotDossierNumber = licnot.getDossierNumber();
			//logger.debug("licnotDossierNumber: " + licnotDossierNumber);
			licnotLicensePlateNumber = licnot.getLicensePlateNumber();
			//logger.debug("licnotLicensePlateNumber: " + licnotLicensePlateNumber);
			licnotCo2Emissions = licnot.getCo2Emissions();
			//logger.debug("licnotCo2Emissions: " + licnotCo2Emissions);
			licnotDateFirstRegistration = licnot.getDateFirstRegistration();
			//logger.debug("licnotDateFirstRegistration: " + licnotDateFirstRegistration);
			
		} else if (delnot != null) {
			delnotTransactionCode = delnot.getTransactionCode();
			//logger.debug("delnotTransactionCode: " + delnotTransactionCode);
			delnotNotificationDate = delnot.getNotificationDate();
			//logger.debug("delnotNotificationDate: " + delnotNotificationDate);
			delnotDossierNumber = delnot.getDossierNumber();
			//logger.debug("delnotDossierNumber: " + delnotDossierNumber);
			delnotCo2Emissions = delnot.getCo2Emissions();
			//logger.debug("delnotCo2Emissions: " + delnotCo2Emissions);
			delnotDateFirstRegistration = delnot.getDateFirstRegistration();
			//logger.debug("delnotDateFirstRegistration: " + delnotDateFirstRegistration);
			delnotMileage = delnot.getMileage();
			//logger.debug("delnotMileage: " + delnotMileage);
			delnotConstructionYear = delnot.getConstructionYear();
			//logger.debug("delnotConstructionYear: " + delnotConstructionYear);
			delnotKeyCode = delnot.getKeyCode();
			//logger.debug("delnotKeyCode: " + delnotKeyCode);
			delnotStartCode = delnot.getStartCode();
			//logger.debug("delnotStartCode: " + delnotStartCode);
			delnotPreviousVehicleDropoff = delnot.isPreviousVehicleDropoff();
			//logger.debug("delnotPreviousVehicleDropoff: " + delnotPreviousVehicleDropoff);
			delnotPreviousVehicleLicensePlate = delnot.getPreviousVehicleLicensePlate();
			//logger.debug("delnotPreviousVehicleLicensePlate: " + delnotPreviousVehicleLicensePlate);
			delnotPreviousVehicleOwner = delnot.getPreviousVehicleOwner();
			//logger.debug("delnotPreviousVehicleOwner: " + delnotPreviousVehicleOwner);
			delnotActualDeliveryDate = delnot.getActualDeliveryDate();
			//logger.debug("delnotActualDeliveryDate: " + delnotActualDeliveryDate);
			delnotDriverFirstName = delnot.getDriverFirstName();
			//logger.debug("delnotDriverFirstName: " + delnotDriverFirstName);
			delnotDriverLastName = delnot.getDriverLastName();
			//logger.debug("delnotDriverLastName: " + delnotDriverLastName);
			delnotDriverBirthDate = delnot.getDriverBirthDate();
			//logger.debug("delnotDriverBirthDate: " + delnotDriverBirthDate);
			delnotDriverBirthPlace = delnot.getDriverBirthPlace();
			//logger.debug("delnotDriverBirthPlace: " + delnotDriverBirthPlace);
			delnotRegistrationCertificate = delnot.isRegistrationCertificate();
			//logger.debug("delnotRegistrationCertificate: " + delnotRegistrationCertificate);
			delnotInsuranceCertificate = delnot.isInsuranceCertificate();
			//logger.debug("delnotInsuranceCertificate: " + delnotInsuranceCertificate);
			delnotCertificateOfConformity = delnot.isCertificateOfConformity();
			//logger.debug("delnotCertificateOfConformity: " + delnotCertificateOfConformity);
			delnotTechnicalInspectionCertificate = delnot.isTechnicalInspectionCertificate();
			//logger.debug("delnotTechnicalInspectionCertificate: " + delnotTechnicalInspectionCertificate);
			delnotLegalKit = delnot.isLegalKit();
			//logger.debug("delnotLegalKit: " + delnotLegalKit);
			delnotServiceManual = delnot.isServiceManual();
			//logger.debug("delnotServiceManual: " + delnotServiceManual);
			delnotFuelCard = delnot.isFuelCard();
			//logger.debug("delnotFuelCard: " + delnotFuelCard);
			delnotNumberOfKeys = delnot.getNumberOfKeys();
			//logger.debug("delnotNumberOfKeys: " + delnotNumberOfKeys);
			delnotTyreSpecification = delnot.getTyreSpecification();
			//logger.debug("delnotTyreSpecification: " + delnotTyreSpecification);
			delnotTyreBrand = delnot.getTyreBrand();
			//logger.debug("delnotTyreBrand: " + delnotTyreBrand);
			delnotTyreType = delnot.getTyreType();
			//logger.debug("delnotTyreType: " + delnotTyreType);
			if(!System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
				if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
					delnotExteriorColor = null;
					delnotInteriorColor = null;
				} else {
					try {
						delnotExteriorColor = delnot.getExteriorColor().getDutch();
						//logger.debug("delnotExteriorColor: " + delnotExteriorColor);
						delnotInteriorColor = delnot.getInteriorColor().getDutch();
						//logger.debug("delnotInteriorColor: " + delnotInteriorColor);
					} catch(Exception e) {
						logger.debug("delnotExteriorColor/delnotInteriorColor is null or empty. ");
					}

				}
			}
			
		}
	}
		
		

	}


	public String getComnotTransactionCode() {
		return comnotTransactionCode;
	}

	public String getComnotNotificationDate() {
		return comnotNotificationDate;
	}

	public String getComnotComments() {
		return comnotComments;
	}

	public String getComnotDossierNumber() {
		return comnotDossierNumber;
	}

	public String getDeanotTransactionCode() {
		return deanotTransactionCode;
	}

	public String getDeanotNotificationDate() {
		return deanotNotificationDate;
	}

	public String getDeanotComments() {
		return deanotComments;
	}

	public String getDeanotDossierNumber() {
		return deanotDossierNumber;
	}

	public String getOrdnotTransactionCode() {
		return ordnotTransactionCode;
	}

	public String getOrdnotNotificationDate() {
		return ordnotNotificationDate;
	}

	public String getOrdnotDossierNumber() {
		return ordnotDossierNumber;
	}

	public String getOrdnotComments() {
		return ordnotComments;
	}

	public String getVinnotTransactionCode() {
		return vinnotTransactionCode;
	}

	public String getVinnotNotificationDate() {
		return vinnotNotificationDate;
	}

	public String getVinnotDossierNumber() {
		return vinnotDossierNumber;
	}

	public String getVinnotVinNumber() {
		return vinnotVinNumber;
	}

	public String getVinnotVinControlNumber() {
		return vinnotVinControlNumber;
	}

	public String getVinnotWeight() {
		return vinnotWeight;
	}

	public String getVinnotCylinderContent() {
		return vinnotCylinderContent;
	}

	public String getVinnotPower() {
		return vinnotPower;
	}

	public String getVinnotCo2Emissions() {
		return vinnotCo2Emissions;
	}

	public String getVinnotComments() {
		return vinnotComments;
	}

	public String getVinnotDateFirstRegistration() {
		return vinnotDateFirstRegistration;
	}

	public String getVinnotDateLastRegistration() {
		return vinnotDateLastRegistration;
	}

	public String getVinnotFuel() {
		return vinnotFuel;
	}

	public String getVinnotLicensePlateNumber() {
		return vinnotLicensePlateNumber;
	}

	public String getLicnotTransactionCode() {
		return licnotTransactionCode;
	}

	public String getLicnotNotificationDate() {
		return licnotNotificationDate;
	}

	public String getLicnotDossierNumber() {
		return licnotDossierNumber;
	}

	public String getLicnotLicensePlateNumber() {
		return licnotLicensePlateNumber;
	}

	public String getLicnotCo2Emissions() {
		return licnotCo2Emissions;
	}

	public String getLicnotDateFirstRegistration() {
		return licnotDateFirstRegistration;
	}

	public String getDelnotTransactionCode() {
		return delnotTransactionCode;
	}

	public String getDelnotNotificationDate() {
		return delnotNotificationDate;
	}

	public String getDelnotDossierNumber() {
		return delnotDossierNumber;
	}

	public String getDelnotCo2Emissions() {
		return delnotCo2Emissions;
	}

	public String getDelnotDateFirstRegistration() {
		return delnotDateFirstRegistration;
	}

	public Integer getDelnotMileage() {
		return delnotMileage;
	}

	public Integer getDelnotConstructionYear() {
		return delnotConstructionYear;
	}

	public String getDelnotKeyCode() {
		return delnotKeyCode;
	}

	public String getDelnotStartCode() {
		return delnotStartCode;
	}

	public Boolean getDelnotPreviousVehicleDropoff() {
		return delnotPreviousVehicleDropoff;
	}

	public String getDelnotPreviousVehicleLicensePlate() {
		return delnotPreviousVehicleLicensePlate;
	}

	public String getDelnotPreviousVehicleOwner() {
		return delnotPreviousVehicleOwner;
	}

	public String getDelnotActualDeliveryDate() {
		return delnotActualDeliveryDate;
	}

	public String getDelnotDriverFirstName() {
		return delnotDriverFirstName;
	}

	public String getDelnotDriverLastName() {
		return delnotDriverLastName;
	}

	public String getDelnotDriverBirthDate() {
		return delnotDriverBirthDate;
	}

	public String getDelnotDriverBirthPlace() {
		return delnotDriverBirthPlace;
	}

	public Boolean getDelnotRegistrationCertificate() {
		return delnotRegistrationCertificate;
	}

	public Boolean getDelnotInsuranceCertificate() {
		return delnotInsuranceCertificate;
	}

	public Boolean getDelnotCertificateOfConformity() {
		return delnotCertificateOfConformity;
	}

	public Boolean getDelnotTechnicalInspectionCertificate() {
		return delnotTechnicalInspectionCertificate;
	}

	public Boolean getDelnotLegalKit() {
		return delnotLegalKit;
	}

	public Boolean getDelnotServiceManual() {
		return delnotServiceManual;
	}

	public Boolean getDelnotFuelCard() {
		return delnotFuelCard;
	}

	public String getDelnotNumberOfKeys() {
		return delnotNumberOfKeys;
	}

	public String getDelnotTyreSpecification() {
		return delnotTyreSpecification;
	}

	public String getDelnotTyreBrand() {
		return delnotTyreBrand;
	}

	public String getDelnotTyreType() {
		return delnotTyreType;
	}

	public String getDelnotExteriorColor() {
		return delnotExteriorColor;
	}

	public String getDelnotInteriorColor() {
		return delnotInteriorColor;
	}

	
	
	
}
		
		