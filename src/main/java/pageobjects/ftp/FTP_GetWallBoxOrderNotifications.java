package pageobjects.ftp;

//import apiEngine.model.get.sftp.order.notification.*;
import apiEngine.model.get.sftp.wallbox.order.notification.*;
import cucumber.TestContext;
import helpers.AssertHelper;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageobjects.ui.ORD_Order_Page;

//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class FTP_GetWallBoxOrderNotifications {
	final Logger logger = LogManager.getLogger(FTP_GetWallBoxOrderNotifications.class);

	TestContext testContext;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	ORD_Order_Page orderPage;
	private AssertHelper ah = new AssertHelper();

	String fileName;
	boolean success = false;
	InputStream inputStream;
	OutputStream out1 = null;

	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateToday = simpleDateFormat.format(new Date());
	String localFile = "./src/test/resources/runtime/sftpdownloads/order_update.xml";
	String localFileWB = "./src/test/resources/runtime/sftpdownloads/wallbox_order_update.xml";




		String comnotTransactionCode;
		String comnotNotificationDate;
		String comnotComments;
		String comnotDossierNumber;
		String deanotTransactionCode;
		String deanotNotificationDate;
		String deanotComments;
		String deanotDossierNumber;
		String deanotExpectedInstallationDate;
		String ordnotTransactionCode;
		String ordnotNotificationDate;
		String ordnotDossierNumber;
		String ordnotComments;
		String vinnotTransactionCode;
		String vinnotNotificationDate;
		String vinnotDossierNumber;
		String vinnotVinNumber;
		String vinnotVinControlNumber;
		String vinnotWeight;
		String vinnotCylinderContent;
		String vinnotPower;
		String vinnotCo2Emissions;
		String vinnotComments;
		String vinnotDateFirstRegistration;
		String vinnotDateLastRegistration;
		String vinnotFuel;
		String vinnotLicensePlateNumber;
		String licnotTransactionCode;
		String licnotNotificationDate;
		String licnotDossierNumber;
		String licnotLicensePlateNumber;
		String licnotCo2Emissions;
		String licnotDateFirstRegistration;
		String delnotTransactionCode;
		String delnotNotificationDate;
		String delnotDossierNumber;
		String delnotCo2Emissions;
		String delnotDateFirstRegistration;
		Integer delnotMileage;
		Integer delnotConstructionYear;
		String delnotKeyCode;
		String delnotStartCode;
		Boolean delnotPreviousVehicleDropoff;
		String delnotPreviousVehicleLicensePlate;
		String delnotPreviousVehicleOwner;
		String delnotActualDeliveryDate;
		String delnotDriverFirstName;
		String delnotDriverLastName;
		String delnotDriverBirthDate;
		String delnotDriverBirthPlace;
		Boolean delnotRegistrationCertificate;
		Boolean delnotInsuranceCertificate;
		Boolean delnotCertificateOfConformity;
		Boolean delnotTechnicalInspectionCertificate;
		Boolean delnotLegalKit;
		Boolean delnotServiceManual;
		Boolean delnotFuelCard;
		String delnotNumberOfKeys;
		String delnotTyreSpecification;
		String delnotTyreBrand;
		String delnotTyreType;
		String delnotExteriorColor;
		String delnotInteriorColor;


	public FTP_GetWallBoxOrderNotifications(WallBoxOrderNotifications wallBoxOrderNotifications) {
		
	}
	
	public void get_wallbox_order_notifications_data(WallBoxOrderNotifications wallBoxOrderNotifications) throws ParseException, NullPointerException, JAXBException {
		
		JAXBContext jaxbContext;

		jaxbContext = JAXBContext.newInstance(WallBoxOrderNotifications.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		// OrderNotifications orderNotifications = (OrderNotifications)
		// jaxbUnmarshaller.unmarshal(new
		// File("./src/test/resources/runtime/sftpdownloads/order_update_nn.xml"));
		logger.debug("localFileWB: " + localFileWB);
		wallBoxOrderNotifications = (WallBoxOrderNotifications) jaxbUnmarshaller
				.unmarshal(new File(localFileWB));
		logger.debug("wallBoxOrderNotifications: " + wallBoxOrderNotifications);
		for (int i=0;i < wallBoxOrderNotifications.getOrderNotification().size(); i++) {
		CommentsNotification comnot = wallBoxOrderNotifications.getOrderNotification().get(i).getCommentsNotification();
		DealerAcceptedNotification deanot = wallBoxOrderNotifications.getOrderNotification().get(i).getDealerAcceptedNotification();
		NotAcceptedNotification nannot = wallBoxOrderNotifications.getOrderNotification().get(i).getNotAcceptedNotification();
		OrderedNotification ordnot = wallBoxOrderNotifications.getOrderNotification().get(i).getOrderedNotification();
		ReadyToActivateNotification rtanot = wallBoxOrderNotifications.getOrderNotification().get(i).getReadyToActivateNotification();
		ActivatedNotification actnot = wallBoxOrderNotifications.getOrderNotification().get(i).getActivatedNotification();
		InstalledNotification insnot = wallBoxOrderNotifications.getOrderNotification().get(i).getInstalledNotification();
		CancelledNotification cannot = wallBoxOrderNotifications.getOrderNotification().get(i).getCancelledNotification();
		if (comnot != null) {
			//String tc = orderNotifications.getOrderNotification().get(0).getCommentsNotification().getTransactionCode();
			comnotTransactionCode = comnot.getTransactionCode().value();
			//ah.assertEquals("TransactionCode ", comnotTransactionCode, "COMMENTS");
			//logger.debug("TransactionCode: " + comnotTransactionCode);
			comnotNotificationDate = comnot.getNotificationDate();
			//ah.assertEquals("NotificationDate ", comnotNotificationDate, dateToday);
			//logger.debug("NotificationDate: " + comnotNotificationDate);
			comnotComments = comnot.getComments();
			//logger.debug("Comments: " + comnotComments);
			comnotDossierNumber = comnot.getDossierNumber();
			//ah.assertEquals("DossierNumber ", comnotDossierNumber, postOrderImportsRequest.getDosNr());
			//logger.debug("DossierNumber: " + comnotDossierNumber);
		} else if (deanot != null) {
			deanotTransactionCode = deanot.getTransactionCode().value();
			deanotNotificationDate = deanot.getNotificationDate();
			deanotDossierNumber = deanot.getDossierNumber();
			deanotExpectedInstallationDate = deanot.getExpectedInstallationDate();
		} else if (ordnot != null) {
			ordnotTransactionCode = ordnot.getTransactionCode().value();
			//logger.debug("TransactionCode: " + ordnotTransactionCode);
			ordnotNotificationDate = ordnot.getNotificationDate();
			//logger.debug("NotificationDate: " + ordnotNotificationDate);
			ordnotDossierNumber = ordnot.getDossierNumber();
			//logger.debug("DossierNumber: " + ordnotDossierNumber);
			//ordnotComments = ordnot.getComments();
			//logger.debug("Comments: " + ordnotComments);
		}
			
		}
	}

	public String getComnotTransactionCode() {
		return comnotTransactionCode.toString();
	}

	public String getComnotNotificationDate() {
		return comnotNotificationDate;
	}

	public String getComnotComments() {
		return comnotComments;
	}

	public String getComnotDossierNumber() {
		return comnotDossierNumber;
	}

	public String getDeanotTransactionCode() {
		return deanotTransactionCode;
	}

	public String getDeanotNotificationDate() {
		return deanotNotificationDate;
	}

	public String getDeanotComments() {
		return deanotComments;
	}

	public String getDeanotDossierNumber() {
		return deanotDossierNumber;
	}

	public String getOrdnotTransactionCode() {
		return ordnotTransactionCode;
	}

	public String getOrdnotNotificationDate() {
		return ordnotNotificationDate;
	}

	public String getOrdnotDossierNumber() {
		return ordnotDossierNumber;
	}

	public String getOrdnotComments() {
		return ordnotComments;
	}

	public String getVinnotTransactionCode() {
		return vinnotTransactionCode;
	}

	public String getVinnotNotificationDate() {
		return vinnotNotificationDate;
	}

	public String getVinnotDossierNumber() {
		return vinnotDossierNumber;
	}

	public String getVinnotVinNumber() {
		return vinnotVinNumber;
	}

	public String getVinnotVinControlNumber() {
		return vinnotVinControlNumber;
	}

	public String getVinnotWeight() {
		return vinnotWeight;
	}

	public String getVinnotCylinderContent() {
		return vinnotCylinderContent;
	}

	public String getVinnotPower() {
		return vinnotPower;
	}

	public String getVinnotCo2Emissions() {
		return vinnotCo2Emissions;
	}

	public String getVinnotComments() {
		return vinnotComments;
	}

	public String getVinnotDateFirstRegistration() {
		return vinnotDateFirstRegistration;
	}

	public String getVinnotDateLastRegistration() {
		return vinnotDateLastRegistration;
	}

	public String getVinnotFuel() {
		return vinnotFuel;
	}

	public String getVinnotLicensePlateNumber() {
		return vinnotLicensePlateNumber;
	}

	public String getLicnotTransactionCode() {
		return licnotTransactionCode;
	}

	public String getLicnotNotificationDate() {
		return licnotNotificationDate;
	}

	public String getLicnotDossierNumber() {
		return licnotDossierNumber;
	}

	public String getLicnotLicensePlateNumber() {
		return licnotLicensePlateNumber;
	}

	public String getLicnotCo2Emissions() {
		return licnotCo2Emissions;
	}

	public String getLicnotDateFirstRegistration() {
		return licnotDateFirstRegistration;
	}

	public String getDelnotTransactionCode() {
		return delnotTransactionCode;
	}

	public String getDelnotNotificationDate() {
		return delnotNotificationDate;
	}

	public String getDelnotDossierNumber() {
		return delnotDossierNumber;
	}

	public String getDelnotCo2Emissions() {
		return delnotCo2Emissions;
	}

	public String getDelnotDateFirstRegistration() {
		return delnotDateFirstRegistration;
	}

	public Integer getDelnotMileage() {
		return delnotMileage;
	}

	public Integer getDelnotConstructionYear() {
		return delnotConstructionYear;
	}

	public String getDelnotKeyCode() {
		return delnotKeyCode;
	}

	public String getDelnotStartCode() {
		return delnotStartCode;
	}

	public Boolean getDelnotPreviousVehicleDropoff() {
		return delnotPreviousVehicleDropoff;
	}

	public String getDelnotPreviousVehicleLicensePlate() {
		return delnotPreviousVehicleLicensePlate;
	}

	public String getDelnotPreviousVehicleOwner() {
		return delnotPreviousVehicleOwner;
	}

	public String getDelnotActualDeliveryDate() {
		return delnotActualDeliveryDate;
	}

	public String getDelnotDriverFirstName() {
		return delnotDriverFirstName;
	}

	public String getDelnotDriverLastName() {
		return delnotDriverLastName;
	}

	public String getDelnotDriverBirthDate() {
		return delnotDriverBirthDate;
	}

	public String getDelnotDriverBirthPlace() {
		return delnotDriverBirthPlace;
	}

	public Boolean getDelnotRegistrationCertificate() {
		return delnotRegistrationCertificate;
	}

	public Boolean getDelnotInsuranceCertificate() {
		return delnotInsuranceCertificate;
	}

	public Boolean getDelnotCertificateOfConformity() {
		return delnotCertificateOfConformity;
	}

	public Boolean getDelnotTechnicalInspectionCertificate() {
		return delnotTechnicalInspectionCertificate;
	}

	public Boolean getDelnotLegalKit() {
		return delnotLegalKit;
	}

	public Boolean getDelnotServiceManual() {
		return delnotServiceManual;
	}

	public Boolean getDelnotFuelCard() {
		return delnotFuelCard;
	}

	public String getDelnotNumberOfKeys() {
		return delnotNumberOfKeys;
	}

	public String getDelnotTyreSpecification() {
		return delnotTyreSpecification;
	}

	public String getDelnotTyreBrand() {
		return delnotTyreBrand;
	}

	public String getDelnotTyreType() {
		return delnotTyreType;
	}

	public String getDelnotExteriorColor() {
		return delnotExteriorColor;
	}

	public String getDelnotInteriorColor() {
		return delnotInteriorColor;
	}

	
	
	
}
		
		