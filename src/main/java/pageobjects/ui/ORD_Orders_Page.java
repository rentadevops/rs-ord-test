package pageobjects.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import apiEngine.Endpoints;
import cucumber.TestContext;


public class ORD_Orders_Page {
	final Logger logger = LogManager.getLogger(ORD_Orders_Page.class);
	
	ORD_Topbar topBar;
	TestContext testContext;
	
	public String dosNr;
	public WebElement si;
	public WebElement orderFF;
	By order;
	
	WebDriver driver;

	public ORD_Orders_Page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='globalFilter']")
	private WebElement input_search_field;

	@FindBy(how = How.XPATH, using = "//input[@data-cy='searchTerm']")
	private WebElement input_search_field_wb;
	
	@FindBy(how = How.XPATH, using = "//mat-icon[contains(text(), 'filter_list')]")
	private WebElement btn_filter;
	
	@FindBy(how = How.XPATH, using = "//section//mat-icon[contains(text(), 'close')]")
	private WebElement btn_close_filter;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td/span")
	private WebElement first_result;

	@FindBy(how = How.XPATH, using = "//a[@href='rs-number']")
	private WebElement user_ui;

	@FindBy(how = How.XPATH, using = "//a[@href='ev-products']")
	private WebElement ev_products;

	@FindBy(how = How.XPATH, using = "//a[@data-menuitem='active-orders-overview']")
	private WebElement orders;
	
	public void search_order() throws InterruptedException, IOException, URISyntaxException {
		logger.debug("Start search_order");
		Thread.sleep(2000);

		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/properties.properties");
		properties.load(inputDriver);
		String user = properties.getProperty("leasing_company");
		logger.debug("dosNr " + dosNr +  " van proprty file gelezen");
		user_ui.getText().contains(user);
	if(System.getProperty("leasingType").equals("WALLBOX") && !user_ui.getText().contains(user)) {
		logger.debug("1e if wallbox");
		orders.click();
		ev_products.click();
		si = input_search_field_wb;
	} else if(System.getProperty("leasingType").equals("WALLBOX") && user_ui.getText().contains(user)) {
		logger.debug("2e if wallbox");
		orders.click();
		ev_products.click();
		si = input_search_field_wb;
	}
	else {
		logger.debug("geen wallbox, else-tak");
		si = input_search_field;
	}
		
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		//Properties properties = new Properties();

		if(System.getProperty("leasingType").equals("WALLBOX")) {
			inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumberWB.properties");
		} else {
			inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		}

		properties.load(inputDriver);
		dosNr = properties.getProperty("DossierNumber");
		logger.debug("dosNrWB " + dosNr +  " van proprty file gelezen");

		if(System.getProperty("leasingType").equals("WALLBOX")) {
			order = By.xpath("//div[contains(.,'Leasing company')]/parent::div//div[contains(., '" + dosNr + "')]");
		} else {
			order = By.xpath("//span[contains(., '" + dosNr + "')]");
		}

		
		Integer seconden = 600;
		boolean succes = false;
		long endWaitTime = System.currentTimeMillis() + seconden*1000;

		while (System.currentTimeMillis() < endWaitTime && !succes) {
			wait.until(ExpectedConditions.elementToBeClickable(si));
			try {
				si.click();
				si.clear();
				si.sendKeys(dosNr);
				Thread.sleep(2000);
			} catch(Exception e) {
				logger.debug("Kan geen zoekterm opgeven");
			}
			try {

			if(System.getProperty("leasingType").equals("WALLBOX")) {
				orderFF = driver.findElement(By.xpath("//div[contains(.,'Leasing company')]/parent::div//div[contains(., '" + dosNr + "')]"));
			} else {
				orderFF = driver.findElement(By.xpath("//span[contains(., '" + dosNr + "')]"));
			}

			if(driver.findElement(order).isDisplayed()) {
				logger.debug("order gevonden???");
				if(System.getProperty("BrowserName").equals("firefox")) {
					Actions actions = new Actions(driver);
					actions.moveToElement(orderFF);
					actions.click(orderFF);
					Action action = actions.build();
					action.perform();
				} else {
					driver.findElement(order).click();
				}
				succes = true;
				break;
			}
			else {
				logger.debug("nog niet gevonden");

				Thread.sleep(5000);
			}
			} catch(Exception e) {
				logger.debug("Nog niets");

			}
		}
		wait = new WebDriverWait(driver, Duration.ofMillis(20000));

			wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
		logger.debug("End search_order");
	}
	
	public void open_order() throws InterruptedException, IOException {
		Thread.sleep(2000);
		
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		Properties properties = new Properties();

		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		//properties.load(ORD_Orders_Page.class.getClassLoader().getResourceAsStream("dossiernumber.properties"));
		dosNr = properties.getProperty("DossierNumber");
		logger.debug("dosNr " + dosNr +  " van proprty file gelezen");

		if(System.getProperty("leasingType").equals("WALLBOX")) {
			By order = By.xpath("//div[contains(.,'Leasing company')]/parent::div//div[contains(., '" + dosNr + "')]");
			orderFF = driver.findElement(By.xpath("//div[contains(.,'Leasing company')]/parent::div//div[contains(., '" + dosNr + "')]"));
		} else {
			By order = By.xpath("//span[contains(., '" + dosNr + "')]");
			orderFF = driver.findElement(By.xpath("//span[contains(., '" + dosNr + "')]"));
		}

		if(System.getProperty("BrowserName").equals("firefox")) {
			Actions actions = new Actions(driver);
			actions.moveToElement(orderFF);
			actions.click(orderFF);
			Action action = actions.build();
			action.perform();
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();", order);
		} else {
			WebElement el = driver.findElement(order);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", el);
			//driver.findElement(order).click();
		}
		Thread.sleep(5000);
	}
	
	
}