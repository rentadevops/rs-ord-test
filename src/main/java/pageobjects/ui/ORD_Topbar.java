package pageobjects.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataproviders.PropertyFileReader;


public class ORD_Topbar {
	final Logger logger = LogManager.getLogger(ORD_Topbar.class);

	WebDriver driver;
	PropertyFileReader propertyFileReader = new PropertyFileReader();
	JavascriptExecutor jse = (JavascriptExecutor)driver;

	String language;
	String portal_url;

	public ORD_Topbar(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}

	@FindBy(how = How.XPATH, using = "//a[@id='OrdersNav']")
	private WebElement orders;

	@FindBy(how = How.XPATH, using = "//a[@id='RegistrationNav']")
	private WebElement registration;

	@FindBy(how = How.XPATH, using = "//a[@id='/AdministrationNav']")
	private WebElement administration;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@title,'Switch user')]")
	private WebElement switch_user;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'utilisateur')]")
	private WebElement switch_user_fr;
	
	@FindBy(how = How.XPATH, using = "//label[contains(@id, 'mat-form-field')]")
	private WebElement btn_select_membership;

	@FindBy(how = How.XPATH, using = "//mat-select[contains(@id, 'mat-select')]/div")
	private WebElement btn_select_membership_dealer;

	

	@FindBy(how = How.XPATH, using = "//button[@data-testid='footer-lang-switcher-btn']")
	private WebElement footer_actual_language;

	@FindBy(how = How.XPATH, using = "(//button[contains(@class, 'btn-lang-switcher')])[1]")
	private WebElement btn_language;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'English')]")
	private WebElement btn_english;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Nederlands')]")
	private WebElement btn_nederlands;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Français')]")
	private WebElement btn_francais;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Deutsch')]")
	private WebElement btn_deutsch;
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement account;
	
	public WebElement account(String user) {
	    account = driver.findElement(By.xpath("//span[contains(text(), '"+user+"')]"));
		return account;
	}

	public void click_administration() throws FileNotFoundException, IOException, InterruptedException {
		Thread.sleep(2000);
		selectLanguage();
		portal_url = propertyFileReader.getMrtUrl();
		driver.get(portal_url+"administration/overview");
		Thread.sleep(2000);

	}

	public void click_registration() throws InterruptedException, FileNotFoundException, IOException {
		Thread.sleep(2000);
		selectLanguage();
		//portal_url = propertyFileReader.getMrtUrl();
		//driver.get(portal_url+"leasing-contracts?page=1&resultsPerPage=10&userLanguage=en&sortBy=LICENSE_PLATE&sortDirection=ASC");
		registration.click();
		Thread.sleep(2000);
		
	}
	
	public void click_switch_user_to_dealer() throws FileNotFoundException, IOException, InterruptedException {
		logger.debug("Start switch to dealer");
		logger.debug("appLanguage: " + System.getProperty("appLanguage"));
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		Thread.sleep(2000);
		selectLanguage();
		if(System.getProperty("language").equals("fr")) {
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", switch_user_fr);
			Thread.sleep(5000);
			//wait.until(ExpectedConditions.elementToBeClickable(switch_user));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", switch_user_fr);
		}
		else {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", switch_user);
			Thread.sleep(5000);
			//wait.until(ExpectedConditions.elementToBeClickable(switch_user));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", switch_user);
			//switch_user.click();
		}
		Thread.sleep(4000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_select_membership_dealer);
		//btn_select_membership_dealer.click();
		Thread.sleep(500);
		//account(propertyFileReader.getDealer()).click();
		try {
			if(System.getProperty("leasingType").equals("WALLBOX")) {
				account(propertyFileReader.getDealer_wb()).click();
			} else {
				account(propertyFileReader.getDealer()).click();
			}

		} catch(NoSuchElementException e) {
			logger.debug("Account not found");
		}
		Thread.sleep(5000);
		logger.debug("Nu zou ik dealer moeten zijn.");
		logger.debug("End switch to dealer");
	}
	
	public void click_switch_user_to_leasing_company() throws FileNotFoundException, IOException, InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		WebElement resultsDiv;
		Thread.sleep(5000);
		selectLanguage();

		
		long end = System.currentTimeMillis() + 10000;
		while (System.currentTimeMillis() < end) {
		    // Browsers which render content (such as Firefox and IE) return "RenderedWebElements"
		    if(System.getProperty("language").equals("fr")) {
				resultsDiv = driver.findElement(By.xpath("//a[contains(@title,'utilisateur')]"));
			} else {
				resultsDiv = driver.findElement(By.xpath("//a[contains(@title,'Switch user')]"));
			}


		    // If results have been returned, the results are displayed in a drop down.
		    if (resultsDiv.isDisplayed()) {
		      break;
		    }
		}
		
		
		if(System.getProperty("language").equals("fr")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", switch_user_fr);
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(switch_user_fr));
			Thread.sleep(500);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", switch_user_fr);
			//switch_user.click();
			Thread.sleep(3000);
			btn_select_membership.click();
			if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
				try {
					account(propertyFileReader.getLeasingCompanyLux()).click();
				} catch (NoSuchElementException e) {
					logger.debug("Account not found");
				}
			} else {
				try {
					account(propertyFileReader.getLeasingCompany()).click();
				} catch (NoSuchElementException e) {
					logger.debug("Account not found");
				}
			}
			Thread.sleep(5000);
		} else {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", switch_user);
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(switch_user));
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", switch_user);
			//switch_user.click();
			Thread.sleep(5000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_select_membership);
			//btn_select_membership.click();
			if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
				try {
					account(propertyFileReader.getLeasingCompanyLux()).click();
				} catch (NoSuchElementException e) {
					logger.debug("Account not found");
				}
			} else if(System.getProperty("wallbox").equals("Yes")) {
				try {
					account(propertyFileReader.getLeasingCompanyWb()).click();
				} catch (NoSuchElementException e) {
					logger.debug("Account not found");
				}
			}
			else {
				try {
					account(propertyFileReader.getLeasingCompany()).click();
				} catch (NoSuchElementException e) {
					logger.debug("Account not found");
				}
			}
			Thread.sleep(5000);
		}
	}

	public String getLanguage(String language) {
		switch (language) {
		case "nl":
			language = "Nederlands";
			break;
		case "en":
			language = "English";
			break;
		case "fr":
			language = "Français";
			break;
		case "de":
			language = "Deutsch";
			break;
		}
		return language;
	}

	public void selectLanguage() throws FileNotFoundException, IOException, InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		String[] parts = footer_actual_language.getText().split(" ");
		String actualLanguage = parts[0].trim();
		logger.debug("actualLanguage in Topbar selectLanguage: " + actualLanguage);
		logger.debug("systemproperty language: " + System.getProperty("language"));
		if(System.getProperty("language") != null && !System.getProperty("language").isEmpty()) {
			language = getLanguage(System.getProperty("language"));
			logger.debug("language: " + language);
			logger.debug("actualLanguage in topbar getLanguage: " + actualLanguage);
			if(!language.equalsIgnoreCase(actualLanguage)) {
				logger.debug("language != actualLanguage");

				if (language.equals("Nederlands")) {
					Thread.sleep(2000);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_language);
					btn_language.click();
					btn_nederlands.click();
				}
				if (language.equals("English")) {
					Thread.sleep(2000);
					wait.until(ExpectedConditions.elementToBeClickable(btn_language));
					jse.executeScript("arguments[0].click();", btn_language);
					//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_language);
					//btn_language.click();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.elementToBeClickable(btn_english));
					//btn_english.click();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_english);
					logger.debug("button_english geklikt");
				}
				if (language.equals("Français")) {
					logger.debug("Zet language op francais");
					Thread.sleep(2000);
					wait.until(ExpectedConditions.elementToBeClickable(btn_language));
					jse.executeScript("arguments[0].click();", btn_language);
					//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_language);
					//btn_language.click();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.elementToBeClickable(btn_francais));
					//btn_english.click();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_francais);
					logger.debug("btn_francais geklikt");
					/*Thread.sleep(2000);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_language);
					btn_language.click();
					btn_francais.click();*/
				}
				if (language.equals("Deutsch")) {
					Thread.sleep(2000);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_language);
					btn_language.click();
					btn_deutsch.click();
				}
			}
		} else {
			language = propertyFileReader.getProperty("language");
			logger.debug("language from properties file: " + language);
			if(!getLanguage(language).equalsIgnoreCase(actualLanguage)) {

				if (language.equals("nl")) {
					logger.debug("set language nl");
					System.setProperty("language", "nl");
					Thread.sleep(2000);
					btn_language.click();
					btn_nederlands.click();
				}
				if (language.equals("en")) {
					logger.debug("set language en");
					System.setProperty("language", "en");
					Thread.sleep(2000);
					wait.until(ExpectedConditions.elementToBeClickable(btn_language));
					jse.executeScript("arguments[0].click();", btn_language);
					Thread.sleep(2000);
					wait.until(ExpectedConditions.elementToBeClickable(btn_english));
					//btn_english.click();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_english);
				}
				if (language.equals("fr")) {
					logger.debug("set language fr");
					System.setProperty("language", "fr");
					Thread.sleep(2000);
					btn_language.click();
					btn_francais.click();
				}
				if (language.equals("de")) {
					logger.debug("set language de");
					System.setProperty("language", "de");
					Thread.sleep(2000);
					btn_language.click();
					btn_deutsch.click();
				}
			 
			}
		}
	
	}
	
	
}
