package pageobjects.ui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ORD_Wallbox_Order_Details_Page {
	final Logger logger = LogManager.getLogger(ORD_Wallbox_Order_Details_Page.class);

	WebDriver driver;



	private String orderStatus;
	private String leasingCompanyDossierNumber;

	private String leasingCompanyName;
	private String leasingCompanyEnterpriseNumber;
	private String leasingCompanyRsNumber;
	private String leasingCompanybillingAddress;
	private String leasingCompanyCountry;
	private String leasingCompanyContactPerson;
	private String leasingCompanyContactEmail;
	private String leasingCompanyContactMobileNumber;
	private String leasingCompanyContactPhoneNumber;
	private String leasingCompanyContactLanguage;

	private String supplierNameTitle;
	private String supplierName;
	private String supplierEnterpriseNumber;
	private String supplierRsNumber;
	private String supplierbillingAddress;
	private String supplierCountry;
	private String supplierContactPerson;
	private String supplierContactEmail;
	private String supplierContactMobileNumber;
	private String supplierContactPhoneNumber;
	private String supplierContactLanguage;

	private String lesseeNameTitle;
	private String lesseeEnterpriseNumber;
	private String lesseeBillingAddress;
	private String lesseeContactPerson;
	private String lesseeContactEmail;
	private String lesseeContactMobileNumber;
	private String lesseeContactPhoneNumber;
	private String lesseeContactLanguage;

	private String productBrand;
	private String productModel;
	private String productDescription;
	private String productEvNumber;
	private String productCardNumber;
	private String productCardSent;
	private String vehicleBrand;
	private String vehicleModel;


	private String deliveryDesiredInstallationDate;
	private String deliveryExpectedInstallationDate;
	private String deliveryInstallationDate;
	private String deliveryCertificationDate;
	private String deliveryActivationDate;
	private String deliveryAddress;

	private String pricingDescription;
	private String pricingCrossAmount;
	private String pricingDiscount;
	private String pricingTotalNetAmount;

	private String driverContactPerson;
	private String driverContactEmail;
	private String driverContactMobileNumber;
	private String driverContactPhoneNumber;
	private String driverContactLanguage;


	List<WebElement> elements;

	public ORD_Wallbox_Order_Details_Page(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		//get_leasing_company_contract_data();
		
	}
	
//region General
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-order-status-div']")
	private WebElement we_order_status;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-lc-ordernumber-div']")
	private WebElement we_leasingcompany_dossiernumber;

//endregion General

//region Leasing company
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-name-registered-name-div']")
	private WebElement we_leasingcompany_name;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-entreprise-number-div']")
	private WebElement we_leasingcompany_enterprisenumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-lc-rs-number-div']")
	private WebElement we_leasingcompany_rsnumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-lc-billing-address-div']")
	private WebElement we_leasingcompany_billingaddress;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-lc-country-div']")
	private WebElement we_leasingcompany_country;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lc-name-div']")
	private WebElement we_leasingcompany_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lc-email-div']")
	private WebElement we_leasingcompany_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lc-mobile-number-div']")
	private WebElement we_leasingcompany_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lc-phone-number-div']")
	private WebElement we_leasingcompany_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lc-language-div']")
	private WebElement we_leasingcompany_contact_language;
	
	
//endregion Leasing company

//region Supplier

	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Supplier']/h2")
	private WebElement we_supplier_name_title;	
	
	@FindBy(how = How.XPATH, using = "(//div[@data-testid='details-name-registered-name-div'])[2]")
	private WebElement we_supplier_name;	
	
	@FindBy(how = How.XPATH, using = "(//div[@data-testid='details-entreprise-number-div'])[2]")
	private WebElement we_supplier_enterprisenumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-supplier-rs-number-div']")
	private WebElement we_supplier_rsnumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-supplier-billing-address-div']")
	private WebElement we_supplier_billingaddress;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-supplier-country-div']")
	private WebElement we_supplier_country;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-supplier-name-div']")
	private WebElement we_supplier_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-supplier-email-div']")
	private WebElement we_supplier_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-supplier-mobile-number-div']")
	private WebElement we_supplier_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-supplier-phone-number-div']")
	private WebElement we_supplier_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-supplier-language-div']")
	private WebElement we_supplier_contact_language;

//endregion Supplier

//region Lessee
	
	
	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Lessee']/h2")
	private WebElement we_lessee_name_title;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-lessee-entreprise-number-div']")
	private WebElement we_lessee_enterprisenumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-lessee-billing-address-div']")
	private WebElement we_lessee_billingaddress;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lessee-name-div']")
	private WebElement we_lessee_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lessee-email-div']")
	private WebElement we_lessee_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lessee-mobile-number-div']")
	private WebElement we_lessee_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lessee-phone-number-div']")
	private WebElement we_lessee_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-lessee-language-div']")
	private WebElement we_lessee_contact_language;
	
	
//endregion Lessee

//region Product

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-product-brand-div']")
	private WebElement we_product_brand;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-product-model-div']")
	private WebElement we_product_model;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-product-description-div']")
	private WebElement we_product_description;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-product-evb-number-div']")
	private WebElement we_product_ev_number;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-product-card-number-div']")
	private WebElement we_product_card_number;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-product-card-sent-div']")
	private WebElement we_product_card_sent;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-vehicle-brand-div']")
	private WebElement we_vehicle_brand;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-vehicle-model-div']")
	private WebElement we_vehicle_model;

//endregion Product

//region Driver and Delivery
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-driver-and-delivery-contact-info-desired-installation-date-div']")
	private WebElement we_delivery_desired_installation_date;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-driver-and-delivery-contact-info-expected-installation-date-div']")
	private WebElement we_delivery_expected_installation_date;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-driver-and-delivery-contact-info--installation-date-div']")
	private WebElement we_delivery_installation_date;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-driver-and-delivery-contact-info-certification-date-div']")
	private WebElement we_delivery_certification_date;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-driver-and-delivery-contact-info-activation-date-div']")
	private WebElement we_delivery_activation_date;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-driver-and-delivery-contact-info-delivery-address-div']")
	private WebElement we_delivery_address;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-driver-and-delivery-name-div']")
	private WebElement we_driver_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-driver-and-delivery-email-div']")
	private WebElement we_driver_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-driver-and-delivery-mobile-number-div']")
	private WebElement we_driver_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-driver-and-delivery-phone-number-div']")
	private WebElement we_driver_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='contact-info-driver-and-delivery-language-div']")
	private WebElement we_driver_contact_language;
	
	
//endregion Driver and Delivery


	
//region Pricing
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-pricing-description-div']")
	private WebElement we_pricing_description;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-pricing-gross-amount-div']")
	private WebElement we_pricing_cross_amount;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-pricing-discount-div']")
	private WebElement we_pricing_discount;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='details-pricing-total-net-amount-div']")
	private WebElement we_pricing_total_net_amount;
	

//endregion Pricing

	public void get_wallbox_order_details_data() throws InterruptedException, ParseException {
		
	
		Thread.sleep(5000);
		
		//General
		orderStatus = we_order_status.getText().trim();
		leasingCompanyDossierNumber = we_leasingcompany_dossiernumber.getText().trim();

		logger.debug("orderStatus: "+orderStatus+" leasingCompanyDossierNumber: "+leasingCompanyDossierNumber);
		
		//Leasing company
		leasingCompanyName = we_leasingcompany_name.getText().trim();
		leasingCompanyEnterpriseNumber = we_leasingcompany_enterprisenumber.getText().trim();
		leasingCompanyRsNumber = we_leasingcompany_rsnumber.getText().trim();
		leasingCompanybillingAddress = we_leasingcompany_billingaddress.getText().trim();
		leasingCompanyCountry = we_leasingcompany_country.getText().trim();
		leasingCompanyContactPerson = we_leasingcompany_contact_person.getText().trim();
		leasingCompanyContactEmail = we_leasingcompany_contact_email.getText().trim();
		leasingCompanyContactMobileNumber = we_leasingcompany_contact_mobilenumber.getText().replace("smartphone", "").trim();
		leasingCompanyContactPhoneNumber = we_leasingcompany_contact_phonenumber.getText().replace("phone", "").trim();
		leasingCompanyContactLanguage = we_leasingcompany_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
		logger.debug("leasingCompanyName: "+leasingCompanyName+" leasingCompanyEnterpriseNumber: "+leasingCompanyEnterpriseNumber+
				" leasingCompanyRsNumber: "+leasingCompanyRsNumber+" leasingCompanybillingAddress: "+leasingCompanybillingAddress+
				" leasingCompanyCountry: "+leasingCompanyCountry+" leasingCompanyContactPerson: "+leasingCompanyContactPerson+
				" leasingCompanyContactEmail: "+leasingCompanyContactEmail+" leasingCompanyContactMobileNumber:"+leasingCompanyContactMobileNumber+
				" leasingCompanyContactPhoneNumber: "+leasingCompanyContactPhoneNumber+" leasingCompanyContactLanguage: "+leasingCompanyContactLanguage);

		//Supplier
		supplierNameTitle = we_supplier_name_title.getText().trim();
		supplierName = we_supplier_name.getText().trim();
		supplierEnterpriseNumber = we_supplier_enterprisenumber.getText().trim();
		supplierRsNumber = we_supplier_rsnumber.getText().trim();
		supplierbillingAddress = we_supplier_billingaddress.getText().trim();
		supplierCountry = we_supplier_country.getText().trim();
		supplierContactPerson = we_supplier_contact_person.getText().trim();
		supplierContactEmail = we_supplier_contact_email.getText().replace("email", "").trim();
		supplierContactMobileNumber = we_supplier_contact_mobilenumber.getText().replace("smartphone", "").trim();
		supplierContactPhoneNumber = we_supplier_contact_phonenumber.getText().replace("phone", "").trim();
		supplierContactLanguage = we_supplier_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
		logger.debug("supplierNameTitle: "+supplierNameTitle+ " supplierName: "+supplierName+" supplierEnterpriseNumber: "+supplierEnterpriseNumber+
				" supplierRsNumber: "+supplierRsNumber+" supplierbillingAddress: "+supplierbillingAddress+
				" supplierCountry: "+supplierCountry+" supplierContactPerson: "+supplierContactPerson+
				" supplierContactEmail: "+supplierContactEmail+" supplierContactMobileNumber:"+supplierContactMobileNumber+
				" supplierContactPhoneNumber: "+supplierContactPhoneNumber+" supplierContactLanguage: "+supplierContactLanguage);

		
		//Lessee
		lesseeNameTitle = we_lessee_name_title.getText().trim();
		lesseeEnterpriseNumber = we_lessee_enterprisenumber.getText().trim();
		lesseeBillingAddress = we_lessee_billingaddress.getText().trim();
		lesseeContactPerson = we_lessee_contact_person.getText().trim();
		lesseeContactEmail = we_lessee_contact_email.getText().replace("email", "").trim();
		lesseeContactMobileNumber = we_lessee_contact_mobilenumber.getText().replace("smartphone", "").trim();
		lesseeContactPhoneNumber = we_lessee_contact_phonenumber.getText().replace("phone", "").trim();
		lesseeContactLanguage = we_lessee_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
		logger.debug("lesseeEnterpriseNumber: "+lesseeEnterpriseNumber+" lesseeBillingAddress: "+lesseeBillingAddress+
				" lesseeContactPerson: "+lesseeContactPerson+" lesseeContactEmail: "+lesseeContactEmail+
				" lesseeContactMobileNumber: "+lesseeContactMobileNumber+" lesseeContactPhoneNumber: "+lesseeContactPhoneNumber+
				" lesseeContactLanguage: "+lesseeContactLanguage);

		//Product
		productBrand = we_product_brand.getText().trim();
		productModel = we_product_model.getText().trim();
		productDescription = we_product_description.getText().trim();
		productEvNumber = we_product_ev_number.getText().trim();
		productCardNumber = we_product_card_number.getText().trim();
		productCardSent = we_product_card_sent.getText().trim();
		vehicleBrand = we_vehicle_brand.getText().trim();
		vehicleModel = we_vehicle_model.getText().trim();
		logger.debug("productBrand: "+productBrand+" productModel: "+productModel+
				" productDescription: "+productDescription+" productEvNumber: "+productEvNumber+
				" productCardNumber: "+productCardNumber+" productCardSent: "+productCardSent+
				" vehicleBrand: "+vehicleBrand+" vehicleModel: "+vehicleModel);
		
		//Driver
		driverContactPerson = we_driver_contact_person.getText().trim();
		driverContactEmail = we_driver_contact_email.getText().trim();
		driverContactMobileNumber = we_driver_contact_mobilenumber.getText().replace("smartphone", "").trim();
		driverContactPhoneNumber = we_driver_contact_phonenumber.getText().replace("phone", "").trim();
		driverContactLanguage = we_driver_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
		logger.debug("driverAddress: "+driverContactPerson+
				" driverContactEmail: "+driverContactEmail+" driverContactMobileNumber: "+driverContactMobileNumber+
				" driverContactPhoneNumber: "+driverContactPhoneNumber+" driverContactLanguage: "+driverContactLanguage);


		//Pricing

		pricingDescription = we_pricing_description.getText().trim();
		pricingCrossAmount = we_pricing_cross_amount.getText().trim();
		pricingDiscount = we_pricing_discount.getText().trim();
		pricingTotalNetAmount = we_pricing_total_net_amount.getText().trim();
		logger.debug("pricingDescription: "+pricingDescription+" pricingCrossAmount: "+pricingCrossAmount+" pricingDiscount: "+pricingDiscount+
				" pricingTotalNetAmount: "+pricingTotalNetAmount);
		
		
		//Delivery information
		deliveryDesiredInstallationDate = we_delivery_desired_installation_date.getText().trim();
		deliveryExpectedInstallationDate = we_delivery_expected_installation_date.getText().trim();
		deliveryInstallationDate = we_delivery_installation_date.getText().trim();
		deliveryCertificationDate = we_delivery_certification_date.getText().trim();
		deliveryActivationDate = we_delivery_activation_date.getText().trim();
		deliveryAddress = we_delivery_address.getText().trim();
		logger.debug("deliveryDesiredInstallationDate: "+deliveryDesiredInstallationDate+" deliveryExpectedInstallationDate: "+deliveryExpectedInstallationDate+
				" deliveryInstallationDate: "+deliveryInstallationDate+" deliveryCertificationDate: "+deliveryCertificationDate
				+" deliveryActivationDate: "+deliveryActivationDate+" deliveryAddress: "+deliveryAddress);
		

	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getLeasingCompanyDossierNumber() {
		return leasingCompanyDossierNumber;
	}

	public void setLeasingCompanyDossierNumber(String leasingCompanyDossierNumber) {
		this.leasingCompanyDossierNumber = leasingCompanyDossierNumber;
	}

	public String getLeasingCompanyName() {
		return leasingCompanyName;
	}

	public void setLeasingCompanyName(String leasingCompanyName) {
		this.leasingCompanyName = leasingCompanyName;
	}

	public String getLeasingCompanyEnterpriseNumber() {
		return leasingCompanyEnterpriseNumber;
	}

	public void setLeasingCompanyEnterpriseNumber(String leasingCompanyEnterpriseNumber) {
		this.leasingCompanyEnterpriseNumber = leasingCompanyEnterpriseNumber;
	}

	public String getLeasingCompanyRsNumber() {
		return leasingCompanyRsNumber;
	}

	public void setLeasingCompanyRsNumber(String leasingCompanyRsNumber) {
		this.leasingCompanyRsNumber = leasingCompanyRsNumber;
	}

	public String getLeasingCompanybillingAddress() {
		return leasingCompanybillingAddress;
	}

	public void setLeasingCompanybillingAddress(String leasingCompanybillingAddress) {
		this.leasingCompanybillingAddress = leasingCompanybillingAddress;
	}

	public String getLeasingCompanyCountry() {
		return leasingCompanyCountry;
	}

	public void setLeasingCompanyCountry(String leasingCompanyCountry) {
		this.leasingCompanyCountry = leasingCompanyCountry;
	}

	public String getLeasingCompanyContactPerson() {
		return leasingCompanyContactPerson;
	}

	public void setLeasingCompanyContactPerson(String leasingCompanyContactPerson) {
		this.leasingCompanyContactPerson = leasingCompanyContactPerson;
	}

	public String getLeasingCompanyContactEmail() {
		if(leasingCompanyContactEmail.contains("―")) {
			return null;
		} else {
			return leasingCompanyContactEmail;
		}
	}

	public void setLeasingCompanyContactEmail(String leasingCompanyContactEmail) {
		this.leasingCompanyContactEmail = leasingCompanyContactEmail;
	}

	public String getLeasingCompanyContactMobileNumber() {
		if(leasingCompanyContactMobileNumber.contains("―")) {
			return null;
		} else {
			return leasingCompanyContactMobileNumber;
		}
	}

	public void setLeasingCompanyContactMobileNumber(String leasingCompanyContactMobileNumber) {
		this.leasingCompanyContactMobileNumber = leasingCompanyContactMobileNumber;
	}

	public String getLeasingCompanyContactPhoneNumber() {
		if(leasingCompanyContactPhoneNumber.contains("―")) {
			return null;
		} else {
			return leasingCompanyContactPhoneNumber;
		}
	}

	public void setLeasingCompanyContactPhoneNumber(String leasingCompanyContactPhoneNumber) {
		this.leasingCompanyContactPhoneNumber = leasingCompanyContactPhoneNumber;
	}

	public String getLeasingCompanyContactLanguage() {
		if(leasingCompanyContactLanguage.contains("―")) {
			return null;
		} else {
			return leasingCompanyContactLanguage;
		}
	}

	public void setLeasingCompanyContactLanguage(String leasingCompanyContactLanguage) {
		this.leasingCompanyContactLanguage = leasingCompanyContactLanguage;
	}

	public String getSupplierNameTitle() {
		return supplierNameTitle;
	}

	public void setSupplierNameTitle(String supplierNameTitle) {
		this.supplierNameTitle = supplierNameTitle;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierEnterpriseNumber() {
		return supplierEnterpriseNumber;
	}

	public void setSupplierEnterpriseNumber(String supplierEnterpriseNumber) {
		this.supplierEnterpriseNumber = supplierEnterpriseNumber;
	}

	public String getSupplierRsNumber() {
		return supplierRsNumber;
	}

	public void setSupplierRsNumber(String supplierRsNumber) {
		this.supplierRsNumber = supplierRsNumber;
	}

	public String getSupplierbillingAddress() {
		return supplierbillingAddress;
	}

	public void setSupplierbillingAddress(String supplierbillingAddress) {
		this.supplierbillingAddress = supplierbillingAddress;
	}

	public String getSupplierCountry() {
		return supplierCountry;
	}

	public void setSupplierCountry(String supplierCountry) {
		this.supplierCountry = supplierCountry;
	}

	public String getSupplierContactPerson() {
		return supplierContactPerson;
	}

	public void setSupplierContactPerson(String supplierContactPerson) {
		this.supplierContactPerson = supplierContactPerson;
	}

	public String getSupplierContactEmail() {
		if(supplierContactEmail.equals("―")) {
			return null;
		} else {
			return supplierContactEmail;
		}
	}

	public void setSupplierContactEmail(String supplierContactEmail) {
		this.supplierContactEmail = supplierContactEmail;
	}

	public String getSupplierContactMobileNumber() {
		if(supplierContactMobileNumber.contains("―")) {
			return null;
		} else {
			return supplierContactMobileNumber;
		}
	}

	public void setSupplierContactMobileNumber(String supplierContactMobileNumber) {
		this.supplierContactMobileNumber = supplierContactMobileNumber;
	}

	public String getSupplierContactPhoneNumber() {
		if(supplierContactPhoneNumber.contains("―")) {
			return null;
		} else {
			return supplierContactPhoneNumber;
		}
	}

	public void setSupplierContactPhoneNumber(String supplierContactPhoneNumber) {
		this.supplierContactPhoneNumber = supplierContactPhoneNumber;
	}

	public String getSupplierContactLanguage() {
		if(supplierContactLanguage.contains("―")) {
			return null;
		} else {
			return supplierContactLanguage;
		}
	}

	public void setSupplierContactLanguage(String supplierContactLanguage) {
		this.supplierContactLanguage = supplierContactLanguage;
	}

	public String getLesseeNameTitle() {
		return lesseeNameTitle;
	}

	public void setLesseeNameTitle(String lesseeNameTitle) {
		this.lesseeNameTitle = lesseeNameTitle;
	}

	public String getLesseeEnterpriseNumber() {
		return lesseeEnterpriseNumber;
	}

	public void setLesseeEnterpriseNumber(String lesseeEnterpriseNumber) {
		this.lesseeEnterpriseNumber = lesseeEnterpriseNumber;
	}

	public String getLesseeBillingAddress() {
		return lesseeBillingAddress;
	}

	public void setLesseeBillingAddress(String lesseeBillingAddress) {
		this.lesseeBillingAddress = lesseeBillingAddress;
	}

	public String getLesseeContactPerson() {
		return lesseeContactPerson;
	}

	public void setLesseeContactPerson(String lesseeContactPerson) {
		this.lesseeContactPerson = lesseeContactPerson;
	}

	public String getLesseeContactEmail() {
		if(lesseeContactEmail.contains("―")) {
			return null;
		} else {
			return lesseeContactEmail;
		}
	}

	public void setLesseeContactEmail(String lesseeContactEmail) {
		this.lesseeContactEmail = lesseeContactEmail;
	}

	public String getLesseeContactMobileNumber() {
		if(lesseeContactMobileNumber.contains("―")) {
			return null;
		} else {
			return lesseeContactMobileNumber;
		}
	}

	public void setLesseeContactMobileNumber(String lesseeContactMobileNumber) {
		this.lesseeContactMobileNumber = lesseeContactMobileNumber;
	}

	public String getLesseeContactPhoneNumber() {
		if(lesseeContactPhoneNumber.contains("―")) {
			return null;
		} else {
			return lesseeContactPhoneNumber;
		}
	}

	public void setLesseeContactPhoneNumber(String lesseeContactPhoneNumber) {
		this.lesseeContactPhoneNumber = lesseeContactPhoneNumber;
	}

	public String getLesseeContactLanguage() {
		if(lesseeContactLanguage.contains("―")) {
			return null;
		} else {
			return lesseeContactLanguage;
		}
	}

	public void setLesseeContactLanguage(String lesseeContactLanguage) {
		this.lesseeContactLanguage = lesseeContactLanguage;
	}

	public String getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductEvNumber() {
		return productEvNumber;
	}

	public void setProductEvNumber(String productEvNumber) {
		this.productEvNumber = productEvNumber;
	}

	public String getProductCardNumber() {
		return productCardNumber;
	}

	public void setProductCardNumber(String productCardNumber) {
		this.productCardNumber = productCardNumber;
	}

	public String getProductCardSent() {
		return productCardSent;
	}

	public void setProductCardSent(String productCardSent) {
		this.productCardSent = productCardSent;
	}

	public String getVehicleBrand() {
		return vehicleBrand;
	}

	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getDeliveryDesiredInstallationDate() {
		return deliveryDesiredInstallationDate;
	}

	public void setDeliveryDesiredInstallationDate(String deliveryDesiredInstallationDate) {
		this.deliveryDesiredInstallationDate = deliveryDesiredInstallationDate;
	}

	public String getDeliveryExpectedInstallationDate() {
		return deliveryExpectedInstallationDate;
	}

	public void setDeliveryExpectedInstallationDate(String deliveryExpectedInstallationDate) {
		this.deliveryExpectedInstallationDate = deliveryExpectedInstallationDate;
	}

	public String getDeliveryInstallationDate() {
		return deliveryInstallationDate;
	}

	public void setDeliveryInstallationDate(String deliveryInstallationDate) {
		this.deliveryInstallationDate = deliveryInstallationDate;
	}

	public String getDeliveryCertificationDate() {
		return deliveryCertificationDate;
	}

	public void setDeliveryCertificationDate(String deliveryCertificationDate) {
		this.deliveryCertificationDate = deliveryCertificationDate;
	}

	public String getDeliveryActivationDate() {
		return deliveryActivationDate;
	}

	public void setDeliveryActivationDate(String deliveryActivationDate) {
		this.deliveryActivationDate = deliveryActivationDate;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getPricingDescription() {
		return pricingDescription;
	}

	public void setPricingDescription(String pricingDescription) {
		this.pricingDescription = pricingDescription;
	}

	public String getPricingCrossAmount() {
		return pricingCrossAmount;
	}

	public void setPricingCrossAmount(String pricingCrossAmount) {
		this.pricingCrossAmount = pricingCrossAmount;
	}

	public String getPricingDiscount() {
		return pricingDiscount;
	}

	public void setPricingDiscount(String pricingDiscount) {
		this.pricingDiscount = pricingDiscount;
	}

	public String getPricingTotalNetAmount() {
		return pricingTotalNetAmount;
	}

	public void setPricingTotalNetAmount(String pricingTotalNetAmount) {
		this.pricingTotalNetAmount = pricingTotalNetAmount;
	}

	public String getDriverContactPerson() {
		return driverContactPerson;
	}

	public void setDriverContactPerson(String driverContactPerson) {
		this.driverContactPerson = driverContactPerson;
	}

	public String getDriverContactEmail() {
		if(driverContactEmail.contains("―")) {
			return null;
		} else {
			return driverContactEmail;
		}
	}

	public void setDriverContactEmail(String driverContactEmail) {
		this.driverContactEmail = driverContactEmail;
	}

	public String getDriverContactMobileNumber() {
		if(driverContactMobileNumber.contains("―")) {
			return null;
		} else {
			return driverContactMobileNumber;
		}
	}

	public void setDriverContactMobileNumber(String driverContactMobileNumber) {
		this.driverContactMobileNumber = driverContactMobileNumber;
	}

	public String getDriverContactPhoneNumber() {
		if(driverContactPhoneNumber.contains("―")) {
			return null;
		} else {
			return driverContactPhoneNumber;
		}
	}

	public void setDriverContactPhoneNumber(String driverContactPhoneNumber) {
		this.driverContactPhoneNumber = driverContactPhoneNumber;
	}

	public String getDriverContactLanguage() {
		if(driverContactLanguage.contains("―")) {
			return null;
		} else {
			return driverContactLanguage;
		}
	}

	public void setDriverContactLanguage(String driverContactLanguage) {
		this.driverContactLanguage = driverContactLanguage;
	}

}