package pageobjects.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ORD_Registration_Page_WebElements {

    WebDriver driver;

    private WebElement plateholder_name;
    private WebElement enterpriseNumber;
    String FilterSupplierGroup = "";
    //ORD_Registration_Page_WebElements we = new ORD_Registration_Page_WebElements();
    //ORD_Registration_Page_WebElements we;


    public ORD_Registration_Page_WebElements(WebDriver driver) {
    	this.driver = driver;
    	PageFactory.initElements(driver, this);

    }

    public WebElement getPlateholder_Name() {
        //plateholder_name = driver.findElement(By.xpath("//input[@formcontrolname='name']"));
        return plateholder_name;
    }

    public WebElement getEnterpriseNumber() {
        enterpriseNumber = driver.findElement(By.xpath("//input[@formcontrolname='enterpriseNumber']"));
        return enterpriseNumber;
    }

}