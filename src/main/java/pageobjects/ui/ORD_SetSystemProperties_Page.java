package pageobjects.ui;

import common.DecodePassword;
import cucumber.TestContext;
import dataproviders.PropertyFileReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.time.Duration;

public class ORD_SetSystemProperties_Page {
	final Logger logger = LogManager.getLogger(ORD_SetSystemProperties_Page.class);

	WebDriver driver;
	PropertyFileReader propertyFileReader;

	public ORD_SetSystemProperties_Page(WebDriver driver) {
		this.driver = driver;
		TestContext testContext;
		PageFactory.initElements(driver, this);
		propertyFileReader = new PropertyFileReader();
		
	}

	public void setSystemProperties(String leasingType) throws InterruptedException, IOException {
		if(leasingType.equals("OPERATIONAL_LEASE"))	{
			System.setProperty("leasingType", leasingType);
		} else if(leasingType.equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			System.setProperty("leasingType", leasingType);
		} else if(leasingType.equals("FINANCIAL_LEASE")) {
			System.setProperty("leasingType", leasingType);
		} else if(leasingType.equals("WALLBOX")) {
			System.setProperty("leasingType", leasingType);
		}
	}
	

}
