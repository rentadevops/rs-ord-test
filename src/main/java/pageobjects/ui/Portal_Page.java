package pageobjects.ui;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.DecodePassword;
import cucumber.TestContext;
import dataproviders.PropertyFileReader;
//import io.cucumber.java.Scenario;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Portal_Page {
	final Logger logger = LogManager.getLogger(Portal_Page.class);

	WebDriver driver;
	PropertyFileReader propertyFileReader;	

	public Portal_Page(WebDriver driver) {
		this.driver = driver;
		TestContext testContext;
		PageFactory.initElements(driver, this);
		propertyFileReader = new PropertyFileReader();
		
	}

	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	private WebElement input_login_email;

	@FindBy(how = How.XPATH, using = "//input[@name='password']")
	private WebElement input_login_password;

	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	private WebElement button_login;
	
	@FindBy(how = How.XPATH, using = "//a[@href = '/application/ord2']")
	private WebElement ord;
	
	@FindBy(how = How.XPATH, using = "")
	private WebElement account;
	
	public WebElement account(String user) {
	    account = driver.findElement(By.xpath("//span[contains(text(), '"+user+"')]"));
		return account;
	}

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-cy='rsNumberDropdown']")
	private WebElement member_drop_down;
	
	public void navigateToPortalPage() throws InterruptedException {
		logger.debug("Driver portalpage" + driver.toString());
		logger.debug("Driver get portalpage" + propertyFileReader.getPortalUrl());
		Thread.sleep(2000);
		driver.get(propertyFileReader.getPortalUrl());

		
	}
	
	public void login() throws InterruptedException {
		logger.debug("Start Login");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		DecodePassword decodePassword = new DecodePassword();
		
		if (System.getProperty("BrowserName").equals("chrome")) {
			wait.until(ExpectedConditions.elementToBeClickable(input_login_email));
		}

		Thread.sleep(2000);
		input_login_email.sendKeys("rstest2@rentasolutions.org");
		input_login_password.sendKeys(decodePassword.getDecodedPassword("UlN0ZXN0OTk5"));
		button_login.click();

		Thread.sleep(1000);
		logger.debug("End Login");

	}
	
	public void navigateToORDPage() throws InterruptedException, IOException {
		logger.debug("Start navigateToORDPage");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(ord));
		ord.click();
		Thread.sleep(3000);
		driver.navigate().refresh();

		Thread.sleep(3000);
		driver.navigate().refresh();
		
		By dropDownMembershipNumber = By.cssSelector(".mat-select-arrow");
		//driver.get(propertyFileReader.getMrtUrl());
		


		if(System.getProperty("BrowserName").equals("firefox")) {
			Thread.sleep(2000);
			driver.findElement(dropDownMembershipNumber).click();
			try {
				driver.findElement(dropDownMembershipNumber).click();
			} catch(ElementClickInterceptedException e) {
				logger.debug("Dropdown not clickable");
			}
		} else {
			Thread.sleep(5000);
			member_drop_down.click();
			wait.until(ExpectedConditions.elementToBeClickable(account(propertyFileReader.getLeasingCompany())));
		}
		
		try {
			logger.debug("Property wallbox is: " + System.getProperty("wallbox"));
			if(System.getProperty("wallbox").equals("Yes")) {
				account(propertyFileReader.getLeasingCompanyWb()).click();
			} else if(System.getProperty("leasingType").equals("REGISTRATION_LUXEMBOURG")) {
				account(propertyFileReader.getLeasingCompanyLux()).click();
			}else {
				account(propertyFileReader.getLeasingCompany()).click();
			}

		} catch(NoSuchElementException e) {
			logger.debug("Account not found");
		}
		Thread.sleep(1000);

		Thread.sleep(1000);
		logger.debug("End navigateToORDPage");
	}


}
