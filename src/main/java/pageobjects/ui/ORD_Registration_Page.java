package pageobjects.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Random;

import dataproviders.PropertyFileReader;
import helpers.ORD_Commands_Helper;
import helpers.ORD_ContractNumberGenerator;
import helpers.ORD_DatePicker_Helper;
import io.cucumber.datatable.DataTable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ORD_Registration_Page {
	final Logger logger = LogManager.getLogger(ORD_Registration_Page.class);
	
	WebDriver driver;
	
	String FilterActivityGroupCode = "";
	String FilterSupplierGroup = "";
	//ORD_Registration_Page_WebElements we = new ORD_Registration_Page_WebElements();
	//ORD_Registration_Page_WebElements we;

	ORD_Registration_Page_WebElements rp = new ORD_Registration_Page_WebElements(driver);

	PropertyFileReader propertyFileReader = new PropertyFileReader();
	String language;

	public ORD_Registration_Page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}

	@FindBy(how = How.XPATH, using = "//span[contains(.,'New registration')]/parent::button")
	private WebElement btn_new;

	@FindBy(how = How.XPATH, using = "//mat-icon[contains(text(), 'filter_list')]")
	private WebElement btn_filter;

	@FindBy(how = How.XPATH, using = "//section//mat-icon[contains(text(), 'close')]")
	private WebElement btn_close_filter;

	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td/span")
	private WebElement first_result;

	@FindBy(how = How.XPATH, using = "//a[@href='registrations/active-registrations-overview']")
	private WebElement registrations;

	@FindBy(how = How.XPATH, using = "//a[@data-menuitem='active-registrations-overview']")
	private WebElement registrations_menu;

	@FindBy(how = How.XPATH, using = "//footer//button/span")
	private WebElement footer_actual_language;

	@FindBy(how = How.XPATH, using = "(//button[contains(@class, 'btn-lang-switcher')])[1]")
	private WebElement btn_language;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'English')]")
	private WebElement btn_english;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Nederlands')]")
	private WebElement btn_nederlands;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Français')]")
	private WebElement btn_francais;

	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Deutsch')]")
	private WebElement btn_deutsch;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='name']")
	private WebElement plateholder_name;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='enterpriseNumber']")
	private WebElement plateholder_enterprise_number;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='registration-details-overview-language-field']")
	private WebElement select_plateholder_language;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'French')]")
	private WebElement option_plateholder_language_french;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='registrarDossierNumber']")
	private WebElement input_registration_dossier_number;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='registration-details-overview-insurance-company-field']")
	private WebElement select_insurance_company;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'00079')]")
	private WebElement option_insurance_company_00079;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='referenceNumber']")
	private WebElement input_insurance_reference_number;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='formNumber']")
	private WebElement input_form_number;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@formgroupname='supplier']//input[@formcontrolname='name']")
	private WebElement input_supplier_name;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@formgroupname='supplier']//input[@formcontrolname='enterpriseNumber']")
	private WebElement input_supplier_enterprise_number;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='vin']")
	private WebElement input_vehicle_vin;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='vinControlNumber']")
	private WebElement input_vehicle_vin_control_number;

	@FindBy(how = How.XPATH, using = "//input[@value='DISTRIBUTION_CENTER']")
	private WebElement radio_delivery_distribution_adres;

	@FindBy(how = How.XPATH, using = "//input[@value='LEASING_COMPANY']")
	private WebElement radio_leasing_company_adres;

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Register')]")
	private WebElement btn_register;

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Confirm')]")
	private WebElement btn_confirm;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='plannedRegistrationDate']")
	private WebElement select_wanted_registration_date;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Choose month and year']")
	private WebElement select_choose_month_and_year;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@formcontrolname='frontPlateOrdered']//input")
	private WebElement chkbox_ordered_front_plate;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@formcontrolname='reuseLicensePlate']//input")
	private WebElement chkbox_reuse_license_plate;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@data-testid='registration-details-overview-bicycle-checkbox']//input")
	private WebElement chkbox_bicycle;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@formcontrolname='speedPedelecFlag']//input")
	private WebElement toggle_speed_pedelec;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='licensePlate']")
	private WebElement input_reuse_license_plate;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='lastNameOrCompanyName']")
	private WebElement input_last_name_or_company_name;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='firstName']")
	private WebElement input_first_name;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='houseNumber']")
	private WebElement input_house_number;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='postalCode']")
	private WebElement input_postal_code;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='street']")
	private WebElement input_street;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='city']")
	private WebElement input_city;

	@FindBy(how = How.XPATH, using = "//rs-display-field-or-value[@data-testid='registration-details-overview-used-vehicle-checkbox']//input")
	private WebElement radio_used_vehicle;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='priorLicensePlate']")
	private WebElement input_prior_license_plate;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='lastRegistrationDate']")
	private WebElement select_last_registration_date;

	@FindBy(how = How.XPATH, using = "//td[contains(.,'2022')]")
	private WebElement select_last_registration_date_year;

	@FindBy(how = How.XPATH, using = "//td[contains(.,'MAY')]")
	private WebElement select_last_registration_date_month;

	@FindBy(how = How.XPATH, using = "//td[contains(.,'15')]")
	private WebElement select_last_registration_date_day;

	@FindBy(how = How.XPATH, using = "//mat-option[contains(.,'Pedaling Assistance Only')]")
	private WebElement option_pedeling_assistance_only;

	@FindBy(how = How.XPATH, using = "//mat-option[contains(.,'Pedaling Assistance + Autonomous Driving')]")
	private WebElement option_pedeling_assistance_anonymous_driving;

	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='pedalingAssistance']")
	private WebElement list_pedeling_assistance;

		public void create_registration_ui(List<List<String>> data) throws InterruptedException {
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		//WebElement plateholder_name = rp.getPlateholder_Name();
		//WebElement plateholder_EnterpriseNumber = rp.getEnterpriseNumber();
		ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
		ORD_Commands_Helper och = new ORD_Commands_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(120000));
		String dossierNumber = mcng.getRandomContractNumber();


		String plateHolderName = data.get(1).get(0);
		String plateholderEnterpriseNnumber = data.get(1).get(1);
		//String plateHolderRegistrationDossierNr = data.get(1).get(3);
		String formNumber = data.get(1).get(7);
		String supplierName = data.get(1).get(10);
		String supplierEnterpriseNumber = data.get(1).get(11);
		String vehicleVin = data.get(1).get(12);
		String vehicleVinControlNumber = data.get(1).get(13);
		String orderedFrontPlate = data.get(1).get(18);
		String wantedRegistrationDate = data.get(1).get(9);
		String reusePlate = data.get(1).get(16);
		String reuseLicensePlate = data.get(1).get(20);
		String bicycle = data.get(1).get(15);
		String speed_pedelec = data.get(1).get(21);
		String boxNumber = data.get(1).get(22);
		String city = data.get(1).get(23);
		String firstName = data.get(1).get(24);
		String houseNumber = data.get(1).get(25);
		String postalCode = data.get(1).get(26);
		String street = data.get(1).get(27);
		String lastNameOrCompanyName = data.get(1).get(28);
		String priorLicensePlate = data.get(1).get(29);
			String usedVehicle = data.get(1).get(14);

		Thread.sleep(2000);
			plateholder_name.sendKeys(plateHolderName);
		plateholder_enterprise_number.clear();
		plateholder_enterprise_number.sendKeys(plateholderEnterpriseNnumber);
		//select_plateholder_language.click();
		//option_plateholder_language_french.click();
		input_registration_dossier_number.sendKeys(dossierNumber);
		select_insurance_company.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", option_insurance_company_00079);
		option_insurance_company_00079.click();
		input_insurance_reference_number.sendKeys(dossierNumber);

			//Random r = new Random();
			//int low = 900000000;
			//int high = 999999999;
			//int result = r.nextInt(high-low) + low;
			//formNumber = String.valueOf(result);
		input_form_number.sendKeys(formNumber);

		//select_wanted_registration_date.click();
		//odh.pick_date_wanted_registration(1);
		if(wantedRegistrationDate != null) {
			if(System.getProperty("insuranceType").equals("planned")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_wanted_registration_date);
				Thread.sleep(1000);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", odh.select_day(odh.pick_date_wanted_registration(1)));

			} else {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_wanted_registration_date);
				//odh.select_day(odh.pick_day()).click();
				//odh.select_day(odh.pick_date_wanted_registration(1)).click();
				Thread.sleep(1000);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", odh.select_day(odh.pick_date_wanted_registration(0)));
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", odh.select_day(odh.pick_date_wanted_registration(1)));

			}
					}


		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_supplier_name);
		input_supplier_name.sendKeys(supplierName);
		input_supplier_enterprise_number.sendKeys(supplierEnterpriseNumber);
		input_vehicle_vin.sendKeys(vehicleVin);
		input_vehicle_vin_control_number.sendKeys(vehicleVinControlNumber);

			if(usedVehicle != null && !usedVehicle.isEmpty()) {
				if((usedVehicle.equalsIgnoreCase("true") && !radio_used_vehicle.isSelected()) || (usedVehicle.equalsIgnoreCase("false") && radio_used_vehicle.isSelected())) {
					och.click_element_JS(radio_used_vehicle);
					Thread.sleep(500);
				}
			}

			if(data.get(1).get(14) != null) {
				if(data.get(1).get(14).equalsIgnoreCase("true")) {
					input_prior_license_plate.sendKeys(priorLicensePlate);
				} else {

				}
			}

			if(data.get(1).get(14).equalsIgnoreCase("true")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_last_registration_date);
				select_last_registration_date_year.click();
				select_last_registration_date_month.click();
				select_last_registration_date_day.click();
			} else {

				}


		/*if(speed_pedelec != null && !speed_pedelec.isEmpty()) {
			if((speed_pedelec.equalsIgnoreCase("true") && !toggle_speed_pedelec.isSelected()) || (speed_pedelec.equalsIgnoreCase("false") && toggle_speed_pedelec.isSelected())) {
				och.click_element_JS(toggle_speed_pedelec);
				//list_pedeling_assistance.click();
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", list_pedeling_assistance);
				//Thread.sleep(2000);
				//option_pedeling_assistance_anonymous_driving.click();
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", option_pedeling_assistance_anonymous_driving);
				Thread.sleep(500);
			}
		}*/

		if(bicycle != null && !bicycle.isEmpty()) {
			if((bicycle.equalsIgnoreCase("true") && !chkbox_bicycle.isSelected()) || (bicycle.equalsIgnoreCase("false") && chkbox_bicycle.isSelected())) {
				och.click_element_JS(chkbox_bicycle);
				Thread.sleep(500);
			}
		}

		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", list_pedeling_assistance);
			Thread.sleep(2000);
			//option_pedeling_assistance_anonymous_driving.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", option_pedeling_assistance_anonymous_driving);
			Thread.sleep(500);
		} catch(Exception e) {
			logger.debug("set pedaling assistance error");
		}

		if(orderedFrontPlate != null && !orderedFrontPlate.isEmpty()) {
			if((orderedFrontPlate.equalsIgnoreCase("true") && !chkbox_ordered_front_plate.isSelected()) || (orderedFrontPlate.equalsIgnoreCase("false") && chkbox_ordered_front_plate.isSelected())) {
				och.click_element_JS(chkbox_ordered_front_plate);
				Thread.sleep(500);
			}
		}

		if(reusePlate != null && !reusePlate.isEmpty()) {
			if((reusePlate.equalsIgnoreCase("true") && !chkbox_reuse_license_plate.isSelected()) || (reusePlate.equalsIgnoreCase("false") && chkbox_reuse_license_plate.isSelected())) {
				och.click_element_JS(chkbox_reuse_license_plate);
				Thread.sleep(500);
			}
		}

		if(data.get(1).get(16) != null) {

			if (data.get(1).get(16).equalsIgnoreCase("true")) {
				input_reuse_license_plate.sendKeys(reuseLicensePlate);
			} else {

			}
		}
		//input_reuse_license_plate.sendKeys(reuseLicensePlate);

		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_delivery_distribution_adres);
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio_delivery_distribution_adres);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_leasing_company_adres);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio_leasing_company_adres);
			//input_last_name_or_company_name.sendKeys(lastNameOrCompanyName);
			//input_street.sendKeys(street);
			//input_house_number.sendKeys(houseNumber);
			//input_postal_code.sendKeys(postalCode);
			//input_city.sendKeys(city);

		//select_wanted_registration_date.click();
		//odh.pick_date_wanted_registration(1);

		//REGISTER OR NOT
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_register);
			Thread.sleep(20000);
			wait.until(ExpectedConditions.elementToBeClickable(btn_confirm));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_confirm);
			//END REGISTER OR NOT


	}

	public void update_registrations() throws InterruptedException {
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
		ORD_Commands_Helper och = new ORD_Commands_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(120000));

		String wantedRegistrationDate = "planned";

		if(wantedRegistrationDate != null) {

				Thread.sleep(5000);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_wanted_registration_date);
				//odh.select_day(odh.pick_day()).click();
				//odh.select_day(odh.pick_date_wanted_registration(1)).click();
				Thread.sleep(5000);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", odh.select_day(odh.pick_date_wanted_registration(0)));
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", odh.select_day(odh.pick_date_wanted_registration(1)));


		}

		Thread.sleep(5000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_register);
		//btn_register.click();
		Thread.sleep(10000);
		//wait.until(ExpectedConditions.elementToBeClickable(btn_confirm));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_confirm);
		//btn_confirm.click();

	}






	public void open_registrations() throws InterruptedException {
			Thread.sleep(5000);
		registrations_menu.click();
		registrations.click();
	}

	public void click_add(List<List<String>> data) throws InterruptedException, IOException {

		Thread.sleep(5000);

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));

		selectLanguage();

		btn_new.click();
		Thread.sleep(2000);
		create_registration_ui(data);
	}

	public String getLanguage(String language) {
		switch (language) {
			case "nl":
				language = "Nederlands";
				break;
			case "en":
				language = "English";
				break;
			case "fr":
				language = "Français";
				break;
			case "de":
				language = "Deutsch";
				break;
		}
		return language;
	}

	public void selectLanguage() throws FileNotFoundException, IOException, InterruptedException {
		ORD_Commands_Helper mch = new ORD_Commands_Helper(driver);
		String[] parts = footer_actual_language.getText().split(" ");
		String actualLanguage = parts[0].trim();
		logger.debug("actualLanguage in Topbar selectLanguage: " + actualLanguage);
		logger.debug("systemproperty language: " + System.getProperty("language"));
		if(System.getProperty("language") != null && !System.getProperty("language").isEmpty()) {
			language = getLanguage(System.getProperty("language"));
			logger.debug("language: " + language);
			logger.debug("actualLanguage in topbar getLanguage: " + actualLanguage);
			if(!language.equalsIgnoreCase(actualLanguage)) {
				logger.debug("language != actualLanguage");

				if (language.equals("Nederlands")) {
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_nederlands);
				}
				if (language.equals("English")) {
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_english);
					logger.debug("button_english geklikt");
				}
				if (language.equals("Français")) {
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_francais);
				}
				if (language.equals("Deutsch")) {
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_deutsch);
				}
			}
		} else {
			language = propertyFileReader.getProperty("language");
			logger.debug("language from properties file: " + language);
			if(!getLanguage(language).equalsIgnoreCase(actualLanguage)) {

				if (language.equals("nl")) {
					logger.debug("set language nl");
					System.setProperty("language", "nl");
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_nederlands);
				}
				if (language.equals("en")) {
					logger.debug("set language en");
					System.setProperty("language", "en");
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_english);
				}
				if (language.equals("fr")) {
					logger.debug("set language fr");
					System.setProperty("language", "fr");
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_francais);
				}
				if (language.equals("de")) {
					logger.debug("set language de");
					System.setProperty("language", "de");
					Thread.sleep(2000);
					mch.click_element_JS(btn_language);
					mch.click_element_JS(btn_deutsch);
				}

			}
		}
	}
	
	
}