package pageobjects.ui;

import cucumber.TestContext;
import dataproviders.PropertyFileReader;
import helpers.AssertHelper;
import io.appium.java_client.AppiumDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import pageobjects.ftp.FTP_PostOrderImportsRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;


public class Validate_Mobile_Vehicle_Info {
	public final Logger logger = LogManager.getLogger(Validate_Mobile_Vehicle_Info.class);

	TestContext testContext;
	AppiumDriver driver;
	PropertyFileReader propertyFileReader = new PropertyFileReader();
	JavascriptExecutor jse = (JavascriptExecutor)driver;

	String language;
	String portal_url;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	AssertHelper ah = new AssertHelper();
	private PDDocument doc;


	public Validate_Mobile_Vehicle_Info(TestContext context) throws InterruptedException {
		//super();
		//this.driver = driver;
		//PageFactory.initElements(driver, this);
		testContext = context;
		logger.debug("testContext: " + testContext.getScenarioContext().toString());
		//ScenarioContext scenarioContext = testContext.getScenarioContext();
		postOrderImportsRequest = testContext.getPageObjectManager().postFTP_PostOrderImportsRequest();
		//getOrderByDossierNumber = testContext.getPageObjectManager().getAPI_GetOrderByDossierNumber();
		
	}

	public void validateVehicleInfo() {
		logger.debug("Start validateVehicleInfo");
		try {
			File toRead=new File("./src/test/resources/runtime/VehicleMap");
			FileInputStream fis=new FileInputStream(toRead);
			ObjectInputStream ois=new ObjectInputStream(fis);

			HashMap<String,String> mapInFile=(HashMap<String,String>)ois.readObject();

			ois.close();
			fis.close();
			//print All data in MAP
			//for(Map.Entry<String,String> m :mapInFile.entrySet()){
			//	System.out.println(m.getKey()+" : "+m.getValue());
			//}
			mapInFile.forEach((k, v) -> {
				logger.debug("Key: " + k + ", Value: " + v);
				if (k.equals("Titel")) {
					ah.assertEquals("Titel: ", "Ingeleverd voertuig", v);
				}
				if (k.equals("Merk: ")) {
					ah.assertEquals("Merk: ", postOrderImportsRequest.getVehicleBrand(), v);
				}
			});
		} catch(Exception e) {}
	}

		/*mapInFile.forEach((k, v) -> {
			logger.debug("Key: " + k + ", Value: " + v);
			if (k.equals("Titel")) {
				ah.assertEquals("Titel: ", "Ingeleverd voertuig", v);
			}
			if (k.equals("Merk: ")) {
				ah.assertEquals("Merk: ", postOrderImportsRequest.getVehicleBrand(), v);
			}
		});*/
	}



	
	

