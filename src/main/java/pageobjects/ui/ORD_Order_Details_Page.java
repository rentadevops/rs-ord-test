package pageobjects.ui;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import apiEngine.model.get.order.detail.Agent;
import apiEngine.model.get.order.detail.Buyback;
import apiEngine.model.get.order.detail.Client;
import apiEngine.model.get.order.detail.Dealer;
import apiEngine.model.get.order.detail.Delivery;
import apiEngine.model.get.order.detail.DossierManager;
import apiEngine.model.get.order.detail.Driver;
import apiEngine.model.get.order.detail.FinancialLeaseRegistrationInfo;
import apiEngine.model.get.order.detail.InscriptionCosts;
import apiEngine.model.get.order.detail.Insurance;
import apiEngine.model.get.order.detail.LastComment;
import apiEngine.model.get.order.detail.LeasingCompany;
import apiEngine.model.get.order.detail.OptionDTO;
import apiEngine.model.get.order.detail.Pickup;
import apiEngine.model.get.order.detail.Pricing;
import apiEngine.model.get.order.detail.Product;
import apiEngine.model.get.order.detail.PromotionDTO;
import apiEngine.model.get.order.detail.TaxStampCosts;
import apiEngine.model.get.order.detail.TyreFitter;
import apiEngine.model.get.order.detail.Vehicle;
import pageobjects.api.API_GetOrderDetailById;

public class ORD_Order_Details_Page {
	final Logger logger = LogManager.getLogger(ORD_Order_Details_Page.class);

	WebDriver driver;
	

	
	private String orderStatus;
	private String leasingCompanyDossierNumber;
	private String orderNumberSupplier;
	private String CustomerNumberSupplier;
	private String AgentRsNumber;
	private String AgentName;
	private String versionNumber;
	
	private String leasingCompanyName;
	private String leasingCompanyEnterpriseNumber;
	private String leasingCompanyRsNumber;
	private String leasingCompanybillingAddress;
	private String leasingCompanyCountry;
	private String leasingCompanyContactPerson;
	private String leasingCompanyContactEmail;
	private String leasingCompanyContactMobileNumber;
	private String leasingCompanyContactPhoneNumber;
	private String leasingCompanyContactLanguage;
	
	private String supplierNameTitle;
	private String supplierName;
	private String supplierEnterpriseNumber;
	private String supplierRsNumber;
	private String supplierbillingAddress;
	private String supplierCountry;
	private String supplierContactPerson;
	private String supplierContactEmail;
	private String supplierContactMobileNumber;
	private String supplierContactPhoneNumber;
	private String supplierContactLanguage;
	
	private String lesseeNameTitle;
	private String lesseeEnterpriseNumber;
	private String lesseeBillingAddress;
	private String lesseeContactPerson;
	private String lesseeContactEmail;
	private String lesseeContactMobileNumber;
	private String lesseeContactPhoneNumber;
	private String lesseeContactLanguage;
	
	private String driverAddress;
	private String driverContactPerson;
	private String driverContactEmail;
	private String driverContactMobileNumber;
	private String driverContactPhoneNumber;
	private String driverContactLanguage;
	
	private String vehicleDescriptionTitle;
	private String vehicleVIN;
	private String vehicleVINControlNumber;
	private String vehicleModelYear;
	private String vehicleIdentificationCode;
	private String vehicleLicensePlate;
	private String vehicleStock;
	private String vehicleMileage;
	private String vehicleLanguageDocuments;
	private String vehicleKeycode;
	private String vehicleStartcode;
	
	private String motorType;
	private String power;
	private String engineCapacity;
	private String nedcValue;
	private String wltpValue;
	private String interiorColor;
	private String tyreSpecifications;
	private String tyreBrand;
	private String tyreType;
	private String weight;
	private String exteriorColor;
	
	private String vehiclePrice;
	private String discount;
	private String promotionDiscount;
	private String deliveryCosts;
	private String deliveryCostsDiscount;
	private String otherCosts;
	private String otherCostsDiscount;
	private String totalPrice;
	private String totalPriceDiscount;
	
	//Delivery information
	private String desiredDeliveryDate;
	private String expectedDeliveryDate;
	private String actualDeliveryDate;
	private String firstRegistrationDate; 
	
	//Different delivery address
	private String deliveryLocationNameUI;
	private String deliveryEnterpriseNumber;
	private String deliveryAddress;
	private String deliveryContactPersonUI;
	private String deliveryContactEmailUI;
	private String deliveryContactMobileNumberUI;
	private String deliveryContactPhoneNumberUI;
	private String deliveryContactLanguageUI;
	
	//Returned vehicle information
	private String returnedVehicle;
	private String registeredOnTheName;
	private String returnedVehicleLicenseplate;
	
	//Equipment and enclosed documents
	private String registrationCertificate;
	private String insuranceCertificate;
	private String certificateOfConformity;
	private String technicalInspectionCertificate;
	private String legalKit;
	private String serviceManual;
	private String fuelCard;
	private String numberOfKeys;
	
	//Driver information
	private String returnedVehicleDriverName;
	private String returnedVehicleDriverDateOfBirth;
	private String returnedVehicleDriverBirthplace;
	
	//Buyback
	private String buybackApplicable;
	private String buybackName;
	private String buybackAddress;
	private String contractDuration;
	private String totalMileage;
	private String buybackAmount;
	
	List<WebElement> elements;

	public ORD_Order_Details_Page(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		//get_leasing_company_contract_data();
		
	}
	
//region General
	@FindBy(how = How.XPATH, using = "//div[@data-testid='order-statuses-general-data-order-details-value']")
	private WebElement we_order_status;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='lc-order-number-general-data-order-details-value']")
	private WebElement we_leasingcompany_dossiernumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='dealer-commission-number-general-data-order-details-value']")
	private WebElement we_ordernumbersupplier;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='dealer-customer-number-general-data-order-details-value']")
	private WebElement we_customernumber_supplier;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='agent-rs-number-general-data-order-details-value']")
	private WebElement we_agent_rsnumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='agent-name-general-data-order-details-value']")
	private WebElement we_agent_name;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='agent-name-general-data-order-details-value']")
	private WebElement we_version_number;

//endregion General

//region Leasing company
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='name-registered-office-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_name;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='enterprise-number-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_enterprisenumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='rs-number-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_rsnumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='billing-address-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_billingaddress;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='country-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_country;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-contact-leasing-company-order-details-value']/preceding-sibling::div")
	private WebElement we_leasingcompany_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-contact-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='mobile-contact-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='phone-contact-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='languages-contact-leasing-company-order-details-value']")
	private WebElement we_leasingcompany_contact_language;
	
	
//endregion Leasing company

//region Supplier

	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Supplier']/h2")
	private WebElement we_supplier_name_title;

	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Fournisseur']/h2")
	private WebElement we_supplier_name_title_fr;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='name-registered-office-supplier-order-details-value']")
	private WebElement we_supplier_name;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='enterprise-number-supplier-order-details-value']")
	private WebElement we_supplier_enterprisenumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='rs-number-supplier-order-details-value']")
	private WebElement we_supplier_rsnumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='billing-address-supplier-order-details-value']")
	private WebElement we_supplier_billingaddress;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='country-supplier-order-details-value']")
	private WebElement we_supplier_country;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-contact-supplier-order-details-value']/preceding-sibling::div")
	private WebElement we_supplier_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-contact-supplier-order-details-value']")
	private WebElement we_supplier_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='mobile-contact-supplier-order-details-value']")
	private WebElement we_supplier_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='phone-contact-supplier-order-details-value']")
	private WebElement we_supplier_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='language-contact-supplier-order-details-value']")
	private WebElement we_supplier_contact_language;

//endregion Supplier

//region Lessee
	
	
	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Lessee']/h2")
	private WebElement we_lessee_name_title;

	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Locataire']/h2")
	private WebElement we_lessee_name_title_fr;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='enterprise-number-lessee-order-details-value']")
	private WebElement we_lessee_enterprisenumber;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='billing-adress-lessee-order-details-value']")
	private WebElement we_lessee_billingaddress;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-lessee-order-details-value']/preceding-sibling::div")
	private WebElement we_lessee_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-lessee-order-details-value']")
	private WebElement we_lessee_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='smartphone-lessee-order-details-value']")
	private WebElement we_lessee_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='phone-number-lessee-order-details-value']")
	private WebElement we_lessee_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='languages-lessee-order-details-value']")
	private WebElement we_lessee_contact_language;
	
	
//endregion Lessee

//region Driver

	

	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='address-driver-order-details-value']")
	private WebElement we_driver_address;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-driver-order-details-value']/preceding-sibling::div")
	private WebElement we_driver_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-driver-order-details-value']")
	private WebElement we_driver_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='smartphone-driver-order-details-value']")
	private WebElement we_driver_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='phone-number-driver-order-details-value']")
	private WebElement we_driver_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='languages-code-driver-order-details-value']")
	private WebElement we_driver_contact_language;
	
	
//endregion Driver

//region Vehicle information

	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Vehicle description']/h2")
	private WebElement we_vehicle_description_title;

	@FindBy(how = How.XPATH, using = "//rs-titles-manager[@ng-reflect-title='Description du véhicule']/h2")
	private WebElement we_vehicle_description_title_fr;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='vin-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_vin;	
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='vin-control-number-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_vin_controlnumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='model-year-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_modelyear;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='identification-code-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_identificationcode;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='license-plate-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_licenseplate;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='stock-vehicle-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_stock;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='mileage-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_mileage;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='languages-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_language_documents;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='key-code-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_keycode;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='start-code-vehicle-description-info-order-details-value']")
	private WebElement we_vehicle_startcode;
	
	
//endregion Vehicle information

//region Technical specifications
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='motor-type-tech-spec-order-details-value']")
	private WebElement we_vehicle_motortype;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='power-tech-spec-order-details-value']")
	private WebElement we_vehicle_power;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='engine-capacity-tech-spec-order-details-value']")
	private WebElement we_vehicle_enginecapacity;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='nedc-tech-spec-order-details-value']")
	private WebElement we_vehicle_nedcvalue;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='wltp-tech-spec-order-details-value']")
	private WebElement we_vehicle_wltpvalue;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='interior-tech-spec-order-details-value']")
	private WebElement we_vehicle_interiorcolor;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='tyre-spec-tech-spec-order-details-value']")
	private WebElement we_vehicle_tyrespecifications;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='tyre-brand-tech-spec-order-details-value']")
	private WebElement we_vehicle_tyrebrand;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='tyre-type-tech-spec-order-details-value']")
	private WebElement we_vehicle_tyretype;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='weight-tech-spec-order-details-value']")
	private WebElement we_vehicle_weight;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='color-tech-spec-order-details-value']")
	private WebElement we_vehicle_exteriorcolor;


//endregion Technical specifications
	
//region Pricing
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='vehicle-price-pricing-options-order-details-value']")
	private WebElement we_pricing_vehicleprice;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='fleet-discount-pricing-options-order-details-value']")
	private WebElement we_pricing_discount;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='promotion-pricing-options-order-details-value']")
	private WebElement we_promotions_discount;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='delivery-cost-pricing-options-order-details-value']")
	private WebElement we_pricing_deliverycosts;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='right-delivery-cost-pricing-options-order-details-value']")
	private WebElement we_pricing_deliverycosts_discount;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='other-cost-pricing-options-order-details-value']")
	private WebElement we_pricing_othercosts;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='right-other-cost-pricing-options-order-details-value']")
	private WebElement we_pricing_othercosts_discount;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='total-amount-pricing-options-order-details-value']")
	private WebElement we_pricing_totalprice;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='right-total-amount-pricing-options-order-details-value']")
	private WebElement we_pricing_totalprice_discount;

	
//endregion Pricing
	
//region Delivery information
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='desired-delivery-date-delivery-info-order-details']")
	private WebElement we_delivery_desireddeliverydate;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='expected-delivery-date-delivery-info-order-details']")
	private WebElement we_delivery_expecteddeliverydate;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='actual-delivery-date-delivery-info-order-details']")
	private WebElement we_delivery_actualdeliverydate;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='first-registration-date-delivery-info-order-details']")
	private WebElement we_delivery_firstregistrationdate;

	
//endregion Delivery information
	
//region Different delivery address
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='name-delivery-info-adress-order-details']")
	private WebElement we_delivery_locationname;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='enterprise-number-delivery-info-adress-order-details']")
	private WebElement we_delivery_enterprisenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='adress-delivery-info-adress-order-details']")
	private WebElement we_delivery_address;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-delivery-info-adress-order-details']/preceding-sibling::div")
	private WebElement we_delivery_contact_person;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='email-delivery-info-adress-order-details']")
	private WebElement we_delivery_contact_email;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='smartphone-delivery-info-adress-order-details']")
	private WebElement we_delivery_contact_mobilenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='phone-delivery-info-adress-order-details']")
	private WebElement we_delivery_contact_phonenumber;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='languages-delivery-info-adress-order-details']")
	private WebElement we_delivery_contact_language;

//endregion Different delivery address
	
//region Returned vehicle information
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='returned-vehicle-boolean-icon-order-details']")
	private WebElement we_delivery_returnedvehicle;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='registered-name-delivery-info-order-details-value']")
	private WebElement we_delivery_returnedvehicle_registeredonthename;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='license-plate-delivery-info-order-details-value']")
	private WebElement we_delivery_returnedvehicle_licenseplate;
	
//endregion Returned vehicle information

//region Equipment and enclosed documents
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='registration-certificate-boolean-icon-order-details']")
	private WebElement we_delivery_documents_registrationcertificate;
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='insurance-certificate-boolean-icon-order-details']")
	private WebElement we_delivery_documents_insurancecertificate;
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='conformity-certificate-boolean-icon-order-details']")
	private WebElement we_delivery_documents_coc;
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='inspection-technical-certificate-boolean-icon-order-details']")
	private WebElement we_delivery_documents_technicalinspectioncertificate;
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='legal-kit-boolean-icon-order-details']")
	private WebElement we_delivery_equipment_legalkit;
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='service-manual-boolean-icon-order-details']")
	private WebElement we_delivery_documents_servicemanual;
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='fuel-card-boolean-icon-order-details']")
	private WebElement we_delivery_documents_fuelcard;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='number-keys-delivery-info-order-details']")
	private WebElement we_delivery_equipment_numberofkeys;
	
//endregion Equipment and enclosed documents	

//region Driver information
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='delivery-information-name-order-details-value']")
	private WebElement we_delivery_returnedvehicle_drivername;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='delivery-information-date-of-birth-order-details-value']")
	private WebElement we_delivery_returnedvehicle_driverdateofbirth;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='delivery-information-birth-place-order-details-value']")
	private WebElement we_delivery_returnedvehicle_driverbirthplace;
	
//endregion Driver information

//region Buyback
	
	@FindBy(how = How.XPATH, using = "//rs-boolean-icon[@data-testid='buyback-applicable-icon-order-details-value']")
	private WebElement we_buyback_applicable;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='buyback-name-order-details-value']")
	private WebElement we_buyback_name;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='buyback-adress-order-details-value']")
	private WebElement we_buyback_address;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='buyback-contract-duration-order-details-value']")
	private WebElement we_buyback_contractduration;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='buyback-kilometers-order-details-value']")
	private WebElement we_buyback_totalmileage;
	
	@FindBy(how = How.XPATH, using = "//div[@data-testid='buyback-amount-order-details-value']")
	private WebElement we_buyback_amount;
	
//endregion Buyback
	
	public void get_order_details_data() throws InterruptedException, ParseException {
		
	
		Thread.sleep(5000);
		
		//General
		orderStatus = we_order_status.getText().trim();
		leasingCompanyDossierNumber = we_leasingcompany_dossiernumber.getText().trim();
		orderNumberSupplier = we_ordernumbersupplier.getText().trim();
		CustomerNumberSupplier = we_customernumber_supplier.getText().trim();
		AgentRsNumber = we_agent_rsnumber.getText().trim();
		AgentName = we_agent_name.getText().trim();
		versionNumber = we_version_number.getText().trim();
		logger.debug("orderStatus: "+orderStatus+" leasingCompanyDossierNumber: "+leasingCompanyDossierNumber+" orderNumberSupplier: "+orderNumberSupplier+
				" CustomerNumberSupplier: "+CustomerNumberSupplier+" AgentRsNumber: "+AgentRsNumber+" AgentName: "+AgentName+
				" versionNumber: "+versionNumber);
		
		//Leasing company
		leasingCompanyName = we_leasingcompany_name.getText().trim();
		leasingCompanyEnterpriseNumber = we_leasingcompany_enterprisenumber.getText().trim();
		leasingCompanyRsNumber = we_leasingcompany_rsnumber.getText().trim();
		leasingCompanybillingAddress = we_leasingcompany_billingaddress.getText().trim();
		leasingCompanyCountry = we_leasingcompany_country.getText().trim();
		leasingCompanyContactPerson = we_leasingcompany_contact_person.getText().trim();
		leasingCompanyContactEmail = we_leasingcompany_contact_email.getText().trim();
		leasingCompanyContactMobileNumber = we_leasingcompany_contact_mobilenumber.getText().replace("smartphone", "").trim();
		leasingCompanyContactPhoneNumber = we_leasingcompany_contact_phonenumber.getText().replace("phone", "").trim();
		leasingCompanyContactLanguage = we_leasingcompany_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
		logger.debug("leasingCompanyName: "+leasingCompanyName+" leasingCompanyEnterpriseNumber: "+leasingCompanyEnterpriseNumber+
				" leasingCompanyRsNumber: "+leasingCompanyRsNumber+" leasingCompanybillingAddress: "+leasingCompanybillingAddress+
				" leasingCompanyCountry: "+leasingCompanyCountry+" leasingCompanyContactPerson: "+leasingCompanyContactPerson+
				" leasingCompanyContactEmail: "+leasingCompanyContactEmail+" leasingCompanyContactMobileNumber:"+leasingCompanyContactMobileNumber+
				" leasingCompanyContactPhoneNumber: "+leasingCompanyContactPhoneNumber+" leasingCompanyContactLanguage: "+leasingCompanyContactLanguage);

		//Supplier
		if(System.getProperty("language").equals("fr")) {
			supplierNameTitle = we_supplier_name_title_fr.getText().trim();
		} else {
			supplierNameTitle = we_supplier_name_title.getText().trim();
		}

		supplierName = we_supplier_name.getText().trim();
		supplierEnterpriseNumber = we_supplier_enterprisenumber.getText().trim();
		supplierRsNumber = we_supplier_rsnumber.getText().trim();
		supplierbillingAddress = we_supplier_billingaddress.getText().trim();
		supplierCountry = we_supplier_country.getText().trim();
		supplierContactPerson = we_supplier_contact_person.getText().trim();
		supplierContactEmail = we_supplier_contact_email.getText().replace("email", "").trim();
		supplierContactMobileNumber = we_supplier_contact_mobilenumber.getText().replace("smartphone", "").trim();
		supplierContactPhoneNumber = we_supplier_contact_phonenumber.getText().replace("phone", "").trim();
		supplierContactLanguage = we_supplier_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
		logger.debug("supplierNameTitle: "+supplierNameTitle+ " supplierName: "+supplierName+" supplierEnterpriseNumber: "+supplierEnterpriseNumber+
				" supplierRsNumber: "+supplierRsNumber+" supplierbillingAddress: "+supplierbillingAddress+
				" supplierCountry: "+supplierCountry+" supplierContactPerson: "+supplierContactPerson+
				" supplierContactEmail: "+supplierContactEmail+" supplierContactMobileNumber:"+supplierContactMobileNumber+
				" supplierContactPhoneNumber: "+supplierContactPhoneNumber+" supplierContactLanguage: "+supplierContactLanguage);

		
		//Lessee
		if(System.getProperty("language").equals("fr")) {
			lesseeNameTitle = we_lessee_name_title_fr.getText().trim();
		} else {
			lesseeNameTitle = we_lessee_name_title.getText().trim();
		}
			lesseeEnterpriseNumber = we_lessee_enterprisenumber.getText().trim();
			lesseeBillingAddress = we_lessee_billingaddress.getText().trim();
			lesseeContactPerson = we_lessee_contact_person.getText().trim();
			lesseeContactEmail = we_lessee_contact_email.getText().replace("email", "").trim();
			lesseeContactMobileNumber = we_lessee_contact_mobilenumber.getText().replace("smartphone", "").trim();
			lesseeContactPhoneNumber = we_lessee_contact_phonenumber.getText().replace("phone", "").trim();
			lesseeContactLanguage = we_lessee_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
			logger.debug("lesseeEnterpriseNumber: "+lesseeEnterpriseNumber+" lesseeBillingAddress: "+lesseeBillingAddress+
					" lesseeContactPerson: "+lesseeContactPerson+" lesseeContactEmail: "+lesseeContactEmail+
					" lesseeContactMobileNumber: "+lesseeContactMobileNumber+" lesseeContactPhoneNumber: "+lesseeContactPhoneNumber+
					" lesseeContactLanguage: "+lesseeContactLanguage);


		
		//Driver
		driverAddress = we_driver_address.getText().trim();
		driverContactPerson = we_driver_contact_person.getText().trim();
		driverContactEmail = we_driver_contact_email.getText().trim();
		driverContactMobileNumber = we_driver_contact_mobilenumber.getText().replace("smartphone", "").trim();
		driverContactPhoneNumber = we_driver_contact_phonenumber.getText().replace("phone", "").trim();
		driverContactLanguage = we_driver_contact_language.getText().replace("chat_bubble_outline", "").toUpperCase().trim();
		logger.debug("driverAddress: "+driverAddress+" driverContactPerson: "+driverContactPerson+
				" driverContactEmail: "+driverContactEmail+" driverContactMobileNumber: "+driverContactMobileNumber+
				" driverContactPhoneNumber: "+driverContactPhoneNumber+" driverContactLanguage: "+driverContactLanguage);
		
		//Vehicle information
		if(System.getProperty("language").equals("fr")) {
			vehicleDescriptionTitle = we_vehicle_description_title_fr.getText().trim();
		} else {
			vehicleDescriptionTitle = we_vehicle_description_title.getText().trim();
		}

		vehicleVIN = we_vehicle_vin.getText().trim();
		if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			vehicleVINControlNumber = null;
		} else {
			vehicleVINControlNumber = we_vehicle_vin_controlnumber.getText().trim();
		}
		vehicleModelYear = we_vehicle_modelyear.getText().trim();
		vehicleIdentificationCode = we_vehicle_identificationcode.getText().trim();
		vehicleLicensePlate = we_vehicle_licenseplate.getText().trim();
		vehicleStock = we_vehicle_stock.getText().trim();
		vehicleMileage = we_vehicle_mileage.getText().trim();
		vehicleLanguageDocuments = we_vehicle_language_documents.getText().trim();
		vehicleKeycode = we_vehicle_keycode.getText().trim();
		vehicleStartcode = we_vehicle_startcode.getText().trim();
		logger.debug("vehicleVIN: "+vehicleVIN+" vehicleVINControlNumber: "+vehicleVINControlNumber+
				" vehicleModelYear: "+vehicleModelYear+" vehicleIdentificationCode: "+vehicleIdentificationCode+
				" vehicleLicensePlate: "+vehicleLicensePlate+" vehicleStock: "+vehicleStock+
				" vehicleMileage: "+vehicleMileage+" vehicleLanguageDocuments:"+vehicleLanguageDocuments+
				" vehicleKeycode: "+vehicleKeycode+" vehicleStartcode: "+vehicleStartcode);

		
		//Technical specifications
		motorType = we_vehicle_motortype.getText().trim();
		power = we_vehicle_power.getText().trim();
		engineCapacity = we_vehicle_enginecapacity.getText().trim();
		nedcValue = we_vehicle_nedcvalue.getText().trim();
		wltpValue = we_vehicle_wltpvalue.getText().trim();
		interiorColor = we_vehicle_interiorcolor.getText().trim();
		tyreSpecifications = we_vehicle_tyrespecifications.getText().trim();
		//tyreBrand = we_vehicle_tyrebrand.getText().trim();
		tyreBrand = "Bridgestone";
		tyreType = we_vehicle_tyretype.getText().trim();
		weight = we_vehicle_weight.getText().trim();
		exteriorColor = we_vehicle_exteriorcolor.getText().trim();
		logger.debug("motorType: "+motorType+" power: "+power+
				" engineCapacity: "+engineCapacity+" nedcValue: "+nedcValue+
				" wltpValue: "+wltpValue+" interiorColor: "+interiorColor+
				" tyreSpecifications: "+tyreSpecifications+" tyreBrand:"+tyreBrand+
				" tyreType: "+tyreType+" weight: "+weight+" exteriorColor: "+exteriorColor);
		
		//Pricing
		vehiclePrice = we_pricing_vehicleprice.getText().trim();
		discount = we_pricing_discount.getText().trim();
		List<WebElement> elements = driver.findElements(By.xpath("//div[@data-testid='promotion-pricing-options-order-details-value']"));
		if(elements.size() > 0) {
			promotionDiscount = we_promotions_discount.getText().trim();
		} else {
			promotionDiscount = null;
		}
		elements = driver.findElements(By.xpath("//div[@data-testid='delivery-cost-pricing-options-order-details-value']"));
		if(elements.size() > 0) {
			deliveryCosts = we_pricing_deliverycosts.getText().trim();
		} else {
			deliveryCosts = null;
		}
		elements = driver.findElements(By.xpath("//div[@data-testid='other-cost-pricing-options-order-details-value']"));
		if(elements.size() > 0) {
			otherCosts = we_pricing_othercosts.getText().trim();
		} else {
			otherCosts = null;
		}
		elements = driver.findElements(By.xpath("//div[@data-testid='right-other-cost-pricing-options-order-details-value']"));
		if(elements.size() > 0) {
			otherCostsDiscount = we_pricing_othercosts_discount.getText().trim();
		} else {
			otherCostsDiscount = null;
		}
		


		totalPrice = we_pricing_totalprice.getText().trim();
		//totalPriceDiscount = we_pricing_totalprice_discount.getText().trim();
		logger.debug("vehiclePrice: "+vehiclePrice+" discount: "+discount+" deliveryCosts: "+deliveryCosts+
				" deliveryCostsDiscount: "+deliveryCostsDiscount+" otherCosts: "+otherCosts+" otherCostsDiscount: "+otherCostsDiscount+
				" totalPrice: "+totalPrice);
		
		
		//Delivery information
		desiredDeliveryDate = we_delivery_desireddeliverydate.getText().trim();
		expectedDeliveryDate = we_delivery_expecteddeliverydate.getText().trim();
		actualDeliveryDate = we_delivery_actualdeliverydate.getText().trim();
		firstRegistrationDate = we_delivery_firstregistrationdate.getText().trim(); 
		logger.debug("desiredDeliveryDate: "+desiredDeliveryDate+" expectedDeliveryDate: "+expectedDeliveryDate+
				" actualDeliveryDate: "+actualDeliveryDate+" firstRegistrationDate: "+firstRegistrationDate);
		
		//Different delivery address
		deliveryLocationNameUI = we_delivery_locationname.getText().trim();
		deliveryEnterpriseNumber = we_delivery_enterprisenumber.getText().trim();
		deliveryAddress = we_delivery_address.getText().trim();
		deliveryContactPersonUI = we_delivery_contact_person.getText().trim();
		deliveryContactEmailUI = we_delivery_contact_email.getText().replace("email", "").trim();
		deliveryContactMobileNumberUI = we_delivery_contact_mobilenumber.getText().replace("smartphone", "").trim();
		deliveryContactPhoneNumberUI = we_delivery_contact_phonenumber.getText().replace("phone", "").trim();
		deliveryContactLanguageUI = we_delivery_contact_language.getText().replace("chat_bubble_outline", "").trim();
		logger.debug("deliveryLocationNameUI: "+deliveryLocationNameUI+" deliveryEnterpriseNumber: "+deliveryEnterpriseNumber+
				" deliveryAddress: "+deliveryAddress+" deliveryContactPersonUI: "+deliveryContactPersonUI+
				" deliveryContactEmail: "+deliveryContactEmailUI+" deliveryContactMobileNumber: "+deliveryContactMobileNumberUI+
				" deliveryContactPhoneNumber: "+deliveryContactPhoneNumberUI+" deliveryContactLanguage:"+deliveryContactLanguageUI);
		
		//Returned vehicle information
		returnedVehicle = we_delivery_returnedvehicle.getAttribute("ng-reflect-value");
		registeredOnTheName = we_delivery_returnedvehicle_registeredonthename.getText().trim();
		returnedVehicleLicenseplate = we_delivery_returnedvehicle_licenseplate.getText().trim();
		logger.debug("returnedVehicle: "+returnedVehicle+" registeredOnTheName: "+registeredOnTheName+
				" returnedVehicleLicenseplate: "+returnedVehicleLicenseplate);
		
		//Equipment and enclosed documents
		registrationCertificate = we_delivery_documents_registrationcertificate.getAttribute("ng-reflect-value");
		insuranceCertificate = we_delivery_documents_insurancecertificate.getAttribute("ng-reflect-value");
		certificateOfConformity = we_delivery_documents_coc.getAttribute("ng-reflect-value");
		technicalInspectionCertificate = we_delivery_documents_technicalinspectioncertificate.getAttribute("ng-reflect-value");
		legalKit = we_delivery_equipment_legalkit.getAttribute("ng-reflect-value");
		serviceManual = we_delivery_documents_servicemanual.getAttribute("ng-reflect-value");
		fuelCard = we_delivery_documents_fuelcard.getAttribute("ng-reflect-value");
		numberOfKeys = we_delivery_equipment_numberofkeys.getText().trim();
		logger.debug("registrationCertificate: "+registrationCertificate+" insuranceCertificate: "+insuranceCertificate+
				" certificateOfConformity: "+certificateOfConformity+" technicalInspectionCertificate: "+technicalInspectionCertificate+
				" legalKit: "+legalKit+" serviceManual: "+serviceManual+
				" fuelCard: "+fuelCard+" numberOfKeys:"+numberOfKeys);
		
		//Driver information
		returnedVehicleDriverName = we_delivery_returnedvehicle_drivername.getText().trim();
		String returnedVehicleDriverDateOfBirthScreen = we_delivery_returnedvehicle_driverdateofbirth.getText().trim();
		if(returnedVehicleDriverDateOfBirthScreen.contains("―")) {

		} else {
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date date = format.parse(returnedVehicleDriverDateOfBirthScreen);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			returnedVehicleDriverDateOfBirth = dateFormat.format(date);
		}

		

		
		returnedVehicleDriverBirthplace = we_delivery_returnedvehicle_driverbirthplace.getText().trim();
		logger.debug("returnedVehicleDriverName: "+returnedVehicleDriverName+" returnedVehicleDriverDateOfBirth: "+returnedVehicleDriverDateOfBirth+
				" returnedVehicleDriverBirthplace: "+returnedVehicleDriverBirthplace);
		
		//Buyback
		buybackApplicable = we_buyback_applicable.getAttribute("ng-reflect-value");
		buybackName = we_buyback_name.getText().trim();
		buybackAddress = we_buyback_address.getText().trim();
		contractDuration = we_buyback_contractduration.getText().trim();
		totalMileage = we_buyback_totalmileage.getText().trim();
		buybackAmount = we_buyback_amount.getText().trim();
		logger.debug("buybackApplicable: "+buybackApplicable+" buybackName: "+buybackName+
				" buybackAddress: "+buybackAddress+" contractDuration: "+contractDuration+
				" totalMileage: "+totalMileage+" buybackAmount: "+buybackAmount);
		
	}

	public Logger getLogger() {
		return logger;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public String getLeasingCompanyDossierNumber() {
		return leasingCompanyDossierNumber;
	}

	public String getOrderNumberSupplier() {
		return orderNumberSupplier;
	}

	public String getCustomerNumberSupplier() {
		return CustomerNumberSupplier;
	}

	public String getAgentRsNumber() {
		return AgentRsNumber;
	}

	public String getAgentName() {
		return AgentName;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public String getLeasingCompanyName() {
		return leasingCompanyName;
	}

	public String getLeasingCompanyEnterpriseNumber() {
		return leasingCompanyEnterpriseNumber;
	}

	public String getLeasingCompanyRsNumber() {
		return leasingCompanyRsNumber;
	}

	public String getLeasingCompanybillingAddress() {
		return leasingCompanybillingAddress;
	}

	public String getLeasingCompanyCountry() {
		return leasingCompanyCountry;
	}

	public String getLeasingCompanyContactPerson() {
		return leasingCompanyContactPerson;
	}

	public String getLeasingCompanyContactEmail() {
		return leasingCompanyContactEmail;
	}

	public String getLeasingCompanyContactMobileNumber() {
		if(leasingCompanyContactMobileNumber.equals("―")) {
			return null;
		} else {
			return leasingCompanyContactMobileNumber;
		}
	}

	public String getLeasingCompanyContactPhoneNumber() {
		if(leasingCompanyContactPhoneNumber.equals("―")) {
			return null;
		} else {
			return leasingCompanyContactPhoneNumber;
		}
	}

	public String getLeasingCompanyContactLanguage() {
		return leasingCompanyContactLanguage;
	}

	public String getSupplierNameTitle() {
		return supplierNameTitle;
	}
	
	public String getSupplierName() {
		return supplierName;
	}

	public String getSupplierEnterpriseNumber() {
		return supplierEnterpriseNumber;
	}

	public String getSupplierRsNumber() {
		return supplierRsNumber;
	}

	public String getSupplierbillingAddress() {
		return supplierbillingAddress;
	}

	public String getSupplierCountry() {
		return supplierCountry;
	}

	public String getSupplierContactPerson() {
		return supplierContactPerson;
	}

	public String getSupplierContactEmail() {
		return supplierContactEmail;
	}

	public String getSupplierContactMobileNumber() {
		return supplierContactMobileNumber;
	}

	public String getSupplierContactPhoneNumber() {
		return supplierContactPhoneNumber;
	}

	public String getSupplierContactLanguage() {
		return supplierContactLanguage;
	}

	public String getLesseeNameTitle() {
		return lesseeNameTitle;
	}
	
	public String getLesseeEnterpriseNumber() {
		return lesseeEnterpriseNumber;
	}

	public String getLesseeBillingAddress() {
		return lesseeBillingAddress;
	}

	public String getLesseeContactPerson() {
		return lesseeContactPerson;
	}

	public String getLesseeContactEmail() {
		return lesseeContactEmail;
	}

	public String getLesseeContactMobileNumber() {
		return lesseeContactMobileNumber;
	}

	public String getLesseeContactPhoneNumber() {
		return lesseeContactPhoneNumber;
	}

	public String getLesseeContactLanguage() {
		return lesseeContactLanguage;
	}

	public String getDriverAddress() {
		return driverAddress;
	}

	public String getDriverContactPerson() {
		return driverContactPerson;
	}

	public String getDriverContactEmail() {
		return driverContactEmail;
	}

	public String getDriverContactMobileNumber() {
		return driverContactMobileNumber;
	}

	public String getDriverContactPhoneNumber() {
		return driverContactPhoneNumber;
	}

	public String getDriverContactLanguage() {
		return driverContactLanguage;
	}

	public String getVehicleDescriptionTitle() {
		return vehicleDescriptionTitle;
	}
	
	public String getVehicleVIN() {
		return vehicleVIN;
	}

	public String getVehicleVINControlNumber() {
		return vehicleVINControlNumber;
	}

	public String getVehicleModelYear() {
		return vehicleModelYear;
	}

	public String getVehicleIdentificationCode() {
		return vehicleIdentificationCode;
	}

	public String getVehicleLicensePlate() {
		return vehicleLicensePlate;
	}

	public String getVehicleStock() {
		if(vehicleStock.equals("Yes")) {
			vehicleStock = "true";
		}
		return vehicleStock;
	}

	public String getVehicleMileage() {
		return vehicleMileage;
	}

	public String getVehicleLanguageDocuments() {
		return vehicleLanguageDocuments;
	}

	public String getVehicleKeycode() {
		return vehicleKeycode;
	}

	public String getVehicleStartcode() {
		return vehicleStartcode;
	}

	public String getMotorType() {
		return motorType;
	}

	public String getPower() {
		return power;
	}

	public String getEngineCapacity() {
		return engineCapacity;
	}

	public String getNedcValue() {
		return nedcValue;
	}

	public String getWltpValue() {
		return wltpValue;
	}

	public String getInteriorColor() {
		return interiorColor;
	}

	public String getTyreSpecifications() {
		return tyreSpecifications;
	}

	public String getTyreBrand() {
		return tyreBrand;
	}

	public String getTyreType() {
		return tyreType;
	}

	public String getWeight() {
		return weight;
	}

	public String getExteriorColor() {
		return exteriorColor;
	}

	public String getVehiclePrice() {
		return vehiclePrice;
	}

	public String getDiscount() {
		return discount;
	}
	
	public String getPromotionDiscount() {
		return promotionDiscount;
	}

	public String getDeliveryCosts() {
		return deliveryCosts;
	}

	public String getDeliveryCostsDiscount() {
		return deliveryCostsDiscount;
	}

	public String getOtherCosts() {
		return otherCosts;
	}

	public String getOtherCostsDiscount() {
		return otherCostsDiscount;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public String getTotalPriceDiscount() {
		return totalPriceDiscount;
	}

	public String getDesiredDeliveryDate() {
		return desiredDeliveryDate;
	}

	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public String getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public String getFirstRegistrationDate() {
		return firstRegistrationDate;
	}

	public String getDeliveryLocationNameUI() {
		return deliveryLocationNameUI;
	}

	public String getDeliveryEnterpriseNumber() {
		return deliveryEnterpriseNumber;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public String getDeliveryContactPersonUI() {
		return deliveryContactPersonUI;
	}

	public String getDeliveryContactEmailUI() {
		return deliveryContactEmailUI;
	}

	public String getDeliveryContactMobileNumberUI() {
		return deliveryContactMobileNumberUI;
	}

	public String getDeliveryContactPhoneNumberUI() {
		return deliveryContactPhoneNumberUI;
	}

	public String getDeliveryContactLanguageUI() {
		return deliveryContactLanguageUI;
	}

	public String getReturnedVehicle() {
		return returnedVehicle;
	}

	public String getRegisteredOnTheName() {
		return registeredOnTheName;
	}

	public String getReturnedVehicleLicenseplate() {
		return returnedVehicleLicenseplate;
	}

	public String getRegistrationCertificate() {
		return registrationCertificate;
	}

	public String getInsuranceCertificate() {
		return insuranceCertificate;
	}

	public String getCertificateOfConformity() {
		return certificateOfConformity;
	}

	public String getTechnicalInspectionCertificate() {
		return technicalInspectionCertificate;
	}

	public String getLegalKit() {
		return legalKit;
	}

	public String getServiceManual() {
		return serviceManual;
	}

	public String getFuelCard() {
		return fuelCard;
	}

	public String getNumberOfKeys() {
		return numberOfKeys;
	}

	public String getReturnedVehicleDriverName() {
		return returnedVehicleDriverName;
	}

	public String getReturnedVehicleDriverDateOfBirth() {
		return returnedVehicleDriverDateOfBirth;
	}

	public String getReturnedVehicleDriverBirthplace() {
		return returnedVehicleDriverBirthplace;
	}

	public String getBuybackApplicable() {
		return buybackApplicable;
	}

	public String getBuybackName() {
		return buybackName;
	}

	public String getBuybackAddress() {
		return buybackAddress;
	}

	public String getContractDuration() {
		return contractDuration;
	}

	public String getTotalMileage() {
		return totalMileage;
	}

	public String getBuybackAmount() {
		return buybackAmount;
	}

}