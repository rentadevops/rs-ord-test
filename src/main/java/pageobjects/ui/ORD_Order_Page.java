package pageobjects.ui;

import java.awt.image.BufferedImage;
import java.io.*;
//import java.math.BigInteger;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
//import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;


//import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.script.ScriptException;

//import com.google.common.base.Converter;
//import com.fasterxml.jackson.databind.util.Converter;
//import com.google.gson.Gson;
import apiEngine.Endpoints;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
//import org.apache.batik.transcoder.TranscoderException;
//import org.apache.batik.transcoder.TranscoderInput;
//import org.apache.batik.transcoder.TranscoderOutput;
//import org.apache.batik.transcoder.image.JPEGTranscoder;
//import com.itextpdf.kernel.exceptions.PdfException;
import io.cucumber.datatable.DataTable;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import managers.WebDriverManager_ORD;
import net.datafaker.Faker;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.*;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.pdfbox.cos.COSDocument;
//import org.apache.pdfbox.cos.COSName;
//import org.apache.pdfbox.io.RandomAccessRead;
//import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.PDDocument;
//import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.preflight.PreflightDocument;
import org.apache.pdfbox.preflight.ValidationResult;
//import org.apache.pdfbox.preflight.exception.ValidationException;
//import org.apache.pdfbox.preflight.parser.PreflightParser;
//import org.apache.pdfbox.preflight.Format;
//import org.apache.pdfbox.preflight.PreflightConfiguration;
//import org.apache.pdfbox.preflight.PreflightConstants;
//import org.apache.pdfbox.preflight.PreflightContext;
//import org.apache.pdfbox.preflight.ValidationResult.ValidationError;
//import org.apache.pdfbox.preflight.exception.SyntaxValidationException;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.junit.Assert;
import org.openqa.selenium.*;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.devtools.DevTools;
//import org.openqa.selenium.devtools.v85.network.Network;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.apache.commons.codec.binary.Base64;

import common.DropFile;
import cucumber.TestContext;
import helpers.*;
//import org.w3c.dom.Element;
import pageobjects.api.API_GetOrderByDossierNumber;
import pageobjects.ftp.FTP_PostOrderImportsRequest;
import pageobjects.ui.Validate_Pdf;



import static com.github.romankh3.image.comparison.model.ImageComparisonState.MATCH;
import static com.github.romankh3.image.comparison.model.ImageComparisonState.MISMATCH;
import static org.apache.pdfbox.pdfparser.PDFParser.load;


public class ORD_Order_Page {
	final Logger logger = LogManager.getLogger(ORD_Order_Page.class);
	Faker faker = new Faker(new Locale("nl_BE"));

	ORD_Topbar topBar;
	TestContext testContext;

	private String dealerDossierNr;
	private String VIN = "00000000071100083";
	private String VINControlNumber = "441";
	private String LicensePlate;
	private String taxStampCosts = "1000.01";
	private String inscriptionCosts = "9991.91";
	private String proposedDeliveryTime = "1259";
	private String maxWeight = "4545";
	private String engineDisplacement = "1999";
	private String power = "194";
	private String fuelType = "DIESEL";
	private String NEDCInput = "6666";
	private String NEDC;
	private String WLTPInput = "3333";
	private String WLTPInputLux = "5556";
	private String WLTP;
	private String WLTPLux;
	private String modelYear = "2020";
	private String mileage = "55";
	private String numberOfKeys = "2";
	private String keycode = "123ABC321";
	private String startcode = "999GGG999";
	private String width = "175";
	private String relationWidthHeight = "80";
	private String rimDiameter = "14.99";
	private String speedIndex = "T";
	private String loadIndex = "88";
	private String tyreSpecs;
	private String tyreBrand = "Bridgestone99";
	private String tyreType = "SUMMER";
	private String licensePlateReturnedVehicle = "1FFF333";
	private String dateFirstRegistrationYear;
	private String dateFirstRegistrationMonth;
	private String dateFirstRegistrationDay;
	private String dateFirstRegistration;
	private String dateFirstRegistrationLux;
	private String returnedVehicle = "true";
	private String returnedVehicleLux = "false";
	//private String ownerReturnedVehicle = "ALD Automotive";
	private String ownerReturnedVehicle = "ALD Automotive";


	private String selectRegistrationCertificate = "true";
	private String selectInsuranceCertificate = "false";
	private String selectCertificateOfConformity = "true";
	private String selectTechnicalInspectionCertificate = "false";
	private String selectLegalKit = "false";
	private String selectServiceManual = "false";
	private String selectFuelCard = "false";
	private String driverFirstName = "Dirk";
	private String driverLastName = "River";
	private String driverName = driverFirstName + " " + driverLastName;
	private String driverBirthPlace = "UNKNOWN";
	//private String driverDateOfBirth = "1990-07-13";
	private String driverDateOfBirth = "19/06/1965";
	private String evbNumber = faker.bothify("??????????????????????????????????-#####");
	private String cardNumber = faker.bothify("??????????????????????????????????-#####");

	//PDF-variables
	String title;
	String leasingCompanyDossierNumber;
	String dataClient;
	String dataDriver;
	String clientNameLabel;
	String clientStreetLabel;
	String clientPostalCodeLabel;
	String clientName;
	String clientStreet;
	String driverNameLabel;
	String driverStreetLabel;
	String driverPostalCodeLabel;
	String driverMobileLabel;
	String driverEmailLabel;
	String driverNamePdf;
	String driverStreet;
	String driverPostalCode;
	String driverMobile;
	String driverEmail;
	String dataVehicle;
	String vehicleBrandLabel;
	String vehicleModelLabel;
	String vehicleVersionLabel;
	String vehicleExtColorLabel;
	String vehicleIntColorLabel;
	String vehicleTyresLabel;
	String vehicleVinLabel;
	String vehicleLicensePlateLabel;
	String vehicleModel;
	String vehicleVersion;
	String vehicleExtColor;
	String vehicleVin;
	String vehicleLicensePlate;
	String extraProofOfRegistration;
	String extraProofOfInsurance;
	String extraCertificateOfConformity;
	String extraProofOfRoadworthiness;
	String extraLegalKit;
	String extraManual;
	String extraFuelCard;
	String extraNumberOfKeysLabel;
	String extraNumberOfKeys;
	String dataPreviousVehicle;
	String previousVehicleLicensePlateLabel;
	String previousVehicleOwnerLabel;
	String previousVehicleLicensePlate;
	String previousVehicleOwner;
	String dataSignature;
	String vehicleBrand;
	String extraProofOfRegistrationLabel;
	String extraProofOfInsuranceLabel;
	String extraCertificateOfConformityLabel;
	String extraProofOfRoadworthinessLabel;
	String extraLegalKitLabel;
	String extraManualLabel;
	String extraFuelCardLabel;
	String vehicleIntColor;
	String vehicleTyres;
	String previousVehicle;
	String dataDeliveryLocation;
	String deliveryLocationNameLabel;
	String deliveryLocationStreetLabel;
	String deliveryLocationPostalCodeLabel;
	String deliveryLocationName;
	String deliveryLocationStreet;
	String deliveryLocationPostalCode;
	String dataExtra;
	String clientCityLabel;
	String clientPostalCode;
	String clientCity;
	String driverCityLabel;
	String driverCity;
	String deliveryLocationCityLabel;
	String deliveryLocationCity;
	String signatureDescriptionLine1;
	String signatureDescriptionLine2;
	String financialLeaseSignatureDisclaimer;
	String dummyFieldName1;

	PreflightDocument document = null;
	DateFormat sdf;

	WebDriver driver;
	//private static ChromeDriver driver;
	//private static DevTools chromeDevTools;
	FTP_PostOrderImportsRequest postOrderImportsRequest;
	//API_GetOrderByDossierNumber getOrderByDossierNumber;
	//Validate_Pdf validatePdf = new Validate_Pdf(testContext);
	Validate_Pdf validate_Pdf;

	//ValidateDeliveryReceiptPdf validateDigitalDeliveryReceiptPdf;
	//Validate_Digital_Delivery_Receipt_Pdf validateDeliveryReceiptPdf;

	DropFile df;




	//region Webelements
	@FindBy(how = How.XPATH, using = "(//span[contains(.,'Overview')])[1]")
	private WebElement overview;

	@FindBy(how = How.XPATH, using = "(//span[contains(.,'Résumé')])[1]")
	private WebElement overview_fr;

	@FindBy(how = How.XPATH, using = "(//span[contains(.,'Details')])[1]")
	private WebElement details;

	@FindBy(how = How.XPATH, using = "(//span[contains(.,'Détails')])[1]")
	private WebElement details_fr;

	@FindBy(how = How.XPATH, using = "(//button[@id='messages-order-details-subheader'])[1]")
	private WebElement messages;

	@FindBy(how = How.XPATH, using = "(//button[@id='documents-order-details-subheader'])[1]")
	private WebElement documents;

	@FindBy(how = How.XPATH, using = "(//button[@data-testid='btn-download-document'])[1]")
	private WebElement btn_download_receipt;

	@FindBy(how = How.XPATH, using = "(//button[@id='ev-products-messages-section-subheader'])[1]")
	private WebElement messages_wb;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='comment']")
	private WebElement input_comment;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='send-comment-order-messages-button']")
	private WebElement btn_send_comment;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='done-all-comment-order-messages-button']")
	private WebElement btn_done_all;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='see-more-order-timeline-button']")
	private WebElement btn_see_more;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='slide-order-timeline-toggle']")
	private WebElement slide_toggle_order_timeline;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='expected-delivery-date-step-one-field']")
	private WebElement select_date_picker_expected_delivery_date;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@formcontrolname='expectedDeliveryDate']")
	private WebElement select_date_picker_expected_delivery_date_ordered;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@data-testid='terms-and-conditions-step-one-checkbox']")
	private WebElement checkbox_accept_terms_conditions;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@data-testid='different-conditions-step-one-checkbox']")
	private WebElement checkbox_different_conditions;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='change-needed-step-one-button']")
	private WebElement btn_change_needed;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='accept-order-step-one-button']")
	private WebElement btn_accept_order;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='step1-accept-order-btn']")
	private WebElement btn_accept_order_wb;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@data-testid='check-confirm-order-step-two-checkbox']")
	private WebElement checkbox_confirm_order;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='confirm-order-step-two-button']")
	private WebElement btn_confirm_order;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='step2-confirm-order-button']")
	private WebElement btn_confirm_order_wb;

	@FindBy(how = How.XPATH, using = "//mat-radio-button[@data-testid='update-delivery-date-step-three-radio-button']")
	private WebElement radio_vehicle_not_arrived;

	@FindBy(how = How.XPATH, using = "//mat-radio-button[@data-testid='vehicle-at-supplier-step-three-radio-button']")
	private WebElement radio_vehicle_arrived;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='next-vehicle-arrived-step-three-button']")
	private WebElement btn_next;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='back-ordered-step-three-button']")
	private WebElement btn_back_after_vin;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='next-ordered-step-three-button']")
	private WebElement btn_next_after_vin;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='vin-step-three-field']//input")
	private WebElement input_vinnumber;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-vin-field']//input")
	private WebElement input_vinnumber_lux;

	//@FindBy(how = How.XPATH, using = "//input[@formcontrolname='vinNumber']")
	//private WebElement input_vinnumber;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='vin-control-number-step-three-field']//input")
	private WebElement input_vin_control_number;

	//@FindBy(how = How.XPATH, using = "//input[@formcontrolname='vinControlNumber']")
	//private WebElement input_vin_control_number;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='back-vehicle-at-supplier-step-three-button']")
	private WebElement btn_back_vehicle_at_supplier;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='vehicle-at-supplier-step-three-button']")
	private WebElement btn_vehicle_at_supplier;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='license-plate-step-four-field']//input")
	private WebElement input_license_plate;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='licensePlate']")
	private WebElement input_license_plate_fl;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='licensePlate']")
	private WebElement input_license_plate_lux;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='first-registration-date-step-four-field']")
	private WebElement select_date_picker_date_first_registration;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='dateFirstRegistration']")
	private WebElement select_date_picker_date_first_registration_fl;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Choose month and year']")
	private WebElement select_choose_month_and_year;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='maxWeight']")
	private WebElement input_max_weight_fl;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='engineDisplacement']")
	private WebElement input_engine_displacement_fl;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='power']")
	private WebElement input_power_fl;

	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='fuelType']")
	private WebElement select_fuel_type_fl;

	@FindBy(how = How.XPATH, using = "//mat-option[@ng-reflect-value='DIESEL']")
	private WebElement option_fuel_type_diesel_fl;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='co2EmissionsNedc']")
	private WebElement input_NEDC_fl;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='co2EmissionsNedc']")
	private WebElement input_NEDC_lux;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='co2EmissionsWltp']")
	private WebElement input_WLTP_fl;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-wltp-value-field']//input")
	private WebElement input_WLTP_lux;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='wltp-value-step-four-field']//input")
	private WebElement input_WLTP;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='nedc-value-step-four-field']//input")
	private WebElement input_NEDC;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='model-year-step-four-field']//input")
	private WebElement input_model_year;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='postpone-step-four-button']")
	private WebElement btn_postpone;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='ready-for-delivery-step-four-button']")
	private WebElement btn_ready_for_delivery;

	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Processed successfully')]")
	private WebElement message_processed_successfully;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='language-delivery-receipt-step-five-field']")
	private WebElement select_language_delivery_receipt;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'Generate delivery')]/parent::button")
	private WebElement btn_generate_delivery_receipt;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='next-deliver-vehicle-step-five-button']")
	private WebElement btn_deliver_vehicle;

	@FindBy(how = How.XPATH, using = "(//button[contains(.,'Prepare delivery')])[1]")
	private WebElement btn_prepare_delivery;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='prepare-delivery-vehicle-step-five-button']")
	private WebElement btn_prepare_delivery_fr;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='prepare-delivery-step-five-button']")
	private WebElement btn_prepare_digital_delivery;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle")
	private WebElement toggle_digital_delivery;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='deliver-vehicle-step-five-button']")
	private WebElement btn_deliver_vehicle_after_file_upload;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='deliver-vehicle-step-five-back-button']")
	private WebElement btn_back_deliver_vehicle_after_file_upload;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='order-commission-delivery-info-step-five-field']//input")
	private WebElement input_dealer_dossier_number;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='actual-date-delivery-info-step-five-field']//input")
	private WebElement select_date_picker_actual_delivery_date;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='first-registration-date-delivery-info-step-five-field']//input")
	private WebElement select_date_picker_date_first_registration_delivery;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='model-year-delivery-info-step-five-field']//input")
	private WebElement input_model_year_delivery;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='mileage-delivery-info-step-five-field']//input")
	private WebElement input_mileage;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='number-keys-delivery-info-step-five-field']//input")
	private WebElement input_number_of_keys;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='keycode-delivery-info-step-five-field']//input")
	private WebElement input_keycode;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='startcode-delivery-info-step-five-field']//input")
	private WebElement input_startcode;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='tyre-spec-entered-step-five-pencil-button']")
	private WebElement icon_edit_tyre_specifications;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='step3-tyre-description-field']")
	private WebElement input_tyre_specifications;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-tyre-description-field']//input")
	private WebElement input_tyre_specifications_lux;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='width-tyre-spec-step-five-field']//input")
	private WebElement input_width;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='aspect-ratio-tyre-spec-step-five-field']//input")
	private WebElement input_relation_w_h;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='rim-diameter-tyre-spec-step-five-field']//input")
	private WebElement input_rim_diameter;

	@FindBy(how = How.XPATH, using = "//mat-select[@data-testid='speed-index-tyre-spec-step-five-select']")
	private WebElement select_speed_index;

	@FindBy(how = How.XPATH, using = "//mat-select[@data-testid='load-index-tyre-spec-step-five-select']")
	private WebElement select_load_index;

	@FindBy(how = How.XPATH, using = "//mat-select[@data-testid='tyre-type-step-five-select']")
	private WebElement select_tyre_type;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'Summer')]/parent::mat-option")
	private WebElement select_tyre_type_summer;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'Eté')]/parent::mat-option")
	private WebElement select_tyre_type_summer_fr;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='back-tyre-spec-step-five-button']")
	private WebElement btn_cancel_tyre_specifications;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='tyre-brand-step-five-field']//input")
	private WebElement input_tyre_brand;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='returned-vehicle-step-five-tgl']")
	private WebElement toggle_returned_vehicle;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='registered-name-of-step-five-field']")
	private WebElement select_owner_returned_vehicle;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='license-plate-tyre-spec-step-five-field']//input")
	private WebElement input_license_plate_returned_vehicle;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='next-tyre-spec-entered-step-five-button']")
	private WebElement btn_next_after_delivery_information;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='next-to-additional-info-step-five-button']")
	private WebElement btn_next_to_additional_information;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='next-tyre-spec-entered-step-five-button']")
	private WebElement btn_next_tyre_spec_entered_step_five_button;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='back-tyre-spec-entered-step-five-button']")
	private WebElement btn_back_after_delivery_information;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'Renta Solutions')]/parent::mat-option")
	private WebElement option_registered_on_name_of_renta;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='registration-certificate-step-five-toggle']//input")
	private WebElement toggle_registration_certificate;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='insurance-certificate-step-five-toggle']//input")
	private WebElement toggle_insurance_certificate;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='conformity-certificate-step-five-toggle']//input")
	private WebElement toggle_certificate_of_conformity;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='technical-inspection-certificate-step-five-toggle']//input")
	private WebElement toggle_technical_inspection_certificate;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='legal-kit-step-five-toggle']//input")
	private WebElement toggle_legal_kit;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='service-manual-step-five-toggle']//input")
	private WebElement toggle_service_manual;

	@FindBy(how = How.XPATH, using = "//mat-slide-toggle[@data-testid='fuel-card-step-five-toggle']//input")
	private WebElement toggle_fuel_card;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='first-name-step-five-field']//input")
	private WebElement input_driver_first_name;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='last-name-step-five-field']//input")
	private WebElement input_driver_last_name;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='birth-date-step-five-field']")
	private WebElement select_driver_birthday;

	@FindBy(how = How.XPATH, using = "//button[contains(@aria-label,'1990')]")
	private WebElement select_date_picker_birthDate_Year_1990;

	@FindBy(how = How.XPATH, using = "//button[contains(@aria-label,'1965')]")
	private WebElement select_date_picker_birthDate_Year_1965;

	@FindBy(how = How.XPATH, using = "//button[contains(@aria-label,'2000')]")
	private WebElement select_date_picker_birthDate_Year_2000;

	@FindBy(how = How.XPATH, using = "//div[contains(text(), 'JUL')]")
	private WebElement select_date_picker_birthDate_Month_JUL;

	@FindBy(how = How.XPATH, using = "//div[contains(text(), 'JUN')]")
	private WebElement select_date_picker_birthDate_Month_JUN;

	@FindBy(how = How.XPATH, using = "//div[text() = ' 13 ']")
	private WebElement select_date_picker_birthDate_Day_13;

	@FindBy(how = How.XPATH, using = "//div[text() = ' 19 ']")
	private WebElement select_date_picker_birthDate_Day_19;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='birth-place-step-five-field']//input")
	private WebElement input_driver_birthplace;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='next-driver-information-step-five-button']")
	private WebElement btn_next_after_additional_information;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='back-driver-information-step-five-button']")
	private WebElement btn_back_after_additional_information;

	@FindBy(how = How.XPATH, using = "(//input[contains(@class,'file-input')])[1]")
	private WebElement dropzone;

	@FindBy(how = How.XPATH, using = "(//input[contains(@class,'file-input')])[2]")
	private WebElement dropzone_2;

	@FindBy(how = How.XPATH, using = "(//input[contains(@class,'file-input')])[3]")
	private WebElement dropzone_3;

	@FindBy(how = How.XPATH, using = "(//input[contains(@class,'file-input')])[4]")
	private WebElement dropzone_4;

	@FindBy(how = How.XPATH, using = "//div[@data-testid='action-needed-order-timeline-snackbar']//span")
	private WebElement msg_action_needed;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-proposed-delivery-date-field']")
	private WebElement select_date_picker_proposed_delivery_date;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-proposed-delivery-time-field']")
	private WebElement select_proposed_delivery_time;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-proposed-delivery-time-field']//input")
	private WebElement input_proposed_delivery_time;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='proposedDeliveryTime']")
	private WebElement input_proposed_delivery_time_lux;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-first-registration-date-field']")
	private WebElement select_date_picker_first_registration_date_lux;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-tax-stamp-field']//input")
	private WebElement input_tax_stamp_costs;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-inscription-field']//input")
	private WebElement input_inscription_costs;

	@FindBy(how = How.XPATH, using = "//mat-form-field[@data-testid='step3-delivery-contact-field']//input")
	private WebElement input_delivery_contact;

	@FindBy(how = How.XPATH, using = "//mat-checkbox[@data-testid='step3-plate-type-checkbox']")
	private WebElement checkbox_plate_type_specified;

	@FindBy(how = How.XPATH, using = "//mat-select[@formcontrolname='licensePlateType']")
	private WebElement listbox_plate_type;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='expectedInstallationDate']")
	private WebElement input_expected_installation_date;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='evbNumber']")
	private WebElement input_evb_number;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='chargeCardNumber']")
	private WebElement input_charge_card_number;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='installationDate']")
	private WebElement select_datepicker_installation_date;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='certificationDate']")
	private WebElement select_datepicker_certification_date;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='step3-save-draft-submit-btn']")
	private WebElement btn_save_draft;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='step3-wall-box-installed-submit-btn']")
	private WebElement btn_installed;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='step4-ready-to-activate-btn']")
	private WebElement btn_ready_to_activate;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='activationDate']")
	private WebElement select_datepicker_activation_date;

	@FindBy(how = How.XPATH, using = "//button[@data-testid='step5-activate-btn']")
	private WebElement btn_activate;

	@FindBy(how = How.XPATH, using = "//h2[contains(.,'Warning')]")
	private WebElement popup_warning_unread_messages;

	@FindBy(how = How.XPATH, using = "//button[@mat-dialog-close='mark-comments-as-read']")
	private WebElement button_unread_messages_yes;

	@FindBy(how = How.XPATH, using = "//img[@alt='QR Code']")
	private WebElement qr_code_digital_delivery;

	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='formNumber']")
	private WebElement input_form_number;

	@FindBy(how = How.XPATH, using = "//span[@class = 'mat-slide-toggle-bar']/input")
	private WebElement toggle_digi_delivery;

	@FindBy(how = How.XPATH, using = "//a[@class='introjs-skipbutton']")
	private WebElement popup_skip;


//endregion Webelements
		WebDriverWait wait;

	public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{
		TakesScreenshot scrShot =((TakesScreenshot)webdriver);

		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

		File DestFile=new File(fileWithPath);

		FileUtils.copyFile(SrcFile, DestFile);

	}


	public ORD_Order_Page(WebDriver driver) throws IOException, InterruptedException {
		this.driver = driver;
		logger.debug("drijvert: " + driver);
		//WebDriverManager_ORD wdmo = null;
		//driver = wdmo.getDriver();
		//logger.debug("drijvert 2: " + driver);

		PageFactory.initElements(driver, this);
		ORD_LicensplateNumberGenerator lng = new ORD_LicensplateNumberGenerator();
		FTP_PostOrderImportsRequest postOrderImportsRequest = new FTP_PostOrderImportsRequest(driver);
		Validate_Pdf validatePdf;
		LicensePlate = lng.getRandomLicenseplateNumber().replace("-", "");
		logger.debug("LicensePlate: " + LicensePlate);
		Properties p = new Properties();
		p.put("LicensePlate", LicensePlate);
		FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/licenseplate.properties");
		p.store(fos, "");
		logger.debug("LicensePlate " + LicensePlate +  " naar proprty file geschreven");
		//String csvFilename = "./src/test/resources/runtime/TestInfo.csv";
		//FileWriter fw = new FileWriter(csvFilename,true);
		//fw.append(LicensePlate);
		//fw.append("\n");
		//fw.close();

		wait = new WebDriverWait(driver, Duration.ofMillis(20000));
	}

	public enum PlateType {
		Transferred("Transferred license plate"),
		Special("Special license plate"),
		Normal("Normal license plate");

		private final String plateType;

		PlateType(String plateType) {
			this.plateType = plateType;
		}

		public String getPlateType() {
			return this.plateType;
		}
	}

	public void mark_messages_as_read() throws Exception {

		//driver = new ChromeDriver();
		//chromeDevTools = driver.getDevTools();
		//chromeDevTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
		//chromeDevTools.addListener(Network.requestWillBeSent(),
		//		entry -> {
		//			System.out.println("Request URI : " + entry.getRequest().getUrl()+"\n"
		//					+ " With method : "+entry.getRequest().getMethod() + "\n");
		//			entry.getRequest().getMethod();
		//		};
		logger.debug("Start mark_messages_as_read");
		Thread.sleep(2000);
		if (System.getProperty("BrowserName").equals("firefox")) {
			//wait.until(ExpectedConditions.elementToBeClickable(btn_done_all));
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(btn_done_all));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_done_all);
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_done_all);
		} else {
			//wait.until(ExpectedConditions.elementToBeClickable(btn_done_all));
			Thread.sleep(2000);
			//this.takeSnapShot(driver, "c://ordimages//beforeClickAllMessagesRead.png") ;
			this.takeSnapShot(driver, "./src/test/resources/runtime/ordImages/beforeClickAllMessagesRead.jpg") ;
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_done_all);
			//btn_done_all.click();
			//this.takeSnapShot(driver, "c://ordimages//afterClickAllMessagesRead.png") ;
			this.takeSnapShot(driver, "./src/test/resources/runtime/ordImages/afterClickAllMessagesRead.jpg") ;
		}
		logger.debug("End mark_messages_as_read");
		Thread.sleep(2000);

		//chromeDevTools.send(Network.disable());
	}

	public void open_overview() throws Exception {
		if(System.getProperty("language").equals("fr")) {
			wait.until(ExpectedConditions.elementToBeClickable(overview_fr));
			if (System.getProperty("BrowserName").equals("firefox")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", overview_fr);
				wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
			} else {
				overview_fr.click();
			}

		} else {
			wait.until(ExpectedConditions.elementToBeClickable(overview));
			if (System.getProperty("BrowserName").equals("firefox")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", overview);
				wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
			} else {
				this.takeSnapShot(driver, "./src/test/resources/runtime/ordImages/beforeOpenOverwiew.jpg") ;
				//overview.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", overview);
				this.takeSnapShot(driver, "./src/test/resources/runtime/ordImages/afterOpenOverwiew.jpg") ;
			}
		}
		/*wait.until(ExpectedConditions.elementToBeClickable(overview));
		if (System.getProperty("BrowserName").equals("firefox")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", overview);
			wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
		} else {
			overview.click();
		}*/
		try {
			logger.debug("Try inread messages");
			Thread.sleep(2000);
			if (popup_warning_unread_messages.isDisplayed()) {
				logger.debug("Wachten op warning messages");
				//button_unread_messages_yes.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", button_unread_messages_yes);
				logger.debug("Einde if in try catch");

			}
		}
		catch(Exception e) {
			logger.debug("Catch warning: Warning not found");
			}
		Thread.sleep(1000);
	}

	public void open_details() throws InterruptedException, IOException {
		if(System.getProperty("language").equals("fr")) {
			Thread.sleep(5000);
			Actions actions = new Actions(driver);
			wait.until(ExpectedConditions.elementToBeClickable(details_fr));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", details_fr);
			if (System.getProperty("BrowserName").equals("firefox")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", details_fr);
				wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
			} else {
				actions.moveToElement(details_fr);
				Thread.sleep(2000);
				//details.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", details_fr);
			}
		} else {
			Thread.sleep(5000);
			Actions actions = new Actions(driver);
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
			wait.until(ExpectedConditions.elementToBeClickable(details));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", details);
			if (System.getProperty("BrowserName").equals("firefox")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", details);
				wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
			} else {
				actions.moveToElement(details);
				Thread.sleep(2000);
				//details.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", details);
			}
		}
		try {
			if (popup_warning_unread_messages.isDisplayed()) {
				button_unread_messages_yes.click();

			}
		}
		catch(Exception e) {
			logger.debug("Warning not found");
		}
		Thread.sleep(1000);

	}

	public void open_messages() throws InterruptedException, IOException {
		logger.debug("Start open_messages");
		Thread.sleep(3000);
		if(System.getProperty("leasingType").equals("WALLBOX")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", messages_wb);
			//messages.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", messages_wb);
			Thread.sleep(1000);
		} else {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", messages);
			//messages.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", messages);
			Thread.sleep(1000);
		}
		logger.debug("End open_messages");
	}

	public void open_documents() throws InterruptedException, IOException {
		Thread.sleep(3000);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", documents);
			//messages.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", documents);
			Thread.sleep(1000);

	}

	public void download_documents() throws InterruptedException, IOException {
		Thread.sleep(3000);
		File fileDir = new File("./src/test/resources/runtime/Pdf");
		FileUtils.cleanDirectory(fileDir);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_download_receipt);
		//messages.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_download_receipt);
		Thread.sleep(1000);

	}

	public PDDocument open_pdf() throws InterruptedException, IOException, ParseException {
		File folder = new File("./src/test/resources/runtime/Pdf");
		File[] listOfFiles = folder.listFiles();

		Long fileLast = 0L;
		File fileDownload = null;
		ValidationResult result = null;
		//PreflightDocument document = null;
		URL fileURL = null;
		for (File file : listOfFiles) {
			if (file.isFile()) {
				logger.debug("FileName: " + file.getName());

				String fileName = file.getName();
				String ext = fileName.substring(fileName.length() - 4);
				logger.debug("file extention: " + ext);
				String fileTemp = file.getName().replace("delivery-receipt-", "");
				String fileTemp1 = fileTemp.replace("-", "");
				String fileTemp2 = fileTemp1.replace("(", "");
				String fileTemp4 = fileTemp2.replace(")", "").trim();
				String fileTemp5 = fileTemp4.replace(" ", "").trim();
				String fileTemp6 = null;
				if(ext.equals(".pdf")) {
					fileTemp6 = fileTemp5.replace(".pdf", "");
				} else if(ext.equals(".jpg")) {
					fileTemp6 = fileTemp5.replace(".jpg", "");
				} else if(ext.equals(".png")) {
					fileTemp6 = fileTemp5.replace(".png", "");
				}
				//fileTemp6 = fileTemp5.replace(".jpg", "");
				//fileTemp6 = fileTemp5.replace(".svg", "");
				Long fileNr = Long.parseLong(fileTemp6);
				if(fileNr > fileLast) {
					fileLast = fileNr;
					fileDownload = file;
					fileURL = file.toURL();
					logger.debug("fileDownload name: " + file.getName());
				}
			}
		}

		File file = new File("./src/test/resources/runtime/Pdf" + "/" + fileDownload.getName());
		PDDocument doc = new PDDocument();
		doc = load(file);
		logger.debug("Number of pages: " + doc.getNumberOfPages());
		logger.debug("Document information: " + doc.getDocumentInformation().getCOSObject().toString());
		logger.debug("Get pages: " + doc.getPages().get(1));
		logger.debug("Get page 1: " + doc.getPage(1));
		InputStream is = doc.getPages().get(1).getContents();

		String textScan = null;
		try (Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name())) {
			textScan = scanner.useDelimiter("\\A").next();
			//logger.debug("textScan: " + textScan);
		}







		return doc;
	}

		//Dit weer actief maken
		/*PreflightParser parser = new PreflightParser("./src/test/resources/runtime/Pdf" + "/" + fileDownload.getName());

		parser.parse();
		try (PreflightDocument document = parser.getPreflightDocument()) {
			document.validate();
			result = document.getResult();
			logger.debug("Optional of result: " + Optional.of(result).toString());
			Gson gs = new Gson();
			String res = gs.toJson(result);
			logger.debug("Result = " + res.toString());

		}*/
		/*try {
			PreflightDocument document1 = parser.getPreflightDocument();
				document1.validate();
				result = document1.getResult();
			if (result.getErrorsList().size() > 0) {
				logger.error("The following validation errors occured for PDF-A validation");
			}

			for (ValidationResult.ValidationError ve : result.getErrorsList()) {
				logger.error("\t" + ve.getErrorCode() + ": " + ve.getDetails());
			}

			if (!result.isValid()) {
				throw new PdfException("The file is not a valid PDF-A document");
			}
		}
			catch (SyntaxValidationException e) {
				logger.error("The file is syntactically invalid.", e);
				throw new PdfException("Resulting PDF Document is syntactically invalid.");
			} catch (ValidationException e) {
				logger.error("The file is not a valid PDF-A document.", e);
				throw new PdfException("The file is not a valid PDF-A document");
			} catch (IOException e) {
				logger.error("An IOException (" + e.getMessage() + ") occurred, while validating the PDF-A conformance",
						e);
				throw new PdfException("Failed validating PDF Document IOException.");
			} catch (RuntimeException e) {
				logger.debug(
						"An RuntimeException (" + e.getMessage() + ") occurred, while validating the PDF-A conformance",
						e);
				throw new PdfException("Failed validating PDF Document RuntimeException.");
			} finally {
				if (document != null) {
					IOUtils.closeQuietly((Closeable) document);
				}
			}*/


		// Dit weer actief maken
		/*PDFTextStripper pdfStripper1 = null;

		COSDocument cosDoc = null;
		File file1 = new File("./src/test/resources/runtime/Pdf" + "/" + fileDownload.getName());
		PDDocument pdDoc = PDDocument.load(file);

		PreflightParser parser1 = new PreflightParser("./src/test/resources/runtime/Pdf" + "/" + fileDownload.getName());
		parser1.parse();
		cosDoc = parser1.getDocument();
		pdfStripper = new PDFTextStripper();
		pdDoc = new PDDocument(cosDoc);


		logger.debug("Number of pages: " + pdDoc.getNumberOfPages());
		//PDDocument doc = PDDocument.load(new File("./src/test/resources/runtime/Pdf" + "/" + fileDownload.getName()));
		//pdfStripper.setStartPage(1);
		//pdfStripper.setEndPage(3);
		pdfStripper.setSortByPosition(true);
		String text1 = pdfStripper.getText(pdDoc);;
		System.out.println("Text1 in PDF\n---------------------------------");
		System.out.println(text1);*/
		//doc.close();
		//pdDoc.close();

	public void validatePdf(PDDocument doc) throws IOException {
		StringBuffer content = new StringBuffer();
		PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
		PDAcroForm acroForm = docCatalog.getAcroForm();
		content.append("\n");
		List<PDField> fields = acroForm.getFields();
		logger.debug("{} top-level fields were found on the form", fields.size());

		for (PDField field : fields) {
			content.append(field.getPartialName());
			content.append(" = ");
			content.append(field.getValueAsString());
			content.append(" \n ");
			String attr = field.getPartialName();
			String value = field.getValueAsString();
			attr = value;
			logger.debug("attributes with value: " + attr);
		}
		logger.debug("content: " + content.toString());
		//String title = acroForm.getField("Title").toString();
		//logger.debug("Title: " + title);
		//String value = acroForm.getField("Title").getValueAsString();
		title = acroForm.getField("Title").getValueAsString();
		logger.debug("Title value: " + title);
		Assert.assertEquals("Ontvangstbewijs voertuig", title);
		leasingCompanyDossierNumber = acroForm.getField("LeasingCompanyDossierNumber").getValueAsString();
		Properties properties = new Properties();
		InputStream inputDriver = new FileInputStream("./src/test/resources/runtime/dossiernumber.properties");
		properties.load(inputDriver);
		String dosNr = properties.getProperty("DossierNumber");
		//dosNr = orderPage.getDosNr();
		Response response = Endpoints.getOrderByDosNr(dosNr);
		logger.debug("response getOrderByDossierNumber:" + response.asPrettyString());
		JsonPath jsonPathEvaluator = response.jsonPath();
		String jsonDosNr = jsonPathEvaluator.get("leasingCompanyDossierNumber").toString();
		String str1 = jsonDosNr.replace("[", "");
		String str2 = str1.replace("]", "");
		dosNr = str2;
		Assert.assertEquals(dosNr, leasingCompanyDossierNumber);
		dataClient = acroForm.getField("DataClient").getValueAsString();
		Assert.assertEquals("Gegevens huurder", dataClient);
		dataDriver = acroForm.getField("DataDriver").getValueAsString();
		Assert.assertEquals("", dataDriver);
		clientNameLabel = acroForm.getField("ClientNameLabel").getValueAsString();
		Assert.assertEquals("Naam:", clientNameLabel);
		clientStreetLabel = acroForm.getField("ClientStreetLabel").getValueAsString();
		Assert.assertEquals("Straat:", clientStreetLabel);
		clientPostalCodeLabel = acroForm.getField("ClientPostalCodeLabel").getValueAsString();
		Assert.assertEquals("Postcode:", clientPostalCodeLabel);
		//FTP_PostOrderImportsRequest postOrderImportsRequest = new FTP_PostOrderImportsRequest();
		//GetOrderByDossierNumberApiSteps getOrderByDossierNumberApiSteps = new API_GetOrderByDossierNumber();
		//logger.debug("Order by dossiernumber clientName: " + getOrderByDossierNumberApiSteps.);
		//clientName = acroForm.getField("ClientName").getValueAsString();
		//Assert.assertEquals(getOrderByDossierNumber.getOrder().getClientName(), clientName);
	}

	public void validatePdfLogo(PDDocument doc) throws IOException {
		File imgLogo = null;
		PDPageTree list = doc.getPages();
		for (PDPage page : list) {
			PDResources pdResources = page.getResources();
			for (COSName c : pdResources.getXObjectNames()) {
				PDXObject o = pdResources.getXObject(c);
				if (o instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
					imgLogo = new File("./src/test/resources/runtime/Pdf/" + System.nanoTime() + ".jpg");
					logger.debug("File imgLogo: " + imgLogo.toString());
					ImageIO.write(((org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject)o).getImage(), "jpeg", imgLogo);
					logger.debug("image weggeschreven naar timestamp.jpg");
				}
			}
		}

		BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources("./src/test/resources/runtime/Logo_56.jpg");
		BufferedImage actualImage = ImageComparisonUtil.readImageFromResources(imgLogo.toString());

		File resultDestination = new File( "./src/test/resources/runtime/result.png" );

		ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();
		logger.debug("imageComparisonResult: " + imageComparisonResult.getResult().toString());

		//Images mogen niet meer dan 1% afwijken
		ImageComparison imageComparison = new ImageComparison(expectedImage, actualImage)
				.setAllowingPercentOfDifferentPixels(1);
		ImageComparisonResult compareResult = imageComparison.compareImages();
		Assert.assertEquals(MATCH, compareResult.getImageComparisonState());
		compareResult = imageComparison.setAllowingPercentOfDifferentPixels(0).compareImages();
		Assert.assertEquals(MISMATCH, compareResult.getImageComparisonState());


		PDFRenderer pdfRenderer	= new PDFRenderer(doc);
		BufferedImage img = pdfRenderer.renderImage(0);
		ImageIO.write(
				img, "JPEG",
				new File("./src/test/resources/runtime/Pdf/delivery-receipt-00000000-000001.jpg"));
		System.out.println(
				"Image has been extracted successfully");
		doc.close();
	}




	public void accept_order() throws InterruptedException, IOException {
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(10000));
		wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));

		Date date = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String today = formatter.format(date);
		String message_action_needed = String.format("The follow up date for this order was reached on %s", today);
		if(System.getProperty("leasingType").equals("WALLBOX")) {
			wait.until(ExpectedConditions.visibilityOf(input_expected_installation_date));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_expected_installation_date);
			//wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_expected_delivery_date));
			//select_date_picker_expected_delivery_date.click();
			Thread.sleep(2000);
			odh.select_day(odh.pick_day()).click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_accept_order_wb);
			Thread.sleep(5000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_accept_order_wb);
			//btn_accept_order.click();
			Thread.sleep(5000);
		} else {
			try {
				wait.until(ExpectedConditions.visibilityOf(msg_action_needed));
				logger.debug("message_action_needed: " + msg_action_needed.getText());
			} catch(Exception e) {
				logger.debug("There is no message of action needed ");
			}

			//TODO date on screen is yesterday --> bug?
			//Assert.assertEquals(message_action_needed, msg_action_needed.getText());

			//wait.until(ExpectedConditions.visibilityOf(select_date_picker_expected_delivery_date));
			Thread.sleep(3000);
			try {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_expected_delivery_date);
			} catch(Exception e) {
				logger.debug("select_date_picker_expected_delivery_date niet gelukt");
			}

			//wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_expected_delivery_date));
			//select_date_picker_expected_delivery_date.click();
			Thread.sleep(2000);
			try {
				this.takeSnapShot(driver, "./src/test/resources/runtime/ordImages/beforeSelectExpectedDeliveryDate.jpg") ;
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", odh.select_day(odh.pick_day()));

				Thread.sleep(500);
				odh.select_day(odh.pick_day()).sendKeys(Keys.ENTER);
				this.takeSnapShot(driver, "./src/test/resources/runtime/ordImages/afterSelectExpectedDeliveryDate.jpg") ;
				//odh.select_day(odh.pick_day()).click();

			} catch(Exception e) {
				logger.debug("odh.select_day(odh.pick_day()) niet gelukt");
			}

			//odh.select_day(odh.pick_day()).sendKeys(Keys.TAB);
			//wait.until(ExpectedConditions.elementToBeClickable(btn_accept_order));
			Thread.sleep(2000);
			try {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_accept_order);
				Thread.sleep(5000);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_accept_order);
			} catch(Exception e) {
				logger.debug("click btn_accept_order niet gelukt");
			}

			//btn_accept_order.click();
			Thread.sleep(5000);
			//wait.until(ExpectedConditions.visibilityOf(message_processed_successfully));
		}


	}

	public void confirm_order() throws InterruptedException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		Thread.sleep(6000);
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_confirm_order);
		Thread.sleep(5000);
		//btn_confirm_order.click();
		try {
			if(System.getProperty("leasingType").equals("WALLBOX")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_confirm_order_wb);
			} else {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_confirm_order);
			}
		} catch(Exception e) {
			logger.debug(" click btn_confirm_order mislukt");
		}

		Thread.sleep(5000);
		//wait.until(ExpectedConditions.visibilityOf(message_processed_successfully));
	}

	public void send_comment(String comment) throws InterruptedException {
		logger.debug("Start send_comment");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_comment);
		//logger.debug("After scroll into view");
		wait.until(ExpectedConditions.elementToBeClickable(input_comment));
		input_comment.sendKeys(comment);
		//logger.debug("After send keys");
		wait.until(ExpectedConditions.elementToBeClickable(btn_send_comment));
		if (System.getProperty("BrowserName").equals("firefox")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_send_comment);
			wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
		} else {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_send_comment);
			//btn_send_comment.click();
			wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));

		}
		logger.debug("End send_comment");
		Thread.sleep(2000);
	}

	public void check_vehicle_arrived() throws InterruptedException {
		logger.debug("Start check vehicle arrived");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
		Thread.sleep(5000);
		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			logger.debug("Tak FINANCIAL_LEASE");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_vehicle_arrived);
			Thread.sleep(500);
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio_vehicle_not_arrived);
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio_vehicle_arrived);
			radio_vehicle_arrived.click();
			logger.debug("Start check vehicle arrived, no moet nu geselecteerd zijn");


		} else {
			Thread.sleep(3000);
			try {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_vehicle_arrived);
				Thread.sleep(2000);
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio_vehicle_arrived);
				radio_vehicle_arrived.click();
				logger.debug("vehicle arrived geklikt");
			} catch(Exception e) {
				logger.debug("vehicle arrived klik mislukt");
			}
		}

		//Thread.sleep(5000);
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_next);
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next);
		//btn_next.click();
	}

	public void enter_delivery_info() throws IOException, InterruptedException {
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		ORD_Commands_Helper och = new ORD_Commands_Helper(driver);
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));

		if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_proposed_delivery_date));
			select_date_picker_proposed_delivery_date.click();
			Thread.sleep(2000);
			odh.select_day(odh.pick_day()).click();
			Thread.sleep(2000);

			List<WebElement> elements = driver.findElements(By.xpath("//input[@formcontrolname='proposedDeliveryTime']"));

			for (WebElement element : elements) {
				logger.debug("proposedDeliveryTime enabled: " + element.getAttribute("id"));
				logger.debug("proposedDeliveryTime enabled: " + element.isEnabled());
			}




			select_date_picker_first_registration_date_lux.click();
			select_choose_month_and_year.click();
			String dateFirstRegistrationYear = odh.pick_date_first_registration_year(3);
			odh.select_year(dateFirstRegistrationYear).click();
			// Select month first registration
			String dateFirstRegistrationMonth = odh.pick_date_first_registration_month(3);
			odh.select_month(dateFirstRegistrationMonth).click();
			// Select day first registration
			Thread.sleep(1000);
			String dateFirstRegistrationDay = odh.pick_day();
			odh.select_day(dateFirstRegistrationDay).click();
			Thread.sleep(500);
			dateFirstRegistrationLux = dateFirstRegistrationYear.trim() + "-" + dateFirstRegistrationMonth.trim() + "-" + dateFirstRegistrationDay.trim();
			logger.debug("dateFirstRegistration Luxembourg: " + dateFirstRegistrationLux);

			select_proposed_delivery_time.click();

			((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute(arguments[1],arguments[2])",
					input_proposed_delivery_time_lux, "aria-invalid", false);
			//logger.debug("aria-invalid: " + input_proposed_delivery_time_lux.getAriaRole());
			logger.debug("aria-invalid: " + input_proposed_delivery_time_lux.getAttribute("aria-invalid"));
			//((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute(arguments[1],arguments[2])",
					//input_proposed_delivery_time_lux, "value", "11:59");
			input_proposed_delivery_time_lux.sendKeys("11:59");
			logger.debug("time: " + input_proposed_delivery_time_lux.getText());
			//((JavascriptExecutor) driver).executeScript("arguments[0].value='1259';", input_proposed_delivery_time_lux);
			//input_proposed_delivery_time_lux.sendKeys("1259");

			input_vinnumber_lux.click();
			input_vinnumber_lux.sendKeys(VIN);
			input_license_plate_lux.click();
			input_license_plate_lux.sendKeys(LicensePlate);
			input_tax_stamp_costs.click();
			input_tax_stamp_costs.sendKeys(taxStampCosts);
			input_inscription_costs.click();
			input_inscription_costs.sendKeys(inscriptionCosts);
			tyreSpecs = width + "/" + relationWidthHeight + "/" + rimDiameter + "/" + speedIndex + "/" + loadIndex;
			input_tyre_specifications_lux.click();
			input_tyre_specifications_lux.sendKeys(tyreSpecs);
			input_WLTP_lux.click();
			input_WLTP_lux.sendKeys(WLTPInputLux);
			checkbox_plate_type_specified.click();
			listbox_plate_type.click();

			List<WebElement> options = driver.findElements(By.cssSelector("mat-option"));

			PlateType plateType = PlateType.Normal;
			for (WebElement element : options) {
				if (element.getText().equals(plateType.getPlateType())) {
					//element.click();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
				}
			}
			Thread.sleep(500);
			btn_next_after_vin.click();
			Thread.sleep(500);
			df.dropFile(new File("./src/test/resources/runtime/COC.png"), dropzone, 0, 0);
			Thread.sleep(2500);
			btn_vehicle_at_supplier.click();

		} else {
			if (!System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

			}
			//wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_expected_delivery_date_ordered));
			//Thread.sleep(2000);
			//odh.select_day(odh.pick_day()).click();

			//Thread.sleep(1000);
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_accept_order);
			//Thread.sleep(5000);
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_accept_order);
			Thread.sleep(5000);


			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_vinnumber);
			Thread.sleep(500);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_vinnumber);
			//input_vinnumber.click();
			input_vinnumber.sendKeys(VIN);
			Thread.sleep(1000);
			if (System.getProperty("BrowserName").equals("firefox")) {

				wait.until(ExpectedConditions.elementToBeClickable(input_vin_control_number));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_vin_control_number);
			} else {
				Thread.sleep(1000);
				input_vin_control_number.click();
			}

			input_vin_control_number.sendKeys(VINControlNumber);
			Thread.sleep(1000);
			if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
				int min = 000000010;
				int max = 999999999;
				//int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);
				int random_int = 867293938;
				input_form_number.sendKeys(String.valueOf(random_int));
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_after_vin);
			//btn_next_after_vin.click();


			if (!System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

				df.dropFile(new File("./src/test/resources/runtime/COC.png"), dropzone, 0, 0);
				df.dropFile(new File("./src/test/resources/runtime/Registration.jpeg"), dropzone_2, 0, 0);
				Thread.sleep(5000);
				wait.until(ExpectedConditions.elementToBeClickable(btn_vehicle_at_supplier));
				actions.moveToElement(btn_vehicle_at_supplier);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_vehicle_at_supplier);
				//btn_vehicle_at_supplier.click();
				Thread.sleep(5000);
			}
		}
	}

	public void enter_wallbox_install_info(DataTable table) throws IOException, InterruptedException {
		List<List<String>> data = table.asLists();
		String evbNumberData = data.get(1).get(0);
		String cardNumberData = data.get(1).get(1);

		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));

		logger.debug("enter_wallbox_install_info");
		if(evbNumberData != null && !evbNumberData.isEmpty() && evbNumberData.length()>0) {
			evbNumber = evbNumberData;
		} else {
			evbNumber = faker.bothify("??????????????????????????????????-#####");
			cardNumber = faker.bothify("??????????????????????????????????-#####");
		}
		input_evb_number.sendKeys(evbNumber);
		if(cardNumberData != null && !cardNumberData.isEmpty() && cardNumberData.length()>0) {
			cardNumber = cardNumberData;
		} else {
			cardNumber = faker.bothify("??????????????????????????????????-#####");
		}
		input_charge_card_number.sendKeys(cardNumber);

		//wait.until(ExpectedConditions.visibilityOf(select_datepicker_installation_date));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_datepicker_installation_date);
		//select_datepicker_installation_date.click();
		Thread.sleep(2000);
		odh.select_day(odh.pick_day()).click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_datepicker_certification_date);
		//select_datepicker_certification_date.click();
		Thread.sleep(2000);
		odh.select_day(odh.pick_day()).click();
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_installed);
		Thread.sleep(500);
		btn_installed.click();
		}

	public void enter_ready_to_activate() throws IOException, InterruptedException {
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));

		logger.debug("enter_ready_to_activate");

		Thread.sleep(2000);
		//TODO Scroll into view

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_ready_to_activate);
		Thread.sleep(500);
		btn_ready_to_activate.click();
	}

	public void enter_ready_to_activate_info() throws IOException, InterruptedException {
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));

		logger.debug("enter_ready_to_activate_info");

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_datepicker_activation_date);

		Thread.sleep(2000);
		odh.select_day(odh.pick_day()).click();
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_activate);
		Thread.sleep(500);
		btn_activate.click();
	}


	public void enter_vehicle_at_supplier() throws IOException, InterruptedException, ParseException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
		ORD_LicensplateNumberGenerator lng = new ORD_LicensplateNumberGenerator();
		//LicensePlate = lng.getRandomLicenseplateNumber().replace("-", "");

		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			Thread.sleep(2000);
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
			//radio_vehicle_arrived);
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio_vehicle_arrived);
			//radio_vehicle_arrived.click();
			//btn_next.click();

			wait.until(ExpectedConditions.visibilityOf(input_license_plate_fl));
			input_license_plate_fl.sendKeys(LicensePlate);

			// select first registration date
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					select_date_picker_date_first_registration_fl);
			Thread.sleep(500);
			select_date_picker_date_first_registration_fl.click();
			select_choose_month_and_year.click();
			// select year first registration
			String dateFirstRegistrationYear = odh.pick_date_first_registration_year(6);
			odh.select_year(dateFirstRegistrationYear).click();
			// Select month first registration
			String dateFirstRegistrationMonth = odh.pick_date_first_registration_month(6);
			odh.select_month(dateFirstRegistrationMonth).click();
			// Select day first registration
			Thread.sleep(1000);
			String dateFirstRegistrationDay = odh.pick_day();
			odh.select_day(dateFirstRegistrationDay).click();
			Thread.sleep(500);
			dateFirstRegistration = dateFirstRegistrationYear.trim() + "-" + dateFirstRegistrationMonth.trim() + "-" + dateFirstRegistrationDay.trim();
			logger.debug("dateFirstRegistration FL: " + dateFirstRegistration);

			input_max_weight_fl.sendKeys(maxWeight);
			input_engine_displacement_fl.sendKeys(engineDisplacement);
			input_power_fl.sendKeys(power);


			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", select_fuel_type_fl);
			Thread.sleep(500);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_fuel_type_fl);
			//select_fuel_type_fl.click();
			option_fuel_type_diesel_fl.click();

			input_WLTP_fl.sendKeys(WLTPInput);
			input_NEDC_fl.sendKeys(NEDCInput);

			btn_next_after_vin.click();
			Thread.sleep(1000);
			df.dropFile(new File("./src/test/resources/runtime/Registration.jpeg"), dropzone, 0, 0);
			Thread.sleep(2000);
			btn_vehicle_at_supplier.click();

			//wait.until(ExpectedConditions.visibilityOf(message_processed_successfully));
		} else {
			if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {

			} else {
				wait.until(ExpectedConditions.visibilityOf(input_license_plate));
				input_license_plate.sendKeys(LicensePlate);
				Properties p = new Properties();
				p.put("LicensePlate", LicensePlate);
				FileOutputStream fos = new FileOutputStream("./src/test/resources/runtime/licenseplate.properties");
				p.store(fos, "");
				logger.debug("licensePlate " + LicensePlate +  " naar proprty file geschreven");

				setLicensePlate(LicensePlate);
				File file = new File("./src/test/resources/runtime/TestInfo.csv");
				//String csvFilename = "./src/test/resources/runtime/TestInfo.csv";
				FileWriter fw = new FileWriter(file, true);
				fw.append(LicensePlate);
				fw.append("\n");
				fw.close();
			}

			// select first registration date
			//Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					select_date_picker_date_first_registration);
			Thread.sleep(500);
			//select_date_picker_date_first_registration.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_date_first_registration);
			select_choose_month_and_year.click();
			// select year first registration
			String dateFirstRegistrationYear = odh.pick_date_first_registration_year(3);
			odh.select_year(dateFirstRegistrationYear).click();
			// Select month first registration
			String dateFirstRegistrationMonth = odh.pick_date_first_registration_month(3);
			odh.select_month(dateFirstRegistrationMonth).click();
			// Select day first registration
			Thread.sleep(1000);
			String dateFirstRegistrationDay = odh.pick_day();
			try {
				odh.select_day(dateFirstRegistrationDay).click();
			} catch(Exception e) {
				Integer i = Integer.parseInt(dateFirstRegistrationDay.trim());
				logger.debug("i" + i);
				i -= 1;
				logger.debug("i" + i);
				dateFirstRegistrationDay = i.toString();
				dateFirstRegistrationDay = " " + dateFirstRegistrationDay + " ";
				logger.debug("dateFirstRegistrationDay: " + dateFirstRegistrationDay);
				odh.select_day(dateFirstRegistrationDay).click();
			}
			//odh.select_day(dateFirstRegistrationDay).click();
			Thread.sleep(500);
			if (dateFirstRegistrationDay.length() == 1) {
				dateFirstRegistrationDay = "0" + dateFirstRegistrationDay;
				logger.debug("dateFirstRegistrationDay" + dateFirstRegistrationDay);
			}
			dateFirstRegistration = dateFirstRegistrationYear.trim() + "-" + dateFirstRegistrationMonth.trim() + "-" + dateFirstRegistrationDay.trim();
			logger.debug("dateFirstRegistration" + dateFirstRegistration);

			input_WLTP.sendKeys(WLTPInput);
			input_NEDC.sendKeys(NEDCInput);
			input_model_year.sendKeys(modelYear);
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_ready_for_delivery);
			//btn_ready_for_delivery.click();
			//wait.until(ExpectedConditions.visibilityOf(message_processed_successfully));
			Thread.sleep(5000);
		}


		if(System.getProperty("language").equals("fr")) {
			sdf = new SimpleDateFormat("yyyy-MMM-dd", Locale.FRENCH);
			logger.debug("dateFirstRegistration vlak voor parsen fr: " + dateFirstRegistration);
		} else {
			sdf = new SimpleDateFormat("yyyy-MMM-dd", Locale.ENGLISH);
			logger.debug("dateFirstRegistration vlak voor parsen: " + dateFirstRegistration);
		}

		Date date = sdf.parse(dateFirstRegistration);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		dateFirstRegistration = dateFormat.format(date);


	}

	public void enter_vehicle_ready_for_delivery() throws IOException, InterruptedException, ScriptException, NoSuchMethodException {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);
//		toggle_digital_delivery.click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_prepare_delivery);
		//btn_prepare_delivery.click();

		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

			//btn deliver vehicle

			Thread.sleep(2000);
			//wait.until(ExpectedConditions.visibilityOf(btn_prepare_delivery));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_prepare_delivery);
			Thread.sleep(500);
			//btn_ready_for_delivery.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_prepare_delivery);
		} else if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_prepare_delivery);
			logger.debug("Op prepare delivery geklikt");
			Thread.sleep(1000);
		} else {
			wait.until(ExpectedConditions.visibilityOf(btn_deliver_vehicle));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_deliver_vehicle);
			Thread.sleep(500);
		}


		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

		} else {
			Thread.sleep(2000);
			List<WebElement> buttons = driver.findElements(By.xpath("//button"));
			for (int i = 0; i < buttons.size(); i++) {
				logger.debug("Alle buttons: " + buttons.get(i).getAttribute("data-testid"));
			}

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_deliver_vehicle);
			Thread.sleep(500);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_deliver_vehicle);
			logger.debug("Op btn_deliver_vehicle geklikt");
			//wait.until(ExpectedConditions.elementToBeClickable(btn_deliver_vehicle));
			//btn_deliver_vehicle.click();
		}

		ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
		dealerDossierNr = mcng.getRandomContractNumber();

		wait.until(ExpectedConditions.visibilityOf(input_dealer_dossier_number));
		input_dealer_dossier_number.sendKeys(dealerDossierNr);


		// WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

		} else {
			wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_actual_delivery_date));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_actual_delivery_date);
			//select_date_picker_actual_delivery_date.click();
			Thread.sleep(2000);
			odh.select_day(odh.pick_day()).click();
		}

		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_mileage);
		input_mileage.sendKeys(mileage);
		//((JavascriptExecutor) driver).executeScript("arguments[0].value='"+ mileage +"';", input_mileage);
		input_mileage.sendKeys(Keys.TAB);
		//input_mileage.sendKeys(mileage);

		input_number_of_keys.sendKeys(numberOfKeys);
		input_keycode.sendKeys(keycode);
		input_startcode.sendKeys(startcode);
		if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			//wait.until(ExpectedConditions.elementToBeClickable(btn_next_after_delivery_information));
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_after_delivery_information);
			wait.until(ExpectedConditions.elementToBeClickable(btn_next_to_additional_information));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_to_additional_information);
		} else {

			if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_model_year_delivery);
				input_model_year_delivery.sendKeys(modelYear);
			} else {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", icon_edit_tyre_specifications);
				Thread.sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(icon_edit_tyre_specifications));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", icon_edit_tyre_specifications);
			}
			//icon_edit_tyre_specifications.click();
			// ALLEEN INDIEN GEKLIKT IS OP icon_edit_tyre_specifications ZIJN DE VOLGENDE
			// VELDEN ZICHTBAAR !!!!!!!!!!!!!!!!!!!!!!!!
			input_width.sendKeys(width);
			input_relation_w_h.sendKeys(relationWidthHeight);
			input_rim_diameter.sendKeys(rimDiameter);
			select_speed_index.sendKeys(speedIndex);
			select_load_index.sendKeys(loadIndex);
			tyreSpecs = width + "/" + relationWidthHeight + "/" + rimDiameter + "/" + speedIndex + "/" + loadIndex;
			// btn_cancel_tyre_specifications.click();
			// EINDE EDIT TYRE SPECIFICATIONS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_tyre_brand);
			Thread.sleep(500);
			wait.until(ExpectedConditions.visibilityOf(input_tyre_brand));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_tyre_brand);
			//input_tyre_brand.click();
			input_tyre_brand.sendKeys(tyreBrand);

			select_tyre_type.click();
			Thread.sleep(1000);
			select_tyre_type_summer.click();

			//if((returnedVehicle.equalsIgnoreCase("true") && !toggle_returned_vehicle.isSelected()) || (returnedVehicle.equalsIgnoreCase("false") && toggle_returned_vehicle.isSelected())) {
			//	toggle_returned_vehicle.click();
			//}

			if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

			} else {
				select_owner_returned_vehicle.click();
				wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
				wait.until(ExpectedConditions.visibilityOf(option_registered_on_name_of_renta));
				option_registered_on_name_of_renta.click();

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_license_plate_returned_vehicle);
				Thread.sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(input_license_plate_returned_vehicle));
				input_license_plate_returned_vehicle.click();
				input_license_plate_returned_vehicle.sendKeys(licensePlateReturnedVehicle);
			}

			Thread.sleep(2000);
			if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_tyre_spec_entered_step_five_button);
			} else
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_to_additional_information);
			//btn_next_to_additional_information.click();
		}


		//if((returnedVehicle.equalsIgnoreCase("true") && !toggle_returned_vehicle.isSelected()) || (returnedVehicle.equalsIgnoreCase("false") && toggle_returned_vehicle.isSelected())) {
		//toggle_returned_vehicle.click();
		//}


		//toggle_returned_vehicle.click();
		//select_owner_returned_vehicle.click();
		wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_registration_certificate);
		Thread.sleep(2000);
		//wait.until(ExpectedConditions.elementToBeClickable(toggle_registration_certificate));
		Boolean boolregistrationCertificate = Boolean.parseBoolean(selectRegistrationCertificate);
		if (boolregistrationCertificate) {
			//toggle_registration_certificate.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_registration_certificate);
		}
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_insurance_certificate);
		Thread.sleep(2000);
		Boolean boolinsuranceCertificate = Boolean.parseBoolean(selectInsuranceCertificate);
		if (boolinsuranceCertificate) {
			//toggle_insurance_certificate.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_insurance_certificate);
		}
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_certificate_of_conformity);
		Thread.sleep(2000);
		Boolean boolcertificateOfConformity = Boolean.parseBoolean(selectCertificateOfConformity);
		if (boolcertificateOfConformity) {
			//toggle_certificate_of_conformity.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_certificate_of_conformity);
		}
		Boolean booltechnicalInspectionCertificate = Boolean.parseBoolean(selectTechnicalInspectionCertificate);
		if (booltechnicalInspectionCertificate) {
			//toggle_technical_inspection_certificate.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_technical_inspection_certificate);
		}
		Boolean boollegalKit = Boolean.parseBoolean(selectLegalKit);
		if (boollegalKit) {
			//toggle_legal_kit.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_legal_kit);
		}
		Boolean boolserviceManual = Boolean.parseBoolean(selectServiceManual);
		if (boolserviceManual) {
			//toggle_service_manual.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_service_manual);
		}
		Boolean boolfuelCard = Boolean.parseBoolean(selectFuelCard);
		if (boolfuelCard) {
			//toggle_fuel_card.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_fuel_card);
		}

		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_prepare_digital_delivery);
		} else {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_driver_first_name);
			Thread.sleep(2000);
			input_driver_first_name.sendKeys(driverFirstName);
			input_driver_last_name.sendKeys(driverLastName);

			select_driver_birthday.click();
			if (System.getProperty("BrowserName").equals("firefox")) {
				//wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_birthDate_Year_1981));
				Thread.sleep(2000);
				//((JavascriptExecutor) driver).executeScript("document.body.style.transform='scale(0.9)';");

				driver.manage().window().setSize(new Dimension(1280, 768));
				//driver.set_window_size(2000, 694);

				//Actions actions = new Actions(driver);
				//actions.moveToElement(select_date_picker_birthDate_Year_1980);
				//actions.click(select_date_picker_birthDate_Year_1980);
				//Action action = actions.build();
				//action.perform();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Year_1965);
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Year_1980);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Month_JUN);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Day_19);
			} else {
				select_date_picker_birthDate_Year_2000.click();
				Thread.sleep(2000);
				select_date_picker_birthDate_Month_JUN.click();
				Thread.sleep(2000);
				select_date_picker_birthDate_Day_19.click();
			}
			//select_date_picker_birthDate_Year_1980.click();
			//select_date_picker_birthDate_Month_JUL.click();
			//select_date_picker_birthDate_Day_13.click();

			input_driver_birthplace.sendKeys(driverBirthPlace);

			WebElement nextFF = driver.findElement(By.cssSelector("form.ng-star-inserted:nth-child(2) > div:nth-child(11) > div:nth-child(2) > button:nth-child(1)"));
			if (System.getProperty("BrowserName").equals("firefox")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", nextFF);
				//nextFF.click();
			} else {
				btn_next_after_additional_information.click();
			}

			Thread.sleep(2000);
			if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
				df.dropFile(new File("./src/test/resources/runtime/delivery-receipt.pdf"), dropzone, 0, 0);
				Thread.sleep(500);
				df.dropFile(new File("./src/test/resources/runtime/rijbewijs.jpeg"), dropzone_3, 0, 0);
				Thread.sleep(500);
				df.dropFile(new File("./src/test/resources/runtime/Registration.jpeg"), dropzone_4, 0, 0);
				Thread.sleep(500);
			} else {

				df.dropFile(new File("./src/test/resources/runtime/delivery-receipt.pdf"), dropzone, 0, 0);
				df.dropFile(new File("./src/test/resources/runtime/rijbewijs.jpeg"), dropzone_2, 0, 0);

				Thread.sleep(2000);
				//wait.until(ExpectedConditions.elementToBeClickable(btn_deliver_vehicle_after_file_upload));
				//btn_deliver_vehicle_after_file_upload.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_deliver_vehicle_after_file_upload);
				Thread.sleep(2000);
			}
		}
	}


	public void prepare_digital_delivery() throws IOException, InterruptedException, ScriptException, NoSuchMethodException, com.google.zxing.NotFoundException, URISyntaxException, TranscoderException {


		FileInputStream fis;
		BufferedImage image;
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);

		//if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

			//Thread.sleep(2000);
			//wait.until(ExpectedConditions.visibilityOf(btn_ready_for_delivery));
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_ready_for_delivery);
			//Thread.sleep(500);
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_ready_for_delivery);
		//} else if(System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
			//Thread.sleep(2000);
		if(System.getProperty("language").equals("fr")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_prepare_delivery_fr);
			//logger.debug("Op prepare delivery geklikt");
		} else {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_prepare_delivery);
		}



			Thread.sleep(1000);
		//}


			Thread.sleep(2000);

			popup_skip.click();

			ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
			dealerDossierNr = mcng.getRandomContractNumber();

			wait.until(ExpectedConditions.visibilityOf(input_dealer_dossier_number));
			input_dealer_dossier_number.sendKeys(dealerDossierNr);

			// First registration date is al gevuld
			// Model year is al gevuld
			// NEDC value is al gevuld
			// WLTP value is al gevuld



			Thread.sleep(2000);
			if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_model_year_delivery);
				input_model_year_delivery.sendKeys(modelYear);
			} else {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_model_year_delivery);
				input_model_year_delivery.sendKeys(modelYear);
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_mileage);
			input_mileage.sendKeys(mileage);
			//((JavascriptExecutor) driver).executeScript("arguments[0].value='"+ mileage +"';", input_mileage);
			input_mileage.sendKeys(Keys.TAB);
			//input_mileage.sendKeys(mileage);

			input_number_of_keys.sendKeys(numberOfKeys);
			input_keycode.sendKeys(keycode);
			input_startcode.sendKeys(startcode);

		if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", icon_edit_tyre_specifications);
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(icon_edit_tyre_specifications));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", icon_edit_tyre_specifications);
			//icon_edit_tyre_specifications.click();
		}

				// ALLEEN INDIEN GEKLIKT IS OP icon_edit_tyre_specifications ZIJN DE VOLGENDE
				// VELDEN ZICHTBAAR !!!!!!!!!!!!!!!!!!!!!!!!
				input_width.sendKeys(width);
				input_relation_w_h.sendKeys(relationWidthHeight);
				input_rim_diameter.sendKeys(rimDiameter);
				select_speed_index.sendKeys(speedIndex);
				select_load_index.sendKeys(loadIndex);
				tyreSpecs = width + "/" + relationWidthHeight + "/" + rimDiameter + "/" + speedIndex + "/" + loadIndex;
				// btn_cancel_tyre_specifications.click();
				// EINDE EDIT TYRE SPECIFICATIONS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_tyre_brand);
				Thread.sleep(500);
				wait.until(ExpectedConditions.visibilityOf(input_tyre_brand));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_tyre_brand);
				//input_tyre_brand.click();
				input_tyre_brand.sendKeys(tyreBrand);

				select_tyre_type.click();
				Thread.sleep(1000);
				if(System.getProperty("language").equals("fr")) {
					select_tyre_type_summer_fr.click();
				} else {
					select_tyre_type_summer.click();
				}


				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_after_delivery_information);


			//toggle_returned_vehicle.click();
			//select_owner_returned_vehicle.click();
			wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_registration_certificate);
			Thread.sleep(2000);
			//wait.until(ExpectedConditions.elementToBeClickable(toggle_registration_certificate));
			Boolean boolregistrationCertificate = Boolean.parseBoolean(selectRegistrationCertificate);
			if (boolregistrationCertificate) {
				//toggle_registration_certificate.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_registration_certificate);
			}
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_insurance_certificate);
			Thread.sleep(2000);
			Boolean boolinsuranceCertificate = Boolean.parseBoolean(selectInsuranceCertificate);
			if (boolinsuranceCertificate) {
				//toggle_insurance_certificate.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_insurance_certificate);
			}
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_certificate_of_conformity);
			Thread.sleep(2000);
			Boolean boolcertificateOfConformity = Boolean.parseBoolean(selectCertificateOfConformity);
			if (boolcertificateOfConformity) {
				//toggle_certificate_of_conformity.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_certificate_of_conformity);
			}
			Boolean booltechnicalInspectionCertificate = Boolean.parseBoolean(selectTechnicalInspectionCertificate);
			if (booltechnicalInspectionCertificate) {
				//toggle_technical_inspection_certificate.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_technical_inspection_certificate);
			}
			Boolean boollegalKit = Boolean.parseBoolean(selectLegalKit);
			if (boollegalKit) {
				//toggle_legal_kit.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_legal_kit);
			}
			Boolean boolserviceManual = Boolean.parseBoolean(selectServiceManual);
			if (boolserviceManual) {
				//toggle_service_manual.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_service_manual);
			}
			Boolean boolfuelCard = Boolean.parseBoolean(selectFuelCard);
			if (boolfuelCard) {
				//toggle_fuel_card.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_fuel_card);
			}

			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_prepare_digital_delivery);
			//btn_prepare_digital_delivery.click();

			Thread.sleep(5000);

			String qrCodeURL = qr_code_digital_delivery.getAttribute("src");

			InputStream in = new URL(qrCodeURL).openStream();
			Files.copy(in, Paths.get("./src/test/resources/runtime/QRCode.svg"), StandardCopyOption.REPLACE_EXISTING);

			fis = new FileInputStream(String.valueOf("./src/test/resources/runtime/QRCode.svg"));
			image = ImageIO.read(in);

			JPEGTranscoder transcoder = new JPEGTranscoder();
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY,new Float(1.0));
			TranscoderInput input = new TranscoderInput(fis);
			OutputStream ostream = new FileOutputStream("./src/test/resources/runtime/QRCode.jpg");
			TranscoderOutput output = new TranscoderOutput(ostream);
			transcoder.transcode(input, output);
			ostream.close();

			fis = new FileInputStream(new File("./src/test/resources/runtime/QRCode.jpg"));
			image = ImageIO.read(fis);
			//BufferedImage cropedImage = image.getSubimage(0, 0, 400, 400);

			LuminanceSource luminanceSource= new BufferedImageLuminanceSource(image);
			BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(luminanceSource));
			Result result = new MultiFormatReader().decode(binaryBitmap);
			String textInQrCode = result.getText();
			logger.debug("textInQrCode:" + textInQrCode);


	}

	/*private String decodeQRCode(Object qrCodeImage) {
		Result result = null;

		try {
			BufferedImage bufferedImage;

			// if not (probably it is a URL
			if (((String) qrCodeImage).contains("http")) {
				bufferedImage = ImageIO.read((new URL((String)qrCodeImage)));

				// if is a Base64 String
			} else {
				byte[] decoded = Base64.decodeBase64((String)qrCodeImage);
				bufferedImage = ImageIO.read(new ByteArrayInputStream(decoded));
			}

			LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
			BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

			result = new MultiFormatReader().decode(bitmap);
		} catch (NotFoundException | IOException | com.google.zxing.NotFoundException e) {
			logger.debug("Error reading the QR Code", e);
		}
		return result.getText();
	}*/

	public void enter_deliver_vehicle() throws IOException, InterruptedException, ScriptException, NoSuchMethodException {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(20000));
		ORD_DatePicker_Helper odh = new ORD_DatePicker_Helper(driver);

		//wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
		//wait.until(ExpectedConditions.elementToBeClickable(btn_deliver_vehicle));

		Thread.sleep(3000);
		if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_deliver_vehicle_after_file_upload);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_deliver_vehicle_after_file_upload);
		} else {
			//else {
			//	logger.debug("scroll naar button");
			//	if(System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			//		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_deliver_vehicle);
			//		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_deliver_vehicle);
			//	}

			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_deliver_vehicle);
			logger.debug("klik button delivery");
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_deliver_vehicle);
			Thread.sleep(5000);
			//btn_deliver_vehicle.click();

			ORD_ContractNumberGenerator mcng = new ORD_ContractNumberGenerator();
			dealerDossierNr = mcng.getRandomContractNumber();

			//if(System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {

			//} else {
			//wait.until(ExpectedConditions.visibilityOf(input_dealer_dossier_number));
			Thread.sleep(2000);
			input_dealer_dossier_number.sendKeys(dealerDossierNr);

			//wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_actual_delivery_date));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_actual_delivery_date);
			//select_date_picker_actual_delivery_date.click();
			Thread.sleep(2000);
			odh.select_day(odh.pick_day()).click();
			logger.debug("actualDeliveryDate: " + odh.select_day(odh.pick_day()));


			input_model_year_delivery.sendKeys(modelYear);
			input_mileage.sendKeys(mileage);
			input_number_of_keys.sendKeys(numberOfKeys);
			input_keycode.sendKeys(keycode);
			input_startcode.sendKeys(startcode);


			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", icon_edit_tyre_specifications);
			//Thread.sleep(500);
			//wait.until(ExpectedConditions.elementToBeClickable(icon_edit_tyre_specifications));
			//icon_edit_tyre_specifications.click();
			// ALLEEN INDIEN GEKLIKT IS OP icon_edit_tyre_specifications ZIJN DE VOLGENDE
			// VELDEN ZICHTBAAR !!!!!!!!!!!!!!!!!!!!!!!!
			input_width.sendKeys(width);
			input_relation_w_h.sendKeys(relationWidthHeight);
			input_rim_diameter.sendKeys(rimDiameter);
			select_speed_index.sendKeys(speedIndex);
			select_load_index.sendKeys(loadIndex);
			tyreSpecs = width + "/" + relationWidthHeight + "/" + rimDiameter + "/" + speedIndex + "/" + loadIndex;
			// btn_cancel_tyre_specifications.click();
			// EINDE EDIT TYRE SPECIFICATIONS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_tyre_brand);
			Thread.sleep(500);
			wait.until(ExpectedConditions.visibilityOf(input_tyre_brand));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", input_tyre_brand);
			//input_tyre_brand.click();
			input_tyre_brand.sendKeys(tyreBrand);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", select_tyre_type);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_tyre_type);
			//select_tyre_type.click();
			Thread.sleep(1000);
			select_tyre_type_summer.click();
			setTyreType(tyreType);
		}

		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE")) {
			logger.debug("Nu wordt op next to additional information");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn_next_to_additional_information);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_to_additional_information);
			//btn_next_to_additional_information.click();
		}
		logger.debug("Op next to additional information geklikt, nu op next after delivery klikken");

		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE") || System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			logger.debug("Niet klikken");
			//btn_next_after_additional_information.click();
		} else {
			btn_next_after_delivery_information.click();
		}


		logger.debug("Op next after delivery");

		if (System.getProperty("leasingType").equals("FINANCIAL_LEASE") || System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {

		} else {
			if ((returnedVehicle.equalsIgnoreCase("true") && !toggle_returned_vehicle.isSelected()) || (returnedVehicle.equalsIgnoreCase("false") && toggle_returned_vehicle.isSelected())) {
				toggle_returned_vehicle.click();
			}


			//***********************************************2022-10-10**********************************************************
			toggle_returned_vehicle.click();
			select_owner_returned_vehicle.click();
			wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
			wait.until(ExpectedConditions.visibilityOf(option_registered_on_name_of_renta));
			option_registered_on_name_of_renta.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_license_plate_returned_vehicle);
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(input_license_plate_returned_vehicle));
			input_license_plate_returned_vehicle.click();
			input_license_plate_returned_vehicle.sendKeys(licensePlateReturnedVehicle);
			btn_next_after_delivery_information.click();
			//**********************************************2022-10-10-Einde**************************************
		}

		if (System.getProperty("leasingType").equals("OPERATIONAL_LEASE_LUXEMBOURG")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_deliver_vehicle_after_file_upload);
		} else {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_registration_certificate);
			Thread.sleep(2000);
			//wait.until(ExpectedConditions.elementToBeClickable(toggle_registration_certificate));
			Boolean boolregistrationCertificate = Boolean.parseBoolean(selectRegistrationCertificate);
			if (boolregistrationCertificate) {
				//toggle_registration_certificate.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_registration_certificate);
			}
			Boolean boolinsuranceCertificate = Boolean.parseBoolean(selectInsuranceCertificate);
			if (boolinsuranceCertificate) {
				//toggle_insurance_certificate.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_insurance_certificate);
			}
			Boolean boolcertificateOfConformity = Boolean.parseBoolean(selectCertificateOfConformity);
			if (boolcertificateOfConformity) {
				//toggle_certificate_of_conformity.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_certificate_of_conformity);
			}
			Boolean booltechnicalInspectionCertificate = Boolean.parseBoolean(selectTechnicalInspectionCertificate);
			if (booltechnicalInspectionCertificate) {
				//toggle_technical_inspection_certificate.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_technical_inspection_certificate);
			}
			Boolean boollegalKit = Boolean.parseBoolean(selectLegalKit);
			if (boollegalKit) {
				//toggle_legal_kit.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_legal_kit);
			}
			Boolean boolserviceManual = Boolean.parseBoolean(selectServiceManual);
			if (boolserviceManual) {
				//toggle_service_manual.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_service_manual);
			}
			Boolean boolfuelCard = Boolean.parseBoolean(selectFuelCard);
			if (boolfuelCard) {
				//toggle_fuel_card.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_fuel_card);
			}

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", input_driver_first_name);
			Thread.sleep(2000);
			input_driver_first_name.sendKeys(driverFirstName);
			input_driver_last_name.sendKeys(driverLastName);

			select_driver_birthday.click();
			if (System.getProperty("BrowserName").equals("firefox")) {
				wait.until(ExpectedConditions.elementToBeClickable(select_date_picker_birthDate_Year_1990));

				driver.manage().window().setSize(new Dimension(1280, 768));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Year_1965);
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Year_1980);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Month_JUN);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", select_date_picker_birthDate_Day_19);
			} else {
				select_date_picker_birthDate_Year_1965.click();
				select_date_picker_birthDate_Month_JUN.click();
				select_date_picker_birthDate_Day_19.click();
			}
			//select_date_picker_birthDate_Year_1980.click();
			//select_date_picker_birthDate_Month_JUL.click();
			//select_date_picker_birthDate_Day_13.click();

			input_driver_birthplace.sendKeys(driverBirthPlace);

			Thread.sleep(2000);
			//btn_next_after_additional_information.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_next_after_additional_information);

			/*
			 * WebElement nextFF = driver.findElement(By.
			 * cssSelector("form.ng-star-inserted:nth-child(2) > div:nth-child(11) > div:nth-child(2) > button:nth-child(1)"
			 * )); if(System.getProperty("BrowserName").equals("firefox")) {
			 * ((JavascriptExecutor) driver).executeScript("arguments[0].click();", nextFF);
			 * } else { btn_next_after_additional_information.click(); }
			 */

			Thread.sleep(2000);

			df.dropFile(new File("./src/test/resources/runtime/delivery-receipt.pdf"), dropzone, 0, 0);
			df.dropFile(new File("./src/test/resources/runtime/rijbewijs.jpeg"), dropzone_2, 0, 0);

			//wait.until(ExpectedConditions.elementToBeClickable(btn_deliver_vehicle_after_file_upload));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn_deliver_vehicle_after_file_upload);
			//btn_deliver_vehicle_after_file_upload.click();
			Thread.sleep(5000);
		}
	}
	//}

	public void set_toggle_digital_delivery(String choice) throws InterruptedException {
		Thread.sleep(3000);
		//WebElement toggle = driver.findElement(By.xpath("//span[@class = 'mat-slide-toggle-bar']/input"));
		wait.until(ExpectedConditions.elementToBeClickable(toggle_digi_delivery));
		String digitalDeliveryChecked = toggle_digi_delivery.getAttribute("aria-checked");
		//WebElement toggle = driver.findElement(By.xpath("//span[@class = 'mat-slide-toggle-bar']/input"));
		boolean toggleIsChecked = Boolean.parseBoolean(digitalDeliveryChecked);
		logger.debug("Toggle checked is: " + toggleIsChecked);
		if(choice != null && !choice.isEmpty()) {
			if(choice.equals("Yes")) {
				if(toggleIsChecked) {
					logger.debug("Choice Yes and checked");
				} else {
					logger.debug("Choice Yes and not checked");
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_digi_delivery);
					Thread.sleep(500);
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_digi_delivery);
				}
			//if((speed_pedelec.equalsIgnoreCase("true") && !toggle_speed_pedelec.isSelected()) || (speed_pedelec.equalsIgnoreCase("false") && toggle_speed_pedelec.isSelected())) {
			//	och.click_element_JS(toggle_speed_pedelec);
			//	Thread.sleep(500);
			}
			else {
				if(toggleIsChecked) {
					logger.debug("Choice No and checked");
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toggle_digi_delivery);
					Thread.sleep(500);
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", toggle_digi_delivery);
				} else {
					logger.debug("Choice No and not checked");
				}
			}
		}


	}


	public ORD_Topbar getTopBar() {
		return topBar;
	}

	public void setTopBar(ORD_Topbar topBar) {
		this.topBar = topBar;
	}

	public TestContext getTestContext() {
		return testContext;
	}

	public void setTestContext(TestContext testContext) {
		this.testContext = testContext;
	}

	public String getDealerDossierNr() {
		return dealerDossierNr;
	}

	public void setDealerDossierNr(String dealerDossierNr) {
		this.dealerDossierNr = dealerDossierNr;
	}

	public String getVIN() {
		return VIN;
	}

	public void setVIN(String vIN) {
		VIN = vIN;
	}

	public String getVINControlNumber() {
		return VINControlNumber;
	}

	public void setVINControlNumber(String vINControlNumber) {
		VINControlNumber = vINControlNumber;
	}

	public String getLicensePlate() {
		logger.debug("LicensePlate in getter: " + LicensePlate);
		return LicensePlate.trim();
	}

	public void setLicensePlate(String licensePlate) {
		LicensePlate = licensePlate;
	}
	
	public String getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(String maxWeight) {
		this.maxWeight = maxWeight;
	}
	
	public String getEngineDisplacement() {
		return engineDisplacement;
	}

	public void setEngineDisplacement(String engineDisplacement) {
		this.engineDisplacement = engineDisplacement;
	}
	
	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}
	
	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	public String getDateFirstRegistration() {
		//String pattern = "yyyy-MM-dd";
		//SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		//String date = simpleDateFormat.format(new Date(dateFirstRegistration));
		//return date;
		return dateFirstRegistration;
	}

	public String getDateFirstRegistrationLux() {
		return dateFirstRegistrationLux;
	}

	public String getNEDC() throws ParseException {
		Locale loc = new Locale("en", "US");
		NumberFormat nf = NumberFormat.getNumberInstance(loc);
		DecimalFormat df = (DecimalFormat)nf;
		String pattern = "#,###.##";
		df.applyPattern(pattern);
		//DecimalFormat myFormatter = new DecimalFormat(pattern);
		Double DoubleNEDC = Double.parseDouble(NEDCInput);
		NEDC = df.format(DoubleNEDC).toString();
		logger.debug("NEDC: " + NEDC);
	
		return NEDC;
	}

	public void setNEDC(String nEDC) {
		NEDC = nEDC;
	}

	public String getWLTP() throws ParseException {
		Locale loc = new Locale("en", "US");
		NumberFormat nf = NumberFormat.getNumberInstance(loc);
		DecimalFormat df = (DecimalFormat)nf;
		String pattern = "#,###.##";
		df.applyPattern(pattern);
		//DecimalFormat myFormatter = new DecimalFormat(pattern);
		Double DoubleWLTP = Double.parseDouble(WLTPInput);
		WLTP = df.format(DoubleWLTP).toString();
		logger.debug("WLTP: " + WLTP);
	
		return WLTP;

	}

	public void setWLTP(String wLTP) {
		WLTP = wLTP;
	}

	public String getWLTPLux() throws ParseException {
		Locale loc = new Locale("en", "US");
		NumberFormat nf = NumberFormat.getNumberInstance(loc);
		DecimalFormat df = (DecimalFormat)nf;
		String pattern = "#,###.##";
		df.applyPattern(pattern);
		//DecimalFormat myFormatter = new DecimalFormat(pattern);
		Double DoubleWLTPLux = Double.parseDouble(WLTPInputLux);
		WLTPLux = df.format(DoubleWLTPLux).toString();
		logger.debug("WLTPLux: " + WLTPLux);

		return WLTPLux;

	}

	public void setWLTPLux(String wLTPLux) {
		WLTPLux = wLTPLux;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public String getMileage() {
		return mileage;
	}

	public void setMileage(String mileage) {
		this.mileage = mileage;
	}

	public String getEvbNumber() {
		return evbNumber;
	}

	public void setEvbNumber(String evbNumber) {
		this.evbNumber = evbNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getNumberOfKeys() {

		Locale loc = new Locale("en", "EN");
		DecimalFormat df = new DecimalFormat();
		//DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		//symbols.setDecimalSeparator(',');
		//symbols.setGroupingSeparator(' ');
		//df.setDecimalFormatSymbols(symbols);
		//df.setMaximumFractionDigits(2);
		//df.setMinimumFractionDigits(2);

		//String s = "2";
		numberOfKeys = numberOfKeys.replace("," , ".");
		Double d = Double.parseDouble(numberOfKeys);
		numberOfKeys = df.format(d).toString();
		logger.debug("numberOfKeys: " + numberOfKeys);

		return numberOfKeys;
	}

	public void setNumberOfKeys(String numberOfKeys) {
		this.numberOfKeys = numberOfKeys;
	}

	public String getKeycode() {
		return keycode;
	}

	public void setKeycode(String keycode) {
		this.keycode = keycode;
	}

	public String getStartcode() {
		return startcode;
	}

	public void setStartcode(String startcode) {
		this.startcode = startcode;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getRelationWidthHeight() {
		return relationWidthHeight;
	}

	public void setRelationWidthHeight(String relationWidthHeight) {
		this.relationWidthHeight = relationWidthHeight;
	}

	public String getRimDiameter() {
		return rimDiameter;
	}

	public void setRimDiameter(String rimDiameter) {
		this.rimDiameter = rimDiameter;
	}

	public String getSpeedIndex() {
		return speedIndex;
	}

	public void setSpeedIndex(String speedIndex) {
		this.speedIndex = speedIndex;
	}

	public String getLoadIndex() {
		return loadIndex;
	}

	public void setLoadIndex(String loadIndex) {
		this.loadIndex = loadIndex;
	}
	
	public String getTyreSpecs() {
		return tyreSpecs;
	}

	public void setTyreSpecs(String tyreSpecs) {
		this.tyreSpecs = tyreSpecs;
	}

	public String getTyreBrand() {
		return tyreBrand;
	}

	public void setTyreBrand(String tyreBrand) {
		this.tyreBrand = tyreBrand;
	}
	
	public String getTyreType() {
		return tyreType;
	}

	public void setTyreType(String tyreType) {
		this.tyreType = tyreType;
	}

	public String getLicensePlateReturnedVehicle() {
		return licensePlateReturnedVehicle;
	}

	public void setLicensePlateReturnedVehicle(String licensePlateReturnedVehicle) {
		this.licensePlateReturnedVehicle = licensePlateReturnedVehicle;
	}

	public String getReturnedVehicle() {
		return returnedVehicle;
	}

	public String getReturnedVehicleLux() {
		return returnedVehicleLux;
	}
	
	public String getOwnerReturnedVehicle() {
		return ownerReturnedVehicle;
	}

	public void setSelectRegistrationCertificate(String selectRegistrationCertificate) {
		this.selectRegistrationCertificate = selectRegistrationCertificate;
	}

	public String getSelectRegistrationCertificate() {
		return selectRegistrationCertificate;
	}
	
	public String getSelectInsuranceCertificate() {
		return selectInsuranceCertificate;
	}

	public void setSelectInsuranceCertificate(String selectInsuranceCertificate) {
		this.selectInsuranceCertificate = selectInsuranceCertificate;
	}

	public String getSelectCertificateOfConformity() {
		return selectCertificateOfConformity;
	}

	public void setSelectCertificateOfConformity(String selectCertificateOfConformity) {
		this.selectCertificateOfConformity = selectCertificateOfConformity;
	}

	public String getSelectTechnicalInspectionCertificate() {
		return selectTechnicalInspectionCertificate;
	}

	public void setSelectTechnicalInspectionCertificate(String selectTechnicalInspectionCertificate) {
		this.selectTechnicalInspectionCertificate = selectTechnicalInspectionCertificate;
	}

	public String getSelectLegalKit() {
		return selectLegalKit;
	}

	public void setSelectLegalKit(String selectLegalKit) {
		this.selectLegalKit = selectLegalKit;
	}

	public String getSelectServiceManual() {
		return selectServiceManual;
	}

	public void setSelectServiceManual(String selectServiceManual) {
		this.selectServiceManual = selectServiceManual;
	}

	public String getSelectFuelCard() {
		return selectFuelCard;
	}

	public void setSelectFuelCard(String selectFuelCard) {
		this.selectFuelCard = selectFuelCard;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverLastName() {
		return driverLastName;
	}
	
	public String getDriverName() {
		return driverName;
	}
	
	public String getDriverDateOfBirth() {
		return driverDateOfBirth;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}

	public String getDriverBirthPlace() {
		return driverBirthPlace;
	}

	public void setDriverBirthPlace(String driverBirthPlace) {
		this.driverBirthPlace = driverBirthPlace;
	}

	

	

}