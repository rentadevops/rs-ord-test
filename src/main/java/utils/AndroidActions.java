package utils;

import com.google.common.collect.ImmutableMap;
//import io.appium.java_client.AppiumBy;
import cucumber.ScenarioContext;
import cucumber.TestContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HasOnScreenKeyboard;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
//import static com.epam.jdi.light.mobile.MobileUtils.executeDriverMethod;



public class AndroidActions extends AppiumUtils{
    AppiumDriver driver;
    private ScenarioContext scenarioContext;
    public AndroidActions(AppiumDriver driver) {
        this.driver = driver;
    }

    //public AndroidActions(TestContext testContext) {
    //    this.testContext = testContext;
    //}

    public AndroidActions() {

    }

    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }

    public AndroidActions(TestContext testContext) {
        scenarioContext = testContext.getScenarioContext();
    }

    public void longPressAction(WebElement ele) {
        ((JavascriptExecutor)driver).executeScript("mobile: longClickGesture",
                ImmutableMap.of("elementId",((RemoteWebElement)ele).getId(),
                        "duration", 2000));
    }


    /*public static void hideKeyboard() {

        executeDriverMethod(HasOnScreenKeyboard.class, HasOnScreenKeyboard::isKeyboardShown);
        executeDriverMethod(HidesKeyboard.class, HidesKeyboard::hideKeyboard);
    }*/

    /*public void scrollToEndAction() {
        boolean canScrollMore;
        do {
            canScrollMore = (Boolean)((JavascriptExecutor) driver).executeScript("mobile: scrollGesture",
                     ImmutableMap.of("left", 100, "top", 100, "width", 200, "height", 200,"direction","down", "percent", 3.0));
        } while(canScrollMore);
    }*/

    public void scrollToText(String text) {
        //driver.findElement(AppiumBy.androidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\""+text+"\"));"));
    }

    public void swipeAction(WebElement ele, String direction) {
        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of("elementId", ((RemoteWebElement)ele).getId(),
                "direction", direction,
                "percent", 0.75));
    }
}
