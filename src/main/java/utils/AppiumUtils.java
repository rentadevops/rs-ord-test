package utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import cucumber.TestContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;


public class AppiumUtils {
    final Logger logger = LogManager.getLogger(AppiumUtils.class);
    public Process p;
    public ProcessBuilder builder;
    static String statusCode;

    //AppiumDriver driver;
    public AppiumDriverLocalService service;
    protected TestContext testContext;

    //public AppiumUtils(AppiumDriver driver) {
    //    this.driver = driver;
    //}
    public Double getFormattedAmount(String amount) {
        Double price = Double.parseDouble(amount.substring(1));
        return price;
    }

    public List<HashMap<String, String>> getJsonData(String jsonFilePath) throws IOException {
//System.getProperty("user.dir")+"//src//test//java//org//rahulshettyacademy//testData//eCommerce.json"
        // conver json file content to json string
        String jsonContent = FileUtils.readFileToString(new File(jsonFilePath), String.valueOf(StandardCharsets.UTF_8));

        ObjectMapper mapper = new ObjectMapper();
        List<HashMap<String, String>> data = mapper.readValue(jsonContent,
                new TypeReference<List<HashMap<String, String>>>() {
                });

        return data;

    }

    /*public AppiumDriverLocalService startAppiumServer(String ipAddress, int port)
    {
        service = new AppiumServiceBuilder().withAppiumJS(new File("C:/Users/AdAntonisse/AppData/Roaming/npm/node_modules/appium/build/lib/main.js"))
                .withIPAddress(ipAddress).usingPort(port).build();
        service.start();
        return service;
    }*/

    public AppiumDriver startAppiumServer() throws MalformedURLException {
        Runtime rt = Runtime.getRuntime();
        try {
            String[] command = new String[]{"C:/Windows/System32/cmd.exe", "/c", "start cmd.exe", "/c", "appium", "-a", "127.0.0.1", "-p", "4723", "--session-override", "-dc"};
            builder = new ProcessBuilder(command);
            p = builder.start();

            Thread.sleep(10000);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        UiAutomator2Options options = new UiAutomator2Options();
        options.setAutomationName("uiautomator2");
        //options.setDeviceName("Galaxy S21 5G");
        //options.setDeviceName("Galaxy S21 5G");
        //options.setDeviceName("Pixel 2 XL API 31");
        options.setPlatformVersion("12");
        options.setPlatformName("Android");
        options.setChromedriverExecutable("C:/UdemyAppium/Drivers/chromedriver.exe");
        options.setCapability("browserName", "Chrome");
        options.setCapability("autoAcceptAlerts", "true");
        //options.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", true));
        options.setPlatformName("Android");
        //options.setDeviceName("AdjesMob1");
        //options.setDeviceName("Pixel 2 XL API 31");
        //options.setDeviceName("Galaxy Nexus API 28");
        options.setDeviceName("TestConnection");
        //options.setCapability("video", true);
        //options.setCapability("network", true);
        //options.setCapability("console", true);
        //options.setCapability("visual", true);

        //AppiumDriver appiumDriver = new AppiumDriver(new URL("http://localhost"), options);
        //DesiredCapabilities capabilities = new DesiredCapabilities();
        //capabilities.setBrowserName("chrome");
        //capabilities.setCapability("browserName", "Chrome");
        //capabilities.setCapability("platformName", "Android");
        //capabilities.setCapability("platformVersion", "12");
        //capabilities.setCapability("deviceName", "TestConnection");
        //capabilities.setCapability("autoAcceptAlerts", true);
        //options.setCapability("chromeDriverExecutable", "C:/UdemyAppium/Drivers/chromedriver.exe");

        logger.debug("Beforeclass Start android driver");
        AppiumDriver driver = new AppiumDriver(new URL("http://127.0.0.1:4723"), options);
        logger.debug("Beforeclass driver " + driver);
        //androidDriver.get("https://www.google.com");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        return driver;
    }

    public AppiumDriver startAppiumServerLambdaTest() throws MalformedURLException {

        /*List<LinkedHashMap<String,String>> list = new ArrayList<LinkedHashMap<String, String>>();

        LinkedHashMap<String, String> devices = new LinkedHashMap<String, String>();
        devices.put("Pixel 4","10");                    //OK
        devices.put("Galaxy Note20","11");              //OK
        devices.put("Galaxy Tab S8","12");              //OK
        devices.put("Galaxy S20+","10");                //OK
        devices.put("Galaxy S8","7");                   //OK
        devices.put("Galaxy J7 Prime","8");             //OK
        devices.put("Moto G7 Play","9");                //OK
        devices.put("OnePlus Nord","11");               //OK
        devices.put("Vivo T1 5G","12");
        devices.put("Xperia Z5 Dual","7.1");            //OK
        devices.put("Zenfone 6","10");
        devices.put("Galaxy S10","11");
        devices.put("Galaxy Note20 Ultra 5G","11");
        devices.put("Galaxy S21 Ultra 5G","11");
        devices.put("Galaxy S21 5G","11");
        devices.put("Galaxy S21 5G","12");
        devices.put("Galaxy S22","12");
        devices.put("Galaxy S22 Ultra","12");
        devices.put("Galaxy S20","11");
        devices.put("Galaxy S10","9");
        devices.put("Galaxy M12","11");
        devices.put("Huawei P30","10");
        devices.put("Huawei P30 Pro","10");
        devices.put("Huawei Mate 20 Pro","10");
        devices.put("Huawei P20 Pro","10");
        devices.put("Huawei Mate 20 Pro","9");
        devices.put("Vivo Y12","11");
        devices.put("Vivo Y20g","11");
        devices.put("Vivo Y50","10");

        list.add(devices);

        for(Map<String, String> map : list){
            for(String key : map.keySet()){
                System.out.println("key: " + key + " value " + map.get(key));
            }
        }*/

        String userName = System.getenv("LT_USERNAME") == null ?
                "a.antonisse" : System.getenv("LT_USERNAME"); //Add username here
        String accessKey = System.getenv("LT_ACCESS_KEY") == null ?
                "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0" : System.getenv("LT_ACCESS_KEY"); //Add accessKey here

        String gridURL = "@mobile-hub.lambdatest.com/wd/hub";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        HashMap<String, Object> ltOptions = new HashMap<String, Object>();
        ltOptions.put("w3c", true);
        ltOptions.put("platformName", "android");
        ltOptions.put("deviceName", "Galaxy S21 5G");
        ltOptions.put("platformVersion", "11");
        ltOptions.put("deviceOrientation", "PORTRAIT");
        capabilities.setCapability("visual", true);
        ltOptions.put("isRealMobile", true);
        capabilities.setCapability("console", true);
        ltOptions.put("network", true);
        ltOptions.put("build", "DIGITAL_DELIVERY");
        ltOptions.put("name", "@OL-no-notification-mobile-nl");
        ltOptions.put("project", "ORD");

        capabilities.setCapability("autoAcceptAlerts", true);
        capabilities.setCapability("lt:options", ltOptions);
        //capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        //capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);

        String hub = "http://" + userName + ":" + accessKey + gridURL;

        AppiumDriver driver = new AppiumDriver(new URL(hub), capabilities);

        return driver;
    }

    public AppiumDriver startAppiumServerLambdaTestWithDevice(String device, String version) throws MalformedURLException {

        //List<LinkedHashMap<String,String>> list = new ArrayList<LinkedHashMap<String, String>>();

        /*LinkedHashMap<String, String> devices = new LinkedHashMap<String, String>();
        devices.put("Pixel 4","10");                    //OK
        devices.put("Galaxy Note20","11");              //OK
        devices.put("Galaxy Tab S8","12");              //OK
        devices.put("Galaxy S20+","10");                //OK
        devices.put("Galaxy S8","7");                   //OK
        devices.put("Galaxy J7 Prime","8");             //OK
        devices.put("Moto G7 Play","9");                //OK
        devices.put("OnePlus Nord","11");               //OK
        devices.put("Vivo T1 5G","12");
        devices.put("Xperia Z5 Dual","7.1");            //OK
        devices.put("Zenfone 6","10");
        devices.put("Galaxy S10","11");
        devices.put("Galaxy Note20 Ultra 5G","11");
        devices.put("Galaxy S21 Ultra 5G","11");
        devices.put("Galaxy S21 5G","11");
        devices.put("Galaxy S21 5G","12");
        devices.put("Galaxy S22","12");
        devices.put("Galaxy S22 Ultra","12");
        devices.put("Galaxy S20","11");
        devices.put("Galaxy S10","9");
        devices.put("Galaxy M12","11");
        devices.put("Huawei P30","10");
        devices.put("Huawei P30 Pro","10");
        devices.put("Huawei Mate 20 Pro","10");
        devices.put("Huawei P20 Pro","10");
        devices.put("Huawei Mate 20 Pro","9");
        devices.put("Vivo Y12","11");
        devices.put("Vivo Y20g","11");
        devices.put("Vivo Y50","10");

        list.add(devices);

        for(Map<String, String> map : list){
            for(String key : map.keySet()){
                System.out.println("key: " + key + " value " + map.get(key));
            }
        }*/

        String userName = System.getenv("LT_USERNAME") == null ?
                "a.antonisse" : System.getenv("LT_USERNAME"); //Add username here
        String accessKey = System.getenv("LT_ACCESS_KEY") == null ?
                "WQ2N4AlMf9Y8T19MUEynIpTEyAs8jxmBt27bJ8rFhFojLTtEC0" : System.getenv("LT_ACCESS_KEY"); //Add accessKey here

        String gridURL = "@mobile-hub.lambdatest.com/wd/hub";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        HashMap<String, Object> ltOptions = new HashMap<String, Object>();
        ltOptions.put("w3c", true);
        ltOptions.put("platformName", "android");
        ltOptions.put("deviceName", device);
        ltOptions.put("platformVersion", version);
        ltOptions.put("deviceOrientation", "PORTRAIT");
        capabilities.setCapability("visual", true);
        ltOptions.put("isRealMobile", true);
        capabilities.setCapability("console", true);
        ltOptions.put("network", true);
        ltOptions.put("build", "DIGITAL_DELIVERY");
        ltOptions.put("name", "@OL-no-notification-mobile-nl");
        ltOptions.put("project", "ORD");
        capabilities.setCapability("autoAcceptAlerts", true);
        capabilities.setCapability("lt:options", ltOptions);

        String hub = "http://" + userName + ":" + accessKey + gridURL;

        AppiumDriver driver = new AppiumDriver(new URL(hub), capabilities);

        return driver;
    }

    public static String postGetCookie() throws FileNotFoundException, IOException {
        RestAssured.baseURI = "https://api.dar.tst.rentasolutions.org";
        //GetCookie getCookie = new GetCookie();
        //COOKIE = getCookie.getCookie(getLeasingCompany()).asString();
        String jsonBody = "{\"email\": \"\",\"firstName\": \"Adje\",\"lastName\": \"Antonisse\"}";

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json")
                //.header("cookie", "sso=" + COOKIE)
                //.header("x-wss-rs-user-number", getLeasingCompany())
                //.pathParams("id", orderId)
                .body(jsonBody)
                .log().all();
        //.filter(new AllureRestAssured());

        Response response = request.post("/test/cookie");
        System.out.println("response: " + response.asPrettyString());
        statusCode = String.valueOf(response.getStatusLine());
        String res = response.asPrettyString();

        //InputStream inputStream = getFileInputStream.getInputStream("ApiSchemas/activity-defect-task-warranty-resource/PostActivityDefectTaskWarrantySchema.json");
        //response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchema(inputStream)).log().all();
        //getFileInputStream.closeInputStream(inputStream);

        return res;

    }


    public void waitForElementToAppear(WebElement ele, AppiumDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.attributeContains((ele), "text", "Cart"));

    }

    public String getScreenshotPath(String testCaseName, AppiumDriver driver) throws IOException
    {
        File source = driver.getScreenshotAs(OutputType.FILE);
        String destinationFile = System.getProperty("user.dir")+"//reports"+testCaseName+".png";
        FileUtils.copyFile(source, new File(destinationFile));
        return destinationFile;
        //1. capture and place in folder //2. extent report pick file and attach to report
    }


}
