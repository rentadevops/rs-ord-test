@MU-1440 @Manual
Feature: MU-1440

  @MU-1440 @MANUAL @COMPLETED
  Scenario: Check alignment
    Given User is logged in
    When User clicks on new registration
    When Vehicle info is displayed
    Then User verifies Speed Pedelec is now a tickbox unstead of a toggle
    When Plate Request is displayed
    Then User verifies that all fields in right column are aligned
